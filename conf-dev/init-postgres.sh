#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER anis LOGIN PASSWORD 'anis';
    CREATE USER keycloak LOGIN PASSWORD 'keycloak';
    CREATE DATABASE anis_metamodel OWNER anis;
    CREATE DATABASE anis_test OWNER anis;
    CREATE DATABASE keycloakdb OWNER keycloak;
EOSQL
psql -v ON_ERROR_STOP=1 -f /sql/data_test.sql --username "anis" --dbname "anis_test"
