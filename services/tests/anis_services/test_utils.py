import os
from unittest import mock
from unittest.mock import call
import pytest

from anis_services.utils import *

class Object(object):
    pass

class MockJWT:
    def decode(self, *args):
        pass

class TestIfsctoolsUtils:

    @mock.patch.dict(os.environ, {}, clear=True)
    def test_check_config(self):
        with pytest.raises(ConfigKeyNotFound):
            check_config()
