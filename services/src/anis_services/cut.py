# Third party imports
from astropy.io import fits
from astropy.nddata import Cutout2D
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy import units as u

def fits_cut(filename, ra, dec, radius):
    fits_file = fits.open(filename, memmap=True)

    for hdu in fits_file:
        # Cut the FITS HDU
        cutout = fits_cut_hdu(hdu, ra, dec, radius)
        
        # Put the cutout image in the FITS HDU
        hdu.data = cutout.data

        # Update the FITS header with the cutout WCS
        hdu.header.update(cutout.wcs.to_header())
    
    return fits_file

def fits_cut_hdu(hdu, ra, dec, radius):
    # Extract WCS infos
    wcs = WCS(hdu.header)

    # SkyCoord position with ra and dec 
    position = SkyCoord(ra, dec, unit="deg")

    # Size with radius
    size = u.Quantity(radius * 2, u.si.arcsec)

    # Return the cutout, including the WCS
    return Cutout2D(hdu.data, position=position, size=size, wcs=wcs)

def getImageLimits(filename):
    hdu = fits.open(filename)[0]
    wcs = WCS(hdu.header)

    pmin = wcs.pixel_to_world(wcs.pixel_shape[0], wcs.pixel_shape[1])
    pmax = wcs.pixel_to_world(0, 0)

    return {
        "ra_min": pmin.ra.degree,
        "ra_max": pmax.ra.degree,
        "dec_min": pmin.dec.degree,
        "dec_max": pmax.dec.degree
    }