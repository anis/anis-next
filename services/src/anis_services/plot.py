# Third party imports
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from aplpy import FITSFigure

def create_figure(hdu, stretch, pmin, pmax, axes, theme=None):
    if axes == 'true':
        image = FITSFigure(hdu)
        image.show_grayscale(stretch=stretch, pmin=pmin, pmax=pmax)
        if theme is not None:
            image.set_theme(theme)
        return image
    else:
        fig = plt.figure()
        image = FITSFigure(hdu, figure=fig)
        image.show_grayscale(stretch=stretch, pmin=pmin, pmax=pmax)
        image.axis_labels.hide()
        image.tick_labels.hide()
        image.ticks.hide()
        return fig
