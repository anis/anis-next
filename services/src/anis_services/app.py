# Standard library imports
import io, os
import uuid

# Third party imports
from flask import Flask, request, Response, make_response
from flask_cors import CORS
import numpy as np
from astropy.io import fits
from astropy.table import Table

# Local application imports
from anis_services import cut
from anis_services import plot
from anis_services import spectra
from anis_services import utils

def create_app():
    utils.check_config()

    app = Flask(__name__)
    CORS(app)

    @app.route('/')
    def index():
        return {
            "message": "it works!"
        }

    @app.route('/get-fits-image-limits/<dname>')
    def get_fits_image_limits(dname):
        filename = request.args.get('filename', default=None)
        if filename is None:
            return {"message": "Parameter filename is mandatory"}, 400

        try:
            file_path = utils.get_file_path(dname, filename)
        except utils.DatasetNotFound as e:
            return {"message": str(e)}, 404
        except utils.FileForbidden as e:
            return {"message": str(e)}, 403
        except utils.FileNotFound as e:
            return {"message": str(e)}, 404
        except utils.AnisServerError as e:
            return {"message": str(e)}, 500
        
        return cut.getImageLimits(file_path)

    @app.route('/fits-cut/<dname>')
    def fits_cut(dname):
        filename = request.args.get('filename', default=None)
        if filename is None:
            return {"message": "Parameter filename is mandatory"}, 400

        ra = float(request.args.get('ra', ''))
        dec = float(request.args.get('dec', ''))
        radius = float(request.args.get('radius', ''))

        if ra is None:
            return {"message": "Parameter ra is mandatory"}, 400
        if dec is None:
            return {"message": "Parameter dec is mandatory"}, 400
        if radius is None:
            return {"message": "Parameter radius is mandatory"}, 400

        try:
            file_path = utils.get_file_path(dname, filename)
        except utils.DatasetNotFound as e:
            return {"message": str(e)}, 404
        except utils.FileForbidden as e:
            return {"message": str(e)}, 403
        except utils.FileNotFound as e:
            return {"message": str(e)}, 404
        except utils.AnisServerError as e:
            return {"message": str(e)}, 500

        fits_file = cut.fits_cut(file_path, ra, dec, radius)

        output = io.BytesIO()
        fits_file.writeto(output, overwrite=True)
        return Response(output.getvalue(), mimetype='image/fits')

    @app.route('/fits-cut-to-png/<dname>')
    def fits_cut_to_png(dname):
        filename = request.args.get('filename', default=None)
        if filename is None:
            return {"message": "Parameter filename is mandatory"}, 400

        ra = request.args.get('ra', default=None, type=float)
        dec = request.args.get('dec', default=None, type=float)
        radius = request.args.get('radius', default=None, type=float)
        stretch = request.args.get('stretch', default=None)
        pmin = request.args.get('pmin', default=None, type=float)
        pmax = request.args.get('pmax', default=None, type=float)
        axes = request.args.get('axes', default=None)
        theme = request.args.get('theme', default=None)

        if ra is None:
            return {"message": "Parameter ra is mandatory"}, 400
        if dec is None:
            return {"message": "Parameter dec is mandatory"}, 400
        if radius is None:
            return {"message": "Parameter radius is mandatory"}, 400
        if stretch is None:
            return {"message": "Parameter stretch is mandatory"}, 400
        if pmin is None:
            return {"message": "Parameter pmin is mandatory"}, 400
        if pmax is None:
            return {"message": "Parameter pmax is mandatory"}, 400
        if axes is None:
            return {"message": "Parameter axes is mandatory"}, 400

        try:
            file_path = utils.get_file_path(dname, filename)
        except utils.DatasetNotFound as e:
            return {"message": str(e)}, 404
        except utils.FileForbidden as e:
            return {"message": str(e)}, 403
        except utils.FileNotFound as e:
            return {"message": str(e)}, 404
        except utils.AnisServerError as e:
            return {"message": str(e)}, 500

        fits_file = cut.fits_cut(file_path, ra, dec, radius)

        output = io.BytesIO()
        fig = plot.create_figure(fits_file, stretch, pmin, pmax, axes, theme)
        if axes == 'true':
            fig.savefig(output, format="png")
        else:
            fig.savefig(output, format="png", bbox_inches='tight', pad_inches = 0)

        return Response(output.getvalue(), mimetype='image/png')

    @app.route('/fits-to-png/<dname>')
    def fits_to_png(dname):
        filename = request.args.get('filename', default=None)
        if filename is None:
            return {"message": "Parameter filename is mandatory"}, 400

        stretch = request.args.get('stretch', default=None)
        pmin = request.args.get('pmin', default=None, type=float)
        pmax = request.args.get('pmax', default=None, type=float)
        axes = request.args.get('axes', default=None)
        theme = request.args.get('theme', default=None)

        try:
            file_path = utils.get_file_path(dname, filename)
        except utils.DatasetNotFound as e:
            return {"message": str(e)}, 404
        except utils.FileForbidden as e:
            return {"message": str(e)}, 403
        except utils.FileNotFound as e:
            return {"message": str(e)}, 404
        except utils.AnisServerError as e:
            return {"message": str(e)}, 500

        fig = plot.create_figure(file_path, stretch, pmin, pmax, axes, theme)
        output = io.BytesIO()
        fig.save(output, format="png")

        return Response(output.getvalue(), mimetype='image/png')

    @app.route('/spectra-to-csv/<dname>')
    def spectra_to_csv(dname):
        filename = request.args.get('filename', default=None)
        if filename is None:
            return {"message": "Parameter filename is mandatory"}, 400

        try:
            file_path = utils.get_file_path(dname, filename)
            csv = spectra.spectra_to_csv(file_path, filename)
        except utils.DatasetNotFound as e:
            return {"message": str(e)}, 404
        except utils.FileForbidden as e:
            return {"message": str(e)}, 403
        except utils.FileNotFound as e:
            return {"message": str(e)}, 404
        except utils.AnisServerError as e:
            return {"message": str(e)}, 500

        return Response(csv, mimetype='text/csv')
        
    @app.route('/download-fits/<selectedData>')
    def download_fits(selectedData):
        #retrieve params from the request  
        args = request.args.get('a')
        dataset_name = request.args.get('n')
          
        #request get data from the server
        data_from_server = utils.get_data(arguments=args, dname=dataset_name)
     
        # create create a new fits table file
        colomn_names =  tuple(data_from_server[0])
        column_values_dictionary = dict()
        
        for name in colomn_names:
            column_values_dictionary[name] = list()
            for data in data_from_server:
                column_values_dictionary[name].append(data[name])         
        
        output = io.BytesIO()
        t = Table([ list(item) for item in list(column_values_dictionary.values())], names=colomn_names)
        t.write(output, format='fits')
        return Response(output.getvalue(), mimetype='application/fits')
          
    return app
    
if __name__ == "__main__":
    try:
        app = create_app()
        app.run(host="0.0.0.0")
    except utils.ConfigKeyNotFound as e:
        app.logger.error("Config error")
        app.logger.error(e)
