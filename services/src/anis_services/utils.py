# Standard library imports
import os

# Third party imports
import requests

def check_config():
    check_keys = {
        'DATA_PATH',
        'SERVER_URL'
    }
    for value in check_keys:
        if value not in os.environ.keys():
            raise ConfigKeyNotFound(value)

def get_dataset(dname):
    server_url = os.environ["SERVER_URL"]

    r = requests.get(server_url + "/dataset/" + dname)

    if (r.status_code == 404):
        raise DatasetNotFound(dname)
    if (r.status_code == 500):
        raise AnisServerError(r.json()["message"])

    return r.json()

def get_data(dname: str, arguments: str):
    server_url = os.environ["SERVER_URL"]
    r = requests.get(f"{server_url}/search/{dname}?a={arguments}")

    if (r.status_code == 404):
        raise DatasetNotFound("")
    if (r.status_code == 500):
        raise AnisServerError(r.json()["message"])
    return r.json()
def get_colums_name(data_list) ->list:
    return data_list[0].keys()

def get_file_path(dname, filename):
    data_path = os.environ["DATA_PATH"]
    dataset = get_dataset(dname)
    dataset_data_path = dataset["full_data_path"]
    file_path = data_path + dataset_data_path + '/' + filename

    if ('..' in filename):
        raise FileForbidden(filename)

    if (not os.path.exists(file_path)):
        raise FileNotFound(filename)

    return file_path

class DatasetNotFound(Exception):
    def __init__(self, dname):
        Exception.__init__(self, dname)
        self.dname = dname

    def __str__(self):
        return f"Dataset {self.dname} was not found"

class AnisServerError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)
        self.message = message

    def __str__(self):
        return f"Anis-server error: {self.message}"

class FileForbidden(Exception):
    def __init__(self, filename):
        Exception.__init__(self, filename)
        self.filename = filename

    def __str__(self):
        return f"File {self.filename} => It is forbidden to use '..'"

class FileNotFound(Exception):
    def __init__(self, filename):
        Exception.__init__(self, filename)
        self.filename = filename

    def __str__(self):
        return f"File {self.filename} was not found"

class ConfigKeyNotFound(Exception):
    def __init__(self, value):
        Exception.__init__(self, value)
        self.value = value

    def __str__(self):
        return f"{self.value} was not found in the environment variables"
