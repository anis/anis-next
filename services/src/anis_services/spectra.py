# Third party imports
from astropy.io import fits

# Local application imports
from anis_services.spectra_strategies import StrategySolver
from anis_services.spectra_strategies import DefaultStrategy
from anis_services.spectra_strategies import SixdFStrategy
from anis_services.spectra_strategies import GamaDR2AATStrategy
from anis_services.spectra_strategies import GamaDR2LTStrategy
from anis_services.spectra_strategies import ZCosmosBrightDr3Strategy
from anis_services.spectra_strategies import EspressoStrategy

def spectra_to_csv(file_path, filename):
    hdulist = fits.open(file_path)

    if(filename.count('6dF') > 0):
        solver = StrategySolver(SixdFStrategy())
    elif(filename.count('XXL-AAOmega') > 0):
        solver = StrategySolver(DefaultStrategy())
    elif(filename.count('AAT') > 0):
        solver = StrategySolver(GamaDR2AATStrategy())
    elif(filename.count('LT') > 0):
        solver = StrategySolver(GamaDR2LTStrategy())
    elif(filename.count('zCOSMOS_BRIGHT_DR3') > 0):
        solver = StrategySolver(ZCosmosBrightDr3Strategy())
    elif(filename.count('ESPRE') > 0):
        solver = StrategySolver(EspressoStrategy())
    else:
        solver = StrategySolver(DefaultStrategy())

    csv = solver.execute(hdulist)
    hdulist.close()
    return csv