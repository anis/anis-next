# Local application imports
from .find_stratgy import FindStrategy

class DefaultStrategy(FindStrategy):
    def algorithm(self, hdulist):
        # sys.stderr.write('default strategy selected (VVDS, ZCOSMOS private)\n')
        
        header = hdulist[0].header
        keys = header.keys()
        
        naxis = header['NAXIS']
        nbpix = header['NAXIS1']
        
        if 'CRPIX1' in keys:
            crpix = header['CRPIX1']
        elif 'CRPIX' in keys:
            crpix = header['CRPIX']

        if 'CRVAL1' in keys:
            crval = header['CRVAL1']
        elif 'CRVAL' in keys:
            crval = header['CRVAL']
            
        if 'CDELT1' in keys:
            cdelt = header['CDELT1']
        elif 'CDELT' in keys:
            cdelt = header['CDELT']
        elif 'CD1_1' in keys:
            cdelt = header['CD1_1']
            
        if(naxis == 1):
            tbdata = hdulist[0].data
        else:
            tbdata = hdulist[0].data[0]
        
        csv = "x,Flux\n"
        i = 0
        while i < nbpix:
            csv += '%.2f,%e' % ((((i - crpix + 1) * cdelt) + crval), tbdata[i])
            csv += "\n"
            i = i + 1
        
        return csv
