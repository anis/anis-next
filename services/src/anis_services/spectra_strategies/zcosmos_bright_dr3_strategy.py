# Local application imports
from .find_stratgy import FindStrategy

class ZCosmosBrightDr3Strategy(FindStrategy):
    def algorithm(self, hdulist):
        # sys.stderr.write('zCosmos Bright Dr3 strategy selected\n')
        
        header = hdulist[1].header
        keys = header.keys()

        nbpix = header['NELEM']
        tbdata = hdulist[1].data[0]
        
        csv = "x,Flux\n"
        i = 0
        while i < nbpix:
            csv += '%.2f,%e' % (tbdata[0][i], tbdata[1][i])
            csv += "\n"
            i = i + 1

        return csv