# Standard library imports
import logging, json, os
from zipfile import ZipFile

# Local application imports
from anis_tasks import utils

def archive_handler(ch, method, properties, body):
    logging.info("Processing a new archive message")

    # Decode JSON
    message = json.loads(body)

    # Retrieve metadata information
    dataset = utils.get_dataset(message["dataset_name"])
    attributes = utils.get_attributes(message["dataset_name"])
    attributes_selected = get_attributes_selected_archive_files(attributes, message["param_a"])

    # Retrieve data
    data = utils.search_data(message["dataset_name"], message["query"], message["token"])

    # create a ZipFile object
    data_path = utils.get_data_path()
    archive_folder = utils.get_archive_folder()
    zip_path = data_path + archive_folder + "/" + message["archive_id"] + ".zip"
    zip = ZipFile(zip_path + ".tmp", 'w')

    # Search files
    for row in data.json():
        for attribute in attributes_selected:
            attribute_label = attribute["label"]
            file_path = utils.get_data_path() + dataset["full_data_path"] + "/" + str(row[attribute_label])
            if (os.path.exists(file_path) and os.path.isfile(file_path) and file_path):
                # Adds file to the zip archive
                zip.write(file_path, row[attribute_label])

    # close the Zip File
    zip.close()

    # Rename the tmp zip file with the correct archive name
    os.rename(zip_path + ".tmp", zip_path)

    logging.info("Zip created: " + zip_path)

def get_attributes_selected_archive_files(attributes, param_a):
    attributes_selected = []
    ids = param_a.split(";")
    for attribute in attributes:
        if (str(attribute["id"]) in ids and attribute["archive"]):
            attributes_selected.append(attribute)
    
    return attributes_selected