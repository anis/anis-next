# Standard library imports
import logging, sys

# Third party imports
import pika
from retry import retry

# Local application imports
from anis_tasks import utils, archive

@retry(pika.exceptions.AMQPConnectionError, delay=5, jitter=(1, 3))
def run():
    try:
        # Check config variables
        utils.check_config()

        # Connect to the rabbitMQ server
        credentials = pika.PlainCredentials(utils.get_rmq_user(), utils.get_rmq_password())
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=utils.get_rmq_host(), port=utils.get_rmq_port(), credentials=credentials))
        channel = connection.channel()

        # Add archive task handler
        channel.queue_declare(queue='archive')
        channel.basic_consume(queue='archive', on_message_callback=archive.archive_handler, auto_ack=True)

        # Start
        logging.info("ANIS tasks started")
        channel.start_consuming()
    except utils.ConfigKeyNotFound as e:
        logging.error("Config error")
        logging.error(e)
        return

if __name__ == '__main__':
    logging.basicConfig(
        stream=sys.stderr,
        level=logging.INFO,
        format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s'
    )
    run()