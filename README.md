# AstroNomical Information System - ANIS 

## Introduction

AstroNomical Information System is a generic web tool that aims to facilitate
the provision of data (Astrophysics), accessible from a database, for the scientific 
community.

This software allows you to control one or more databases related to astronomical 
projects and allows access to datasets via a web interface or URLs.

Anis is protected by the CeCILL licence (see LICENCE file at the software root).

## Authors

Here is the list of people involved in the development:

* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Chrystel Moreau` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Tifenn Guillas` : Laboratoire d'Astrophysique de Marseille (CNRS)

## More resources:

* [Website](https://anis.lam.fr)
* [Documentation](https://anis.lam.fr/doc/)

## Installing and starting the application

Anis Server contains a Makefile that helps the developer to install and start the application.

To list all operations availables just type `make` in your terminal at the root of this application.

- To install client dependancies: `make install_client`
- To install server dependancies: `make install_server`
- To build or rebuild all docker images and start containers: `make rebuild`
- To start/stop/restart/status all services: `make start|stop|restart|status`
- To display logs for all services: `make logs`
- To open a shell command into client container: `make shell_client`
- To open a shell command into server container: `make shell_server`
- To execute server tests suite: `make test_server`
- To execute php code sniffer: `make phpcs`
- To create the metamodel database: `make create-db`
- TO remove the metadata database: `make remove-pgdata`

## Web interface

In development mode the web interface can be accessed at the following url : http://localhost:4200
