UID := 1000
GID := 1000


list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  rebuild          > Rebuild all images and start containers for dev only"
	@echo "  start            > Start containers"
	@echo "  restart          > Restart containers"
	@echo "  stop             > Stop and kill running containers"
	@echo "  status           > Display stack containers status"
	@echo "  logs             > Display containers logs"
	@echo "  install_client   > Install client dependencies"
	@echo "  shell_client     > Shell into angular client container"
	@echo "  build_client     > Generate the angular client dist application (html, css, js)"
	@echo "  test_client      > Run the angular client unit tests and generate code coverage report"
	@echo "  test_client-live > Run the angular client unit tests on every file change"
	@echo "  open-coverage    > Open the client code coverage report in a browser (only available for Linux)"
	@echo "  install_server   > Install server dependencies"
	@echo "  shell_server     > Shell into php server container"
	@echo "  test_server      > Starts the server php unit tests"
	@echo "  phpcs            > Run php code sniffer test suite"
	@echo "  install_services > install services dependencies (virtualenv)"
	@echo "  shell_services   > Shell into python services"
	@echo "  test_services    > Starts the services unit tests"
	@echo "  install_tasks    > install tasks dependencies (virtualenv)"
	@echo "  shell_tasks      > Shell into python tasks"
	@echo "  test_tasks       > Starts the tasks unit tests"
	@echo "  create-db        > Create a database for dev only (need token_enabled=0)"
	@echo "  remove-pgdata    > Remove the anis-next database"
	@echo "  remove-matodata  > Remove the anis-next matomo volumes"
	@echo ""

rebuild:
	@docker-compose up --build -d

start:
	@docker-compose up -d

restart: stop start

stop:
	@docker-compose kill
	@docker-compose rm -v --force

status:
	@docker-compose ps

logs:
	@docker-compose logs -f -t

install_client:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR)/client:/project \
	-w /project node:16-slim yarn install

shell_client:
	@docker-compose exec client bash

build_client:
	@docker-compose exec client ng build

test_client:
	@docker-compose exec client npx jest --coverage --collectCoverageFrom='src/**/*.ts'

test_client_live:
	@docker-compose exec client npx jest --watchAll --coverage

open_coverage_report:
	xdg-open client/coverage/anis-client/index.html

install_server:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-e COMPOSER_CACHE_DIR=/dev/null \
	-v $(CURDIR)/server:/project \
	-w /project composer install --ignore-platform-reqs

shell_server:
	@docker-compose exec server bash

test_server:
	@docker run --init -it --rm \
	-v $(CURDIR)/server:/project \
	-w /project jakzal/phpqa:php8.0 phpdbg -qrr ./vendor/bin/phpunit --bootstrap ./tests/bootstrap.php \
	--whitelist src --colors --coverage-html ./phpunit-coverage ./tests

phpcs:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR)/server:/project \
	-w /project jakzal/phpqa:php8.0 phpcs --standard=PSR12 --extensions=php --colors src tests

install_services:
	@docker run --init -it --rm --user $(UID):$(GID) -v $(CURDIR)/services:/project -w /project python:3.8 /bin/bash -c "python3 -m venv venv && source /project/venv/bin/activate && pip install -r requirements.txt"

shell_services:
	@docker-compose exec services bash

test_services:
	@docker-compose exec services /bin/bash -c "source ./venv/bin/activate && pytest"

install_tasks:
	@docker run --init -it --rm --user $(UID):$(GID) -v $(CURDIR)/tasks:/project -w /project python:3.8 /bin/bash -c "python3 -m venv venv && source /project/venv/bin/activate && pip install -r requirements.txt"

shell_tasks:
	@docker-compose exec tasks bash

test_tasks:
	@docker-compose exec tasks /bin/bash -c "source ./venv/bin/activate && pytest"

create-db:
	@docker-compose exec server sh /mnt/init-keycloak.sh
	@docker-compose exec server sh /mnt/create-db.sh

remove-pgdata:
	@docker volume rm anis-next_pgdata

remove-matodata:
	@docker volume rm anis-next_mariadata
	@docker volume rm anis-next_matomo
