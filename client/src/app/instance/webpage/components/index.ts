import { WebpageComponent } from './webpage-content.component';
import { DatatableComponent } from './datatable.component';

export const dummiesComponents = [
    WebpageComponent,
    DatatableComponent
];
