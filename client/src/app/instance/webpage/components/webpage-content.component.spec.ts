
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SimpleChange } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Instance, Webpage } from "src/app/metamodel/models";
import { StyleService } from "src/app/shared/services/style.service";
import { WebpageComponent } from "./webpage-content.component";

describe('Instance][Webpage][Components] WebpageComponent', () => {
    let component: WebpageComponent;
    let fixture: ComponentFixture<WebpageComponent>;
    let style = new StyleService();
    let webpage: Webpage;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageComponent
            ],
            providers: [{ provide: StyleService, useValue: style }]
        });
        fixture = TestBed.createComponent(WebpageComponent);
        component = fixture.componentInstance;
        component.webpage = { ...webpage, name: 'test', style_sheet: 'test' };
        component.instance = { ...instance, name: 'test' };
        
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('ngOnChanges should call styleService addCSS method', () => {
        let spy = jest.spyOn(style, 'addCSS');
        component.ngOnChanges(
            {
                webpage: new SimpleChange({ ...webpage, name: 'test' }, { ...webpage, name: 'test2', style_sheet: 'test'}, false),

            }
        );
        fixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
    })


});