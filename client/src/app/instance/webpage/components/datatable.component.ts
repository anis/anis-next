/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-datatable',
    templateUrl: 'datatable.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatatableComponent {
    @Input() attributeList: Attribute[];
    @Input() data: any[];
}
