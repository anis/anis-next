import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap, skipWhile } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as webpageSelector from 'src/app/metamodel/selectors/webpage.selector';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';

@Injectable({
    providedIn: 'root'
})
export class WebpageTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(webpageSelector.selectWebpageListIsLoaded).pipe(
            map(webpageListIsLoaded => {
                if (!webpageListIsLoaded) {
                    this.store.dispatch(webpageActions.loadWebpageList());
                }
                return webpageListIsLoaded;
            }),
            skipWhile(webpageListIsLoaded => !webpageListIsLoaded),
            switchMap(() => {
                return combineLatest([
                    this.store.pipe(select(instanceSelector.selectInstanceByRouteName)),
                    this.store.pipe(select(webpageSelector.selectWebpageByRouteName))
                ]).pipe(
                    map(([instance, webpage]) => `${instance.label} - ${webpage.title}`)
                );
            })
        );
    }
}
