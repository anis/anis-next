/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-dataset-sample',
    templateUrl: 'dataset-sample.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetSampleComponent implements OnInit {
    @Input() datasetName: string;
    @Input() sortingColumn: number;
    @Input() sortingDirection: string;
    @Input() nbItems: number;

    attributeList: Observable<Attribute[]>;
    data: Observable<any[]>;

    constructor(private http: HttpClient, private config: AppConfigService) { }

    ngOnInit() {
        this.attributeList = this.http.get<any[]>(`${this.config.apiUrl}/dataset/${this.datasetName}/attribute`);
        const query = `${this.datasetName}?a=all&o=${this.sortingColumn}:${this.sortingDirection}&p=${this.nbItems}:1`;
        this.data = this.http.get<any[]>(`${this.config.apiUrl}/search/${query}`);
    }
}
