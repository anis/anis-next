
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { DatasetSampleComponent } from "./dataset-sample.component";

describe('Instance][Webpage][Dynamic-content][Dynamic-Components] DatasetSampleComponent', () => {
    let component: DatasetSampleComponent;
    let fixture: ComponentFixture<DatasetSampleComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetSampleComponent
            ],
            providers: [
                HttpClient,
                { provide: AppConfigService, useValue: { apiUrl: 'test' } }
            ],
            imports: [
                HttpClientModule
            ]
        });
        fixture = TestBed.createComponent(DatasetSampleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});