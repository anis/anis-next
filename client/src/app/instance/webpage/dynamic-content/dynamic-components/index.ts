import { DatasetSampleComponent } from './dataset-sample.component';
import { FormGoToComponent } from './forms/form-goto.component';
import { FormFindIDComponent } from './forms/IDfinder/form-findID.component';

export const dynamicComponents = [
    DatasetSampleComponent,
    FormGoToComponent,
    FormFindIDComponent,
];
