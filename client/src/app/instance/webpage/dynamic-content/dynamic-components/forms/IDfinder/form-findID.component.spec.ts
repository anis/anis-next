/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFindIDComponent } from './form-findID.component';
import { FormsModule, NgForm } from '@angular/forms';

import { QueryMakerService } from './query-maker.service';
import { AliasesState, AliasesStore, GetterAliases } from './aliases.store';
import { of } from 'rxjs';
import { AttributeService } from 'src/app/metamodel/services/attribute.service';

jest.mock('./query-maker.service');
jest.mock('src/app/metamodel/services/attribute.service');

describe('FormSampleComponent', () => {
    let component: FormFindIDComponent = null;
    let fixture: ComponentFixture<FormFindIDComponent> = null;

    let state: AliasesState = { aliases: [], areAliasesLoaded: false };

    let store = {
        aliases$: of(<string[]>[]),
        areAliasesLoaded$: of(false),
        setState: jest.fn((state: AliasesState) => {
            return state;
        }),
        getAliases: jest.fn((getter: GetterAliases) => {
            return getter;
        }),
    };


    beforeEach(async () => {
        TestBed.overrideComponent(FormFindIDComponent, {
            set: {
                providers: [
                    AttributeService,
                    QueryMakerService,
                    { provide: AliasesStore, useValue: store },
                ],
            },
        });
        await TestBed.configureTestingModule({
            declarations: [FormFindIDComponent],
            imports: [FormsModule],
        }).compileComponents();

        fixture = TestBed.createComponent(FormFindIDComponent);
        component = fixture.componentInstance;
    });


    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call store.getAlias each time the submit button is pushed', () => {
        const spyGetAlias = jest.spyOn(store, 'getAliases');
        const f = <NgForm>{ value: { alias: 'hello' } };

        component.submit(f);
        expect(spyGetAlias).toHaveBeenCalled();
    });
});
