/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { QueryMakerService } from './query-maker.service';
import { SearchService } from 'src/app/instance/store/services/search.service';
import { GetterAliases } from './aliases.store';
import { Attribute } from 'src/app/metamodel/models/attribute.model';
import { AttributeService } from 'src/app/metamodel/services/attribute.service';
import { of } from 'rxjs';
import { cold } from 'jasmine-marbles';

jest.mock('src/app/instance/store/services/search.service');
jest.mock('src/app/metamodel/services/attribute.service');

describe('QueryMakerService', () => {
    let query: QueryMakerService;

    let getter: GetterAliases = {
        alias: 'coucou',
        attributesToSearch: [1, 2, 3],
        datasetName: 'dataset',
    };
    let attr: Attribute[] = <Attribute[]>[
        { type: 'text' },
        { type: 'text' },
        { type: 'int32' },
    ];

    const search = {
        retrieveData: jest.fn((query: string) => {
            return of([]);
        }),
    };

    const dataset = {
        retrieveAttributeList: jest.fn((name: string) => {
            return of(attr);
        }),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                QueryMakerService,
                { provide: SearchService, useValue: search },
                { provide: AttributeService, useValue: dataset },
            ],
        });

        query = TestBed.inject(QueryMakerService);
    });

    it('should be created', () => {
        expect(query).toBeTruthy();
    });

    it('should distinguish the type of user-input alias properly', () => {
        let isString: boolean = (query as any).inputIsString('coucou');
        expect(isString).toBe(true);

        isString = (query as any).inputIsString(123);
        expect(isString).toBe(false);

        isString = (query as any).inputIsString(getter.alias);
        expect(isString).toBe(true);
    });

    it('should return an Observable<Object[]> case#1', () => {
        const inputSpy = jest.spyOn(query as any, 'inputIsString');
        const attrSpy = jest.spyOn(dataset, 'retrieveAttributeList');
        const dataSpy = jest.spyOn(search, 'retrieveData');
        getter.alias = '2';

        const res = query.aliasesRetriever(getter);
        expect(inputSpy).toHaveBeenCalled();
        expect(dataSpy).toHaveBeenCalledTimes(3);
        expect(attrSpy).not.toHaveBeenCalled();
        expect(res).toBeObservable(cold('(aaa|)', { a: [] }));
    });

    it('should return an Observable<Object[]> case#2', () => {
        const inputSpy = jest.spyOn(query as any, 'inputIsString');
        const attrSpy = jest.spyOn(dataset, 'retrieveAttributeList');

        getter.alias = "coucou";
        const res = query.aliasesRetriever(getter);
        expect(inputSpy).toHaveBeenCalled();
        expect(attrSpy).toHaveBeenCalled();
        expect(res).toBeObservable(cold('(aa|)', { a: [] }));
    });
});
