import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { Observable, concatMap, map } from 'rxjs';
import { QueryMakerService } from './query-maker.service';

/**
 * Interface representing the internal state in the component store
 *
 * @interface
 */
export interface AliasesState {
    aliases: string[];
    areAliasesLoaded: boolean;
}

/**
 * Interface to pass all necessary information to retrieve an alias in a database
 *
 * @interface
 */
export interface GetterAliases {
    alias: string;
    attributesToSearch: number[];
    datasetName: string;
}

@Injectable()
export class AliasesStore extends ComponentStore<AliasesState> {
    constructor(private query: QueryMakerService) {
        super();
    }

    aliases$: Observable<string[]> = this.select((state) => state.aliases);
    areAliasesLoaded$: Observable<boolean> = this.select(
        (state) => state.areAliasesLoaded
    );

    readonly getAliases = this.effect((trigger$: Observable<GetterAliases>) => {
        return trigger$.pipe(
            concatMap((id) =>
                this.query.aliasesRetriever(id).pipe(
                    map((result: Object[]) => {
                        let aliases: string[] = [];
                        if (result.length) {
                            for (const prop in result[0]) {
                                result[0][prop] != null
                                    ? aliases.push(result[0][prop])
                                    : 0;
                            }
                            this.setState((_) => ({
                                aliases: aliases,
                                areAliasesLoaded: true,
                            }));
                        }
                    })
                )
            )
        );
    });
}
