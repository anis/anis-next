/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Directive, Input } from '@angular/core';

/**
 * @class
 * @abstract
 * Implement an `abstract` factory for the simple dynamic one-liner form on the webpage
 */
@Directive({})
export abstract class AbstractFormSampleComponent<ArgType, ReturnType> {
    @Input() instanceName: string = null;
    @Input() faSymbol: string = null;
    @Input() type: string = null;
    @Input() label: string = null;
    @Input() textButton: string = null;
    @Input() datasetName: string = null;

    /**
     * @abstract
     * @method Should submit the form
     *
     * Implement a mehod with a template `ArgType` and `ReturnType` to enforce type verification
     */
    abstract submit(f: ArgType): ReturnType;
}
