import { HookParserEntry } from 'ngx-dynamic-hooks';

import { hookParsers } from './parsers';
import { dynamicComponents } from './dynamic-components';

export const componentParsers: Array<HookParserEntry> = [
    ...hookParsers,
    ...dynamicComponents.map(component => {
        return { component };
    })
];

export { hookParsers } from './parsers';
export { dynamicComponents } from './dynamic-components';
