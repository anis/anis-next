/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WebpageComponent } from './containers/webpage.component';
import { WebpageTitleResolver } from './webpage-title.resolver';

const routes: Routes = [
    { path: ':name', component: WebpageComponent, title: WebpageTitleResolver }
];

/**
 * @class
 * @classdesc Webpage routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WebpageRoutingModule { }

export const routedComponents = [
    WebpageComponent
];
