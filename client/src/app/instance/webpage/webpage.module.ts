/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { WebpageRoutingModule, routedComponents } from './webpage-routing.module';
import { dummiesComponents } from './components';
import { hookParsers, dynamicComponents } from './dynamic-content';

/**
 * @class
 * @classdesc Webpage module.
 * @description Concern everything related to webpage added by administrator of anis
 */
@NgModule({
    imports: [
        SharedModule,
        WebpageRoutingModule,
    ],
    declarations: [
        routedComponents,
        dummiesComponents,
        dynamicComponents,
    ],
    providers: [
        hookParsers
    ]
})
export class WebpageModule { }
