/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, Webpage } from 'src/app/metamodel/models';
import * as webpageSelector from 'src/app/metamodel/selectors/webpage.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

/**
 * @class
 * @classdesc Webpage component.
 */
@Component({
    selector: 'app-webpage',
    templateUrl: 'webpage.component.html'
})
export class WebpageComponent {
    public title: HTMLLinkElement = document.querySelector('#title');
    public instance: Observable<Instance>;
    public webpageListIsLoading: Observable<boolean>;
    public webpageListIsLoaded: Observable<boolean>;
    public webpage: Observable<Webpage>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.webpageListIsLoading = store.select(webpageSelector.selectWebpageListIsLoading);
        this.webpageListIsLoaded = store.select(webpageSelector.selectWebpageListIsLoaded);
        this.webpage = this.store.select(webpageSelector.selectWebpageByRouteName);
    }
}
