
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { WebpageComponent } from "./webpage.component";

describe('Instance][SearchMultiple][Containers] WebpageComponent', () => {
    let component: WebpageComponent;
    let fixture: ComponentFixture<WebpageComponent>;
    let store: MockStore;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageComponent
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(WebpageComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });

});