/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceNavbarComponent } from './instance-navbar.component';
import { InstanceFooterComponent } from './instance-footer.component';
import { WebpageFamilyNavComponent } from './webpage-family-nav.component';
import { WebpageFamilyNavMobileComponent } from './webpage-family-nav-mobile.component';

export const dummiesComponents = [
    InstanceNavbarComponent,
    InstanceFooterComponent,
    WebpageFamilyNavComponent,
    WebpageFamilyNavMobileComponent
];
