/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Instance, Logo } from 'src/app/metamodel/models';

@Component({
    selector: 'app-instance-footer',
    templateUrl: 'instance-footer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceFooterComponent {
    @Input() instance: Instance;
    @Input() apiUrl: string;

    public anisClientVersion: string = '3.7.0';
    public year: number = (new Date()).getFullYear();

    /**
     * Returns logo href.
     *
     * @return string
     */
    getLogoHref(logo: Logo): string {
        return `${this.apiUrl}/instance/${this.instance.name}/file-explorer${logo.file}`;
    }
}
