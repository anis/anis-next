/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { Instance, WebpageFamily, Webpage } from 'src/app/metamodel/models';
import { UserProfile } from 'src/app/auth/user-profile.model';
import { isAdmin } from 'src/app/shared/utils';

@Component({
    selector: 'app-instance-navbar',
    templateUrl: 'instance-navbar.component.html',
    styleUrls: [ 'instance-navbar.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceNavbarComponent {
    @Input() isAuthenticated: boolean;
    @Input() userProfile: UserProfile = null;
    @Input() userRoles: string[];
    @Input() authenticationEnabled: boolean;
    @Input() apiUrl: string;
    @Input() adminRoles: string[];
    @Input() instance: Instance;
    @Input() webpageFamilyList: WebpageFamily[];
    @Input() webpageList: Webpage[];
    @Input() firstWebpage: Webpage;
    @Output() login: EventEmitter<any> = new EventEmitter();
    @Output() logout: EventEmitter<any> = new EventEmitter();
    @Output() openEditProfile: EventEmitter<any> = new EventEmitter();

    /**
     * Returns true if user is admin
     * 
     * @returns boolean
     */
    isAdmin() {
        return isAdmin(this.adminRoles, this.userRoles);
    }

    /**
     * Returns logo URL.
     *
     * @return  string
     */
    getLogoURL(): string {
        if (this.instance.design_logo) {
            return `${this.apiUrl}/instance/${this.instance.name}/file-explorer${this.instance.design_logo}`;
        }
        return 'assets/cesam_anis40.png';
    }

    getLogoHref(): string {
        if (this.instance.design_logo_href) {
            return this.instance.design_logo_href;
        } else {
            return this.getInstanceBaseHref();
        }
    }

    getInstanceBaseHref() {
        if (this.firstWebpage) {
            return `/instance/${this.instance.name}/webpage/${this.firstWebpage.name}`;
        } else {
            return `/instance/${this.instance.name}`;
        }
    }

    getWebpageListByFamily(webpageFamily: WebpageFamily) {
        return this.webpageList.filter(webpage => webpage.id_webpage_family === webpageFamily.id);
    }
}
