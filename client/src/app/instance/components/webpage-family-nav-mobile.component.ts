/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { WebpageFamily, Webpage } from 'src/app/metamodel/models';

@Component({
    selector: 'app-webpage-family-nav-mobile',
    templateUrl: 'webpage-family-nav-mobile.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageFamilyNavMobileComponent {
    @Input() webpageFamily: WebpageFamily;
    @Input() webpageList: Webpage[];
}
