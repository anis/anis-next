/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Instance, Logo } from "src/app/metamodel/models";
import { InstanceFooterComponent } from "./instance-footer.component"

describe('[instance][components] InstanceFooterComponent', () => {
    let component: InstanceFooterComponent;
    let fixture: ComponentFixture<InstanceFooterComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceFooterComponent
            ],
        })
        fixture = TestBed.createComponent(InstanceFooterComponent);
        component = fixture.componentInstance;
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('sould return test/instance/instance_test_name/file-explorerlogo_test', () => {
        let logo: Logo = {file: 'logo_test', display: 10, href: '', title: ''};
        let instance: Instance;
        component.apiUrl ='test';
        component.instance = {...instance,name:'instance_test_name'};
        let result = component.getLogoHref(logo);
        expect(result).toEqual('test/instance/instance_test_name/file-explorerlogo_test');
    });
    it('sould return test/instance/instance_test_name/file-explorerlogo_test', () => {
        let logo: Logo = {file: 'logo_test', display: 10, href: '', title: ''};
        let instance: Instance;
        component.apiUrl ='test';
        component.instance = {...instance,name:'instance_test_name'};
        let result = component.getLogoHref(logo);
        expect(result).toEqual('test/instance/instance_test_name/file-explorerlogo_test');
    });
})