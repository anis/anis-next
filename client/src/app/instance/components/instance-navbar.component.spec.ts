/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import * as fromUtils from 'src/app/shared/utils';
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { InstanceNavbarComponent } from "./instance-navbar.component";
import { Instance, Webpage, WebpageFamily } from 'src/app/metamodel/models';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'authImage' })
export class MockAuthImagePipe implements PipeTransform {
    transform(value: any, ...args: any[]) {
    }
}

describe('[instance][components] InstanceNavbarComponent', () => {
    let component: InstanceNavbarComponent;
    let fixture: ComponentFixture<InstanceNavbarComponent>;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceNavbarComponent,
                MockAuthImagePipe
            ],
        })
        fixture = TestBed.createComponent(InstanceNavbarComponent);
        component = fixture.componentInstance;
        component.instance = { ...instance, design_logo_href: 'test' }
        component.userRoles = ['test'];
        component.adminRoles = ['test'];
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it("isAdmin() should call isAdmin(adminRoles: string[], userRoles: string[]) and return true", () => {
        let spy = jest.spyOn(fromUtils, 'isAdmin');
        let result = component.isAdmin();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(['test'], ['test'])
        expect(result).toEqual(true);
    })
    it("isAdmin() should call isAdmin(adminRoles: string[], userRoles: string[]) and return false", () => {
        component.userRoles = ['test1'];
        let spy = jest.spyOn(fromUtils, 'isAdmin');
        let result = component.isAdmin();
        expect(spy).toHaveBeenCalledWith(['test'], ['test1'])
        expect(result).toEqual(false);
    });
    it('getLogoURL() should return test/instance/test_instance_name/file-explorertest_design_logo', () => {
        component.instance = { ...instance, design_logo: 'test_design_logo', name: 'test_instance_name' };
        component.apiUrl = 'test';
        let result = component.getLogoURL();
        expect(result).toEqual('test/instance/test_instance_name/file-explorertest_design_logo');
    });
    it('getLogoURL() should return assets/cesam_anis40.png', () => {
        component.instance = { ...instance, design_logo: '', name: 'test_instance_name' };
        component.apiUrl = 'test';
        let result = component.getLogoURL();
        expect(result).toEqual('assets/cesam_anis40.png');
    });
    it('should return test', () => {
        let result = component.getLogoHref();
        expect(result).toEqual('test');
    });
    it('should call getInstanceBaseHref()', () => {
        let spy = jest.spyOn(component, 'getInstanceBaseHref');
        component.instance = { ...component.instance, design_logo_href: '' };
        component.getLogoHref();
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('getInstanceBaseHref() should return /instance/test/webpage/test', () => {
        let firstWebPage: Webpage;
        component.firstWebpage = { ...firstWebPage, name: 'test' };
        component.instance = { ...component.instance, name: 'test' };
        let result = component.getInstanceBaseHref();
        expect(result).toEqual('/instance/test/webpage/test');
    });
    it('getInstanceBaseHref() should return /instance/test', () => {
        component.firstWebpage = undefined;
        component.instance = { ...component.instance, name: 'test' };
        let result = component.getInstanceBaseHref();
        expect(result).toEqual('/instance/test');
    });
    it('getWebpageListByFamily(webpageFamily: WebpageFamily) an array with one element', () => {
        let webpage: Webpage;
        let webpageFamily: WebpageFamily;
        component.webpageList = [{ ...webpage, id_webpage_family: 1 }, { ...webpage, id_webpage_family: 2 }];
        let result = component.getWebpageListByFamily({ ...webpageFamily, id: 1 })
        expect(result.length).toEqual(1);
    });
})