/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from 'src/app/shared/shared.module';
import { InstanceRoutingModule, routedComponents } from './instance-routing.module';
import { dummiesComponents } from './components';
import { instanceReducer } from './instance.reducer';
import { instanceEffects } from './store/effects';
import { instanceServices } from './store/services';
import { InstanceStyleService } from './instance-style.service';

/**
 * @class
 * @classdesc Instance module.
 */
@NgModule({
    imports: [
        SharedModule,
        InstanceRoutingModule,
        StoreModule.forFeature('instance', instanceReducer),
        EffectsModule.forFeature(instanceEffects)
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ],
    providers: [
        instanceServices,
        InstanceStyleService
    ]
})
export class InstanceModule { }
