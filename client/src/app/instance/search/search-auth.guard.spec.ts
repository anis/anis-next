/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';
import { AppConfigService } from 'src/app/app-config.service';
import { SearchAuthGuard } from './search-auth.guard';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import * as authSelector from 'src/app/auth/auth.selector';
import { Dataset } from 'src/app/metamodel/models';

describe('[instance][search] SearchAuthGuard', () => {
    let store: MockStore;
    let mockDatasetSelectorSelectDatasetListIsLoaded;
    let mockDatasetGroupSelectorSelectDatasetGroupListIsLoaded;
    let mockDatasetSelectorSelectDatasetByRouteName;
    let mockAuthSelectorSelectUserRoles;
    let mockAuthSelectorSelectIsAuthenticated;
    let mockDatasetGroupSelectorSelectAllDatasetGroups;
    let dataset: Dataset;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockStore({}),],
        })
    });
    it('should create component', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true } })
        store = TestBed.inject(MockStore);
        let searchAuthGuard = TestBed.inject(SearchAuthGuard);
        expect(searchAuthGuard).toBeTruthy();
    });
    it('should  return [] when datasetListIsLoaded and datasetGroupListIsLoaded are false', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: {
                authenticationEnabled: true,
                adminRoles: ['test'],

            }
        })
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetListIsLoaded = store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, false);
        mockDatasetGroupSelectorSelectDatasetGroupListIsLoaded = store.overrideSelector(datasetGroupSelector.selectDatasetGroupListIsLoaded, false);
        let searchAuthGuard = TestBed.inject(SearchAuthGuard);
        let result = cold('a', { a: searchAuthGuard.canActivate() });
        let expected = cold('a', { a: [] });
        expect(result).toBeObservable(expected);

    })
    it('should  return true when authenticationEnabled is true ', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: false } })
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetListIsLoaded = store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        mockDatasetGroupSelectorSelectDatasetGroupListIsLoaded = store.overrideSelector(datasetGroupSelector.selectDatasetGroupListIsLoaded, true);
        mockDatasetSelectorSelectDatasetByRouteName = store.overrideSelector(datasetSelector.selectDatasetByRouteName, { ...dataset, name: 'test' });
        mockAuthSelectorSelectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        mockAuthSelectorSelectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockDatasetGroupSelectorSelectAllDatasetGroups = store.overrideSelector(datasetGroupSelector.selectAllDatasetGroups, [
            {
                datasets: [],
                id: 1,
                instance_name: 'test',
                role: 'test'

            }]);
        let searchAuthGuard = TestBed.inject(SearchAuthGuard);
        let result = searchAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);

    });
    it('should  return false when user is authenticated and accessible is false', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: {
                authenticationEnabled: true,
                adminRoles: [],

            }
        })
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetListIsLoaded = store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        mockDatasetGroupSelectorSelectDatasetGroupListIsLoaded = store.overrideSelector(datasetGroupSelector.selectDatasetGroupListIsLoaded, true);
        mockDatasetSelectorSelectDatasetByRouteName = store.overrideSelector(datasetSelector.selectDatasetByRouteName, { ...dataset, name: 'test' });
        mockAuthSelectorSelectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        mockAuthSelectorSelectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockDatasetGroupSelectorSelectAllDatasetGroups = store.overrideSelector(datasetGroupSelector.selectAllDatasetGroups, [
            {
                datasets: [],
                id: 1,
                instance_name: 'test',
                role: 'test'

            }]);
        let searchAuthGuard = TestBed.inject(SearchAuthGuard);
        let result = searchAuthGuard.canActivate();
        let expected = cold('a', { a: false });
        expect(result).toBeObservable(expected);
    });
    it('should  return true when user is authenticated and accessible is true', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: {
                authenticationEnabled: true,
                adminRoles: ['test'],

            }
        })
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetListIsLoaded = store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        mockDatasetGroupSelectorSelectDatasetGroupListIsLoaded = store.overrideSelector(datasetGroupSelector.selectDatasetGroupListIsLoaded, true);
        mockDatasetSelectorSelectDatasetByRouteName = store.overrideSelector(datasetSelector.selectDatasetByRouteName, { ...dataset, name: 'dataset_name' });
        mockAuthSelectorSelectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        mockAuthSelectorSelectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockDatasetGroupSelectorSelectAllDatasetGroups = store.overrideSelector(datasetGroupSelector.selectAllDatasetGroups, [
            {

                datasets: ['dataset_name', 'test2'],
                id: 1,
                instance_name: 'test',
                role: 'test'

            }]);
        let searchAuthGuard = TestBed.inject(SearchAuthGuard);
        let result = searchAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);

    });

})

