/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component} from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AbstractSearchComponent } from './abstract-search.component';
import { DatasetFamily, DatasetGroup } from 'src/app/metamodel/models';
import * as searchActions from '../../store/actions/search.actions';
import * as authSelector from 'src/app/auth/auth.selector';
import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Search dataset container.
 */
@Component({
    selector: 'app-dataset',
    templateUrl: 'dataset.component.html'
})
export class DatasetComponent extends AbstractSearchComponent {
    public isAuthenticated: Observable<boolean>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public userRoles: Observable<string[]>;
    public datasetGroupList: Observable<DatasetGroup[]>;
    public datasetGroupListIsLoading: Observable<boolean>;
    public datasetGroupListIsLoaded: Observable<boolean>;

    constructor(protected store: Store<{ }>, private config: AppConfigService) {
        super(store);
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.userRoles = store.select(authSelector.selectUserRoles);
        this.datasetGroupList = store.select(datasetGroupSelector.selectAllDatasetGroups);
        this.datasetGroupListIsLoading = store.select(datasetGroupSelector.selectDatasetGroupListIsLoading);
        this.datasetGroupListIsLoaded = store.select(datasetGroupSelector.selectDatasetGroupListIsLoaded);
    }

    ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.changeStep({ step: 'dataset' })));
        super.ngOnInit();
    }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }

    /**
     * Returns admin roles list
     * 
     * @returns string[]
     */
    getAdminRoles(): string[] {
        return this.config.adminRoles;
    }
}
