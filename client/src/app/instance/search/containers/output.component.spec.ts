/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { OutputComponent } from './output.component';
import { Attribute, CriteriaFamily, Dataset, OutputCategory, OutputFamily } from '../../../metamodel/models';
import { ConeSearch, Criterion, SearchQueryParams } from '../../store/models';
import { SortByOutputDisplayPipe } from '../pipes/sort-by-output-display.pipe';
import * as searchActions from '../../store/actions/search.actions';
import { AbstractSearchComponent } from './abstract-search.component';

describe('[Instance][Search][Container] OutputComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-output-tabs', template: '' })
    class OutputTabsStubComponent {
        @Input() outputFamilyList: OutputFamily[];
        @Input() outputCategoryList: OutputCategory[];
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() designColor: string;
    }

    @Component({ selector: 'app-summary', template: '' })
    class SummaryStubComponent {
        @Input() currentStep: string;
        @Input() datasetSelected: string;
        @Input() datasetList: Dataset[];
        @Input() attributeList: Attribute[];
        @Input() criteriaFamilyList: CriteriaFamily[];
        @Input() outputFamilyList: OutputFamily[];
        @Input() outputCategoryList: OutputCategory[];
        @Input() criteriaList: Criterion[];
        @Input() outputList: number[];
        @Input() queryParams: SearchQueryParams;
        @Input() coneSearch: ConeSearch;
    }

    let component: OutputComponent;
    let fixture: ComponentFixture<OutputComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                OutputComponent,
                SpinnerStubComponent,
                OutputTabsStubComponent,
                SummaryStubComponent,
                SortByOutputDisplayPipe
            ],
            providers: [provideMockStore({ })]
        });
        fixture = TestBed.createComponent(OutputComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const spy = jest.spyOn(store, 'dispatch');
        jest.spyOn(AbstractSearchComponent.prototype, 'ngOnInit').mockReturnThis();
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(2);
            expect(spy).toHaveBeenCalledWith(searchActions.changeStep({ step: 'output' }));
            expect(spy).toHaveBeenCalledWith(searchActions.checkOutput());
            done();
        });
    });

    it('#updateOutputList() should dispatch updateOutputList action', () => {
        const outputList: number[] = [1, 2];
        const spy = jest.spyOn(store, 'dispatch');
        component.updateOutputList(outputList);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.updateOutputList({ outputList }));
    });
});
