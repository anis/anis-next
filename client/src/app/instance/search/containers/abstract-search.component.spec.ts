/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { AbstractSearchComponent } from './abstract-search.component';
import * as searchActions from '../../store/actions/search.actions';

describe('[Instance][Search][Container] AbstractSearchComponent', () => {
    @Component({
        selector: 'app-fake',
        template: ''
    })
    class MyFakeComponent extends AbstractSearchComponent {
        ngOnInit() {
            super.ngOnInit();
        }
    }

    let component: MyFakeComponent;
    let fixture: ComponentFixture<MyFakeComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [MyFakeComponent],
            providers: [provideMockStore({ })]
        });
        fixture = TestBed.createComponent(MyFakeComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.attributeListIsLoaded = of(true);
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(searchActions.initSearch());
            done();
        });
    });
});
