/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { ResultComponent } from './result.component';
import { Attribute, CriteriaFamily, Dataset, Instance, OutputCategory, OutputFamily } from '../../../metamodel/models';
import { ConeSearch, Criterion, Pagination, PaginationOrder, SearchQueryParams } from '../../store/models';
import { SortByCriteriaDisplayPipe } from '../pipes/sort-by-criteria-display.pipe';
import { SortByOutputDisplayPipe } from '../pipes/sort-by-output-display.pipe';
import { DatasetByNamePipe } from '../../../shared/pipes/dataset-by-name.pipe';
import * as sampActions from 'src/app/samp/samp.actions';
import * as searchActions from '../../store/actions/search.actions';
import * as imageActions from 'src/app/metamodel/actions/image.actions';
import { AbstractSearchComponent } from './abstract-search.component';
import * as archiveActions from '../../store/actions/archive.actions';

describe('[Instance][Search][Container] ResultComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-download', template: '' })
    class DownloadStubComponent {
        @Input() datasetSelected: string;
        @Input() datasetList: Dataset[];
        @Input() criteriaList: Criterion[];
        @Input() outputList: number[];
        @Input() coneSearch: ConeSearch;
        @Input() dataLength: number;
        @Input() sampRegistered: boolean;
    }

    @Component({ selector: 'app-reminder', template: '' })
    class ReminderStubComponent {
        @Input() datasetSelected: string;
        @Input() datasetList: Dataset[];
        @Input() attributeList: Attribute[];
        @Input() criteriaFamilyList: CriteriaFamily[];
        @Input() outputFamilyList: OutputFamily[];
        @Input() outputCategoryList: OutputCategory[];
        @Input() criteriaList: Criterion[];
        @Input() coneSearch: ConeSearch;
        @Input() outputList: number[];
    }

    @Component({ selector: 'app-samp', template: '' })
    class SampStubComponent {
        @Input() datasetSelected: string;
        @Input() datasetList: Dataset[];
        @Input() sampRegistered: boolean;
    }

    @Component({ selector: 'app-url-display', template: '' })
    class UrlDisplayStubComponent {
        @Input() datasetSelected: string;
        @Input() datasetList: Dataset[];
        @Input() criteriaList: Criterion[];
        @Input() outputList: number[];
        @Input() coneSearch: ConeSearch;
    }

    @Component({ selector: 'app-datatable-tab', template: '' })
    class DatatableTabStubComponent {
        @Input() datasetSelected: string;
        @Input() instance: Instance;
        @Input() datasetList: Dataset[];
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() criteriaList: Criterion[];
        @Input() coneSearch: ConeSearch;
        @Input() queryParams: SearchQueryParams;
        @Input() dataLength: number;
        @Input() sampRegistered: boolean;
        @Input() data: any[];
        @Input() dataIsLoading: boolean;
        @Input() dataIsLoaded: boolean;
        @Input() selectedData: any[];
    }

    let component: ResultComponent;
    let fixture: ComponentFixture<ResultComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                ResultComponent,
                SpinnerStubComponent,
                DownloadStubComponent,
                ReminderStubComponent,
                SampStubComponent,
                UrlDisplayStubComponent,
                DatatableTabStubComponent,
                SortByCriteriaDisplayPipe,
                SortByOutputDisplayPipe,
                DatasetByNamePipe
            ],
            providers: [provideMockStore({})]
        });
        fixture = TestBed.createComponent(ResultComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.pristine = of(false);
        const spy = jest.spyOn(store, 'dispatch');
        jest.spyOn(AbstractSearchComponent.prototype, 'ngOnInit').mockReturnThis();
        component.ngOnInit();
        Promise.resolve(null).then(function () {
            expect(spy).toHaveBeenCalledTimes(4);
            expect(spy).toHaveBeenCalledWith(searchActions.changeStep({ step: 'result' }));
            expect(spy).toHaveBeenCalledWith(searchActions.checkResult());
            expect(spy).toHaveBeenCalledWith(imageActions.loadImageList());
            expect(spy).toHaveBeenCalledWith(searchActions.retrieveDataLength());
            done();
        });
    });

    it('#sampRegister() should dispatch SAMP register action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.sampRegister();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(sampActions.register());
    });

    it('#sampUnregister() should dispatch SAMP unregister action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.sampUnregister();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(sampActions.unregister());
    });

    it('#broadcastVotable() should dispatch SAMP broadcastVotable action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.broadcastVotable('http://test.com');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(sampActions.broadcastVotable({ url: 'http://test.com' }));
    });

    it('#retrieveData() should dispatch retrieveData action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const pagination: Pagination = { dname: 'myDataset', page: 1, nbItems: 10, sortedCol: 1, order: PaginationOrder.a };
        component.retrieveData(pagination);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.retrieveData({ pagination }));
    });

    it('#addSearchData() should dispatch retrieveData action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.addSearchData(1);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.addSelectedData({ id: 1 }));
    });

    it('#deleteSearchData() should dispatch retrieveData action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.deleteSearchData(1);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.deleteSelectedData({ id: 1 }));
    });

    it('#downloadFile() should  dispach download file action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.downloadFile({ url: 'test.fr', filename: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.downloadFile({ url: 'test.fr', filename: 'test' }));
    });
    it('#startTaskCreateArchive() should  dispatch startTaskCreateArchive action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let query = 'test';
        component.startTaskCreateArchive(query);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(archiveActions.startTaskCreateArchive({ query }));
    });
    it('#updateOutputList() should  dispatch updateOutputList action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let outputList = []
        component.updateOutputList(outputList);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.updateOutputList({ outputList }));
    });
    it('#updateBackgroundHref() should  set backgroundHref to test', () => {
        expect(component.backgroundHref).not.toEqual('test');
        component.updateBackgroundHref('test');
        expect(component.backgroundHref).toEqual('test')
    });
    it('#updateOpenPlotImage() should  set openPlotImage to true', () => {
        expect(component.openPlotImage).not.toEqual(true);
        component.updateOpenPlotImage(true);
        expect(component.openPlotImage).toEqual(true)
    });
    it('#updateSelectId() should  set selectId to 5', () => {
        expect(component.selectId).not.toEqual(5);
        component.updateSelectId(5);
        expect(component.selectId).toEqual(5)
    });

    it('#ngOnDestroy() should dispatch destroyResults action and unsubscribe from pristineSubscription', () => {
        component.pristineSubscription = of().subscribe();
        const unsubscribeSpy = jest.spyOn(component.pristineSubscription, 'unsubscribe');
        const dispatchSpy = jest.spyOn(store, 'dispatch');
        component.ngOnDestroy();
        expect(unsubscribeSpy).toHaveBeenCalledTimes(1);
        expect(dispatchSpy).toHaveBeenCalledTimes(2);
        expect(dispatchSpy).toHaveBeenCalledWith(searchActions.destroyResults());
    });
});
