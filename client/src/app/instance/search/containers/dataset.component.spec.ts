/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { DatasetComponent } from './dataset.component';
import {
    Attribute,
    CriteriaFamily,
    Dataset,
    DatasetFamily,
    OutputCategory,
    OutputFamily
} from 'src/app/metamodel/models';
import { ConeSearch, Criterion, SearchQueryParams } from 'src/app/instance/store/models';
import { SortByOutputDisplayPipe } from 'src/app/instance/search/pipes/sort-by-output-display.pipe';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import { AbstractSearchComponent } from './abstract-search.component';
import { AppConfigService } from 'src/app/app-config.service';

describe('[Instance][Search][Container] DatasetComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-dataset-tabs', template: '' })
    class DatasetTabsStubComponent {
        @Input() datasetList: Dataset[];
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() instanceSelected: string;
        @Input() datasetSelected: string;
    }

    @Component({ selector: 'app-summary', template: '' })
    class SummaryStubComponent {
        @Input() currentStep: string;
        @Input() datasetSelected: string;
        @Input() datasetList: Dataset[];
        @Input() attributeList: Attribute[];
        @Input() criteriaFamilyList: CriteriaFamily[];
        @Input() outputFamilyList: OutputFamily[];
        @Input() outputCategoryList: OutputCategory[];
        @Input() criteriaList: Criterion[];
        @Input() outputList: number[];
        @Input() queryParams: SearchQueryParams;
        @Input() coneSearch: ConeSearch;
    }

    let component: DatasetComponent;
    let fixture: ComponentFixture<DatasetComponent>;
    let store: MockStore;
    let appConfigServiceStub = new AppConfigService();
    appConfigServiceStub.authenticationEnabled = true;
    appConfigServiceStub.adminRoles = ['test'];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                DatasetComponent,
                SpinnerStubComponent,
                DatasetTabsStubComponent,
                SummaryStubComponent,
                SortByOutputDisplayPipe
            ],
            providers: [
                provideMockStore({}),
                { provide: AppConfigService, useValue: appConfigServiceStub }
            ]
        });
        fixture = TestBed.createComponent(DatasetComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.datasetSelected = of('myDataset');
        const spy = jest.spyOn(store, 'dispatch');
        jest.spyOn(AbstractSearchComponent.prototype, 'ngOnInit').mockReturnThis();
        component.ngOnInit();
        Promise.resolve(null).then(function () {
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(searchActions.changeStep({ step: 'dataset' }));
            done();
        });
    });
    it('getAuthenticationEnabled should return true', () => {

        expect(component.getAuthenticationEnabled()).toBe(true);
    })
    it('getAdminRoles() an array with one element', () => {
        expect(component.getAdminRoles().length).toEqual(1);;
    })
});
