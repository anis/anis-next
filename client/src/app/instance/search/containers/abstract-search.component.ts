/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Directive, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { ConeSearch, Criterion, SearchQueryParams } from '../../store/models';
import { Instance, Dataset, Attribute, CriteriaFamily, OutputFamily, OutputCategory } from 'src/app/metamodel/models';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as attributeSelector from 'src/app/metamodel/selectors/attribute.selector';
import * as criteriaFamilySelector from 'src/app/metamodel/selectors/criteria-family.selector';
import * as outputFamilySelector from 'src/app/metamodel/selectors/output-family.selector';
import * as outputCategorySelector from 'src/app/metamodel/selectors/output-category.selector';
import * as searchActions from '../../store/actions/search.actions';
import * as searchSelector from '../../store/selectors/search.selector';
import * as coneSearchSelector from '../../store/selectors/cone-search.selector';

/**
 * @abstract
 * @class
 * @classdesc Abstract search container.
 *
 * @implements OnInit
 * @implements OnDestroy
 */
@Directive()
export abstract class AbstractSearchComponent implements OnInit {
    public datasetSelected: Observable<string>;
    public instanceSelected: Observable<string>;
    public instance: Observable<Instance>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;
    public attributeList: Observable<Attribute[]>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public criteriaFamilyList: Observable<CriteriaFamily[]>;
    public criteriaFamilyListIsLoading: Observable<boolean>;
    public criteriaFamilyListIsLoaded: Observable<boolean>;
    public outputFamilyList: Observable<OutputFamily[]>;
    public outputFamilyListIsLoading: Observable<boolean>;
    public outputFamilyListIsLoaded: Observable<boolean>;
    public outputCategoryList: Observable<OutputCategory[]>;
    public outputCategoryListIsLoading: Observable<boolean>;
    public outputCategoryListIsLoaded: Observable<boolean>;
    public pristine: Observable<boolean>;
    public currentStep: Observable<string>;
    public criteriaList: Observable<Criterion[]>;
    public outputList: Observable<number[]>;
    public queryParams: Observable<SearchQueryParams>;
    public coneSearch: Observable<ConeSearch>;

    constructor(protected store: Store<{ }>) {
        this.datasetSelected = store.select(datasetSelector.selectDatasetNameByRoute);
        this.instanceSelected = store.select(instanceSelector.selectInstanceNameByRoute);
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
        this.datasetList = store.select(datasetSelector.selectAllDatasets);
        this.attributeList = store.select(attributeSelector.selectAllAttributes);
        this.attributeListIsLoading = store.select(attributeSelector.selectAttributeListIsLoading);
        this.attributeListIsLoaded = store.select(attributeSelector.selectAttributeListIsLoaded);
        this.criteriaFamilyList = store.select(criteriaFamilySelector.selectAllCriteriaFamilies);
        this.criteriaFamilyListIsLoading = store.select(criteriaFamilySelector.selectCriteriaFamilyListIsLoading);
        this.criteriaFamilyListIsLoaded = store.select(criteriaFamilySelector.selectCriteriaFamilyListIsLoaded);
        this.outputFamilyList = store.select(outputFamilySelector.selectAllOutputFamilies);
        this.outputFamilyListIsLoading = store.select(outputFamilySelector.selectOutputFamilyListIsLoading);
        this.outputFamilyListIsLoaded = store.select(outputFamilySelector.selectOutputFamilyListIsLoaded);
        this.outputCategoryList = store.select(outputCategorySelector.selectAllOutputCategories);
        this.outputCategoryListIsLoading = store.select(outputCategorySelector.selectOutputCategoryListIsLoading);
        this.outputCategoryListIsLoaded = store.select(outputCategorySelector.selectOutputCategoryListIsLoaded);
        this.pristine = this.store.select(searchSelector.selectPristine);
        this.currentStep = this.store.select(searchSelector.selectCurrentStep);
        this.criteriaList = this.store.select(searchSelector.selectCriteriaList);
        this.outputList = this.store.select(searchSelector.selectOutputList);
        this.queryParams = this.store.select(searchSelector.selectQueryParams);
        this.coneSearch = this.store.select(coneSearchSelector.selectConeSearch);
    }

    ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.initSearch()));
    }
}
