/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AbstractSearchComponent } from './abstract-search.component';
import { ConeSearchConfig, Dataset, Instance } from 'src/app/metamodel/models';
import { ConeSearch, Criterion } from '../../store/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as coneSearchConfigSelector from 'src/app/metamodel/selectors/cone-search-config.selector';
import * as searchActions from '../../store/actions/search.actions';
import * as coneSearchActions from '../../store/actions/cone-search.actions';
import * as coneSearchSelector from '../../store/selectors/cone-search.selector';

/**
 * @class
 * @classdesc Search criteria container.
 */
@Component({
    selector: 'app-criteria',
    templateUrl: 'criteria.component.html'
})
export class CriteriaComponent extends AbstractSearchComponent {
    public dataset: Observable<Dataset>;
    public resolverIsLoading: Observable<boolean>;
    public resolverIsLoaded: Observable<boolean>;
    public coneSearchConfig: Observable<ConeSearchConfig>;
    public coneSearchConfigIsLoading: Observable<boolean>;
    public coneSearchConfigIsLoaded: Observable<boolean>;

    constructor(protected store: Store<{ }>) {
        super(store);
        this.dataset = store.select(datasetSelector.selectDatasetByRouteName);
        this.resolverIsLoading = this.store.select(coneSearchSelector.selectResolverIsLoading);
        this.resolverIsLoaded = this.store.select(coneSearchSelector.selectResolverIsLoaded);
        this.coneSearchConfig = store.select(coneSearchConfigSelector.selectConeSearchConfig);
        this.coneSearchConfigIsLoading = store.select(coneSearchConfigSelector.selectConeSearchConfigIsLoading);
        this.coneSearchConfigIsLoaded = store.select(coneSearchConfigSelector.selectConeSearchConfigIsLoaded);
    }

    ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.changeStep({ step: 'criteria' })));
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.checkCriteria()));
        super.ngOnInit();
    }

    /**
     * Dispatches action to add the given criterion to the search.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    addCriterion(criterion: Criterion): void {
        this.store.dispatch(searchActions.addCriterion({ criterion }));
    }

    /**
     * Dispatches action to update the given criterion to the search.
     *
     * @param  {Criterion} updatedCriterion - The updated criterion.
     */
    updateCriterion(updatedCriterion: Criterion): void {
        this.store.dispatch(searchActions.updateCriterion({ updatedCriterion }))
    }

    /**
     * Dispatches action to remove the given criterion ID to the search.
     *
     * @param  {number} idCriterion - The criterion ID.
     */
    deleteCriterion(idCriterion: number): void {
        this.store.dispatch(searchActions.deleteCriterion({ idCriterion }));
    }

    /**
     * Dispatches action to add cone search.
     *
     * @param  {ConeSearch} coneSearch - The cone search.
     */
    addConeSearch(coneSearch: ConeSearch): void {
        this.store.dispatch(coneSearchActions.addConeSearch({ coneSearch }));
    }

    /**
     * Dispatches action to update cone search.
     *
     * @param  {ConeSearch} coneSearch - The cone search.
     */
    updateConeSearch(coneSearch: ConeSearch): void {
        this.store.dispatch(coneSearchActions.updateConeSearch({ coneSearch }));
    }

    /**
     * Dispatches action to remove the cone search.
     */
    deleteConeSearch(): void {
        this.store.dispatch(coneSearchActions.deleteConeSearch());
    }

    /**
     * Dispatches action to retrieve object coordinates.
     *
     * @param  {string} name - The object name.
     */
    retrieveCoordinates(name: string): void {
        this.store.dispatch(coneSearchActions.retrieveCoordinates({ name }));
    }
}
