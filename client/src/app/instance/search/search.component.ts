/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { SearchQueryParams } from '../store/models';
import { Instance } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as searchSelector from '../store/selectors/search.selector';

/**
 * @class
 * @classdesc Search container.
 */
@Component({
    selector: 'app-search',
    templateUrl: 'search.component.html'
})
export class SearchComponent {
    public instance: Observable<Instance>;
    public datasetSelected: Observable<string>;
    public currentStep: Observable<string>;
    public criteriaStepChecked: Observable<boolean>;
    public outputStepChecked: Observable<boolean>;
    public resultStepChecked: Observable<boolean>;
    public queryParams: Observable<SearchQueryParams>;
    public outputList: Observable<number[]>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.datasetSelected = store.select(datasetSelector.selectDatasetNameByRoute);
        this.currentStep = store.select(searchSelector.selectCurrentStep);
        this.criteriaStepChecked = store.select(searchSelector.selectCriteriaStepChecked);
        this.outputStepChecked = store.select(searchSelector.selectOutputStepChecked);
        this.resultStepChecked = store.select(searchSelector.selectResultStepChecked);
        this.queryParams = this.store.select(searchSelector.selectQueryParams);
        this.outputList = this.store.select(searchSelector.selectOutputList);
    }
}
