/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SortByCriteriaDisplayPipe } from './sort-by-criteria-display.pipe';
import { SortByOutputDisplayPipe } from './sort-by-output-display.pipe';
import { SortByDetailDisplay } from './sort-by-detail-display';

export const searchPipes = [
    SortByCriteriaDisplayPipe,
    SortByOutputDisplayPipe,
    SortByDetailDisplay
];