/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SortByOutputDisplayPipe } from './sort-by-output-display.pipe';
import { Attribute } from '../../../metamodel/models';

describe('[Instance][Search][Pipe] SortByOutputDisplayPipe', () => {
    let pipe = new SortByOutputDisplayPipe();

    it('orders attributeList by output display', () => {
        const attributeList: Attribute[] = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type : 'field',
                operator : '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: null,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: null,
                id_detail_output_category: null
            }
        ]
        expect(pipe.transform(attributeList)[0].id).toBe(1);
        expect(pipe.transform(attributeList)[1].id).toBe(2);
    });
});
