/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc SortByOutputDisplay pipe.
 */
@Pipe({ name: 'sortByOutputDisplay' })
export class SortByOutputDisplayPipe implements PipeTransform {
    transform(attributeList: Attribute[]): Attribute[] {
        return [...attributeList].sort((a: Attribute, b: Attribute) => a.output_display - b.output_display);
    }
}
