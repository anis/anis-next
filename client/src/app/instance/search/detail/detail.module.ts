/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { DetailRoutingModule, routedComponents } from './detail-routing.module';
import { dummiesComponents } from './components';
import { hookParsers, dynamicComponents } from './dynamic-content';
import { SharedRendererModule } from '../shared-renderer/shared-renderer.module';

/**
 * @class
 * @classdesc Search module.
 */
@NgModule({
    imports: [
        SharedModule,
        DetailRoutingModule,
        SharedRendererModule,
    ],
    declarations: [
        routedComponents,
        dummiesComponents,
        dynamicComponents
    ],
    providers: [
        hookParsers
    ]
})
export class DetailModule { }
