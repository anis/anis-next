/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailComponent } from './containers/detail.component';
import { DetailTitleResolver } from './detail-title.resolver';

const routes: Routes = [
    { path: '', redirectTo: ':dname/:id', pathMatch: 'full' },
    { path: ':dname/:id', component: DetailComponent, title: DetailTitleResolver }
];

/**
 * @class
 * @classdesc Search routing module.
 */
 @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DetailRoutingModule { }

export const routedComponents = [
    DetailComponent
];
