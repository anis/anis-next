/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

@Injectable({
    providedIn: 'root'
})
export class DetailTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        const id = route.params.id;
        return combineLatest([
            this.store.pipe(select(instanceSelector.selectInstanceByRouteName)),
            this.store.pipe(select(datasetSelector.selectDatasetByRouteName))
        ]).pipe(
            map(([instance, dataset]) => `${instance.label} - Search ${dataset.label} - ${id}`)
        );
    }
}
