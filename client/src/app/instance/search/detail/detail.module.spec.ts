/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DetailModule } from './detail.module';

describe('[Instance][Search][detail] module', () => {
    it('Test detail module', () => {
        expect(DetailModule.name).toEqual('DetailModule');
    });
});

