/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-display-json',
    templateUrl: 'display-json.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayJsonComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() attributeJsonId: number;

    getValue() {
        const labelAttributeJson = this.attributeList.find(attribute => attribute.id === this.attributeJsonId).label;
        return this.object[labelAttributeJson];
    }
}
