/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Display spectra component.
 */
@Component({
    selector: 'app-display-spectra',
    templateUrl: 'display-spectra.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplaySpectraComponent {
    @Input() object: any;
    @Input() datasetName: string;
    @Input() attributeList: Attribute[];
    @Input() attributeSpectraId: number;
    @Input() attributeZId: number;

    spectraCSV: Observable<string>;

    constructor(private http: HttpClient, private config: AppConfigService) { }

    ngOnInit() {
        const spectraFile = this.object[this.attributeList.find(attribute => attribute.id === this.attributeSpectraId).label];
        this.spectraCSV = this.http.get(`${this.config.servicesUrl}/spectra-to-csv/${this.datasetName}?filename=${spectraFile}`, { responseType: 'text' });
    }

    getZ() {
        const z = this.object[this.attributeList.find(attribute => attribute.id === this.attributeZId).label];
        return +z;
    }
}
