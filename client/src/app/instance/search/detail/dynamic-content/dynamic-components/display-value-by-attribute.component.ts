/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';

import { Attribute, Dataset, Instance, OutputCategory } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';

@Component({
    selector: 'app-display-value-by-attribute',
    templateUrl: 'display-value-by-attribute.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayValueByAttributeComponent {
    @Input() object: any;
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() attributeList: Attribute[];
    @Input() queryParams: SearchQueryParams;
    @Input() attributeId: number;
    @Input() rendererEnabled: boolean;

    constructor(protected store: Store<{}>) { }

    getAttributeById() {
        return this.attributeList.find(attribute => attribute.id === this.attributeId);
    }

    /**
     * Dispatches action to launch the file download
     * 
     * @param { url: string, filename: string } download
     */
    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }
}
