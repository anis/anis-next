/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { Attribute, Dataset, Image } from "src/app/metamodel/models";
import { DisplayImageComponent } from "./display-image.component";
import * as utils from 'src/app/shared/utils';
describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayImageComponent', () => {
    let component: DisplayImageComponent;
    let fixture: ComponentFixture<DisplayImageComponent>;
    let dataset: Dataset;
    let image: Image;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayImageComponent],
            providers: [
                { provide: AppConfigService, useValue: { servicesUrl: 'test', apiUrl: 'test' } }
            ]
        });
        fixture = TestBed.createComponent(DisplayImageComponent);
        component = fixture.componentInstance;

    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getValue() should return test/fits-to-png/test?filename=test&stretch=linear&pmin=0.25&pmax=99.75&axes=true', () => {
        let expected = 'test/fits-to-png/test?filename=test&stretch=linear&pmin=0.25&pmax=99.75&axes=true';
        component.dataset = { ...dataset, name: 'test' };
        component.getPath = jest.fn().mockImplementationOnce(() => 'test');
        component.type = 'fits';
        expect(component.getValue()).toEqual(expected);
    });
    it('getValue() should return test/dataset/test/file-explorertest', () => {
        let expected = 'test/dataset/test/file-explorertest';
        component.dataset = { ...dataset, name: 'test' };
        component.getPath = jest.fn().mockImplementationOnce(() => 'test');
        let spy = jest.spyOn(utils, 'getHost');
        spy.mockImplementationOnce(() => 'test');
        component.type = 'image';
        expect(component.getValue()).toEqual(expected);
    });
    it('getValue() should return test_result', () => {
        let expected = 'test_result';
        component.getAttributeImage = jest.fn().mockImplementationOnce(() => {
            return { ...attribute, label: 'test' } as Attribute
        });
        component.object = {
            test: 'test_result'
        }
        expect(component.getValue()).toEqual(expected);
    });

    it('getPath() should return /test', () => {
        component.object = { test: 'test' };
        component.getAttributeImage = jest.fn().mockImplementationOnce(() => {
            return { ...attribute, label: 'test' } as Attribute
        });
        expect(component.getPath()).toEqual('/test');
    });
    it('getAttributeImage() should return an attribute with id 2 ', () => {
        component.attributeList = [
            { ...attribute, id: 2 },
            { ...attribute, id: 1 }
        ];
        component.attributeImageId = 2;
        expect(component.getAttributeImage().id).toEqual(2);

    });
    it('getStyle() should return {"width": "10", "height": "5" }', () => {
        component.width = '10';
        component.height = '5';
        expect(component.getStyle()).toEqual({ "width": "10", "height": "5" });

    });

});