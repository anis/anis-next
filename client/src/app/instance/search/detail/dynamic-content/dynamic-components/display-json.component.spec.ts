/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Attribute } from "src/app/metamodel/models";
import { DisplayJsonComponent } from "./display-json.component";
describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayJsonComponent', () => {
    let component: DisplayJsonComponent;
    let fixture: ComponentFixture<DisplayJsonComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayJsonComponent],
        });
        fixture = TestBed.createComponent(DisplayJsonComponent);
        component = fixture.componentInstance;
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getValue() should return test', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test1' },
            { ...attribute, id: 2, label: 'test2' }
        ];
        component.attributeJsonId = 2;
        component.object = { test2: 'test' };
        expect(component.getValue()).toEqual('test');
    });
})