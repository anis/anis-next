/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { getHost } from 'src/app/shared/utils';
import { Attribute, Dataset } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-display-image',
    templateUrl: 'display-image.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayImageComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() dataset: Dataset;
    @Input() attributeImageId: number;
    @Input() type: string;
    @Input() width: string;
    @Input() height: string;

    loading = true;

    constructor(private appConfig: AppConfigService) { }

    onLoad() {
        this.loading = false;
    }

    /**
     * Returns source image.
     *
     * @return string
     */
    getValue(): string {
        if (this.type === 'fits') {
            return `${this.appConfig.servicesUrl}/fits-to-png/${this.dataset.name}?filename=${this.getPath()}`
                + `&stretch=linear&pmin=0.25&pmax=99.75&axes=true`;
        } else if (this.type === 'image') {
            return `${getHost(this.appConfig.apiUrl)}/dataset/${this.dataset.name}/file-explorer${this.getPath()}`;
        } else {
            return this.object[this.getAttributeImage().label] as string;
        }
    }

    getPath() {
        let path = this.object[this.getAttributeImage().label];
        if (path[0] !== '/') {
            path = '/' + path;
        }
        return path;
    }

    getAttributeImage() {
        return this.attributeList.find(attribute => attribute.id === this.attributeImageId);
    }

    getStyle() {
        let style = { 
            "width": '100%'
        } as any;
        if (this.width && this.height) {
            style = {
                "width": this.width,
                "height": this.height
            };
        }
        return style;
    }
}
