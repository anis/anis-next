/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Attribute } from "src/app/metamodel/models";
import { DisplayRaDecComponent } from "./display-ra-dec.component";

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayRaDecComponent', () => {
    let component: DisplayRaDecComponent;
    let fixture: ComponentFixture<DisplayRaDecComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayRaDecComponent],
        });
        fixture = TestBed.createComponent(DisplayRaDecComponent);
        component = fixture.componentInstance;
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getAttributeRa() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test1' },
            { ...attribute, id: 2, label: 'test2' }
        ];
        component.attributeRaId = 2;
        component.object = { test2: 'test' };
        expect(component.getAttributeRa().id).toEqual(2);
    });
    it('getAttributeRa() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test1' },
            { ...attribute, id: 2, label: 'test2' }
        ];
        component.attributeDecId = 2;
        component.object = { test2: 'test' };
        expect(component.getAttributeDec().id).toEqual(2);
    });
    
})