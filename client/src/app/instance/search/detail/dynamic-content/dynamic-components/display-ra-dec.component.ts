/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Display object component.
 */
@Component({
    selector: 'app-display-ra-dec',
    templateUrl: 'display-ra-dec.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayRaDecComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() attributeRaId: number;
    @Input() attributeDecId: number;

    getAttributeRa() {
        return this.attributeList.find(attribute => attribute.id === this.attributeRaId);
    }

    getAttributeDec() {
        return this.attributeList.find(attribute => attribute.id === this.attributeDecId);
    }
}
