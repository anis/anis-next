/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Store } from '@ngrx/store';

import { Attribute, Dataset, Instance, OutputCategory, OutputFamily } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';

/**
 * @class
 * @classdesc Display object component.
 */
@Component({
    selector: 'app-display-object',
    templateUrl: 'display-object.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayObjectComponent {
    @Input() object: any;
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: OutputFamily[];
    @Input() outputCategoryList: OutputCategory[];
    @Input() queryParams: SearchQueryParams;

    constructor(protected store: Store<{}>) { }

    getOutputFamilyList() {
        return this.outputFamilyList.filter(family => this.getOutputCategoryListByFamily(family.id).length > 0);
    }

    getOutputCategoryListByFamily(idOutputFamily: number): OutputCategory[] {
        return this.outputCategoryList
            .filter(outputCategory => outputCategory.id_output_family === idOutputFamily)
            .filter(outputCategory => this.getAttributeListByOutputCategory(outputCategory.id).length > 0);
    }

    getAttributeListByOutputCategory(idOutputCategory: number): Attribute[] {
        return this.attributeList.filter(attribute => attribute.id_detail_output_category === idOutputCategory);
    }

    /**
     * Dispatches action to launch the file download
     * 
     * @param { url: string, filename: string } download
     */
    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }
}
