/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Attribute, Dataset, Image } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-display-fits-cut',
    templateUrl: 'display-fits-cut.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayFitsCutComponent {
    @Input() object: any;
    @Input() dataset: Dataset;
    @Input() imageList: Image[];
    @Input() imageId: number;
    @Input() attributeList: Attribute[];
    @Input() attributeRaId: number;
    @Input() attributeDecId: number;
    @Input() radius: number;

    loading = true;

    constructor(private appConfig: AppConfigService) { }

    onLoad() {
        this.loading = false;
    }

    getHref() {
        const image = this.getImage();
        let href = `${this.appConfig.servicesUrl}/fits-cut-to-png/${this.dataset.name}?filename=${image.file_path}`;
        href += `&ra=${this.getRaValue()}`;
        href += `&dec=${this.getDecValue()}`;
        href += `&radius=${this.radius}`;
        href += `&stretch=${image.stretch}`;
        href += `&pmin=${image.pmin}`;
        href += `&pmax=${image.pmax}`;
        href += `&axes=false`;
        return href;
    }

    getImage() {
        return this.imageList.find(image => image.id === this.imageId);
    }

    getRaValue() {
        const attributeRa = this.attributeList.find(attribute => attribute.id === this.attributeRaId);
        return this.object[attributeRa.label];
    }

    getDecValue() {
        const attributeDec = this.attributeList.find(attribute => attribute.id === this.attributeDecId);
        return this.object[attributeDec.label];
    }
}
