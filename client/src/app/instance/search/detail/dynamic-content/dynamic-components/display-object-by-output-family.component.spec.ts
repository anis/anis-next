/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Attribute } from 'src/app/metamodel/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import { DisplayObjectByOutputFamilyComponent } from './display-object-by-output-family.component';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayObjectByOutputFamilyComponent', () => {
    let component: DisplayObjectByOutputFamilyComponent;
    let fixture: ComponentFixture<DisplayObjectByOutputFamilyComponent>;
    let store: MockStore;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DisplayObjectByOutputFamilyComponent
            ],
            providers: [
                provideMockStore({}),
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        })
        fixture = TestBed.createComponent(DisplayObjectByOutputFamilyComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getFamily() should return output family with id 2', () => {
        component.outputFamilyList = [
            { id: 1, display: 10, label: 'test1', opened: true },
            { id: 2, display: 10, label: 'test2', opened: false }
        ];
        component.outputFamilyId = 2;
        expect(component.getFamily().id).toEqual(2);
    });
    it('getOutputCategoryListByFamily should return an array with two elements', () => {
        component.outputCategoryList = [
            { id: 1, display: 10, id_output_family: 2, label: 'test1' },
            { id: 2, display: 10, id_output_family: 3, label: 'test2' },
            { id: 3, display: 10, id_output_family: 2, label: 'test3' },
        ]
        component.getAttributeListByOutputCategory = jest.fn().mockImplementation(() => [1])
        expect(component.getOutputCategoryListByFamily(2).length).toEqual(2);
    });
    it('getAttributeListByOutputCategory should return an array with two elements', () => {
        let attribute: Attribute;
        component.attributeList = [
            { ...attribute, id: 1, id_detail_output_category: 2 },
            { ...attribute, id: 2, id_detail_output_category: 3 },
            { ...attribute, id: 2, id_detail_output_category: 2 }
        ]
        expect(component.getAttributeListByOutputCategory(2).length).toEqual(2);
    });
    it('should raises store dispatch event with download file action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.downloadFile({ url: 'test.fr', filename: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.downloadFile({ url: 'test.fr', filename: 'test' }));
    })
});