/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Attribute } from 'src/app/metamodel/models';
import { DisplayObjectByOutputCategoryComponent } from './display-object-by-output-category.component';
import * as searchActions from 'src/app/instance/store/actions/search.actions';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayObjectByOutputCategoryComponent', () => {
    let component: DisplayObjectByOutputCategoryComponent;
    let fixture: ComponentFixture<DisplayObjectByOutputCategoryComponent>;
    let store: MockStore;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DisplayObjectByOutputCategoryComponent
            ],
            providers: [
                provideMockStore({}),
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        })
        fixture = TestBed.createComponent(DisplayObjectByOutputCategoryComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getCategory() should return output category with id 2', () => {
        component.outputCategoryList = [
            { id: 1, display: 10, id_output_family: 2, label: 'test1' },
            { id: 2, display: 10, id_output_family: 2, label: 'test2' }
        ];
        component.outputCategoryId = 2;
        expect(component.getCategory().id).toEqual(2);
    });
    it('should return an array with two elements', () => {
        let attribute: Attribute;
        component.attributeList = [
            { ...attribute, id: 1, id_detail_output_category: 2 },
            { ...attribute, id: 2, id_detail_output_category: 3 },
            { ...attribute, id: 2, id_detail_output_category: 2 }
        ]
        expect(component.getAttributeListByOutputCategory(2).length).toEqual(2);
    });
    it('should raises store dispatch event with download file action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.downloadFile({ url: 'test.fr', filename: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.downloadFile({ url: 'test.fr', filename: 'test' }));
    })
});