/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';

import { Attribute, Dataset, Instance, OutputCategory } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';

@Component({
    selector: 'app-display-object-by-output-category',
    templateUrl: 'display-object-by-output-category.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayObjectByOutputCategoryComponent {
    @Input() object: any;
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() attributeList: Attribute[];
    @Input() outputCategoryList: OutputCategory[];
    @Input() queryParams: SearchQueryParams;
    @Input() outputCategoryId: number;
    @Input() isOpened: boolean;

    constructor(protected store: Store<{}>) { }

    getCategory() {
        return this.outputCategoryList.find(outputCategory => outputCategory.id === this.outputCategoryId);
    }

    getAttributeListByOutputCategory(idOutputCategory: number): Attribute[] {
        return this.attributeList.filter(attribute => attribute.id_detail_output_category === idOutputCategory);
    }

    /**
     * Dispatches action to launch the file download
     * 
     * @param { url: string, filename: string } download
     */
    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }
}
