/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { Attribute } from "src/app/metamodel/models";
import { DisplayValueByAttributeComponent } from "./display-value-by-attribute.component";
import * as searchActions from 'src/app/instance/store/actions/search.actions';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayValueByAttributeComponent', () => {
    let component: DisplayValueByAttributeComponent;
    let fixture: ComponentFixture<DisplayValueByAttributeComponent>;
    let attribute: Attribute;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayValueByAttributeComponent],
            providers: [
                provideMockStore({}),
            ]
        });
        fixture = TestBed.createComponent(DisplayValueByAttributeComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);

    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getAttributeById() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1 },
            { ...attribute, id: 2 }
        ];
        component.attributeId = 2;
        expect(component.getAttributeById().id).toEqual(2);
    });
    it('should raises store dispatch event with download file action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.downloadFile({ url: 'test.fr', filename: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.downloadFile({ url: 'test.fr', filename: 'test' }));
    });
});