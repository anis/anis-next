import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { SearchService } from 'src/app/instance/store/services/search.service';
import { Attribute, Dataset, Instance } from 'src/app/metamodel/models';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchModule } from '../../../search.module';

import { DatatableOneLinerComponent } from './datatable-one-liner.component';

class MockSearch {
    constructor() {}

    retrieveData(query: string): Observable<string> {
        return of(query);
    }
}

describe('DatatableOneLinerComponent', () => {
    let component: DatatableOneLinerComponent;
    let fixture: ComponentFixture<DatatableOneLinerComponent>;
    let search = new MockSearch();

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [DatatableOneLinerComponent],
            imports: [SearchModule, SharedModule],
            providers: [
                {
                    provide: SearchService,
                    useValue: search,
                },
            ],
        }).compileComponents();

        fixture = TestBed.createComponent(DatatableOneLinerComponent);
        component = fixture.componentInstance;

        component.attributeToList = 'all';
        component.attributeList = [
            { label: 'id', description: 'id' },
        ] as Attribute[];
        component.dataset = { name: 'default' } as Dataset;
        component.object = { id: 34 };
        component.instance = { result_datatable_bordered: true } as Instance;
    });

    it('should create a null and empty component', () => {
        expect(component).toBeTruthy();
    });

    it('should format query, get data and create the attributesArray on Init', () => {
        const formatting = jest.spyOn(component, 'formatAttributesQuery');
        const gettingData = jest.spyOn(component, 'getData');
        const creatingArray = jest.spyOn(
            component,
            'createAttributesArrayToShow'
        );
        let retrieveData = jest.spyOn(search, 'retrieveData');

        fixture.detectChanges();

        expect(formatting).toHaveBeenCalled();
        expect(gettingData).toHaveBeenCalled();
        expect(creatingArray).toHaveBeenCalled();
        expect(retrieveData).toHaveBeenCalled();
    });

    it('on formatAttributesQuery call, should return a semi-colon separated list of numbers', () => {
        fixture.detectChanges();
        let res = component.formatAttributesQuery();
        expect(res).toEqual('1');

        component.attributeList = [
            { label: 'id', description: 'id' },
            { label: 'poi', description: 'poi' },
            { label: 'planet', description: 'is planet' },
            { label: 'signal', description: 'is signal' },
        ] as Attribute[];
        res = component.formatAttributesQuery();
        expect(res).toEqual('1;2;3;4');

        component.attributeToList = [1, 3];
        res = component.formatAttributesQuery();
        expect(res).toEqual('1;3');
    });

    it('on createAttributesArrayToShow call, should return an array of attributes', () => {
        fixture.detectChanges();
        expect(component.attributesToShow).toEqual(component.attributeList);

        component.attributeList = [
            { label: 'id', description: 'id' },
            { label: 'poi', description: 'poi' },
            { label: 'planet', description: 'is planet' },
            { label: 'signal', description: 'is signal' },
        ] as Attribute[];
        component.attributesToShow = [];
        component.createAttributesArrayToShow();
        expect(component.attributesToShow).toEqual(component.attributeList);

        component.attributeToList = [1, 3];
        component.attributesToShow = [];
        component.createAttributesArrayToShow();
        expect(component.attributesToShow).toEqual([
            { label: 'id', description: 'id' },
            { label: 'planet', description: 'is planet' },
        ] as Attribute[]);
    });

    it('on getData call, should subscribe to a server with the good query', () => {
        const spySearch = jest.spyOn(search, 'retrieveData');
        fixture.detectChanges();

        expect(spySearch).toHaveBeenCalled();
        expect(spySearch).toHaveBeenCalledWith(
            `default?a=1&c=1::eq::34&f=json`
        );

        component.attributeList = [
            { label: 'id', description: 'id' },
            { label: 'poi', description: 'poi' },
            { label: 'planet', description: 'is planet' },
            { label: 'signal', description: 'is signal' },
        ] as Attribute[];
        component.attributeToList = [1, 3];
        component.attributesToShow = [];
        component.ngOnInit();
        expect(spySearch).toHaveBeenCalled();
        expect(spySearch).toHaveBeenCalledWith(
            `default?a=1;3&c=1::eq::34&f=json`
        );

        component.attributeList = [
            { label: 'id', description: 'id' },
            { label: 'poi', description: 'poi' },
            { label: 'planet', description: 'is planet' },
            { label: 'signal', description: 'is signal' },
        ] as Attribute[];
        component.attributesToShow = [];
        component.attributeToList = 'all';
        component.ngOnInit();
        expect(spySearch).toHaveBeenCalled();
        expect(spySearch).toHaveBeenCalledWith(
            `default?a=1;2;3;4&c=1::eq::34&f=json`
        );
    });

    it('should throw two kind of errors if the component is wrongly instantiated', () => {
        const formatting = jest.spyOn(component, 'formatAttributesQuery');
        const spyConsole = jest.spyOn(console, 'error');

        component.attributeToList = 'coconut';
        fixture.detectChanges();
        component.ngOnInit();
        expect(formatting).toHaveBeenCalled();
        expect(formatting).toThrowError();
        expect(spyConsole).toHaveBeenCalled();

        component.attributeToList = [1, 3];
        fixture.detectChanges();
        component.ngOnInit();
        expect(formatting).toHaveBeenCalled();
        expect(formatting).toThrowError();
        expect(spyConsole).toHaveBeenCalled();
    });
});
