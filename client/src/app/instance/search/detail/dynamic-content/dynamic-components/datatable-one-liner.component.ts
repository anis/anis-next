/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Attribute, Dataset, Instance } from 'src/app/metamodel/models';
import { SearchService } from 'src/app/instance/store/services/search.service';
import { Observable } from 'rxjs';

/**
 * Make a simple one-line array on the detail page related to the context provided.
 * @class
 * @classdesc Note that this component is injected dynamically from the database (see ngx-dynamic-hooks).
 */
@Component({
    selector: 'app-datatable-one-liner',
    templateUrl: './datatable-one-liner.component.html',
})
export class DatatableOneLinerComponent implements OnInit {
    @Input() instance: Instance = null;
    @Input() dataset: Dataset = null;
    @Input() attributeList: Attribute[] = null;
    @Input() attributeToList: Array<number> | string = null;
    @Input() object: any = null;

    dataArrived: boolean = null;
    attributesToShow: Attribute[] = [];
    data: any[] = [];

    /**
     * Create a one-liner array.
     * @param {SearchService} search - The service to query data.
     */
    constructor(private search: SearchService) {}

    /**
     * Format the list of attributes accordingly given in the input attributeToList to be shown.
     * attributeToList is in fact the list of data to be visible in the array.
     * @method
     * @returns {string | never} The properly formatted string for the query if any.
     * @throws {Error} If the input attributeToList is wrongly formatted (shall be either 'all' or an array of numbers).
     * @throws {Error} If the input attributeToList contains indices not referencing available attributes of the dataset.
     */
    formatAttributesQuery(): string | never {
        if (this.attributeToList === 'all' && this.attributeList.length === 1) {
            return '1';
        }

        if (this.attributeToList === 'all') {
            const all = [...Array(this.attributeList.length + 1).keys()];
            return all
                .slice(1, this.attributeList.length + 1)
                .toString()
                .replace(/\,/g, ';');
        }

        if (
            this.attributeToList &&
            this.attributeToList.length != 0 &&
            typeof this.attributeToList[0] === 'number'
        ) {
            const isDemandValid: boolean = (
                this.attributeToList as number[]
            ).every((el) => el < this.attributeList.length);
            if (!isDemandValid) {
                throw new Error(
                    `Error: attributeToList value error\nThe injected array shall contains indices less than the total number of attributes of the consulted dataset.`
                );
            }
            return this.attributeToList.toString().replace(/\,/g, ';');
        }

        throw new Error(
            `Error: attributeToList Input parsing error\nattributeToList shall be either an Array of numbers of \"all\"`
        );
    }

    /**
     * Create the array of Attribute to show, manipulate the property attributesToShow.
     * Fill it with the wished array of attributeListn to be shown from the dataset.
     * @method
     * @returns {void}
     */
    createAttributesArrayToShow(): void {
        if (this.attributeToList === 'all') {
            this.attributesToShow = [...this.attributeList];
        } else {
            (this.attributeToList as number[]).forEach((elem) =>
                this.attributesToShow.push(this.attributeList[elem - 1])
            );
        }
    }

    /**
     * Make the subscription to the server hosting the database.
     * Manipulate the property data to fill it with the wished array of data from the dataset.
     * @method
     * @param {string} formattedListOfAttributes - The list of attributes properly formatted to construct the query to the database
     * @returns {void}
     */
    getData(formattedListOfAttributes: string): Observable<any[]> | null {
        if (formattedListOfAttributes != null) {
            const query: string = `${this.dataset.name}?a=${formattedListOfAttributes}&c=1::eq::${this.object.id}&f=json`;
            this.search.retrieveData(query).subscribe((data) => {
                this.data = data[0];
                this.dataArrived = true;
            });
        }
        return null;
    }

    /**
     * Calls in order and successively the methods:
        - DatatableOneLinerComponent.formatAttributesQuery
        - DatatableOneLinerComponent.createAttributesArrayToShow
        - DatatableOneLinerComponent.getData
     * @method
     * @implements OnInit
     */
    ngOnInit(): void {
        this.dataArrived = false;
        let formattedListOfAttributes: string = null;
        try {
            formattedListOfAttributes = this.formatAttributesQuery();
        } catch (error) {
            console.error(error);
            return;
        }
        this.createAttributesArrayToShow();
        this.getData(formattedListOfAttributes);
    }
}
