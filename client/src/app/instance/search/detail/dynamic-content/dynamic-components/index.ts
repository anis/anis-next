/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DisplayObjectComponent } from './display-object.component';
import { DisplayObjectByOutputCategoryComponent } from './display-object-by-output-category.component';
import { DisplayObjectByOutputFamilyComponent } from './display-object-by-output-family.component';
import { DisplayValueByAttributeComponent } from './display-value-by-attribute.component';
import { DisplayRaDecComponent } from './display-ra-dec.component';
import { DisplaySpectraComponent } from './display-spectra.component';
import { DisplayImageComponent } from './display-image.component';
import { DisplayJsonComponent } from './display-json.component';
import { DisplayFitsCutComponent } from './display-fits-cut.component';
import { SpectraGraphComponent } from './spectra-graph/spectra-graph.component';
import { DatatableOneLinerComponent } from './datatable-one-liner.component';

export const dynamicComponents = [
    DisplayObjectComponent,
    DisplayObjectByOutputCategoryComponent,
    DisplayObjectByOutputFamilyComponent,
    DisplayValueByAttributeComponent,
    DisplayRaDecComponent,
    DisplaySpectraComponent,
    DisplayImageComponent,
    DisplayJsonComponent,
    DisplayFitsCutComponent,
    SpectraGraphComponent,
    DatatableOneLinerComponent,
];
