/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { Attribute, Dataset, Image } from "src/app/metamodel/models";
import { DisplayFitsCutComponent } from "./display-fits-cut.component"

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayFitsCutComponent', () => {
    let component: DisplayFitsCutComponent;
    let fixture: ComponentFixture<DisplayFitsCutComponent>;
    let dataset: Dataset;
    let image: Image;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayFitsCutComponent],
            providers: [
                { provide: AppConfigService, useValue: { servicesUrl: 'test' } }
            ]
        });
        fixture = TestBed.createComponent(DisplayFitsCutComponent);
        component = fixture.componentInstance;

    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('should return test/fits-cut-to-png/test?filename=test&ra=2&dec=1&radius=3&stretch=test&pmin=1&pmax=2&axes=false', () => {
        component.dataset = { ...dataset, name: 'test' };

        component.getImage = jest.fn().mockImplementationOnce(() => {
            return { ...image, file_path: 'test', stretch: 'test', pmax: 2, pmin: 1 } as Image
        });
        component.getRaValue = jest.fn().mockImplementationOnce(() => 2);
        component.getDecValue = jest.fn().mockImplementationOnce(() => 1);
        component.radius = 3;
        let expected = 'test/fits-cut-to-png/test?filename=test&ra=2&dec=1&radius=3&stretch=test&pmin=1&pmax=2&axes=false';
        expect(component.getHref()).toEqual(expected);
    });
    it('should return an image with id 2', () => {
        component.imageList = [
            { ...image, id: 1 },
            { ...image, id: 2 }
        ]
        component.imageId = 2;
        expect(component.getImage().id).toEqual(2);
    });
    it('getImage() should return  10', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test_label_ra' },
            { ...attribute, id: 2 }
        ]
        component.attributeRaId = 1;
        component.object = { test_label_ra: 10 };
        expect(component.getRaValue()).toEqual(10);
    });
    it('getDecValue() should return  5', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test_label_ra' },
            { ...attribute, id: 2, label: 'test_label_dec' }
        ]
        component.attributeDecId = 2;
        component.object = { test_label_dec: 5 };
        expect(component.getDecValue()).toEqual(5);
    });


})