/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { HookParserEntry } from 'ngx-dynamic-hooks';

import { hookParsers } from './parsers';
import { dynamicComponents } from './dynamic-components';

export const componentParsers: Array<HookParserEntry> = [
    ...hookParsers,
    ...dynamicComponents.map(component => {
        return { component };
    })
];

export { hookParsers } from './parsers';
export { dynamicComponents } from './dynamic-components';
