/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { Attribute, OutputCategory, OutputFamily, DetailConfig, Dataset, Instance, Image } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import * as detailActions from 'src/app/instance/store/actions/detail.actions';
import * as detailSelector from 'src/app/instance/store/selectors/detail.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as searchSelector from 'src/app/instance/store/selectors/search.selector';
import * as outputFamilySelector from 'src/app/metamodel/selectors/output-family.selector';
import * as outputCategorySelector from 'src/app/metamodel/selectors/output-category.selector';
import * as attributeSelector from 'src/app/metamodel/selectors/attribute.selector';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as detailConfigActions from 'src/app/metamodel/actions/detail-config.actions';
import * as detailConfigSelector from 'src/app/metamodel/selectors/detail-config.selector';
import * as imageActions from 'src/app/metamodel/actions/image.actions';
import * as imageSelector from 'src/app/metamodel/selectors/image.selector';

/**
 * @class
 * @classdesc Detail container.
 *
 * @implements OnInit
 * @implements OnDestroy
 */
@Component({
    selector: 'app-detail-page',
    templateUrl: 'detail.component.html'
})
export class DetailComponent implements OnInit, OnDestroy {
    public dataset: Observable<Dataset>;
    public instance: Observable<Instance>;
    public pristine: Observable<boolean>;
    public detailConfig: Observable<DetailConfig>;
    public detailConfigIsLoading: Observable<boolean>;
    public detailConfigIsLoaded: Observable<boolean>;
    public attributeList: Observable<Attribute[]>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public outputFamilyList: Observable<OutputFamily[]>;
    public outputFamilyListIsLoading: Observable<boolean>;
    public outputFamilyListIsLoaded: Observable<boolean>;
    public outputCategoryList: Observable<OutputCategory[]>;
    public outputCategoryListIsLoading: Observable<boolean>;
    public outputCategoryListIsLoaded: Observable<boolean>;
    public object: Observable<any>;
    public objectIsLoading: Observable<boolean>;
    public objectIsLoaded: Observable<boolean>;
    public imageList: Observable<Image[]>;
    public imageListIsLoading: Observable<boolean>;
    public imageListIsLoaded: Observable<boolean>;
    public queryParams: Observable<SearchQueryParams>;

    public attributeListIsLoadedSubscription: Subscription;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.dataset = store.select(datasetSelector.selectDatasetByRouteName);
        this.pristine = store.select(searchSelector.selectPristine);
        this.detailConfig = store.select(detailConfigSelector.selectDetailConfig);
        this.detailConfigIsLoading = store.select(detailConfigSelector.selectDetailConfigIsLoading);
        this.detailConfigIsLoaded = store.select(detailConfigSelector.selectDetailConfigIsLoaded);
        this.attributeList = store.select(attributeSelector.selectAllAttributesOrderByDetailDisplay);
        this.attributeListIsLoading = store.select(attributeSelector.selectAttributeListIsLoading);
        this.attributeListIsLoaded = store.select(attributeSelector.selectAttributeListIsLoaded);
        this.outputFamilyList = store.select(outputFamilySelector.selectAllOutputFamilies);
        this.outputFamilyListIsLoading = store.select(outputFamilySelector.selectOutputFamilyListIsLoading);
        this.outputFamilyListIsLoaded = store.select(outputFamilySelector.selectOutputFamilyListIsLoaded);
        this.outputCategoryList = store.select(outputCategorySelector.selectAllOutputCategories);
        this.outputCategoryListIsLoading = store.select(outputCategorySelector.selectOutputCategoryListIsLoading);
        this.outputCategoryListIsLoaded = store.select(outputCategorySelector.selectOutputCategoryListIsLoaded);
        this.object = this.store.select(detailSelector.selectObject);
        this.objectIsLoading = this.store.select(detailSelector.selectObjectIsLoading);
        this.objectIsLoaded = this.store.select(detailSelector.selectObjectIsLoaded);
        this.imageList = this.store.select(imageSelector.selectAllImages);
        this.imageListIsLoading = this.store.select(imageSelector.selectImageListIsLoading);
        this.imageListIsLoaded = this.store.select(imageSelector.selectImageListIsLoaded);
        this.queryParams = this.store.select(searchSelector.selectQueryParams);
    }

    ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.initSearch()));
        Promise.resolve(null).then(() => this.store.dispatch(detailConfigActions.loadDetailConfig()));
        Promise.resolve(null).then(() => this.store.dispatch(imageActions.loadImageList()));
        this.attributeListIsLoadedSubscription = this.attributeListIsLoaded.subscribe(attributeListIsLoaded => {
            if (attributeListIsLoaded) {
                Promise.resolve(null).then(() => this.store.dispatch(detailActions.retrieveObject()));
            }
        });
    }

    /**
     * Resets detail information.
     */
    ngOnDestroy(): void {
        if (this.attributeListIsLoadedSubscription) this.attributeListIsLoadedSubscription.unsubscribe();
        this.store.dispatch(detailActions.destroyDetail());
    }
}
