/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { DetailComponent } from './detail.component';
import { Attribute, DetailConfig, OutputCategory, OutputFamily } from 'src/app/metamodel/models';
import * as detailActions from 'src/app/instance/store/actions/detail.actions';
import * as detailConfigActions from 'src/app/metamodel/actions/detail-config.actions';
import * as imageActions from 'src/app/metamodel/actions/image.actions';
import * as searchActions from 'src/app/instance/store/actions/search.actions';

describe('[Instance][Search][Container] DetailComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-detail-content ', template: '' })
    class DetailContentStubComponent {
        @Input() detailConfig: DetailConfig
        @Input() object: any;
        @Input() datasetName: string;
        @Input() attributeList: Attribute[];
        @Input() outputFamilyList: OutputFamily[];
        @Input() outputCategoryList: OutputCategory[];
    }

    let component: DetailComponent;
    let fixture: ComponentFixture<DetailComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailComponent,
                SpinnerStubComponent,
                DetailContentStubComponent
            ],
            providers: [
                provideMockStore({ })
            ]
        });
        fixture = TestBed.createComponent(DetailComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.attributeListIsLoaded = of(true);
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(4);
            expect(spy).toHaveBeenCalledWith(searchActions.initSearch());
            expect(spy).toHaveBeenCalledWith(detailConfigActions.loadDetailConfig());
            expect(spy).toHaveBeenCalledWith(imageActions.loadImageList());
            expect(spy).toHaveBeenCalledWith(detailActions.retrieveObject());
            done();
        });
    });

    it('#ngOnDestroy() should unsubscribe from attributeListIsLoadedSubscription', () => {
        component.attributeListIsLoadedSubscription = of().subscribe();
        const spy = jest.spyOn(component.attributeListIsLoadedSubscription, 'unsubscribe');
        component.ngOnDestroy();
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
