/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Dataset } from "src/app/metamodel/models";
import { StyleService } from "src/app/shared/services/style.service";
import { DetailContentComponent } from "./detail-content.component";

describe('[Instance][Search][Component][Result] DetailContentComponent', () => {
    let component: DetailContentComponent;
    let fixture: ComponentFixture<DetailContentComponent>;
    let dataset: Dataset;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailContentComponent
            ],
            providers: [
                { provide: StyleService, useValue: new StyleService() }
            ]
        });
        fixture = TestBed.createComponent(DetailContentComponent);
        component = fixture.componentInstance;
        component.detailConfig = { content: '', id: 1, style_sheet: 'test' };
        component.dataset = { ...dataset, name: 'test' };
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });

});