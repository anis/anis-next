/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
} from '@angular/core';

import {
    DetailConfig,
    Attribute,
    Dataset,
    OutputFamily,
    OutputCategory,
    Instance,
    Image,
} from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import { globalParsers } from 'src/app/shared/dynamic-content';
import { componentParsers } from '../dynamic-content';
import { StyleService } from 'src/app/shared/services/style.service';

/**
 * @class
 * @classdesc Detail content component.
 */
@Component({
    selector: 'app-detail-content',
    templateUrl: 'detail-content.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailContentComponent implements OnInit {
    @Input() detailConfig: DetailConfig;
    @Input() object: any;
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: OutputFamily[];
    @Input() outputCategoryList: OutputCategory[];
    @Input() imageList: Image[];
    @Input() queryParams: SearchQueryParams;

    constructor(private style: StyleService) {}

    ngOnInit() {
        if (this.detailConfig.style_sheet) {
            this.style.addCSS(
                this.detailConfig.style_sheet.replace(
                    /(.+{)/g,
                    (match, $1) => `.detail-${this.dataset.name} ${$1}`
                ),
                'detail'
            );
        }
    }

    getParsers() {
        return [...globalParsers, ...componentParsers];
    }

    getContext() {
        return {
            object: this.object,
            dataset: this.dataset,
            instance: this.instance,
            attributeList: this.attributeList,
            outputFamilyList: this.outputFamilyList,
            outputCategoryList: this.outputCategoryList,
            imageList: this.imageList,
            queryParams: this.queryParams,
        };
    }
}
