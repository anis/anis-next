/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DetailContentComponent } from './detail-content.component';
import { DisplayDetailRendererComponent } from './display-detail-renderer.component';

export const dummiesComponents = [
    DetailContentComponent,
    DisplayDetailRendererComponent
];
