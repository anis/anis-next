/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Type } from '@angular/core';

import { getRendererComponent } from 'src/app/instance/search/shared-renderer/components';
import { AbstractRendererComponent } from 'src/app/instance/search/shared-renderer/abstract-renderer.component';

export const rendererComponents = [
    
];

export const getDetailRendererComponent = (renderer: string): Type<AbstractRendererComponent> => {
    let nameOfRendererComponent = getRendererComponent(renderer);
    switch(renderer) {
        
    }
    return nameOfRendererComponent;
}
