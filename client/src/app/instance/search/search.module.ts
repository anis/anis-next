/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { ConeSearchModule } from '../cone-search/cone-search.module';
import { SearchRoutingModule, routedComponents } from './search-routing.module';
import { dummiesComponents } from './components';
import { searchPipes } from './pipes';
import { SharedRendererModule } from './shared-renderer/shared-renderer.module';

/**
 * @class
 * @classdesc Search module.
 */
@NgModule({
    imports: [
        SharedModule,
        ConeSearchModule,
        SearchRoutingModule,
        SharedRendererModule
    ],
    declarations: [
        routedComponents,
        dummiesComponents,
        searchPipes
    ],
})
export class SearchModule { }
