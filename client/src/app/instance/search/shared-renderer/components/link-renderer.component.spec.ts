/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkRendererComponent } from './link-renderer.component';
import { LinkRendererConfig } from 'src/app/metamodel/models';

describe('[Instance][Search][Component][Result][Renderer] LinkRendererComponent', () => {
    let component: LinkRendererComponent;
    let fixture: ComponentFixture<LinkRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LinkRendererComponent]
        });
        fixture = TestBed.createComponent(LinkRendererComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return link url', () => {
        const attribute = {
            id: 1,
            name: 'name_one',
            label: 'label_one',
            form_label: 'form_label_one',
            description: 'description_one',
            primary_key: true,
            type: 'integer',
            search_type: 'field',
            operator: '=',
            dynamic_operator: false,
            min: null,
            max: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 2,
            output_display: 2,
            selected: true,
            renderer: null,
            renderer_config: {
                id: 'renderer-config',
                href: 'http://test.com/$value',
                display: 'display',
                text: 'text',
                icon: 'icon',
                blank: true
            } as LinkRendererConfig,
            order_by: true,
            archive: false,
            detail_display: 2,
            detail_renderer: null,
            detail_renderer_config: null,
            options: [
                { label: 'Three', value: 'three', display: 3 },
                { label: 'One', value: 'one', display: 1 },
                { label: 'Two', value: 'two', display: 2 }
            ],
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: 2,
            id_detail_output_category: null
        };

        component.rendererType = 'result';
        component.attribute = attribute;
        component.value = 'myId';
        expect(component.getValue()).toEqual('http://test.com/myId');
    });

    it('#getText() should return link text', () => {
        const attribute = {
            id: 1,
            name: 'name_one',
            label: 'label_one',
            form_label: 'form_label_one',
            description: 'description_one',
            primary_key: true,
            type: 'integer',
            search_type: 'field',
            operator: '=',
            dynamic_operator: false,
            min: null,
            max: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 2,
            output_display: 2,
            selected: true,
            renderer: null,
            renderer_config: {
                id: 'renderer-config',
                href: 'href',
                display: 'display',
                text: 'This is $value',
                icon: 'icon',
                blank: true
            } as LinkRendererConfig,
            order_by: true,
            archive: false,
            detail_display: 2,
            detail_renderer: null,
            detail_renderer_config: null,
            options: [
                { label: 'Three', value: 'three', display: 3 },
                { label: 'One', value: 'one', display: 1 },
                { label: 'Two', value: 'two', display: 2 }
            ],
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: 2,
            id_detail_output_category: null
        };

        component.rendererType = 'result';
        component.attribute = attribute;
        component.value = 'myId';
        expect(component.getText()).toEqual('This is myId');
    });
});
