/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalModule } from 'ngx-bootstrap/modal';

import { ImageRendererComponent } from './image-renderer.component';
import { AppConfigService } from 'src/app/app-config.service';
import { ImageRendererConfig } from 'src/app/metamodel/models';

describe('[Instance][Search][Component][Result][Renderer] ImageRendererComponent', () => {
    let component: ImageRendererComponent;
    let fixture: ComponentFixture<ImageRendererComponent>;
    let appConfigServiceStub = new AppConfigService();

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ImageRendererComponent],
            imports: [ModalModule.forRoot()],
            providers: [{ provide: AppConfigService, useValue: appConfigServiceStub }]
        });
        fixture = TestBed.createComponent(ImageRendererComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return image url', () => {
        const attribute = {
            id: 1,
            name: 'name_one',
            label: 'label_one',
            form_label: 'form_label_one',
            description: 'description_one',
            primary_key: true,
            type: 'integer',
            search_type: 'field',
            operator: '=',
            dynamic_operator: false,
            min: null,
            max: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 2,
            output_display: 2,
            selected: true,
            renderer: null,
            renderer_config: {
                id: 'renderer-config',
                display: 'display',
                type: 'type',
                width: 'width',
                height: 'height'
            } as ImageRendererConfig,
            order_by: true,
            archive: false,
            detail_display: 2,
            detail_renderer: null,
            detail_renderer_config: null,
            options: [
                { label: 'Three', value: 'three', display: 3 },
                { label: 'One', value: 'one', display: 1 },
                { label: 'Two', value: 'two', display: 2 }
            ],
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: 2,
            id_detail_output_category: null
        };

        const dataset = {
            name: 'myDataset',
            table_ref: 'table',
            label: 'my dataset',
            description: 'This is my dataset',
            display: 1,
            data_path: 'path',
            public: true,
            download_json: true,
            download_csv: true,
            download_ascii: true,
            download_vo: true,
            download_fits: true,
            server_link_enabled: true,
            datatable_enabled: true,
            datatable_selectable_rows: true,
            cone_search_config_id: 1,
            id_database: 1,
            id_dataset_family: 1,
            full_data_path: '/data/path'
        };

        component.rendererType = 'result';
        component.value = 'myObjId';
        component.attribute = attribute;
        component.dataset = dataset;
        appConfigServiceStub.apiUrl = 'https://test.com';
        expect(component.getValue()).toEqual('myObjId');
        (component.attribute.renderer_config as ImageRendererConfig).type = 'fits';
        appConfigServiceStub.servicesUrl = 'https://services.com';
        const expectedValue = 'https://services.com/fits-to-png/myDataset?filename=/myObjId&stretch=linear&pmin=0.25&pmax=99.75&axes=true';
        expect(component.getValue()).toEqual(expectedValue);
    });
});
