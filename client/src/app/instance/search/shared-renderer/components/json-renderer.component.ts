/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, TemplateRef } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AbstractRendererComponent } from '../abstract-renderer.component';

/**
 * @class
 * @classdesc JSON renderer component.
 */
@Component({
    selector: 'app-json-renderer',
    templateUrl: 'json-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class JsonRendererComponent extends AbstractRendererComponent {
    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) {
        super();
    }

    getConfig() {
        return super.getConfig();
    }

    /**
     * Opens modal.
     *
     * @param  {TemplateRef<any>} template - The modal template to open.
     */
    openModal(template: TemplateRef<any>): void {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'modal-fit-content' })
        );
    }

    isEmpty() {
        if (!this.value || Object.keys(this.value).length === 0) {
            return false;
        } else {
            return true;
        }
    }
}
