/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { LinkRendererConfig } from 'src/app/metamodel/models/renderers/link-renderer-config.model';
import { AbstractRendererComponent } from '../abstract-renderer.component';

/**
 * @class
 * @classdesc Link renderer component.
 */
@Component({
    selector: 'app-link-renderer',
    templateUrl: 'link-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinkRendererComponent extends AbstractRendererComponent {
    getConfig() {
        return super.getConfig() as LinkRendererConfig;
    }

    /**
     * Returns config href.
     *
     * @return string
     */
    getValue(): string {
        return this.getConfig().href.replace('$value', this.value.toString());
    }

    /**
     * Returns config text.
     *
     * @return string
     */
    getText(): string {
        return this.getConfig().text.replace('$value', this.value.toString());
    }
}
