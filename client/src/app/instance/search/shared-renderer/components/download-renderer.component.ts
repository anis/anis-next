/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

import { DownloadRendererConfig } from 'src/app/metamodel/models/renderers/download-renderer-config.model';
import { getHost } from 'src/app/shared/utils';
import { AppConfigService } from 'src/app/app-config.service';
import { AbstractRendererComponent } from '../abstract-renderer.component';

/**
 * @class
 * @classdesc Download renderer component.
 */
@Component({
    selector: 'app-download-renderer',
    templateUrl: 'download-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DownloadRendererComponent extends AbstractRendererComponent {
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();

    constructor(private appConfig: AppConfigService) {
        super();
    }

    getConfig() {
        return super.getConfig() as DownloadRendererConfig;
    }

    /**
     * Returns link href.
     *
     * @return string
     */
    getHref(): string {
        let path = this.value;
        if (path[0] !== '/') {
            path = '/' + path;
        }
        return `${getHost(this.appConfig.apiUrl)}/dataset/${this.dataset.name}/file-explorer${path}`;
    }

    /**
     * Returns config text.
     *
     * @return string
     */
    getText(): string {
        return this.getConfig().text.replace('$value', this.value.toString());
    }

    getNgStyle() {
        let style = '';
        if (this.getConfig().display=='text-button' 
            || this.getConfig().display=='icon-button' 
            || this.getConfig().display=='icon-text-btn'
        ) {
            style += 'btn btn-outline-primary btn-sm';
        }
        if (this.getConfig().display=='text-button'
            || this.getConfig().display=='icon-text-btn'
        ) {
            style += ' btn-block';
        }
        return style;
    }

    /**
     * Starts downloading file on click.
     */
    click(event): void {
        event.preventDefault();
        
        const url = this.getHref();
        const filename = url.substring(url.lastIndexOf('/') + 1);

        this.downloadFile.emit({ url, filename });
    }
}
