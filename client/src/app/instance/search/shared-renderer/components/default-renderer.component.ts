/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { AbstractRendererComponent } from '../abstract-renderer.component';

/**
 * @class
 * @classdesc JSON renderer component.
 */
@Component({
    selector: 'app-default-renderer',
    templateUrl: 'default-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultRendererComponent extends AbstractRendererComponent {
    getConfig() {
        return super.getConfig();
    }
}
