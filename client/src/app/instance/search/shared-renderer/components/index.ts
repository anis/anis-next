/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Type } from '@angular/core';

import { AbstractRendererComponent } from '../abstract-renderer.component';
import { DefaultRendererComponent } from './default-renderer.component';
import { LinkRendererComponent } from './link-renderer.component';
import { DownloadRendererComponent } from './download-renderer.component';
import { ImageRendererComponent } from './image-renderer.component';
import { JsonRendererComponent } from './json-renderer.component';

export * from './default-renderer.component';
export * from './link-renderer.component';
export * from './download-renderer.component';
export * from './image-renderer.component';
export * from './json-renderer.component';

export const sharedRendererComponents = [
    DefaultRendererComponent,
    LinkRendererComponent,
    DownloadRendererComponent,
    ImageRendererComponent,
    JsonRendererComponent
];

export const getRendererComponent = (renderer: string): Type<AbstractRendererComponent> => {
    let nameOfRendererComponent = null;
    switch(renderer) {
        case 'link': {
            nameOfRendererComponent = LinkRendererComponent;
            break;
        }
        case 'download': {
            nameOfRendererComponent = DownloadRendererComponent;
            break;
        }
        case 'image': {
            nameOfRendererComponent = ImageRendererComponent;
            break;
        }
        case 'json': {
            nameOfRendererComponent = JsonRendererComponent;
            break;
        }
        default: {
            nameOfRendererComponent = DefaultRendererComponent;
            break;
        }
    }
    return nameOfRendererComponent;
}
