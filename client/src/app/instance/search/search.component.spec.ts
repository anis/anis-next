/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { SearchComponent } from './search.component';
import { Instance } from '../../metamodel/models';
import { SearchQueryParams } from '../store/models';

describe('[Instance][Search] SearchComponent', () => {
    @Component({ selector: 'app-progress-bar', template: '' })
    class ProgressBarStubComponent {
        @Input() currentStep: string;
        @Input() instance: Instance;
        @Input() datasetSelected: string;
        @Input() criteriaStepChecked: boolean;
        @Input() outputStepChecked: boolean;
        @Input() resultStepChecked: boolean;
        @Input() queryParams: SearchQueryParams;
        @Input() outputList: number[];
    }

    let component: SearchComponent;
    let fixture: ComponentFixture<SearchComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                SearchComponent,
                ProgressBarStubComponent
            ],
            providers: [provideMockStore({ })]
        }).compileComponents();
        fixture = TestBed.createComponent(SearchComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });
});
