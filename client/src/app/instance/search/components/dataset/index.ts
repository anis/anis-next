/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetTabsComponent } from './dataset-tabs.component';
import { DatasetCardComponent } from './dataset-card.component';

export const datasetComponents = [
    DatasetTabsComponent,
    DatasetCardComponent
];
