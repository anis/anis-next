/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressBarComponent } from './progress-bar.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('[Instance][Search][Component] ProgressBarComponent', () => {
    let component: ProgressBarComponent;
    let fixture: ComponentFixture<ProgressBarComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ProgressBarComponent],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(ProgressBarComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getStepClass() should return correct step class', () => {
        let style = component.getStepClass();
        expect(style).toBe('datasetStep');
        component.currentStep = 'dataset';
        style = component.getStepClass();
        expect(style).toBe('datasetStep');
        component.currentStep = 'criteria';
        style = component.getStepClass();
        expect(style).toBe('criteriaStep');
        component.currentStep = 'output';
        style = component.getStepClass();
        expect(style).toBe('outputStep');
        component.currentStep = 'result';
        style = component.getStepClass();
        expect(style).toBe('resultStep');
    });
});
