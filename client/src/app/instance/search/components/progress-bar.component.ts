/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { SearchQueryParams } from '../../store/models';
import { Instance } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Search progress bar component.
 */
@Component({
    selector: 'app-progress-bar',
    templateUrl: 'progress-bar.component.html',
    styleUrls: ['progress-bar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarComponent {
    @Input() currentStep: string;
    @Input() instance: Instance;
    @Input() datasetSelected: string;
    @Input() criteriaStepChecked: boolean;
    @Input() outputStepChecked: boolean;
    @Input() resultStepChecked: boolean;
    @Input() queryParams: SearchQueryParams;
    @Input() outputList: number[];

    /**
     * Returns step class that match to the current step.
     *
     * @return string
     */
    getStepClass(): string {
        switch (this.currentStep) {
            case 'dataset':
                return 'datasetStep';
            case 'criteria':
                return 'criteriaStep';
            case 'output':
                return 'outputStep';
            case 'result':
                return 'resultStep';
            default:
                return 'datasetStep';
        }
    }
}
