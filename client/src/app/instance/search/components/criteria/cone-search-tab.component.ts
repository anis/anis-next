/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { ConeSearchConfig, Dataset } from 'src/app/metamodel/models';
import { ConeSearch, coneSearchToString } from 'src/app/instance/store/models';

/**
 * @class
 * @classdesc Search cone search tab component.
 */
@Component({
    selector: 'app-cone-search-tab',
    templateUrl: 'cone-search-tab.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchTabComponent {
    @Input() datasetSelected: string;
    @Input() datasetList: Dataset[];
    @Input() coneSearch: ConeSearch;
    @Input() coneSearchConfig: ConeSearchConfig;
    @Input() resolverIsLoading: boolean;
    @Input() resolverIsLoaded: boolean;
    @Output() addConeSearch: EventEmitter<ConeSearch> = new EventEmitter();
    @Output() updateConeSearch: EventEmitter<ConeSearch> = new EventEmitter();
    @Output() deleteConeSearch: EventEmitter<{ }> = new EventEmitter();
    @Output() retrieveCoordinates: EventEmitter<string> = new EventEmitter();

    /**
     * Emits event to add or update cone-search.
     *
     * @fires EventEmitter<ConeSearch>
     */
    emitAdd(newConeSearch: ConeSearch): void {
        if (!this.coneSearch) {
            this.addConeSearch.emit(newConeSearch);
        }
        if (this.coneSearch && coneSearchToString(this.coneSearch) !== coneSearchToString(newConeSearch)) {
            this.updateConeSearch.emit(newConeSearch);
        }
    }
}
