/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SearchCriteriaListComponent } from './search-criteria-list.component';
import { ConeSearchTabComponent } from './cone-search-tab.component';
import { CriteriaTabsComponent } from './criteria-tabs.component';
import { CriteriaByFamilyComponent } from './criteria-by-family.component';
import { CriterionComponent } from './criterion.component';
import { searchTypeComponents } from './search-type';

export const criteriaComponents = [
    SearchCriteriaListComponent,
    ConeSearchTabComponent,
    CriteriaTabsComponent,
    CriteriaByFamilyComponent,
    CriterionComponent,
    searchTypeComponents
];
