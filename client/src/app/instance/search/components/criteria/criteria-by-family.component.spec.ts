/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteriaByFamilyComponent } from './criteria-by-family.component';
import { Option } from '../../../../metamodel/models';
import { Criterion, FieldCriterion, SvomKeyword } from '../../../store/models';

describe('[Instance][Search][Component][Criteria] CriteriaByFamilyComponent', () => {
    @Component({ selector: 'app-field', template: '' })
    class FieldStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() placeholder: string;
        @Input() attributeType: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
    }

    @Component({ selector: 'app-between', template: '' })
    class BetweenStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() placeholderMin: string;
        @Input() placeholderMax: string;
        @Input() criterion: Criterion;
    }

    @Component({ selector: 'app-select', template: '' })
    class SelectStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
    }

    @Component({ selector: 'app-select-multiple', template: '' })
    class SelectMultipleStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
    }

    @Component({ selector: 'app-datalist', template: '' })
    class DatalistStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() placeholder: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
    }

    @Component({ selector: 'app-list', template: '' })
    class ListStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() placeholder: string;
        @Input() criterion: Criterion;
    }

    @Component({ selector: 'app-radio', template: '' })
    class RadioStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
    }

    @Component({ selector: 'app-checkbox', template: '' })
    class CheckboxStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
    }

    @Component({ selector: 'app-date', template: '' })
    class DateStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() placeholder: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
    }

    @Component({ selector: 'app-between-date', template: '' })
    class BetweenDateStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() criterion: Criterion;
    }

    @Component({ selector: 'app-time', template: '' })
    class TimeStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
    }

    @Component({ selector: 'app-datetime', template: '' })
    class DatetimeStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
    }

    @Component({ selector: 'app-json-criteria', template: '' })
    class JsonStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() criterion: Criterion;
    }

    @Component({ selector: 'app-svom-json-kw-criteria', template: '' })
    class SvomJsonStubKwComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() criterion: Criterion;
        @Input() criteriaList: Criterion[];
        @Input() svomKeywords: SvomKeyword[];
    }

    let component: CriteriaByFamilyComponent;
    let fixture: ComponentFixture<CriteriaByFamilyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaByFamilyComponent,
                FieldStubComponent,
                BetweenStubComponent,
                SelectStubComponent,
                SelectMultipleStubComponent,
                DatalistStubComponent,
                ListStubComponent,
                RadioStubComponent,
                CheckboxStubComponent,
                DateStubComponent,
                BetweenDateStubComponent,
                TimeStubComponent,
                DatetimeStubComponent,
                JsonStubComponent,
                SvomJsonStubKwComponent
            ]
        });
        fixture = TestBed.createComponent(CriteriaByFamilyComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getOptions(attribute) should return options of attribute', () => {
        component.attributeList = [
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: [
                    { label: 'Three', value: 'three', display: 3 },
                    { label: 'One', value: 'one', display: 1 },
                    { label: 'Two', value: 'two', display: 2 }
                ],
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: null,
                id_detail_output_category: null
            }
        ];
        const attributeId = 1;
        const options: Option[] = component.getOptions(attributeId);
        expect(options.length).toBe(3)
    });

    it('#getCriterion(criterionId) should return correct criterion', () => {
        component.criteriaList = [
            { 'id': 1, 'type': 'field', 'operator': 'eq', 'value': 'one' } as FieldCriterion,
            { 'id': 2, 'type': 'field', 'operator': 'eq', 'value': 'two' } as FieldCriterion
        ];
        const criterionId = 1;
        const criterion = component.getCriterion(criterionId) as FieldCriterion;
        expect(criterion.value).toBe('one');
    });

    it('raises the add criterion event when clicked', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: 'test' } as FieldCriterion;
        component.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(criterion));
        component.emitAdd(criterion);
    });
    it('raises the emitUpdate criterion event when clicked', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: 'test' } as FieldCriterion;
        let spy = jest.spyOn(component.updateCriterion, 'emit');
        component.emitUpdate(criterion);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criterion);
    });

    it('raises the delete criterion event when clicked', () => {
        const criterionId = 1;
        component.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        component.emitDelete(criterionId);
    });
});
