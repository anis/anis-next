/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ViewChild, SimpleChanges, OnInit, OnChanges, ChangeDetectionStrategy } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';
import { Criterion, FieldCriterion } from 'src/app/instance/store/models';
import { SearchTypeLoaderDirective, AbstractSearchTypeComponent, getSearchTypeComponent } from './search-type';
import { criterionToString } from 'src/app/instance/store/models';

@Component({
    selector: 'app-criterion',
    templateUrl: 'criterion.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriterionComponent implements OnInit, OnChanges {
    @Input() datasetSelected: string;
    @Input() attribute: Attribute;
    @Input() criterion: Criterion;
    @Input() criteriaList: Criterion[];
    @Output() addCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() updateCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    @ViewChild(SearchTypeLoaderDirective, {static: true}) SearchTypeLoaderDirective!: SearchTypeLoaderDirective;

    public searchTypeComponent: AbstractSearchTypeComponent;

    ngOnInit() {
        const viewContainerRef = this.SearchTypeLoaderDirective.viewContainerRef;
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent<AbstractSearchTypeComponent>(
            getSearchTypeComponent(this.attribute.search_type)
        );
        componentRef.instance.setDatasetSelected(this.datasetSelected);
        componentRef.instance.setAttribute(this.attribute);
        componentRef.instance.setCriterion(this.criterion);
        componentRef.instance.setCriteriaList(this.criteriaList);
        componentRef.instance.emitAdd.subscribe(() => this.emitAdd());
        componentRef.instance.emitDelete.subscribe(() => {
            if (this.criterion) {
                this.deleteCriterion.emit(this.criterion.id)
            }
        });
        this.searchTypeComponent = componentRef.instance;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.criterion && !changes.criterion.firstChange) {
            this.searchTypeComponent.setCriterion(changes.criterion.currentValue);
        }

        if (changes.criteriaList && !changes.criteriaList.firstChange) {
            this.searchTypeComponent.setCriteriaList(changes.criteriaList.currentValue);
        }
    }

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<Criterion>
     */
    emitAdd(): void {
        let criterion: Criterion;
        if (this.searchTypeComponent.nullOrNotNull) {
            criterion = {
                id: this.attribute.id,
                type: 'field',
                operator: this.searchTypeComponent.nullOrNotNull
            } as FieldCriterion;
        } else {
            criterion = this.searchTypeComponent.getCriterion();
        }

        const existingCriterion = this.criteriaList.find(c => c.id === criterion.id)
        if (existingCriterion) {
            if (criterionToString(existingCriterion) !== criterionToString(criterion)) {
                this.updateCriterion.emit(criterion);
            }
        } else {
            this.addCriterion.emit(criterion);
        }
    }
}
