/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Criterion, SvomKeyword } from '../../../store/models';
import { CriteriaFamily, Attribute } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Search criteria tabs component.
 */
@Component({
    selector: 'app-criteria-tabs',
    templateUrl: 'criteria-tabs.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriteriaTabsComponent {
    @Input() datasetSelected: string;
    @Input() attributeList: Attribute[];
    @Input() criteriaFamilyList: CriteriaFamily[];
    @Input() criteriaList: Criterion[];
    @Output() addCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() updateCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    getCriteriaFamilyList() {
        return this.criteriaFamilyList
            .filter(criteriaFamily => this.getAttributeListByCriteriaFamily(criteriaFamily).length > 0);
    }

    getAttributeListByCriteriaFamily(criteriaFamily: CriteriaFamily) {
        return this.attributeList.filter(attribute => attribute.id_criteria_family === criteriaFamily.id);
    }

    /**
     * Emits event to add the given criterion to the criteria list.
     *
     * @param  {Criterion} criterion - The criterion.
     *
     * @fires EventEmitter<Criterion>
     */
    emitAdd(criterion: Criterion): void {
        this.addCriterion.emit(criterion);
    }

    /**
     * Emits event to update the given criterion to the criteria list.
     *
     * @param  {Criterion} updatedCriterion - The updated criterion.
     *
     * @fires EventEmitter<Criterion>
     */
    emitUpdate(updatedCriterion: Criterion): void {
        this.updateCriterion.emit(updatedCriterion);
    }

    /**
     * Emits event to remove the given criterion ID to the criteria list.
     *
     * @param  {number} id - The criterion ID.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(id: number): void {
        this.deleteCriterion.emit(id);
    }
}
