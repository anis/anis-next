/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ConeSearch, Criterion, SearchQueryParams } from 'src/app/instance/store/models';
import { Attribute, Dataset, Instance } from 'src/app/metamodel/models';

@Component({
    selector: 'app-search-criteria-list',
    templateUrl: 'search-criteria-list.component.html',
    styleUrls: ['search-criteria-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchCriteriaListComponent {
    @Input() instance: Instance;
    @Input() coneSearch: ConeSearch;
    @Input() criteriaList: Criterion[];
    @Input() attributeList: Attribute[];
    @Input() dataset: Dataset;
    @Input() datasetSelected: String;
    @Input() queryParams: SearchQueryParams;
}
