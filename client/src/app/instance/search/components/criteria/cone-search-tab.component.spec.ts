/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ConeSearchTabComponent } from './cone-search-tab.component';
import { ConeSearch } from '../../../store/models';
import { DatasetByNamePipe } from '../../../../shared/pipes/dataset-by-name.pipe';

describe('[Instance][Search][Components][Criteria] ConeSearchTabComponent', () => {
    @Component({ selector: 'app-cone-search', template: '' })
    class ConeSearchStubComponent {
        @Input() coneSearch: ConeSearch;
        @Input() resolverIsLoading: boolean;
        @Input() resolverIsLoaded: boolean;
    }

    let component: ConeSearchTabComponent;
    let fixture: ComponentFixture<ConeSearchTabComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchTabComponent,
                ConeSearchStubComponent,
                DatasetByNamePipe
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(ConeSearchTabComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('should raises addConeSearch event with the newConeSearch', () => {
        let newConeSearch: ConeSearch = { dec: 10, ra: 5, radius: 10 };
        const spy = jest.spyOn(component.addConeSearch, 'emit');
        component.emitAdd(newConeSearch);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(newConeSearch);
    });
    it('should raises  updateConeSearch with the newConeSearch', () => {
        let newConeSearch: ConeSearch = { dec: 10, ra: 5, radius: 10 };
        component.coneSearch = { dec: 5, ra: 2, radius: 5 }
        const spy = jest.spyOn(component.updateConeSearch, 'emit');
        component.emitAdd(newConeSearch);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(newConeSearch);
    })
});
