/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Criterion } from 'src/app/instance/store/models';
import { Attribute, Option } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Search criteria by family component.
 */
@Component({
    selector: 'app-criteria-by-family',
    templateUrl: 'criteria-by-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriteriaByFamilyComponent {
    @Input() datasetSelected: string;
    @Input() attributeList: Attribute[];
    @Input() criteriaList: Criterion[];
    @Output() addCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() updateCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    /**
     * Returns options for the given attribute ID.
     *
     * @param  {number} idAttribute - The attribute ID.
     *
     * @return Option[]
     */
    getOptions(idAttribute: number): Option[] {
        return [...this.attributeList.find(attribute => attribute.id === idAttribute).options];
    }

    /**
     * Returns criterion for the given criterion ID.
     *
     * @param  {number} id - The criterion ID.
     *
     * @return Criterion
     */
    getCriterion(id: number): Criterion {
        return this.criteriaList.find(criterion => criterion.id === id);
    }

    /**
     * Emits event to add the given criterion to the criteria list.
     *
     * @param  {Criterion} criterion - The criterion.
     *
     * @fires EventEmitter<Criterion>
     */
    emitAdd(criterion: Criterion): void {
        this.addCriterion.emit(criterion);
    }

    /**
     * Emits event to update the given criterion to the criteria list.
     *
     * @param  {Criterion} updatedCriterion - The updated criterion.
     *
     * @fires EventEmitter<Criterion>
     */
    emitUpdate(updatedCriterion: Criterion): void {
        this.updateCriterion.emit(updatedCriterion);
    }

    /**
     * Emits event to remove the given criterion ID from the criteria list.
     *
     * @param  {number} id - The criterion ID.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(id: number): void {
        this.deleteCriterion.emit(id);
    }
}
