/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, BetweenCriterion, FieldCriterion } from 'src/app/instance/store/models';

@Component({
    selector: 'app-between-date',
    templateUrl: 'between-date.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BetweenDateComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'bw', label: 'between' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];

    constructor() {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl(),
            dateRange: new UntypedFormControl('', [Validators.required])
        });

    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'between') {
                const betweenCriterion = criterion as BetweenCriterion;
                this.form.controls.dateRange.setValue([
                    new Date(betweenCriterion.min),
                    new Date(betweenCriterion.max)
                ]);
                this.form.controls.label.setValue('bw');
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls.label.setValue(fieldCriterion.operator);
            }
        } else {
            this.form.controls.label.setValue('bw');
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        const dateMin = this.getDateString(this.form.controls.dateRange.value[0]);
        const dateMax = this.getDateString(this.form.controls.dateRange.value[1]);

        return {
            id: this.attribute.id,
            type: 'between',
            min: dateMin,
            max: dateMax
        } as BetweenCriterion;
    }

    /**
     * Stringifies the given date.
     *
     * @param  {Date} date - The date.
     *
     * @return string
     */
    getDateString(date: Date): string {
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + (date.getDate())).slice(-2);
        return `${date.getFullYear()}-${month}-${day}`;
    }

    labelOnChange() {
        if (this.form.controls.label.value === 'nl' || this.form.controls.label.value === 'nnl') {
            this.nullOrNotNull = this.form.controls.label.value;
            this.form.controls.dateRange.disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls.dateRange.enable();
        }
    }
}
