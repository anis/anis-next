/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Attribute } from 'src/app/metamodel/models';
import { DatalistComponent } from './datalist.component';

describe('[Instance][search][components][criteria][search-type] DatalistComponent', () => {
    let component: DatalistComponent;
    let fixture: ComponentFixture<DatalistComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DatalistComponent],
            imports: [ReactiveFormsModule]
        });
        fixture = TestBed.createComponent(DatalistComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should call operatorOnchange method when criterion param is defined', () => {
        const spy = jest.spyOn(component, 'operatorOnChange');
        component.setCriterion({ id: 1, type: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('setCriterion(criterion: Criterion) should set test to form.operator value when criterion param is undefined', () => {
        expect(component.form.controls.operator.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.operator.value).toEqual('test');
    });
    it('getCriterion() should return a criterion of type FieldCriterion', () => {
        expect(component.getCriterion().type).toEqual('field');
    });
    it('isValid() should true when form is valid or when operator value is nl or nnl', () => {
        expect(component.isValid()).toBe(false);
        component.form.controls.operator.setValue('test');
        component.form.controls.value.setValue('test');
        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nnl');
        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nl');
        expect(component.isValid()).toBe(true);
    });
    it('getPlaceholder() should return "" when attribute.placeholder is undefined', () => {
        component.attribute.placeholder_min = null;
        expect(component.getPlaceholder()).toEqual('');
    })
    it('getPlaceholder() should return min ', () => {
        expect(component.getPlaceholder()).toEqual('min');
    });
    it('operatorOnChange() should disable value formcontrol when operator value is nl or nnl', () => {
        expect(component.form.controls.value.disabled).toBe(false);
        component.form.controls.operator.setValue('nl');
        component.operatorOnChange();
        expect(component.form.controls.value.disabled).toBe(true);

    });
    it('operatorOnChange() should enable value formcontrol when operator value is not  nl or nnl', () => {
        component.form.controls.value.disable();
        expect(component.form.controls.value.enabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls.value.enabled).toBe(true);

    });

});
