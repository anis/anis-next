/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormArray, UntypedFormControl } from '@angular/forms';
import { FieldCriterion, SelectMultipleCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { CheckboxComponent } from './checkbox.component';

describe('[Instance][search][components][criteria][search-type] CheckboxComponent', () => {
    let component: CheckboxComponent;
    let fixture: ComponentFixture<CheckboxComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CheckboxComponent],
            imports: [ReactiveFormsModule]
        });
        fixture = TestBed.createComponent(CheckboxComponent);
        component = fixture.componentInstance;
        component.form.addControl('checkboxes', new UntypedFormArray([]));
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ]
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setAttribute(attribute: Attribute) should add a checkboxes with attributes options ', () => {
        let attributeParam = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ]
        }
        component.form.removeControl('checkboxes');
        component.setAttribute(attributeParam);
        fixture.detectChanges()
        expect(component.form.controls.checkboxes.value.length).toEqual(2);
    });
    it('setCriterion(criterion: Criterion) should set label value to in and test1 option  to true', () => {
        let spy = jest.spyOn(component.form.controls.label, 'setValue');
        let criterion: SelectMultipleCriterion = { id: 1, type: 'multiple', options: [{ label: 'test1', display: 1, value: 'test1' }] };

        component.form.removeControl('checkboxes');
        component.form.addControl('checkboxes', new UntypedFormArray([
            new UntypedFormControl(),
        ]));
        component.setCriterion(criterion);
        expect(spy).toHaveBeenCalledWith('in');
        expect((component.form.controls.checkboxes as UntypedFormArray).controls[0].value).toBe(true);
    });
    it('setCriterion(criterion: Criterion) should set label value to test', () => {
        let spy = jest.spyOn(component.form.controls.label, 'setValue');
        let criterion: FieldCriterion = { id: 1, type: 'test', operator: 'test', value: null };
        component.setCriterion(criterion);
        expect(spy).toHaveBeenCalledWith('test');
    });
    it('setCriterion(criterion: Criterion) should set label value to in when criterion param is undefined', () => {
        let spy = jest.spyOn(component.form.controls.label, 'setValue');
        component.setCriterion(undefined);
        expect(spy).toHaveBeenCalledWith('in');
    });
    it('getCriterion() should return a criterion of type multiple', () => {

        expect(component.getCriterion().type).toEqual('multiple');
    });
    it('isValid() should return true when values length is more then 0', () => {
        component.form.removeControl('checkboxes');
        component.form.addControl('checkboxes', new UntypedFormArray([
            new UntypedFormControl('test'),
            new UntypedFormControl('test2')
        ]));
        expect(component.isValid()).toBe(true);
    });
    it('isValid() should return true when  label value is nl or nnl', () => {
        component.form.controls.label.setValue('nl')
        expect(component.isValid()).toBe(true);
        component.form.controls.label.setValue('nnl')
        expect(component.isValid()).toBe(true);
    });
    it('isChecked() should return true when at least one option is checked ', () => {

        component.form.removeControl('checkboxes');
        component.form.addControl('checkboxes', new UntypedFormArray([
            new UntypedFormControl('test'),
        ]));
        expect(component.isChecked()).toBe(true);
    });
    it('labelOnChange() should disable checkbox when label value is nl or nnl', () => {

        component.form.removeControl('checkboxes');
        component.form.addControl('checkboxes', new UntypedFormArray([
            new UntypedFormControl('test'),
        ]));
        let checkboxes = component.form.controls.checkboxes as UntypedFormArray
        expect(checkboxes.controls.find(value => value.disabled)).toBeUndefined();
        component.form.controls.label.setValue('nl');
        component.labelOnChange();
        expect(checkboxes.controls.find(value => value.disabled)).toBeDefined();
        component.form.controls.label.setValue('nnl');
        component.labelOnChange();
        expect(checkboxes.controls.find(value => value.disabled)).toBeDefined();


    });

    it('labelOnChange() should enable  checkboxes options when label value is not nl or nnl', () => {
        component.form.removeControl('checkboxes');
        component.form.addControl('checkboxes', new UntypedFormArray([
            new UntypedFormControl('test'),
        ]));
        let checkboxes = component.form.controls.checkboxes as UntypedFormArray
        checkboxes.controls.forEach(value => value.disable());
        expect(checkboxes.controls.find(value => value.enabled)).toBeUndefined();
        component.labelOnChange();
        expect(checkboxes.controls.find(value => value.enabled)).toBeDefined();


    });

});
