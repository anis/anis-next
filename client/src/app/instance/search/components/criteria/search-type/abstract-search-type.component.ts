/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Directive, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

import { Attribute } from 'src/app/metamodel/models';
import { Criterion } from 'src/app/instance/store/models';
import { searchTypeOperators } from 'src/app/shared/utils';
import { debounceTime, Subscription } from 'rxjs';

@Directive()
export abstract class AbstractSearchTypeComponent implements OnInit, OnDestroy {
    datasetSelected: string;
    attribute: Attribute;
    criteriaList: Criterion[];
    emitAdd: EventEmitter<{}> = new EventEmitter<{}>();
    emitDelete: EventEmitter<{}> = new EventEmitter<{}>();

    form: UntypedFormGroup;
    formValueChangesSubscription: Subscription;
    operators = searchTypeOperators;
    nullOrNotNull: string = '';

    constructor() { }

    ngOnInit() {
        this.formValueChangesSubscription = this.form.valueChanges.pipe(
            debounceTime(300),
        )
        .subscribe(() => {
            if (this.isValid()) {
                this.emitAdd.emit();
            } else {
                this.emitDelete.emit();
            }
        });
    }

    setDatasetSelected(datasetSelected: string) {
        this.datasetSelected = datasetSelected;
    }

    setAttribute(attribute: Attribute) {
        this.attribute = attribute;
    }

    setCriterion(criterion: Criterion) {
        if (criterion) {
            this.form.patchValue(criterion);
        } else {
            this.form.reset();
            this.enable();
            this.nullOrNotNull = '';
        }
    }

    setCriteriaList(criteriaList: Criterion[]) {
        this.criteriaList = criteriaList;
    }

    abstract getCriterion(): Criterion;

    isValid() {
        return this.form.valid;
    }

    disable() {
        this.form.disable();
    }

    enable() {
        this.form.enable();
    }

    /**
     * Return field type.
     *
     * @return string
     */
    getType(): string {
        const numberTypeList = ['smallint', 'integer', 'decimal', 'float'];
        if (this.attribute.operator === 'in' || this.attribute.operator === 'nin' || !numberTypeList.includes(this.attribute.type)) {
            return 'text';
        } else {
            return 'number';
        }
    }

    ngOnDestroy(): void {
        this.formValueChangesSubscription.unsubscribe();
    }
}
