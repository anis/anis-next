/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Observable, Subject, concat, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap, tap, filter } from 'rxjs/operators';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, FieldCriterion, SelectMultipleCriterion } from 'src/app/instance/store/models';
import { AppConfigService } from 'src/app/app-config.service';
import { Alias } from 'src/app/instance/store/models';

@Component({
    selector: 'app-select-multiple-alias',
    templateUrl: 'select-multiple-alias.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectMultipleAliasComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'in', label: 'in' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];

    aliases: Observable<Alias[]>;
    aliasesLoading = false;
    aliasInput$ = new Subject<string>();

    constructor(private http: HttpClient, private config: AppConfigService) {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl(''),
            select: new UntypedFormControl('', [Validators.required])
        });
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.loadAliases();
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'multiple') {
                const multipleCriterion = criterion as SelectMultipleCriterion;
                const values = multipleCriterion.options.map(option => ({
                    name: option.value,
                    alias: option.value,
                    alias_long: option.value
                }));
                this.form.controls.select.setValue(values);
                this.form.controls.label.setValue('in');
                this.loadAliases();
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls.label.setValue(fieldCriterion.operator);
                this.labelOnChange();
            }
        } else {
            this.form.controls.label.setValue('in');
            this.loadAliases();
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        const values = this.form.value.select as Alias[];
        const options = values.map(alias => ({
            label: alias.name,
            value: alias.name,
            display: 10
        }));

        return {
            id: this.attribute.id,
            type: 'multiple',
            options
        } as SelectMultipleCriterion;
    }

    labelOnChange() {
        if (this.form.controls.label.value === 'nl' || this.form.controls.label.value === 'nnl') {
            this.nullOrNotNull = this.form.controls.label.value;
            this.form.controls.select.disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls.select.enable();
        }
    }

    trackByFn(item: Alias) {
        return item.name;
    }

    getSelectedObjects() {
        let getSelectedObjects = [];
        const aliases = this.form.value.select as Alias[];
        if (aliases) {
            getSelectedObjects = aliases.map(alias => alias.name);
        }
        return getSelectedObjects;
    }

    private loadAliases() {
        this.aliases = concat(
            of([]), // default items
            this.aliasInput$.pipe(
                distinctUntilChanged(),
                debounceTime(200),
                filter(term => !!term),
                tap(() => this.aliasesLoading = true),
                switchMap(term => this.getHttpAliases(term).pipe(
                    catchError(() => of([])), // empty list on error
                    tap(() => {
                        this.aliasesLoading = false
                    })
                ))
            )
        );
    }

    getHttpAliases(term: string = null): Observable<Alias[]> {
        const value = term.toLowerCase().replace(/[^a-z0-9+]/g, '');
        return this.http.get<Alias[]>(`${this.config.apiUrl}/dataset/${this.datasetSelected}/search-alias/${value}`).pipe(
            map(aliases => aliases.filter(alias => !this.getSelectedObjects().includes(alias.name)))
        );
    }
}
