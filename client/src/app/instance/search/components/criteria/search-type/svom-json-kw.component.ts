/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { AppConfigService } from 'src/app/app-config.service';
import { Criterion, FieldCriterion, JsonCriterion, SvomKeyword } from 'src/app/instance/store/models';

@Component({
    selector: 'app-svom-json-kw',
    templateUrl: 'svom-json-kw.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SvomJsonKwComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'js', label: 'json' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];
    svomKeywords: SvomKeyword[] = [];
    
    constructor(private changeDetectorRef: ChangeDetectorRef, private http: HttpClient, private config: AppConfigService) {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl(''),
            path: new UntypedFormControl('', [Validators.required]),
            operator: new UntypedFormControl('', [Validators.required]),
            value: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'json') {
                this.form.controls.label.setValue('js');
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls.label.setValue(fieldCriterion.operator);
                this.form.controls.operator.setValue('');
            }
        } else {
            this.form.controls.label.setValue('js');
        }
    }
    
    setCriteriaList(criteriaList: Criterion[]): void {
        super.setCriteriaList(criteriaList);
        if (this.svomKeywords.length < 1 && criteriaList.find((c: Criterion) => c.id === 3)) {
            const acronym = (criteriaList.find((c: Criterion) => c.id === 3) as FieldCriterion).value;
            this.http.get<{search_kw: SvomKeyword[]}[]>(`${this.config.apiUrl}/search/sp_cards?a=8&c=1::eq::${acronym}`).pipe(
                map(data => data[0].search_kw)
            ).subscribe(svomKeywords => {
                this.svomKeywords = svomKeywords;
                this.changeDetectorRef.detectChanges();
            });
        }

        if (this.svomKeywords.length > 0 && !criteriaList.find((c: Criterion) => c.id === 3)) {
            this.svomKeywords = [];
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        return {
            id: this.attribute.id,
            type: 'json',
            ...this.form.value
        } as JsonCriterion;
    }

    /**
     * Transform a SVOM json Keyword to as path value (anis json search)
     * 
     * @param svomKeyword Keyword selected by user
     * @returns string path value
     */
    getKeywordValue(svomKeyword: SvomKeyword): string {
        return `${svomKeyword.extension},${svomKeyword.name}`
    }

    labelOnChange() {
        if (this.form.controls.label.value === 'nl' || this.form.controls.label.value === 'nnl') {
            this.nullOrNotNull = this.form.controls.label.value;
            this.form.controls.operator.disable();
            this.form.controls.path.disable();
            this.form.controls.value.disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls.operator.enable();
            this.form.controls.path.enable();
            this.form.controls.value.enable();
        }
    }
}
