/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Observable, Subject, concat, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap, filter } from 'rxjs/operators';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, FieldCriterion } from 'src/app/instance/store/models';
import { searchTypeOperators } from 'src/app/shared/utils';
import { AppConfigService } from 'src/app/app-config.service';
import { Alias } from 'src/app/instance/store/models';

@Component({
    selector: 'app-select-alias',
    templateUrl: 'select-alias.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectAliasComponent extends AbstractSearchTypeComponent {
    aliases: Observable<Alias[]>;
    aliasesLoading = false;
    aliasInput$ = new Subject<string>();

    constructor(private http: HttpClient, private config: AppConfigService) {
        super();
        this.form = new UntypedFormGroup({
            operator: new UntypedFormControl(''),
            select: new UntypedFormControl('', [Validators.required])
        });
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.loadAliases();
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (this.form.controls.operator.value != 'nl' && this.form.controls.operator.value != 'nnl') {
                this.form.controls.select.setValue({
                    name: (criterion as FieldCriterion).value,
                    alias: (criterion as FieldCriterion).value,
                    alias_long: (criterion as FieldCriterion).value
                });
            }
            this.operatorOnChange();
        } else {
            this.form.controls.operator.setValue(this.attribute.operator);
            this.loadAliases();
        }
        if (!this.attribute.dynamic_operator) {
            this.operators = searchTypeOperators.filter(
                operator => [this.attribute.operator, 'nl', 'nnl'].includes(operator.value)
            );
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        let value = null;
        if (this.form.controls.operator.value != 'nl' && this.form.controls.operator.value != 'nnl') {
            const alias = this.form.value.select as Alias;
            value = alias.name;
        }
        return {
            id: this.attribute.id,
            type: 'field',
            operator: this.form.controls.operator.value,
            value
        } as FieldCriterion;
    }

    isValid(): boolean {
        return this.form.valid || this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl';
    }
    
    operatorOnChange() {
        if (this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl') {
            this.form.controls.select.disable();
        } else {
            this.form.controls.select.enable();
        }
    }

    trackByFn(item: Alias) {
        return item.name;
    }

    private loadAliases() {
        this.aliases = concat(
            of([]), // default items
            this.aliasInput$.pipe(
                distinctUntilChanged(),
                debounceTime(200),
                filter(term => !!term),
                tap(() => this.aliasesLoading = true),
                switchMap(term => this.getHttpAliases(term).pipe(
                    catchError(() => of([])), // empty list on error
                    tap(() => {
                        this.aliasesLoading = false
                    })
                ))
            )
        );
    }

    getHttpAliases(term: string = null): Observable<Alias[]> {
        const value = term.toLowerCase().replace(/[^a-z0-9+]/g, '');
        return this.http.get<Alias[]>(`${this.config.apiUrl}/dataset/${this.datasetSelected}/search-alias/${value}`);
    }
}
