/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Attribute } from 'src/app/metamodel/models';
import { DateTimeComponent } from './datetime.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FieldCriterion } from 'src/app/instance/store/models';


describe('[Instance][search][components][criteria][search-type] DateTimeComponent', () => {
    let component: DateTimeComponent;
    let fixture: ComponentFixture<DateTimeComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DateTimeComponent],
            imports: [
                ReactiveFormsModule,
                NgSelectModule
            ],
        });
        fixture = TestBed.createComponent(DateTimeComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should set operator value, date, hh, mm when a criterion is given in param', () => {

        let spyOnDate = jest.spyOn(component.form.controls.date, 'setValue');
        let spyOnHh = jest.spyOn(component.form.controls.hh, 'setValue');
        let spyOnMm = jest.spyOn(component.form.controls.mm, 'setValue');
        component.setCriterion({ id: 1, type: 'test', operator: 'test', value: '2022-12-03 1240' } as FieldCriterion);
        expect(spyOnDate).toHaveBeenCalledTimes(1);
        expect(spyOnHh).toHaveBeenCalledTimes(1);
        expect(spyOnMm).toHaveBeenCalledTimes(1);
    });
    it('setCriterion(criterion: Criterion) should set operator value to test', () => {
        expect(component.form.controls.operator.value).toEqual('')
        component.setCriterion(null);
        expect(component.form.controls.operator.value).toEqual('test');

    });
    it('getCriterion() should return a criterion of type FieldCriterion', () => {
        component.form.controls.date.setValue(new Date());
        expect(component.getCriterion().type).toEqual('field');
    });
    it('isValid() should true when form is valid or when operator value is nl or nnl', () => {
        expect(component.isValid()).toBe(false);
        component.form.controls.operator.setValue('test');
        component.form.controls.date.setValue(new Date());
        component.form.controls.hh.setValue('10');
        component.form.controls.mm.setValue('10');

        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nnl');
        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nl');
        expect(component.isValid()).toBe(true);
    });
    it('operatorOnChange() should disable date, hh, mm formcontrol when operator value is nl or nnl', () => {
        component.form.controls.operator.setValue('nl');
        expect(component.form.controls.date.disabled).toBe(false);
        expect(component.form.controls.hh.disabled).toBe(false);
        expect(component.form.controls.hh.disabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls.date.disabled).toBe(true);
        expect(component.form.controls.hh.disabled).toBe(true);
        expect(component.form.controls.hh.disabled).toBe(true);

    });
    it('operatorOnChange() should enable date, hh, mm formcontrol when operator value is not  nl or nnl', () => {
        component.form.controls.date.disable();
        expect(component.form.controls.date.enabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls.date.enabled).toBe(true);

    });

});
