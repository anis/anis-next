/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from "@angular/core/testing";
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from "@angular/forms";
import { Criterion } from "src/app/instance/store/models";
import { Attribute } from "src/app/metamodel/models";
import { AbstractSearchTypeComponent } from "./abstract-search-type.component";
class TestAbstractSearchTypeComponent extends AbstractSearchTypeComponent {
    getCriterion(): Criterion {
        return { id: 1, type: 'test' }
    }
}
describe('[instance][search][components][criteria][search-type] AbstractSearchTypeComponent', () => {
    let component: TestAbstractSearchTypeComponent;
    let attribute: Attribute;
    TestBed.configureTestingModule({
        declarations: [
            TestAbstractSearchTypeComponent
        ],
        imports: [
            ReactiveFormsModule
        ]
    });
    beforeEach(() => {
        component = new TestAbstractSearchTypeComponent();
        component.form = new UntypedFormGroup({
            test: new UntypedFormControl('test')
        });
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should call form.patchValue', () => {
        let spy = jest.spyOn(component.form, 'patchValue');
        let criterion: Criterion = { id: 1, type: 'test' };
        component.setCriterion(criterion);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criterion);
    });
    it('setCriterion(criterion: Criterion) should call form.reset', () => {
        let spy = jest.spyOn(component.form, 'reset');
        component.setCriterion(null);
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('isValid() should return true', () => {
        expect(component.isValid()).toBe(true);
    });
    it('disable() should disable the form', () => {
        expect(component.form.disabled).toBe(false);
        component.disable();
        expect(component.form.disabled).toBe(true);
    });
    it('getType() should return text', () => {
        component.attribute = { ...attribute, operator: 'in' };
        expect(component.getType()).toEqual('text');
    });
    it('getType() should return number', () => {
        component.attribute = { ...attribute, type: 'integer' };
        expect(component.getType()).toEqual('number');
    });
    it('ngOnDestroy() should unsubscribe from formValueChangesSubscription', () => {
        component.formValueChangesSubscription = component.form.valueChanges.pipe().subscribe();
        expect(component.formValueChangesSubscription.closed).toBe(false);
        component.ngOnDestroy();
        expect(component.formValueChangesSubscription.closed).toBe(true);
    });
});