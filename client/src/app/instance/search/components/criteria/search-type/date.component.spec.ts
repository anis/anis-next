/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { DateComponent } from './date.component';

describe('[Instance][search][components][criteria][search-type] DateComponent', () => {
    let component: DateComponent;
    let fixture: ComponentFixture<DateComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DateComponent],
            imports: [ReactiveFormsModule]
        });
        fixture = TestBed.createComponent(DateComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should set date value when criterion param is defined', () => {
        expect(component.form.controls.date.value).toEqual('');
        component.setCriterion({ id: 1, type: 'test', value: new Date().toDateString() } as FieldCriterion);
        expect(component.form.controls.date.value).not.toEqual('');
    });
    it('setCriterion(criterion: Criterion) should set test to form.operator value when criterion param is undefined', () => {
        expect(component.form.controls.operator.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.operator.value).toEqual('test');
    });
    it('getCriterion() should return a criterion of type FieldCriterion', () => {
        component.form.controls.date.setValue(new Date());
        expect(component.getCriterion().type).toEqual('field');
    });
    it('isValid() should true when form is valid or when operator value is nl or nnl', () => {
        expect(component.isValid()).toBe(false);
        component.form.controls.operator.setValue('test');
        component.form.controls.date.setValue(new Date());
        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nnl');
        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nl');
        expect(component.isValid()).toBe(true);
    });
    it('getPlaceholder() should return "" when attribute.placeholder is undefined', () => {
        component.attribute.placeholder_min = null;
        expect(component.getPlaceholder()).toEqual('');
    })
    it('getPlaceholder() should return min ', () => {
        expect(component.getPlaceholder()).toEqual('min');
    });
    it('getDateString(date: Date) should return 2022-12-03', () => {
        expect(component.getDateString(new Date('12-03-2022'))).toEqual('2022-12-03');
    });
    it('operatorOnChange() should disable date formcontrol when operator value is nl or nnl', () => {
        expect(component.form.controls.date.disabled).toBe(false);
        component.form.controls.operator.setValue('nl');
        component.operatorOnChange();
        expect(component.form.controls.date.disabled).toBe(true);
    });
    it('operatorOnChange() should enable date formcontrol when operator value is not  nl or nnl', () => {
        component.form.controls.date.disable();
        expect(component.form.controls.date.enabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls.date.enabled).toBe(true);

    });

});
