/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, FieldCriterion } from 'src/app/instance/store/models';
import { searchTypeOperators } from 'src/app/shared/utils';

@Component({
    selector: 'app-datetime',
    templateUrl: 'datetime.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateTimeComponent extends AbstractSearchTypeComponent {
    hours: string[] = this.initTime(24);
    minutes: string[] = this.initTime(60);

    constructor() {
        super();
        this.form = new UntypedFormGroup({
            operator: new UntypedFormControl(''),
            date: new UntypedFormControl('', [Validators.required]),
            hh: new UntypedFormControl('', [Validators.required]),
            mm: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            const fieldCriterion = criterion as FieldCriterion;
            this.form.controls.operator.setValue(fieldCriterion.operator);
            if (fieldCriterion.operator != 'nl' && fieldCriterion.operator != 'nnl') {
                const [date, time] = fieldCriterion.value.split(' ');
                this.form.controls.date.setValue(new Date(date));
                this.form.controls.hh.setValue(time.slice(0, 2));
                this.form.controls.mm.setValue(time.slice(3, 5));
            }
        } else {
            this.form.controls.operator.setValue(this.attribute.operator);
            if (!this.attribute.dynamic_operator) {
                this.operators = searchTypeOperators.filter(
                    operator => [this.attribute.operator, 'nl', 'nnl'].includes(operator.value)
                );
            }
        }
    }
   
    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        let value = null;
        if (this.form.controls.operator.value != 'nl' && this.form.controls.operator.value != 'nnl') {
            const date = this.getDateString(this.form.value.date);
            const time = `${this.form.value.hh}:${this.form.value.mm}`;
            value = `${date} ${time}`;
        }

        return {
            id: this.attribute.id,
            type: 'field',
            operator: this.form.controls.operator.value,
            value
        } as FieldCriterion;
    }

    isValid(): boolean {
        return this.form.valid || this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl';
    }

    /**
     * Returns string array to represent the given time.
     *
     * @param  {number} time - The number max to represent time.
     *
     * @return string[]
     */
    initTime(time: number): string[] {
        const array: string[] = [];
        for (let i = 0; i < time; i++) {
            const t = ('0' + i).slice(-2);
            array.push(t);
        }
        return array;
    }

    /**
     * Stringifies the given date.
     *
     * @param  {Date} date - The date.
     *
     * @return string
     */
    getDateString(date: Date): string {
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + (date.getDate())).slice(-2);
        return `${date.getFullYear()}-${month}-${day}`;
    }

    operatorOnChange() {
        if (this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl') {
            this.form.controls.date.disable();
            this.form.controls.hh.disable();
            this.form.controls.mm.disable();
        } else {
            this.form.controls.date.enable();
            this.form.controls.hh.enable();
            this.form.controls.mm.enable();
        }
    }
}
