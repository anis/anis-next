/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component} from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, BetweenCriterion, FieldCriterion } from 'src/app/instance/store/models';

@Component({
    selector: 'app-between',
    templateUrl: 'between.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BetweenComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'bw', label: 'min/max' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];

    constructor() {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl('', [Validators.required]),
            min: new UntypedFormControl('', [Validators.required]),
            max: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion): void {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'between') {
                this.form.controls.label.setValue('bw');
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls.label.setValue(fieldCriterion.operator);
            }
        } else {
            this.form.controls.label.setValue('bw');
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        return {
            id: this.attribute.id,
            type: 'between',
            min: (this.form.controls.min.value) ? this.form.controls.min.value : null,
            max: (this.form.controls.max.value) ? this.form.controls.max.value : null,
        } as BetweenCriterion;
    }

    /**
     * Returns placeholder for the minimum value.
     *
     * @return string
     */
    getPlaceholderMin(): string {
        if (!this.attribute.placeholder_min) {
            return '';
        } else {
            return this.attribute.placeholder_min;
        }
    }

    /**
     * Returns placeholder for the maximum value.
     *
     * @return string
     */
    getPlaceholderMax(): string {
        if (!this.attribute.placeholder_max) {
            return '';
        } else {
            return this.attribute.placeholder_max;
        }
    }

    isValid() {
        return this.form.controls.min.value
            || this.form.controls.max.value
            || this.form.controls.label.value === 'nl'
            || this.form.controls.label.value === 'nnl';
    }

    labelOnChange() {
        if (this.form.controls.label.value === 'nl' || this.form.controls.label.value === 'nnl') {
            this.nullOrNotNull = this.form.controls.label.value;
            this.form.controls.min.disable();
            this.form.controls.max.disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls.max.enable();
            this.form.controls.min.enable();
        }
    }
}
