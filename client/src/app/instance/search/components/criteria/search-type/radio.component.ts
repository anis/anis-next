/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, FieldCriterion } from 'src/app/instance/store/models';
import { searchTypeOperators } from 'src/app/shared/utils';

@Component({
    selector: 'app-radio',
    templateUrl: 'radio.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadioComponent extends AbstractSearchTypeComponent {
    constructor() {
        super();
        this.form = new UntypedFormGroup({
            operator: new UntypedFormControl(''),
            radio: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            this.form.controls.radio.setValue((criterion as FieldCriterion).value);
            this.operatorOnChange();
        } else {
            this.form.controls.operator.setValue(this.attribute.operator);
        }
        if (!this.attribute.dynamic_operator) {
            this.operators = searchTypeOperators.filter(
                operator => [this.attribute.operator, 'nl', 'nnl'].includes(operator.value)
            );
        }
    }
    
    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        let value = null;
        if (this.form.controls.operator.value != 'nl' && this.form.controls.operator.value != 'nnl') {
            const option = this.attribute.options.find(o => o.value === this.form.value.radio);
            value = option.value;
        }

        return {
            id: this.attribute.id,
            type: 'field',
            operator: this.form.controls.operator.value,
            value
        } as FieldCriterion;
    }

    isValid(): boolean {
        return this.form.valid || this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl';
    }

    operatorOnChange() {
        if (this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl') {
            this.form.controls.radio.disable();
        } else {
            this.form.controls.radio.enable();
        }
    }
}
