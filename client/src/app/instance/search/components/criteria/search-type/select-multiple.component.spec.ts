/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FieldCriterion, SelectMultipleCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { SelectMultipleComponent } from './select-multiple.component';

describe('[Instance][search][components][criteria][search-type] SelectMultipleComponent', () => {
    let component: SelectMultipleComponent;
    let fixture: ComponentFixture<SelectMultipleComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SelectMultipleComponent],
            imports: [
                ReactiveFormsModule,
                NgSelectModule
            ]
        });
        fixture = TestBed.createComponent(SelectMultipleComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should set label and select values when criterion type is muliple', () => {
        expect(component.form.controls.select.value).toEqual('');
        expect(component.form.controls.label.value).toEqual('');
        let criterion: SelectMultipleCriterion = {
            id: 1, options: [{ display: 10, label: 'test1', value: 'test1' },
            { display: 10, label: 'test2', value: 'test2' }
            ], type: 'multiple'
        };
        component.setCriterion(criterion);
        expect(component.form.controls.select.value).toEqual(['test1', 'test2']);
        expect(component.form.controls.label.value).toEqual('in');

    });
    it('setCriterion(criterion: Criterion) should set label and select values when criterion type is not multiple', () => {
        expect(component.form.controls.label.value).toEqual('');
        let criterion: FieldCriterion = {
            id: 1,
            operator: 'test',
            type: 'test',
            value: ''
        }
        component.setCriterion(criterion);
        expect(component.form.controls.label.value).toEqual('test');

    });
    it('setCriterion(criterion: Criterion) should set label and select values when criterion param is not defined', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.label.value).toEqual('in');

    });
    it('getCriterion() should return a criterion with type list', () => {
        expect(component.getCriterion().type).toEqual('multiple');
    });
    it('labelOnChange() should disable select when label value is nl or nnl', () => {
        component.form.controls.label.setValue('nl');
        expect(component.form.controls.select.enabled).toBe(true);
        component.labelOnChange();
        expect(component.form.controls.select.enabled).toBe(false);

    });
    it('labelOnChange() should enable list when label value is not  nl or nnl', () => {
        component.form.controls.select.disable()
        component.labelOnChange();
        expect(component.form.controls.select.enabled).toBe(true);


    });
});
