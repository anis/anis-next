/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { of } from 'rxjs';
import { AppConfigService } from 'src/app/app-config.service';
import { FieldCriterion, SvomKeyword } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { SvomJsonKwComponent } from './svom-json-kw.component';
class MockHttpClient extends HttpClient {
    get = jest.fn().mockImplementation(() => of([{ search_kw: { data_type: 'test', default: '', extension: 'test', name: '' } }]))
}

describe('[Instance][search][components][criteria][search-type] SvomJsonKwComponent', () => {
    let component: SvomJsonKwComponent;
    let fixture: ComponentFixture<SvomJsonKwComponent>;
    let attribute: Attribute;
    let httpClient: MockHttpClient = new MockHttpClient(null);
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SvomJsonKwComponent],
            imports: [
                ReactiveFormsModule,
                HttpClientModule,
                NgSelectModule

            ],
            providers: [
                { provide: AppConfigService, useValue: { authenticationEnabled: false } },
                ChangeDetectorRef,
                { provide: HttpClient, useValue: httpClient }
            ]
        });
        fixture = TestBed.createComponent(SvomJsonKwComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion)  should set label value to js when criterion type is json', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion({ id: 1, type: 'json' });
        expect(component.form.controls.label.value).toEqual('js');
    });
    it('setCriterion(criterion: Criterion)  should set label value to js when criterion type is json', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion({ id: 1, type: 'json' });
        expect(component.form.controls.label.value).toEqual('js');
    });
    it('setCriterion(criterion: Criterion)  should set label value to test and operator value to ""  when criterion type is not  json', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.form.controls.operator.setValue('test');
        component.setCriterion({ id: 1, type: 'test', operator: 'test' } as FieldCriterion);
        expect(component.form.controls.label.value).toEqual('test');
        expect(component.form.controls.operator.value).toEqual('');
    });
    it('setCriterion(criterion: Criterion)  should set label value to js when criterion param is type is not defined', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.label.value).toEqual('js');
    });
    it('setCriteriaList(criteriaList: Criterion[]) should ', () => {
        let spyOnHttp = jest.spyOn(httpClient, 'get');
        component.setCriteriaList([{ id: 3, type: 'test' }]);
        expect(spyOnHttp).toHaveBeenCalledTimes(1);
    });
    it('setCriteriaList(criteriaList: Criterion[]) should ', () => {
        component.svomKeywords = [{ data_type: 'test', default: 'test', extension: 'test', name: 'test' }]
        component.setCriteriaList([{ id: 2, type: 'test' }]);
        expect(component.svomKeywords.length).toEqual(0);
    });
    it('getCriterion() should return a criterion of type FieldCriterion', () => {
        expect(component.getCriterion().type).toEqual('json');
    });
    it('getKeywordValue() should return test_extension,test_name', () => {
        let svomKeyword: SvomKeyword = { data_type: 'test', default: 'test', name: 'test_name', extension: 'test_extension' }
        expect(component.getKeywordValue(svomKeyword)).toEqual('test_extension,test_name');
    });
    it('labelOnChange() should disable operator, path, value when label value is nl or nnl', () => {
        component.form.controls.label.setValue('nl');
        expect(component.form.controls.operator.enabled).toBe(true);
        expect(component.form.controls.path.enabled).toBe(true);
        expect(component.form.controls.value.enabled).toBe(true);
        component.labelOnChange();
        expect(component.form.controls.operator.enabled).toBe(false);
        expect(component.form.controls.path.enabled).toBe(false);
        expect(component.form.controls.value.enabled).toBe(false);

    });
    it('labelOnChange() should enable operator, path, value when label value is not  nl or nnl', () => {
        component.form.controls.operator.disable()
        component.form.controls.path.disable()
        component.form.controls.value.disable()
        component.labelOnChange();
        expect(component.form.controls.operator.enabled).toBe(true);
        expect(component.form.controls.path.enabled).toBe(true);
        expect(component.form.controls.value.enabled).toBe(true);

    });

})