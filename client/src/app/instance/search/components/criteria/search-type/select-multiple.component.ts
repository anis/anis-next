/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, SelectMultipleCriterion, FieldCriterion } from 'src/app/instance/store/models';

@Component({
    selector: 'app-select-multiple',
    templateUrl: 'select-multiple.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectMultipleComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'in', label: 'in' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];

    constructor() {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl(''),
            select: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'multiple') {
                const multipleCriterion = criterion as SelectMultipleCriterion;
                const values = multipleCriterion.options.map(option => option.value);
                this.form.controls.select.setValue(values);
                this.form.controls.label.setValue('in');
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls.label.setValue(fieldCriterion.operator);
                this.labelOnChange();
            }
        } else {
            this.form.controls.label.setValue('in');
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        const values = this.form.value.select as string[];
        const options = this.attribute.options.filter(option => values.includes(option.value));

        return {
            id: this.attribute.id,
            type: 'multiple',
            options
        } as SelectMultipleCriterion;
    }

    labelOnChange() {
        if (this.form.controls.label.value === 'nl' || this.form.controls.label.value === 'nnl') {
            this.nullOrNotNull = this.form.controls.label.value;
            this.form.controls.select.disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls.select.enable();
        }
    }
}
