/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldCriterion, ListCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { ListComponent } from './list.component';

describe('[Instance][search][components][criteria][search-type] ListComponent', () => {
    let component: ListComponent;
    let fixture: ComponentFixture<ListComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ListComponent],
            imports: [
                ReactiveFormsModule,

            ],
        });
        fixture = TestBed.createComponent(ListComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should set list and label value when criterion param is defined', () => {
        let spyonLabel = jest.spyOn(component.form.controls.label, 'setValue');
        let spyOnList = jest.spyOn(component.form.controls.list, 'setValue');
        component.setCriterion({ id: 1, type: 'list', values: [] } as ListCriterion);
        expect(spyOnList).toHaveBeenCalledTimes(1);
        expect(spyonLabel).toHaveBeenCalledTimes(1);

    });
    it('setCriterion(criterion: Criterion) should set label value to test when criterion.type value is not list', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion({ id: 1, type: '', operator: 'test', } as FieldCriterion);
        expect(component.form.controls.label.value).toEqual('test');

    });
    it('setCriterion(criterion: Criterion) should set label value to in when criterion param is not defined', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.label.value).toEqual('in');

    });
    it('setCriterion(criterion: Criterion) should set label value to in when criterion param is not defined', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.label.value).toEqual('in');

    });
    it('getCriterion() should return a criterion with type list', () => {
        expect(component.getCriterion().type).toEqual('list');
    });
    it('getPlaceholder() should return "" when attribute.placeholder is undefined', () => {
        component.attribute.placeholder_min = null;
        expect(component.getPlaceholder()).toEqual('');
    })
    it('getPlaceholder() should return min ', () => {
        expect(component.getPlaceholder()).toEqual('min');
    });
    it('labelOnChange() should disable list when label value is nl or nnl', () => {
        component.form.controls.label.setValue('nl');
        expect(component.form.controls.list.enabled).toBe(true);
        component.labelOnChange();
        expect(component.form.controls.list.enabled).toBe(false);

    });
    it('labelOnChange() should enable list when label value is not  nl or nnl', () => {
        component.form.controls.list.disable()

        component.labelOnChange();
        expect(component.form.controls.list.enabled).toBe(true);


    });
})