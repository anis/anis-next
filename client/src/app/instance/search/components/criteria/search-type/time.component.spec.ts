/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FieldCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { TimeComponent } from './time.component';

describe('[Instance][search][components][criteria][search-type] TimeComponent', () => {
    let component: TimeComponent;
    let fixture: ComponentFixture<TimeComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TimeComponent],
            imports: [
                ReactiveFormsModule,
                NgSelectModule
            ],
        });
        fixture = TestBed.createComponent(TimeComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should set select value and call operatorOnChange when criterion param is defined', () => {
        let spy = jest.spyOn(component, 'operatorOnChange');
        expect(component.form.controls.hh.value).toEqual('');
        expect(component.form.controls.mm.value).toEqual('');

        component.setCriterion({ id: 1, value: '12:30', type: 'test' } as FieldCriterion);
        expect(component.form.controls.hh.value).toEqual('12');
        expect(component.form.controls.mm.value).toEqual('30');
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('setCriterion(criterion: Criterion) should set operator value to test value when criterion param is not defined', () => {
        expect(component.form.controls.operator.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.operator.value).toEqual('test');
    });
    it('getCriterion() should return a criterion of type FieldCriterion', () => {
        expect(component.getCriterion().type).toEqual('field');
    });
    it('isValid() should true when form is valid or when operator value is nl or nnl', () => {
        expect(component.isValid()).toBe(false);
        component.form.controls.operator.setValue('test');
        component.form.controls.hh.setValue('10');
        component.form.controls.mm.setValue('10');
        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nnl');
        expect(component.isValid()).toBe(true);
        component.form.controls.operator.setValue('nl');
        expect(component.isValid()).toBe(true);
    });
    it('operatorOnChange() should disable  hh, mm formcontrol when operator value is nl or nnl', () => {
        component.form.controls.operator.setValue('nl');
        expect(component.form.controls.hh.disabled).toBe(false);
        expect(component.form.controls.hh.disabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls.hh.disabled).toBe(true);
        expect(component.form.controls.hh.disabled).toBe(true);

    });
    it('operatorOnChange() should enable  hh, mm formcontrol when operator value is not  nl or nnl', () => {
        component.form.controls.hh.disable();
        component.form.controls.mm.disable();
        expect(component.form.controls.hh.enabled).toBe(false);
        expect(component.form.controls.mm.enabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls.hh.enabled).toBe(true);
        expect(component.form.controls.mm.enabled).toBe(true);

    });
})