/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SearchTypeLoaderDirective } from './search-type-loader.directive';
import { FieldComponent } from './field.component';
import { BetweenComponent } from './between.component';
import { SelectComponent } from './select.component';
import { SelectMultipleComponent } from './select-multiple.component';
import { DatalistComponent } from './datalist.component';
import { ListComponent } from './list.component';
import { RadioComponent } from './radio.component';
import { CheckboxComponent } from './checkbox.component';
import { BetweenDateComponent } from './between-date.component';
import { DateComponent } from './date.component';
import { TimeComponent } from './time.component';
import { DateTimeComponent } from './datetime.component';
import { SelectAliasComponent } from './select-alias.component';
import { SelectMultipleAliasComponent } from './select-multiple-alias.component';
import { JsonComponent } from './json.component';
import { SvomJsonKwComponent } from './svom-json-kw.component';

export * from './abstract-search-type.component';
export * from './search-type-loader.directive';

export const searchTypeComponents = [
    SearchTypeLoaderDirective,
    FieldComponent,
    BetweenComponent,
    SelectComponent,
    SelectMultipleComponent,
    DatalistComponent,
    ListComponent,
    RadioComponent,
    CheckboxComponent,
    BetweenDateComponent,
    DateComponent,
    TimeComponent,
    DateTimeComponent,
    SelectAliasComponent,
    SelectMultipleAliasComponent,
    JsonComponent,
    SvomJsonKwComponent
];

export const getSearchTypeComponent = (searchType: string) => {
    let nameOfSearchTypeComponent = null;
    switch(searchType) {
        case 'field': {
            nameOfSearchTypeComponent = FieldComponent;
            break;
        }
        case 'between': {
            nameOfSearchTypeComponent = BetweenComponent;
            break;
        }
        case 'select': {
            nameOfSearchTypeComponent = SelectComponent;
            break;
        }
        case 'select-multiple': {
            nameOfSearchTypeComponent = SelectMultipleComponent;
            break;
        }
        case 'datalist': {
            nameOfSearchTypeComponent = DatalistComponent;
            break;
        }
        case 'list': {
            nameOfSearchTypeComponent = ListComponent;
            break;
        }
        case 'radio': {
            nameOfSearchTypeComponent = RadioComponent;
            break;
        }
        case 'checkbox': {
            nameOfSearchTypeComponent = CheckboxComponent;
            break;
        }
        case 'between-date': {
            nameOfSearchTypeComponent = BetweenDateComponent;
            break;
        }
        case 'date': {
            nameOfSearchTypeComponent = DateComponent;
            break;
        }
        case 'time': {
            nameOfSearchTypeComponent = TimeComponent;
            break;
        }
        case 'date-time': {
            nameOfSearchTypeComponent = DateTimeComponent;
            break;
        }
        case 'select-alias':
            nameOfSearchTypeComponent = SelectAliasComponent;
            break;
        case 'select-multiple-alias':
            nameOfSearchTypeComponent = SelectMultipleAliasComponent;
            break;
        case 'json': {
            nameOfSearchTypeComponent = JsonComponent;
            break;
        }
        case 'svom_json_kw': {
            nameOfSearchTypeComponent = SvomJsonKwComponent;
            break;
        }
        default: {
            nameOfSearchTypeComponent = null;
            break;
        }
    }
    return nameOfSearchTypeComponent;
}
