/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, UntypedFormArray } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, SelectMultipleCriterion, FieldCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-checkbox',
    templateUrl: 'checkbox.component.html',
    styleUrls: ['checkbox.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'in', label: 'in' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];

    constructor() {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl('')
        });
    }

    setAttribute(attribute: Attribute): void {
        super.setAttribute(attribute);
        // Initialization of checkboxes (1 per option)
        const formControls: UntypedFormControl[] = [];
        for (let i = 0; i < attribute.options.length; i++) {
            formControls.push(new UntypedFormControl());
        }
        this.form.addControl('checkboxes', new UntypedFormArray(formControls));
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        
        if (criterion) {
            if (criterion.type === 'multiple') {
                for (let i = 0; i < this.attribute.options.length; i++) {
                    if ((criterion as SelectMultipleCriterion).options.find(o => o.label === this.attribute.options[i].label)) {
                        this.getCheckboxes().controls[i].setValue(true);
                    }
                }
                this.form.controls.label.setValue('in');
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls.label.setValue(fieldCriterion.operator);
            }
        } else {
            this.form.controls.label.setValue('in');
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        const selected = this.getCheckboxes().value;
        const values = [...this.attribute.options.filter((option, index) => selected[index])];

        return {
            id: this.attribute.id,
            type: 'multiple',
            options: values
        } as SelectMultipleCriterion;
    }

    isValid(): boolean {
        const selected = this.getCheckboxes().value;
        const values = [...this.attribute.options.filter((option, index) => selected[index])];
        return values.length > 0
            || this.form.controls.label.value === 'nl'
            || this.form.controls.label.value === 'nnl';
    }

    /**
     * Returns checkboxes form.
     *
     * @return FormArray
     */
    getCheckboxes(): UntypedFormArray {
        return this.form.controls.checkboxes as UntypedFormArray;
    }

    /**
     * Checks if one of the checkboxes is checked.
     *
     * @return boolean
     */
    isChecked(): boolean {
        // If one of the checkboxes is checked returns true else returns false
        return this.getCheckboxes().controls.filter(formControl => formControl.value).length > 0;
    }

    labelOnChange() {
        if (this.form.controls.label.value === 'nl' || this.form.controls.label.value === 'nnl') {
            this.nullOrNotNull = this.form.controls.label.value;
            this.getCheckboxes().controls.map((value, index) => {
                value.disable();
            });
        } else {
            this.nullOrNotNull = '';
            this.getCheckboxes().controls.map((value, index) => {
                value.enable();
            });
        }
    }
}
