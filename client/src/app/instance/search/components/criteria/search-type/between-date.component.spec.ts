/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Criterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { BetweenDateComponent } from './between-date.component';

describe('[Instance][search][components][criteria][search-type] BetweenDateComponent', () => {
    let component: BetweenDateComponent;
    let fixture: ComponentFixture<BetweenDateComponent>;
    let spyOnLabel;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [BetweenDateComponent],
            imports: [ReactiveFormsModule]
        });
        fixture = TestBed.createComponent(BetweenDateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        spyOnLabel = jest.spyOn(component.form.controls.label, 'setValue');

    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should set values in form on dateRange and  label with bw', () => {
        let criterion: Criterion = { id: 1, type: 'between' };
        let spyOnDateRange = jest.spyOn(component.form.controls.dateRange, 'setValue');

        component.setCriterion(criterion);
        expect(spyOnDateRange).toHaveBeenCalledTimes(1);
        expect(spyOnLabel).toHaveBeenCalledTimes(1);
        expect(spyOnLabel).toHaveBeenCalledWith('bw');

    });
    it('setCriterion(criterion: Criterion) should set value on label when the  criterion type is not between', () => {
        let criterion: Criterion = { id: 1, type: 'test' };
        component.setCriterion(criterion);
        expect(spyOnLabel).toHaveBeenCalledTimes(1);
    });
    it('setCriterion(criterion: Criterion) should set value on label when criterion param is undefined', () => {
        component.setCriterion(null);
        expect(spyOnLabel).toHaveBeenCalledWith('bw');
    });
    it('getCriterion() should return an criterion of type between', () => {
        component.form.controls.dateRange.setValue([
            new Date(),
            new Date()
        ]);
        component.attribute = { ...attribute, id: 1 };
        expect(component.getCriterion().type).toEqual('between');
    });
    it('labelOnChange() should disable dateRange', () => {
        component.form.controls.label.setValue('nl');
        expect(component.form.controls.dateRange.disabled).toBe(false);
        component.labelOnChange();
        expect(component.form.controls.dateRange.disabled).toBe(true);
    });
    it('labelOnChange() should enable dateRange', () => {
        component.form.controls.dateRange.disable();
        expect(component.form.controls.dateRange.enabled).toBe(false);
        component.labelOnChange();
        expect(component.form.controls.dateRange.enabled).toBe(true);
    });
});
