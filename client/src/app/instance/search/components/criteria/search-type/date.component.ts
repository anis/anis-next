/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, FieldCriterion } from 'src/app/instance/store/models';
import { searchTypeOperators } from 'src/app/shared/utils';

@Component({
    selector: 'app-date',
    templateUrl: 'date.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateComponent extends AbstractSearchTypeComponent {
    constructor() {
        super();
        this.form = new UntypedFormGroup({
            operator: new UntypedFormControl(''),
            date: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            const fieldCriterion = criterion as FieldCriterion;
            if (fieldCriterion.value) {
                this.form.controls.date.setValue(new Date(fieldCriterion.value));
            }
        } else {
            this.form.controls.operator.setValue(this.attribute.operator);
            if (!this.attribute.dynamic_operator) {
                this.operators = searchTypeOperators.filter(
                    operator => [this.attribute.operator, 'nl', 'nnl'].includes(operator.value)
                );
            }
        }
    }
    
    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        let value = null;
        if (this.form.controls.operator.value != 'nl' && this.form.controls.operator.value != 'nnl') {
            value = this.getDateString(this.form.value.date)
        }

        return {
            id: this.attribute.id,
            type: 'field',
            operator: this.form.controls.operator.value,
            value
        } as FieldCriterion;
    }

    isValid(): boolean {
        return this.form.valid || this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl';
    }

    /**
     * Return placeholder.
     *
     * @return string
     */
    getPlaceholder(): string {
        if (!this.attribute.placeholder_min) {
            return '';
        } else {
            return this.attribute.placeholder_min;
        }
    }

    /**
     * Stringifies the given date.
     *
     * @param  {Date} date - The date.
     *
     * @return string
     */
    getDateString(date: Date): string {
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + (date.getDate())).slice(-2);
        return `${date.getFullYear()}-${month}-${day}`;
    }

    operatorOnChange() {
        if (this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl') {
            this.form.controls.date.disable();
        } else {
            this.form.controls.date.enable();
        }
    }
}
