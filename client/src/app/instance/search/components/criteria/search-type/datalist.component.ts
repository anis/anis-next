/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, FieldCriterion } from 'src/app/instance/store/models';
import { searchTypeOperators } from 'src/app/shared/utils';

@Component({
    selector: 'app-datalist',
    templateUrl: 'datalist.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatalistComponent extends AbstractSearchTypeComponent {
    constructor() {
        super();
        this.form = new UntypedFormGroup({
            operator: new UntypedFormControl(''),
            value: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            this.operatorOnChange();
        } else {
            this.form.controls.operator.setValue(this.attribute.operator);
        }
        if (!this.attribute.dynamic_operator) {
            this.operators = searchTypeOperators.filter(
                operator => [this.attribute.operator, 'nl', 'nnl'].includes(operator.value)
            );
        }
    }
    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        return {
            id: this.attribute.id,
            type: 'field',
            ...this.form.value
        } as FieldCriterion;
    }

    isValid(): boolean {
        return this.form.valid || this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl';
    }

    /**
     * Return placeholder.
     *
     * @return string
     */
    getPlaceholder(): string {
        if (!this.attribute.placeholder_min) {
            return '';
        } else {
            return this.attribute.placeholder_min;
        }
    }

    /**
     * Returns string datalist ID.
     *
     * @return string
     */
    getDatalistId(): string {
        return `datalist_${this.attribute.id}`;
    }

    operatorOnChange() {
        if (this.form.controls.operator.value === 'nl' || this.form.controls.operator.value === 'nnl') {
            this.form.controls.value.disable();
        } else {
            this.form.controls.value.enable();
        }
    }
}
