/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SearchTypeLoaderDirective } from "./search-type-loader.directive";

class MockSearchTypeLoaderDirective extends SearchTypeLoaderDirective {
    constructor() {
        super(null)
    }

}
describe('[Instance][search][components][criteria][search-type] SearchTypeLoaderDirective', () => {
    it('should create the directive', () => {
        let searchTypeLoaderDirective = new MockSearchTypeLoaderDirective();
        expect(searchTypeLoaderDirective).toBeTruthy();
    })

})