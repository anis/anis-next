/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FieldCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { JsonComponent } from './json.component';

describe('[Instance][search][components][criteria][search-type] JsonComponent', () => {
    let component: JsonComponent;
    let fixture: ComponentFixture<JsonComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [JsonComponent],
            imports: [
                ReactiveFormsModule,
                NgSelectModule
            ],
        });
        fixture = TestBed.createComponent(JsonComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();

    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('setCriterion(criterion: Criterion) should set label value with js if criterion type is json', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion({ id: 1, type: 'json' });
        expect(component.form.controls.label.value).toEqual('js');

    });
    it('setCriterion(criterion: Criterion) should set label value with test if criterion type is not json', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion({ id: 1, type: '', operator: 'test' } as FieldCriterion);
        expect(component.form.controls.label.value).toEqual('test');

    });
    it('setCriterion(criterion: Criterion) should set label value with js when criterion param is not defined', () => {
        expect(component.form.controls.label.value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls.label.value).toEqual('js');

    });
    it('getCriterion() should return a criterion with type json', () => {
        expect(component.getCriterion().type).toEqual('json');
    });
    it('labelOnChange() should disable operator, path, value when label value is nl or nnl', () => {
        component.form.controls.label.setValue('nl');
        expect(component.form.controls.operator.enabled).toBe(true);
        expect(component.form.controls.path.enabled).toBe(true);
        expect(component.form.controls.value.enabled).toBe(true);
        component.labelOnChange();
        expect(component.form.controls.operator.enabled).toBe(false);
        expect(component.form.controls.path.enabled).toBe(false);
        expect(component.form.controls.value.enabled).toBe(false);

    });
    it('labelOnChange() should enable operator, path, value when label value is not  nl or nnl', () => {
        component.form.controls.operator.disable()
        component.form.controls.path.disable()
        component.form.controls.value.disable()
        component.labelOnChange();
        expect(component.form.controls.operator.enabled).toBe(true);
        expect(component.form.controls.path.enabled).toBe(true);
        expect(component.form.controls.value.enabled).toBe(true);

    });
});
