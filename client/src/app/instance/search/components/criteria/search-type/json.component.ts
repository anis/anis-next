/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, JsonCriterion, FieldCriterion } from 'src/app/instance/store/models';

@Component({
    selector: 'app-json',
    templateUrl: 'json.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class JsonComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'js', label: 'json' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];

    constructor() {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl(''),
            path: new UntypedFormControl('', [Validators.required]),
            operator: new UntypedFormControl('', [Validators.required]),
            value: new UntypedFormControl('', [Validators.required])
        });
    }

    setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'json') {
                this.form.controls.label.setValue('js');
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls.label.setValue(fieldCriterion.operator);
                this.form.controls.operator.setValue('');
            }
        } else {
            this.form.controls.label.setValue('js');
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        return {
            id: this.attribute.id,
            type: 'json',
            ...this.form.value
        } as JsonCriterion;
    }

    labelOnChange() {
        if (this.form.controls.label.value === 'nl' || this.form.controls.label.value === 'nnl') {
            this.nullOrNotNull = this.form.controls.label.value;
            this.form.controls.operator.disable();
            this.form.controls.path.disable();
            this.form.controls.value.disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls.operator.enable();
            this.form.controls.path.enable();
            this.form.controls.value.enable();
        }
    }
}
