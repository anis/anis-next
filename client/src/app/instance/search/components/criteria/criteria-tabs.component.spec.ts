/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { CriteriaTabsComponent } from './criteria-tabs.component';
import { Attribute, CriteriaFamily } from '../../../../metamodel/models';
import { Criterion, FieldCriterion, SvomKeyword } from '../../../store/models';
import { AttributeListByFamilyPipe } from '../../../../shared/pipes/attribute-list-by-family.pipe';

describe('[Instance][Search][Component][Criteria] CriteriaTabsComponent', () => {
    @Component({ selector: 'app-criteria-by-family', template: '' })
    class CriteriaByFamilyStubComponent {
        @Input() attributeList: Attribute[];
        @Input() criteriaList: Criterion[];
        @Input() svomKeywords: SvomKeyword[];
    }

    let component: CriteriaTabsComponent;
    let fixture: ComponentFixture<CriteriaTabsComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaTabsComponent,
                CriteriaByFamilyStubComponent,
                AttributeListByFamilyPipe
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(CriteriaTabsComponent);
        component = fixture.componentInstance;
        component.attributeList = [
            { ...attribute, id_criteria_family: 1 },
            { ...attribute, id_criteria_family: 2 },
            { ...attribute, id_criteria_family: 1 }
        ];
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('getCriteriaFamilyList() should return an attribute array one criteriaFamily', () => {
        component.criteriaFamilyList = [ 
            { display: 10, id: 1, label: 'test', opened: true },
            { display: 10, id: 5, label: 'test2', opened: true },
            { display: 10, id: 3, label: 'test3', opened: true },
        ]
        let result = component.getCriteriaFamilyList();
        expect(result.length).toEqual(1);
    });
    it('getAttributeListByCriteriaFamily(criteriaFamily: CriteriaFamily) should return an attribute array two attributes', () => {
        const criteriaFamily: CriteriaFamily = { display: 10, id: 1, label: 'test', opened: true };
        let result = component.getAttributeListByCriteriaFamily(criteriaFamily);
        expect(result.length).toEqual(2);

    });
    it('raises the add criterion event when clicked', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: 'test' } as FieldCriterion;
        component.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(criterion));
        component.emitAdd(criterion);
    });
    it('raises the emitUpdate criterion event', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: 'test' } as FieldCriterion;
        const spy = jest.spyOn(component.updateCriterion, 'emit');
        component.emitUpdate(criterion);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criterion);
    });

    it('raises the delete criterion event when clicked', () => {
        const criterionId = 1;
        component.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1
        ));
        component.emitDelete(criterionId);
    });

});

