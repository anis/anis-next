/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Directive, Input, Output, EventEmitter } from '@angular/core';

import { Dataset } from 'src/app/metamodel/models';
import { Criterion, ConeSearch, criterionToString } from 'src/app/instance/store/models';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Directive()
export abstract class AbstractDownloadComponent {
    @Input() dataset: Dataset;
    @Input() criteriaList: Criterion[];
    @Input() outputList: number[];
    @Input() coneSearch: ConeSearch;
    @Input() archiveIsCreating: boolean;
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();

    constructor(private appConfig: AppConfigService) { }

    /**
     * Returns API URL to get data with user parameters.
     *
     * @return string
     */
    getUrl(format: string, selectedData: string = null): string {
        if(format === 'fits'){
            return `${getHost(this.appConfig.servicesUrl)}/download-fits/${this.getQuery(selectedData)}&n=${this.dataset.name}`
        } else {
            return `${getHost(this.appConfig.apiUrl)}/search/${this.getQuery(selectedData)}&f=${format}`;
        }
    }

    getQuery(selectedData: string = null) {
        let query = `${this.dataset.name}?a=${this.outputList.join(';')}`;
        if (this.criteriaList.length > 0) {
            query += `&c=${this.criteriaList.map(criterion => criterionToString(criterion)).join(';')}`;
            if (selectedData) {
                query += `;${selectedData}`;
            }
        } else if (selectedData) {
            query += `&c=${selectedData}`;
        }
        if (this.coneSearch) {
            query += `&cs=${this.coneSearch.ra}:${this.coneSearch.dec}:${this.coneSearch.radius}`;
        }
        return query;
    }

    download(event, url: string, format: string) {
        event.preventDefault();

        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);
        const filename = `result_${this.dataset.name}_${today.toISOString()}.${this.formatToExtension(format)}`;

        this.downloadFile.emit({ url, filename });
    }

    formatToExtension(format: string) {
        let extension: string;
        switch (format) {
            case 'json': { 
                extension = 'json';
                break; 
            }
            case 'csv': { 
                extension = 'csv';
                break; 
            }
            case 'ascii': { 
                extension = 'txt';
                break; 
            }
            case 'votable': { 
                extension = 'xml';
                break; 
            }
            case 'fits': {
                extension = 'fits';
                break;
            }
            default: { 
                extension = 'json';
                break; 
            }
        }
        return extension;
    }
}