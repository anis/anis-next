/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { Attribute } from "src/app/metamodel/models";
import { DatatableActionsComponent } from "./datatable-actions.component"

describe('[instance][search][components][result] DatatableActionsComponent', () => {
    let component: DatatableActionsComponent;
    let fixture: ComponentFixture<DatatableActionsComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatatableActionsComponent
            ],
            providers: [
                AppConfigService
            ]
        });
        fixture = TestBed.createComponent(DatatableActionsComponent);
        component = fixture.componentInstance;
        component.attributeList = [
            { ...attribute, id: 1, archive: true, primary_key: true }
        ]

    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('isArchiveIsAvailable should return true', () => {

        component.outputList = [1];
        expect(component.isArchiveIsAvailable()).toBe(true);
    });
    it('getDatatableUrl(format: string) should call getUrl', () => {
        let spy = jest.spyOn(component, 'getUrl').mockImplementationOnce(() =>'');
        component.getDatatableUrl('');
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should raises broadcastvotable event', () => {
        let spy = jest.spyOn(component.broadcastVotable, 'emit');
        component.getDatatableUrl = jest.fn().mockImplementationOnce(() => 'test.fr')
        component.broadcastResult();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test.fr');
    });
    it('should raises startTaskCreateArchive event', () => {
        let spy = jest.spyOn(component.startTaskCreateArchive, 'emit');
        component.getQuery = jest.fn().mockImplementationOnce(() => 'test/test')
        component.downloadArchive();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test/test');
    });

});