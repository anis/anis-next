/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { Dataset, Image } from "src/app/metamodel/models";
import { ImageDisplayComponent } from "./image-display.component";

describe('[Instance][Search][Component][Result] ImageDisplayComponent', () => {
    let component: ImageDisplayComponent;
    let fixture: ComponentFixture<ImageDisplayComponent>;
    let dataset: Dataset;
    let image: Image;
    
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ImageDisplayComponent
            ],
            providers: [
                { provide: AppConfigService, useValue: { servicesUrl: 'test' } }
            ]
        });
        fixture = TestBed.createComponent(ImageDisplayComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getHref() should return the href', () => {
        let expected = 'test/fits-cut-to-png/test_dataset_name?filename=test_image_file_path&ra=5&dec=10&radius=3&stretch=test&pmin=5&pmax=10&axes=false';
        component.dataset = { ...dataset, name: 'test_dataset_name' };
        component.coneSearch = { dec: 10, ra: 5, radius: 3 }
        expect(component.getHref({ ...image, file_path: 'test_image_file_path', stretch: 'test', pmax: 10, pmin: 5 })).toEqual(expected);

    });

    it('getFitsCutUrl() should return test/fits-cut/test?filename=test&ra=5&dec=10&radius=3', () => {
        let expected = 'test/fits-cut/test?filename=test&ra=5&dec=10&radius=3';
        component.dataset = { ...dataset, name: 'test' };
        component.coneSearch = { dec: 10, ra: 5, radius: 3 }
        expect(component.getFitsCutUrl({ ...image, file_path: 'test', stretch: 'test', pmax: 10, pmin: 5 })).toEqual(expected);
    });

    it('should raises download file event', () => {
        let event = { preventDefault: jest.fn() };
        component.getFitsCutUrl = jest.fn().mockImplementationOnce(() => 'test');
        let spy = jest.spyOn(component.downloadFile, 'emit');
        component.saveFitsCutFile(event, { ...image, file_path: 'test/test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ url: 'test', filename: 'test' })
    });

    it('should raises download file event', () => {

        component.getFitsCutUrl = jest.fn().mockImplementationOnce(() => 'test');
        let spy = jest.spyOn(component.broadcastImage, 'emit');
        component.broadcast({ ...image, file_path: 'test/test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test');
    });

    it('should raises emitBackGroundHref and openPlotImage events', () => {
        let spyOnemitBackGroundHref = jest.spyOn(component.emitBackGroundHref, 'emit');
        let spyOnopenPlotImage = jest.spyOn(component.openPlotImage, 'emit');
        component.openConeSearch('test');
        expect(spyOnemitBackGroundHref).toHaveBeenCalledWith('test');
        expect(spyOnopenPlotImage).toHaveBeenCalledWith(true);
    })
});
