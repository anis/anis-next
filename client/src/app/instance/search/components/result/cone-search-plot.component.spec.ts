/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ConeSearchPlotComponent } from "./cone-search-plot.component";

describe('[instance][search][components][result] ConeSearchPlotComponent', () => {
    let component: ConeSearchPlotComponent;
    let fixture: ComponentFixture<ConeSearchPlotComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ConeSearchPlotComponent],
        });
        fixture = TestBed.createComponent(ConeSearchPlotComponent);
        component = fixture.componentInstance;
        component.coneSearchPlot = jest.fn().mockImplementationOnce(() => { })
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
})