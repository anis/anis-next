/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ResultInfoComponent } from './result-info.component';
import { DownloadResultComponent } from './download-result.component';
import { ImageListResultComponent } from './image-list-result.component';
import { ImageDisplayComponent } from './image-display.component';
import { DatatableActionsComponent } from './datatable-actions.component';
import { UrlDisplayComponent } from './url-display.component';
import { DatatableComponent } from './datatable.component';
import { ConeSearchPlotComponent } from './cone-search-plot.component';
import { DisplayResultRendererComponent } from './display-result-renderer.component';
import { ConeSearchImageComponent } from './cone-search-image.component';
import { rendererComponents } from './renderer';

export const resultComponents = [
    ResultInfoComponent,
    UrlDisplayComponent,
    ImageListResultComponent,
    ImageDisplayComponent,
    DatatableComponent,
    DatatableActionsComponent,
    ConeSearchPlotComponent,
    DownloadResultComponent,
    DisplayResultRendererComponent,
    ConeSearchImageComponent,
    rendererComponents
];
