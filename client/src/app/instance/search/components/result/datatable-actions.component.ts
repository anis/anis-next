/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { AbstractDownloadComponent } from './abstract-download.component';
import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-datatable-actions',
    templateUrl: 'datatable-actions.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatatableActionsComponent extends AbstractDownloadComponent {
    @Input() attributeList: Attribute[];
    @Input() selectedData: any[] = [];
    @Input() sampRegistered: boolean;
    @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();
    @Output() startTaskCreateArchive: EventEmitter<string> = new EventEmitter();

    isArchiveIsAvailable() {
        return this.attributeList
            .filter(attribute => this.outputList.includes(attribute.id))
            .filter(attribute => attribute.archive)
            .length > 0;
    }

    getDatatableUrl(format: string): string {
        const attributeId = this.attributeList.find(a => a.primary_key);
        return this.getUrl(format, `${attributeId.id}::in::${this.selectedData.join('|')}`);
    }
    
    broadcastResult() {
        const url = this.getDatatableUrl('votable');
        this.broadcastVotable.emit(url);
    }

    downloadArchive() {
        const attributeId = this.attributeList.find(a => a.primary_key);
        this.startTaskCreateArchive.emit(this.getQuery(`${attributeId.id}::in::${this.selectedData.join('|')}`));
    }
}
