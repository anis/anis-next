/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";

import { ConeSearch } from "src/app/instance/store/models";
import { Attribute, ConeSearchConfig, Dataset } from "src/app/metamodel/models";

@Component({
    selector: 'app-cone-search-image',
    templateUrl: 'cone-search-image.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchImageComponent {
    @Input() dataset: Dataset;
    @Input() attributeList: Attribute[];
    @Input() coneSearch: ConeSearch;
    @Input() coneSearchConfig: ConeSearchConfig;
    @Input() data: any;
    @Input() dataIsLoading: boolean;
    @Input() dataIsLoaded: boolean;
    @Input() backgroundHref: string;
    @Output() selectId: EventEmitter<any> = new EventEmitter();
    @Output() closeConeSearchPlotImageOutPut: EventEmitter<boolean> = new EventEmitter();

    getData() {
        const columnId = this.attributeList.find(a => a.primary_key);
        const columnRa = this.attributeList.find(a => a.id === this.coneSearchConfig.column_ra);
        const columnDec = this.attributeList.find(a => a.id === this.coneSearchConfig.column_dec);
        return this.data.map(d => ({
            "id": d[columnId.label],
            "x": +d[columnRa.label],
            "y": +d[columnDec.label]
        }));
    }
    
    closeConeSearchPlotImage(){
        this.closeConeSearchPlotImageOutPut.emit(false);
        this.selectId.emit(null);
    }
}
