/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Dataset, Image } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-image-display',
    templateUrl: 'image-display.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageDisplayComponent {
    @Input() dataset: Dataset;
    @Input() coneSearch: ConeSearch;
    @Input() image: Image;
    @Input() sampRegistered: boolean;
    @Output() broadcastImage: EventEmitter<string> = new EventEmitter();
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();
    @Output() emitBackGroundHref: EventEmitter<string> = new EventEmitter();
    @Output() openPlotImage: EventEmitter<boolean> = new EventEmitter();

    loading = true;

    constructor(private config: AppConfigService) { }

    onLoad() {
        this.loading = false;
    }

    getHref(image: Image) {
        let href = `${this.config.servicesUrl}/fits-cut-to-png/${this.dataset.name}?filename=${image.file_path}`;
        href += `&ra=${this.coneSearch.ra}`;
        href += `&dec=${this.coneSearch.dec}`;
        href += `&radius=${this.coneSearch.radius}`;
        href += `&stretch=${image.stretch}`;
        href += `&pmin=${image.pmin}`;
        href += `&pmax=${image.pmax}`;
        href += `&axes=false`;
        return href;
    }

    getFitsCutUrl(image: Image) {
        let url = `${this.config.servicesUrl}/fits-cut/${this.dataset.name}?filename=${image.file_path}`;
        url += `&ra=${this.coneSearch.ra}`;
        url += `&dec=${this.coneSearch.dec}`;
        url += `&radius=${this.coneSearch.radius}`;
        return url;
    }

    saveFitsCutFile(event, image: Image) {
        event.preventDefault();

        const url = this.getFitsCutUrl(image);
        const filename = image.file_path.substring(image.file_path.lastIndexOf('/') + 1);

        this.downloadFile.emit({url, filename});
    }

    broadcast(image: Image) {
        this.broadcastImage.emit(
            this.getFitsCutUrl(image)
        );
    }

    openConeSearch(backgroundHref: string): void {
        this.emitBackGroundHref.emit(backgroundHref);
        this.openPlotImage.emit(true);
    }
}