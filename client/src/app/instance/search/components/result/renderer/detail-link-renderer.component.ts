/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { AbstractRendererComponent } from 'src/app/instance/search/shared-renderer/abstract-renderer.component';
import { DetailLinkRendererConfig } from 'src/app/metamodel/models/renderers/detail-link-renderer-config.model';

/**
 * @class
 * @classdesc Detail link renderer component.
 */
@Component({
    selector: 'app-detail-link-renderer',
    templateUrl: 'detail-link-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailLinkRendererComponent extends AbstractRendererComponent {
    getConfig() {
        return this.attribute.renderer_config as DetailLinkRendererConfig;
    }
}
