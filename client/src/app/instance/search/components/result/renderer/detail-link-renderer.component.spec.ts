/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailLinkRendererComponent } from './detail-link-renderer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Attribute, DetailLinkRendererConfig } from 'src/app/metamodel/models';

describe('[Instance][Search][Component][Result][Renderer] DetailRendererComponent', () => {
    let component: DetailLinkRendererComponent;
    let fixture: ComponentFixture<DetailLinkRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [DetailLinkRendererComponent]
        });
        fixture = TestBed.createComponent(DetailLinkRendererComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('getConfig() should  return the attribute render_config property', () => {

        let detailLinkRendererConfig: DetailLinkRendererConfig = {
            display: '',
            component: '',
            id: 'renderer-config'
        }
        let attribute: Attribute;
        component.attribute = { ...attribute, renderer_config: { ...detailLinkRendererConfig } }
        expect(component.getConfig()).toEqual(detailLinkRendererConfig);
    });
});
