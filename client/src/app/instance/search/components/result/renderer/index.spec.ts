/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { getResultRendererComponent } from "."
import { DetailLinkRendererComponent } from "./detail-link-renderer.component";

describe('[Instance][Search][Component][Result][Renderer] index', () => {

    it('should return DetailLinkRendererComponent', () => {
        let mockgetResultRendererComponent = getResultRendererComponent;
        expect(mockgetResultRendererComponent(('detail-link'))).toEqual(DetailLinkRendererComponent)
    })
})