/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { Attribute, Dataset } from "src/app/metamodel/models";
import { DownloadResultComponent } from "./download-result.component"

describe('[Instance][Search][Component][Result] DownloadResultComponent', () => {
    let component: DownloadResultComponent;
    let fixture: ComponentFixture<DownloadResultComponent>;
    let attribute: Attribute;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DownloadResultComponent
            ],
            providers: [
                AppConfigService
            ]
        });
        fixture = TestBed.createComponent(DownloadResultComponent);
        component = fixture.componentInstance;
        component.attributeList = [
            { ...attribute, id: 1, archive: true, primary_key: true }
        ]
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('isArchiveIsAvailable should return true', () => {
        component.outputList = [1];
        expect(component.isArchiveIsAvailable()).toBe(true);
    });
    it('should raises broadcastvotable event', () => {
        let spy = jest.spyOn(component.broadcastVotable, 'emit');
        component.getUrl = jest.fn().mockImplementationOnce(() => 'test.fr')
        component.broadcastResult();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test.fr');
    });
    it('should raises startTaskCreateArchive event', () => {
        let spy = jest.spyOn(component.startTaskCreateArchive, 'emit');
        component.getQuery = jest.fn().mockImplementationOnce(() => 'test/test')
        component.downloadArchive();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test/test');
    });
    it('should return true', () => {
        let dataset: Dataset;
        component.dataset = { ...dataset, download_json: true };
        expect(component.isDownloadEnabled()).toBe(true);
        component.dataset = { ...dataset, download_csv: true };
        expect(component.isDownloadEnabled()).toBe(true);
        component.dataset = { ...dataset, download_ascii: true };
        expect(component.isDownloadEnabled()).toBe(true);
        component.dataset = { ...dataset, download_vo: true };
        expect(component.isDownloadEnabled()).toBe(true);
    });

})