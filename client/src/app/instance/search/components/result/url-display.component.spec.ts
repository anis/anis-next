/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ToastrService } from 'ngx-toastr';

import { UrlDisplayComponent } from './url-display.component';
import { AppConfigService } from 'src/app/app-config.service';

describe('[Instance][Search][Component][Result] UrlDisplayComponent', () => {
    let component: UrlDisplayComponent;
    let fixture: ComponentFixture<UrlDisplayComponent>;
    let appConfigServiceStub = new AppConfigService();

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UrlDisplayComponent],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ],
            providers: [
                { provide: AppConfigService, useValue: appConfigServiceStub },
                { provide: ToastrService, useValue: { }}
            ]
        });
        fixture = TestBed.createComponent(UrlDisplayComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
