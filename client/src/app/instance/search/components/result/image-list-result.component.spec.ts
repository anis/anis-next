/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { ImageListResultComponent } from "./image-list-result.component";

describe('[Instance][Search][Component][Result] ImageListResultComponent', () => {
    let component: ImageListResultComponent;
    let fixture: ComponentFixture<ImageListResultComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ImageListResultComponent
            ],
            providers: [
                { provide: AppConfigService, useValue: { servicesUrl: 'test' } }
            ]
        });
        fixture = TestBed.createComponent(ImageListResultComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
