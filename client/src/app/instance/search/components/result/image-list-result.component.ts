/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Dataset, Image } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';

@Component({
    selector: 'app-image-list-result',
    templateUrl: 'image-list-result.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageListResultComponent {
    @Input() dataset: Dataset;
    @Input() coneSearch: ConeSearch;
    @Input() imageList: Image[];
    @Input() imageListIsLoading: boolean;
    @Input() imageListIsLoaded: boolean;
    @Input() sampRegistered: boolean;
    @Output() broadcastImage: EventEmitter<string> = new EventEmitter();
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();
    @Output() emitBackGroundHref: EventEmitter<string> = new EventEmitter();
    @Output() openPlotImage: EventEmitter<boolean> = new EventEmitter();
}
