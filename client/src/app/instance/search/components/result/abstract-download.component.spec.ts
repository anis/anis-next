/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from "@angular/core/testing";
import { AppConfigService } from "src/app/app-config.service";
import { Dataset } from "src/app/metamodel/models";
import { AbstractDownloadComponent } from "./abstract-download.component";
class TestAbstractDownloadComponent extends AbstractDownloadComponent {

}
describe('[instance][search][components][result] AbstractDownloadComponent', () => {
    let component: TestAbstractDownloadComponent;
    let dataset: Dataset;
    TestBed.configureTestingModule({
        declarations: [
            TestAbstractDownloadComponent
        ],
    });
    beforeEach(() => {
        let appService: AppConfigService = {
            apiUrl: "http://test.fr",
            servicesUrl: "",
            baseHref: "",
            authenticationEnabled: false,
            ssoAuthUrl: "",
            ssoRealm: "",
            ssoClientId: "",
            adminRoles: [],
            matomoEnabled: false,
            matomoSiteId: 0,
            matomoTrackerUrl: ""
        }
        component = new TestAbstractDownloadComponent(appService);
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getUrl(format: string, selectedData: string = null) should return http://test.fr/search/test', () => {
        component.getQuery = jest.fn().mockImplementationOnce(() => 'test');
        expect(component.getUrl('format_test')).toEqual('http://test.fr/search/test&f=format_test');
    });
    it('should return test_name?a=1;2&c=1;3;test when criterialist as at least one element', () => {
        component.outputList = [1, 2]
        component.criteriaList = [
            { id: 1, type: 'test1' },
            { id: 3, type: 'test2' }
        ];
        component.dataset = { ...dataset, name: 'test_name' }
        expect(component.getQuery('test')).toEqual('test_name?a=1;2&c=1;3;test');
    });
    it('should return test_name?a=1;2&c=1;3;test when criteria list as 0 element ', () => {
        component.outputList = [1, 2]
        component.criteriaList = [];
        component.dataset = { ...dataset, name: 'test_name' }
        expect(component.getQuery('test')).toEqual('test_name?a=1;2&c=test');
    });

    it('should return test_name?a=1;2&c=1;3;test&cs=3:5:1 when criteria list as 0 element and there is a conesearch', () => {
        component.outputList = [1, 2]
        component.criteriaList = [];
        component.dataset = { ...dataset, name: 'test_name' }
        component.coneSearch = {
            dec: 5,
            ra: 3,
            radius: 1
        }
        expect(component.getQuery('test')).toEqual('test_name?a=1;2&c=test&cs=3:5:1');
    });
    it('should raises download file event', () => {
        let spy = jest.spyOn(component.downloadFile, 'emit');
        component.dataset = { ...dataset, name: 'test_name' }
        const e = { preventDefault: jest.fn() };
        component.download(e, 'test.fr', '')
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should return json', () => {
        expect(component.formatToExtension('json')).toEqual('json');
    });
    it('should return csv', () => {
        expect(component.formatToExtension('csv')).toEqual('csv');
    });
    it('should return txt', () => {
        expect(component.formatToExtension('ascii')).toEqual('txt');
    });
    it('should return xml', () => {
        expect(component.formatToExtension('votable')).toEqual('xml');
    });
});