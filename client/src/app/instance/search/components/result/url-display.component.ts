/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { AbstractDownloadComponent } from './abstract-download.component';

/**
 * @class
 * @classdesc Search result URL display component.
 */
@Component({
    selector: 'app-url-display',
    templateUrl: 'url-display.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UrlDisplayComponent extends AbstractDownloadComponent { }
