/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Type } from '@angular/core';

import { AbstractDisplayRendererComponent } from 'src/app/instance/search/shared-renderer/abstract-display-renderer.component';
import { AbstractRendererComponent } from 'src/app/instance/search/shared-renderer/abstract-renderer.component';
import { getResultRendererComponent } from './renderer';

@Component({
    selector: 'app-display-result-renderer',
    templateUrl: 'display-result-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayResultRendererComponent extends AbstractDisplayRendererComponent {
    getRendererComponent(renderer: string): Type<AbstractRendererComponent> {
        return getResultRendererComponent(renderer);
    }
}
