/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ResultInfoComponent } from "./result-info.component";

describe('[Instance][Search][Component][Result] ResultInfoComponent', () => {
    let component: ResultInfoComponent;
    let fixture: ComponentFixture<ResultInfoComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ResultInfoComponent
            ],
        });
        fixture = TestBed.createComponent(ResultInfoComponent);
        component = fixture.componentInstance;
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getCriteriaLength() should return 3', () => {
        component.criteriaList = [
            { id: 1, type: 'test1' },
            { id: 1, type: 'test1' }
        ];
        component.coneSearch = { dec: 10, ra: 10, radius: 5 };
        expect(component.getCriteriaLength()).toEqual(3);
    });

});