/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { AbstractDownloadComponent } from './abstract-download.component';
import { Instance, Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-download-result',
    templateUrl: 'download-result.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DownloadResultComponent extends AbstractDownloadComponent{
    @Input() instance: Instance;
    @Input() attributeList: Attribute[];
    @Input() sampRegistered: boolean;
    @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
    @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();
    @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();
    @Output() startTaskCreateArchive: EventEmitter<string> = new EventEmitter();

    isArchiveIsAvailable() {
        return this.attributeList
            .filter(attribute => this.outputList.includes(attribute.id))
            .filter(attribute => attribute.archive)
            .length > 0;
    }

    broadcastResult() {
        const url = this.getUrl('votable');
        this.broadcastVotable.emit(url);
    }

    downloadArchive() {
        this.startTaskCreateArchive.emit(this.getQuery());
    }

    isDownloadEnabled() {
        return this.dataset.download_json
            || this.dataset.download_csv
            || this.dataset.download_ascii
            || this.dataset.download_vo;
    }
}