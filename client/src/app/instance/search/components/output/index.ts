/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OutputTabsComponent } from "./output-tabs.component";
import { OutputByFamilyComponent } from "./output-by-family.component";
import { OutputByCategoryComponent } from "./output-by-category.component";

export const outputComponents = [
    OutputTabsComponent,
    OutputByFamilyComponent,
    OutputByCategoryComponent
];
