/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Search output by category component.
 */
@Component({
    selector: 'app-output-by-category',
    templateUrl: 'output-by-category.component.html',
    styleUrls: ['output-by-category.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputByCategoryComponent {
    @Input() categoryLabel: string;
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    @Input() isAllSelected: boolean;
    @Input() isAllUnselected: boolean;
    @Output() change: EventEmitter<number[]> = new EventEmitter();

    /**
     * Checks if the given output ID is selected.
     *
     * @param  {number} id - The output ID.
     *
     * @return boolean
     */
    isSelected(id: number): boolean {
        return this.outputList.filter(i => i === id).length > 0;
    }

    /**
     * Toggles output selection for the given attribute ID and emits updated output list.
     *
     * @param  {number} attributeId - The attribute ID.
     *
     * @fires EventEmitter<number[]>
     */
    toggleSelection(attributeId: number): void {
        const clonedOutputList = [...this.outputList];
        const index = clonedOutputList.indexOf(attributeId);
        if (index > -1) {
            clonedOutputList.splice(index, 1);
        } else {
            clonedOutputList.push(attributeId);
        }
        this.change.emit(clonedOutputList);
    }

    /**
     * Selects all attributes and emits updated output list.
     *
     * @fires EventEmitter<number[]>
     */
    selectAll(): void {
        const clonedOutputList = [...this.outputList];
        const attributeListId = this.attributeList.map(a => a.id);
        attributeListId.filter(id => clonedOutputList.indexOf(id) === -1).forEach(id => {
            clonedOutputList.push(id);
        });
        this.change.emit(clonedOutputList);
    }

    /**
     * Unselects all attributes and emits updated output list.
     *
     * @fires EventEmitter<number[]>
     */
    unselectAll(): void {
        const clonedOutputList = [...this.outputList];
        const attributeListId = this.attributeList.map(a => a.id);
        attributeListId.filter(id => clonedOutputList.indexOf(id) > -1).forEach(id => {
            const index = clonedOutputList.indexOf(id);
            clonedOutputList.splice(index, 1);
        });
        this.change.emit(clonedOutputList);
    }
}
