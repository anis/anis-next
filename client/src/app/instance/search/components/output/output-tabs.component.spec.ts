/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { OutputTabsComponent } from './output-tabs.component';
import { Attribute, OutputCategory, OutputFamily } from '../../../../metamodel/models';

describe('[Instance][Search][Component][Output] OutputTabsComponent', () => {
    @Component({ selector: 'app-output-by-family', template: '' })
    class OutputByFamilyStubComponent {
        @Input() outputFamily: OutputFamily;
        @Input() outputCategoryList: OutputCategory[];
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() designColor: string;
    }

    let component: OutputTabsComponent;
    let fixture: ComponentFixture<OutputTabsComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputTabsComponent,
                OutputByFamilyStubComponent
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(OutputTabsComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('getOutputFamilyList() should return an array with one outputFamilyList with id 2', () => {
        let outputFamily: OutputFamily;
        component.outputFamilyList = [
            { display: 5, id: 1, label: 'test1', opened: true },
            { display: 10, id: 2, label: 'test2', opened: false }
        ]
        component.outputCategoryList = [{ display: 5, id: 2, id_output_family: 2, label: 'test1' }];
        let result = component.getOutputFamilyList();
        expect(result.length).toEqual(1);
        expect(result[0].id).toEqual(2);

    });
    it('should return an array with two  category', () => {

        component.outputCategoryList = [
            { display: 5, id: 2, id_output_family: 2, label: 'test1' },
            { display: 10, id: 1, id_output_family: 1, label: 'test2' },
            { display: 15, id: 3, id_output_family: 2, label: 'test3' }
        ];
        let result = component.getOutputCategoryListByFamily({ display: 10, id: 2, label: 'test', opened: false });
        expect(result.length).toEqual(2);
    });
});
