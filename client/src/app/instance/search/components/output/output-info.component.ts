/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input  } from '@angular/core';

import { SearchQueryParams } from 'src/app/instance/store/models';
import { Instance, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-output-info',
    templateUrl: './output-info.component.html',
    styleUrls: ['./output-info.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputInfoComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() datasetSelected: String;
    @Input() queryParams: SearchQueryParams;
}
