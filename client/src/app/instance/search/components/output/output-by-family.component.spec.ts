/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { OutputByFamilyComponent } from './output-by-family.component';
import { Attribute, OutputCategory, OutputFamily } from '../../../../metamodel/models';

describe('[Instance][Search][Component][Output] OutputByFamilyComponent', () => {
    @Component({ selector: 'app-output-by-category', template: '' })
    class OutputByCategoryStubComponent {
        @Input() categoryLabel: string;
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() designColor: string;
        @Input() isAllSelected: boolean;
        @Input() isAllUnselected: boolean;
    }

    let component: OutputByFamilyComponent;
    let fixture: ComponentFixture<OutputByFamilyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputByFamilyComponent,
                OutputByCategoryStubComponent
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(OutputByFamilyComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getCategoryListByFamily(idFamily) should return categories belonging to idFamily', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 2,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            }
        ];
        component.outputCategoryList = [
            {
                id: 1,
                label: 'Another output category',
                display: 20,
                id_output_family: 1
            },
            {
                id: 3,
                label: 'The last output category',
                display: 10,
                id_output_family: 2
            },
            {
                id: 2,
                label: 'Default output category',
                display: 10,
                id_output_family: 1
            }
        ];
        const sortedCategoryList: OutputCategory[] = component.getCategoryListByFamily(1);
        expect(sortedCategoryList.length).toBe(2);
        expect(sortedCategoryList[0].id).not.toBe(3);
        expect(sortedCategoryList[1].id).not.toBe(3);
    });

    it('#getAttributeByCategory(idCategory) should return attributes belonging to idCategory', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 2,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            }
        ];
        expect(component.getAttributeByCategory(1).length).toBe(1);
        expect(component.getAttributeByCategory(1)[0].id).toBe(1);
    });

    it('#getIsAllSelected(idCategory) should return true if all outputs of idCategory are selected', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            }
        ];
        component.outputList = [1, 2];
        expect(component.getIsAllSelected(1)).toBeTruthy();
    });

    it('#getIsAllSelected(idCategory) should return false if not all outputs of idCategory are selected', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            }
        ];
        component.outputList = [1];
        expect(component.getIsAllSelected(1)).toBeFalsy();
    });

    it('#getIsAllUnselected(idCategory) should return true if all outputs of idCategory are not selected', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            }
        ];
        component.outputList = [];
        expect(component.getIsAllUnselected(1)).toBeTruthy();
    });

    it('#getIsAllUnselected(idCategory) should return false if not all outputs of idCategory are not selected', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            }
        ];
        component.outputList = [1];
        expect(component.getIsAllUnselected(1)).toBeFalsy();
    });

    it('#emitChange(outputList) should raise change event', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: 1,
                id_detail_output_category: null
            }
        ];
        const expectedOutputList = [1];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.emitChange([1]);
    });
});
