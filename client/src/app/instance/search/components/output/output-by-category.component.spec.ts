/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputByCategoryComponent } from './output-by-category.component';

describe('[Instance][Search][Component][Output] OutputByCategoryComponent', () => {
    let component: OutputByCategoryComponent;
    let fixture: ComponentFixture<OutputByCategoryComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [OutputByCategoryComponent]
        });
        fixture = TestBed.createComponent(OutputByCategoryComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#isSelected(idOutput) should return true if output is selected', () => {
        component.outputList = [1];
        expect(component.isSelected(1)).toBeTruthy();
    });

    it('#isSelected(idOutput) should return false if output is not selected', () => {
        component.outputList = [1];
        expect(component.isSelected(2)).toBeFalsy();
    });

    it('#toggleSelection(idOutput) should remove idOutput from outputList and raise change event', () => {
        component.outputList = [1];
        const idOutput = 1;
        const expectedOutputList = [];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.toggleSelection(idOutput);
    });

    it('#toggleSelection(idOutput) should add idOutput to outputList and raise change event', () => {
        component.outputList = [];
        const idOutput = 1;
        const expectedOutputList = [1];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.toggleSelection(idOutput);
    });

    it('#selectAll() should add all outputs to outputList and raise change event', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type : 'field',
                operator : '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: null,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: null,
                id_detail_output_category: null
            }
        ];
        component.outputList = [];
        const expectedOutputList = [1, 2];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.selectAll();
    });

    it('#unselectAll() should remove all outputs to outputList and raise change event', () => {
        component.attributeList = [
            {
                id: 2,
                name: 'name_two',
                label: 'label_two',
                form_label: 'form_label_two',
                description : 'description_two',
                primary_key: false,
                type: '',
                search_type : 'field',
                operator : '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 1,
                output_display: 1,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 1,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: null,
                id_detail_output_category: null
            },
            {
                id: 1,
                name: 'name_one',
                label: 'label_one',
                form_label: 'form_label_one',
                description: 'description_one',
                primary_key: true,
                type: 'integer',
                search_type: 'field',
                operator: '=',
                dynamic_operator: false,
                min: null,
                max: null,
                placeholder_min: null,
                placeholder_max: null,
                criteria_display: 2,
                output_display: 2,
                selected: true,
                renderer: null,
                renderer_config: null,
                order_by: true,
                archive: false,
                detail_display: 2,
                detail_renderer: null,
                detail_renderer_config: null,
                options: null,
                vo_utype: null,
                vo_ucd: null,
                vo_unit: null,
                vo_description: null,
                vo_datatype: null,
                vo_size: null,
                id_criteria_family: null,
                id_output_category: null,
                id_detail_output_category: null
            }
        ];
        component.outputList = [1, 2];
        const expectedOutputList = [];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.unselectAll();
    });
});
