/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { OutputFamily, OutputCategory, Attribute } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Search output tabs component.
 */
@Component({
    selector: 'app-output-tabs',
    templateUrl: 'output-tabs.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputTabsComponent {
    @Input() outputFamilyList: OutputFamily[];
    @Input() outputCategoryList: OutputCategory[];
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    @Output() change: EventEmitter<number[]> = new EventEmitter();

    getOutputFamilyList() {
        return this.outputFamilyList
            .filter(outputFamily => this.getOutputCategoryListByFamily(outputFamily).length > 0);
    }

    getOutputCategoryListByFamily(outputFamily: OutputFamily) {
        return this.outputCategoryList.filter(category => category.id_output_family === outputFamily.id);
    }
}
