/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ProgressBarComponent } from './progress-bar.component';
import { CriteriaListParametersComponent } from './criteria-list-parameters.component';
import { datasetComponents } from './dataset';
import { criteriaComponents } from './criteria';
import { outputComponents } from './output';
import { OutputInfoComponent } from './output/output-info.component';
import { resultComponents } from './result';

export const dummiesComponents = [
    ProgressBarComponent,
    CriteriaListParametersComponent,
    OutputInfoComponent,
    datasetComponents,
    criteriaComponents,
    outputComponents,
    resultComponents
];
