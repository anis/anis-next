/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { SearchMultipleDatasetLength } from '../models';

export const initSearch = createAction('[Search Multiple] Init Search');
export const restartSearch = createAction('[Search Multiple] Restart Search');
export const markAsDirty = createAction('[Search Multiple] Mark As Dirty');
export const changeStep = createAction('[Search Multiple] Change Step', props<{ step: string }>());
export const checkDatasets = createAction('[Search Multiple] Check Datasets');
export const checkResult = createAction('[Search Multiple] Check Result');
export const updateSelectedDatasets = createAction('[Search Multiple] Update Selected Datasets', props<{ selectedDatasets: string[] }>());
export const retrieveDataLength = createAction('[Search Multiple] Retrieve Data Length');
export const retrieveDataLengthSuccess = createAction('[Search Multiple] Retrieve Data Length Success', props<{ dataLength: SearchMultipleDatasetLength[] }>());
export const retrieveDataLengthFail = createAction('[Search Multiple] Retrieve Data Length Fail');
