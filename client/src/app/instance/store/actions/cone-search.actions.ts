/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { ConeSearch, Resolver } from '../models';

export const addConeSearch = createAction('[ConeSearch] Add Cone Search', props<{ coneSearch: ConeSearch }>());
export const updateConeSearch = createAction('[ConeSearch] Update Cone Search', props<{ coneSearch: ConeSearch }>());
export const deleteConeSearch = createAction('[ConeSearch] Delete Cone Search');
export const retrieveCoordinates = createAction('[ConeSearch] Retrieve Coordinates', props<{ name: string }>());
export const retrieveCoordinatesSuccess = createAction('[ConeSearch] Retrieve Coordinates Success', props<{ resolver: Resolver }>());
export const retrieveCoordinatesFail = createAction('[ConeSearch] Retrieve Coordinates Fail');
