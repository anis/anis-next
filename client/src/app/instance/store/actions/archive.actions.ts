/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

export const startTaskCreateArchive = createAction('[Archive] Start Task Create Archive', props<{ query: string }>());
export const startTaskCreateArchiveSuccess = createAction('[Archive] Start Task Create Archive Success', props<{ fileId: string, filename: string, datasetName: string }>());
export const startTaskCreateArchiveFail = createAction('[Archive] Start Task Create Archive Fail');
export const isArchiveAvailable = createAction('[Archive] Is Archive Available', props<{ fileId: string, filename: string, datasetName: string }>());
export const isArchiveAvailableSuccess = createAction('[Archive] Is Archive Available Success', props<{ url: string, filename: string }>());
export const isArchiveAvailableFail = createAction('[Archive] Is Archive Available Fail');
export const resetArchive = createAction('[Archive] Reset Archive');