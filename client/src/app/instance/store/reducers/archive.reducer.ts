/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as archiveActions from '../actions/archive.actions';

/**
 * Interface for file state.
 *
 * @interface State
 */
export interface State {
    archiveIsCreating: boolean;
}

export const initialState: State = {
    archiveIsCreating: false
};

export const archiveReducer = createReducer(
    initialState,
    on(archiveActions.startTaskCreateArchiveSuccess, state => ({
        ...state,
        archiveIsCreating: true
    })),
    on(archiveActions.isArchiveAvailableSuccess, state => ({
        ...state,
        archiveIsCreating: false
    })),
    on(archiveActions.resetArchive, () => ({
        ...initialState
    }))
);

export const selectArchiveIsCreating = (state: State) => state.archiveIsCreating;
