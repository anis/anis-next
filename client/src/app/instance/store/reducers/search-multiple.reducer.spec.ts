import { Action } from '@ngrx/store';

import * as fromSearchMultiple from './search-multiple.reducer';
import * as searchMultipleActions from '../actions/search-multiple.actions';
import { SearchMultipleDatasetLength } from '../models';

describe('[Instance][Store] SearchMultiple reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromSearchMultiple;
        const action = { type: 'Unknown' };
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state).toBe(initialState);
    });

    it('restartSearch action should set currentStep to \'position\'', () => {
        const { initialState } = fromSearchMultiple;
        const action = searchMultipleActions.restartSearch();
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toEqual('position');
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('changeStep action should change the currentStep', () => {
        const { initialState } = fromSearchMultiple;
        const action = searchMultipleActions.changeStep({ step: 'myStep' });
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toEqual('myStep');
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('markAsDirty action should set pristine to false', () => {
        const { initialState } = fromSearchMultiple;
        const action = searchMultipleActions.markAsDirty();
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeFalsy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('checkDatasets action should set datasetsStepChecked to true', () => {
        const { initialState } = fromSearchMultiple;
        const action = searchMultipleActions.checkDatasets();
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetsStepChecked).toBeTruthy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('checkResult action should set resultStepChecked to true', () => {
        const { initialState } = fromSearchMultiple;
        const action = searchMultipleActions.checkResult();
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeTruthy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('updateSelectedDatasets action should set selectedDatasets', () => {
        const { initialState } = fromSearchMultiple;
        const selectedDatasets: string[] = ['myDataset'];
        const action = searchMultipleActions.updateSelectedDatasets({ selectedDatasets });
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(1);
        expect(state.selectedDatasets).toEqual(selectedDatasets);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataLength action should set dataLengthIsLoading to true and dataLengthIsLoaded to false', () => {
        const initialState = {
            ...fromSearchMultiple.initialState,
            dataLengthIsLoaded: true
        };
        const action = searchMultipleActions.retrieveDataLength();
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeTruthy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataLengthSuccess action should set dataLength, set dataLengthIsLoading to true and dataLengthIsLoaded to false', () => {
        const initialState = {
            ...fromSearchMultiple.initialState,
            dataLengthIsLoading: true
        };
        const dataLength: SearchMultipleDatasetLength[] = [{ datasetName: 'myDataset', length: 1 }];
        const action = searchMultipleActions.retrieveDataLengthSuccess({ dataLength });
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeTruthy();
        expect(state.dataLength.length).toEqual(1);
        expect(state.dataLength).toEqual(dataLength);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataLengthFail action should set dataLengthIsLoading to false', () => {
        const initialState = {
            ...fromSearchMultiple.initialState,
            dataLengthIsLoading: true
        };
        const action = searchMultipleActions.retrieveDataLengthFail();
        const state = fromSearchMultiple.searchMultipleReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('should get pristine', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectPristine(state)).toBeTruthy();
    });

    it('should get currentStep', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectCurrentStep(state)).toBeNull();
    });

    it('should get datasetsStepChecked', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectDatasetsStepChecked(state)).toBeFalsy();
    });

    it('should get resultStepChecked', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectResultStepChecked(state)).toBeFalsy();
    });

    it('should get selectedDatasets', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectSelectedDatasets(state).length).toEqual(0);
    });

    it('should get dataLengthIsLoading', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectDataLengthIsLoading(state)).toBeFalsy();
    });

    it('should get dataLengthIsLoaded', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectDataLengthIsLoaded(state)).toBeFalsy();
    });

    it('should get dataLength', () => {
        const action = {} as Action;
        const state =  fromSearchMultiple.searchMultipleReducer(undefined, action);

        expect(fromSearchMultiple.selectDataLength(state).length).toEqual(0);
    });
});
