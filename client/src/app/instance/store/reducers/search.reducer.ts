/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import { Criterion } from '../models';
import * as searchActions from '../actions/search.actions';

/**
 * Interface for search state.
 *
 * @interface State
 */
export interface State {
    pristine: boolean;
    currentDataset: string,
    currentStep: string;
    criteriaStepChecked: boolean;
    outputStepChecked: boolean;
    resultStepChecked: boolean;
    coneSearchAdded: boolean;
    criteriaList: Criterion[];
    outputList: number[];
    dataLengthIsLoading: boolean;
    dataLengthIsLoaded: boolean;
    dataLength: number;
    data: any[],
    dataIsLoading: boolean,
    dataIsLoaded: boolean,
    selectedData: any[];
}

export const initialState: State = {
    pristine: true,
    currentDataset: null,
    currentStep: null,
    criteriaStepChecked: false,
    outputStepChecked: false,
    resultStepChecked: false,
    coneSearchAdded: false,
    criteriaList: [],
    outputList: [],
    dataLengthIsLoading: false,
    dataLengthIsLoaded: false,
    dataLength: null,
    data: [],
    dataIsLoading: false,
    dataIsLoaded: false,
    selectedData: []
};

export const searchReducer = createReducer(
    initialState,
    on(searchActions.restartSearch, () => ({
        ...initialState,
        currentStep: 'criteria',
        criteriaStepChecked: true
    })),
    on(searchActions.changeStep, (state, { step }) => ({
        ...state,
        currentStep: step
    })),
    on(searchActions.markAsDirty, state => ({
        ...state,
        pristine: false
    })),
    on(searchActions.changeCurrentDataset, (state, { currentDataset }) => ({
        ...state,
        currentDataset
    })),
    on(searchActions.checkCriteria, state => ({
        ...state,
        criteriaStepChecked: true
    })),
    on(searchActions.checkOutput, state => ({
        ...state,
        outputStepChecked: true
    })),
    on(searchActions.checkResult, state => ({
        ...state,
        resultStepChecked: true
    })),
    on(searchActions.updateCriteriaList, (state, { criteriaList }) => ({
        ...state,
        criteriaList
    })),
    on(searchActions.addCriterion, (state, { criterion }) => ({
        ...state,
        criteriaList: [...state.criteriaList, criterion]
    })),
    on(searchActions.updateCriterion, (state, { updatedCriterion }) => ({
        ...state,
        criteriaList: state.criteriaList.map(
            criterion => (criterion.id === updatedCriterion.id) ? updatedCriterion : criterion
        )
    })),
    on(searchActions.deleteCriterion, (state, { idCriterion }) => ({
        ...state,
        criteriaList: [...state.criteriaList.filter(c => c.id !== idCriterion)]
    })),
    on(searchActions.updateOutputList, (state, { outputList }) => ({
        ...state,
        outputList
    })),
    on(searchActions.addSelectedData, (state, { id }) => ({
        ...state,
        selectedData: [...state.selectedData, id]
    })),
    on(searchActions.deleteSelectedData, (state, { id }) => ({
        ...state,
        selectedData: [...state.selectedData.filter(d => d !== id)]
    })),
    on(searchActions.retrieveDataLength, state => ({
        ...state,
        dataLengthIsLoading: true,
        dataLengthIsLoaded: false
    })),
    on(searchActions.retrieveDataLengthSuccess, (state, { length }) => ({
        ...state,
        dataLength: length,
        dataLengthIsLoading: false,
        dataLengthIsLoaded: true
    })),
    on(searchActions.retrieveDataLengthFail, state => ({
        ...state,
        dataLengthIsLoading: false
    })),
    on(searchActions.retrieveData, state => ({
        ...state,
        dataIsLoading: true,
        dataIsLoaded: false
    })),
    on(searchActions.retrieveDataSuccess, (state, { data }) => ({
        ...state,
        data,
        dataIsLoading: false,
        dataIsLoaded: true
    })),
    on(searchActions.retrieveDataFail, state => ({
        ...state,
        dataIsLoading: false
    })),
    on(searchActions.destroyResults, state => ({
        ...state,
        data: [],
        dataLength: null
    })),
    on(searchActions.resetSearch, () => ({
        ...initialState,
        currentStep: 'dataset'
    }))
);

export const selectPristine = (state: State) => state.pristine;
export const selectCurrentDataset = (state: State) => state.currentDataset;
export const selectCurrentStep = (state: State) => state.currentStep;
export const selectCriteriaStepChecked = (state: State) => state.criteriaStepChecked;
export const selectOutputStepChecked = (state: State) => state.outputStepChecked;
export const selectResultStepChecked = (state: State) => state.resultStepChecked;
export const selectIsConeSearchAdded = (state: State) => state.coneSearchAdded;
export const selectCriteriaList = (state: State) => state.criteriaList;
export const selectOutputList = (state: State) => state.outputList;
export const selectDataLengthIsLoading = (state: State) => state.dataLengthIsLoading;
export const selectDataLengthIsLoaded = (state: State) => state.dataLengthIsLoaded;
export const selectDataLength = (state: State) => state.dataLength;
export const selectData = (state: State) => state.data;
export const selectDataIsLoading = (state: State) => state.dataIsLoading;
export const selectDataIsLoaded = (state: State) => state.dataIsLoaded;
export const selectSelectedData = (state: State) => state.selectedData;
