import { Action } from '@ngrx/store';

import * as fromConeSearch from './cone-search.reducer';
import * as coneSearchActions from '../actions/cone-search.actions';
import { ConeSearch, Resolver } from '../models';

describe('[Instance][Store] ConeSearch reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromConeSearch;
        const action = { type: 'Unknown' };
        const state = fromConeSearch.coneSearchReducer(initialState, action);

        expect(state).toBe(initialState);
    });

    it('addConeSearch action should add conesearch', () => {
        const { initialState } = fromConeSearch;
        const coneSearch: ConeSearch = { ra: 1, dec: 2, radius: 3 };
        const action = coneSearchActions.addConeSearch({ coneSearch });
        const state = fromConeSearch.coneSearchReducer(initialState, action);

        expect(state.coneSearch).toEqual(coneSearch);
        expect(state.resolverIsLoading).toBeFalsy();
        expect(state.resolverIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('deleteConeSearch action should delete conesearch', () => {
        const initialState = {
            ...fromConeSearch.initialState,
            coneSearch: { ra: 1, dec: 2, radius: 3 }
        };
        const action = coneSearchActions.deleteConeSearch();
        const state = fromConeSearch.coneSearchReducer(initialState, action);

        expect(state.coneSearch).toBeNull();
        expect(state.resolverIsLoading).toBeFalsy();
        expect(state.resolverIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveCoordinates action should set resolverIsLoading to true and resolverIsLoaded to false', () => {
        const { initialState } = fromConeSearch;
        const action = coneSearchActions.retrieveCoordinates({ name: 'myObject' });
        const state = fromConeSearch.coneSearchReducer(initialState, action);

        expect(state.coneSearch).toBeNull();
        expect(state.resolverIsLoading).toBeTruthy();
        expect(state.resolverIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveCoordinatesSuccess action should set resolverIsLoading to false and resolverIsLoaded to true', () => {
        const { initialState } = fromConeSearch;
        const resolver: Resolver = { name: 'myObject', ra: 1, dec: 2 };
        const action = coneSearchActions.retrieveCoordinatesSuccess({ resolver });
        const state = fromConeSearch.coneSearchReducer(initialState, action);

        expect(state.coneSearch).toBeNull();
        expect(state.resolverIsLoading).toBeFalsy();
        expect(state.resolverIsLoaded).toBeTruthy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveCoordinatesFail action should set resolverIsLoading to false', () => {
        const initialState = {
            ...fromConeSearch.initialState,
            resolverIsLoading: true
        };
        const action = coneSearchActions.retrieveCoordinatesFail();
        const state = fromConeSearch.coneSearchReducer(initialState, action);

        expect(state.coneSearch).toBeNull();
        expect(state.resolverIsLoading).toBeFalsy();
        expect(state.resolverIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('should get coneSearch', () => {
        const action = {} as Action;
        const state =  fromConeSearch.coneSearchReducer(undefined, action);

        expect(fromConeSearch.selectConeSearch(state)).toBeNull();
    });

    it('should get resolverIsLoading', () => {
        const action = {} as Action;
        const state =  fromConeSearch.coneSearchReducer(undefined, action);

        expect(fromConeSearch.selectResolverIsLoading(state)).toBeFalsy();
    });

    it('should get resolverIsLoaded', () => {
        const action = {} as Action;
        const state = fromConeSearch.coneSearchReducer(undefined, action);

        expect(fromConeSearch.selectResolverIsLoaded(state)).toBeFalsy();
    });
});
