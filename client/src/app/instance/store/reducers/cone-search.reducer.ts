/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as coneSearchActions from '../actions/cone-search.actions';
import { ConeSearch } from '../models';

/**
 * Interface for cone search state.
 *
 * @interface State
 */
export interface State {
    coneSearch: ConeSearch;
    resolverIsLoading: boolean;
    resolverIsLoaded: boolean;
}

export const initialState: State = {
    coneSearch: null,
    resolverIsLoading: false,
    resolverIsLoaded: false
}

export const coneSearchReducer = createReducer(
    initialState,
    on(coneSearchActions.addConeSearch, (state, { coneSearch }) => ({
        ...state,
        coneSearch
    })),
    on(coneSearchActions.updateConeSearch, (state, { coneSearch }) => ({
        ...state,
        coneSearch
    })),
    on(coneSearchActions.deleteConeSearch, state => ({
        ...initialState
    })),
    on(coneSearchActions.retrieveCoordinates, state => ({
        ...state,
        resolverIsLoading: true,
        resolverIsLoaded: false
    })),
    on(coneSearchActions.retrieveCoordinatesSuccess, state => ({
        ...state,
        resolverIsLoading: false,
        resolverIsLoaded: true
    })),
    on(coneSearchActions.retrieveCoordinatesFail, state => ({
        ...state,
        resolverIsLoading: false
    }))
);

export const selectConeSearch = (state: State) => state.coneSearch;
export const selectResolverIsLoading = (state: State) => state.resolverIsLoading;
export const selectResolverIsLoaded = (state: State) => state.resolverIsLoaded;
