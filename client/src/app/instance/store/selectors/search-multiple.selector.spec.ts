import * as searchMultipleSelector from './search-multiple.selector';
import * as fromSearchMultiple from '../reducers/search-multiple.reducer';
import * as fromConeSearch from '../reducers/cone-search.reducer';
import { ConeSearch } from '../models';

describe('[Instance][Store] Search multiple selector', () => {
    it('should get selectPristine', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectPristine(state)).toBeTruthy();
    });

    it('should get currentStep', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectCurrentStep(state)).toBeNull();
    });

    it('should get datasetsStepChecked', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectDatasetsStepChecked(state)).toBeFalsy();
    });

    it('should get resultStepChecked', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectResultStepChecked(state)).toBeFalsy();
    });

    it('should get selectedDatasets', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectSelectedDatasets(state).length).toEqual(0);
    });

    it('should get dataLengthIsLoading', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectDataLengthIsLoading(state)).toBeFalsy();
    });

    it('should get dataLengthIsLoaded', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectDataLengthIsLoaded(state)).toBeFalsy();
    });

    it('should get dataLength', () => {
        const state = { instance: { searchMultiple: { ...fromSearchMultiple.initialState }}};
        expect(searchMultipleSelector.selectDataLength(state).length).toEqual(0);
    });

    it('should get queryParams', () => {
        const state = {
            instance: {
                searchMultiple: { ...fromSearchMultiple.initialState },
                coneSearch: { ...fromConeSearch.initialState }
            }
        };
        const expected = { };

        expect(searchMultipleSelector.selectQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with cone search', () => {
        const coneSearch: ConeSearch = { ra: 3, dec: 4, radius: 5 };
        const state = {
            instance: {
                searchMultiple: { ...fromSearchMultiple.initialState },
                coneSearch: {
                    ...fromConeSearch.initialState,
                    coneSearch
                }
            }
        };
        const expected = { cs: '3:4:5' };

        expect(searchMultipleSelector.selectQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with datasets', () => {
        const selectedDatasets: string[] = ['d1', 'd2'];
        const state = {
            instance: {
                searchMultiple: {
                    ...fromSearchMultiple.initialState,
                    selectedDatasets
                },
                coneSearch: { ...fromConeSearch.initialState }
            }
        };
        const expected = { d: 'd1;d2' };

        expect(searchMultipleSelector.selectQueryParams(state)).toEqual(expected);
    });

    it('should get steps by route', () => {
        const state = { router: { state: { queryParams: { d: 'd1;d2' }}}};
        expect(searchMultipleSelector.selectSelectedDatasetsByRoute(state)).toEqual('d1;d2');
    });
});
