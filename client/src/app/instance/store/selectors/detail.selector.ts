/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../instance.reducer';
import * as fromDetail from '../reducers/detail.reducer';

export const selectDetailState = createSelector(
    reducer.getInstanceState,
    (state: reducer.State) => state.detail
);

export const selectObject = createSelector(
    selectDetailState,
    fromDetail.selectObject
);

export const selectObjectIsLoading = createSelector(
    selectDetailState,
    fromDetail.selectObjectIsLoading
);

export const selectObjectIsLoaded = createSelector(
    selectDetailState,
    fromDetail.selectObjectIsLoaded
);

export const selectSpectraCSV = createSelector(
    selectDetailState,
    fromDetail.selectSpectraCSV
);

export const selectSpectraIsLoading = createSelector(
    selectDetailState,
    fromDetail.selectSpectraIsLoading
);

export const selectSpectraIsLoaded = createSelector(
    selectDetailState,
    fromDetail.selectSpectraIsLoaded
);

export const selectIdByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.params.id
);
