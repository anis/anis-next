/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../instance.reducer';
import * as fromConeSearch from '../reducers/cone-search.reducer';

export const selectConeSearchState = createSelector(
    reducer.getInstanceState,
    (state: reducer.State) => state.coneSearch
);

export const selectConeSearch = createSelector(
    selectConeSearchState,
    fromConeSearch.selectConeSearch
);

export const selectResolverIsLoading = createSelector(
    selectConeSearchState,
    fromConeSearch.selectResolverIsLoading
);

export const selectResolverIsLoaded = createSelector(
    selectConeSearchState,
    fromConeSearch.selectResolverIsLoaded
);

export const selectConeSearchByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.queryParams.cs as string
);
