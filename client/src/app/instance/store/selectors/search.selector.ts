/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import { Criterion, SearchQueryParams, criterionToString, ConeSearch, coneSearchToString } from '../models';
import * as reducer from '../../instance.reducer';
import * as fromSearch from '../reducers/search.reducer';
import * as coneSearchSelector from './cone-search.selector';

export const selectSearchState = createSelector(
    reducer.getInstanceState,
    (state: reducer.State) => state.search
);

export const selectPristine = createSelector(
    selectSearchState,
    fromSearch.selectPristine
);

export const selectCurrentDataset = createSelector(
    selectSearchState,
    fromSearch.selectCurrentDataset
);

export const selectCurrentStep = createSelector(
    selectSearchState,
    fromSearch.selectCurrentStep
);

export const selectCriteriaStepChecked = createSelector(
    selectSearchState,
    fromSearch.selectCriteriaStepChecked
);

export const selectOutputStepChecked = createSelector(
    selectSearchState,
    fromSearch.selectOutputStepChecked
);

export const selectResultStepChecked = createSelector(
    selectSearchState,
    fromSearch.selectResultStepChecked
);

export const selectIsConeSearchAdded = createSelector(
    selectSearchState,
    fromSearch.selectIsConeSearchAdded
);

export const selectCriteriaList = createSelector(
    selectSearchState,
    fromSearch.selectCriteriaList
);

export const selectOutputList = createSelector(
    selectSearchState,
    fromSearch.selectOutputList
);

export const selectDataLengthIsLoading = createSelector(
    selectSearchState,
    fromSearch.selectDataLengthIsLoading
);

export const selectDataLengthIsLoaded = createSelector(
    selectSearchState,
    fromSearch.selectDataLengthIsLoaded
);

export const selectDataLength = createSelector(
    selectSearchState,
    fromSearch.selectDataLength
);

export const selectDataIsLoading = createSelector(
    selectSearchState,
    fromSearch.selectDataIsLoading
);

export const selectDataIsLoaded = createSelector(
    selectSearchState,
    fromSearch.selectDataIsLoaded
);

export const selectData = createSelector(
    selectSearchState,
    fromSearch.selectData
);

export const selectSelectedData = createSelector(
    selectSearchState,
    fromSearch.selectSelectedData
);

export const selectQueryParams = createSelector(
    selectCriteriaList,
    coneSearchSelector.selectConeSearch,
    selectOutputList,
    selectCriteriaStepChecked,
    selectOutputStepChecked,
    selectResultStepChecked,
    (
        criteriaList: Criterion[],
        coneSearch: ConeSearch,
        outputList: number[],
        criteriaStepChecked: boolean,
        outputStepChecked: boolean,
        resultStepChecked: boolean) => {
        let step = '';
        step += (criteriaStepChecked) ? '1' : '0';
        step += (outputStepChecked) ? '1' : '0';
        step += (resultStepChecked) ? '1' : '0';
        let queryParams: SearchQueryParams = {
            s: step,
            a: outputList.join(';')
        };
        if (coneSearch) {
            queryParams = {
                ...queryParams,
                cs: coneSearchToString(coneSearch)
            };
        }
        if (criteriaList.length > 0) {
            queryParams = {
                ...queryParams,
                c: criteriaList.map(criterion => criterionToString(criterion)).join(';')
            };
        }
        return queryParams;
    }
);

export const selectStepsByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.queryParams.s as string
);

export const selectCriteriaListByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.queryParams.c as string
);

export const selectOutputListByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.queryParams.a as string
);
