/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import { SearchMultipleQueryParams, ConeSearch, coneSearchToString, SearchMultipleDatasetLength } from '../models';
import * as reducer from '../../instance.reducer';
import * as fromSearchMultiple from '../reducers/search-multiple.reducer';
import * as coneSearchSelector from './cone-search.selector';

export const selectSearchMultipleState = createSelector(
    reducer.getInstanceState,
    (state: reducer.State) => state.searchMultiple
);

export const selectPristine = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectPristine
);

export const selectCurrentStep = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectCurrentStep
);

export const selectDatasetsStepChecked = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectDatasetsStepChecked
);

export const selectResultStepChecked = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectResultStepChecked
);

export const selectSelectedDatasets = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectSelectedDatasets
);

export const selectDataLengthIsLoading = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectDataLengthIsLoading
);

export const selectDataLengthIsLoaded = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectDataLengthIsLoaded
);

export const selectDataLength = createSelector(
    selectSearchMultipleState,
    fromSearchMultiple.selectDataLength
);

export const selectQueryParams = createSelector(
    coneSearchSelector.selectConeSearch,
    selectSelectedDatasets,
    (
        coneSearch: ConeSearch,
        selectedDatasets: string[]) => {
        let queryParams: SearchMultipleQueryParams = { };
        if (coneSearch) {
            queryParams = {
                ...queryParams,
                cs: `${coneSearchToString(coneSearch)}`
            };
        }
        if (selectedDatasets.length > 0) {
            queryParams = {
                ...queryParams,
                d: selectedDatasets.join(';')
            };
        }
        return queryParams;
    }
);

export const selectDataFound = createSelector(
    selectDataLength,
    (searchMultipleDatasetLength: SearchMultipleDatasetLength[]) => {
        return searchMultipleDatasetLength.filter(
            searchMultipleDatasetLength => searchMultipleDatasetLength.length > 0
        ).length > 0;
    }
);

export const selectSelectedDatasetsByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.queryParams.d as string
);
