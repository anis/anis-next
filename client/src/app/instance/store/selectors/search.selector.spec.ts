import * as searchSelector from './search.selector';
import * as fromSearch from '../reducers/search.reducer';
import * as fromConeSearch from '../reducers/cone-search.reducer';
import { ConeSearch, Criterion, FieldCriterion } from '../models';

describe('[Instance][Store] Search selector', () => {
    it('should get selectPristine', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectPristine(state)).toBeTruthy();
    });

    it('should get currentDataset', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectCurrentDataset(state)).toBeNull();
    });

    it('should get currentStep', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectCurrentStep(state)).toBeNull();
    });

    it('should get criteriaStepChecked', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectCriteriaStepChecked(state)).toBeFalsy();
    });

    it('should get outputStepChecked', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectOutputStepChecked(state)).toBeFalsy();
    });

    it('should get resultStepChecked', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectResultStepChecked(state)).toBeFalsy();
    });

    it('should get coneSearchAdded', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectIsConeSearchAdded(state)).toBeFalsy();
    });

    it('should get criteriaList', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectCriteriaList(state).length).toEqual(0);
    });

    it('should get outputList', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectOutputList(state).length).toEqual(0);
    });

    it('should get dataLengthIsLoading', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectDataLengthIsLoading(state)).toBeFalsy();
    });

    it('should get dataLengthIsLoaded', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectDataLengthIsLoaded(state)).toBeFalsy();
    });

    it('should get dataLength', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectDataLength(state)).toBeNull();
    });

    it('should get data', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectData(state).length).toEqual(0);
    });

    it('should get dataIsLoading', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectDataIsLoading(state)).toBeFalsy();
    });

    it('should get dataIsLoaded', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectDataIsLoaded(state)).toBeFalsy();
    });

    it('should get selectedData', () => {
        const state = { instance: { search: { ...fromSearch.initialState }}};
        expect(searchSelector.selectSelectedData(state).length).toEqual(0);
    });

    it('should get queryParams without criteria', () => {
        const outputList: number[] = [1, 2];
        const state = {
            instance: {
                search: {
                    ...fromSearch.initialState,
                    outputList
                },
                coneSearch: { ...fromConeSearch.initialState }
            }
        };
        const expected = { s: '000', a: '1;2' };

        expect(searchSelector.selectQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with criteria', () => {
        const outputList: number[] = [1, 2];
        const criteriaList: Criterion[] = [
            { id: 1, type: 'field', operator: 'eq', value: 'one' } as FieldCriterion,
            { id: 2, type: 'field', operator: 'eq', value: 'two' } as FieldCriterion
        ];
        const state = {
            instance: {
                search: {
                    ...fromSearch.initialState,
                    outputList,
                    criteriaList
                },
                coneSearch: { ...fromConeSearch.initialState }
            }
        };
        const expected = { s: '000', a: '1;2', c: '1::eq::one;2::eq::two' };

        expect(searchSelector.selectQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with cone search', () => {
        const outputList: number[] = [1, 2];
        const coneSearchAdded: boolean = true;
        const coneSearch: ConeSearch = { ra: 3, dec: 4, radius: 5 };
        const state = {
            instance: {
                search: {
                    ...fromSearch.initialState,
                    outputList,
                    coneSearchAdded
                },
                coneSearch: {
                    ...fromConeSearch.initialState,
                    coneSearch
                }
            }
        };
        const expected = { s: '000', a: '1;2', cs: '3:4:5' };

        expect(searchSelector.selectQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with checked steps', () => {
        const criteriaStepChecked: boolean = true;
        const outputStepChecked: boolean = true;
        const resultStepChecked: boolean = true;
        const outputList: number[] = [1, 2];
        const state = {
            instance: {
                search: {
                    ...fromSearch.initialState,
                    criteriaStepChecked,
                    outputStepChecked,
                    resultStepChecked,
                    outputList
                },
                coneSearch: { ...fromConeSearch.initialState }
            }
        };
        const expected = { s: '111', a: '1;2' };

        expect(searchSelector.selectQueryParams(state)).toEqual(expected);
    });

    it('should get steps by route', () => {
        const state = { router: { state: { queryParams: { s: 'myParams' }}}};
        expect(searchSelector.selectStepsByRoute(state)).toEqual('myParams');
    });

    it('should get criteria by route', () => {
        const state = { router: { state: { queryParams: { c: 'myParams' }}}};
        expect(searchSelector.selectCriteriaListByRoute(state)).toEqual('myParams');
    });

    it('should get output by route', () => {
        const state = { router: { state: { queryParams: { a: 'myParams' }}}};
        expect(searchSelector.selectOutputListByRoute(state)).toEqual('myParams');
    });
});
