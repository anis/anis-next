/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { TestBed, waitForAsync } from "@angular/core/testing";
import { throwError } from "rxjs";
import { AppConfigService } from "src/app/app-config.service";
import { ArchiveService } from "./archive.service"

describe('[instance][store][services] ArchiveService', () => {
    let archiveService: ArchiveService;
    let httpController: HttpTestingController;
    let config: AppConfigService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                ArchiveService,
                { provide: AppConfigService, useValue: {
                    apiUrl: 'test',
                }},

            ]
        });
        archiveService = TestBed.inject(ArchiveService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
      
    }));
    it('#startTaskCreateArchive() should request return an Observable<any>', () => {
    
        archiveService.startTaskCreateArchive('test').subscribe();
        const url = 'test/start-task-create-archive/test';
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`,});
      
    });
    it('#isArchiveAvailable() should request return an Observable<any>', () => {
    
        archiveService.isArchiveAvailable('1').subscribe();
        const url = 'test/is-archive-available/1';
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`,});
      
    });
   


   



})