/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { instanceServices } from "."

describe('[instance][store][services] index', () => {
    it('test index', () => {
        expect(instanceServices.length).toEqual(4);
    });
})