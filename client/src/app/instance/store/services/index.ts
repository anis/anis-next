import { SearchService } from './search.service';
import { ConeSearchService } from './cone-search.service';
import { DetailService } from './detail.service';
import { ArchiveService } from './archive.service';

export const instanceServices = [
    SearchService,
    ConeSearchService,
    DetailService,
    ArchiveService
];
