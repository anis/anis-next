/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Search service.
 */
@Injectable()
export class SearchService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves results for the given parameters.
     *
     * @param  {string} query - The query.
     *
     * @return Observable<any[]>
     */
    retrieveData(query: string): Observable<any[]> {
        return this.http.get<any[]>(`${this.config.apiUrl}/search/${query}`);
    }

    /**
     * Retrieves results number for the given parameters.
     *
     * @param  {string} query - The query.
     *
     * @return Observable<{ nb: number }[]>
     */
    retrieveDataLength(query: string): Observable<{ nb: number }[]> {
        return this.http.get<{ nb: number }[]>(`${this.config.apiUrl}/search/${query}`);
    }
}
