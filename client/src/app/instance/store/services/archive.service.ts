/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConfigService } from 'src/app/app-config.service';

@Injectable({providedIn: 'root'})
export class ArchiveService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    startTaskCreateArchive(query: string) {
        return this.http.get<{"archive_name": string, "archive_id": string}>(`${this.config.apiUrl}/start-task-create-archive/${query}`);
    }

    isArchiveAvailable(id: string) {
        return this.http.get<{"archive_is_available": boolean}>(`${this.config.apiUrl}/is-archive-available/${id}`);
    }
}