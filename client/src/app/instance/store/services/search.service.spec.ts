/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SearchService } from './search.service';
import { AppConfigService } from 'src/app/app-config.service';

describe('[Instance][Store] SearchService', () => {
    let service: SearchService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                SearchService
            ]
        });
        service = TestBed.inject(SearchService);
    });

    it('#retrieveData() should return an Observable<any[]>',
        inject([HttpTestingController, SearchService], (httpMock: HttpTestingController, searchService: SearchService) => {
            const mockResponse = ['myData'];

            searchService.retrieveData('myQuery').subscribe((event: any[]) => {
                expect(event).toEqual(mockResponse);
            });

            const mockRequest = httpMock.expectOne('http://testing.com/search/myQuery');

            expect(mockRequest.cancelled).toBeFalsy();
            expect(mockRequest.request.responseType).toEqual('json');
            mockRequest.flush(mockResponse);

            httpMock.verify();
        }
        )
    );

    it('#retrieveDataLength() should return an Observable<{ nb: number }[]>',
        inject([HttpTestingController, SearchService], (httpMock: HttpTestingController, searchService: SearchService) => {
            const mockResponse = [{ nb: 1 }];

            searchService.retrieveDataLength('myQuery').subscribe((event: { nb: number }[]) => {
                expect(event).toEqual(mockResponse);
            });

            const mockRequest = httpMock.expectOne('http://testing.com/search/myQuery');

            expect(mockRequest.cancelled).toBeFalsy();
            expect(mockRequest.request.responseType).toEqual('json');
            mockRequest.flush(mockResponse);

            httpMock.verify();
        }
        )
    );
});
