/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DetailService } from './detail.service';
import { AppConfigService } from 'src/app/app-config.service';

describe('[Instance][Store] DetailService', () => {
    let service: DetailService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com', servicesUrl: 'http://testingService.com' } },
                DetailService
            ]
        });
        service = TestBed.inject(DetailService);
    });

    it('#retrieveObject() should return an Observable<any[]>',
        inject([HttpTestingController, DetailService], (httpMock: HttpTestingController, detailService: DetailService) => {
            const mockResponse = ['myData'];

            detailService.retrieveObject('myDataset', 1, 'myObject').subscribe((event: any[]) => {
                expect(event).toEqual(mockResponse);
            });

            const mockRequest = httpMock.expectOne('http://testing.com/search/myDataset?c=1::eq::myObject&a=all');

            expect(mockRequest.cancelled).toBeFalsy();
            expect(mockRequest.request.responseType).toEqual('json');
            mockRequest.flush(mockResponse);

            httpMock.verify();
        })
    );

    it('#retrieveSpectra() should return an Observable<string>',
        inject([HttpTestingController, DetailService], (httpMock: HttpTestingController, detailService: DetailService) => {
            const mockResponse = 'mySpectraData';

            detailService.retrieveSpectra('myDataset', 'mySpectraFile').subscribe((event: string) => {
                expect(event).toEqual(mockResponse);
            });

            const mockRequest = httpMock.expectOne('http://testingService.com/spectra-to-csv/myDataset?filename=mySpectraFile');

            expect(mockRequest.cancelled).toBeFalsy();
            expect(mockRequest.request.responseType).toEqual('text');
            mockRequest.flush(mockResponse);

            httpMock.verify();
        })
    );
});
