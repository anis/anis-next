/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
/**
 * @class
 * @classdesc Cone search service.
 */
export class ConeSearchService {
    constructor(private http: HttpClient) { }

    /**
     * Retrieves coordinates of an object name.
     *
     * @param  {string} name - The name of the object.
     *
     * @return Observable<any>
     */
    retrieveCoordinates(name: string): Observable<any> {
        const url = `https://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-ox/NSV?${name}`;
        return this.http.get(url, { responseType: 'text' });
    }
}
