/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Detail service.
 */
@Injectable()
export class DetailService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves object details for the given parameters.
     *
     * @param  {string} dname - The dataset name.
     * @param  {number} criterionId - The criterion ID.
     * @param  {string} objectSelected - The selected object ID.
     * @param  {number[]} outputList - The output list.
     *
     * @return Observable<any[]>
     */
    retrieveObject(dname: string, criterionId: number, objectSelected: string): Observable<any[]> {
        const query = `${dname}?c=${criterionId}::eq::${objectSelected}&a=all`;
        return this.http.get<any[]>(`${this.config.apiUrl}/search/${query}`);
    }

    /**
     * Retrieves spectra data for the given spectra file.
     *
     * @param  {string} dname - The dataset name.
     * @param  {string} spectraFile - The spectra file name.
     *
     * @return Observable<string>
     */
    retrieveSpectra(dname: string, spectraFile: string): Observable<string> {
        return this.http.get(`${this.config.servicesUrl}/spectra-to-csv/${dname}?filename=${spectraFile}`, { responseType: 'text' });
    }
}
