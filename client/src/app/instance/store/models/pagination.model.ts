/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for datatable pagination.
 *
 * @interface Pagination
 */
 export interface Pagination {
    dname: string;
    page: number;
    nbItems: number;
    sortedCol: number;
    order: PaginationOrder
}

/**
 * Enum for PaginationOrder values.
 * @readonly
 * @enum {string}
 */
export enum PaginationOrder {
    a = 'a',
    d = 'd'
}
