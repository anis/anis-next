import { criterionToString, stringToCriterion, getPrettyCriterion, getPrettyOperator } from './';
import {
    FieldCriterion,
    JsonCriterion,
    SelectMultipleCriterion,
    BetweenCriterion,
    Criterion,
    ListCriterion
} from '.';
import { Attribute } from '../../../metamodel/models';

describe('[Instance][Store] CriterionModel', () => {
    it('#criterionToString should convert criterion object to string', () => {
        let betweenCriterion: BetweenCriterion = { id: 1, type: 'between', min: 'un', max: null };
        let expected = '1::gte::un';
        expect(criterionToString(betweenCriterion)).toEqual(expected);
        betweenCriterion = { id: 1, type: 'between', min: null, max: 'deux' };
        expected = '1::lte::deux';
        expect(criterionToString(betweenCriterion)).toEqual(expected);
        betweenCriterion = { id: 1, type: 'between', min: 'un', max: 'deux' };
        expected = '1::bw::un|deux';
        expect(criterionToString(betweenCriterion)).toEqual(expected);
        const fieldCriterion = { id: 1, type: 'field', operator: 'eq', value: 'value' } as FieldCriterion;
        expected = '1::eq::value';
        expect(criterionToString(fieldCriterion)).toEqual(expected);
        const listCriterion = { id: 1, type: 'list', values: ['un', 'deux'] } as ListCriterion;
        expected = '1::in::un|deux';
        expect(criterionToString(listCriterion)).toEqual(expected);
        const jsonCriterion = { id: 1, type: 'json', path: 'path', operator: 'eq', value: 'value' } as JsonCriterion;
        expected = '1::js::path|eq|value';
        expect(criterionToString(jsonCriterion)).toEqual(expected);
        const selectMultipleCriterion = { id: 1, type: 'multiple', options: [{ label: 'un', value: '1', display: 1 }, { label: 'deux', value: '2', display: 2 }] } as SelectMultipleCriterion;
        expected = '1::in::1|2';
        expect(criterionToString(selectMultipleCriterion)).toEqual(expected);
    });
    it('criterionToString() should test case select-multiple-alias and return a criterion with id 1, type field', () => {
        let attribute: Attribute;
        attribute = { ...attribute, search_type: 'select-multiple-alias', id: 1 }
        let result = stringToCriterion(attribute, ['test', 'nnl']);
        expect(result.id).toEqual(1);
        expect(result.type).toEqual('field');

    });
    it('criterionToString() should test case select-multiple-alias and return a criterion with id 2, type multiple', () => {
        let attribute: Attribute;
        attribute = { ...attribute, search_type: 'select-multiple-alias', id: 2 }
        let result = stringToCriterion(attribute, ['test', 'test', 'test1|test2']);
        expect(result.id).toEqual(2);
        expect(result.type).toEqual('multiple');

    });
    it('criterionToString() should test case select-multiple-alias and return a criterion with id 2, type multiple', () => {
        let attribute: Attribute;
        attribute = { ...attribute, search_type: 'select-multiple-alias', id: 3, min: 'test1|test2' }
        let result = stringToCriterion(attribute);
        expect(result.id).toEqual(3);
        expect(result.type).toEqual('multiple');

    });
    it('criterionToString() should test case list and return a criterion with id 4, type field', () => {
        let attribute: Attribute;
        attribute = { ...attribute, search_type: 'list', id: 4, min: 'test1|test2' }
        let result = stringToCriterion(attribute, ['test', 'nnl']);
        expect(result.id).toEqual(4);
        expect(result.type).toEqual('field');

    });
    it('criterionToString() should test case between-date and return a criterion with id 5, type field', () => {
        let attribute: Attribute;
        attribute = { ...attribute, search_type: 'between-date', id: 5, min: 'test1|test2' }
        let result = stringToCriterion(attribute, ['test', 'nnl']);
        expect(result.id).toEqual(5);
        expect(result.type).toEqual('field');

    });
    it('criterionToString() should test case checkbox and return a criterion with id 6, type field', () => {
        let attribute: Attribute;
        attribute = { ...attribute, search_type: 'checkbox', id: 6, min: 'test1|test2' }
        let result = stringToCriterion(attribute, ['test', 'nnl']);
        expect(result.id).toEqual(6);
        expect(result.type).toEqual('field');

    });
    it('criterionToString() should test case svom_json_kw and return a criterion with id 6, type field', () => {
        let attribute: Attribute;
        attribute = { ...attribute, search_type: 'svom_json_kw', id: 6, min: 'test1|test2' }
        let result = stringToCriterion(attribute, ['test', 'nnl']);
        expect(result.id).toEqual(6);
        expect(result.type).toEqual('field');

    });
    

    it('#stringToCriterion should convert string criterion into object', () => {
        let attribute: Attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'field',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: null,
            max: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            options: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        let expected: Criterion = { id: 1, type: 'field', operator: 'neq', value: 'otherValue' } as FieldCriterion;
        expect(stringToCriterion(attribute, ['', 'neq', 'otherValue'])).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'field',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: 'value',
            max: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            options: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expected = { id: 1, type: 'field', operator: 'eq', value: 'value' } as FieldCriterion;
        expect(stringToCriterion(attribute)).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'list',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: 'value',
            max: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            options: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expected = { id: 1, type: 'list', values: ['one', 'two'] } as ListCriterion;
        expect(stringToCriterion(attribute, ['', '', 'one|two'])).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'list',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: 'valueA|valueB',
            max: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            options: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expected = { id: 1, type: 'list', values: ['valueA', 'valueB'] } as ListCriterion;
        expect(stringToCriterion(attribute)).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'between',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: 'one',
            max: 'two',
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            options: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expected = { id: 1, type: 'between', min: 'valueA', max: 'valueB' } as BetweenCriterion;
        expect(stringToCriterion(attribute, ['', 'bw', 'valueA|valueB'])).toEqual(expected);
        expected = { id: 1, type: 'between', min: 'valueA', max: null } as BetweenCriterion;
        expect(stringToCriterion(attribute, ['', 'gte', 'valueA'])).toEqual(expected);
        expected = { id: 1, type: 'between', min: null, max: 'valueB' } as BetweenCriterion;
        expect(stringToCriterion(attribute, ['', 'lte', 'valueB'])).toEqual(expected);
        expected = { id: 1, type: 'between', min: 'one', max: 'two' } as BetweenCriterion;
        expect(stringToCriterion(attribute)).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'select-multiple',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: '1|2',
            max: null,
            options: [
                { label: 'one', value: '1', display: 1 },
                { label: 'two', value: '2', display: 2 },
                { label: 'three', value: '3', display: 3 }
            ],
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expected = { id: 1, type: 'multiple', options: [{ label: 'one', value: '1', display: 1 }, { label: 'three', value: '3', display: 3 }] } as SelectMultipleCriterion;
        expect(stringToCriterion(attribute, ['', '', '1|3'])).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'select-multiple',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: '1|2',
            max: null,
            options: [
                { label: 'one', value: '1', display: 1 },
                { label: 'two', value: '2', display: 2 },
                { label: 'three', value: '3', display: 3 }
            ],
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expected = { id: 1, type: 'multiple', options: [{ label: 'one', value: '1', display: 1 }, { label: 'two', value: '2', display: 2 }] } as SelectMultipleCriterion;
        expect(stringToCriterion(attribute)).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'json',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: null,
            max: null,
            options: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expected = { id: 1, type: 'json', path: 'path', operator: 'op', value: 'value' } as JsonCriterion;
        expect(stringToCriterion(attribute, ['', '', 'path|op|value'])).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: 'json',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: 'path|op|value',
            max: null,
            options: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expect(stringToCriterion(attribute)).toEqual(expected);
        attribute = {
            id: 1,
            name: 'myAttribute',
            label: 'my attribute',
            form_label: 'My Attribute',
            description: null,
            primary_key: true,
            search_type: '',
            type: 'field',
            operator: 'eq',
            dynamic_operator: false,
            min: null,
            max: null,
            options: null,
            placeholder_min: null,
            placeholder_max: null,
            criteria_display: 1,
            output_display: 1,
            selected: true,
            renderer: null,
            renderer_config: null,
            order_by: true,
            archive: false,
            detail_display: 1,
            detail_renderer: null,
            detail_renderer_config: null,
            vo_utype: null,
            vo_ucd: null,
            vo_unit: null,
            vo_description: null,
            vo_datatype: null,
            vo_size: null,
            id_criteria_family: null,
            id_output_category: null,
            id_detail_output_category: null
        };
        expect(stringToCriterion(attribute)).toBeNull();
    });

    it('#getPrettyCriterion should print criterion correctly', () => {
        const fieldCriterion = { id: 1, type: 'field', operator: 'eq', value: 'value' } as FieldCriterion;
        let expectedPrintedCriterion = '= value';
        expect(getPrettyCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        const jsonCriterion = { id: 1, type: 'json', path: 'path', operator: 'eq', value: 'value' } as JsonCriterion;
        expectedPrintedCriterion = 'path eq value';
        expect(getPrettyCriterion(jsonCriterion)).toEqual(expectedPrintedCriterion);
        const selectMultipleCriterion = { id: 1, type: 'multiple', options: [{ label: 'un', value: '1', display: 1 }, { label: 'deux', value: '2', display: 2 }] } as SelectMultipleCriterion;
        expectedPrintedCriterion = '[un,deux]';
        expect(getPrettyCriterion(selectMultipleCriterion)).toEqual(expectedPrintedCriterion);
        let betweenCriterion = { id: 1, type: 'between', min: 'un', max: null } as BetweenCriterion;
        expectedPrintedCriterion = '>= un';
        expect(getPrettyCriterion(betweenCriterion)).toEqual(expectedPrintedCriterion);
        betweenCriterion = { id: 1, type: 'between', min: null, max: 'deux' } as BetweenCriterion;
        expectedPrintedCriterion = '<= deux';
        expect(getPrettyCriterion(betweenCriterion)).toEqual(expectedPrintedCriterion);
        betweenCriterion = { id: 1, type: 'between', min: 'un', max: 'deux' } as BetweenCriterion;
        expectedPrintedCriterion = '∈ [un;deux]';
        expect(getPrettyCriterion(betweenCriterion)).toEqual(expectedPrintedCriterion);
        const listCriterion = { id: 1, type: 'list', values: ['un', 'deux'] } as ListCriterion;
        expectedPrintedCriterion = '= [un,deux]';
        expect(getPrettyCriterion(listCriterion)).toEqual(expectedPrintedCriterion);
        const notCriterion = { id: 1, type: null } as Criterion;
        expectedPrintedCriterion = 'Criterion type not valid!';
        expect(getPrettyCriterion(notCriterion)).toEqual(expectedPrintedCriterion);
    });

    it('#getPrettyOperator() should prettify operator', () => {
        expect(getPrettyOperator('eq')).toEqual('=');
        expect(getPrettyOperator('neq')).toEqual('≠');
        expect(getPrettyOperator('gt')).toEqual('>');
        expect(getPrettyOperator('gte')).toEqual('>=');
        expect(getPrettyOperator('lt')).toEqual('<');
        expect(getPrettyOperator('lte')).toEqual('<=');
        expect(getPrettyOperator('lk')).toEqual('like');
        expect(getPrettyOperator('nlk')).toEqual('not like');
        expect(getPrettyOperator('in')).toEqual('in');
        expect(getPrettyOperator('nin')).toEqual('not in');
        expect(getPrettyOperator('toto')).toEqual('toto');
        expect(getPrettyOperator('nl')).toEqual('is null');
        expect(getPrettyOperator('nnl')).toEqual('is not null');


        expect(getPrettyOperator('')).toEqual('');
        expect(getPrettyOperator('')).toEqual('');
    });
});
