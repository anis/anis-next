/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @class
 * @classdesc Cone search class.
 */
export class ConeSearch {
    ra: number;
    dec: number;
    radius: number;
}

/**
 * Returns cone-search notation for Anis Server.
 *
 * @param  {ConeSearch} coneSearch - The cone-search to transform.
 *
 * @return string
 *
 * @example
 * coneSearchToString(coneSearch)
 */
export const coneSearchToString = (coneSearch: ConeSearch): string => {
    return `${coneSearch.ra}:${coneSearch.dec}:${coneSearch.radius}`;
};

/**
 * Returns cone-search object from serialized cone-search notation.
 *
 * @param  {string[]} params - The cone-search parameters.
 *
 * @return ConeSearch
 *
 * @example
 * stringToCriterion('280.5:0.0:10')
 */
export const stringToConeSearch = (params: string): ConeSearch => {
    const csParams = params.split(':');
    return {
        ra: +csParams[0],
        dec: +csParams[1],
        radius: +csParams[2]
    };
}
