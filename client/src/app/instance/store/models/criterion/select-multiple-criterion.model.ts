/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Criterion } from '../criterion.model';
import { Option } from 'src/app/metamodel/models/option.model';

/**
 * Interface for select multiple criterion.
 *
 * @interface SelectMultipleCriterion
 */
export interface SelectMultipleCriterion extends Criterion {
    id: number;
    type: string;
    options: Option[];
}
