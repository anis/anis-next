/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    BetweenCriterion,
    FieldCriterion,
    JsonCriterion,
    SelectMultipleCriterion,
    ListCriterion
} from './criterion';
import { Attribute } from 'src/app/metamodel/models';

/**
 * Interface for criterion.
 *
 * @interface Criterion
 */
export interface Criterion {
    id: number;
    type: string;
}

/**
 * Returns criterion notation for Anis Server.
 *
 * @param  {Criterion} criterion - The criterion to transform.
 *
 * @return string
 *
 * @example
 * criterionToString(criterion)
 */
export const criterionToString = (criterion: Criterion): string => {
    let str: string = criterion.id.toString();
    if (criterion.type === 'between') {
        const bw = criterion as BetweenCriterion;
        if (bw.min === null) {
            str += `::lte::${bw.max}`;
        } else if (bw.max === null) {
            str += `::gte::${bw.min}`;
        } else {
            str += `::bw::${bw.min}|${bw.max}`;
        }
    }
    if (criterion.type === 'field') {
        const fd = criterion as FieldCriterion;
        str += `::${fd.operator}`;
        if (fd.operator != 'nl' && fd.operator != 'nnl') {
            str += `::${fd.value}`;
        }
    }
    if (criterion.type === 'list') {
        const ls = criterion as ListCriterion;
        str += `::in::${ls.values.join('|')}`;
    }
    if (criterion.type === 'json') {
        const json = criterion as JsonCriterion;
        str += `::js::${json.path}|${json.operator}|${json.value}`;
    }
    if (criterion.type === 'multiple') {
        const multiple = criterion as SelectMultipleCriterion;
        str += `::in::${multiple.options.map(option => option.value).join('|')}`;
    }
    return str;
}

/**
 * Returns criterion object from serialized criterion notation.
 *
 * @param  {Attribute} attribute - The criterion to transform.
 * @param  {string[]} params - The criterion parameters.
 *
 * @return Criterion
 *
 * @example
 * stringToCriterion(myAttribute, ['firstParameter', 'secondParameter'])
 */
export const stringToCriterion = (attribute: Attribute, params: string[] = null): Criterion => {
    switch (attribute.search_type) {
        case 'field':
        case 'select':
        case 'select-alias':
        case 'datalist':
        case 'radio':
        case 'date':
        case 'date-time':
        case 'time':
            if (params) {
                let value = null;
                if (params[1] != 'nl' && params[1] != 'nnl') {
                    value = params[2];
                }
                return { id: attribute.id, type: 'field', operator: params[1], value } as FieldCriterion;
            } else {
                let value = null;
                if (attribute.operator != 'nl' && attribute.operator != 'nnl') {
                    value = attribute.min.toString();
                }
                return { id: attribute.id, type: 'field', operator: attribute.operator, value } as FieldCriterion;
            }
        case 'list':
            if (params) {
                if (params[1] === 'nl' || params[1] === 'nnl') {
                    return { id: attribute.id, type: 'field', operator: params[1], value: null } as FieldCriterion;
                } else {
                    return { id: attribute.id, type: 'list', values: params[2].split('|') } as ListCriterion;
                }
            } else {
                return { id: attribute.id, type: 'list', values: attribute.min.toString().split('|') } as ListCriterion;
            }
        case 'between':
        case 'between-date':
            if (params) {
                if (params[1] === 'nl' || params[1] === 'nnl') {
                    return { id: attribute.id, type: 'field', operator: params[1], value: null } as FieldCriterion;
                } else if (params[1] === 'bw') {
                    const bwValues = params[2].split('|');
                    return { id: attribute.id, type: 'between', min: bwValues[0], max: bwValues[1] } as BetweenCriterion;
                } else if (params[1] === 'gte') {
                    return { id: attribute.id, type: 'between', min: params[2], max: null } as BetweenCriterion;
                } else {
                    return { id: attribute.id, type: 'between', min: null, max: params[2] } as BetweenCriterion;
                }
            } else {
                const min = (attribute.min) ? attribute.min.toString() : null;
                const max = (attribute.max) ? attribute.max.toString() : null;
                console.log(max);
                return { id: attribute.id, type: 'between', min, max } as BetweenCriterion;
            }
        case 'select-multiple':
        case 'checkbox':
            if (params) {
                if (params[1] === 'nl' || params[1] === 'nnl') {
                    return { id: attribute.id, type: 'field', operator: params[1], value: null } as FieldCriterion;
                } else {
                    const msValues = params[2].split('|');
                    const options = attribute.options.filter(option => msValues.includes(option.value));
                    return { id: attribute.id, type: 'multiple', options } as SelectMultipleCriterion;
                }
            } else {
                const msValues = attribute.min.toString().split('|');
                const options = attribute.options.filter(option => msValues.includes(option.value));
                return { id: attribute.id, type: 'multiple', options } as SelectMultipleCriterion;
            }
        case 'select-multiple-alias':
            if (params) {
                if (params[1] === 'nl' || params[1] === 'nnl') {
                    return { id: attribute.id, type: 'field', operator: params[1], value: null } as FieldCriterion;
                } else {
                    const msValues = params[2].split('|');
                    const options = msValues.map(value => ({
                        label: value,
                        value,
                        display: 10
                    }));
                    return { id: attribute.id, type: 'multiple', options } as SelectMultipleCriterion;
                }
            } else {
                const msValues = attribute.min.toString().split('|');
                const options = msValues.map(value => ({
                    label: value,
                    value,
                    display: 10
                }));
                return { id: attribute.id, type: 'multiple', options } as SelectMultipleCriterion;
            }
        case 'json':
        case 'svom_json_kw':
            if (params) {
                if (params[1] === 'nl' || params[1] === 'nnl') {
                    return { id: attribute.id, type: 'field', operator: params[1], value: null } as FieldCriterion;
                } else {
                    const [path, operator, value] = params[2].split('|');
                    return { id: attribute.id, type: 'json', path, operator, value } as JsonCriterion;
                }
            } else {
                const [path, operator, value] = attribute.min.toString().split('|');
                return { id: attribute.id, type: 'json', path, operator, value } as JsonCriterion;
            }
        default:
            return null;
    }
}

/**
 * Returns pretty criterion string.
 *
 * @param  {Criterion} criterion - The criterion to pretty print.
 *
 * @return string
 *
 * @example
 * {{ printCriterion(criterion) }}
 */
export const getPrettyCriterion = (criterion: Criterion): string => {
    switch (criterion.type) {
        case 'between':
            const bw = criterion as BetweenCriterion;
            if (bw.min === null) {
                return `<= ${bw.max}`;
            } else if (bw.max === null) {
                return `>= ${bw.min}`;
            } else {
                return `∈ [${bw.min};${bw.max}]`;
            }
        case 'field':
            const fd = criterion as FieldCriterion;
            let value = '';
            if (fd.operator != 'nl' && fd.operator != 'nnl') {
                value = `${fd.value.split('|').join(', ')}`;
            }
            return `${getPrettyOperator(fd.operator)} ${value}`;
        case 'list':
            const ls = criterion as ListCriterion;
            return `= [${ls.values.join(',')}]`;
        case 'json' :
            const json = criterion as JsonCriterion;
            return `${json.path} ${json.operator} ${json.value}`;
        case 'multiple':
            const multiple = criterion as SelectMultipleCriterion;
            return `[${multiple.options.map(option => option.label).join(',')}]`;
        default:
            return 'Criterion type not valid!';
    }
}

/**
 * Returns an Anis Server string operator to a pretty form label operator.
 *
 * @param  {string} operator - The operator to prettify.
 *
 * @return string
 *
 * @example
 * // returns =
 * getPrettyOperator('eq')
 */
export const getPrettyOperator = (operator: string): string => {
    switch (operator) {
        case 'eq':
            return '=';
        case 'neq':
            return '≠';
        case 'gt':
            return '>';
        case 'gte':
            return '>=';
        case 'lt':
            return '<';
        case 'lte':
            return '<=';
        case 'lk':
            return 'like';
        case 'nlk':
            return 'not like';
        case 'in':
            return 'in';
        case 'nin':
            return 'not in';
        case 'nl':
            return 'is null';
        case 'nnl':
            return 'is not null';
        default:
            return operator;
    }
}
