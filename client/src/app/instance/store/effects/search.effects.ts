/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store, Action, select } from '@ngrx/store';
import { of, combineLatest } from 'rxjs';
import { map, switchMap, tap, mergeMap, catchError, skipWhile } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { KeycloakService } from 'keycloak-angular';
import FileSaver from 'file-saver';

import { ConeSearch, criterionToString, stringToCriterion, coneSearchToString, stringToConeSearch } from '../models';
import { SearchService } from '../services/search.service';
import * as searchActions from '../actions/search.actions';
import * as attributeActions from 'src/app/metamodel/actions/attribute.actions';
import * as attributeSelector from 'src/app/metamodel/selectors/attribute.selector';
import * as criteriaFamilyActions from 'src/app/metamodel/actions/criteria-family.actions';
import * as outputFamilyActions from 'src/app/metamodel/actions/output-family.actions';
import * as outputCategoryActions from 'src/app/metamodel/actions/output-category.actions';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as searchSelector from '../selectors/search.selector';
import * as coneSearchActions from '../actions/cone-search.actions';
import * as coneSearchSelector from '../selectors/cone-search.selector';
import * as coneSearchConfigActions from 'src/app/metamodel/actions/cone-search-config.actions';
import * as coneSearchConfigSelector from 'src/app/metamodel/selectors/cone-search-config.selector';
import * as authSelector from 'src/app/auth/auth.selector';

/**
 * @class
 * @classdesc Search effects.
 */
@Injectable()
export class SearchEffects {
    /**
     * Calls actions to initialize search.
     */
    initSearch$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchActions.initSearch),
            concatLatestFrom(() => [
                this.store.select(datasetSelector.selectDatasetNameByRoute),
                this.store.select(searchSelector.selectCurrentDataset),
                this.store.select(searchSelector.selectPristine),
                this.store.select(searchSelector.selectStepsByRoute)
            ]),
            mergeMap(([, datasetName, currentDataset, pristine, steps]) => {
                // User has changed dataset: reload initial state and init search
                if (datasetName && currentDataset && datasetName !== currentDataset) {
                    return of(searchActions.restartSearch());
                }

                // User has selected a dataset or page is reloaded: load dataset metamodel
                if (datasetName && pristine) {
                    let actions: Action[] = [
                        searchActions.changeCurrentDataset({ currentDataset: datasetName }),
                        attributeActions.loadAttributeList(),
                        criteriaFamilyActions.loadCriteriaFamilyList(),
                        outputFamilyActions.loadOutputFamilyList(),
                        outputCategoryActions.loadOutputCategoryList(),
                        coneSearchConfigActions.loadConeSearchConfig(),
                        searchActions.tryLoadDefaultFormParameters()
                    ];
                    if (steps) {
                        if(steps[0] === '1') {
                            actions.push(searchActions.checkCriteria());
                        }
                        if(steps[1] === '1') {
                            actions.push(searchActions.checkOutput());
                        }
                        if(steps[2] === '1') {
                            actions.push(searchActions.checkResult());
                        }
                    }
                    return actions;
                }

                // User come back to the search module: reload initial state
                if(!datasetName && !pristine) {
                    return of(searchActions.resetSearch());
                }

                // User change step and it's the same search: No action
                return of({ type: '[No Action] Init Search' });
            })
        )
    );

    /**
     * Calls actions to restart search.
     */
    restartSearch$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchActions.restartSearch),
            mergeMap(() => [
                attributeActions.resetAttributeList(),
                coneSearchConfigActions.resetConeSearchConfig(),
                searchActions.initSearch()
            ])
        )
    );

    resetSearch$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchActions.resetSearch),
            mergeMap(() => [
                attributeActions.resetAttributeList(),
                coneSearchConfigActions.resetConeSearchConfig()
            ])
        )
    );

    tryLoadDefaultFormParameters$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchActions.tryLoadDefaultFormParameters),
            switchMap(() => combineLatest([
                this.store.pipe(select(attributeSelector.selectAttributeListIsLoaded)),
                this.store.pipe(select(coneSearchConfigSelector.selectConeSearchConfigIsLoaded)),
            ]).pipe(
                skipWhile(([attributeListIsLoaded, coneSearchConfigIsLoaded]) => !attributeListIsLoaded || !coneSearchConfigIsLoaded),
                map(() => searchActions.loadDefaultFormParameters())
            ))
        )
    );

    /**
     * Calls actions to load default form parameters.
     */
    loadDefaultFormParameters$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchActions.loadDefaultFormParameters),
            concatLatestFrom(() => [
                this.store.select(searchSelector.selectPristine),
                this.store.select(searchSelector.selectCurrentDataset),
                this.store.select(attributeSelector.selectAllAttributes),
                this.store.select(coneSearchConfigSelector.selectConeSearchConfig),
                this.store.select(searchSelector.selectCriteriaListByRoute),
                this.store.select(coneSearchSelector.selectConeSearchByRoute),
                this.store.select(searchSelector.selectOutputListByRoute)
            ]),
            mergeMap(([, pristine, currentDataset, attributeList, coneSearchConfig, criteriaList, coneSearch, outputList]) => {
                // Default form parameters already loaded or no dataset selected
                if (!pristine || !currentDataset) {
                    return of({ type: '[No Action] Load Default Form Parameters' });
                }

                // Update criteria list
                let defaultCriteriaList = [];
                if (criteriaList) {
                    // Build criteria list with the URL query parameters (c)
                    defaultCriteriaList = criteriaList.split(';').map((c: string) => {
                        const params = c.split('::');
                        const attribute = attributeList.find(a => a.id === parseInt(params[0], 10));
                        return stringToCriterion(attribute, params);
                    });
                }

                // Update cone search
                let defaultConeSearch: ConeSearch = null;
                if (coneSearch) {
                    // Build cone-search with the URL query parameters (cs)
                    defaultConeSearch = stringToConeSearch(coneSearch);
                }

                // Update output list
                let defaultOutputList = [];
                if (outputList) {
                    // Build output list with the URL query parameters (a)
                    defaultOutputList = outputList.split(';').map((o: string) => parseInt(o, 10));
                }

                // If the user has moved from step 1 to 2
                // outputList is empty and ANIS cand build
                // 1. default criteria list with the attribute list metamodel configuration
                // 2. default output list with the attribute list metamodel configuration
                if (!outputList) {
                    defaultCriteriaList = attributeList
                        .filter(attribute => attribute.id_criteria_family && attribute.search_type && (attribute.min || attribute.max))
                        .map(attribute => stringToCriterion(attribute));

                    if (coneSearchConfig 
                        && coneSearchConfig.default_ra !== null
                        && coneSearchConfig.default_dec !== null
                        && coneSearchConfig.default_radius !== null
                    ) {
                        defaultConeSearch = stringToConeSearch(
                            `${coneSearchConfig.default_ra}:${coneSearchConfig.default_dec}:${coneSearchConfig.default_radius}`
                        );
                    }

                    defaultOutputList = attributeList
                        .filter(attribute => attribute.selected && attribute.id_output_category)
                        .sort((a, b) => a.output_display - b.output_display)
                        .map(attribute => attribute.id);
                }

                // Returns actions and mark the form as dirty
                return [
                    searchActions.updateCriteriaList({ criteriaList: defaultCriteriaList }),
                    coneSearchActions.addConeSearch({ coneSearch: defaultConeSearch }),
                    searchActions.updateOutputList({ outputList: defaultOutputList }),
                    searchActions.markAsDirty()
                ];
            })
        )
    );

    /**
     * Calls actions to retrieve data length.
     */
    retrieveDataLength$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchActions.retrieveDataLength),
            concatLatestFrom(() => [
                this.store.select(datasetSelector.selectDatasetNameByRoute),
                this.store.select(searchSelector.selectCriteriaList),
                this.store.select(coneSearchSelector.selectConeSearch)
            ]),
            mergeMap(([, datasetName, criteriaList, coneSearch]) => {
                let query = `${datasetName}?a=count`;
                if (criteriaList.length > 0) {
                    query += `&c=${criteriaList.map(criterion => criterionToString(criterion)).join(';')}`;
                }
                if (coneSearch) {
                    query += `&cs=${coneSearchToString(coneSearch)}`;
                }

                return this.searchService.retrieveDataLength(query)
                    .pipe(
                        map((response: { nb: number }[]) => searchActions.retrieveDataLengthSuccess({ length: response[0].nb })),
                        catchError(() => of(searchActions.retrieveDataLengthFail()))
                    )
            })
        )
    );

    /**
     * Displays retrieve data length error notification.
     */
    retrieveDataLengthFail$ = createEffect(() => 
        this.actions$.pipe(
            ofType(searchActions.retrieveDataLengthFail),
            tap(() => this.toastr.error('Loading Failed', 'The search data length loading failed'))
        ), { dispatch: false}
    );

    /**
     * Calls actions to retrieve data.
     */
    retrieveData$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchActions.retrieveData),
            concatLatestFrom(() => [
                this.store.select(datasetSelector.selectDatasetNameByRoute),
                this.store.select(searchSelector.selectCriteriaList),
                this.store.select(coneSearchSelector.selectConeSearch),
                this.store.select(searchSelector.selectOutputList)
            ]),
            mergeMap(([action, datasetName, criteriaList, coneSearch, outputList]) => {
                let query = `${datasetName}?a=${outputList.join(';')}`;
                if (criteriaList.length > 0) {
                    query += `&c=${criteriaList.map(criterion => criterionToString(criterion)).join(';')}`;
                }
                if (coneSearch) {
                    query += `&cs=${coneSearchToString(coneSearch)}`;
                }

                query += `&p=${action.pagination.nbItems}:${action.pagination.page}`;
                query += `&o=${action.pagination.sortedCol}:${action.pagination.order}`;

                return this.searchService.retrieveData(query)
                    .pipe(
                        map((data: any[]) => searchActions.retrieveDataSuccess({ data })),
                        catchError(() => of(searchActions.retrieveDataFail()))
                    )
            })
        )
    );

    /**
     * Displays retrieve data error notification.
     */
    retrieveDataFail$ = createEffect(() => 
        this.actions$.pipe(
            ofType(searchActions.retrieveDataFail),
            tap(() => this.toastr.error('Loading Failed', 'The search data loading failed'))
        ), { dispatch: false}
    );

    downloadFile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(searchActions.downloadFile),
            concatLatestFrom(() => this.store.select(authSelector.selectIsAuthenticated)),
            tap(([action, isAuthenticated]) => {
                if (isAuthenticated) {
                    this.keycloak.getToken().then(token => {
                        let separator = '?';
                        if (action.url.indexOf('?') > -1) {
                            separator = '&';
                        }
                        FileSaver.saveAs(`${action.url}${separator}token=${token}`, action.filename);
                    });
                } else {
                    FileSaver.saveAs(action.url, action.filename);
                }
            })
        ), { dispatch: false}
    );

    constructor(
        private actions$: Actions,
        private searchService: SearchService,
        private store: Store<{ }>,
        private toastr: ToastrService,
        private keycloak: KeycloakService,
    ) {}
}
