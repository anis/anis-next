/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, Subject, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import * as fromArchive from 'src/app/instance/store/reducers/archive.reducer';
import { ArchiveEffects } from './archive.effects';
import { ArchiveService } from '../services/archive.service';
import { AppConfigService } from 'src/app/app-config.service';
import * as archiveActions from '../actions/archive.actions';
import * as searchActions from '../actions/search.actions';
import * as searchSelector from '../selectors/search.selector';
import * as fromSharedUtils from 'src/app/shared/utils';

describe('[Instance][Store] ArchiveEffects', () => {
    let actions = new Observable();
    let effects: ArchiveEffects;
    let metadata: EffectsMetadata<ArchiveEffects>;
    let archiveService: ArchiveService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = {
        coneSearch: { ...fromArchive.initialState }
    };
    let mockSearchSelectorSelectCurrentDataset;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ArchiveEffects,
                { provide: ArchiveService, useValue: { retrieveCoordinates: jest.fn() } },
                { provide: ToastrService, useValue: { error: jest.fn() } },
                { provide: AppConfigService, useValue: { apiUrl: 'test' } },
                provideMockActions(() => actions),
                provideMockStore({ initialState }),

            ]
        });
        effects = TestBed.inject(ArchiveEffects);
        metadata = getEffectsMetadata(effects);
        archiveService = TestBed.inject(ArchiveService);
        toastr = TestBed.inject(ToastrService);
        store = TestBed.inject(MockStore);
    });

    it('effect should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('startTaskCreateArchive$ effect', () => {
        it('should start create archive', () => {
            let param = { datasetName: 'test', fileId: '', filename: 'test' }
            const action = archiveActions.startTaskCreateArchive({ query: 'test' });
            const completion = archiveActions.startTaskCreateArchiveSuccess(param);
            archiveService.startTaskCreateArchive = jest.fn().mockImplementation(() => of({ archive_id: '', archive_name: 'test', datasetName: 'test' }));
            mockSearchSelectorSelectCurrentDataset = store.overrideSelector(searchSelector.selectCurrentDataset, 'test');
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion })
            expect(effects.startTaskCreateArchive$).toBeObservable(expected);
        });
        it('should trigger startTaskCreateArchiveFail', () => {
            const action = archiveActions.startTaskCreateArchive({ query: 'test' });
            const completion = archiveActions.startTaskCreateArchiveFail();
            archiveService.startTaskCreateArchive = jest.fn().mockImplementation((error) => throwError(() => error));
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion })
            expect(effects.startTaskCreateArchive$).toBeObservable(expected);
        });


    });
    describe('startTaskCreateArchiveFail$ effect', () => {
        it('should trigger fail toast error', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = archiveActions.startTaskCreateArchiveFail();
            actions = hot('a', { a: action });
            expect(effects.startTaskCreateArchiveFail$).toBeObservable(cold('a', { a: action }));
            expect(spy).toHaveBeenCalledTimes(1);

        });

    });
    describe('isArchiveAvailable$ effect', () => {
        it('should trigger isArchiveAvailableSuccess action', () => {
            let param = { datasetName: 'test', fileId: '', filename: 'test' }
            // @ts-ignore
            effects.kill$ = new Subject();
            let spy = jest.spyOn(fromSharedUtils, 'getHost');
            spy.mockImplementation(() => 'test');
            const action = archiveActions.isArchiveAvailable(param)
            const completion = archiveActions.isArchiveAvailableSuccess({ filename: 'test', url: 'test/download-archive/test/' });
            archiveService.isArchiveAvailable = jest.fn().mockImplementation(() => of({ archive_is_available: true }));
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion })
            expect(effects.isArchiveAvailable$).toBeObservable(expected);

        });
        it('should trigger [No Action] Is Archive Available action', () => {
            let param = { datasetName: 'test', fileId: '', filename: 'test' }
            const action = archiveActions.isArchiveAvailable(param)
            const completion = { type: '[No Action] Is Archive Available' };
            archiveService.isArchiveAvailable = jest.fn().mockImplementation(() => of({ archive_is_available: false }));
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion })
            expect(effects.isArchiveAvailable$).toBeObservable(expected);

        });
        it('should trigger isArchiveAvailableFail action', () => {
            let param = { datasetName: 'test', fileId: '', filename: 'test' }
            const action = archiveActions.isArchiveAvailable(param)
            const completion = archiveActions.isArchiveAvailableFail();
            archiveService.isArchiveAvailable = jest.fn().mockImplementation(() => of({ archive_is_available: true }));
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion })
            expect(effects.isArchiveAvailable$).toBeObservable(expected);

        });

    });
    describe('isArchiveAvailableSuccess$ effect', () => {
        it('should trigger downloadFile action ', () => {
            const action = archiveActions.isArchiveAvailableSuccess({ filename: 'test', url: 'test' });
            actions = hot('a', { a: action });
            const completion = searchActions.downloadFile({ filename: 'test', url: 'test' });
            expect(effects.isArchiveAvailableSuccess$).toBeObservable(cold('a', { a: completion }));

        });
    });
    describe('isArchiveAvailableFail$ effect', () => {
        it('should trigger fail toast error', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = archiveActions.isArchiveAvailableFail();
            actions = hot('a', { a: action });
            expect(effects.isArchiveAvailableFail$).toBeObservable(cold('a', { a: action }));
            expect(spy).toHaveBeenCalledTimes(1);

        });

    });
    describe('resetArchive$ effect', () => {
        it('should call unsuscribe on kill', () => {
            //@ts-ignore
            effects.kill$ = new Subject();
            //@ts-ignore
            let spy = jest.spyOn(effects.kill$, 'unsubscribe');
            const action = archiveActions.resetArchive();
            actions = hot('a', { a: action });
            expect(effects.resetArchive$).toBeObservable(cold('a', { a: action }));
            expect(spy).toHaveBeenCalledTimes(1);

        });

    });

});
