/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { SearchMultipleDatasetLength, coneSearchToString, stringToConeSearch } from '../models';
import { SearchService } from '../services/search.service';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as searchMultipleActions from '../actions/search-multiple.actions';
import * as searchMultipleSelector from '../selectors/search-multiple.selector';
import * as coneSearchActions from '../actions/cone-search.actions';
import * as coneSearchSelector from '../selectors/cone-search.selector';
import * as authSelector from 'src/app/auth/auth.selector';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import { AppConfigService } from 'src/app/app-config.service';
import { isDatasetAccessible } from 'src/app/shared/utils';

@Injectable()
export class SearchMultipleEffects {
    /**
     * Calls actions to initialize search multiple.
     */
    initSearch$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchMultipleActions.initSearch),
            concatLatestFrom(() => [
                this.store.select(searchMultipleSelector.selectPristine),
                this.store.select(coneSearchSelector.selectConeSearchByRoute),
                this.store.select(searchMultipleSelector.selectSelectedDatasetsByRoute),
                this.store.select(instanceSelector.selectInstanceByRouteName),
                this.store.select(datasetSelector.selectAllConeSearchDatasets),
                this.store.select(authSelector.selectIsAuthenticated),
                this.store.select(authSelector.selectUserRoles),
                this.store.select(datasetGroupSelector.selectAllDatasetGroups)
            ]),
            mergeMap(([, 
                pristine, 
                coneSearchByRoute, 
                selectedDatasetsByRoute, 
                instance, 
                datasetList, 
                isAuthenticated, 
                userRoles, 
                datasetGroupList
            ]) => {
                // Restart search
                if (!pristine && !coneSearchByRoute) {
                    return [
                        coneSearchActions.deleteConeSearch(),
                        searchMultipleActions.restartSearch()
                    ];
                }

                // Default form parameters already loaded or no dataset selected
                if (!pristine) {
                    return of({ type: '[No Action] Load Default Form Parameters' });
                }

                let actions: Action[] = [
                    searchMultipleActions.markAsDirty()
                ];

                // Update cone search
                if (coneSearchByRoute) {
                    const coneSearch = stringToConeSearch(coneSearchByRoute);
                    actions.push(coneSearchActions.addConeSearch({ coneSearch }));
                }

                // Update selected datasets
                if (selectedDatasetsByRoute) {
                    // Build output list with the URL query parameters (a)
                    const selectedDatasets = selectedDatasetsByRoute.split(';');
                    actions.push(
                        searchMultipleActions.updateSelectedDatasets({ selectedDatasets }),
                        searchMultipleActions.checkDatasets()
                    );
                } else if (instance.search_multiple_all_datasets_selected) {
                    const selectedDatasets = datasetList.filter(dataset => isDatasetAccessible(
                        dataset,
                        this.config.authenticationEnabled,
                        isAuthenticated,
                        datasetGroupList,
                        this.config.adminRoles,
                        userRoles
                    )).map(dataset => dataset.name);
                    actions.push(
                        searchMultipleActions.updateSelectedDatasets({ selectedDatasets })
                    );
                }

                // Returns actions and mark the form as dirty
                return actions;
            })
        )
    );

    /**
     * Calls actions to restart search multiple.
     */
    restartSearch$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchMultipleActions.restartSearch),
            map(() => searchMultipleActions.initSearch())
        )
    );

    /**
     * Calls actions to retrieve data length.
     */
    retrieveDataLength$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(searchMultipleActions.retrieveDataLength),
            concatLatestFrom(() => [
                this.store.select(searchMultipleSelector.selectSelectedDatasets),
                this.store.select(coneSearchSelector.selectConeSearch)
            ]),
            mergeMap(([, selectedDatasets, coneSearch]) => {
                const queries = selectedDatasets.map(datasetName => this.searchService.retrieveDataLength(
                    `${datasetName}?a=count&cs=${coneSearchToString(coneSearch)}`
                ).pipe(
                    map((response: { nb: number }[]) => ({ datasetName, length: response[0].nb }))
                ));

                return forkJoin(queries)
                    .pipe(
                        map((response: SearchMultipleDatasetLength[]) => searchMultipleActions.retrieveDataLengthSuccess({ dataLength: response })),
                        catchError(() => of(searchMultipleActions.retrieveDataLengthFail()))
                    )
            })
        )
    );

    /**
     * Displays retrieve data length error notification.
     */
    retrieveDataLengthFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(searchMultipleActions.retrieveDataLengthFail),
            tap(() => this.toastr.error('Loading Failed', 'The search multiple data length loading failed'))
        ), { dispatch: false }
    );

    constructor(
        private config: AppConfigService,
        private actions$: Actions,
        private searchService: SearchService,
        private store: Store<{ }>,
        private toastr: ToastrService
    ) {}
}
