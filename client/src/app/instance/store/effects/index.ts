import { SearchEffects } from './search.effects';
import { SearchMultipleEffects } from './search-multiple.effects';
import { ConeSearchEffects } from './cone-search.effects';
import { DetailEffects } from './detail.effects';
import { ArchiveEffects } from './archive.effects';

export const instanceEffects = [
    SearchEffects,
    SearchMultipleEffects,
    ConeSearchEffects,
    DetailEffects,
    ArchiveEffects
];
