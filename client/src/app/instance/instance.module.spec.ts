/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceModule } from "./instance.module";

describe('[instance] module', () => {
    it('test instance module', () => {
        expect(InstanceModule.name).toEqual('InstanceModule');
    });
});