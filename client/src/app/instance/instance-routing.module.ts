/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InstanceComponent } from './instance.component';
import { InstanceAuthGuard } from './instance-auth.guard';

const routes: Routes = [
    { 
        path: ':iname', component: InstanceComponent, canActivate: [InstanceAuthGuard], children: [
            { path: 'webpage', loadChildren: () => import('./webpage/webpage.module').then(m => m.WebpageModule) },
            { path: 'search', loadChildren: () => import('./search/search.module').then(m => m.SearchModule) },
            { path: 'search-multiple', loadChildren: () => import('./search-multiple/search-multiple.module').then(m => m.SearchMultipleModule) },
            { path: 'documentation', loadChildren: () => import('./documentation/documentation.module').then(m => m.DocumentationModule) }
        ]
    }
];

/**
 * @class
 * @classdesc Instance routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InstanceRoutingModule { }

export const routedComponents = [
    InstanceComponent
];
