import { Injectable } from '@angular/core';

import { Instance } from 'src/app/metamodel/models';
import { StyleService } from 'src/app/shared/services/style.service';

@Injectable({
    providedIn: 'root'
})
export class InstanceStyleService {
    constructor(private style: StyleService) { }

    public applyInstanceStyle(instance: Instance) {
        this.generalStyle(instance);
        this.navbarStyle(instance);
        this.footerStyle(instance);
        this.searchProgressBarStyle(instance);
        this.searchNextBackButtonsStyle(instance);
        this.searchFamilyStyle(instance);
        this.searchInfoStyle(instance);
        this.searchDatasetSelectionStyle(instance);
        this.searchCriteriaStyle(instance);
        this.searchOutputColumnsStyle(instance);
        this.searchResultPanel(instance);
        this.searchResultTable(instance);
    }

    private generalStyle(instance: Instance) {
        this.style.setStyles('.instance-main', {
            'background-color': instance.design_background_color,
            'color': instance.design_text_color,
            'font-family': instance.design_font_family
        });
        this.style.setStyle('.instance-main a', 'color', instance.design_link_color);
        this.style.setStyle('.instance-main a:hover', 'color', instance.design_link_hover_color);
        this.style.setStyle('.instance-main a.btn', 'color', '#212529');
    }

    private navbarStyle(instance: Instance) {
        this.style.setStyles('.navbar-instance', {
            'background-color': instance.navbar_background_color,
            'border-bottom': `1px solid ${instance.navbar_border_bottom_color}`,
            'font-family': instance.navbar_font_family
        });
        this.style.setStyle('.navbar-instance a, .navbar-instance .webpage-family-nav-mobile-label', 'color', instance.navbar_color_href);
        this.style.setStyle('.navbar-instance a:hover', 'color', instance.navbar_color_href);
        this.style.setStyle('.navbar-instance a.active', 'font-weight', 'bold');
        this.style.setStyles('.navbar-instance .dropdown-menu', {
            'background-color': instance.navbar_background_color
        });
        this.style.setStyles('.navbar-instance .dropdown-item:hover', {
            'background-color': instance.navbar_background_color,
            'color': instance.navbar_color_href
        })
        this.style.setStyles('.navbar-instance .dropdown-item.active', {
            'background-color': instance.navbar_background_color,
            'color': instance.navbar_color_href
        });
        this.style.setStyle('.navbar-instance .navbar-toggler', 'color', instance.navbar_color_href);
        this.style.setStyles('.navbar-instance #button-sign-in', {
            'color': instance.navbar_sign_in_btn_color,
            'border-color': instance.navbar_sign_in_btn_color
        });
        this.style.setStyles('.navbar-instance #button-sign-in:hover', {
            'background-color': instance.navbar_background_color
        });
        this.style.setStyles('.navbar-instance #button-basic', {
            'background-color': instance.navbar_background_color,
            'border': 'none'
        });
        this.style.setStyle('.navbar-instance #button-user', 'color', instance.navbar_user_btn_color);
    }

    private footerStyle(instance: Instance) {
        this.style.setStyles('.footer', {
            'background-color': instance.footer_background_color,
            'border-top': `1px solid ${instance.footer_border_top_color}`,
            'color': instance.footer_text_color,
        });
    }

    private searchProgressBarStyle(instance: Instance) {
        this.style.setStyle('.progress-bar-title', 'color', instance.progress_bar_title_color);
        this.style.setStyle('.progress-bar-subtitle', 'color', instance.progress_bar_subtitle_color);
        this.style.setStyle('.progress-navigation .progress.progress-with-circle', 'background-color', instance.progress_bar_color);
        this.style.setStyle('.progress-navigation .progress.progress-with-circle .progress-bar', 'background-color', instance.progress_bar_active_color);
        const progressTitleBold = (instance.progress_bar_text_bold) ? 'bold' : 'normal';
        this.style.setStyles('.progress-navigation .nav-link, .progress-navigation .nav-link.disabled', {
            'color': instance.progress_bar_text_color,
            'font-weight': progressTitleBold
        });
        this.style.setStyle('.progress-navigation .nav-link:hover', 'color', instance.progress_bar_text_color);
        this.style.setStyle('.progress-navigation .nav-item.checked .nav-link, .progress-navigation.nav-item.active .nav-link', 'color', instance.progress_bar_active_color);
        this.style.setStyles('.progress-navigation .nav-item .icon-circle', {
            'border-color': instance.progress_bar_color,
            'background-color': instance.progress_bar_circle_color,
            'color': instance.progress_bar_circle_icon_color
        });
        this.style.setStyles('.progress-navigation .nav-item.checked .icon-circle', {
            'background-color': instance.progress_bar_circle_color,
            'border-color': instance.progress_bar_active_color,
            'color': instance.progress_bar_active_color
        });
        this.style.setStyles('.progress-navigation .nav-item.active .icon-circle', {
            'border-color': instance.progress_bar_active_color,
            'background-color': instance.progress_bar_active_color,
            'color': instance.progress_bar_circle_icon_active_color
        });
    }

    private searchNextBackButtonsStyle(instance: Instance) {
        // Next button
        this.style.setStyles('.search-next.btn.btn-outline-primary', {
            'color': instance.search_next_btn_color,
            'border-color': instance.search_next_btn_color
        });
        this.style.setStyles('.search-next:hover.btn.btn-outline-primary', {
            'color': instance.search_next_btn_hover_text_color,
            'background-color': instance.search_next_btn_hover_color,
            'border-color': instance.search_next_btn_hover_color
        });

        // Back button
        this.style.setStyles('.search-back.btn.btn-outline-secondary', {
            'color': instance.search_back_btn_color,
            'border-color': instance.search_back_btn_color
        });
        this.style.setStyles('.search-back:hover.btn.btn-outline-secondary', {
            'color': instance.search_back_btn_hover_text_color,
            'background-color': instance.search_back_btn_hover_color,
            'border-color': instance.search_back_btn_hover_color
        });
    }

    private searchFamilyStyle(instance: Instance) {
        this.style.setStyle('.panel.card.custom-accordion', 'border-color', instance.family_border_color);
        this.style.setStyle('.panel.card.custom-accordion .card-header', 'border-bottom-color', instance.family_border_color);
        this.style.setStyle('.custom-accordion .panel-heading', 'background-color', instance.family_header_background_color);
        this.style.setStyle('.custom-accordion .panel-heading .btn-link', 'color', instance.family_title_color);
        const familyTitleBold = (instance.family_title_bold) ? 'bold' : 'normal';
        this.style.setStyle('.custom-accordion .panel-heading .btn-link', 'font-weight', familyTitleBold);
        this.style.setStyles('.custom-accordion .panel-body', {
            'color': instance.family_text_color,
            'background-color': `${instance.family_background_color}`
        });
        this.style.setStyle('.custom-accordion .panel-body .card', 'background-color', instance.family_background_color);
    }

    private searchInfoStyle(instance: Instance) {
        this.style.setStyles('.search-info.jumbotron', {
            'background-color': instance.search_info_background_color,
            'color': instance.search_info_text_color
        });
        this.style.setStyle('.search-info .btn.btn-outline-primary', 'color', '#007BFF');
        this.style.setStyle('.search-info .btn.btn-outline-primary:hover', 'color', '#FFFFFF');
    }

    private searchDatasetSelectionStyle(instance: Instance) {
        this.style.setStyles('.dataset-select-btn.btn.btn-outline-secondary', {
            'color': instance.dataset_select_btn_color,
            'border-color': instance.dataset_select_btn_color
        });
        this.style.setStyles('.dataset-select-btn:hover.btn.btn-outline-secondary', {
            'color': instance.dataset_select_btn_hover_text_color,
            'background-color': instance.dataset_select_btn_hover_color,
            'border-color': instance.dataset_select_btn_hover_color
        });
        this.style.setStyle('.search-dataset-selected', 'color', instance.dataset_selected_icon_color);
    }

    private searchCriteriaStyle(instance: Instance) {
        this.style.setStyles('.search_criterium', {
            'background-color': instance.search_criterion_background_color,
            'color': instance.search_criterion_text_color
        });
    }

    private searchOutputColumnsStyle(instance: Instance) {
        this.style.setStyle('.output_columns_selected', 'color', instance.output_columns_selected_color);

        this.style.setStyles('.select-all.btn.btn-outline-secondary', {
            'color': instance.output_columns_select_all_btn_color,
            'border-color': instance.output_columns_select_all_btn_color
        });
        this.style.setStyles('.select-all:not([disabled]):hover.btn.btn-outline-secondary', {
            'color': instance.output_columns_select_all_btn_hover_text_color,
            'background-color': instance.output_columns_select_all_btn_hover_color,
            'border-color': instance.output_columns_select_all_btn_hover_color
        });
    }

    private searchResultPanel(instance: Instance) {
        this.style.setStyles('.result-panel', {
            'border': `${instance.result_panel_border_size} solid ${instance.result_panel_border_color}`,
            'background-color': instance.result_panel_background_color,
            'color': instance.result_panel_text_color
        });
        this.style.setStyles('.result-panel h3', {
            'color': instance.result_panel_title_color,
            'font-weight': 'bold',
            'border-bottom': `1px solid ${instance.result_panel_border_color}`,
            'margin-bottom': '20px',
            'padding-left': '10px'
        });
    }

    private searchResultTable(instance: Instance) {
        // Result header (download + SAMP)
        this.style.setStyles('.search-info .btn.btn-primary', {
            'background-color': instance.result_download_btn_color,
            'border-color': instance.result_download_btn_color,
            'color': instance.result_download_btn_text_color
        });
        this.style.setStyles('.search-info .btn.btn-primary:hover', {
            'background-color': instance.result_download_btn_hover_color,
            'border-color': instance.result_download_btn_hover_color,
            'color': instance.result_download_btn_text_color
        });

        // Datatable button actions
        this.style.setStyles('.btn-datatable-actions.btn.btn-primary.dropdown-toggle', {
            'color': instance.result_datatable_actions_btn_text_color,
            'background-color': instance.result_datatable_actions_btn_color,
            'border-color': instance.result_datatable_actions_btn_color
        });
        this.style.setStyles('.btn-datatable-actions:not([disabled]):hover.btn.btn-primary', {
            'background-color': instance.result_datatable_actions_btn_hover_color,
            'border-color': instance.result_datatable_actions_btn_hover_color,
        });
        this.style.setStyles('.btn-datatable-actions:not([disabled]):focus.btn.btn-primary', {
            'box-shadow': 'none'
        });

        // Datatable
        if (instance.result_datatable_bordered) {
            this.style.setStyle('#datatable.table-bordered th, #datatable.table-bordered td', 'border', `1px solid ${instance.result_datatable_border_color}`);
            this.style.setStyle('#datatable.table-bordered thead th', 'border-bottom', `2px solid ${instance.result_datatable_border_color}`);
            if (instance.result_datatable_bordered_radius) {
                this.style.setStyles('.datatable-responsive.table-responsive', {
                    'border-top-left-radius': '0.3rem',
                    'border-top-right-radius': '0.3rem',
                });
            }
        } else {
            this.style.setStyle('#datatable.table th, #datatable.table td', 'border-top', `1px solid ${instance.result_datatable_border_color}`);
            this.style.setStyle('#datatable.table thead th', 'border-bottom', `2px solid ${instance.result_datatable_border_color}`);
        }
        this.style.setStyles('#datatable.table thead tr', {
            'background-color': instance.result_datatable_header_background_color,
            'color': instance.result_datatable_header_text_color
        });
        this.style.setStyles('#datatable.table thead tr .column-sorted', {
            'color': instance.result_datatable_sorted_active_color,
            'background-color': instance.result_datatable_header_background_color
        });
        this.style.setStyle('#datatable.table thead tr .click-to-sort .unsorted', 'color', instance.result_datatable_sorted_color);
        this.style.setStyles('#datatable.table thead tr .click-to-sort .on-hover', {
            'color': instance.result_datatable_sorted_color,
            'background-color': instance.result_datatable_header_background_color
        });
        this.style.setStyles('#datatable.table tbody tr', {
            'background-color': instance.result_datatable_rows_background_color,
            'color': instance.result_datatable_rows_text_color
        });
        this.style.setStyle('#datatable.table tbody tr.datum-selected-in-plot', 'background-color', instance.result_datatable_rows_selected_color);
        this.style.setStyle('#datatable.table tbody tr .checked', 'color', instance.result_datatable_rows_selected_color);
        this.style.setStyle('#datatable.table a, .detail a.btn', 'color', instance.result_datatable_link_color);
        this.style.setStyle('#datatable.table a, .detail a', 'text-decoration', 'none');
        this.style.setStyle('#datatable.table a.btn-outline-primary, .detail a.btn-outline-primary', 'border-color', instance.result_datatable_link_color);
        this.style.setStyle('#datatable.table a:hover, .detail a:hover', 'color', instance.result_datatable_link_hover_color);
        this.style.setStyle('#datatable.table a.btn-outline-primary:hover, .detail a.btn-outline-primary:hover', 'color', instance.result_datatable_link_hover_color);
        this.style.setStyle('#datatable.table a.btn-outline-primary:hover, .detail a.btn-outline-primary:hover', 'background-color', instance.result_datatable_rows_background_color);
        this.style.setStyle('#datatable.table a.btn-outline-primary:hover, .detail a.btn-outline-primary:hover', 'border-color', instance.result_datatable_link_hover_color);
    
        // Datatable pagination
        this.style.setStyle('.page-item > .page-link', 'color', instance.result_datatable_pagination_link_color);
        this.style.setStyles('.pagination-page.page-item.active > .page-link', {
            'backgroundColor': instance.result_datatable_pagination_active_bck_color,
            'borderColor': instance.result_datatable_pagination_active_bck_color,
            'color': instance.result_datatable_pagination_active_text_color
        });
    }
}
