/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AppConfigService } from '../app-config.service';
import * as authSelector from 'src/app/auth/auth.selector';
import { InstanceAuthGuard } from './instance-auth.guard';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';
import * as fromSharedUtils from 'src/app/shared/utils';
import { cold } from 'jasmine-marbles';
import { Instance } from '../metamodel/models';

describe('[instance] InstanceAuthGuard', () => {
    let store: MockStore;
    let mockinstanceSelectorInstanceListIsLoaded;
    let mockInstanceSelectorSelectInstanceByRouteName;
    let mockAuthSelectorSelectUserRoles;
    let mockAuthSelectorSelectIsAuthenticated;
    let mockInstanceGroupSelectorSelectAllInstanceGroups;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockStore({}),],
        })
    });
    it('should create instanceAuthGuard', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true } })
        store = TestBed.inject(MockStore);

        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        expect(instanceAuthGuard).toBeTruthy();
    });
    it('canActivate() should return true if authenticationEnabled is false', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true } })
        store = TestBed.inject(MockStore);
        mockinstanceSelectorInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, name: 'test' });
        mockAuthSelectorSelectUserRoles = store.overrideSelector(authSelector.selectUserRoles, []);
        mockAuthSelectorSelectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockInstanceGroupSelectorSelectAllInstanceGroups = store.overrideSelector(instanceGroupSelector.selectAllInstanceGroups, []);
        let spyOnIsAdmin = jest.spyOn(fromSharedUtils, 'isAdmin');
        spyOnIsAdmin.mockImplementation(() => true);

        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        let expected = cold('a', { a: true });
        expect(instanceAuthGuard.canActivate()).toBeObservable(expected);
    });
    it('canActivate() should return true if user is authenticated and authorized ', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true } })
        store = TestBed.inject(MockStore);
        mockinstanceSelectorInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, name: 'test' });
        mockAuthSelectorSelectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        mockAuthSelectorSelectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockInstanceGroupSelectorSelectAllInstanceGroups = store.overrideSelector(instanceGroupSelector.selectAllInstanceGroups, [
            { id: 1, instances: ['test'], role: 'test' }
        ]);
        let spyOnIsAdmin = jest.spyOn(fromSharedUtils, 'isAdmin');
        spyOnIsAdmin.mockImplementation(() => false);
        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        let expected = cold('a', { a: true });
        expect(instanceAuthGuard.canActivate()).toBeObservable(expected);
    });
    it('canActivate() should return false', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true } })
        store = TestBed.inject(MockStore);
        mockinstanceSelectorInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, name: 'test' });
        mockAuthSelectorSelectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        mockAuthSelectorSelectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, false);
        mockInstanceGroupSelectorSelectAllInstanceGroups = store.overrideSelector(instanceGroupSelector.selectAllInstanceGroups, [
            { id: 1, instances: ['test'], role: 'test' }
        ]);
        let spyOnIsAdmin = jest.spyOn(fromSharedUtils, 'isAdmin');
        spyOnIsAdmin.mockImplementation(() => false);
        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        let expected = cold('a', { a: false });
        expect(instanceAuthGuard.canActivate()).toBeObservable(expected);
    });
});

