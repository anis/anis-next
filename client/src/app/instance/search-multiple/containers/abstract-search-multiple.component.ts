import { Directive, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Instance, Dataset, DatasetFamily } from 'src/app/metamodel/models';
import { ConeSearch, SearchMultipleQueryParams } from '../../store/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as searchMultipleActions from '../../store/actions/search-multiple.actions';
import * as searchMultipleSelector from 'src/app/instance/store/selectors/search-multiple.selector';
import * as coneSearchSelector from 'src/app/instance/store/selectors/cone-search.selector';

/**
 * @abstract
 * @class
 * @classdesc Abstract search multiple container.
 *
 * @implements OnInit
 * @implements OnDestroy
 */
@Directive()
export abstract class AbstractSearchMultipleComponent implements OnInit {
    public pristine: Observable<boolean>;
    public instance: Observable<Instance>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;
    public currentStep: Observable<string>;
    public selectedDatasets: Observable<string[]>;
    public coneSearch: Observable<ConeSearch>;
    public queryParams: Observable<SearchMultipleQueryParams>;

    constructor(protected store: Store<{ }>) {
        this.pristine = this.store.select(searchMultipleSelector.selectPristine);
        this.instance = this.store.select(instanceSelector.selectInstanceByRouteName);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.datasetListIsLoading = this.store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = this.store.select(datasetSelector.selectDatasetListIsLoaded);
        this.datasetList = this.store.select(datasetSelector.selectAllConeSearchDatasets);
        this.currentStep = this.store.select(searchMultipleSelector.selectCurrentStep)
        this.selectedDatasets = this.store.select(searchMultipleSelector.selectSelectedDatasets);
        this.coneSearch = this.store.select(coneSearchSelector.selectConeSearch);
        this.queryParams = this.store.select(searchMultipleSelector.selectQueryParams);
    }

    ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.initSearch()));
    }
}
