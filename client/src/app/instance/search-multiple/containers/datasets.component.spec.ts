import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { DatasetsComponent } from './datasets.component';
import { Dataset, DatasetFamily } from 'src/app/metamodel/models';
import { ConeSearch, SearchMultipleQueryParams } from 'src/app/instance/store/models';
import { AbstractSearchMultipleComponent } from './abstract-search-multiple.component';
import * as searchMultipleActions from 'src/app/instance/store/actions/search-multiple.actions';
import { AppConfigService } from 'src/app/app-config.service';

describe('[Instance][SearchMultiple][Container] DatasetsComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-dataset-list', template: '' })
    class DatasetListStubComponent {
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() datasetList: Dataset[];
        @Input() selectedDatasets: string[];
    }

    @Component({ selector: 'app-summary-multiple', template: '' })
    class SummaryMultipleStubComponent {
        @Input() currentStep: string;
        @Input() coneSearch: ConeSearch;
        @Input() selectedDatasets: string[];
        @Input() queryParams: SearchMultipleQueryParams;
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() datasetList: Dataset[];
    }

    let component: DatasetsComponent;
    let fixture: ComponentFixture<DatasetsComponent>;
    let store: MockStore;
    let appConfigServiceStub = new AppConfigService();
    appConfigServiceStub.authenticationEnabled = true;
    appConfigServiceStub.adminRoles = ['test'];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                DatasetsComponent,
                SpinnerStubComponent,
                DatasetListStubComponent,
                SummaryMultipleStubComponent
            ],
            providers: [
                provideMockStore({}),
                { provide: AppConfigService, useValue: appConfigServiceStub }
            ]
        });
        fixture = TestBed.createComponent(DatasetsComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const spy = jest.spyOn(store, 'dispatch');
        jest.spyOn(AbstractSearchMultipleComponent.prototype, 'ngOnInit').mockReturnThis();
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(2);
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.changeStep({ step: 'datasets' }));
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.checkDatasets());
            done();
        });
    });

    it('#updateSelectedDatasets() should dispatch updateSelectedDatasets action', () => {
        const selectedDatasets: string[] = ['myDataset', 'myOtherDataset'];
        const spy = jest.spyOn(store, 'dispatch');
        component.updateSelectedDatasets(selectedDatasets);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchMultipleActions.updateSelectedDatasets({ selectedDatasets }));
    });
});
