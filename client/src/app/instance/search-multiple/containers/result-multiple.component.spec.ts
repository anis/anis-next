import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { ResultMultipleComponent } from './result-multiple.component';
import { Dataset, DatasetFamily } from '../../../metamodel/models';
import { ConeSearch, SearchMultipleDatasetLength, SearchMultipleQueryParams } from '../../store/models';
import { AbstractSearchMultipleComponent } from './abstract-search-multiple.component';
import * as searchMultipleActions from '../../store/actions/search-multiple.actions';

describe('[Instance][SearchMultiple][Container] ResultMultipleComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-overview', template: '' })
    class OverviewStubComponent {
        @Input() instanceSelected: string;
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() datasetList: Dataset[];
        @Input() coneSearch: ConeSearch;
        @Input() selectedDatasets: string[];
        @Input() dataLength: SearchMultipleDatasetLength[];
        @Input() queryParams: SearchMultipleQueryParams;
    }

    let component: ResultMultipleComponent;
    let fixture: ComponentFixture<ResultMultipleComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                ResultMultipleComponent,
                SpinnerStubComponent,
                OverviewStubComponent
            ],
            providers: [provideMockStore({ })]
        });
        fixture = TestBed.createComponent(ResultMultipleComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.pristine = of(false);
        const spy = jest.spyOn(store, 'dispatch');
        jest.spyOn(AbstractSearchMultipleComponent.prototype, 'ngOnInit').mockReturnThis();
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(4);
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.changeStep({ step: 'result' }));
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.checkDatasets());
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.checkResult());
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.retrieveDataLength());
            done();
        });
    });
});
