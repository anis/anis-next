import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { AbstractSearchMultipleComponent } from './abstract-search-multiple.component';
import * as searchMultipleActions from '../../store/actions/search-multiple.actions';

describe('[Instance][SearchMultiple][Container] AbstractSearchComponent', () => {
    @Component({
        selector: 'app-fake',
        template: ''
    })
    class MyFakeComponent extends AbstractSearchMultipleComponent {
        ngOnInit() {
            super.ngOnInit();
        }
    }

    let component: MyFakeComponent;
    let fixture: ComponentFixture<MyFakeComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [MyFakeComponent],
            providers: [provideMockStore({ })]
        });
        fixture = TestBed.createComponent(MyFakeComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.datasetListIsLoaded = of(true);
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.initSearch());
            done();
        });
    });
});
