/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { PositionComponent } from './position.component';
import { Dataset, DatasetFamily } from '../../../metamodel/models';
import { ConeSearch, SearchMultipleQueryParams } from '../../store/models';
import { AbstractSearchMultipleComponent } from './abstract-search-multiple.component';
import * as searchMultipleActions from '../../store/actions/search-multiple.actions';
import * as coneSearchActions from '../../store/actions/cone-search.actions';

describe('[Instance][SearchMultiple][Container] PositionComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-cone-search', template: '' })
    class ConeSearchStubComponent {
        @Input() coneSearch: ConeSearch;
        @Input() resolverIsLoading: boolean;
        @Input() resolverIsLoaded: boolean;
    }

    @Component({ selector: 'app-summary-multiple', template: '' })
    class SummaryMultipleStubComponent {
        @Input() currentStep: string;
        @Input() coneSearch: ConeSearch;
        @Input() selectedDatasets: string[];
        @Input() queryParams: SearchMultipleQueryParams;
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() datasetList: Dataset[];
    }

    let component: PositionComponent;
    let fixture: ComponentFixture<PositionComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                PositionComponent,
                SpinnerStubComponent,
                ConeSearchStubComponent,
                SummaryMultipleStubComponent
            ],
            providers: [provideMockStore({ })]
        });
        fixture = TestBed.createComponent(PositionComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const spy = jest.spyOn(store, 'dispatch');
        jest.spyOn(AbstractSearchMultipleComponent.prototype, 'ngOnInit').mockReturnThis();
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(searchMultipleActions.changeStep({ step: 'position' }));
            done();
        });
    });

    it('#addConeSearch() should dispatch addConeSearch action', () => {
        const coneSearch: ConeSearch = { ra: 1, dec: 2, radius: 3 };
        const spy = jest.spyOn(store, 'dispatch');
        component.addConeSearch(coneSearch);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchActions.addConeSearch({ coneSearch }));
    });
    it('#updateConeSearch() should dispatch updateConeSearch action', () => {
        const coneSearch: ConeSearch = { ra: 1, dec: 2, radius: 3 };
        const spy = jest.spyOn(store, 'dispatch');
        component.updateConeSearch(coneSearch);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchActions.updateConeSearch({ coneSearch }));
    });

    it('#deleteConeSearch() should dispatch deleteConeSearch action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.deleteConeSearch();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchActions.deleteConeSearch());
    });

    it('#retrieveCoordinates() should dispatch retrieveCoordinates action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.retrieveCoordinates('toto');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchActions.retrieveCoordinates({ name: 'toto' }));
    });
});
