/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { SearchMultipleDatasetLength } from '../../store/models';
import { AbstractSearchMultipleComponent } from './abstract-search-multiple.component';
import * as searchMultipleActions from '../../store/actions/search-multiple.actions';
import * as searchMultipleSelector from 'src/app/instance/store/selectors/search-multiple.selector';
import * as searchActions from '../../store/actions/search.actions';
import * as sampActions from 'src/app/samp/samp.actions';
import * as sampSelector from 'src/app/samp/samp.selector';

/**
 * @class
 * @classdesc Result multiple container.
 *
 * @extends AbstractSearchMultipleComponent
 * @implements OnInit
 * @implements OnDestroy
 */
@Component({
    selector: 'app-result-multiple',
    templateUrl: 'result-multiple.component.html'
})
export class ResultMultipleComponent extends AbstractSearchMultipleComponent implements OnInit, OnDestroy{
    public dataLength: Observable<SearchMultipleDatasetLength[]>;
    public dataLengthIsLoading: Observable<boolean>;
    public dataLengthIsLoaded: Observable<boolean>;
    public dataFound: Observable<boolean>;
    public sampRegistered: Observable<boolean>;
    public pristineSubscription: Subscription;

    constructor(protected store: Store<{ }>) {
        super(store);
        this.dataLength = this.store.select(searchMultipleSelector.selectDataLength);
        this.dataLengthIsLoading = this.store.select(searchMultipleSelector.selectDataLengthIsLoading);
        this.dataLengthIsLoaded = this.store.select(searchMultipleSelector.selectDataLengthIsLoaded);
        this.dataFound = this.store.select(searchMultipleSelector.selectDataFound);
        this.sampRegistered = this.store.select(sampSelector.selectRegistered);
    }

    ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.changeStep({ step: 'result' })));
        Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.checkDatasets()));
        Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.checkResult()));
        super.ngOnInit();
        this.pristineSubscription = this.pristine.subscribe(pristine => {
            if (!pristine) {
                Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.retrieveDataLength()));
            }
        });
    }

    ngOnDestroy(): void {
        if (this.pristineSubscription) this.pristineSubscription.unsubscribe();
    }

    /**
     * Dispatches action to launch the file download
     * 
     * @param { url: string, filename: string } download
     */
    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }

    /**
     * Dispatches action to register to SAMP.
     */
    sampRegister(): void {
        this.store.dispatch(sampActions.register());
    }

    /**
     * Dispatches action to disconnect to SAMP.
     */
    sampUnregister(): void {
        this.store.dispatch(sampActions.unregister());
    }

    /**
     * Dispatches action to broadcast data.
     *
     * @param  {string} url - The broadcast URL.
     */
    broadcastVotable(url: string): void {
        this.store.dispatch(sampActions.broadcastVotable({ url }));
    }
}
