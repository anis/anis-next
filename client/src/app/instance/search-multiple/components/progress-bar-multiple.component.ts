/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Instance } from 'src/app/metamodel/models';
import { ConeSearch, SearchMultipleQueryParams } from 'src/app/instance/store/models';

/**
 * @class
 * @classdesc Progress bar multiple component.
 */
@Component({
    selector: 'app-progress-bar-multiple',
    templateUrl: 'progress-bar-multiple.component.html',
    styleUrls: [ '../../search/components/progress-bar.component.scss', 'progress-bar-multiple.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarMultipleComponent {
    @Input() instance: Instance;
    @Input() currentStep: string;
    @Input() datasetsStepChecked: boolean;
    @Input() resultStepChecked: boolean;
    @Input() coneSearch: ConeSearch;
    @Input() selectedDatasets: string[];
    @Input() queryParams: SearchMultipleQueryParams;

    /**
     * Returns step class that match to the current step.
     *
     * @return string
     */
    getStepClass(): string {
        switch (this.currentStep) {
            case 'position':
                return 'positionStep';
            case 'datasets':
                return 'datasetsStep';
            case 'result':
                return 'resultStep';
            default:
                return 'positionStep';
        }
    }
}
