import { ProgressBarMultipleComponent } from './progress-bar-multiple.component';
import { positionComponents } from './position';
import { datasetsComponents } from './datasets';
import { resultComponents } from './result';

export const dummiesComponents = [
    ProgressBarMultipleComponent,
    positionComponents,
    datasetsComponents,
    resultComponents
];
