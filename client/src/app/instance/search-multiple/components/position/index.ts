import { ParametersPanelComponent } from './parameters-panel.component';
import { ConeSearchPanelComponent } from './cone-search-panel.component';

export const positionComponents = [
    ParametersPanelComponent,
    ConeSearchPanelComponent
];
