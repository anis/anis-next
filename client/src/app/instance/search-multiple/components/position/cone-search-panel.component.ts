/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { ConeSearch, coneSearchToString } from 'src/app/instance/store/models';

@Component({
    selector: 'app-cone-search-panel',
    templateUrl: 'cone-search-panel.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchPanelComponent {
    @Input() coneSearch: ConeSearch;
    @Input() resolverIsLoading: boolean;
    @Input() resolverIsLoaded: boolean;
    @Output() addConeSearch: EventEmitter<ConeSearch> = new EventEmitter();
    @Output() updateConeSearch: EventEmitter<ConeSearch> = new EventEmitter();
    @Output() deleteConeSearch: EventEmitter<{ }> = new EventEmitter();
    @Output() retrieveCoordinates: EventEmitter<string> = new EventEmitter();

    /**
     * Emits event to add or update cone-search.
     *
     * @fires EventEmitter<ConeSearch>
     */
    emitAdd(newConeSearch: ConeSearch): void {
        if (!this.coneSearch) {
            this.addConeSearch.emit(newConeSearch);
        }
        if (this.coneSearch && coneSearchToString(this.coneSearch) !== coneSearchToString(newConeSearch)) {
            this.updateConeSearch.emit(newConeSearch);
        }
    }
}
