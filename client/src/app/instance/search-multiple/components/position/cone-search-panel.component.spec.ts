
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ConeSearchPanelComponent } from "./cone-search-panel.component";

describe('Instance][SearchMultiple][Component][Position] ConeSearchPanelComponent', () => {
    let component: ConeSearchPanelComponent;
    let fixture: ComponentFixture<ConeSearchPanelComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchPanelComponent
            ]
        });
        fixture = TestBed.createComponent(ConeSearchPanelComponent);
        component = fixture.componentInstance;
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('should emit addConeSearch event with the new cone search when conesearch property is undefined', () => {
        let spy = jest.spyOn(component.addConeSearch, 'emit');
        component.emitAdd({ ra: 10, dec: 5, radius: 10 });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ra: 10, dec: 5, radius: 10 });
    });
    it('should emit updateConeSearch event with the new cone search when conesearch property is defined', () => {
        let spy = jest.spyOn(component.updateConeSearch, 'emit');
        component.coneSearch = { ra: 1, dec: 5, radius: 1 };
        component.emitAdd({ ra: 10, dec: 5, radius: 10 });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ra: 10, dec: 5, radius: 10 });
    });

});