/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { ConeSearch } from 'src/app/instance/store/models';
import { Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-parameters-panel',
    templateUrl: 'parameters-panel.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParametersPanelComponent {
    @Input() coneSearch: ConeSearch;
    @Input() dataset: Dataset;
}
