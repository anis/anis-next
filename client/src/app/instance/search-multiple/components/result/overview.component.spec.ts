import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { OverviewComponent } from './overview.component';

describe('[Instance][SearchMultiple][Component][Result] OverviewComponent', () => {
    let component: OverviewComponent;
    let fixture: ComponentFixture<OverviewComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [OverviewComponent],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(OverviewComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
