/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';

import { Instance, DatasetFamily, Dataset } from 'src/app/metamodel/models';
import { ConeSearch, SearchMultipleDatasetLength } from 'src/app/instance/store/models';

@Component({
    selector: 'app-multi-result-by-dataset',
    templateUrl: 'multi-result-by-dataset.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiResultByDatasetComponent {
    @Input() instance: Instance;
    @Input() datasetFamilyList: DatasetFamily[];
    @Input() datasetList: Dataset[];
    @Input() coneSearch: ConeSearch;
    @Input() selectedDatasets: string[];
    @Input() dataLength: SearchMultipleDatasetLength[];
    @Input() sampRegistered: boolean;
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();
    @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
    @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();
    @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();

    /**
     * Returns dataset families that contains selected datasets.
     *
     * @return DatasetFamily[]
     */
    getSortedDatasetFamilyList(): DatasetFamily[] {
        let datasetFamiliesWithSelectedDataset: DatasetFamily[] = [];
        this.selectedDatasets
            .filter(datasetName => this.getCountByDataset(datasetName) > 0)
            .forEach(dname => {
                const dataset: Dataset = this.datasetList.find(d => d.name === dname);
                const datasetFamily: DatasetFamily = this.datasetFamilyList.find(f => f.id === dataset.id_dataset_family);
                if (!datasetFamiliesWithSelectedDataset.includes(datasetFamily)) {
                    datasetFamiliesWithSelectedDataset.push(datasetFamily);
                }
            }
        );
        return datasetFamiliesWithSelectedDataset;
    }

    /**
     * Returns selected dataset list for the given dataset family ID.
     *
     * @param  {number} familyId - The family ID.
     *
     * @return Dataset[]
     */
    getSelectedDatasetsByFamily(familyId: number): Dataset[] {
        return this.datasetList
            .filter(d => d.id_dataset_family === familyId)
            .filter(d => this.selectedDatasets.includes(d.name))
            .filter(d => this.getCountByDataset(d.name) > 0);
    }

    /**
     * Returns the result count for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return number
     */
    getCountByDataset(dname: string): number {
        return this.dataLength.find(datasetLength => datasetLength.datasetName === dname).length;
    }
}
