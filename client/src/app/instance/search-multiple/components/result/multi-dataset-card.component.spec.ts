import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MultiDatasetCardComponent } from './multi-dataset-card.component';

describe('[Instance][SearchMultiple][Component][Result] MultiResultByDatasetComponent', () => {
    let component: MultiDatasetCardComponent;
    let fixture: ComponentFixture<MultiDatasetCardComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MultiDatasetCardComponent],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(MultiDatasetCardComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
