import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MultiResultByDatasetComponent } from './multi-result-by-dataset.component';

describe('[Instance][SearchMultiple][Component][Result] MultiResultByDatasetComponent', () => {
    let component: MultiResultByDatasetComponent;
    let fixture: ComponentFixture<MultiResultByDatasetComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MultiResultByDatasetComponent],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(MultiResultByDatasetComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
