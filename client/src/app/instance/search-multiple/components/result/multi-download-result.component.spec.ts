import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MultiDownloadResultComponent } from './multi-download-result.component';
import { AppConfigService } from "src/app/app-config.service";

describe('[Instance][SearchMultiple][Component][Result] MultiDownloadResultComponent', () => {
    let component: MultiDownloadResultComponent;
    let fixture: ComponentFixture<MultiDownloadResultComponent>;

    beforeEach(() => {
        let appService: AppConfigService = {
            apiUrl: "http://test.fr",
            servicesUrl: "",
            baseHref: "",
            authenticationEnabled: false,
            ssoAuthUrl: "",
            ssoRealm: "",
            ssoClientId: "",
            adminRoles: [],
            matomoEnabled: false,
            matomoSiteId: 0,
            matomoTrackerUrl: ""
        };
        TestBed.configureTestingModule({
            declarations: [MultiDownloadResultComponent],
            providers: [ { provide: AppConfigService, useValue: appService } ],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(MultiDownloadResultComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
