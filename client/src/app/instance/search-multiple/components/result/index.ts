import { OverviewComponent } from './overview.component';
import { MultiResultByDatasetComponent } from './multi-result-by-dataset.component';
import { MultiDatasetCardComponent } from './multi-dataset-card.component';
import { MultiDownloadResultComponent }from './multi-download-result.component';

export const resultComponents = [
    OverviewComponent,
    MultiResultByDatasetComponent,
    MultiDatasetCardComponent,
    MultiDownloadResultComponent
];
