/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Dataset } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';
import { SearchMultipleDatasetLength } from 'src/app/instance/store/models';

/**
 * @class
 * @classdesc Overview component.
 */
@Component({
    selector: 'app-overview',
    templateUrl: 'overview.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OverviewComponent {
    @Input() selectedDatasets: string[];
    @Input() datasetList: Dataset[];
    @Input() coneSearch: ConeSearch;
    @Input() dataLength: SearchMultipleDatasetLength[];

    getLengthByDatasetName(datasetName: string) {
        return this.dataLength.find(dataLength => dataLength.datasetName === datasetName).length;
    }
}
