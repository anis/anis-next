/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';

import { Instance, Dataset } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';

@Component({
    selector: 'app-multi-dataset-card',
    templateUrl: 'multi-dataset-card.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiDatasetCardComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() coneSearch: ConeSearch;
    @Input() dataLength: number;
    @Input() sampRegistered: boolean;
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();
    @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
    @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();
    @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();

    /**
     * Returns the cone search query parameters.
     *
     * @return { cs: string }
     */
    getCsQueryParams(): { cs: string } {
        return { cs: `${this.coneSearch.ra}:${this.coneSearch.dec}:${this.coneSearch.radius}` }
    }
}
