import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ProgressBarMultipleComponent } from './progress-bar-multiple.component';

describe('[Instance][SearchMultiple][Component] ProgressBarMultipleComponent', () => {
    let component: ProgressBarMultipleComponent;
    let fixture: ComponentFixture<ProgressBarMultipleComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ProgressBarMultipleComponent],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(ProgressBarMultipleComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getStepClass() should return correct step class', () => {
        let style = component.getStepClass();
        expect(style).toBe('positionStep');
        component.currentStep = 'position';
        style = component.getStepClass();
        expect(style).toBe('positionStep');
        component.currentStep = 'datasets';
        style = component.getStepClass();
        expect(style).toBe('datasetsStep');
        component.currentStep = 'result';
        style = component.getStepClass();
        expect(style).toBe('resultStep');
    });
});
