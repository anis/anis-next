import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { MultiDatasetSelectComponent } from './multi-dataset-select.component';

describe('[Instance][SearchMultiple][Component][Datasets] MultiDatasetSelectComponent', () => {
    let component: MultiDatasetSelectComponent;
    let fixture: ComponentFixture<MultiDatasetSelectComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MultiDatasetSelectComponent],
            imports: [TooltipModule.forRoot()]
        });
        fixture = TestBed.createComponent(MultiDatasetSelectComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
