import { DatasetListComponent } from './dataset-list.component';
import { DatasetsByFamilyComponent } from './datasets-by-family.component';
import { MultiDatasetSelectComponent } from './multi-dataset-select.component';

export const datasetsComponents = [
    DatasetListComponent,
    DatasetsByFamilyComponent,
    MultiDatasetSelectComponent
];
