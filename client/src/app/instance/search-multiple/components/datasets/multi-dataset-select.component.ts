/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';

import { Dataset, DatasetGroup } from 'src/app/metamodel/models';
import { isAdmin, isDatasetAccessible } from 'src/app/shared/utils';

@Component({
    selector: 'app-multi-dataset-select',
    templateUrl: 'multi-dataset-select.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiDatasetSelectComponent {
    @Input() dataset: Dataset;
    @Input() isSelected: boolean;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];
    @Input() datasetGroupList: DatasetGroup[];
    @Output() toggleSelection: EventEmitter<string> = new EventEmitter();

    isDatasetAccessible() {
        return isDatasetAccessible(
            this.dataset, 
            this.authenticationEnabled, 
            this.isAuthenticated,
            this.datasetGroupList,
            this.adminRoles,
            this.userRoles
        );
    }

    /**
     * Returns true if user is admin
     *
     * @returns boolean
     */
    isAdmin() {
        return isAdmin(this.adminRoles, this.userRoles);
    }
}