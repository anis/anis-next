/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Dataset, DatasetFamily, DatasetGroup } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Dataset list component.
 */
@Component({
    selector: 'app-dataset-list',
    templateUrl: 'dataset-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetListComponent {
    @Input() datasetFamilyList: DatasetFamily[];
    @Input() datasetList: Dataset[];
    @Input() selectedDatasets: string[];
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];
    @Input() datasetGroupList: DatasetGroup[];
    @Output() updateSelectedDatasets: EventEmitter<string[]> = new EventEmitter();

    /**
     * Returns dataset family list that contains datasets with cone search enabled.
     *
     * @return DatasetFamily[]
     */
    getDatasetFamilyList(): DatasetFamily[] {
        const familyId: number[] = [];
        this.datasetList.forEach(d => {
            if (!familyId.includes(d.id_dataset_family)) {
                familyId.push(d.id_dataset_family);
            }
        });
        return this.datasetFamilyList
            .filter(f => familyId.includes(f.id));
    }

    /**
     * Returns dataset list that belongs to the given ID family.
     *
     * @param  {number} familyId - The dataset family ID.
     *
     * @return Dataset[]
     */
    getDatasetsByFamily(familyId: number): Dataset[] {
        return this.datasetList.filter(d => d.id_dataset_family === familyId);
    }

    /**
     * Checks if all datasets that belongs to the given dataset family ID are selected.
     *
     * @param  {number} familyId - The dataset family ID.
     *
     * @return boolean
     */
    getIsAllSelected(familyId: number): boolean {
        const datasetListName = this.getDatasetsByFamily(familyId).map(d => d.name);
        const filteredSelectedDatasets = this.selectedDatasets.filter(name => datasetListName.indexOf(name) > -1);
        return datasetListName.length === filteredSelectedDatasets.length;
    }

    /**
     * Checks if none of datasets that belongs to the given dataset family ID are selected.
     *
     * @param  {number} familyId - The dataset family ID.
     *
     * @return boolean
     */
    getIsAllUnselected(familyId: number): boolean {
        const datasetListName = this.getDatasetsByFamily(familyId).map(d => d.name);
        const filteredSelectedDatasets = this.selectedDatasets.filter(name => datasetListName.indexOf(name) > -1);
        return filteredSelectedDatasets.length === 0;
    }
}
