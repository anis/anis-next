/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

import { Dataset, DatasetFamily, DatasetGroup } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Datasets by family component.
 */
@Component({
    selector: 'app-datasets-by-family',
    templateUrl: 'datasets-by-family.component.html',
    styleUrls: ['datasets-by-family.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class DatasetsByFamilyComponent {
    @Input() datasetFamily: DatasetFamily;
    @Input() datasetList: Dataset[];
    @Input() selectedDatasets: string[];
    @Input() isAllSelected: boolean;
    @Input() isAllUnselected: boolean;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];
    @Input() datasetGroupList: DatasetGroup[];
    @Output() updateSelectedDatasets: EventEmitter<string[]> = new EventEmitter();

    /**
     * Checks if dataset is selected fot the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return boolean
     */
    isSelected(dname: string): boolean {
        return this.selectedDatasets.filter(i => i === dname).length > 0;
    }

    /**
     * Emits event to update dataset list selection with the given updated selected dataset name list.
     *
     * @param  {string} dname - The dataset name.
     */
    toggleSelection(dname: string): void {
        const clonedSelectedDatasets = [...this.selectedDatasets];
        const index = clonedSelectedDatasets.indexOf(dname);
        if (index > -1) {
            clonedSelectedDatasets.splice(index, 1);
        } else {
            clonedSelectedDatasets.push(dname);
        }
        this.updateSelectedDatasets.emit(clonedSelectedDatasets);
    }

    /**
     * Emits event to update dataset list selection with all datasets names.
     */
    selectAll(): void {
        const clonedSelectedDatasets = [...this.selectedDatasets];
        const datasetListName = this.datasetList.map(d => d.name);
        datasetListName.filter(name => clonedSelectedDatasets.indexOf(name) === -1).forEach(name => {
            clonedSelectedDatasets.push(name);
        });
        this.updateSelectedDatasets.emit(clonedSelectedDatasets);
    }

    /**
     * Emits event to update dataset list selection with no datasets names.
     */
    unselectAll(): void {
        const clonedSelectedDatasets = [...this.selectedDatasets];
        const datasetListName = this.datasetList.map(d => d.name);
        datasetListName.filter(name => clonedSelectedDatasets.indexOf(name) > -1).forEach(name => {
            const index = clonedSelectedDatasets.indexOf(name);
            clonedSelectedDatasets.splice(index, 1);
        });
        this.updateSelectedDatasets.emit(clonedSelectedDatasets);
    }
}
