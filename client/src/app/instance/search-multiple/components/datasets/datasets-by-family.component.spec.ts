import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { DatasetsByFamilyComponent } from './datasets-by-family.component';
import { DATASET_LIST } from 'src/test-data';

describe('[Instance][SearchMultiple][Component][Datasets] DatasetsByFamilyComponent', () => {
    let component: DatasetsByFamilyComponent;
    let fixture: ComponentFixture<DatasetsByFamilyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DatasetsByFamilyComponent],
            imports: [TooltipModule.forRoot()]
        });
        fixture = TestBed.createComponent(DatasetsByFamilyComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#isSelected() should return true if the given dataset is selected', () => {
        component.selectedDatasets = ['toto'];
        expect(component.isSelected('toto')).toBeTruthy();
    });

    it('#isSelected() should return false if the given dataset is not selected', () => {
        component.selectedDatasets = [];
        expect(component.isSelected('toto')).toBeFalsy();
    });

    it('#toggleSelection() should remove the given dataset from selectedDatasets and raise updateSelectedDatasets event', () => {
        component.selectedDatasets = ['toto'];
        const expectedSelectedDatasets = [];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.toggleSelection('toto');
    });

    it('#toggleSelection() should add the given dataset to selectedDatasets and raise updateSelectedDatasets event', () => {
        component.selectedDatasets = [];
        const expectedSelectedDatasets = ['toto'];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.toggleSelection('toto');
    });

    it('#selectAll() should add all datasets to selectedDatasets and raise updateSelectedDatasets event', () => {
        component.datasetList = DATASET_LIST.filter(d => d.id_dataset_family === 1);
        component.selectedDatasets = [];
        const expectedSelectedDatasets = ['myDataset', 'anotherDataset'];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.selectAll();
    });

    it('#unselectAll() should remove all datasets to selectedDatasets and raise updateSelectedDatasets event', () => {
        component.datasetList = DATASET_LIST.filter(d => d.id_dataset_family === 1);
        component.selectedDatasets = ['myDataset', 'anotherDataset'];
        const expectedSelectedDatasets = [];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.unselectAll();
    });
});
