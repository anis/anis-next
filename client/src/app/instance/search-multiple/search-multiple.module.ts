/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { ConeSearchModule } from '../cone-search/cone-search.module';
import { SearchMultipleRoutingModule, routedComponents } from './search-multiple-routing.module';
import { dummiesComponents } from './components';

/**
 * @class
 * @classdesc Search multiple module.
 */
@NgModule({
    imports: [
        SharedModule,
        ConeSearchModule,
        SearchMultipleRoutingModule
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ]
})
export class SearchMultipleModule { }
