import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Injectable({
    providedIn: 'root'
})
export class SearchMultipleTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        const step = route.component.name.replace('Component', '').replace('Multiple', '');
        return this.store.select(instanceSelector.selectInstanceByRouteName).pipe(
            map(instance => `${instance.label} - Search Multiple - ${step}`)
        );
    }
}
