/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { combineReducers, createFeatureSelector } from '@ngrx/store';

import { RouterReducerState } from 'src/app/custom-route-serializer';
import * as search from './store/reducers/search.reducer';
import * as searchMultiple from './store/reducers/search-multiple.reducer';
import * as coneSearch from './store/reducers/cone-search.reducer';
import * as detail from './store/reducers/detail.reducer';
import * as archive from './store/reducers/archive.reducer';

/**
 * Interface for instance state.
 *
 * @interface State
 */
export interface State {
    search: search.State,
    searchMultiple: searchMultiple.State,
    coneSearch: coneSearch.State
    detail: detail.State,
    archive: archive.State
}

const reducers = {
    search: search.searchReducer,
    searchMultiple: searchMultiple.searchMultipleReducer,
    coneSearch: coneSearch.coneSearchReducer,
    detail: detail.detailReducer,
    archive: archive.archiveReducer
};

export const instanceReducer = combineReducers(reducers);
export const getInstanceState = createFeatureSelector<State>('instance');
export const selectRouterState = createFeatureSelector<RouterReducerState>('router');
