
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Instance } from "../metamodel/models";
import { StyleService } from "../shared/services/style.service";
import { InstanceStyleService } from "./instance-style.service";

describe('[Instance] InstanceStyleService', () => {
    class mockStyleService extends StyleService{
        setStyle = jest.fn().mockImplementation(() =>{})
        setStyles =jest.fn()
    }
    let service: InstanceStyleService;
    let instance: Instance = {
        name: "",
        label: 'test',
        description: 'test',
        scientific_manager: "",
        instrument: "",
        wavelength_domain: "",
        display: 0,
        data_path: "",
        files_path: "",
        public: false,
        portal_logo: "",
        portal_color: "",
        design_background_color: "",
        design_text_color: "",
        design_font_family: "",
        design_link_color: "",
        design_link_hover_color: "",
        design_logo: "",
        design_logo_href: "",
        design_favicon: "",
        navbar_background_color: "",
        navbar_border_bottom_color: "",
        navbar_color_href: "",
        navbar_font_family: "",
        navbar_sign_in_btn_color: "",
        navbar_user_btn_color: "",
        footer_background_color: "",
        footer_border_top_color: "",
        footer_text_color: "",
        footer_logos: [],
        family_border_color: "",
        family_header_background_color: "",
        family_title_color: "",
        family_title_bold: false,
        family_background_color: "",
        family_text_color: "",
        progress_bar_title: "",
        progress_bar_title_color: "",
        progress_bar_subtitle: "",
        progress_bar_subtitle_color: "",
        progress_bar_step_dataset_title: "",
        progress_bar_step_criteria_title: "",
        progress_bar_step_output_title: "",
        progress_bar_step_result_title: "",
        progress_bar_color: "",
        progress_bar_active_color: 'test',
        progress_bar_circle_color: 'test',
        progress_bar_circle_icon_color: 'test',
        progress_bar_circle_icon_active_color: 'test',
        progress_bar_text_color: 'test',
        progress_bar_text_bold: false,
        search_next_btn_color: 'test',
        search_next_btn_hover_color: 'test',
        search_next_btn_hover_text_color: 'test',
        search_back_btn_color: "",
        search_back_btn_hover_color: "",
        search_back_btn_hover_text_color: "",
        search_info_background_color: "",
        search_info_text_color: "",
        search_info_help_enabled: false,
        dataset_select_btn_color: "",
        dataset_select_btn_hover_color: "",
        dataset_select_btn_hover_text_color: "",
        dataset_selected_icon_color: "",
        search_criterion_background_color: "",
        search_criterion_text_color: "",
        output_columns_selected_color: "",
        output_columns_select_all_btn_color: "",
        output_columns_select_all_btn_hover_color: "",
        output_columns_select_all_btn_hover_text_color: "",
        result_panel_border_size: '1px',
        result_panel_border_color: '#DEE2E6',
        result_panel_title_color: '#000000',
        result_panel_background_color: '#FFFFFF',
        result_panel_text_color: '#000000',
        result_download_btn_color: "",
        result_download_btn_hover_color: "",
        result_download_btn_text_color: "",
        result_datatable_actions_btn_color: "",
        result_datatable_actions_btn_hover_color: "",
        result_datatable_actions_btn_text_color: "",
        result_datatable_bordered: false,
        result_datatable_bordered_radius: false,
        result_datatable_border_color: "",
        result_datatable_header_background_color: "",
        result_datatable_header_text_color: "",
        result_datatable_rows_background_color: "",
        result_datatable_rows_text_color: "",
        result_datatable_sorted_color: "",
        result_datatable_sorted_active_color: "",
        result_datatable_link_color: "",
        result_datatable_link_hover_color: "",
        result_datatable_rows_selected_color: "",
        result_datatable_pagination_link_color:"#7AC29A",
        result_datatable_pagination_active_bck_color:"#7AC29A",
        result_datatable_pagination_active_text_color: "#FFFFFF",
        samp_enabled: false,
        back_to_portal: false,
        user_menu_enabled: false,
        search_by_criteria_allowed: false,
        search_by_criteria_label: "",
        search_multiple_allowed: false,
        search_multiple_label: "",
        search_multiple_all_datasets_selected: false,
        search_multiple_progress_bar_title : 'Search around a position in multiple datasets',
        search_multiple_progress_bar_subtitle : 'Fill RA & DEC position, select datasets and display the result.',
        search_multiple_progress_bar_step_position: 'Position',
        search_multiple_progress_bar_step_datasets : 'Datasets',
        search_multiple_progress_bar_step_result : 'Result',
        documentation_allowed: false,
        documentation_label: "",
        nb_dataset_families: 0,
        nb_datasets: 0
    }
    beforeEach(() =>{
        service = new InstanceStyleService(new mockStyleService());
    })
  
    it('call all services  privates methods', () =>{
      
        let spyOnGeneralStyle = jest.spyOn((service as any), 'generalStyle');
        let spyOnNavbarStyle =jest.spyOn((service as any), 'navbarStyle');
        let spyOnFooterStyle =jest.spyOn((service as any), 'footerStyle');
        let spyOnSearchProgressBarStyle = jest.spyOn((service as any), 'searchProgressBarStyle')
        let spyOnsearchNextBackButtonsStyle = jest.spyOn((service as any), 'searchNextBackButtonsStyle');
        let spyOnSearchFamilyStyle = jest.spyOn((service as any), 'searchFamilyStyle');
        let spyOnSearchInfoStyle = jest.spyOn((service as any), 'searchInfoStyle');
        let spyOnSearchDatasetSelectionStyle = jest.spyOn((service as any), 'searchDatasetSelectionStyle');
        let spyOnSearchCriteriaStyle =jest.spyOn((service as any), 'searchCriteriaStyle');
        let spyOnSearchOutputColumnsStyle = jest.spyOn((service as any), 'searchOutputColumnsStyle');
        let spyOnSearchResultTable= jest.spyOn((service as any), 'searchResultTable');
        service.applyInstanceStyle(instance)
        expect(spyOnGeneralStyle).toHaveBeenCalledTimes(1);
        expect(spyOnNavbarStyle).toHaveBeenCalledTimes(1);
        expect(spyOnFooterStyle).toHaveBeenCalledTimes(1);

    })
    it('call set style for bordered data_table', () =>{
      
        let spyOnGeneralStyle = jest.spyOn((service as any), 'generalStyle');
        let spyOnNavbarStyle =jest.spyOn((service as any), 'navbarStyle');
        let spyOnFooterStyle =jest.spyOn((service as any), 'footerStyle');
        instance.result_datatable_bordered = true;
        instance.result_datatable_bordered_radius = true;
        service.applyInstanceStyle(instance)
        expect(spyOnGeneralStyle).toHaveBeenCalledTimes(1);
        expect(spyOnNavbarStyle).toHaveBeenCalledTimes(1);
        expect(spyOnFooterStyle).toHaveBeenCalledTimes(1);

    })
});