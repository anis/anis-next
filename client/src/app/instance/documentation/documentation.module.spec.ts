
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DocumentationModule } from "./documentation.module";


describe('Instance][documentation] DocumentationModule', () => {
    it('test Documentation Module', () => {
        expect(DocumentationModule.name).toEqual('DocumentationModule')
    });
});