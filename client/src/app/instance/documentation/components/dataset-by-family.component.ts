/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Dataset, DatasetFamily, DatasetGroup } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Documentation dataset by family component.
 */
@Component({
    selector: 'app-dataset-by-family',
    templateUrl: 'dataset-by-family.component.html',
    styleUrls: ['dataset-by-family.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetByFamilyComponent {
    @Input() datasetList: Dataset[];
    @Input() datasetFamilyList: DatasetFamily[];
    @Input() instanceSelected: string;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];
    @Input() datasetGroupList: DatasetGroup[];
}
