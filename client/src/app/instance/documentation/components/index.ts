/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetByFamilyComponent } from './dataset-by-family.component';
import { DatasetCardDocComponent } from './dataset-card-doc.component';
import { OutputListComponent } from './output-list.component';

export const dummiesComponents = [
    DatasetByFamilyComponent,
    DatasetCardDocComponent,
    OutputListComponent
];
