/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DatasetByFamilyComponent } from './dataset-by-family.component';
import { sharedPipes } from 'src/app/shared/pipes'
import { Dataset, DatasetFamily } from 'src/app/metamodel/models';

const DATASET_LIST: Dataset[] = [
    {
        name: 'myDataset1',
        table_ref: 'table1',
        label: 'my dataset one',
        description: 'This is my dataset one',
        display: 1,
        data_path: 'path1',
        public: true,
        download_json: true,
        download_csv: true,
        download_ascii: true,
        download_vo: true,
        download_fits: true,
        server_link_enabled: true,
        datatable_enabled: true,
        datatable_selectable_rows: true,
        cone_search_config_id: 1,
        id_database: 1,
        id_dataset_family: 1,
        full_data_path: '/data/path1'
    },
    {
        name: 'myDataset2',
        table_ref: 'table2',
        label: 'my dataset two',
        description: 'This is my dataset two',
        display: 2,
        data_path: 'path2',
        public: true,
        download_json: true,
        download_csv: true,
        download_ascii: true,
        download_vo: true,
        download_fits: true,
        server_link_enabled: true,
        datatable_enabled: true,
        datatable_selectable_rows: true,
        cone_search_config_id: 1,
        id_database: 1,
        id_dataset_family: 1,
        full_data_path: '/data/path2'
    }
];
const DATASET_FAMILY_LIST: DatasetFamily[] = [
    {
        id: 1,
        label: 'datasetFamily1',
        display: 1,
        opened: true
    },
    {
        id: 2,
        label: 'datasetFamily2',
        display: 2,
        opened: true
    }
];

describe('[Instance][Documentation][Component] DatasetByFamilyComponent', () => {
    @Component({ selector: '<app-dataset-card-doc', template: '' })
    class DatasetCardDocStubComponent {
        @Input() dataset: Dataset;
        @Input() instanceSelected: string;
    }

    let component: DatasetByFamilyComponent;
    let fixture: ComponentFixture<DatasetByFamilyComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [AccordionModule.forRoot()],
            declarations: [
                DatasetByFamilyComponent,
                DatasetCardDocStubComponent,
                sharedPipes
            ]

        }).compileComponents();
        fixture = TestBed.createComponent(DatasetByFamilyComponent);
        component = fixture.componentInstance;

        component.datasetList = DATASET_LIST;
        component.datasetFamilyList = DATASET_FAMILY_LIST;
        component.instanceSelected = 'myInstance';
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });
});
