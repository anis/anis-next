/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Documentation output list component.
 */
@Component({
    selector: 'app-output-list',
    templateUrl: 'output-list.component.html',
    styleUrls: ['../documentation.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputListComponent {
    @Input() datasetSelected: string;
    @Input() attributeList: Attribute[];
}
