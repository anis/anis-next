/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';

import { Dataset, DatasetGroup } from 'src/app/metamodel/models';
import { isAdmin, isDatasetAccessible } from 'src/app/shared/utils';

/**
 * @class
 * @classdesc Documentation dataset card component.
 */
@Component({
    selector: 'app-dataset-card-doc',
    templateUrl: 'dataset-card-doc.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetCardDocComponent {
    @Input() dataset: Dataset;
    @Input() instanceSelected: string;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];
    @Input() datasetGroupList: DatasetGroup[];

    constructor(private router: Router) { }

    isDatasetAccessible() {
        return isDatasetAccessible(
            this.dataset, 
            this.authenticationEnabled, 
            this.isAuthenticated,
            this.datasetGroupList,
            this.adminRoles,
            this.userRoles
        );
    }

    /**
     * Returns true if user is admin
     *
     * @returns boolean
     */
    isAdmin() {
        return isAdmin(this.adminRoles, this.userRoles);
    }

    /**
     * Navigates to the documentation page for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     */
    selectDataset(datasetName: string): void {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.navigate([`/instance/${this.instanceSelected}/documentation/${datasetName}`]);
    }
}
