/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import { Dataset, DatasetFamily, DatasetGroup } from 'src/app/metamodel/models';
import * as authSelector from '../../../auth/auth.selector';
import * as instanceSelector from '../../../metamodel/selectors/instance.selector';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Documentation dataset list container.
 *
 * @implements OnInit
 */
@Component({
    selector: 'app-dataset-list',
    templateUrl: 'dataset-list.component.html',
    styleUrls: ['../documentation.component.scss']
})
export class DatasetListComponent implements OnInit {
    public instanceSelected: Observable<string>;
    public isAuthenticated: Observable<boolean>;
    public userRoles: Observable<string[]>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;
    public datasetGroupList: Observable<DatasetGroup[]>;
    public datasetGroupListIsLoading: Observable<boolean>;
    public datasetGroupListIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>, private config: AppConfigService) {
        this.instanceSelected = store.select(instanceSelector.selectInstanceNameByRoute);
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.userRoles = store.select(authSelector.selectUserRoles);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
        this.datasetList = store.select(datasetSelector.selectAllDatasets);
        this.datasetGroupList = store.select(datasetGroupSelector.selectAllDatasetGroups);
        this.datasetGroupListIsLoading = store.select(datasetGroupSelector.selectDatasetGroupListIsLoading);
        this.datasetGroupListIsLoaded = store.select(datasetGroupSelector.selectDatasetGroupListIsLoaded);
    }

    ngOnInit(): void {
        this.store.dispatch(datasetActions.loadDatasetList());
    }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }

    /**
     * Returns admin roles list
     * 
     * @returns string[]
     */
    getAdminRoles(): string[] {
        return this.config.adminRoles;
    }
}
