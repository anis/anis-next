/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { DocumentationComponent } from './documentation.component';
import { AppConfigService } from 'src/app/app-config.service';
import { Attribute } from '../../../metamodel/models';
import * as attributeActions from '../../../metamodel/actions/attribute.actions';

describe('[Instance][Documentation][Container] DocumentationComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: '<app-output-list', template: '' })
    class OutputListStubComponent {
        @Input() datasetSelected: string;
        @Input() attributeList: Attribute[];
    }

    let component: DocumentationComponent;
    let fixture: ComponentFixture<DocumentationComponent>;
    let store: MockStore;
    let appConfigServiceStub = new AppConfigService();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                DocumentationComponent,
                SpinnerStubComponent,
                OutputListStubComponent,
            ],
            providers: [
                provideMockStore({ }),
                { provide: AppConfigService, useValue: appConfigServiceStub },
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(DocumentationComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('should execute ngOnInit lifecycle', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(attributeActions.loadAttributeList());
    });

    it('#getUrlServer() should return server URL address', () => {
        appConfigServiceStub.apiUrl = 'http://test.com';
        expect(component.getUrlServer()).toBe('http://test.com');
        appConfigServiceStub.apiUrl = '/testing';
        expect(component.getUrlServer()).toBe('http://localhost/testing');
    });
});
