/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { DatasetListComponent } from './dataset-list.component';
import { AppConfigService } from 'src/app/app-config.service';
import { Dataset, DatasetFamily } from 'src/app/metamodel/models';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';

describe('[Instance][Documentation][Container] DatasetListComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: '<app-dataset-by-family', template: '' })
    class DatasetByFamilyStubComponent {
        @Input() datasetList: Dataset[];
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() instanceSelected: string;
    }

    let component: DatasetListComponent;
    let fixture: ComponentFixture<DatasetListComponent>;
    let store: MockStore;
    let appConfigServiceStub = new AppConfigService();
    appConfigServiceStub.authenticationEnabled = true;
    appConfigServiceStub.adminRoles = ['test']

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                DatasetListComponent,
                SpinnerStubComponent,
                DatasetByFamilyStubComponent,
            ],
            providers: [
                provideMockStore({}),
                { provide: AppConfigService, useValue: appConfigServiceStub },
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(DatasetListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('should execute ngOnInit lifecycle', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetActions.loadDatasetList());
    });
    it('getAuthenticationEnabled() should return true', () => {
        expect(component.getAuthenticationEnabled()).toBe(true);
    });
    it('getAdminRoles() should return an array with one element', () => {
        expect(component.getAdminRoles().length).toBe(1);
    });
});
