/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatasetListComponent } from './containers/dataset-list.component';
import { DocumentationComponent } from './containers/documentation.component';
import { DocumentationTitleResolver } from './documentation-title.resolver';
import { SearchAuthGuard } from 'src/app/instance/search/search-auth.guard';

const routes: Routes = [
    { path: '', component: DatasetListComponent, title: DocumentationTitleResolver },
    { path: ':dname', canActivate: [SearchAuthGuard], component: DocumentationComponent, title: DocumentationTitleResolver }
];

/**
 * @class
 * @classdesc Documentation routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DocumentationRoutingModule { }

export const routedComponents = [
    DatasetListComponent,
    DocumentationComponent
];
