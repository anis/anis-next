/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { DocumentationRoutingModule, routedComponents } from './documentation-routing.module';
import { dummiesComponents } from './components';

/**
 * @class
 * @classdesc Documentation module.
 */
@NgModule({
    imports: [
        SharedModule,
        DocumentationRoutingModule
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ]
})
export class DocumentationModule { }
