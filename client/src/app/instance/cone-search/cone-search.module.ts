/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { coneSearchComponents } from './components';

@NgModule({
    imports: [
        SharedModule,
        RouterModule
    ],
    declarations: [
        coneSearchComponents
    ],
    exports: [
        coneSearchComponents
    ]
})
export class ConeSearchModule { }
