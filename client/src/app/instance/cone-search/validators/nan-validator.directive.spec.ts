/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { UntypedFormControl } from '@angular/forms';

import { nanValidator } from './nan-validator.directive';

describe('[Instance][ConeSearch][Validators] nanValidator', () => {
    it('should return valid', () => {
        let field = new UntypedFormControl('');
        expect(nanValidator(field)).toBeNull();
        field.setValue(17);
        expect(nanValidator(field)).toBeNull();
    });

    it('should return nan error', () => {
        let field = new UntypedFormControl('');
        field.setValue('toto');
        expect(nanValidator(field).nan.value).toBe('toto is not a number');
    });
});
