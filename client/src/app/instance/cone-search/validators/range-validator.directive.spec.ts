/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { UntypedFormControl } from '@angular/forms';

import { rangeValidator } from './range-validator.directive';

describe('[Instance][ConeSearch][Validators] rangeValidator', () => {
    it('should return valid', () => {
        let field = new UntypedFormControl('', rangeValidator(0, 10));
        field.setValue(7);
        expect(field.errors).toBeNull();
    });

    it('should return range error', () => {
        let field = new UntypedFormControl('', rangeValidator(0, 10));
        field.setValue(17);
        expect(field.errors.range).toBeTruthy();
        field.setValue(-2);
        expect(field.errors.range).toBeTruthy();
    });

    it('should return range error with form label', () => {
        let field = new UntypedFormControl('', rangeValidator(0, 10, 'toto'));
        field.setValue(17);
        expect(field.errors.range.value).toEqual('toto must be between 0 and 10');
    });
});
