/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { UntypedFormControl } from '@angular/forms';

/**
 * Validates NaN.
 *
 * @param  {FormControl} control - The form control where to check NaN.
 *
 * @return {[key: string]: any} | null
 */
export function nanValidator(control: UntypedFormControl): {[key: string]: any} | null {
    const value = parseFloat(control.value);
    const regexp: RegExp = /^\-?\d+([.,]+\d*)?$/;
    const isFloat = regexp.test(control.value);
    if (control.value === '' || control.value === null) {
        return null;
    }
    if (isNaN(value) || !isFloat) {
        return { 'nan': { value: `${control.value} is not a number` } };
    }
    return null;
}
