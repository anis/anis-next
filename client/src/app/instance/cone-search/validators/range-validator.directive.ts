/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ValidatorFn, AbstractControl } from '@angular/forms';

/**
 * Validates range.
 *
 * @param  {number} min - The minimum for the range.
 * @param  {number} max - The maximum for the range.
 * @param  {string} [formLabel] - The label of the form displayed in error message.
 *
 * @return ValidatorFn
 */
export function rangeValidator(min: number, max: number, formLabel?: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        const value = parseFloat(control.value);
        if (value < min || value > max) {
            if (formLabel) {
                return { 'range': { value: `${formLabel} must be between ${min} and ${max}` } };
            }
            return { 'range': { value: `Must be between ${min} and ${max}` } };
        }
        return null;
    }
}
