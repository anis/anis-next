/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ConeSearchComponent } from './cone-search.component';
import { ResolverComponent } from './resolver.component';
import { RaComponent } from './ra.component';
import { DecComponent } from './dec.component';
import { RadiusComponent } from './radius.component';
import { ConeSearchParametersComponent } from './cone-search-parameters.component';

export const coneSearchComponents = [
    ConeSearchComponent,
    ResolverComponent,
    RaComponent,
    DecComponent,
    RadiusComponent,
    ConeSearchParametersComponent
];
