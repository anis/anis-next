/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

import { of } from 'rxjs';

import { RaComponent } from './ra.component';
import { nanValidator, rangeValidator } from '../validators';

describe('[Instance][ConeSearch][Components] RaComponent', () => {
    let form = new FormGroup({
        ra: new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 360, 'RA')]),
        ra_hms: new FormGroup({
            h: new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 24, 'Hours')]),
            m: new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 60, 'Minutes')]),
            s: new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 60, 'Seconds')])
        })
    });

    @Component({
        selector: `app-host`,
        template: `
            <app-ra
                    [form]="form" 
                    [unit]="unit" 
                    [resolver]="resolver">
            </app-ra>`
    })
    class TestHostComponent {
        @ViewChild(RaComponent, { static: false })
        public testedComponent: RaComponent;
        public form: FormGroup = form;
        public unit: string = 'degree';
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: RaComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                RaComponent
            ],
            imports: [ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#ngOnInit() should disable ra_hms control and convert degrees to HMS', () => {
        testedComponent.form.controls.ra.setValue(78.2);
        expect(testedComponent.form.controls.ra_hms.disabled).toBeTruthy();
        setTimeout(function () {
            expect(testedComponent.getRaHmsForm().controls.h.value).toEqual(5);
            expect(testedComponent.getRaHmsForm().controls.m.value).toEqual(12);
            expect(testedComponent.getRaHmsForm().controls.s.value).toEqual(48);
        }, 300);
    });

    it('should call ngOnChanges and apply changes', () => {
        testedComponent.raControlSubscription = of().subscribe();
        testedComponent.raHmsFormSubscription = of().subscribe();
        const spyRa = jest.spyOn(testedComponent.raControlSubscription, 'unsubscribe');
        const spy = jest.spyOn(testedComponent, 'ngOnChanges');
        testHostComponent.unit = 'hms';
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(testedComponent.form.controls.ra_hms.enabled).toBeTruthy();
        expect(testedComponent.form.controls.ra.disabled).toBeTruthy();
        expect(spyRa).toHaveBeenCalledTimes(1);
        testHostComponent.unit = 'degree';
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(2);
        expect(testedComponent.form.controls.ra_hms.disabled).toBeTruthy();
        expect(testedComponent.form.controls.ra.enabled).toBeTruthy();
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(2);
        expect(testedComponent.form.controls.ra.value).toEqual(78.2);
    });

    it('#getRaHmsForm() should return RA HMS form control', () => {
        const form: FormGroup = testedComponent.getRaHmsForm();
        expect(Object.keys(form.controls).length).toEqual(3);
        expect(Object.keys(form.controls)).toContain('h');
        expect(Object.keys(form.controls)).toContain('m');
        expect(Object.keys(form.controls)).toContain('s');
    });

    it('#deg2HMS(value) convert RA from degree to HH:MM:SS', () => {
        testedComponent.deg2HMS(78.2);
        expect(testedComponent.getRaHmsForm().controls.h.value).toBe(5);
        expect(testedComponent.getRaHmsForm().controls.m.value).toBe(12);
        expect(parseFloat(testedComponent.getRaHmsForm().controls.s.value)).toBe(48);
    });

    it('#HMS2Deg(hms) convert RA from HH:MM:SS to degree', () => {
        testedComponent.HMS2Deg({ h: 5, m: 12, s: 48 });
        expect(testedComponent.form.controls.ra.value).toBe(78.2);
    });

    it('#ngOnDestroy() should unsubscribe from raControlSubscription and raHmsFormSubscription', () => {
        testedComponent.raControlSubscription = of().subscribe();
        testedComponent.raHmsFormSubscription = of().subscribe();
        const spyRa = jest.spyOn(testedComponent.raControlSubscription, 'unsubscribe');
        const spyRaHms = jest.spyOn(testedComponent.raHmsFormSubscription, 'unsubscribe');
        testedComponent.ngOnDestroy();
        expect(spyRa).toHaveBeenCalledTimes(1);
        expect(spyRaHms).toHaveBeenCalledTimes(1);
    });
});
