/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

import { of } from 'rxjs';

import { DecComponent } from './dec.component';
import { nanValidator, rangeValidator } from '../validators';

describe('[Instance][ConeSearch][Components] DecComponent', () => {
    let form = new FormGroup({
        dec: new FormControl('', [Validators.required, nanValidator, rangeValidator(-90, 90, 'DEC')]),
        dec_dms: new FormGroup({
            d: new FormControl('', [Validators.required, nanValidator, rangeValidator(-90, 90, 'Degree')]),
            m: new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 60, 'Minutes')]),
            s: new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 60, 'Seconds')])
        })
    });

    @Component({
        selector: `app-host`,
        template: `
            <app-dec
                    [form]="form" 
                    [unit]="unit" 
                    [resolver]="resolver">
            </app-dec>`
    })
    class TestHostComponent {
        @ViewChild(DecComponent, { static: false })
        public testedComponent: DecComponent;
        public form: FormGroup = form;
        public unit: string = 'degree';
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DecComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                DecComponent
            ],
            imports: [ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#ngOnInit() should disable dec_dms control and convert degrees to HMS', () => {
        testHostComponent.form.controls.dec.setValue(32.87);
        expect(testedComponent.form.controls.dec_dms.disabled).toBeTruthy();
        setTimeout(function () {
            expect(testedComponent.getDecDmsForm().controls.d.value).toEqual(32);
            expect(testedComponent.getDecDmsForm().controls.m.value).toEqual(52);
            expect(testedComponent.getDecDmsForm().controls.s.value).toEqual(12);
        }, 300);
    });

    it('should call ngOnChanges and apply changes', () => {
        testedComponent.decControlSubscription = of().subscribe();
        testedComponent.decDmsSubscription = of().subscribe();
        const spyDec = jest.spyOn(testedComponent.decControlSubscription, 'unsubscribe');
        const spy = jest.spyOn(testedComponent, 'ngOnChanges');
        testHostComponent.unit = 'hms';
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(testedComponent.form.controls.dec_dms.enabled).toBeTruthy();
        expect(testedComponent.form.controls.dec.disabled).toBeTruthy();
        expect(spyDec).toHaveBeenCalledTimes(1);
        testHostComponent.unit = 'degree';
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(2);
        expect(testedComponent.form.controls.dec_dms.disabled).toBeTruthy();
        expect(testedComponent.form.controls.dec.enabled).toBeTruthy();
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(2);
        expect(testedComponent.form.controls.dec.value).toEqual(32.87);
    });

    it('#getDecDmsForm() should return DEC DMS form control', () => {
        const form: FormGroup = testedComponent.getDecDmsForm();
        expect(Object.keys(form.controls).length).toEqual(3);
        expect(Object.keys(form.controls)).toContain('d');
        expect(Object.keys(form.controls)).toContain('m');
        expect(Object.keys(form.controls)).toContain('s');
    });

    it('#deg2DMS(value) convert DEC from degree to D:MM:SS', () => {
        testedComponent.deg2DMS(32.87);
        expect(testedComponent.getDecDmsForm().controls.d.value).toBe(32);
        expect(testedComponent.getDecDmsForm().controls.m.value).toBe(52);
        expect(parseFloat(testedComponent.getDecDmsForm().controls.s.value)).toBe(12);
    });

    it('#DMS2Deg(dms) convert DEC from D:MM:SS to degree', () => {
        testedComponent.DMS2Deg({ d: 32, m: 52, s: 12 });
        expect(testedComponent.form.controls.dec.value).toBe(32.87);
        testedComponent.DMS2Deg({ d: -32, m: 52, s: 12 });
        expect(testedComponent.form.controls.dec.value).toBe(-32.87);
    });

    it('#ngOnDestroy() should unsubscribe from decControlSubscription and decDmsSubscription', () => {
        testedComponent.decControlSubscription = of().subscribe();
        testedComponent.decDmsSubscription = of().subscribe();
        const spyDec = jest.spyOn(testedComponent.decControlSubscription, 'unsubscribe');
        const spyDecDms = jest.spyOn(testedComponent.decDmsSubscription, 'unsubscribe');
        testedComponent.ngOnDestroy();
        expect(spyDec).toHaveBeenCalledTimes(1);
        expect(spyDecDms).toHaveBeenCalledTimes(1);
    });
});
