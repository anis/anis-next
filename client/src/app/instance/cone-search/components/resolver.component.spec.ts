/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { ResolverComponent } from './resolver.component';
import { ConeSearch } from 'src/app/instance/store/models';

describe('[Instance][ConeSearch][Components] ResolverComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-resolver
                [coneSearch]="coneSearch"
                [resolverIsLoading]="resolverIsLoading"
                [resolverIsLoaded]="resolverIsLoaded">
            </app-resolver>`
    })
    class TestHostComponent {
        @ViewChild(ResolverComponent, { static: false })
        public testedComponent: ResolverComponent;
        public coneSearch: ConeSearch = undefined;
        public resolverIsLoading: boolean = false;
        public resolverIsLoaded: boolean = false;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: ResolverComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                ResolverComponent
            ],
            imports: [ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#submit() should raise retrieveCoordinates event', () => {
        testedComponent.form.controls.name.setValue('myObjectName');
        const spy = jest.spyOn(testedComponent.retrieveCoordinates, 'emit');
        testedComponent.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('myObjectName');
    });
});
