/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { ConeSearch } from 'src/app/instance/store/models';

/**
 * @class
 * @classdesc DEC component.
 *
 * @implements OnInit
 * @implements OnChanges
 * @implements OnDestroy
 */
@Component({
    selector: 'app-dec',
    templateUrl: 'dec.component.html',
    styleUrls: ['input-group.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DecComponent implements OnInit, OnDestroy, OnChanges {
    @Input() form: UntypedFormGroup;
    @Input() unit: string;
    @Input() coneSearch: ConeSearch;

    public decControlSubscription: Subscription
    public decDmsSubscription: Subscription;

    ngOnInit(): void {
        if (this.unit === 'degree') {
            this.form.controls.dec_dms.disable();
        } else {
            this.form.controls.dec.disable();
        }
        
        this.decControlSubscription = this.form.controls.dec.valueChanges.pipe(debounceTime(250))
            .subscribe(deg => this.deg2DMS(deg));
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.unit && !changes.unit.firstChange) {
            if (changes.unit.currentValue === 'degree') {
                this.form.controls.dec_dms.disable();
                this.form.controls.dec.enable();
                this.decControlSubscription = this.form.controls.dec.valueChanges.pipe(debounceTime(250))
                    .subscribe(deg => this.deg2DMS(deg));
                if (this.decDmsSubscription) this.decDmsSubscription.unsubscribe();
            }
            if (changes.unit.currentValue === 'hms') {
                this.form.controls.dec_dms.enable();
                this.form.controls.dec.disable();
                this.decDmsSubscription = this.form.controls.dec_dms.valueChanges.pipe(debounceTime(250))
                    .subscribe(value => this.DMS2Deg(value));
                if (this.decControlSubscription) this.decControlSubscription.unsubscribe();
            }
        }

        if (changes.coneSearch && changes.coneSearch.currentValue) {
            const dec = changes.coneSearch.currentValue.dec;
            this.form.controls.dec.setValue(changes.coneSearch.currentValue.dec);
            this.deg2DMS(dec);
        }
    }

    /**
     * Returns DEC form group.
     *
     * @return FormGroup
     */
    getDecDmsForm(): UntypedFormGroup {
        return this.form.controls.dec_dms as UntypedFormGroup;
    }

    /**
     * Converts DEC degree minute second from degree and sets Dec DMS fields.
     *
     * @param  {number} deg - The degree value.
     */
    deg2DMS(deg: number): void {
        if (deg !== null) {
            const hh = Math.trunc(deg);
            let tmp = (Math.abs(deg - hh)) * 60;
            const mm = Math.trunc(tmp);
            tmp = (tmp - mm) * 60;
            const ss = tmp.toFixed(2);
            const decDmsForm = this.getDecDmsForm();
            decDmsForm.controls.d.setValue(hh);
            decDmsForm.controls.m.setValue(mm);
            decDmsForm.controls.s.setValue(ss);
        }
    }

    /**
     * Sets DEC degree from degree minute second and sets RA degree field.
     *
     * @param {d: number, m: number, s: number} dms - Coordinates in DMS.
     */
    DMS2Deg(dms: {d: number, m: number, s: number }): void {
        if (dms !== null) {
            const tmp = ((dms.s / 60) + dms.m) / 60;
            let deg = tmp + Math.abs(dms.d);
            if (dms.d < 0) {
                deg = -deg;
            }
            this.form.controls.dec.setValue(deg);
        }
    }

    ngOnDestroy(): void {
        if (this.decControlSubscription) this.decControlSubscription.unsubscribe();
        if (this.decDmsSubscription) this.decDmsSubscription.unsubscribe();
    }
}
