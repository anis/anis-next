/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, OnDestroy, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { ConeSearch } from 'src/app/instance/store/models';

/**
 * @class
 * @classdesc RA component.
 *
 * @implements OnInit
 * @implements OnChanges
 * @implements OnDestroy
 */
@Component({
    selector: 'app-ra',
    templateUrl: 'ra.component.html',
    styleUrls: ['input-group.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RaComponent implements OnInit, OnDestroy, OnChanges  {
    @Input() form: UntypedFormGroup;
    @Input() unit: string;
    @Input() coneSearch: ConeSearch;

    public raControlSubscription: Subscription;
    public raHmsFormSubscription: Subscription;

    ngOnInit(): void {
        if (this.unit === 'degree') {
            this.form.controls.ra_hms.disable();
        } else {
            this.form.controls.ra.disable();
        }

        this.raControlSubscription = this.form.controls.ra.valueChanges.pipe(debounceTime(250))
            .subscribe(deg => this.deg2HMS(deg));
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.unit && !changes.unit.firstChange) {
            if (changes.unit.currentValue === 'degree') {
                this.form.controls.ra_hms.disable();
                this.form.controls.ra.enable();
                this.raControlSubscription = this.form.controls.ra.valueChanges.pipe(debounceTime(250))
                    .subscribe(deg => this.deg2HMS(deg));
                if (this.raHmsFormSubscription) this.raHmsFormSubscription.unsubscribe();
            }
            if (changes.unit.currentValue === 'hms') {
                this.form.controls.ra_hms.enable();
                this.form.controls.ra.disable();
                this.raHmsFormSubscription = this.form.controls.ra_hms.valueChanges.pipe(debounceTime(250))
                    .subscribe(value => this.HMS2Deg(value));
                if (this.raControlSubscription) this.raControlSubscription.unsubscribe();
            }
        }

        if (changes.coneSearch && changes.coneSearch.currentValue) {
            const ra = changes.coneSearch.currentValue.ra;
            this.form.controls.ra.setValue(changes.coneSearch.currentValue.ra);
            this.deg2HMS(ra);
        }
    }

    /**
     * Returns RA form group.
     *
     * @return FormGroup
     */
    getRaHmsForm(): UntypedFormGroup {
        return this.form.controls.ra_hms as UntypedFormGroup;
    }

    /**
     * Converts RA hour minute second from degree and sets RA HMS fields.
     *
     * @param  {number} deg - The degree value.
     */
    deg2HMS(deg: number): void {
        if (deg !== null) {
            let tmp = deg / 15;
            const hh = Math.trunc(tmp);
            tmp = (tmp - hh) * 60;
            const mm = Math.trunc(tmp);
            tmp = (tmp - mm) * 60;
            const ss = +tmp.toFixed(2);
            const raHmsForm = this.getRaHmsForm();
            raHmsForm.controls.h.setValue(hh);
            raHmsForm.controls.m.setValue(mm);
            raHmsForm.controls.s.setValue(ss);
        }
    }

    /**
     * Sets RA degree from hour minute second and sets RA degree field.
     *
     * @param {h: number, m: number, s: number} hms - Coordinates in HMS.
     */
    HMS2Deg(hms: { h: number, m: number, s: number }): void {
        if (hms !== null) {
            const deg = +(((((hms.s / 60) + hms.m) / 60) + hms.h) * 15).toFixed(8);
            this.form.controls.ra.setValue(deg);
        }
    }

    ngOnDestroy(): void {
        if (this.raControlSubscription) this.raControlSubscription.unsubscribe();
        if (this.raHmsFormSubscription) this.raHmsFormSubscription.unsubscribe();
    }
}
