/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { RadiusComponent } from './radius.component';

describe('[Instance][ConeSearch][Components] RadiusComponent', () => {
    let component: RadiusComponent;
    let fixture: ComponentFixture<RadiusComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [RadiusComponent],
            imports: [ReactiveFormsModule]
        });
        fixture = TestBed.createComponent(RadiusComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
