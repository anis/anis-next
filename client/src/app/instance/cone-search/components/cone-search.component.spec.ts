/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input, ViewChild } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';

import { ConeSearchComponent } from './cone-search.component';
import { ConeSearch } from 'src/app/instance/store/models';

describe('[Instance][ConeSearch][Components] ConeSearchComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-cone-search
                [coneSearch]="coneSearch"
                [resolverIsLoading]="resolverIsLoading"
                [resolverIsLoaded]="resolverIsLoaded">
            </app-cone-search>`
    })
    class TestHostComponent {
        @ViewChild(ConeSearchComponent, { static: false })
        public testedComponent: ConeSearchComponent;
        public coneSearch: ConeSearch = undefined;
        public resolverIsLoading: boolean = false;
        public resolverIsLoaded: boolean = false;
    }

    @Component({ selector: 'app-resolver', template: '' })
    class ResolverStubComponent {
        @Input() coneSearch: ConeSearch;
        @Input() resolverIsLoading: boolean;
        @Input() resolverIsLoaded: boolean;
    }

    @Component({ selector: 'app-ra', template: '' })
    class RaStubComponent {
        @Input() form: FormGroup;
        @Input() unit: string;
    }

    @Component({ selector: 'app-dec', template: '' })
    class DecStubComponent {
        @Input() form: FormGroup;
        @Input() unit: string;
    }

    @Component({ selector: 'app-radius', template: '' })
    class RadiusStubComponent {
        @Input() form: FormGroup;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: ConeSearchComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                ConeSearchComponent,
                ResolverStubComponent,
                RaStubComponent,
                DecStubComponent,
                RadiusStubComponent
            ],
            imports: [ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('should call ngOnChanges and apply changes', () => {
        const spy = jest.spyOn(testedComponent, 'ngOnChanges');
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(0);
        expect(testedComponent.unit).toEqual('degree');
        expect(testedComponent.form.controls.ra.enabled).toBeTruthy();
        expect(testedComponent.form.controls.dec.enabled).toBeTruthy();
        expect(testedComponent.form.controls.radius.enabled).toBeTruthy();

        testHostComponent.coneSearch = { ra: 1, dec: 2, radius: 3 };
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(testedComponent.form.controls.ra.value).toEqual(null);
        expect(testedComponent.form.controls.dec.value).toEqual(null);
        expect(testedComponent.form.controls.radius.value).toEqual(3);

        testHostComponent.coneSearch = undefined;
        testedComponent.unit = 'hms';
        testHostFixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(2);
        expect(testedComponent.form.controls.ra_hms.enabled).toBeTruthy();
        expect(testedComponent.form.controls.dec_dms.enabled).toBeTruthy();
    });

    it('#getConeSearch() should return cone search from form', () => {
        testedComponent.form.controls.ra.setValue(1);
        testedComponent.form.controls.dec.setValue(2);
        testedComponent.form.controls.radius.setValue(3);
        expect(testedComponent.getConeSearch()).toEqual({ ra: 1, dec: 2, radius: 3 });
    });
});
