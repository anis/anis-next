/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { ConeSearch } from "src/app/instance/store/models";

@Component({
    selector: 'app-cone-search-parameters',
    templateUrl: 'cone-search-parameters.component.html',
    styleUrls: ['cone-search-parameters.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchParametersComponent {
    @Input() coneSearch: ConeSearch;
}
