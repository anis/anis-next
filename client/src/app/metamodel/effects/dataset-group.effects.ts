/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as datasetGroupActions from '../actions/dataset-group.actions';
import { DatasetGroupService } from '../services/dataset-group.service';
import * as instanceSelector from '../selectors/instance.selector';

/**
 * @class
 * @classdesc Dataset group effects.
 */
@Injectable()
export class DatasetGroupEffects {
    /**
     * Calls action to retrieve group list.
     */
    loadDatasetGroups$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetGroupActions.loadDatasetGroupList),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([, instanceName]) => this.groupService.retrieveDatasetGroupList(instanceName)
                .pipe(
                    map(datasetGroups => datasetGroupActions.loadDatasetGroupListSuccess({ datasetGroups })),
                    catchError(() => of(datasetGroupActions.loadDatasetGroupListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a dataset group.
     */
    addDatasetGroup$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetGroupActions.addDatasetGroup),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.groupService.addDatasetGroup(instanceName, action.datasetGroup)
                .pipe(
                    map(datasetGroup => datasetGroupActions.addDatasetGroupSuccess({ datasetGroup })),
                    catchError(() => of(datasetGroupActions.addDatasetGroupFail()))
                )
            )
        )
    );

    /**
     * Displays add dataset group success notification.
     */
    addDatasetGroupSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetGroupActions.addDatasetGroupSuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.router.navigateByUrl(`/admin/instance/configure-instance/${instanceName}/dataset-group`);
                this.toastr.success('Dataset group successfully added', 'The new dataset group was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add dataset group error notification.
     */
    addDatasetGroupFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetGroupActions.addDatasetGroupFail),
            tap(() => this.toastr.error('Failure to add dataset group', 'The new dataset group could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a dataset group.
     */
    editDatasetGroup$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetGroupActions.editDatasetGroup),
            mergeMap(action => this.groupService.editDatasetGroup(action.datasetGroup)
                .pipe(
                    map(datasetGroup => datasetGroupActions.editDatasetGroupSuccess({ datasetGroup })),
                    catchError(() => of(datasetGroupActions.editDatasetGroupFail()))
                )
            )
        )
    );

    /**
     * Displays edit dataset group success notification.
     */
    editDatasetGroupSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetGroupActions.editDatasetGroupSuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.router.navigateByUrl(`/admin/instance/configure-instance/${instanceName}/dataset-group`);
                this.toastr.success('Dataset group successfully edited', 'The existing dataset group has been edited into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays edit group error notification.
     */
    editDatasetGroupFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetGroupActions.editDatasetGroupFail),
            tap(() => this.toastr.error('Failure to edit dataset group', 'The existing dataset group could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a dataset group.
     */
    deleteDatasetGroup$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetGroupActions.deleteDatasetGroup),
            mergeMap(action => this.groupService.deleteDatasetGroup(action.datasetGroup.id)
                .pipe(
                    map(() => datasetGroupActions.deleteDatasetGroupSuccess({ datasetGroup: action.datasetGroup })),
                    catchError(() => of(datasetGroupActions.deleteDatasetGroupFail()))
                )
            )
        )
    );

    /**
     * Displays delete dataset group success notification.
     */
    deleteDatasetGroupSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetGroupActions.deleteDatasetGroupSuccess),
            tap(() => this.toastr.success('Dataset group successfully deleted', 'The existing dataset group has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete dataset group error notification.
     */
    deleteDatasetGroupFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetGroupActions.deleteDatasetGroupFail),
            tap(() => this.toastr.error('Failure to delete dataset group', 'The existing dataset group could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private groupService: DatasetGroupService,
        private router: Router,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
