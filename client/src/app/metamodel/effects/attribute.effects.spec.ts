/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';

import { ToastrService } from 'ngx-toastr';
import { AttributeEffects } from './attribute.effects';
import { AttributeService } from '../services/attribute.service';
import * as attributeActions from '../actions/attribute.actions';
import { ATTRIBUTE, ATTRIBUTE_LIST } from '../../../test-data';
import * as datasetSelector from '../selectors/dataset.selector';
import { Attribute } from '../models';

describe('[Metamodel][Effects] AttributeEffects', () => {
    let actions = new Observable();
    let effects: AttributeEffects;
    let metadata: EffectsMetadata<AttributeEffects>;
    let service: AttributeService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockDatasetSelectorSelectDatasetNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AttributeEffects,
                { provide: AttributeService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(AttributeEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(AttributeService);
        toastr = TestBed.inject(ToastrService);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute, ''
        );
        router = TestBed.inject(Router);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadAttributes$ effect', () => {
        it('should dispatch the loadAttributeListSuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = attributeActions.loadAttributeList();
            const outcome = attributeActions.loadAttributeListSuccess({ attributes: ATTRIBUTE_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: ATTRIBUTE_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveAttributeList = jest.fn(() => response);

            expect(effects.loadAttributes$).toBeObservable(expected);
            expect(service.retrieveAttributeList).toHaveBeenCalledWith('myDataset');
        });

        it('should dispatch the loadAttributeListFail action on HTTP failure', () => {
            const action = attributeActions.loadAttributeList();
            const error = new Error();
            const outcome = attributeActions.loadAttributeListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveAttributeList = jest.fn(() => response);

            expect(effects.loadAttributes$).toBeObservable(expected);
        });
    });
    describe('addAttributeList$ effect', () => {
        it('should  dispatch  addAttribute action', () => {
            let attribute: Attribute;
            let attributeList: Attribute[] = [{ ...attribute, id: 1 }]
            actions = hot('a', { a: attributeActions.addAttributeList({ attributeList }) });
            let spy = jest.spyOn(store, 'dispatch');
            let expected = cold('a', { a: attributeActions.addAttributeList({ attributeList }) });
            expect(effects.addAttributeList$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith(attributeActions.addAttribute({ attribute: { ...attribute, id: 1 } }));
        });

    });
    describe('addAttribute$ effect', () => {
        it('should dispatch the addAttributeSuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = attributeActions.addAttribute({ attribute: ATTRIBUTE });
            const outcome = attributeActions.addAttributeSuccess({ attribute: ATTRIBUTE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: ATTRIBUTE });
            const expected = cold('--b', { b: outcome });
            service.addAttribute = jest.fn(() => response);

            expect(effects.addAttribute$).toBeObservable(expected);
            expect(service.addAttribute).toHaveBeenCalledWith('myDataset', ATTRIBUTE);
        });

        it('should dispatch the addAttributeFail action on HTTP failure', () => {
            const action = attributeActions.addAttribute({ attribute: ATTRIBUTE });
            const error = new Error();
            const outcome = attributeActions.addAttributeFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.addAttribute = jest.fn(() => response);

            expect(effects.addAttribute$).toBeObservable(expected);
        });
    });

    describe('addAttributeSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addAttributeSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = attributeActions.addAttributeSuccess({ attribute: ATTRIBUTE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addAttributeSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Attribute successfully added',
                'The new attribute was added into the database'
            );
        });
    });

    describe('addAttributeFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addAttributeFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = attributeActions.addAttributeFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addAttributeFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add attribute',
                'The new attribute could not be added into the database'
            );
        });
    });

    describe('editAttribute$ effect', () => {
        it('should dispatch the editAttributeSuccess action on success', () => {
            const action = attributeActions.editAttribute({ attribute: ATTRIBUTE });
            const outcome = attributeActions.editAttributeSuccess({ attribute: ATTRIBUTE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: ATTRIBUTE });
            const expected = cold('--b', { b: outcome });
            service.editAttribute = jest.fn(() => response);

            expect(effects.editAttribute$).toBeObservable(expected);
        });

        it('should dispatch the editAttributeFail action on HTTP failure', () => {
            const action = attributeActions.editAttribute({ attribute: ATTRIBUTE });
            const error = new Error();
            const outcome = attributeActions.editAttributeFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.editAttribute = jest.fn(() => response);

            expect(effects.editAttribute$).toBeObservable(expected);
        });
    });

    describe('editAttributeFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editAttributeFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = attributeActions.editAttributeFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editAttributeFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit attribute',
                'The existing attribute could not be edited into the database'
            );
        });
    });

    describe('deleteAttribute$ effect', () => {
        it('should dispatch the deleteAttributeSuccess action on success', () => {
            const action = attributeActions.deleteAttribute({ attribute: ATTRIBUTE });
            const outcome = attributeActions.deleteAttributeSuccess({ attribute: ATTRIBUTE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: ATTRIBUTE });
            const expected = cold('--b', { b: outcome });
            service.deleteAttribute = jest.fn(() => response);

            expect(effects.deleteAttribute$).toBeObservable(expected);
        });

        it('should dispatch the deleteAttributeFail action on HTTP failure', () => {
            const action = attributeActions.deleteAttribute({ attribute: ATTRIBUTE });
            const error = new Error();
            const outcome = attributeActions.deleteAttributeFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.deleteAttribute = jest.fn(() => response);

            expect(effects.deleteAttribute$).toBeObservable(expected);
        });
    });

    describe('deleteAttributeSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteAttributeSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = attributeActions.deleteAttributeSuccess({ attribute: ATTRIBUTE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteAttributeSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Attribute successfully deleted',
                'The existing attribute has been deleted'
            );
        });
    });

    describe('deleteAttributeFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteAttributeFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = attributeActions.deleteAttributeFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteAttributeFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete attribute',
                'The existing attribute could not be deleted from the database'
            );
        });
    });
});
