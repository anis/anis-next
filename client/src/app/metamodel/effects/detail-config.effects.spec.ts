/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import { DetailConfigEffects } from './detail-config.effects';
import { DetailConfigService } from '../services/detail-config.service';
import * as datasetSelector from '../selectors/dataset.selector';
import * as detailConfigActions from '../actions/detail-config.actions';
import { DetailConfig } from '../models';

describe('[Metamodel][Effects] DetailConfigEffects', () => {
    let actions = new Observable();
    let effects: DetailConfigEffects;
    let metadata: EffectsMetadata<DetailConfigEffects>;
    let service: DetailConfigService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockDatasetSelectorDatasetNameByRoute;
    let router: Router;
    let detailConfig: DetailConfig;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DetailConfigEffects,
                { provide: DetailConfigService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(DetailConfigEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(DetailConfigService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute, 'test'
        );
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadDetailsConfig$ effect', () => {
        it('should dispatch the loadDetailConfigSuccess action on success', () => {
            actions = hot('a', { a: detailConfigActions.loadDetailConfig() });
            service.retrieveDetailConfig = jest.fn(() => of({ ...detailConfig, id: 1 }));
            let expected = cold('b', { b: detailConfigActions.loadDetailConfigSuccess({ detailConfig: { ...detailConfig, id: 1 } }) });
            expect(effects.loadDetailsConfig$).toBeObservable(expected);
            expect(service.retrieveDetailConfig).toHaveBeenCalledWith('test');

        });
        it('should dispatch the loadDetailConfigFail action on HTTP failure', () => {
            actions = hot('a', { a: detailConfigActions.loadDetailConfig() });
            service.retrieveDetailConfig = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: detailConfigActions.loadDetailConfigFail() });
            expect(effects.loadDetailsConfig$).toBeObservable(expected);
            expect(service.retrieveDetailConfig).toHaveBeenCalledWith('test');

        });
    });
    describe('addDetailConfig$ effect', () => {
        it('should dispatch the addDetailConfigSuccess action on success', () => {
            actions = hot('a', { a: detailConfigActions.addDetailConfig({ detailConfig: { ...detailConfig, id: 1 } }) });
            service.addDetailConfig = jest.fn(() => of({ ...detailConfig, id: 1 }));
            let expected = cold('b', { b: detailConfigActions.addDetailConfigSuccess({ detailConfig: { ...detailConfig, id: 1 } }) });
            expect(effects.addDetailConfig$).toBeObservable(expected);
            expect(service.addDetailConfig).toHaveBeenCalledWith('test', { ...detailConfig, id: 1 });

        });
        it('should dispatch the addDetailConfigFail action on HTTP failure', () => {
            actions = hot('a', { a: detailConfigActions.addDetailConfig({ detailConfig: { ...detailConfig, id: 1 } }) });
            service.addDetailConfig = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: detailConfigActions.addDetailConfigFail() });
            expect(effects.addDetailConfig$).toBeObservable(expected);
            expect(service.addDetailConfig).toHaveBeenCalledWith('test', { ...detailConfig, id: 1 });

        });
    });
    describe('addDetailConfigSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: detailConfigActions.addDetailConfigSuccess({ detailConfig: { ...detailConfig, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: detailConfigActions.addDetailConfigSuccess({ detailConfig: { ...detailConfig, id: 1 } }) });
            expect(effects.addDetailConfigSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Detail config successfully added', 'The new detail config was added into the database');
        });
    });
    describe('addDetailConfigFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: detailConfigActions.addDetailConfigFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: detailConfigActions.addDetailConfigFail() });
            expect(effects.addDetailConfigFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add detail config', 'The new detail config could not be added into the database');
        });
    });
    describe('editDetailConfig$ effect', () => {
        it('should dispatch the editDetailConfigSuccess action on success', () => {
            actions = hot('a', { a: detailConfigActions.editDetailConfig({ detailConfig: { ...detailConfig, id: 1 } }) });
            service.editDetailConfig = jest.fn(() => of({ ...detailConfig, id: 1 }));
            let expected = cold('b', { b: detailConfigActions.editDetailConfigSuccess({ detailConfig: { ...detailConfig, id: 1 } }) });
            expect(effects.editDetailConfig$).toBeObservable(expected);
            expect(service.editDetailConfig).toHaveBeenCalledWith('test', { ...detailConfig, id: 1 });

        });
        it('should dispatch the editDetailConfigFail action on HTTP failure', () => {
            actions = hot('a', { a: detailConfigActions.editDetailConfig({ detailConfig: { ...detailConfig, id: 1 } }) });
            service.editDetailConfig = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: detailConfigActions.editDetailConfigFail() });
            expect(effects.editDetailConfig$).toBeObservable(expected);
            expect(service.editDetailConfig).toHaveBeenCalledWith('test', { ...detailConfig, id: 1 });

        });
    });

    describe('editDetailConfigSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: detailConfigActions.editDetailConfigSuccess({ detailConfig: { ...detailConfig, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: detailConfigActions.editDetailConfigSuccess({ detailConfig: { ...detailConfig, id: 1 } }) });
            expect(effects.editDetailConfigSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Detail configuration successfully edited', 'The existing detail configuration has been edited into the database');
        });
    });
    describe('editDetailConfigFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: detailConfigActions.editDetailConfigFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: detailConfigActions.editDetailConfigFail() });
            expect(effects.editDetailConfigFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit detail configuration', 'The existing detail configuration could not be edited into the database');
        });
    });

});
