/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as outputFamilyActions from '../actions/output-family.actions';
import { OutputFamilyService } from '../services/output-family.service';
import * as datasetSelector from '../selectors/dataset.selector';

/**
 * @class
 * @classdesc Output family effects.
 */
@Injectable()
export class OutputFamilyEffects {

    /**
     * Calls action to retrieve output family list for the given dataset.
     */
    loadOutputFamilies$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputFamilyActions.loadOutputFamilyList),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.outputFamilyService.retrieveOutputFamilyList(datasetName)
                .pipe(
                    map(outputFamilies => outputFamilyActions.loadOutputFamilyListSuccess({ outputFamilies })),
                    catchError(() => of(outputFamilyActions.loadOutputFamilyListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add an output family to the given dataset.
     */
    addOutputFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputFamilyActions.addOutputFamily),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.outputFamilyService.addOutputFamily(datasetName, action.outputFamily)
                .pipe(
                    map(outputFamily => outputFamilyActions.addOutputFamilySuccess({ outputFamily })),
                    catchError(() => of(outputFamilyActions.addOutputFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays add output family success notification.
     */
    addOutputFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputFamilyActions.addOutputFamilySuccess),
            tap(() => this.toastr.success('Output family successfully added', 'The new output family was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add output family error notification.
     */
    addOutputFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputFamilyActions.addOutputFamilyFail),
            tap(() => this.toastr.error('Failure to add output family', 'The new output family could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an output family.
     */
    editOutputFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputFamilyActions.editOutputFamily),
            mergeMap(action => this.outputFamilyService.editOutputFamily(action.outputFamily)
                .pipe(
                    map(outputFamily => outputFamilyActions.editOutputFamilySuccess({ outputFamily })),
                    catchError(() => of(outputFamilyActions.editOutputFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays edit output family success notification.
     */
    editOutputFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputFamilyActions.editOutputFamilySuccess),
            tap(() => this.toastr.success('Output family successfully edited', 'The existing output family has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit output family error notification.
     */
    editOutputFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputFamilyActions.editOutputFamilyFail),
            tap(() => this.toastr.error('Failure to edit output family', 'The existing output family could not be edited into the database'))
        ), { dispatch: false }
    );


    /**
     * Calls action to remove an output family.
     */
    deleteOutputFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputFamilyActions.deleteOutputFamily),
            mergeMap(action => this.outputFamilyService.deleteOutputFamily(action.outputFamily.id)
                .pipe(
                    map(() => outputFamilyActions.deleteOutputFamilySuccess({ outputFamily: action.outputFamily })),
                    catchError(() => of(outputFamilyActions.deleteOutputFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays delete output family success notification.
     */
    deleteOutputFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputFamilyActions.deleteOutputFamilySuccess),
            tap(() => this.toastr.success('Output family successfully deleted', 'The existing output family has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete output family error notification.
     */
    deleteOutputFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputFamilyActions.deleteOutputFamilyFail),
            tap(() => this.toastr.error('Failure to delete output family', 'The existing output family could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private outputFamilyService: OutputFamilyService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
