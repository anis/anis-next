/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import * as datasetSelector from '../selectors/dataset.selector';
import * as coneSearchConfigActions from '../actions/cone-search-config.actions';
import { ConeSearchConfigEffects } from './cone-search-config.effects';
import { ConeSearchConfigService } from '../services/cone-search-config.service';
import { ConeSearchConfig } from '../models';

describe('[Metamodel][Effects] ConeSearchConfigEffects', () => {
    let actions = new Observable();
    let effects: ConeSearchConfigEffects;
    let metadata: EffectsMetadata<ConeSearchConfigEffects>;
    let service: ConeSearchConfigService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let router: Router;
    let mockDatasetSelectorDatasetNameByRoute;
    let coneSearchConfig: ConeSearchConfig;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ConeSearchConfigEffects,
                { provide: ConeSearchConfigService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(ConeSearchConfigEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(ConeSearchConfigService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        coneSearchConfig = {
            ...coneSearchConfig,
            id: 1
        }
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadConeSearchsConfig$ effect', () => {
        it('should dispatch the loadConeSearchConfigSuccess action on success', () => {
            mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(datasetSelector.selectDatasetNameByRoute, 'test');
            service.retrieveConeSearchConfig = jest.fn().mockImplementation(() => of(coneSearchConfig));
            actions = hot('a', { a: coneSearchConfigActions.loadConeSearchConfig() });
            const expected = cold('b', { b: coneSearchConfigActions.loadConeSearchConfigSuccess({ coneSearchConfig }) });
            expect(effects.loadConeSearchsConfig$).toBeObservable(expected);
            expect(service.retrieveConeSearchConfig).toHaveBeenCalledWith('test');
        });

        it('should dispatch the loadAliasConfigFail action on HTTP failure', () => {
            service.retrieveConeSearchConfig = jest.fn().mockImplementation(() => throwError(() => ''));
            actions = hot('a', { a: coneSearchConfigActions.loadConeSearchConfig() });
            const expected = cold('b', { b: coneSearchConfigActions.loadConeSearchConfigFail() });
            expect(effects.loadConeSearchsConfig$).toBeObservable(expected);
        });
    });
    describe('addConeSearchConfig$ effect', () => {
        it('should dispatch the addConeSearchConfigSuccess action on success', () => {
            mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(datasetSelector.selectDatasetNameByRoute, 'test');
            service.addConeSearchConfig = jest.fn().mockImplementation(() => of(coneSearchConfig));
            actions = hot('a', { a: coneSearchConfigActions.addConeSearchConfig({ coneSearchConfig }) });
            const expected = cold('b', { b: coneSearchConfigActions.addConeSearchConfigSuccess({ coneSearchConfig }) });
            expect(effects.addConeSearchConfig$).toBeObservable(expected);
            expect(service.addConeSearchConfig).toHaveBeenCalledWith('test', coneSearchConfig);
        });

        it('should dispatch the addConeSearchConfigFail action on HTTP failure', () => {
            service.addConeSearchConfig = jest.fn().mockImplementation(() => throwError(() => ''));
            actions = hot('a', { a: coneSearchConfigActions.addConeSearchConfig({ coneSearchConfig }) });
            const expected = cold('b', { b: coneSearchConfigActions.addConeSearchConfigFail() });
            expect(effects.addConeSearchConfig$).toBeObservable(expected);
        });
    });
    describe('addConeSearchConfigSuccess$ effect', () => {
        it('should  toast success event', () => {
            actions = hot('a', { a: coneSearchConfigActions.addConeSearchConfigSuccess({ coneSearchConfig }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('a', { a: coneSearchConfigActions.addConeSearchConfigSuccess({ coneSearchConfig }) });
            expect(effects.addConeSearchConfigSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Cone search config successfully added', 'The new cone search config was added into the database');
        });

    });
    describe('addConeSearchConfigFail$ effect', () => {
        it('should  toast error event', () => {
            actions = hot('a', { a: coneSearchConfigActions.addConeSearchConfigFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('a', { a: coneSearchConfigActions.addConeSearchConfigFail() });
            expect(effects.addConeSearchConfigFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add cone search config', 'The new cone search config could not be added into the database');
        });

    });
    describe('editConeSearchConfig$ effect', () => {
        it('should dispatch the editConeSearchConfigSuccess action on success', () => {
            mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(datasetSelector.selectDatasetNameByRoute, 'test');
            service.editConeSearchConfig = jest.fn().mockImplementation(() => of(coneSearchConfig));
            actions = hot('a', { a: coneSearchConfigActions.editConeSearchConfig({ coneSearchConfig }) });
            const expected = cold('b', { b: coneSearchConfigActions.editConeSearchConfigSuccess({ coneSearchConfig }) });
            expect(effects.editConeSearchConfig$).toBeObservable(expected);
            expect(service.editConeSearchConfig).toHaveBeenCalledWith('test', coneSearchConfig);
        });
        it('should dispatch the editConeSearchConfigFail action on HTTP failure', () => {
            service.editConeSearchConfig = jest.fn().mockImplementation(() => throwError(() => ''));
            actions = hot('a', { a: coneSearchConfigActions.editConeSearchConfig({ coneSearchConfig }) });
            const expected = cold('b', { b: coneSearchConfigActions.editConeSearchConfigFail() });
            expect(effects.editConeSearchConfig$).toBeObservable(expected);
        });
    });
    describe('editConeSearchConfigSuccess$ effect', () => {
        it('should  toast success event', () => {
            actions = hot('a', { a: coneSearchConfigActions.editConeSearchConfigSuccess({ coneSearchConfig }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('a', { a: coneSearchConfigActions.editConeSearchConfigSuccess({ coneSearchConfig }) });
            expect(effects.editConeSearchConfigSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Cone search configuration successfully edited', 'The existing cone search configuration has been edited into the database');
        });

    });
    describe('editConeSearchConfigFail$ effect', () => {
        it('should  toast error event', () => {
            actions = hot('a', { a: coneSearchConfigActions.editConeSearchConfigFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('a', { a: coneSearchConfigActions.editConeSearchConfigFail() });
            expect(effects.editConeSearchConfigFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit cone search configuration', 'The existing cone search configuration could not be edited into the database');
        });

    });
});