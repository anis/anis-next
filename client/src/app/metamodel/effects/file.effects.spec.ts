/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import * as datasetSelector from '../selectors/dataset.selector';
import * as fileActions from '../actions/file.actions';
import { FileEffects } from './file.effects';
import { FileService } from '../services/file.service';
import { File } from '../models';



describe('[Metamodel][Effects] FileEffects', () => {
    let actions = new Observable();
    let effects: FileEffects;
    let metadata: EffectsMetadata<FileEffects>;
    let service: FileService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockDatasetSelectorDatasetNameByRoute;
    let router: Router;
    let file: File;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                FileEffects,
                { provide: FileService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(FileEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(FileService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute, 'test'
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadFiles$ effect', () => {
        it('should dispatch the loadFileListSuccess action on success', () => {
            actions = hot('a', { a: fileActions.loadFileList() });
            let files: File[] = [{ ...file, id: 1 }];
            service.retrieveFileList = jest.fn(() => of(files));
            let expected = cold('b', { b: fileActions.loadFileListSuccess({ files }) });
            expect(effects.loadFiles$).toBeObservable(expected);
            expect(service.retrieveFileList).toHaveBeenCalledWith('test');

        });
        it('should dispatch the loadFileListFail action on HTTP failure', () => {
            actions = hot('a', { a: fileActions.loadFileList() });
            service.retrieveFileList = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: fileActions.loadFileListFail() });
            expect(effects.loadFiles$).toBeObservable(expected);
            expect(service.retrieveFileList).toHaveBeenCalledWith('test');

        });
    });
    describe('addFile$ effect', () => {
        it('should dispatch the addFileSuccess action on success', () => {
            actions = hot('a', { a: fileActions.addFile({ file: { ...file, id: 1 } }) });
            service.addFile = jest.fn(() => of({ ...file, id: 1 }));
            let expected = cold('b', { b: fileActions.addFileSuccess({ file: { ...file, id: 1 } }) });
            expect(effects.addFile$).toBeObservable(expected);
            expect(service.addFile).toHaveBeenCalledWith('test', { ...file, id: 1 });

        });
        it('should dispatch the addFileFail action on HTTP failure', () => {
            actions = hot('a', { a: fileActions.addFile({ file: { ...file, id: 1 } }) });
            service.addFile = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: fileActions.addFileFail() });
            expect(effects.addFile$).toBeObservable(expected);
            expect(service.addFile).toHaveBeenCalledWith('test', { ...file, id: 1 });

        });
    });
    describe('addFileSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: fileActions.addFileSuccess({ file: { ...file, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: fileActions.addFileSuccess({ file: { ...file, id: 1 } }) });
            expect(effects.addFileSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('File successfully added', 'The new file was added into the database');
        });
    });
    describe('addFileFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: fileActions.addFileFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: fileActions.addFileFail() });
            expect(effects.addFileFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add file', 'The new file could not be added into the database');
        });
    });
    describe('editFile$ effect', () => {
        it('should dispatch the editFileSuccess action on success', () => {
            actions = hot('a', { a: fileActions.editFile({ file: { ...file, id: 1 } }) });
            service.editFile = jest.fn(() => of({ ...file, id: 1 }));
            let expected = cold('b', { b: fileActions.editFileSuccess({ file: { ...file, id: 1 } }) });
            expect(effects.editFile$).toBeObservable(expected);
            expect(service.editFile).toHaveBeenCalledWith({ ...file, id: 1 });

        });
        it('should dispatch the editFileFail action on HTTP failure', () => {
            actions = hot('a', { a: fileActions.editFile({ file: { ...file, id: 1 } }) });
            service.editFile = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: fileActions.editFileFail() });
            expect(effects.editFile$).toBeObservable(expected);
            expect(service.editFile).toHaveBeenCalledWith({ ...file, id: 1 });

        });
    });
    describe('editFileSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: fileActions.editFileSuccess({ file: { ...file, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: fileActions.editFileSuccess({ file: { ...file, id: 1 } }) });
            expect(effects.editFileSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('File successfully edited', 'The existing file has been edited into the database');
        });
    });
    describe('editFileFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: fileActions.editFileFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: fileActions.editFileFail() });
            expect(effects.editFileFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit file', 'The existing file could not be edited into the database');
        });
    });
    describe('deleteFile$ effect', () => {
        it('should dispatch the deleteFileSuccess action on success', () => {
            actions = hot('a', { a: fileActions.deleteFile({ file: { ...file, id: 1 } }) });
            service.deleteFile = jest.fn(() => of({ ...file, id: 1 }));
            let expected = cold('b', { b: fileActions.deleteFileSuccess({ file: { ...file, id: 1 } }) });
            expect(effects.deleteFile$).toBeObservable(expected);
            expect(service.deleteFile).toHaveBeenCalledWith(1);

        });
        it('should dispatch the deleteFileFail action on HTTP failure', () => {
            actions = hot('a', { a: fileActions.deleteFile({ file: { ...file, id: 1 } }) });
            service.deleteFile = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: fileActions.deleteFileFail() });
            expect(effects.deleteFile$).toBeObservable(expected);
            expect(service.deleteFile).toHaveBeenCalledWith(1);

        });
    });
    describe('deleteFileSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: fileActions.deleteFileSuccess({ file: { ...file, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: fileActions.deleteFileSuccess({ file: { ...file, id: 1 } }) });
            expect(effects.deleteFileSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('File successfully deleted', 'The existing file has been deleted');
        });
    });
    describe('deleteFileFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: fileActions.deleteFileFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: fileActions.deleteFileFail() });
            expect(effects.deleteFileFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to delete file', 'The existing file could not be deleted from the database');
        });
    });
});
