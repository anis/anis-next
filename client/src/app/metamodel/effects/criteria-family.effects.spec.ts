/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';

import { ToastrService } from 'ngx-toastr';
import { CriteriaFamilyEffects } from './criteria-family.effects';
import { CriteriaFamilyService } from '../services/criteria-family.service';
import * as criteriaFamilyActions from '../actions/criteria-family.actions';
import { CRITERIA_FAMILY, CRITERIA_FAMILY_LIST } from '../../../test-data';
import * as datasetSelector from '../selectors/dataset.selector';

describe('[Metamodel][Effects] CriteriaFamilyEffects', () => {
    let actions = new Observable();
    let effects: CriteriaFamilyEffects;
    let metadata: EffectsMetadata<CriteriaFamilyEffects>;
    let service: CriteriaFamilyService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: { } };
    let mockDatasetSelectorSelectDatasetNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                CriteriaFamilyEffects,
                { provide: CriteriaFamilyService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }},
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(CriteriaFamilyEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(CriteriaFamilyService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute,''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadCriteriaFamilies$ effect', () => {
        it('should dispatch the loadCriteriaFamilyListSuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = criteriaFamilyActions.loadCriteriaFamilyList();
            const outcome = criteriaFamilyActions.loadCriteriaFamilyListSuccess({ criteriaFamilies: CRITERIA_FAMILY_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CRITERIA_FAMILY_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveCriteriaFamilyList = jest.fn(() => response);

            expect(effects.loadCriteriaFamilies$).toBeObservable(expected);
            expect(service.retrieveCriteriaFamilyList).toHaveBeenCalledWith('myDataset');
        });

        it('should dispatch the loadCriteriaFamilyListFail action on HTTP failure', () => {
            const action = criteriaFamilyActions.loadCriteriaFamilyList();
            const error = new Error();
            const outcome = criteriaFamilyActions.loadCriteriaFamilyListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveCriteriaFamilyList = jest.fn(() => response);

            expect(effects.loadCriteriaFamilies$).toBeObservable(expected);
        });
    });

    describe('addCriteriaFamilies$ effect', () => {
        it('should dispatch the addCriteriaFamilySuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = criteriaFamilyActions.addCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
            const outcome = criteriaFamilyActions.addCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CRITERIA_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.addCriteriaFamily = jest.fn(() => response);

            expect(effects.addCriteriaFamily$).toBeObservable(expected);
            expect(service.addCriteriaFamily).toHaveBeenCalledWith('myDataset', CRITERIA_FAMILY);
        });

        it('should dispatch the addCriteriaFamilyFail action on HTTP failure', () => {
            const action = criteriaFamilyActions.addCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
            const error = new Error();
            const outcome = criteriaFamilyActions.addCriteriaFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.addCriteriaFamily = jest.fn(() => response);

            expect(effects.addCriteriaFamily$).toBeObservable(expected);
        });
    });

    describe('addCriteriaFamilySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addCriteriaFamilySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = criteriaFamilyActions.addCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addCriteriaFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Criteria family successfully added',
                'The new criteria family was added into the database'
            );
        });
    });

    describe('addCriteriaFamilyFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addCriteriaFamilyFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = criteriaFamilyActions.addCriteriaFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addCriteriaFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add criteria family',
                'The new criteria family could not be added into the database'
            );
        });
    });

    describe('editCriteriaFamily$ effect', () => {
        it('should dispatch the editCriteriaFamilySuccess action on success', () => {
            const action = criteriaFamilyActions.editCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
            const outcome = criteriaFamilyActions.editCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CRITERIA_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.editCriteriaFamily = jest.fn(() => response);

            expect(effects.editCriteriaFamily$).toBeObservable(expected);
        });

        it('should dispatch the editCriteriaFamilyFail action on HTTP failure', () => {
            const action = criteriaFamilyActions.editCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
            const error = new Error();
            const outcome = criteriaFamilyActions.editCriteriaFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.editCriteriaFamily = jest.fn(() => response);

            expect(effects.editCriteriaFamily$).toBeObservable(expected);
        });
    });

    describe('editCriteriaFamilySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editCriteriaFamilySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = criteriaFamilyActions.editCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editCriteriaFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Criteria family successfully edited',
                'The existing criteria family has been edited into the database'
            );
        });
    });

    describe('editCriteriaFamilyFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editCriteriaFamilyFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = criteriaFamilyActions.editCriteriaFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editCriteriaFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit criteria family',
                'The existing criteria family could not be edited into the database'
            );
        });
    });

    describe('deleteCriteriaFamily$ effect', () => {
        it('should dispatch the deleteSurveySuccess action on success', () => {
            const action = criteriaFamilyActions.deleteCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
            const outcome = criteriaFamilyActions.deleteCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CRITERIA_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.deleteCriteriaFamily = jest.fn(() => response);

            expect(effects.deleteCriteriaFamily$).toBeObservable(expected);
        });

        it('should dispatch the deleteCriteriaFamilyFail action on HTTP failure', () => {
            const action = criteriaFamilyActions.deleteCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
            const error = new Error();
            const outcome = criteriaFamilyActions.deleteCriteriaFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.deleteCriteriaFamily = jest.fn(() => response);

            expect(effects.deleteCriteriaFamily$).toBeObservable(expected);
        });
    });

    describe('deleteCriteriaFamilySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteCriteriaFamilySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = criteriaFamilyActions.deleteCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteCriteriaFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Criteria family successfully deleted',
                'The existing criteria family has been deleted'
            );
        });
    });

    describe('deleteCriteriaFamilyFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteCriteriaFamilyFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = criteriaFamilyActions.deleteCriteriaFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteCriteriaFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete criteria family',
                'The existing criteria family could not be deleted from the database'
            );
        });
    });
});
