/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import { AliasConfigEffects } from './alias-config.effects';
import { AliasConfigService } from '../services/alias-config.service';
import * as datasetSelector from '../selectors/dataset.selector';
import { AliasConfig } from '../models';
import * as aliasConfigActions from '../actions/alias-config.actions';

describe('[Metamodel][Effects] AliasConfigEffects', () => {
    let actions = new Observable();
    let effects: AliasConfigEffects;
    let metadata: EffectsMetadata<AliasConfigEffects>;
    let service: AliasConfigService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let router: Router;
    let mockDatasetSelectorDatasetNameByRoute;
    let aliasConfig: AliasConfig;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AliasConfigEffects,
                { provide: AliasConfigService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(AliasConfigEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(AliasConfigService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        aliasConfig = {
            column_alias: 'test',
            column_alias_long: 'test',
            column_name: 'test',
            table_alias: 'test'
        }
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadAliassConfig$ effect', () => {
        it('should dispatch the loadAliasConfigSuccess action on success', () => {
            mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(datasetSelector.selectDatasetNameByRoute, 'test');
            service.retrieveAliasConfig = jest.fn().mockImplementation(() => of(aliasConfig));
            actions = hot('a', { a: aliasConfigActions.loadAliasConfig() });
            const expected = cold('b', { b: aliasConfigActions.loadAliasConfigSuccess({ aliasConfig }) });
            expect(effects.loadAliassConfig$).toBeObservable(expected);
            expect(service.retrieveAliasConfig).toHaveBeenCalledWith('test');
        });

        it('should dispatch the loadAliasConfigFail action on HTTP failure', () => {
            service.retrieveAliasConfig = jest.fn().mockImplementation(() => throwError(() => ''));
            actions = hot('a', { a: aliasConfigActions.loadAliasConfig() });
            const expected = cold('b', { b: aliasConfigActions.loadAliasConfigFail() });
            expect(effects.loadAliassConfig$).toBeObservable(expected);
        });
    });
    describe('addAliasConfig$ effect', () => {
        it('should dispatch the addAliasConfigSuccess action on success', () => {
            mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(datasetSelector.selectDatasetNameByRoute, 'test');
            service.addAliasConfig = jest.fn().mockImplementation(() => of(aliasConfig));
            actions = hot('a', { a: aliasConfigActions.addAliasConfig({ aliasConfig: aliasConfig }) });
            const expected = cold('b', { b: aliasConfigActions.addAliasConfigSuccess({ aliasConfig }) });
            expect(effects.addAliasConfig$).toBeObservable(expected);
            expect(service.addAliasConfig).toHaveBeenCalledWith('test', aliasConfig);
        });
        it('should dispatch the addAliasConfigFail action on HTTP failure', () => {
            service.addAliasConfig = jest.fn().mockImplementation(() => throwError(() => ''));
            actions = hot('a', { a: aliasConfigActions.addAliasConfig({ aliasConfig: aliasConfig }) });
            const expected = cold('b', { b: aliasConfigActions.addAliasConfigFail() });
            expect(effects.addAliasConfig$).toBeObservable(expected);
        });
    });
    describe('addAliasConfigSuccess$ effect', () => {
        it('should toast success event', () => {
            actions = hot('a', { a: aliasConfigActions.addAliasConfigSuccess({ aliasConfig }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('a', { a: aliasConfigActions.addAliasConfigSuccess({ aliasConfig }) });
            expect(effects.addAliasConfigSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Cone search config successfully added', 'The new alias config was added into the database');
        });

    });
    describe('addAliasConfigFail$ effect', () => {
        it('should  toast error event', () => {
            actions = hot('a', { a: aliasConfigActions.addAliasConfigFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('a', { a: aliasConfigActions.addAliasConfigFail() });
            expect(effects.addAliasConfigFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add alias config', 'The new alias config could not be added into the database');
        });

    });
    describe('editAliasConfig$ effect', () => {
        it('should dispatch the editAliasConfigSuccess action on success', () => {
            mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(datasetSelector.selectDatasetNameByRoute, 'test');
            service.editAliasConfig = jest.fn().mockImplementation(() => of(aliasConfig));
            actions = hot('a', { a: aliasConfigActions.editAliasConfig({ aliasConfig: aliasConfig }) });
            const expected = cold('b', { b: aliasConfigActions.editAliasConfigSuccess({ aliasConfig }) });
            expect(effects.editAliasConfig$).toBeObservable(expected);
            expect(service.editAliasConfig).toHaveBeenCalledWith('test', aliasConfig);
        });
        it('should dispatch the editAliasConfigFail action on HTTP failure', () => {
            service.editAliasConfig = jest.fn().mockImplementation(() => throwError(() => ''));
            actions = hot('a', { a: aliasConfigActions.editAliasConfig({ aliasConfig: aliasConfig }) });
            const expected = cold('b', { b: aliasConfigActions.editAliasConfigFail() });
            expect(effects.editAliasConfig$).toBeObservable(expected);
        });
    });
    describe('editAliasConfigSuccess$ effect', () => {
        it('should toast success event', () => {
            actions = hot('a', { a: aliasConfigActions.editAliasConfigSuccess({ aliasConfig }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('a', { a: aliasConfigActions.editAliasConfigSuccess({ aliasConfig }) });
            expect(effects.editAliasConfigSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Cone search configuration successfully edited', 'The existing alias configuration has been edited into the database');
        });

    });
    describe('editAliasConfigFail$ effect', () => {
        it('should  toast error event', () => {
            actions = hot('a', { a: aliasConfigActions.editAliasConfigFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('a', { a: aliasConfigActions.editAliasConfigFail() });
            expect(effects.editAliasConfigFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit alias configuration', 'The existing alias configuration could not be edited into the database');
        });

    });
});