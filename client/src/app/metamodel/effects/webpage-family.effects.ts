/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as webpageFamilyActions from '../actions/webpage-family.actions';
import { WebpageFamilyService } from '../services/webpage-family.service';
import * as instanceSelector from '../selectors/instance.selector';

/**
 * @class
 * @classdesc Webpage family effects.
 */
@Injectable()
export class WebpageFamilyEffects {
    /**
     * Calls action to retrieve webpage family list.
     */
    loadWebpageFamilies$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.loadWebpageFamilyList),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([, instanceName]) => this.webpageFamilyService.retrieveWebpageFamilyList(instanceName)
                .pipe(
                    map(webpageFamilies => webpageFamilyActions.loadWebpageFamilyListSuccess({ webpageFamilies })),
                    catchError(() => of(webpageFamilyActions.loadWebpageFamilyListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a webpage family.
     */
    addWebpageFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.addWebpageFamily),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.webpageFamilyService.addWebpageFamily(instanceName, action.webpageFamily)
                .pipe(
                    map(webpageFamily => webpageFamilyActions.addWebpageFamilySuccess({ webpageFamily })),
                    catchError(() => of(webpageFamilyActions.addWebpageFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays add webpage family success notification.
     */
    addWebpageFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.addWebpageFamilySuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.toastr.success('Webpage family successfully added', 'The new webpage family was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add webpage family fail notification.
     */
    addWebpageFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.addWebpageFamilyFail),
            tap(() => this.toastr.error('Failure to add webpage family', 'The new webpage family could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a webpage family.
     */
    editWebpageFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.editWebpageFamily),
            mergeMap(action => this.webpageFamilyService.editWebpageFamily(action.webpageFamily)
                .pipe(
                    map(webpageFamily => webpageFamilyActions.editWebpageFamilySuccess({ webpageFamily })),
                    catchError(() => of(webpageFamilyActions.editWebpageFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays edit webpage family success notification.
     */
    editWebpageFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.editWebpageFamilySuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([action, instanceName]) => {
                this.toastr.success('Webpage family successfully edited', 'The existing webpage family has been edited into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays edit webpage family error notification.
     */
    editWebpageFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.editWebpageFamilyFail),
            tap(() => this.toastr.error('Failure to edit webpage family', 'The existing webpage family could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a webpage family.
     */
    deleteWebpageFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.deleteWebpageFamily),
            mergeMap(action => this.webpageFamilyService.deleteWebpageFamily(action.webpageFamily.id)
                .pipe(
                    map(() => webpageFamilyActions.deleteWebpageFamilySuccess({ webpageFamily: action.webpageFamily })),
                    catchError(() => of(webpageFamilyActions.deleteWebpageFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays delete webpage family success notification.
     */
    deleteWebpageFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.deleteWebpageFamilySuccess),
            tap(() => this.toastr.success('Webpage family successfully deleted', 'The existing webpage family has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete webpage family error notification.
     */
    deleteWebpageFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageFamilyActions.deleteWebpageFamilyFail),
            tap(() => this.toastr.error('Failure to delete webpage family', 'The existing webpage family could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private webpageFamilyService: WebpageFamilyService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
