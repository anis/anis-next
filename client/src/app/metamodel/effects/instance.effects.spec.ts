/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { InstanceEffects } from './instance.effects';
import { InstanceService } from '../services/instance.service';
import * as instanceActions from '../actions/instance.actions';
import { INSTANCE, INSTANCE_LIST } from '../../../test-data';

describe('[Metamodel][Effects] InstanceEffects', () => {
    let actions = new Observable();
    let effects: InstanceEffects;
    let metadata: EffectsMetadata<InstanceEffects>;
    let service: InstanceService;
    let toastr: ToastrService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                InstanceEffects,
                { provide: InstanceService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }},
                provideMockActions(() => actions)
            ]
        }).compileComponents();
        effects = TestBed.inject(InstanceEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(InstanceService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadInstances$ effect', () => {
        it('should dispatch the loadInstanceListSuccess action on success', () => {
            const action = instanceActions.loadInstanceList();
            const outcome = instanceActions.loadInstanceListSuccess({ instances: INSTANCE_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: INSTANCE_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveInstanceList = jest.fn(() => response);

            expect(effects.loadInstances$).toBeObservable(expected);
        });

        it('should dispatch the loadSurveyListFail action on HTTP failure', () => {
            const action = instanceActions.loadInstanceList();
            const error = new Error();
            const outcome = instanceActions.loadInstanceListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveInstanceList = jest.fn(() => response);

            expect(effects.loadInstances$).toBeObservable(expected);
        });
    });

    describe('addInstance$ effect', () => {
        it('should dispatch the addSurveySuccess action on success', () => {
            const action = instanceActions.addInstance({ instance: INSTANCE });
            const outcome = instanceActions.addInstanceSuccess({ instance: INSTANCE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: INSTANCE });
            const expected = cold('--b', { b: outcome });
            service.addInstance = jest.fn(() => response);

            expect(effects.addInstance$).toBeObservable(expected);
        });

        it('should dispatch the addSurveyFail action on HTTP failure', () => {
            const action = instanceActions.addInstance({ instance: INSTANCE });
            const error = new Error();
            const outcome = instanceActions.addInstanceFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.addInstance = jest.fn(() => response);

            expect(effects.addInstance$).toBeObservable(expected);
        });
    });

    describe('addInstanceSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addInstanceSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const routerSpy = jest.spyOn(router, 'navigate');
            const action = instanceActions.addInstanceSuccess({ instance: INSTANCE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addInstanceSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Instance successfully added',
                'The new instance was added into the database'
            );
            expect(routerSpy).toHaveBeenCalledTimes(1);
            expect(routerSpy).toHaveBeenCalledWith(['/admin/instance/instance-list']);
        });
    });

    describe('addInstanceFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addInstanceFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = instanceActions.addInstanceFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addInstanceFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add instance',
                'The new instance could not be added into the database'
            );
        });
    });

    describe('editInstance$ effect', () => {
        it('should dispatch the editInstanceSuccess action on success', () => {
            const action = instanceActions.editInstance({ instance: INSTANCE });
            const outcome = instanceActions.editInstanceSuccess({ instance: INSTANCE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: INSTANCE });
            const expected = cold('--b', { b: outcome });
            service.editInstance = jest.fn(() => response);

            expect(effects.editInstance$).toBeObservable(expected);
        });

        it('should dispatch the editSurveyFail action on HTTP failure', () => {
            const action = instanceActions.editInstance({ instance: INSTANCE });
            const error = new Error();
            const outcome = instanceActions.editInstanceFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.editInstance = jest.fn(() => response);

            expect(effects.editInstance$).toBeObservable(expected);
        });
    });

    describe('editInstanceSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editInstanceSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const routerSpy = jest.spyOn(router, 'navigate');
            const action = instanceActions.editInstanceSuccess({ instance: INSTANCE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editInstanceSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Instance successfully edited',
                'The existing instance has been edited into the database'
            );
        });
    });

    describe('editInstanceFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editInstanceFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = instanceActions.editInstanceFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editInstanceFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit instance',
                'The existing instance could not be edited into the database'
            );
        });
    });

    describe('deleteInstance$ effect', () => {
        it('should dispatch the deleteInstanceSuccess action on success', () => {
            const action = instanceActions.deleteInstance({ instance: INSTANCE });
            const outcome = instanceActions.deleteInstanceSuccess({ instance: INSTANCE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: INSTANCE });
            const expected = cold('--b', { b: outcome });
            service.deleteInstance = jest.fn(() => response);

            expect(effects.deleteInstance$).toBeObservable(expected);
        });

        it('should dispatch the deleteSurveyFail action on HTTP failure', () => {
            const action = instanceActions.deleteInstance({ instance: INSTANCE });
            const error = new Error();
            const outcome = instanceActions.deleteInstanceFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.deleteInstance = jest.fn(() => response);

            expect(effects.deleteInstance$).toBeObservable(expected);
        });
    });

    describe('deleteInstanceSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteInstanceSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = instanceActions.deleteInstanceSuccess({ instance: INSTANCE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteInstanceSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Instance successfully deleted',
                'The existing instance has been deleted'
            );
        });
    });

    describe('deleteInstanceFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteInstanceFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = instanceActions.deleteInstanceFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteInstanceFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete instance',
                'The existing instance could not be deleted from the database'
            );
        });
    });
});
