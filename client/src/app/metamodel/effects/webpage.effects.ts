/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as webpageActions from '../actions/webpage.actions';
import { WebpageService } from '../services/webpage.service';
import * as instanceSelector from '../selectors/instance.selector';

/**
 * @class
 * @classdesc Webpage effects.
 */
@Injectable()
export class WebpageEffects {
    /**
     * Calls action to retrieve webpage list.
     */
    loadWebpages$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageActions.loadWebpageList),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([, instanceName]) => this.webpageService.retrieveWebpageList(instanceName)
                .pipe(
                    map(webpages => webpageActions.loadWebpageListSuccess({ webpages })),
                    catchError(() => of(webpageActions.loadWebpageListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a webpage.
     */
    addWebpage$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageActions.addWebpage),
            mergeMap(action => this.webpageService.addWebpage(action.webpage)
                .pipe(
                    map(webpage => webpageActions.addWebpageSuccess({ webpage })),
                    catchError(() => of(webpageActions.addWebpageFail()))
                )
            )
        )
    );

    /**
     * Displays add webpage success notification.
     */
    addWebpageSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageActions.addWebpageSuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.router.navigateByUrl(`/admin/instance/configure-instance/${instanceName}/webpage`);
                this.toastr.success('Webpage successfully added', 'The new webpage was added into the database');
            })
        ), { dispatch: false }
    );

    /**
     * Displays add webpage fail notification.
     */
    addWebpageFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageActions.addWebpageFail),
            tap(() => this.toastr.error('Failure to add webpage', 'The new webpage could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a webpage.
     */
    editWebpage$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageActions.editWebpage),
            mergeMap(action => this.webpageService.editWebpage(action.webpage)
                .pipe(
                    map(webpage => webpageActions.editWebpageSuccess({ webpage })),
                    catchError(() => of(webpageActions.editWebpageFail()))
                )
            )
        )
    );

    /**
     * Displays edit webpage success notification.
     */
    editWebpageSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageActions.editWebpageSuccess),
            tap(() => this.toastr.success('Webpage successfully edited', 'The existing webpage has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit webpage error notification.
     */
    editWebpageFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageActions.editWebpageFail),
            tap(() => this.toastr.error('Failure to edit webpage', 'The existing webpage could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a webpage.
     */
    deleteWebpage$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(webpageActions.deleteWebpage),
            mergeMap(action => this.webpageService.deleteWebpage(action.webpage.id)
                .pipe(
                    map(() => webpageActions.deleteWebpageSuccess({ webpage: action.webpage })),
                    catchError(() => of(webpageActions.deleteWebpageFail()))
                )
            )
        )
    );

    /**
     * Displays delete webpage success notification.
     */
    deleteWebpageSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageActions.deleteWebpageSuccess),
            tap(() => this.toastr.success('Webpage successfully deleted', 'The existing webpage has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete webpage error notification.
     */
    deleteWebpageFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(webpageActions.deleteWebpageFail),
            tap(() => this.toastr.error('Failure to delete webpage', 'The existing webpage could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private webpageService: WebpageService,
        private router: Router,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
