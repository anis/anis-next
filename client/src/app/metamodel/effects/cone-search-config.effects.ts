/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as coneSearchConfigActions from '../actions/cone-search-config.actions';
import { ConeSearchConfigService } from '../services/cone-search-config.service';
import * as datasetSelector from '../selectors/dataset.selector';

/**
 * @class
 * @classdesc ConeSearch effects.
 */
@Injectable()
export class ConeSearchConfigEffects {
    /**
     * Calls action to retrieve cone-search configuration
     */
    loadConeSearchsConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(coneSearchConfigActions.loadConeSearchConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.coneSearchConfigService.retrieveConeSearchConfig(datasetName)
                .pipe(
                    map(coneSearchConfig => coneSearchConfigActions.loadConeSearchConfigSuccess({ coneSearchConfig })),
                    catchError(() => of(coneSearchConfigActions.loadConeSearchConfigFail()))
                )
            )
        )
    );

    /**
     * Calls action to add an coneSearch.
     */
    addConeSearchConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(coneSearchConfigActions.addConeSearchConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.coneSearchConfigService.addConeSearchConfig(datasetName, action.coneSearchConfig)
                .pipe(
                    map(coneSearchConfig => coneSearchConfigActions.addConeSearchConfigSuccess({ coneSearchConfig })),
                    catchError(() => of(coneSearchConfigActions.addConeSearchConfigFail()))
                )
            )
        )
    );

    /**
     * Displays add cone search configuration success notification.
     */
    addConeSearchConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(coneSearchConfigActions.addConeSearchConfigSuccess),
            tap(() => this.toastr.success('Cone search config successfully added', 'The new cone search config was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add cone search configuration error notification.
     */
    addConeSearchConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(coneSearchConfigActions.addConeSearchConfigFail),
            tap(() => this.toastr.error('Failure to add cone search config', 'The new cone search config could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an cone search configuration
     */
    editConeSearchConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(coneSearchConfigActions.editConeSearchConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.coneSearchConfigService.editConeSearchConfig(datasetName, action.coneSearchConfig)
                .pipe(
                    map(coneSearchConfig => coneSearchConfigActions.editConeSearchConfigSuccess({ coneSearchConfig })),
                    catchError(() => of(coneSearchConfigActions.editConeSearchConfigFail()))
                )
            )
        )
    );

    /**
     * Displays edit cone search configuration success notification.
     */
    editConeSearchConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(coneSearchConfigActions.editConeSearchConfigSuccess),
            tap(() => this.toastr.success('Cone search configuration successfully edited', 'The existing cone search configuration has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit cone search configuration error notification.
     */
    editConeSearchConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(coneSearchConfigActions.editConeSearchConfigFail),
            tap(() => this.toastr.error('Failure to edit cone search configuration', 'The existing cone search configuration could not be edited into the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private coneSearchConfigService: ConeSearchConfigService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
