/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as fileActions from '../actions/file.actions';
import { FileService } from '../services/file.service';
import * as datasetSelector from '../selectors/dataset.selector';

/**
 * @class
 * @classdesc File effects.
 */
@Injectable()
export class FileEffects {
    /**
     * Calls action to retrieve file list.
     */
    loadFiles$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(fileActions.loadFileList),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.fileService.retrieveFileList(datasetName)
                .pipe(
                    map(files => fileActions.loadFileListSuccess({ files })),
                    catchError(() => of(fileActions.loadFileListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add an file.
     */
    addFile$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(fileActions.addFile),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.fileService.addFile(datasetName, action.file)
                .pipe(
                    map(file => fileActions.addFileSuccess({ file })),
                    catchError(() => of(fileActions.addFileFail()))
                )
            )
        )
    );

    /**
     * Displays add file success notification.
     */
    addFileSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.addFileSuccess),
            tap(() => this.toastr.success('File successfully added', 'The new file was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add file error notification.
     */
    addFileFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.addFileFail),
            tap(() => this.toastr.error('Failure to add file', 'The new file could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an file.
     */
    editFile$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(fileActions.editFile),
            mergeMap(action => this.fileService.editFile(action.file)
                .pipe(
                    map(file => fileActions.editFileSuccess({ file })),
                    catchError(() => of(fileActions.editFileFail()))
                )
            )
        )
    );

    /**
     * Displays edit file success notification.
     */
    editFileSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.editFileSuccess),
            tap(() => this.toastr.success('File successfully edited', 'The existing file has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit file error notification.
     */
    editFileFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.editFileFail),
            tap(() => this.toastr.error('Failure to edit file', 'The existing file could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove an file.
     */
    deleteFile$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(fileActions.deleteFile),
            mergeMap(action => this.fileService.deleteFile(action.file.id)
                .pipe(
                    map(() => fileActions.deleteFileSuccess({ file: action.file })),
                    catchError(() => of(fileActions.deleteFileFail()))
                )
            )
        )
    );

    /**
     * Displays delete file success notification.
     */
    deleteFileSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.deleteFileSuccess),
            tap(() => this.toastr.success('File successfully deleted', 'The existing file has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete file error notification.
     */
    deleteFileFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.deleteFileFail),
            tap(() => this.toastr.error('Failure to delete file', 'The existing file could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private fileService: FileService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
