/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import * as instanceSelector from '../selectors/instance.selector';
import { WebpageEffects } from './webpage.effects';
import { WebpageService } from '../services/webpage.service';
import { Webpage } from '../models';
import * as webpageActions from '../actions/webpage.actions';

describe('[Metamodel][Effects] WebpageEffects', () => {
    let actions = new Observable();
    let effects: WebpageEffects;
    let metadata: EffectsMetadata<WebpageEffects>;
    let service: WebpageService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let router: Router;
    let webpage: Webpage;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                WebpageEffects,
                { provide: WebpageService, useValue: {} },
                {
                    provide: Router, useValue: {
                        navigate: jest.fn(),
                        navigateByUrl: jest.fn()
                    }
                },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(WebpageEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(WebpageService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute, 'test'
        );
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadWebpages$ effect', () => {
        it('should dispatch the loadWebpageListSuccess action on success', () => {
            actions = hot('a', { a: webpageActions.loadWebpageList() });
            service.retrieveWebpageList = jest.fn(() => of([{ ...webpage, id: 1 }]));
            let expected = cold('b', { b: webpageActions.loadWebpageListSuccess({ webpages: [{ ...webpage, id: 1 }] }) });
            expect(effects.loadWebpages$).toBeObservable(expected);
            expect(service.retrieveWebpageList).toHaveBeenCalledTimes(1);

        });
        it('should dispatch the loadWebpageListFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageActions.loadWebpageList() });
            service.retrieveWebpageList = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageActions.loadWebpageListFail() });
            expect(effects.loadWebpages$).toBeObservable(expected);

        });
    });
    describe('addWebpage$ effect', () => {
        it('should dispatch the addWebpageSuccess action on success', () => {
            actions = hot('a', { a: webpageActions.addWebpage({ webpage: { ...webpage, id: 1 } }) });
            service.addWebpage = jest.fn(() => of({ ...webpage, id: 1 }));
            let expected = cold('b', { b: webpageActions.addWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            expect(effects.addWebpage$).toBeObservable(expected);
            expect(service.addWebpage).toHaveBeenCalledWith({ ...webpage, id: 1 });

        });
        it('should dispatch the addWebpageFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageActions.addWebpage({ webpage: { ...webpage, id: 1 } }) });
            service.addWebpage = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageActions.addWebpageFail() });
            expect(effects.addWebpage$).toBeObservable(expected);
            expect(service.addWebpage).toHaveBeenCalledWith({ ...webpage, id: 1 });

        });

    });
    describe('addWebpageSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: webpageActions.addWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: [webpageActions.addWebpageSuccess({ webpage: { ...webpage, id: 1 } }), 'test'] });
            expect(effects.addWebpageSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Webpage successfully added', 'The new webpage was added into the database');
        });
    });
    describe('addWebpageFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: webpageActions.addWebpageFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: webpageActions.addWebpageFail() });
            expect(effects.addWebpageFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add webpage', 'The new webpage could not be added into the database');
        });
    });
    describe('editWebpage$ effect', () => {
        it('should dispatch the editWebpageSuccess action on success', () => {
            actions = hot('a', { a: webpageActions.editWebpage({ webpage: { ...webpage, id: 1 } }) });
            service.editWebpage = jest.fn(() => of({ ...webpage, id: 1 }));
            let expected = cold('b', { b: webpageActions.editWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            expect(effects.editWebpage$).toBeObservable(expected);
            expect(service.editWebpage).toHaveBeenCalledWith({ ...webpage, id: 1 });

        });
        it('should dispatch the editWebpageFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageActions.editWebpage({ webpage: { ...webpage, id: 1 } }) });
            service.editWebpage = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageActions.editWebpageFail() });
            expect(effects.editWebpage$).toBeObservable(expected);
            expect(service.editWebpage).toHaveBeenCalledWith({ ...webpage, id: 1 });

        });

    });
    describe('editWebpageSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: webpageActions.editWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: webpageActions.editWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            expect(effects.editWebpageSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Webpage successfully edited', 'The existing webpage has been edited into the database');
        });
    });
    describe('editWebpageFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: webpageActions.editWebpageFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: webpageActions.editWebpageFail() });
            expect(effects.editWebpageFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit webpage', 'The existing webpage could not be edited into the database');
        });
    });
    describe('deleteWebpage$ effect', () => {
        it('should dispatch the deleteWebpageSuccess action on success', () => {
            actions = hot('a', { a: webpageActions.deleteWebpage({ webpage: { ...webpage, id: 1 } }) });
            service.deleteWebpage = jest.fn(() => of({ ...webpage, id: 1 }));
            let expected = cold('b', { b: webpageActions.deleteWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            expect(effects.deleteWebpage$).toBeObservable(expected);
            expect(service.deleteWebpage).toHaveBeenCalledWith(1);

        });
        it('should dispatch the deleteWebpageFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageActions.deleteWebpage({ webpage: { ...webpage, id: 1 } }) });
            service.deleteWebpage = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageActions.deleteWebpageFail() });
            expect(effects.deleteWebpage$).toBeObservable(expected);
            expect(service.deleteWebpage).toHaveBeenCalledWith(1);

        });

    });
    describe('deleteWebpageSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: webpageActions.deleteWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: webpageActions.deleteWebpageSuccess({ webpage: { ...webpage, id: 1 } }) });
            expect(effects.deleteWebpageSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Webpage successfully deleted', 'The existing webpage has been deleted');
        });
    });
    describe('deleteWebpageSuccess$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: webpageActions.deleteWebpageFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: webpageActions.deleteWebpageFail() });
            expect(effects.deleteWebpageFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to delete webpage', 'The existing webpage could not be deleted from the database');
        });
    });

});