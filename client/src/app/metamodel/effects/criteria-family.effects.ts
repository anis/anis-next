/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as criteriaFamilyActions from '../actions/criteria-family.actions';
import { CriteriaFamilyService } from '../services/criteria-family.service';
import * as datasetSelector from '../selectors/dataset.selector';

/**
 * @class
 * @classdesc Criteria family effects.
 */
@Injectable()
export class CriteriaFamilyEffects {

    /**
     * Calls action to retrieve criteria family list.
     */
    loadCriteriaFamilies$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.loadCriteriaFamilyList),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.criteriaFamilyService.retrieveCriteriaFamilyList(datasetName)
                .pipe(
                    map(criteriaFamilies => criteriaFamilyActions.loadCriteriaFamilyListSuccess({ criteriaFamilies })),
                    catchError(() => of(criteriaFamilyActions.loadCriteriaFamilyListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a criteria family.
     */
    addCriteriaFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.addCriteriaFamily),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.criteriaFamilyService.addCriteriaFamily(datasetName, action.criteriaFamily)
                .pipe(
                    map(criteriaFamily => criteriaFamilyActions.addCriteriaFamilySuccess({ criteriaFamily })),
                    catchError(() => of(criteriaFamilyActions.addCriteriaFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays add criteria family success notification.
     */
    addCriteriaFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.addCriteriaFamilySuccess),
            tap(() => this.toastr.success('Criteria family successfully added', 'The new criteria family was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add criteria family error notification.
     */
    addCriteriaFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.addCriteriaFamilyFail),
            tap(() => this.toastr.error('Failure to add criteria family', 'The new criteria family could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a criteria family.
     */
    editCriteriaFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.editCriteriaFamily),
            mergeMap(action => this.criteriaFamilyService.editCriteriaFamily(action.criteriaFamily)
                .pipe(
                    map(criteriaFamily => criteriaFamilyActions.editCriteriaFamilySuccess({ criteriaFamily })),
                    catchError(() => of(criteriaFamilyActions.editCriteriaFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays edit criteria family success notification.
     */
    editCriteriaFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.editCriteriaFamilySuccess),
            tap(() => this.toastr.success('Criteria family successfully edited', 'The existing criteria family has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit criteria family error notification.
     */
    editCriteriaFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.editCriteriaFamilyFail),
            tap(() => this.toastr.error('Failure to edit criteria family', 'The existing criteria family could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a criteria family.
     */
    deleteCriteriaFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.deleteCriteriaFamily),
            mergeMap(action => this.criteriaFamilyService.deleteCriteriaFamily(action.criteriaFamily.id)
                .pipe(
                    map(() => criteriaFamilyActions.deleteCriteriaFamilySuccess({ criteriaFamily: action.criteriaFamily })),
                    catchError(() => of(criteriaFamilyActions.deleteCriteriaFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays delete criteria family success notification.
     */
    deleteCriteriaFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.deleteCriteriaFamilySuccess),
            tap(() => this.toastr.success('Criteria family successfully deleted', 'The existing criteria family has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete criteria family error notification.
     */
    deleteCriteriaFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(criteriaFamilyActions.deleteCriteriaFamilyFail),
            tap(() => this.toastr.error('Failure to delete criteria family', 'The existing criteria family could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private criteriaFamilyService: CriteriaFamilyService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
