/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import * as instanceSelector from '../selectors/instance.selector';
import { WebpageFamilyEffects } from './webpage-family.effects';
import { WebpageFamilyService } from '../services/webpage-family.service';
import * as webpageFamilyActions from '../actions/webpage-family.actions';
import { WebpageFamily } from '../models';

describe('[Metamodel][Effects] WebpageFamilyEffects', () => {
    let actions = new Observable();
    let effects: WebpageFamilyEffects;
    let metadata: EffectsMetadata<WebpageFamilyEffects>;
    let service: WebpageFamilyService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let router: Router;
    let webpageFamily: WebpageFamily;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                WebpageFamilyEffects,
                { provide: WebpageFamilyService, useValue: {} },
                {
                    provide: Router, useValue: {
                        navigate: jest.fn(),
                        navigateByUrl: jest.fn()
                    }
                },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(WebpageFamilyEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(WebpageFamilyService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute, 'test'
        );
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadWebpageFamilies$ effect', () => {
        it('should dispatch the loadWebpageFamilyListSuccess action on success', () => {
            actions = hot('a', { a: webpageFamilyActions.loadWebpageFamilyList() });
            service.retrieveWebpageFamilyList = jest.fn(() => of([{ ...webpageFamily, id: 1 }]));
            let expected = cold('b', { b: webpageFamilyActions.loadWebpageFamilyListSuccess({ webpageFamilies: [{ ...webpageFamily, id: 1 }] }) });
            expect(effects.loadWebpageFamilies$).toBeObservable(expected);
            expect(service.retrieveWebpageFamilyList).toHaveBeenCalledTimes(1);

        });
        it('should dispatch the loadWebpageFamilyListFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageFamilyActions.loadWebpageFamilyList() });
            service.retrieveWebpageFamilyList = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageFamilyActions.loadWebpageFamilyListFail() });
            expect(effects.loadWebpageFamilies$).toBeObservable(expected);

        });
    });
    describe('addWebpageFamily$ effect', () => {
        it('should dispatch the addWebpageFamilySuccess action on success', () => {
            actions = hot('a', { a: webpageFamilyActions.addWebpageFamily({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            service.addWebpageFamily = jest.fn(() => of({ ...webpageFamily, id: 1 }));
            let expected = cold('b', { b: webpageFamilyActions.addWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            expect(effects.addWebpageFamily$).toBeObservable(expected);
            expect(service.addWebpageFamily).toHaveBeenCalledWith('test', { ...webpageFamily, id: 1 });

        });
        it('should dispatch the addWebpageFamilyFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageFamilyActions.addWebpageFamily({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            service.addWebpageFamily = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageFamilyActions.addWebpageFamilyFail() });
            expect(effects.addWebpageFamily$).toBeObservable(expected);
            expect(service.addWebpageFamily).toHaveBeenCalledWith('test', { ...webpageFamily, id: 1 });

        });

    });
    describe('addWebpageFamilySuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: webpageFamilyActions.addWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: [webpageFamilyActions.addWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }), 'test'] });
            expect(effects.addWebpageFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Webpage family successfully added', 'The new webpage family was added into the database');
        });
    });
    describe('addInstanceGroupFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: webpageFamilyActions.addWebpageFamilyFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: webpageFamilyActions.addWebpageFamilyFail() });
            expect(effects.addWebpageFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add webpage family', 'The new webpage family could not be added into the database');
        });
    });
    describe('editWebpageFamily$ effect', () => {
        it('should dispatch the editWebpageFamilySuccess action on success', () => {
            actions = hot('a', { a: webpageFamilyActions.editWebpageFamily({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            service.editWebpageFamily = jest.fn(() => of({ ...webpageFamily, id: 1 }));
            let expected = cold('b', { b: webpageFamilyActions.editWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            expect(effects.editWebpageFamily$).toBeObservable(expected);
            expect(service.editWebpageFamily).toHaveBeenCalledWith({ ...webpageFamily, id: 1 });

        });
        it('should dispatch the editWebpageFamilyFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageFamilyActions.editWebpageFamily({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            service.editWebpageFamily = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageFamilyActions.editWebpageFamilyFail() });
            expect(effects.editWebpageFamily$).toBeObservable(expected);
            expect(service.editWebpageFamily).toHaveBeenCalledWith({ ...webpageFamily, id: 1 });

        });

    });
    describe('editWebpageFamilySuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: webpageFamilyActions.editWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: [webpageFamilyActions.editWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }), 'test'] });
            expect(effects.editWebpageFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Webpage family successfully edited', 'The existing webpage family has been edited into the database');
        });
    });
    describe('editWebpageFamilyFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: webpageFamilyActions.editWebpageFamilyFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: webpageFamilyActions.editWebpageFamilyFail() });
            expect(effects.editWebpageFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit webpage family', 'The existing webpage family could not be edited into the database');
        });
    });
    describe('deleteWebpageFamily$ effect', () => {
        it('should dispatch the deleteWebpageFamilySuccess action on success', () => {
            actions = hot('a', { a: webpageFamilyActions.deleteWebpageFamily({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            service.deleteWebpageFamily = jest.fn(() => of({ ...webpageFamily, id: 1 }));
            let expected = cold('b', { b: webpageFamilyActions.deleteWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            expect(effects.deleteWebpageFamily$).toBeObservable(expected);
            expect(service.deleteWebpageFamily).toHaveBeenCalledWith(1);

        });
        it('should dispatch the deleteWebpageFamilyFail action on HTTP failure', () => {
            actions = hot('a', { a: webpageFamilyActions.deleteWebpageFamily({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            service.deleteWebpageFamily = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: webpageFamilyActions.deleteWebpageFamilyFail() });
            expect(effects.deleteWebpageFamily$).toBeObservable(expected);
            expect(service.deleteWebpageFamily).toHaveBeenCalledWith(1);

        });

    });
    describe('deleteWebpageFamilySuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: webpageFamilyActions.deleteWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: webpageFamilyActions.deleteWebpageFamilySuccess({ webpageFamily: { ...webpageFamily, id: 1 } }) });
            expect(effects.deleteWebpageFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Webpage family successfully deleted', 'The existing webpage family has been deleted');
        });
    });
    describe('deleteWebpageFamilyFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: webpageFamilyActions.deleteWebpageFamilyFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: webpageFamilyActions.deleteWebpageFamilyFail() });
            expect(effects.deleteWebpageFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to delete webpage family', 'The existing webpage family could not be deleted from the database');
        });
    });
});
