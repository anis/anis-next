/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as databaseActions from '../actions/database.actions';
import { DatabaseService } from '../services/database.service';

/**
 * @class
 * @classdesc Database effects.
 */
@Injectable()
export class DatabaseEffects {

    /**
     * Calls action to retrieve database list.
     */
    loadDatabases$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(databaseActions.loadDatabaseList),
            mergeMap(() => this.databaseService.retrieveDatabaseList()
                .pipe(
                    map(databases => databaseActions.loadDatabaseListSuccess({ databases })),
                    catchError(() => of(databaseActions.loadDatabaseListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a database.
     */
    addDatabase$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(databaseActions.addDatabase),
            mergeMap(action => this.databaseService.addDatabase(action.database)
                .pipe(
                    map(database => databaseActions.addDatabaseSuccess({ database })),
                    catchError(() => of(databaseActions.addDatabaseFail()))
                )
            )
        )
    );

    /**
     * Displays add database success notification.
     */
    addDatabaseSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(databaseActions.addDatabaseSuccess),
            tap(() => {
                this.router.navigate(['/admin/database/database-list']);
                this.toastr.success('Database successfully added', 'The new database was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add database error notification.
     */
    addDatabaseFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(databaseActions.addDatabaseFail),
            tap(() => this.toastr.error('Failure to add database', 'The new database could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a database.
     */
    editDatabase$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(databaseActions.editDatabase),
            mergeMap(action => this.databaseService.editDatabase(action.database)
                .pipe(
                    map(database => databaseActions.editDatabaseSuccess({ database })),
                    catchError(() => of(databaseActions.editDatabaseFail()))
                )
            )
        )
    );

    /**
     * Displays edit database success notification.
     */
    editDatabaseSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(databaseActions.editDatabaseSuccess),
            tap(() => {
                this.router.navigate(['/admin/database/database-list']);
                this.toastr.success('Database successfully edited', 'The existing database has been edited into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays edit database error notification.
     */
    editDatabaseFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(databaseActions.editDatabaseFail),
            tap(() => this.toastr.error('Failure to edit database', 'The existing database could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a database.
     */
    deleteDatabase$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(databaseActions.deleteDatabase),
            mergeMap(action => this.databaseService.deleteDatabase(action.database.id)
                .pipe(
                    map(() => databaseActions.deleteDatabaseSuccess({ database: action.database })),
                    catchError(() => of(databaseActions.deleteDatabaseFail()))
                )
            )
        )
    );

    /**
     * Displays delete database success notification.
     */
    deleteDatabaseSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(databaseActions.deleteDatabaseSuccess),
            tap(() => this.toastr.success('Database successfully deleted', 'The existing database has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete database error notification.
     */
    deleteDatabaseFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(databaseActions.deleteDatabaseFail),
            tap(() => this.toastr.error('Failure to delete database', 'The existing database could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private databaseService: DatabaseService,
        private router: Router,
        private toastr: ToastrService
    ) {}
}
