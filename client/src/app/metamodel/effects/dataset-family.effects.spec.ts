/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { DatasetFamilyEffects } from './dataset-family.effects';
import { DatasetFamilyService } from '../services/dataset-family.service';
import * as datasetFamilyActions from '../actions/dataset-family.actions';
import * as instanceSelector from '../selectors/instance.selector';
import { DATASET_FAMILY_LIST, DATASET_FAMILY } from '../../../test-data';
import { DatasetFamily } from '../models';

describe('[Metamodel][Effects] DatasetFamilyEffects', () => {
    let actions = new Observable();
    let effects: DatasetFamilyEffects;
    let metadata: EffectsMetadata<DatasetFamilyEffects>;
    let service: DatasetFamilyService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DatasetFamilyEffects,
                { provide: DatasetFamilyService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(DatasetFamilyEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(DatasetFamilyService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute, ''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDatasetFamilies$ effect', () => {
        it('should dispatch the loadDatasetFamilyListSuccess action on success', () => {
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'myInstance'
            );

            const action = datasetFamilyActions.loadDatasetFamilyList();
            const outcome = datasetFamilyActions.loadDatasetFamilyListSuccess({ datasetFamilies: DATASET_FAMILY_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET_FAMILY_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveDatasetFamilyList = jest.fn(() => response);

            expect(effects.loadDatasetFamilies$).toBeObservable(expected);
            expect(service.retrieveDatasetFamilyList).toHaveBeenCalledWith('myInstance');
        });

        it('should dispatch the loadDatasetFamilyListFail action on HTTP failure', () => {
            const action = datasetFamilyActions.loadDatasetFamilyList();
            const error = new Error();
            const outcome = datasetFamilyActions.loadDatasetFamilyListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveDatasetFamilyList = jest.fn(() => response);

            expect(effects.loadDatasetFamilies$).toBeObservable(expected);
        });
    });

    describe('addDatasetFamily$ effect', () => {
        it('should dispatch the addDatasetFamilySuccess action on success', () => {
            const action = datasetFamilyActions.addDatasetFamily({ datasetFamily: DATASET_FAMILY });
            const outcome = datasetFamilyActions.addDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.addDatasetFamily = jest.fn(() => response);

            expect(effects.addDatasetFamily$).toBeObservable(expected);
        });

        it('should dispatch the addDatasetFamilyFail action on HTTP failure', () => {
            const action = datasetFamilyActions.addDatasetFamily({ datasetFamily: DATASET_FAMILY });
            const error = new Error();
            const outcome = datasetFamilyActions.addDatasetFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.addDatasetFamily = jest.fn(() => response);

            expect(effects.addDatasetFamily$).toBeObservable(expected);
        });
    });

    describe('addDatasetFamilySuccess$ effect', () => {
        it('should display a success notification', () => {
            let datasetFamily: DatasetFamily;
            const spy = jest.spyOn(toastr, 'success');
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'test'
            );

            actions = hot('a', { a: datasetFamilyActions.addDatasetFamilySuccess({ datasetFamily: { ...datasetFamily, id: 1 } }) });
            const expected = cold('a', { a: [datasetFamilyActions.addDatasetFamilySuccess({ datasetFamily: { ...datasetFamily, id: 1 } }), 'test'] });
            expect(effects.addDatasetFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Dataset family successfully added', 'The new dataset family was added into the database');
        });
    });

    describe('addDatasetFamilyFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetFamilyActions.addDatasetFamilyFail();
            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addDatasetFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add dataset family',
                'The new dataset family could not be added into the database'
            );
        });
    });

    describe('editDatasetFamily$ effect', () => {
        it('should dispatch the editDatasetFamilySuccess action on success', () => {
            const action = datasetFamilyActions.editDatasetFamily({ datasetFamily: DATASET_FAMILY });
            const outcome = datasetFamilyActions.editDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.editDatasetFamily = jest.fn(() => response);

            expect(effects.editDatasetFamily$).toBeObservable(expected);
        });

        it('should dispatch the editDatasetFamilyFail action on HTTP failure', () => {
            const action = datasetFamilyActions.editDatasetFamily({ datasetFamily: DATASET_FAMILY });
            const error = new Error();
            const outcome = datasetFamilyActions.editDatasetFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.editDatasetFamily = jest.fn(() => response);

            expect(effects.editDatasetFamily$).toBeObservable(expected);
        });
    });

    describe('editDatasetFamilySuccess$ effect', () => {
        it('should display a success notification', () => {
            let spy = jest.spyOn(toastr, 'success');
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'test'
            );
            let action = datasetFamilyActions.editDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
            let expected = cold('a', { a: [action, 'test'] });
            actions = hot('a', { a: action })
            expect(effects.editDatasetFamilySuccess$).toBeObservable(expected)
        });
    });


    describe('editDatasetFamilyFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetFamilyActions.editDatasetFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editDatasetFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit dataset family',
                'The existing dataset family could not be edited into the database'
            );
        });
    });

    describe('deleteDatasetFamily$ effect', () => {
        it('should dispatch the deleteDatasetFamilySuccess action on success', () => {
            const action = datasetFamilyActions.deleteDatasetFamily({ datasetFamily: DATASET_FAMILY });
            const outcome = datasetFamilyActions.deleteDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.deleteDatasetFamily = jest.fn(() => response);

            expect(effects.deleteDatasetFamily$).toBeObservable(expected);
        });

        it('should dispatch the deleteDatasetFamilyFail action on HTTP failure', () => {
            const action = datasetFamilyActions.deleteDatasetFamily({ datasetFamily: DATASET_FAMILY });
            const error = new Error();
            const outcome = datasetFamilyActions.deleteDatasetFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.deleteDatasetFamily = jest.fn(() => response);

            expect(effects.deleteDatasetFamily$).toBeObservable(expected);
        });
    });

    describe('deleteDatasetFamilySuccess$ effect', () => {
        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = datasetFamilyActions.deleteDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'test'
            );
            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatasetFamilySuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Dataset family successfully deleted',
                'The existing dataset family has been deleted'
            );
        });
    });

    describe('deleteDatasetFamilyFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetFamilyActions.deleteDatasetFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatasetFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete dataset family',
                'The existing dataset family could not be deleted from the database'
            );
        });
    });
});
