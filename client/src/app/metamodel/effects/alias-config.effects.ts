/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as aliasConfigActions from '../actions/alias-config.actions';
import { AliasConfigService } from '../services/alias-config.service';
import * as datasetSelector from '../selectors/dataset.selector';

/**
 * @class
 * @classdesc Alias effects.
 */
@Injectable()
export class AliasConfigEffects {
    /**
     * Calls action to retrieve alias configuration
     */
    loadAliassConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(aliasConfigActions.loadAliasConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.aliasConfigService.retrieveAliasConfig(datasetName)
                .pipe(
                    map(aliasConfig => aliasConfigActions.loadAliasConfigSuccess({ aliasConfig })),
                    catchError(() => of(aliasConfigActions.loadAliasConfigFail()))
                )
            )
        )
    );

    /**
     * Calls action to add an alias.
     */
    addAliasConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(aliasConfigActions.addAliasConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.aliasConfigService.addAliasConfig(datasetName, action.aliasConfig)
                .pipe(
                    map(aliasConfig => aliasConfigActions.addAliasConfigSuccess({ aliasConfig })),
                    catchError(() => of(aliasConfigActions.addAliasConfigFail()))
                )
            )
        )
    );

    /**
     * Displays add alias configuration success notification.
     */
    addAliasConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(aliasConfigActions.addAliasConfigSuccess),
            tap(() => this.toastr.success('Cone search config successfully added', 'The new alias config was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add alias configuration error notification.
     */
    addAliasConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(aliasConfigActions.addAliasConfigFail),
            tap(() => this.toastr.error('Failure to add alias config', 'The new alias config could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an alias configuration
     */
    editAliasConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(aliasConfigActions.editAliasConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.aliasConfigService.editAliasConfig(datasetName, action.aliasConfig)
                .pipe(
                    map(aliasConfig => aliasConfigActions.editAliasConfigSuccess({ aliasConfig })),
                    catchError(() => of(aliasConfigActions.editAliasConfigFail()))
                )
            )
        )
    );

    /**
     * Displays edit alias configuration success notification.
     */
    editAliasConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(aliasConfigActions.editAliasConfigSuccess),
            tap(() => this.toastr.success('Cone search configuration successfully edited', 'The existing alias configuration has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit alias configuration error notification.
     */
    editAliasConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(aliasConfigActions.editAliasConfigFail),
            tap(() => this.toastr.error('Failure to edit alias configuration', 'The existing alias configuration could not be edited into the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private aliasConfigService: AliasConfigService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
