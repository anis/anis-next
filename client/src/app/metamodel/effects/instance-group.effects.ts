/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as instanceGroupActions from '../actions/instance-group.actions';
import { InstanceGroupService } from '../services/instance-group.service';

/**
 * @class
 * @classdesc Survey effects.
 */
@Injectable()
export class InstanceGroupEffects {
    /**
     * Calls action to retrieve instance group list.
     */
    loadInstanceGroups$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(instanceGroupActions.loadInstanceGroupList),
            mergeMap(() => this.instanceGroupService.retrieveInstanceGroupList()
                .pipe(
                    map(instanceGroups => instanceGroupActions.loadInstanceGroupListSuccess({ instanceGroups })),
                    catchError(() => of(instanceGroupActions.loadInstanceGroupListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a instanceGroup.
     */
    addInstanceGroup$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(instanceGroupActions.addInstanceGroup),
            mergeMap(action => this.instanceGroupService.addInstanceGroup(action.instanceGroup)
                .pipe(
                    map(instanceGroup => instanceGroupActions.addInstanceGroupSuccess({ instanceGroup })),
                    catchError(() => of(instanceGroupActions.addInstanceGroupFail()))
                )
            )
        )
    );

    /**
     * Displays add instanceGroup success notification.
     */
    addInstanceGroupSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceGroupActions.addInstanceGroupSuccess),
            tap(() => {
                this.router.navigateByUrl(`/admin/instance/instance-group`);
                this.toastr.success('Instance group successfully added', 'The new instance group was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add instanceGroup error notification.
     */
    addInstanceGroupFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceGroupActions.addInstanceGroupFail),
            tap(() => this.toastr.error('Failure to add instance group', 'The new instance group could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a instanceGroup.
     */
    editInstanceGroup$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(instanceGroupActions.editInstanceGroup),
            mergeMap(action => this.instanceGroupService.editInstanceGroup(action.instanceGroup)
                .pipe(
                    map(instanceGroup => instanceGroupActions.editInstanceGroupSuccess({ instanceGroup })),
                    catchError(() => of(instanceGroupActions.editInstanceGroupFail()))
                )
            )
        )
    );

    /**
     * Displays edit instanceGroup success notification.
     */
    editInstanceGroupSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceGroupActions.editInstanceGroupSuccess),
            tap(() => {
                this.router.navigateByUrl(`/admin/instance/instance-group`);
                this.toastr.success('Instance group successfully edited', 'The existing instance group has been edited into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays edit instanceGroup error notification.
     */
    editInstanceGroupFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceGroupActions.editInstanceGroupFail),
            tap(() => this.toastr.error('Failure to edit instance group', 'The existing instance group could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a instanceGroup.
     */
    deleteInstanceGroup$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(instanceGroupActions.deleteInstanceGroup),
            mergeMap(action => this.instanceGroupService.deleteInstanceGroup(action.instanceGroup.id)
                .pipe(
                    map(() => instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: action.instanceGroup })),
                    catchError(() => of(instanceGroupActions.deleteInstanceGroupFail()))
                )
            )
        )
    );

    /**
     * Displays delete instanceGroup success notification.
     */
    deleteInstanceGroupSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceGroupActions.deleteInstanceGroupSuccess),
            tap(() => this.toastr.success('Instance group successfully deleted', 'The existing instance group has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete instanceGroup error notification.
     */
    deleteInstanceGroupFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceGroupActions.deleteInstanceGroupFail),
            tap(() => this.toastr.error('Failure to delete instance group', 'The existing instance group could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private instanceGroupService: InstanceGroupService,
        private router: Router,
        private toastr: ToastrService
    ) {}
}
