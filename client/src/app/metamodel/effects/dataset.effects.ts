/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as datasetActions from '../actions/dataset.actions';
import { DatasetService } from '../services/dataset.service';
import * as instanceSelector from '../selectors/instance.selector';

/**
 * @class
 * @classdesc Dataset effects.
 */
@Injectable()
export class DatasetEffects {

    /**
     * Calls action to retrieve dataset list.
     */
    loadDatasets$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetActions.loadDatasetList),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([, instanceName]) => this.datasetService.retrieveDatasetList(instanceName)
                .pipe(
                    map(datasets => datasetActions.loadDatasetListSuccess({ datasets })),
                    catchError(() => of(datasetActions.loadDatasetListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a dataset.
     */
    addDataset$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetActions.addDataset),
            mergeMap(action => this.datasetService.addDataset(action.dataset)
                .pipe(
                    map(dataset => datasetActions.addDatasetSuccess({ dataset })),
                    catchError(() => of(datasetActions.addDatasetFail()))
                )
            )
        )
    );

    /**
     * Displays add dataset success notification.
     */
    addDatasetSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetActions.addDatasetSuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.router.navigate([`/admin/instance/configure-instance/${instanceName}`]);
                this.toastr.success('Dataset successfully added', 'The new dataset was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add dataset error notification.
     */
    addDatasetFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetActions.addDatasetFail),
            tap(() => this.toastr.error('Failure to add dataset', 'The new dataset could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to add a dataset.
     */
    editDataset$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetActions.editDataset),
            mergeMap(action => this.datasetService.editDataset(action.dataset)
                .pipe(
                    map(dataset => datasetActions.editDatasetSuccess({ dataset })),
                    catchError(() => of(datasetActions.editDatasetFail()))
                )
            )
        )
    );

    /**
     * Displays modify dataset success notification.
     */
    editDatasetSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetActions.editDatasetSuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.router.navigate([`/admin/instance/configure-instance/${instanceName}`]);
                this.toastr.success('Dataset successfully edited', 'The existing dataset has been edited into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays modify dataset error notification.
     */
    editDatasetFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetActions.editDatasetFail),
            tap(() => this.toastr.error('Failure to edit dataset', 'The existing dataset could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to add a dataset.
     */
    deleteDataset$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetActions.deleteDataset),
            mergeMap(action => this.datasetService.deleteDataset(action.dataset.name)
                .pipe(
                    map(() => datasetActions.deleteDatasetSuccess({ dataset: action.dataset })),
                    catchError(() => of(datasetActions.deleteDatasetFail()))
                )
            )
        )
    );

    /**
     * Displays remove dataset success notification.
     */
    deleteDatasetSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetActions.deleteDatasetSuccess),
            tap(() => this.toastr.success('Dataset successfully deleted', 'The existing dataset has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays remove dataset error notification.
     */
    deleteDatasetFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetActions.deleteDatasetFail),
            tap(() => this.toastr.error('Failure to delete dataset', 'The existing dataset could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private datasetService: DatasetService,
        private router: Router,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
