/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as detailConfigActions from '../actions/detail-config.actions';
import { DetailConfigService } from '../services/detail-config.service';
import * as datasetSelector from '../selectors/dataset.selector';

/**
 * @class
 * @classdesc Detail effects.
 */
@Injectable()
export class DetailConfigEffects {
    /**
     * Calls action to retrieve detail configuration
     */
    loadDetailsConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(detailConfigActions.loadDetailConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.detailConfigService.retrieveDetailConfig(datasetName)
                .pipe(
                    map(detailConfig => detailConfigActions.loadDetailConfigSuccess({ detailConfig })),
                    catchError(() => of(detailConfigActions.loadDetailConfigFail()))
                )
            )
        )
    );

    /**
     * Calls action to add an detail.
     */
    addDetailConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(detailConfigActions.addDetailConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.detailConfigService.addDetailConfig(datasetName, action.detailConfig)
                .pipe(
                    map(detailConfig => detailConfigActions.addDetailConfigSuccess({ detailConfig })),
                    catchError(() => of(detailConfigActions.addDetailConfigFail()))
                )
            )
        )
    );

    /**
     * Displays add detail configuration success notification.
     */
    addDetailConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(detailConfigActions.addDetailConfigSuccess),
            tap(() => this.toastr.success('Detail config successfully added', 'The new detail config was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add detail configuration error notification.
     */
    addDetailConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(detailConfigActions.addDetailConfigFail),
            tap(() => this.toastr.error('Failure to add detail config', 'The new detail config could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an detail configuration
     */
    editDetailConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(detailConfigActions.editDetailConfig),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.detailConfigService.editDetailConfig(datasetName, action.detailConfig)
                .pipe(
                    map(detailConfig => detailConfigActions.editDetailConfigSuccess({ detailConfig })),
                    catchError(() => of(detailConfigActions.editDetailConfigFail()))
                )
            )
        )
    );

    /**
     * Displays edit detail configuration success notification.
     */
    editDetailConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(detailConfigActions.editDetailConfigSuccess),
            tap(() => this.toastr.success('Detail configuration successfully edited', 'The existing detail configuration has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit detail configuration error notification.
     */
    editDetailConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(detailConfigActions.editDetailConfigFail),
            tap(() => this.toastr.error('Failure to edit detail configuration', 'The existing detail configuration could not be edited into the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private detailConfigService: DetailConfigService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
