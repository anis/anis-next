/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { DatasetGroupEffects } from './dataset-group.effects';
import { DatasetGroupService } from '../services/dataset-group.service';
import * as datasetGroupActions from '../actions/dataset-group.actions';
import * as instanceSelector from '../selectors/instance.selector';
import { GROUP, GROUP_LIST } from '../../../test-data';
import { DatasetGroup } from '../models';

describe('[Metamodel][Effects] DatasetGroupEffects', () => {
    let actions = new Observable();
    let effects: DatasetGroupEffects;
    let metadata: EffectsMetadata<DatasetGroupEffects>;
    let service: DatasetGroupService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DatasetGroupEffects,
                { provide: DatasetGroupService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn(), navigateByUrl: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(DatasetGroupEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(DatasetGroupService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute, ''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDatasetGroups$ effect', () => {
        it('should dispatch the loadDatasetGroupListSuccess action on success', () => {
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'myInstance'
            );

            const action = datasetGroupActions.loadDatasetGroupList();
            const outcome = datasetGroupActions.loadDatasetGroupListSuccess({ datasetGroups: GROUP_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: GROUP_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveDatasetGroupList = jest.fn(() => response);

            expect(effects.loadDatasetGroups$).toBeObservable(expected);
            expect(service.retrieveDatasetGroupList).toHaveBeenCalledWith('myInstance');
        });

        it('should dispatch the loadDatasetGroupListFail action on HTTP failure', () => {
            const action = datasetGroupActions.loadDatasetGroupList();
            const error = new Error();
            const outcome = datasetGroupActions.loadDatasetGroupListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveDatasetGroupList = jest.fn(() => response);

            expect(effects.loadDatasetGroups$).toBeObservable(expected);
        });
    });

    describe('addDatasetGroup$ effect', () => {
        it('should dispatch the addDatasetGroupSuccess action on success', () => {
            const action = datasetGroupActions.addDatasetGroup({ datasetGroup: GROUP });
            const outcome = datasetGroupActions.addDatasetGroupSuccess({ datasetGroup: GROUP });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: GROUP });
            const expected = cold('--b', { b: outcome });
            service.addDatasetGroup = jest.fn(() => response);

            expect(effects.addDatasetGroup$).toBeObservable(expected);
        });

        it('should dispatch the addDatasetGroupFail action on HTTP failure', () => {
            const action = datasetGroupActions.addDatasetGroup({ datasetGroup: GROUP });
            const error = new Error();
            const outcome = datasetGroupActions.addDatasetGroupFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.addDatasetGroup = jest.fn(() => response);

            expect(effects.addDatasetGroup$).toBeObservable(expected);
        });
    });

    describe('addDatasetGroupSuccess$ effect', () => {
        it('hould display success notification  and call router.navigateByUrl', () => {
            let spyOnToast = jest.spyOn(toastr, 'success');
            let spyOnRouterNavigateByUrl = jest.spyOn(router, 'navigateByUrl');
            let datasetGroup: DatasetGroup;
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'myInstance'
            );
            actions = hot('a', { a: datasetGroupActions.addDatasetGroupSuccess({ datasetGroup: { ...datasetGroup, id: 1 } }) });
            let expected = cold('b', { b: [datasetGroupActions.addDatasetGroupSuccess({ datasetGroup: { ...datasetGroup, id: 1 } }) , 'myInstance']});
            expect(effects.addDatasetGroupSuccess$).toBeObservable(expected);
            expect(spyOnToast).toHaveBeenCalledWith('Dataset group successfully added', 'The new dataset group was added into the database');
            expect(spyOnRouterNavigateByUrl).toHaveBeenCalledWith('/admin/instance/configure-instance/myInstance/dataset-group');

        });
    });

    describe('addDatasetGroupFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetGroupActions.addDatasetGroupFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addDatasetGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add dataset group',
                'The new dataset group could not be added into the database'
            );
        });
    });

    describe('editDatasetGroup$ effect', () => {
        it('should dispatch the editDatasetGroupSuccess action on success', () => {
            const action = datasetGroupActions.editDatasetGroup({ datasetGroup: GROUP });
            const outcome = datasetGroupActions.editDatasetGroupSuccess({ datasetGroup: GROUP });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: GROUP });
            const expected = cold('--b', { b: outcome });
            service.editDatasetGroup = jest.fn(() => response);

            expect(effects.editDatasetGroup$).toBeObservable(expected);
        });

        it('should dispatch the editDatasetGroupFail action on HTTP failure', () => {
            const action = datasetGroupActions.editDatasetGroup({ datasetGroup: GROUP });
            const error = new Error();
            const outcome = datasetGroupActions.editDatasetGroupFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.editDatasetGroup = jest.fn(() => response);

            expect(effects.editDatasetGroup$).toBeObservable(expected);
        });
    });

    describe('editDatasetGroupSuccess$ effect', () => {
        it('should display success notification and call router.naviteByUrl', () => {
            let spyOnRouterNavigateByUrl = jest.spyOn(router, 'navigateByUrl');
            let datasetGroup: DatasetGroup;
            let spyOnToast = jest.spyOn(toastr, 'success');
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'myInstance'
            );
            actions = hot('a', { a: datasetGroupActions.editDatasetGroupSuccess({ datasetGroup: { ...datasetGroup, id: 1 } }) });
            let expected = cold('b', { b: [datasetGroupActions.editDatasetGroupSuccess({ datasetGroup: { ...datasetGroup, id: 1 } }) , 'myInstance']});
            expect(effects.editDatasetGroupSuccess$).toBeObservable(expected);
            expect(spyOnToast).toHaveBeenCalledWith('Dataset group successfully edited', 'The existing dataset group has been edited into the database');
            expect(spyOnRouterNavigateByUrl).toHaveBeenCalledWith('/admin/instance/configure-instance/myInstance/dataset-group');

        });
    });

    describe('editDatasetGroupFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetGroupActions.editDatasetGroupFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editDatasetGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit dataset group',
                'The existing dataset group could not be edited into the database'
            );
        });
    });

    describe('deleteDatasetGroup$ effect', () => {
        it('should dispatch the deleteDatasetGroupSuccess action on success', () => {
            const action = datasetGroupActions.deleteDatasetGroup({ datasetGroup: GROUP });
            const outcome = datasetGroupActions.deleteDatasetGroupSuccess({ datasetGroup: GROUP });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: GROUP });
            const expected = cold('--b', { b: outcome });
            service.deleteDatasetGroup = jest.fn(() => response);

            expect(effects.deleteDatasetGroup$).toBeObservable(expected);
        });

        it('should dispatch the deleteDatasetGroupFail action on HTTP failure', () => {
            const action = datasetGroupActions.deleteDatasetGroup({ datasetGroup: GROUP });
            const error = new Error();
            const outcome = datasetGroupActions.deleteDatasetGroupFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.deleteDatasetGroup = jest.fn(() => response);

            expect(effects.deleteDatasetGroup$).toBeObservable(expected);
        });
    });

    describe('deleteDatasetGroupSuccess$ effect', () => {


        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = datasetGroupActions.deleteDatasetGroupSuccess({ datasetGroup: GROUP });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatasetGroupSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Dataset group successfully deleted',
                'The existing dataset group has been deleted'
            );
        });
    });

    describe('deleteDatasetGroupFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetGroupActions.deleteDatasetGroupFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatasetGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete dataset group',
                'The existing dataset group could not be deleted from the database'
            );
        });
    });
});
