/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import * as datasetSelector from '../selectors/dataset.selector';
import { ImageEffects } from './image.effects';
import { ImageService } from '../services/image.service';
import * as imageActions from '../actions/image.actions';
import { Image } from '../models';

describe('[Metamodel][Effects] ImageEffects', () => {
    let actions = new Observable();
    let effects: ImageEffects;
    let metadata: EffectsMetadata<ImageEffects>;
    let service: ImageService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockDatasetSelectorDatasetNameByRoute;
    let router: Router;
    let image: Image;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ImageEffects,
                { provide: ImageService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(ImageEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(ImageService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute, 'test'
        );
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadImages$ effect', () => {
        it('should dispatch the loadImageListSuccess action on success', () => {
            actions = hot('a', { a: imageActions.loadImageList() });
            service.retrieveImageList = jest.fn(() => of([{ ...image, id: 1 }]));
            let expected = cold('b', { b: imageActions.loadImageListSuccess({ images: [{ ...image, id: 1 }] }) });
            expect(effects.loadImages$).toBeObservable(expected);
            expect(service.retrieveImageList).toHaveBeenCalledWith('test');

        });
        it('should dispatch the loadImageListFail action on HTTP failure', () => {
            actions = hot('a', { a: imageActions.loadImageList() });
            service.retrieveImageList = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: imageActions.loadImageListFail() });
            expect(effects.loadImages$).toBeObservable(expected);
            expect(service.retrieveImageList).toHaveBeenCalledWith('test');

        });
    });
    describe('addImage$ effect', () => {
        it('should dispatch the addImageSuccess action on success', () => {
            actions = hot('a', { a: imageActions.addImage({ image: { ...image, id: 1 } }) });
            service.addImage = jest.fn(() => of({ ...image, id: 1 }));
            let expected = cold('b', { b: imageActions.addImageSuccess({ image: { ...image, id: 1 } }) });
            expect(effects.addImage$).toBeObservable(expected);
            expect(service.addImage).toHaveBeenCalledWith('test', { ...image, id: 1 });

        });
        it('should dispatch the addImageFail action on HTTP failure', () => {
            actions = hot('a', { a: imageActions.addImage({ image: { ...image, id: 1 } }) });
            service.addImage = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: imageActions.addImageFail() });
            expect(effects.addImage$).toBeObservable(expected);
            expect(service.addImage).toHaveBeenCalledWith('test', { ...image, id: 1 });

        });
    });
    describe('addImageSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: imageActions.addImageSuccess({ image: { ...image, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: imageActions.addImageSuccess({ image: { ...image, id: 1 } }) });
            expect(effects.addImageSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Image successfully added', 'The new image was added into the database');
        });
    });
    describe('addImageFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: imageActions.addImageFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: imageActions.addImageFail() });
            expect(effects.addImageFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add image', 'The new image could not be added into the database');
        });
    });
    describe('editImage$ effect', () => {
        it('should dispatch the editImageSuccess action on success', () => {
            actions = hot('a', { a: imageActions.editImage({ image: { ...image, id: 1 } }) });
            service.editImage = jest.fn(() => of({ ...image, id: 1 }));
            let expected = cold('b', { b: imageActions.editImageSuccess({ image: { ...image, id: 1 } }) });
            expect(effects.editImage$).toBeObservable(expected);
            expect(service.editImage).toHaveBeenCalledWith({ ...image, id: 1 });

        });
        it('should dispatch the editImageFail action on HTTP failure', () => {
            actions = hot('a', { a: imageActions.editImage({ image: { ...image, id: 1 } }) });
            service.editImage = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: imageActions.editImageFail() });
            expect(effects.editImage$).toBeObservable(expected);
            expect(service.editImage).toHaveBeenCalledWith({ ...image, id: 1 });

        });
    });
    describe('editImageSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: imageActions.editImageSuccess({ image: { ...image, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: imageActions.editImageSuccess({ image: { ...image, id: 1 } }) });
            expect(effects.editImageSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Image successfully edited', 'The existing image has been edited into the database');
        });
    });
    describe('editImageFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: imageActions.editImageFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: imageActions.editImageFail() });
            expect(effects.editImageFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit image', 'The existing image could not be edited into the database');
        });
    });
    describe('deleteImage$ effect', () => {
        it('should dispatch the deleteImageSuccess action on success', () => {
            actions = hot('a', { a: imageActions.deleteImage({ image: { ...image, id: 1 } }) });
            service.deleteImage = jest.fn(() => of({ ...image, id: 1 }));
            let expected = cold('b', { b: imageActions.deleteImageSuccess({ image: { ...image, id: 1 } }) });
            expect(effects.deleteImage$).toBeObservable(expected);
            expect(service.deleteImage).toHaveBeenCalledWith(1);

        });
        it('should dispatch the deleteImageFail action on HTTP failure', () => {
            actions = hot('a', { a: imageActions.deleteImage({ image: { ...image, id: 1 } }) });
            service.deleteImage = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: imageActions.deleteImageFail() });
            expect(effects.deleteImage$).toBeObservable(expected);
            expect(service.deleteImage).toHaveBeenCalledWith(1);

        });
    });
    describe('deleteImageSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: imageActions.deleteImageSuccess({ image: { ...image, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: imageActions.deleteImageSuccess({ image: { ...image, id: 1 } }) });
            expect(effects.deleteImageSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Image successfully deleted', 'The existing image has been deleted');
        });
    });
    describe('deleteImageFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: imageActions.deleteImageFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: imageActions.deleteImageFail() });
            expect(effects.deleteImageFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to delete image', 'The existing image could not be deleted from the database');
        });
    });
});
