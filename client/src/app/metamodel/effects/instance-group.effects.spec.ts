/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import * as datasetSelector from '../selectors/dataset.selector';
import { InstanceGroupEffects } from './instance-group.effects';
import { InstanceGroupService } from '../services/instance-group.service';
import * as instanceGroupActions from '../actions/instance-group.actions';
import { InstanceGroup } from '../models';

describe('[Metamodel][Effects] InstanceGroupEffects', () => {
    let actions = new Observable();
    let effects: InstanceGroupEffects;
    let metadata: EffectsMetadata<InstanceGroupEffects>;
    let service: InstanceGroupService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockDatasetSelectorDatasetNameByRoute;
    let router: Router;
    let instanceGroup: InstanceGroup;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                InstanceGroupEffects,
                { provide: InstanceGroupService, useValue: {} },
                {
                    provide: Router, useValue: {
                        navigate: jest.fn(),
                        navigateByUrl: jest.fn()
                    }
                },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(InstanceGroupEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(InstanceGroupService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute, 'test'
        );
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadInstanceGroups$ effect', () => {
        it('should dispatch the loadInstanceGroupListSuccess action on success', () => {
            actions = hot('a', { a: instanceGroupActions.loadInstanceGroupList() });
            service.retrieveInstanceGroupList = jest.fn(() => of([{ ...instanceGroup, id: 1 }]));
            let expected = cold('b', { b: instanceGroupActions.loadInstanceGroupListSuccess({ instanceGroups: [{ ...instanceGroup, id: 1 }] }) });
            expect(effects.loadInstanceGroups$).toBeObservable(expected);
            expect(service.retrieveInstanceGroupList).toHaveBeenCalledTimes(1);

        });
        it('should dispatch the loadInstanceGroupListFail action on HTTP failure', () => {
            actions = hot('a', { a: instanceGroupActions.loadInstanceGroupList() });
            service.retrieveInstanceGroupList = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: instanceGroupActions.loadInstanceGroupListFail() });
            expect(effects.loadInstanceGroups$).toBeObservable(expected);

        });
    });
    describe('addInstanceGroup$ effect', () => {
        it('should dispatch the addInstanceGroupSuccess action on success', () => {
            actions = hot('a', { a: instanceGroupActions.addInstanceGroup({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            service.addInstanceGroup = jest.fn(() => of({ ...instanceGroup, id: 1 }));
            let expected = cold('b', { b: instanceGroupActions.addInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            expect(effects.addInstanceGroup$).toBeObservable(expected);
            expect(service.addInstanceGroup).toHaveBeenCalledWith({ ...instanceGroup, id: 1 });

        });
        it('should dispatch the addInstanceGroupFail action on HTTP failure', () => {
            actions = hot('a', { a: instanceGroupActions.addInstanceGroup({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            service.addInstanceGroup = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: instanceGroupActions.addInstanceGroupFail() });
            expect(effects.addInstanceGroup$).toBeObservable(expected);
            expect(service.addInstanceGroup).toHaveBeenCalledWith({ ...instanceGroup, id: 1 });

        });
    });
    describe('addInstanceGroupSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: instanceGroupActions.addInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            let spyOnToast = jest.spyOn(toastr, 'success');
            let spyOnnavigateByUrl = jest.spyOn(router, 'navigateByUrl');
            let expected = cold('b', { b: instanceGroupActions.addInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            expect(effects.addInstanceGroupSuccess$).toBeObservable(expected);
            expect(spyOnToast).toHaveBeenCalledWith('Instance group successfully added', 'The new instance group was added into the database');
            expect(spyOnnavigateByUrl).toHaveBeenCalledWith(`/admin/instance/instance-group`);
        });
    });
    describe('addInstanceGroupFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: instanceGroupActions.addInstanceGroupFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: instanceGroupActions.addInstanceGroupFail() });
            expect(effects.addInstanceGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to add instance group', 'The new instance group could not be added into the database');
        });
    });

    describe('editInstanceGroup$ effect', () => {
        it('should dispatch the editInstanceGroupSuccess action on success', () => {
            actions = hot('a', { a: instanceGroupActions.editInstanceGroup({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            service.editInstanceGroup = jest.fn(() => of({ ...instanceGroup, id: 1 }));
            let expected = cold('b', { b: instanceGroupActions.editInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            expect(effects.editInstanceGroup$).toBeObservable(expected);
            expect(service.editInstanceGroup).toHaveBeenCalledWith({ ...instanceGroup, id: 1 });

        });
        it('should dispatch the editInstanceGroupFail action on HTTP failure', () => {
            actions = hot('a', { a: instanceGroupActions.editInstanceGroup({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            service.editInstanceGroup = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: instanceGroupActions.editInstanceGroupFail() });
            expect(effects.editInstanceGroup$).toBeObservable(expected);
            expect(service.editInstanceGroup).toHaveBeenCalledWith({ ...instanceGroup, id: 1 });

        });
    });
    describe('editInstanceGroupSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: instanceGroupActions.editInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            let spyOnToast = jest.spyOn(toastr, 'success');
            let spyOnnavigateByUrl = jest.spyOn(router, 'navigateByUrl');
            let expected = cold('b', { b: instanceGroupActions.editInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            expect(effects.editInstanceGroupSuccess$).toBeObservable(expected);
            expect(spyOnToast).toHaveBeenCalledWith('Instance group successfully edited', 'The existing instance group has been edited into the database');
            expect(spyOnnavigateByUrl).toHaveBeenCalledWith(`/admin/instance/instance-group`);
        });
    });
    describe('editInstanceGroupFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: instanceGroupActions.editInstanceGroupFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: instanceGroupActions.editInstanceGroupFail() });
            expect(effects.editInstanceGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to edit instance group', 'The existing instance group could not be edited into the database');
        });
    });
    describe('deleteInstanceGroup$ effect', () => {
        it('should dispatch the deleteInstanceGroupSuccess action on success', () => {
            actions = hot('a', { a: instanceGroupActions.deleteInstanceGroup({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            service.deleteInstanceGroup = jest.fn(() => of({ ...instanceGroup, id: 1 }));
            let expected = cold('b', { b: instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            expect(effects.deleteInstanceGroup$).toBeObservable(expected);
            expect(service.deleteInstanceGroup).toHaveBeenCalledWith(1);

        });
        it('should dispatch the deleteInstanceGroupFail action on HTTP failure', () => {
            actions = hot('a', { a: instanceGroupActions.deleteInstanceGroup({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            service.deleteInstanceGroup = jest.fn(() => throwError(() => new Error()));
            let expected = cold('b', { b: instanceGroupActions.deleteInstanceGroupFail() });
            expect(effects.deleteInstanceGroup$).toBeObservable(expected);
            expect(service.deleteInstanceGroup).toHaveBeenCalledWith(1);

        });
    });
    describe('deleteInstanceGroupSuccess$ effect', () => {
        it('should display notification on success', () => {
            actions = hot('a', { a: instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            let spy = jest.spyOn(toastr, 'success');
            let expected = cold('b', { b: instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: { ...instanceGroup, id: 1 } }) });
            expect(effects.deleteInstanceGroupSuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Instance group successfully deleted', 'The existing instance group has been deleted');
        });
    });
    describe('deleteInstanceGroupFail$ effect', () => {
        it('should display notification on faillure', () => {
            actions = hot('a', { a: instanceGroupActions.deleteInstanceGroupFail() });
            let spy = jest.spyOn(toastr, 'error');
            let expected = cold('b', { b: instanceGroupActions.deleteInstanceGroupFail() });
            expect(effects.deleteInstanceGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('Failure to delete instance group', 'The existing instance group could not be deleted from the database');
        });
    });
});
