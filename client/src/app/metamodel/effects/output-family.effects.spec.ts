/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { OutputFamilyEffects } from './output-family.effects';
import { OutputFamilyService } from '../services/output-family.service';
import * as outputFamilyActions from '../actions/output-family.actions';
import { OUTPUT_FAMILY, OUTPUT_FAMILY_LIST } from '../../../test-data';
import * as datasetSelector from '../selectors/dataset.selector';

describe('[Metamodel][Effects] OutputFamilyEffects', () => {
    let actions = new Observable();
    let effects: OutputFamilyEffects;
    let metadata: EffectsMetadata<OutputFamilyEffects>;
    let service: OutputFamilyService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: { } };
    let mockDatasetSelectorSelectDatasetNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                OutputFamilyEffects,
                { provide: OutputFamilyService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }},
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(OutputFamilyEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(OutputFamilyService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute,''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadOutputFamilies$ effect', () => {
        it('should dispatch the loadOutputFamilyListSuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = outputFamilyActions.loadOutputFamilyList();
            const outcome = outputFamilyActions.loadOutputFamilyListSuccess({ outputFamilies: OUTPUT_FAMILY_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: OUTPUT_FAMILY_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveOutputFamilyList = jest.fn(() => response);

            expect(effects.loadOutputFamilies$).toBeObservable(expected);
            expect(service.retrieveOutputFamilyList).toHaveBeenCalledWith('myDataset');
        });

        it('should dispatch the loadOutputFamilyListFail action on HTTP failure', () => {
            const action = outputFamilyActions.loadOutputFamilyList();
            const error = new Error();
            const outcome = outputFamilyActions.loadOutputFamilyListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveOutputFamilyList = jest.fn(() => response);

            expect(effects.loadOutputFamilies$).toBeObservable(expected);
        });
    });

    describe('addOutputFamily$ effect', () => {
        it('should dispatch the addOutputFamilySuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = outputFamilyActions.addOutputFamily({ outputFamily: OUTPUT_FAMILY });
            const outcome = outputFamilyActions.addOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: OUTPUT_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.addOutputFamily = jest.fn(() => response);

            expect(effects.addOutputFamily$).toBeObservable(expected);
            expect(service.addOutputFamily).toHaveBeenCalledWith('myDataset', OUTPUT_FAMILY);
        });

        it('should dispatch the addOutputFamilyFail action on HTTP failure', () => {
            const action = outputFamilyActions.addOutputFamily({ outputFamily: OUTPUT_FAMILY });
            const error = new Error();
            const outcome = outputFamilyActions.addOutputFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.addOutputFamily = jest.fn(() => response);

            expect(effects.addOutputFamily$).toBeObservable(expected);
        });
    });

    describe('addOutputFamilySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addOutputFamilySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = outputFamilyActions.addOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addOutputFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Output family successfully added',
                'The new output family was added into the database'
            );
        });
    });

    describe('addOutputFamilyFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addOutputFamilyFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = outputFamilyActions.addOutputFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addOutputFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add output family',
                'The new output family could not be added into the database'
            );
        });
    });

    describe('editOutputFamily$ effect', () => {
        it('should dispatch the editOutputFamilySuccess action on success', () => {
            const action = outputFamilyActions.editOutputFamily({ outputFamily: OUTPUT_FAMILY });
            const outcome = outputFamilyActions.editOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: OUTPUT_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.editOutputFamily = jest.fn(() => response);

            expect(effects.editOutputFamily$).toBeObservable(expected);
        });

        it('should dispatch the editOutputFamilyFail action on HTTP failure', () => {
            const action = outputFamilyActions.editOutputFamily({ outputFamily: OUTPUT_FAMILY });
            const error = new Error();
            const outcome = outputFamilyActions.editOutputFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.editOutputFamily = jest.fn(() => response);

            expect(effects.editOutputFamily$).toBeObservable(expected);
        });
    });

    describe('editOutputFamilySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editOutputFamilySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = outputFamilyActions.editOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editOutputFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Output family successfully edited',
                'The existing output family has been edited into the database'
            );
        });
    });

    describe('editOutputFamilyFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editOutputFamilyFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = outputFamilyActions.editOutputFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editOutputFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit output family',
                'The existing output family could not be edited into the database'
            );
        });
    });

    describe('deleteOutputFamily$ effect', () => {
        it('should dispatch the deleteSurveySuccess action on success', () => {
            const action = outputFamilyActions.deleteOutputFamily({ outputFamily: OUTPUT_FAMILY });
            const outcome = outputFamilyActions.deleteOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: OUTPUT_FAMILY });
            const expected = cold('--b', { b: outcome });
            service.deleteOutputFamily = jest.fn(() => response);

            expect(effects.deleteOutputFamily$).toBeObservable(expected);
        });

        it('should dispatch the deleteOutputFamilyFail action on HTTP failure', () => {
            const action = outputFamilyActions.deleteOutputFamily({ outputFamily: OUTPUT_FAMILY });
            const error = new Error();
            const outcome = outputFamilyActions.deleteOutputFamilyFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.deleteOutputFamily = jest.fn(() => response);

            expect(effects.deleteOutputFamily$).toBeObservable(expected);
        });
    });

    describe('deleteOutputFamilySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteOutputFamilySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = outputFamilyActions.deleteOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteOutputFamilySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Output family successfully deleted',
                'The existing output family has been deleted'
            );
        });
    });

    describe('deleteOutputFamilyFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteOutputFamilyFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = outputFamilyActions.deleteOutputFamilyFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteOutputFamilyFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete output family',
                'The existing output family could not be deleted from the database'
            );
        });
    });
});
