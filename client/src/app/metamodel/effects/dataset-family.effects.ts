/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom }  from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as datasetFamilyActions from '../actions/dataset-family.actions';
import { DatasetFamilyService } from '../services/dataset-family.service';
import * as instanceSelector from '../selectors/instance.selector';


/**
 * @class
 * @classdesc Dataset family effects.
 */
@Injectable()
export class DatasetFamilyEffects {

    /**
     * Calls action to retrieve dataset family list.
     */
    loadDatasetFamilies$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.loadDatasetFamilyList),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([, instanceName]) => this.datasetFamilyService.retrieveDatasetFamilyList(instanceName)
                .pipe(
                    map(datasetFamilies => datasetFamilyActions.loadDatasetFamilyListSuccess({ datasetFamilies })),
                    catchError(() => of(datasetFamilyActions.loadDatasetFamilyListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a dataset family.
     */
    addDatasetFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.addDatasetFamily),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.datasetFamilyService.addDatasetFamily(instanceName, action.datasetFamily)
                .pipe(
                    map(datasetFamily => datasetFamilyActions.addDatasetFamilySuccess({ datasetFamily })),
                    catchError(() => of(datasetFamilyActions.addDatasetFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays add dataset family success notification.
     */
    addDatasetFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.addDatasetFamilySuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.toastr.success('Dataset family successfully added', 'The new dataset family was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add dataset family fail notification.
     */
    addDatasetFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.addDatasetFamilyFail),
            tap(() => this.toastr.error('Failure to add dataset family', 'The new dataset family could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a dataset family.
     */
    editDatasetFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.editDatasetFamily),
            mergeMap(action => this.datasetFamilyService.editDatasetFamily(action.datasetFamily)
                .pipe(
                    map(datasetFamily => datasetFamilyActions.editDatasetFamilySuccess({ datasetFamily })),
                    catchError(() => of(datasetFamilyActions.editDatasetFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays edit dataset family success notification.
     */
    editDatasetFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.editDatasetFamilySuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([action, instanceName]) => {
                this.toastr.success('Dataset family successfully edited', 'The existing dataset family has been edited into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays edit dataset family error notification.
     */
    editDatasetFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.editDatasetFamilyFail),
            tap(() => this.toastr.error('Failure to edit dataset family', 'The existing dataset family could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a dataset family.
     */
    deleteDatasetFamily$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.deleteDatasetFamily),
            mergeMap(action => this.datasetFamilyService.deleteDatasetFamily(action.datasetFamily.id)
                .pipe(
                    map(() => datasetFamilyActions.deleteDatasetFamilySuccess({ datasetFamily: action.datasetFamily })),
                    catchError(() => of(datasetFamilyActions.deleteDatasetFamilyFail()))
                )
            )
        )
    );

    /**
     * Displays delete dataset family success notification.
     */
    deleteDatasetFamilySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.deleteDatasetFamilySuccess),
            tap(() => this.toastr.success('Dataset family successfully deleted', 'The existing dataset family has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete dataset family error notification.
     */
    deleteDatasetFamilyFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(datasetFamilyActions.deleteDatasetFamilyFail),
            tap(() => this.toastr.error('Failure to delete dataset family', 'The existing dataset family could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private datasetFamilyService: DatasetFamilyService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
