/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';

import { ToastrService } from 'ngx-toastr';
import { OutputCategoryEffects } from './output-category.effects';
import { OutputCategoryService } from '../services/output-category.service';
import * as outputCategoryActions from '../actions/output-category.actions';
import * as datasetSelector from '../selectors/dataset.selector';
import { CATEGORY, CATEGORY_LIST } from '../../../test-data';

describe('[Metamodel][Effects] OutputCategoryEffects', () => {
    let actions = new Observable();
    let effects: OutputCategoryEffects;
    let metadata: EffectsMetadata<OutputCategoryEffects>;
    let service: OutputCategoryService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: { } };
    let mockDatasetSelectorSelectDatasetNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                OutputCategoryEffects,
                { provide: OutputCategoryService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }},
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(OutputCategoryEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(OutputCategoryService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute,''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadOutputCategories$ effect', () => {
        it('should dispatch the loadOutputCategoryListSuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = outputCategoryActions.loadOutputCategoryList();
            const outcome = outputCategoryActions.loadOutputCategoryListSuccess({ outputCategories: CATEGORY_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CATEGORY_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveOutputCategoryList = jest.fn(() => response);

            expect(effects.loadOutputCategories$).toBeObservable(expected);
            expect(service.retrieveOutputCategoryList).toHaveBeenCalledWith('myDataset');
        });

        it('should dispatch the loadOutputCategoryListFail action on HTTP failure', () => {
            const action = outputCategoryActions.loadOutputCategoryList();
            const error = new Error();
            const outcome = outputCategoryActions.loadOutputCategoryListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveOutputCategoryList = jest.fn(() => response);

            expect(effects.loadOutputCategories$).toBeObservable(expected);
        });
    });

    describe('addOutputCategory$ effect', () => {
        it('should dispatch the addOutputCategorySuccess action on success', () => {
            const action = outputCategoryActions.addOutputCategory({ outputCategory: CATEGORY });
            const outcome = outputCategoryActions.addOutputCategorySuccess({ outputCategory: CATEGORY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CATEGORY });
            const expected = cold('--b', { b: outcome });
            service.addOutputCategory = jest.fn(() => response);

            expect(effects.addOutputCategory$).toBeObservable(expected);
        });

        it('should dispatch the addOutputCategoryFail action on HTTP failure', () => {
            const action = outputCategoryActions.addOutputCategory({ outputCategory: CATEGORY });
            const error = new Error();
            const outcome = outputCategoryActions.addOutputCategoryFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.addOutputCategory = jest.fn(() => response);

            expect(effects.addOutputCategory$).toBeObservable(expected);
        });
    });

    describe('addOutputCategorySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addOutputCategorySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = outputCategoryActions.addOutputCategorySuccess({ outputCategory: CATEGORY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addOutputCategorySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Output category successfully added',
                'The new output category was added into the database'
            );
        });
    });

    describe('addOutputCategoryFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addOutputCategoryFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = outputCategoryActions.addOutputCategoryFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addOutputCategoryFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add output category',
                'The new output category could not be added into the database'
            );
        });
    });

    describe('editOutputCategory$ effect', () => {
        it('should dispatch the editOutputCategorySuccess action on success', () => {
            const action = outputCategoryActions.editOutputCategory({ outputCategory: CATEGORY });
            const outcome = outputCategoryActions.editOutputCategorySuccess({ outputCategory: CATEGORY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CATEGORY });
            const expected = cold('--b', { b: outcome });
            service.editOutputCategory = jest.fn(() => response);

            expect(effects.editOutputCategory$).toBeObservable(expected);
        });

        it('should dispatch the editOutputCategoryFail action on HTTP failure', () => {
            const action = outputCategoryActions.editOutputCategory({ outputCategory: CATEGORY });
            const error = new Error();
            const outcome = outputCategoryActions.editOutputCategoryFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.editOutputCategory = jest.fn(() => response);

            expect(effects.editOutputCategory$).toBeObservable(expected);
        });
    });

    describe('editOutputCategorySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editOutputCategorySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = outputCategoryActions.editOutputCategorySuccess({ outputCategory: CATEGORY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editOutputCategorySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Output category successfully edited',
                'The existing output category has been edited into the database'
            );
        });
    });

    describe('editOutputCategoryFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editOutputCategoryFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = outputCategoryActions.editOutputCategoryFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editOutputCategoryFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit output category',
                'The existing output category could not be edited into the database'
            );
        });
    });

    describe('deleteOutputCategory$ effect', () => {
        it('should dispatch the deleteOutputCategorySuccess action on success', () => {
            const action = outputCategoryActions.deleteOutputCategory({ outputCategory: CATEGORY });
            const outcome = outputCategoryActions.deleteOutputCategorySuccess({ outputCategory: CATEGORY });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: CATEGORY });
            const expected = cold('--b', { b: outcome });
            service.deleteOutputCategory = jest.fn(() => response);

            expect(effects.deleteOutputCategory$).toBeObservable(expected);
        });

        it('should dispatch the deleteOutputFamilyFail action on HTTP failure', () => {
            const action = outputCategoryActions.deleteOutputCategory({ outputCategory: CATEGORY });
            const error = new Error();
            const outcome = outputCategoryActions.deleteOutputCategoryFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.deleteOutputCategory = jest.fn(() => response);

            expect(effects.deleteOutputCategory$).toBeObservable(expected);
        });
    });

    describe('deleteOutputCategorySuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteOutputCategorySuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const spy = jest.spyOn(toastr, 'success');
            const action = outputCategoryActions.deleteOutputCategorySuccess({ outputCategory: CATEGORY });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteOutputCategorySuccess$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Output category successfully deleted',
                'The existing output category has been deleted'
            );
        });
    });

    describe('deleteOutputCategoryFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteOutputCategoryFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = outputCategoryActions.deleteOutputCategoryFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteOutputCategoryFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete output category',
                'The existing output category could not be deleted from the database'
            );
        });
    });
});
