/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatabaseEffects } from './database.effects';
import { InstanceEffects } from './instance.effects';
import { DatasetFamilyEffects } from './dataset-family.effects';
import { DatasetEffects } from './dataset.effects';
import { AttributeEffects } from './attribute.effects';
import { DatasetGroupEffects } from './dataset-group.effects';
import { InstanceGroupEffects } from './instance-group.effects';
import { CriteriaFamilyEffects } from './criteria-family.effects';
import { OutputCategoryEffects } from './output-category.effects';
import { OutputFamilyEffects } from './output-family.effects';
import { ImageEffects } from './image.effects';
import { FileEffects } from './file.effects';
import { ConeSearchConfigEffects } from './cone-search-config.effects'
import { DetailConfigEffects } from './detail-config.effects';
import { AliasConfigEffects } from './alias-config.effects';
import { WebpageFamilyEffects } from './webpage-family.effects';
import { WebpageEffects } from './webpage.effects';

export const metamodelEffects = [
    DatabaseEffects,
    InstanceEffects,
    DatasetFamilyEffects,
    DatasetEffects,
    AttributeEffects,
    DatasetGroupEffects,
    InstanceGroupEffects,
    CriteriaFamilyEffects,
    OutputCategoryEffects,
    OutputFamilyEffects,
    ImageEffects,
    FileEffects,
    ConeSearchConfigEffects,
    DetailConfigEffects,
    AliasConfigEffects,
    WebpageFamilyEffects,
    WebpageEffects
];
