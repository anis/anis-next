/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as imageActions from '../actions/image.actions';
import { ImageService } from '../services/image.service';
import * as datasetSelector from '../selectors/dataset.selector';

/**
 * @class
 * @classdesc Image effects.
 */
@Injectable()
export class ImageEffects {
    /**
     * Calls action to retrieve image list.
     */
    loadImages$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(imageActions.loadImageList),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.imageService.retrieveImageList(datasetName)
                .pipe(
                    map(images => imageActions.loadImageListSuccess({ images })),
                    catchError(() => of(imageActions.loadImageListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add an image.
     */
    addImage$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(imageActions.addImage),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.imageService.addImage(datasetName, action.image)
                .pipe(
                    map(image => imageActions.addImageSuccess({ image })),
                    catchError(() => of(imageActions.addImageFail()))
                )
            )
        )
    );

    /**
     * Displays add image success notification.
     */
    addImageSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(imageActions.addImageSuccess),
            tap(() => this.toastr.success('Image successfully added', 'The new image was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add image error notification.
     */
    addImageFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(imageActions.addImageFail),
            tap(() => this.toastr.error('Failure to add image', 'The new image could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an image.
     */
    editImage$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(imageActions.editImage),
            mergeMap(action => this.imageService.editImage(action.image)
                .pipe(
                    map(image => imageActions.editImageSuccess({ image })),
                    catchError(() => of(imageActions.editImageFail()))
                )
            )
        )
    );

    /**
     * Displays edit image success notification.
     */
    editImageSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(imageActions.editImageSuccess),
            tap(() => this.toastr.success('Image successfully edited', 'The existing image has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit image error notification.
     */
    editImageFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(imageActions.editImageFail),
            tap(() => this.toastr.error('Failure to edit image', 'The existing image could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove an image.
     */
    deleteImage$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(imageActions.deleteImage),
            mergeMap(action => this.imageService.deleteImage(action.image.id)
                .pipe(
                    map(() => imageActions.deleteImageSuccess({ image: action.image })),
                    catchError(() => of(imageActions.deleteImageFail()))
                )
            )
        )
    );

    /**
     * Displays delete image success notification.
     */
    deleteImageSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(imageActions.deleteImageSuccess),
            tap(() => this.toastr.success('Image successfully deleted', 'The existing image has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete image error notification.
     */
    deleteImageFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(imageActions.deleteImageFail),
            tap(() => this.toastr.error('Failure to delete image', 'The existing image could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private imageService: ImageService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
