/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { DatabaseEffects } from './database.effects';
import { DatabaseService } from '../services/database.service';
import * as databaseActions from '../actions/database.actions';
import { DATABASE, DATABASE_LIST } from '../../../test-data';

describe('[Metamodel][Effects] DatabaseEffects', () => {
    let actions = new Observable();
    let effects: DatabaseEffects;
    let metadata: EffectsMetadata<DatabaseEffects>;
    let service: DatabaseService;
    let toastr: ToastrService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DatabaseEffects,
                { provide: DatabaseService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }},
                provideMockActions(() => actions)
            ]
        }).compileComponents();
        effects = TestBed.inject(DatabaseEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(DatabaseService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDatabases$ effect', () => {
        it('should dispatch the loadDatabaseListSuccess action on success', () => {
            const action = databaseActions.loadDatabaseList();
            const outcome = databaseActions.loadDatabaseListSuccess({ databases: DATABASE_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATABASE_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveDatabaseList = jest.fn(() => response);

            expect(effects.loadDatabases$).toBeObservable(expected);
        });

        it('should dispatch the loadDatabaseListFail action on HTTP failure', () => {
            const action = databaseActions.loadDatabaseList();
            const error = new Error();
            const outcome = databaseActions.loadDatabaseListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveDatabaseList = jest.fn(() => response);

            expect(effects.loadDatabases$).toBeObservable(expected);
        });
    });

    describe('addDatabase$ effect', () => {
        it('should dispatch the addDatabaseSuccess action on success', () => {
            const action = databaseActions.addDatabase({ database: DATABASE });
            const outcome = databaseActions.addDatabaseSuccess({ database: DATABASE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATABASE });
            const expected = cold('--b', { b: outcome });
            service.addDatabase = jest.fn(() => response);

            expect(effects.addDatabase$).toBeObservable(expected);
        });

        it('should dispatch the addDatabaseFail action on HTTP failure', () => {
            const action = databaseActions.addDatabase({ database: DATABASE });
            const error = new Error();
            const outcome = databaseActions.addDatabaseFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.addDatabase = jest.fn(() => response);

            expect(effects.addDatabase$).toBeObservable(expected);
        });
    });

    describe('addDatabaseSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addDatabaseSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const routerSpy = jest.spyOn(router, 'navigate');
            const action = databaseActions.addDatabaseSuccess({ database: DATABASE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addDatabaseSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Database successfully added',
                'The new database was added into the database'
            );
            expect(routerSpy).toHaveBeenCalledTimes(1);
            expect(routerSpy).toHaveBeenCalledWith(['/admin/database/database-list']);
        });
    });

    describe('addDatabaseFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.addDatabaseFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = databaseActions.addDatabaseFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addDatabaseFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add database',
                'The new database could not be added into the database'
            );
        });
    });

    describe('editDatabase$ effect', () => {
        it('should dispatch the editDatabaseSuccess action on success', () => {
            const action = databaseActions.editDatabase({ database: DATABASE });
            const outcome = databaseActions.editDatabaseSuccess({ database: DATABASE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATABASE });
            const expected = cold('--b', { b: outcome });
            service.editDatabase = jest.fn(() => response);

            expect(effects.editDatabase$).toBeObservable(expected);
        });

        it('should dispatch the editDatabaseFail action on HTTP failure', () => {
            const action = databaseActions.editDatabase({ database: DATABASE });
            const error = new Error();
            const outcome = databaseActions.editDatabaseFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.editDatabase = jest.fn(() => response);

            expect(effects.editDatabase$).toBeObservable(expected);
        });
    });

    describe('editDatabaseSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editDatabaseSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const routerSpy = jest.spyOn(router, 'navigate');
            const action = databaseActions.editDatabaseSuccess({ database: DATABASE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editDatabaseSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Database successfully edited',
                'The existing database has been edited into the database'
            );
            expect(routerSpy).toHaveBeenCalledTimes(1);
            expect(routerSpy).toHaveBeenCalledWith(['/admin/database/database-list']);
        });
    });

    describe('editDatabaseFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.editDatabaseFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = databaseActions.editDatabaseFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editDatabaseFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit database',
                'The existing database could not be edited into the database'
            );
        });
    });

    describe('deleteDatabase$ effect', () => {
        it('should dispatch the deleteDatabaseSuccess action on success', () => {
            const action = databaseActions.deleteDatabase({ database: DATABASE });
            const outcome = databaseActions.deleteDatabaseSuccess({ database: DATABASE });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATABASE });
            const expected = cold('--b', { b: outcome });
            service.deleteDatabase = jest.fn(() => response);

            expect(effects.deleteDatabase$).toBeObservable(expected);
        });

        it('should dispatch the deleteDatabaseFail action on HTTP failure', () => {
            const action = databaseActions.deleteDatabase({ database: DATABASE });
            const error = new Error();
            const outcome = databaseActions.deleteDatabaseFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.deleteDatabase = jest.fn(() => response);

            expect(effects.deleteDatabase$).toBeObservable(expected);
        });
    });

    describe('deleteDatabaseSuccess$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteDatabaseSuccess$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = databaseActions.deleteDatabaseSuccess({ database: DATABASE });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatabaseSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Database successfully deleted',
                'The existing database has been deleted'
            );
        });
    });

    describe('deleteDatabaseFail$ effect', () => {
        it('should not dispatch', () => {
            expect(metadata.deleteDatabaseFail$).toEqual(
                expect.objectContaining({ dispatch: false })
            );
        });

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = databaseActions.deleteDatabaseFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatabaseFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete database',
                'The existing database could not be deleted from the database'
            );
        });
    });
});
