/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as attributeActions from '../actions/attribute.actions';
import { AttributeService } from '../services/attribute.service';
import * as datasetSelector from '../selectors/dataset.selector';


/**
 * @class
 * @classdesc Attribute effects.
 */
@Injectable()
export class AttributeEffects {

    /**
     * Calls action to retrieve attribute list.
     */
    loadAttributes$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(attributeActions.loadAttributeList),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([, datasetName]) => this.attributeService.retrieveAttributeList(datasetName)
                .pipe(
                    map(attributes => attributeActions.loadAttributeListSuccess({ attributes })),
                    catchError(() => of(attributeActions.loadAttributeListFail()))
                )
            )
        )
    );

    addAttributeList$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(attributeActions.addAttributeList),
            tap(action => {
                for (const attribute of action.attributeList) {
                    this.store.dispatch(attributeActions.addAttribute({ attribute }))
                }
            })
        ), { dispatch: false }
    );

    /**
     * Calls action to add an attribute.
     */
    addAttribute$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(attributeActions.addAttribute),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.attributeService.addAttribute(datasetName, action.attribute)
                .pipe(
                    map(attribute => attributeActions.addAttributeSuccess({ attribute })),
                    catchError(() => of(attributeActions.addAttributeFail()))
                )
            )
        )
    );

    /**
     * Displays add attribute success notification.
     */
    addAttributeSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(attributeActions.addAttributeSuccess),
            tap(() => {
                this.toastr.success('Attribute successfully added', 'The new attribute was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add attribute error notification.
     */
    addAttributeFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(attributeActions.addAttributeFail),
            tap(() => this.toastr.error('Failure to add attribute', 'The new attribute could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an attribute.
     */
    editAttribute$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(attributeActions.editAttribute),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.attributeService.editAttribute(datasetName, action.attribute)
                .pipe(
                    map(attribute => attributeActions.editAttributeSuccess({ attribute })),
                    catchError(() => of(attributeActions.editAttributeFail()))
                )
            )
        )
    );

    /**
     * Displays edit attribute error notification.
     */
    editAttributeFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(attributeActions.editAttributeFail),
            tap(() => this.toastr.error('Failure to edit attribute', 'The existing attribute could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove an attribute.
     */
    deleteAttribute$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(attributeActions.deleteAttribute),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.attributeService.deleteAttribute(datasetName, action.attribute)
                .pipe(
                    map(() => attributeActions.deleteAttributeSuccess({ attribute: action.attribute })),
                    catchError(() => of(attributeActions.deleteAttributeFail()))
                )
            )
        )
    );

    /**
     * Displays delete attribute success notification.
     */
    deleteAttributeSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(attributeActions.deleteAttributeSuccess),
            tap(() => this.toastr.success('Attribute successfully deleted', 'The existing attribute has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete attribute error notification.
     */
    deleteAttributeFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(attributeActions.deleteAttributeFail),
            tap(() => this.toastr.error('Failure to delete attribute', 'The existing attribute could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private attributeService: AttributeService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
