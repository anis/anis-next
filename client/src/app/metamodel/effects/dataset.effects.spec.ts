/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { DatasetEffects } from './dataset.effects';
import { DatasetService } from '../services/dataset.service';
import * as datasetActions from '../actions/dataset.actions';
import * as instanceSelector from '../selectors/instance.selector';
import { DATASET, DATASET_LIST } from '../../../test-data';
import { Dataset } from '../models';

describe('[Metamodel][Effects] DatasetEffects', () => {
    let actions = new Observable();
    let effects: DatasetEffects;
    let metadata: EffectsMetadata<DatasetEffects>;
    let service: DatasetService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DatasetEffects,
                { provide: DatasetService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(DatasetEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(DatasetService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute, ''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDatasets$ effect', () => {
        it('should dispatch the loadDatasetListSuccess action on success', () => {
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'myInstance'
            );

            const action = datasetActions.loadDatasetList();
            const outcome = datasetActions.loadDatasetListSuccess({ datasets: DATASET_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveDatasetList = jest.fn(() => response);

            expect(effects.loadDatasets$).toBeObservable(expected);
            expect(service.retrieveDatasetList).toHaveBeenCalledWith('myInstance');
        });

        it('should dispatch the loadDatasetListFail action on HTTP failure', () => {
            const action = datasetActions.loadDatasetList();
            const error = new Error();
            const outcome = datasetActions.loadDatasetListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveDatasetList = jest.fn(() => response);

            expect(effects.loadDatasets$).toBeObservable(expected);
        });
    });

    describe('addDataset$ effect', () => {
        it('should dispatch the addDatasetSuccess action on success', () => {
            const action = datasetActions.addDataset({ dataset: DATASET });
            const outcome = datasetActions.addDatasetSuccess({ dataset: DATASET });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET });
            const expected = cold('--b', { b: outcome });
            service.addDataset = jest.fn(() => response);

            expect(effects.addDataset$).toBeObservable(expected);
        });

        it('should dispatch the addDatasetFail action on HTTP failure', () => {
            const action = datasetActions.addDataset({ dataset: DATASET });
            const error = new Error();
            const outcome = datasetActions.addDatasetFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.addDataset = jest.fn(() => response);

            expect(effects.addDataset$).toBeObservable(expected);
        });
    });

    describe('addDatasetSuccess$ effect', () => {
        it('should display success notification and call router.naviteByUrl', () => {
            let spyOnRouterNavigate = jest.spyOn(router, 'navigate');
            let dataset: Dataset;
            let spyOnToast = jest.spyOn(toastr, 'success');
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'myInstance'
            );
            actions = hot('a', { a: datasetActions.addDatasetSuccess({ dataset: { ...dataset, name: 'test' } }) });
            let expected = cold('b', { b: [datasetActions.addDatasetSuccess({ dataset: { ...dataset, name: 'test' } }), 'myInstance'] });
            expect(effects.addDatasetSuccess$).toBeObservable(expected);
            expect(spyOnToast).toHaveBeenCalledWith('Dataset successfully added', 'The new dataset was added into the database');
            expect(spyOnRouterNavigate).toHaveBeenCalledWith(['/admin/instance/configure-instance/myInstance']);

        });
    });

    describe('addDatasetFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetActions.addDatasetFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addDatasetFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add dataset',
                'The new dataset could not be added into the database'
            );
        });
    });

    describe('editDataset$ effect', () => {
        it('should dispatch the editDatasetSuccess action on success', () => {
            const action = datasetActions.editDataset({ dataset: DATASET });
            const outcome = datasetActions.editDatasetSuccess({ dataset: DATASET });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET });
            const expected = cold('--b', { b: outcome });
            service.editDataset = jest.fn(() => response);

            expect(effects.editDataset$).toBeObservable(expected);
        });

        it('should dispatch the editDatasetFail action on HTTP failure', () => {
            const action = datasetActions.editDataset({ dataset: DATASET });
            const error = new Error();
            const outcome = datasetActions.editDatasetFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.editDataset = jest.fn(() => response);

            expect(effects.editDataset$).toBeObservable(expected);
        });
    });

    describe('editDatasetSuccess$ effect', () => {
        it('should display success notification and call router.naviteByUrl', () => {
            let spyOnRouterNavigate = jest.spyOn(router, 'navigate');
            let dataset: Dataset;
            let spyOnToast = jest.spyOn(toastr, 'success');
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'myInstance'
            );
            actions = hot('a', { a: datasetActions.editDatasetSuccess({ dataset: { ...dataset, name: 'test' } }) });
            let expected = cold('b', { b: [datasetActions.editDatasetSuccess({ dataset: { ...dataset, name: 'test' } }), 'myInstance'] });
            expect(effects.editDatasetSuccess$).toBeObservable(expected);
            expect(spyOnToast).toHaveBeenCalledWith('Dataset successfully edited', 'The existing dataset has been edited into the database');
            expect(spyOnRouterNavigate).toHaveBeenCalledWith([`/admin/instance/configure-instance/myInstance`]);

        });
    });

    describe('editDatasetFail$ effect', () => {

        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetActions.editDatasetFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editDatasetFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit dataset',
                'The existing dataset could not be edited into the database'
            );
        });
    });

    describe('deleteDataset$ effect', () => {
        it('should dispatch the deleteDatasetSuccess action on success', () => {
            const action = datasetActions.deleteDataset({ dataset: DATASET });
            const outcome = datasetActions.deleteDatasetSuccess({ dataset: DATASET });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: DATASET });
            const expected = cold('--b', { b: outcome });
            service.deleteDataset = jest.fn(() => response);

            expect(effects.deleteDataset$).toBeObservable(expected);
        });

        it('should dispatch the deleteDatasetFail action on HTTP failure', () => {
            const action = datasetActions.deleteDataset({ dataset: DATASET });
            const error = new Error();
            const outcome = datasetActions.deleteDatasetFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            service.deleteDataset = jest.fn(() => response);

            expect(effects.deleteDataset$).toBeObservable(expected);
        });
    });

    describe('deleteDatasetSuccess$ effect', () => {
        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = datasetActions.deleteDatasetSuccess({ dataset: DATASET });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatasetSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Dataset successfully deleted',
                'The existing dataset has been deleted'
            );
        });
    });

    describe('deleteDatasetFail$ effect', () => {
        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = datasetActions.deleteDatasetFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteDatasetFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete dataset',
                'The existing dataset could not be deleted from the database'
            );
        });
    });
});
