/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { CriteriaFamily } from '../models';
import * as criteriaFamilyActions from '../actions/criteria-family.actions';

/**
 * Interface for criteria family state.
 *
 * @interface State
 */
export interface State extends EntityState<CriteriaFamily> {
    criteriaFamilyListIsLoading: boolean;
    criteriaFamilyListIsLoaded: boolean;
}

export const adapter: EntityAdapter<CriteriaFamily> = createEntityAdapter<CriteriaFamily>({
    selectId: (criteriaFamily: CriteriaFamily) => criteriaFamily.id,
    sortComparer: (a: CriteriaFamily, b: CriteriaFamily) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    criteriaFamilyListIsLoading: false,
    criteriaFamilyListIsLoaded: false
});

export const criteriaFamilyReducer = createReducer(
    initialState,
    on(criteriaFamilyActions.loadCriteriaFamilyList, (state) => {
        return {
            ...state,
            criteriaFamilyListIsLoading: true
        }
    }),
    on(criteriaFamilyActions.loadCriteriaFamilyListSuccess, (state, { criteriaFamilies }) => {
        return adapter.setAll(
            criteriaFamilies,
            {
                ...state,
                criteriaFamilyListIsLoading: false,
                criteriaFamilyListIsLoaded: true
            }
        );
    }),
    on(criteriaFamilyActions.loadCriteriaFamilyListFail, (state) => {
        return {
            ...state,
            criteriaFamilyListIsLoading: false
        }
    }),
    on(criteriaFamilyActions.addCriteriaFamilySuccess, (state, { criteriaFamily }) => {
        return adapter.addOne(criteriaFamily, state)
    }),
    on(criteriaFamilyActions.editCriteriaFamilySuccess, (state, { criteriaFamily }) => {
        return adapter.setOne(criteriaFamily, state)
    }),
    on(criteriaFamilyActions.deleteCriteriaFamilySuccess, (state, { criteriaFamily }) => {
        return adapter.removeOne(criteriaFamily.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectCriteriaFamilyIds = selectIds;
export const selectCriteriaFamilyEntities = selectEntities;
export const selectAllCriteriaFamilies = selectAll;
export const selectCriteriaFamilyTotal = selectTotal;

export const selectCriteriaFamilyListIsLoading = (state: State) => state.criteriaFamilyListIsLoading;
export const selectCriteriaFamilyListIsLoaded = (state: State) => state.criteriaFamilyListIsLoaded;
