/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDatabase from './database.reducer';
import * as databaseActions from '../actions/database.actions';
import { DATABASE, DATABASE_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] Database reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromDatabase;
        const action = { type: 'Unknown' };
        const state = fromDatabase.databaseReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadDatabaseList action should set databaseListIsLoading to true', () => {
        const { initialState } = fromDatabase;
        const action = databaseActions.loadDatabaseList();
        const state = fromDatabase.databaseReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.databaseListIsLoading).toEqual(true);
        expect(state.databaseListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadDatabaseListSuccess action should add database list, set databaseListIsLoading to false and set databaseListIsLoaded to true', () => {
        const { initialState } = fromDatabase;
        const action = databaseActions.loadDatabaseListSuccess({ databases: DATABASE_LIST });
        const state = fromDatabase.databaseReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(1);
        expect(state.ids).toContain(2);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.databaseListIsLoading).toEqual(false);
        expect(state.databaseListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadDatabaseListFail action should set databaseListIsLoading to false', () => {
        const { initialState } = fromDatabase;
        const action = databaseActions.loadDatabaseListFail();
        const state = fromDatabase.databaseReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.databaseListIsLoading).toEqual(false);
        expect(state.databaseListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addDatabaseSuccess action should add a database', () => {
        const { initialState } = fromDatabase;
        const action = databaseActions.addDatabaseSuccess({ database: DATABASE });
        const state = fromDatabase.databaseReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.databaseListIsLoading).toEqual(false);
        expect(state.databaseListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editDatabaseSuccess action should modify a database', () => {
        const initialState = {
            ...fromDatabase.initialState,
            ids: [1],
            entities: { 1: { ...DATABASE, label: 'label' }}
        };
        const action = databaseActions.editDatabaseSuccess({ database: DATABASE });
        const state = fromDatabase.databaseReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[1]).toEqual(DATABASE);
        expect(state.databaseListIsLoading).toEqual(false);
        expect(state.databaseListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteDatabaseSuccess action should modify a database', () => {
        const initialState = {
            ...fromDatabase.initialState,
            ids: [1],
            entities: { 1: DATABASE }
        };
        const action = databaseActions.deleteDatabaseSuccess({ database: DATABASE });
        const state = fromDatabase.databaseReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.databaseListIsLoading).toEqual(false);
        expect(state.databaseListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get databaseListIsLoading', () => {
        const action = {} as Action;
        const state =  fromDatabase.databaseReducer(undefined, action);

        expect(fromDatabase.selectDatabaseListIsLoading(state)).toEqual(false);
    });

    it('should get databaseListIsLoaded', () => {
        const action = {} as Action;
        const state = fromDatabase.databaseReducer(undefined, action);

        expect(fromDatabase.selectDatabaseListIsLoaded(state)).toEqual(false);
    });
});
