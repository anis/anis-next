/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { InstanceGroup } from '../models';
import * as instanceGroupActions from '../actions/instance-group.actions';

/**
 * Interface for instanceGroup state.
 *
 * @interface State
 */
export interface State extends EntityState<InstanceGroup> {
    instanceGroupListIsLoading: boolean;
    instanceGroupListIsLoaded: boolean;
}

export const adapter: EntityAdapter<InstanceGroup> = createEntityAdapter<InstanceGroup>();

export const initialState: State = adapter.getInitialState({
    instanceGroupListIsLoading: false,
    instanceGroupListIsLoaded: false
});

export const instanceGroupReducer = createReducer(
    initialState,
    on(instanceGroupActions.loadInstanceGroupList, (state) => {
        return {
            ...state,
            instanceGroupListIsLoading: true
        }
    }),
    on(instanceGroupActions.loadInstanceGroupListSuccess, (state, { instanceGroups }) => {
        return adapter.setAll(
            instanceGroups,
            {
                ...state,
                instanceGroupListIsLoading: false,
                instanceGroupListIsLoaded: true
            }
        );
    }),
    on(instanceGroupActions.loadInstanceGroupListFail, (state) => {
        return {
            ...state,
            instanceGroupListIsLoading: false
        }
    }),
    on(instanceGroupActions.addInstanceGroupSuccess, (state, { instanceGroup }) => {
        return adapter.addOne(instanceGroup, state)
    }),
    on(instanceGroupActions.editInstanceGroupSuccess, (state, { instanceGroup }) => {
        return adapter.setOne(instanceGroup, state)
    }),
    on(instanceGroupActions.deleteInstanceGroupSuccess, (state, { instanceGroup }) => {
        return adapter.removeOne(instanceGroup.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();


export const selectInstanceGroupIds = selectIds;
export const selectInstanceGroupEntities = selectEntities;
export const selectAllInstanceGroups = selectAll;
export const selectInstanceGroupTotal = selectTotal;

export const selectInstanceGroupListIsLoading = (state: State) => state.instanceGroupListIsLoading;
export const selectInstanceGroupListIsLoaded = (state: State) => state.instanceGroupListIsLoaded;
