/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDatasetFamily from './dataset-family.reducer';
import * as datasetFamilyActions from '../actions/dataset-family.actions';
import { DATASET_FAMILY, DATASET_FAMILY_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] DatasetFamily reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromDatasetFamily;
        const action = { type: 'Unknown' };
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadDatasetFamilyList action should set datasetFamilyListIsLoading to true', () => {
        const { initialState } = fromDatasetFamily;
        const action = datasetFamilyActions.loadDatasetFamilyList();
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetFamilyListIsLoading).toEqual(true);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadDatasetFamilyListSuccess action should add datasetFamily list, set datasetFamilyListIsLoading to false and set datasetFamilyListIsLoaded to true', () => {
        const { initialState } = fromDatasetFamily;
        const action = datasetFamilyActions.loadDatasetFamilyListSuccess({ datasetFamilies: DATASET_FAMILY_LIST });
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(1);
        expect(state.ids).toContain(2);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadDatasetFamilyListFail action should set datasetFamilyListIsLoading to false', () => {
        const { initialState } = fromDatasetFamily;
        const action = datasetFamilyActions.loadDatasetFamilyListFail();
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addDatasetFamilySuccess action should add a datasetFamily', () => {
        const { initialState } = fromDatasetFamily;
        const action = datasetFamilyActions.addDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editDatasetFamilySuccess action should modify a datasetFamily', () => {
        const initialState = {
            ...fromDatasetFamily.initialState,
            ids: [1],
            entities: { 1: { ...DATASET_FAMILY, label: 'label' }}
        };
        const action = datasetFamilyActions.editDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[1]).toEqual(DATASET_FAMILY);
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteDatasetFamilySuccess action should modify a datasetFamily', () => {
        const initialState = {
            ...fromDatasetFamily.initialState,
            ids: [1],
            entities: { 1: DATASET_FAMILY }
        };
        const action = datasetFamilyActions.deleteDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get datasetFamilyListIsLoading', () => {
        const action = {} as Action;
        const state =  fromDatasetFamily.datasetFamilyReducer(undefined, action);

        expect(fromDatasetFamily.selectDatasetFamilyListIsLoading(state)).toEqual(false);
    });

    it('should get datasetFamilyListIsLoaded', () => {
        const action = {} as Action;
        const state = fromDatasetFamily.datasetFamilyReducer(undefined, action);

        expect(fromDatasetFamily.selectDatasetFamilyListIsLoaded(state)).toEqual(false);
    });
});
