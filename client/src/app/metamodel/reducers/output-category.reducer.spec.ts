/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromOutputCategory from './output-category.reducer';
import * as outputCategoryActions from '../actions/output-category.actions';
import { CATEGORY, CATEGORY_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] OutputCategory reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromOutputCategory;
        const action = { type: 'Unknown' };
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadOutputCategoryList action should set outputCategoryListIsLoading to true', () => {
        const { initialState } = fromOutputCategory;
        const action = outputCategoryActions.loadOutputCategoryList();
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputCategoryListIsLoading).toEqual(true);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadOutputCategoryListSuccess action should add outputCategory list, set outputCategoryListIsLoading to false and set outputCategoryListIsLoaded to true', () => {
        const { initialState } = fromOutputCategory;
        const action = outputCategoryActions.loadOutputCategoryListSuccess({ outputCategories: CATEGORY_LIST });
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);
        expect(state.ids.length).toEqual(3);
        expect(state.ids).toContain(1);
        expect(state.ids).toContain(2);
        expect(state.ids).toContain(3);
        expect(Object.keys(state.entities).length).toEqual(3);
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadOutputCategoryListFail action should set outputCategoryListIsLoading to false', () => {
        const { initialState } = fromOutputCategory;
        const action = outputCategoryActions.loadOutputCategoryListFail();
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addOutputCategorySuccess action should add a outputCategory', () => {
        const { initialState } = fromOutputCategory;
        const action = outputCategoryActions.addOutputCategorySuccess({ outputCategory: CATEGORY });
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editOutputCategorySuccess action should modify a outputCategory', () => {
        const initialState = {
            ...fromOutputCategory.initialState,
            ids: [1],
            entities: { 1: { ...CATEGORY, label: 'label' }}
        };
        const action = outputCategoryActions.editOutputCategorySuccess({ outputCategory: CATEGORY });
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[1]).toEqual(CATEGORY);
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteOutputCategorySuccess action should modify a outputCategory', () => {
        const initialState = {
            ...fromOutputCategory.initialState,
            ids: [1],
            entities: { 1: CATEGORY }
        };
        const action = outputCategoryActions.deleteOutputCategorySuccess({ outputCategory: CATEGORY });
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get outputCategoryListIsLoading', () => {
        const action = {} as Action;
        const state =  fromOutputCategory.outputCategoryReducer(undefined, action);

        expect(fromOutputCategory.selectOutputCategoryListIsLoading(state)).toEqual(false);
    });

    it('should get outputCategoryListIsLoaded', () => {
        const action = {} as Action;
        const state = fromOutputCategory.outputCategoryReducer(undefined, action);

        expect(fromOutputCategory.selectOutputCategoryListIsLoaded(state)).toEqual(false);
    });
});
