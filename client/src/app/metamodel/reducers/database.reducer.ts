/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Database } from '../models';
import * as databaseActions from '../actions/database.actions';

/**
 * Interface for database state.
 *
 * @interface State
 */
export interface State extends EntityState<Database> {
    databaseListIsLoading: boolean;
    databaseListIsLoaded: boolean;
}

export const adapter: EntityAdapter<Database> = createEntityAdapter<Database>();

export const initialState: State = adapter.getInitialState({
    databaseListIsLoading: false,
    databaseListIsLoaded: false
});

export const databaseReducer = createReducer(
    initialState,
    on(databaseActions.loadDatabaseList, (state) => {
        return {
            ...state,
            databaseListIsLoading: true
        }
    }),
    on(databaseActions.loadDatabaseListSuccess, (state, { databases }) => {
        return adapter.setAll(
            databases,
            {
                ...state,
                databaseListIsLoading: false,
                databaseListIsLoaded: true
            }
        );
    }),
    on(databaseActions.loadDatabaseListFail, (state) => {
        return {
            ...state,
            databaseListIsLoading: false
        }
    }),
    on(databaseActions.addDatabaseSuccess, (state, { database }) => {
        return adapter.addOne(database, state)
    }),
    on(databaseActions.editDatabaseSuccess, (state, { database }) => {
        return adapter.setOne(database, state)
    }),
    on(databaseActions.deleteDatabaseSuccess, (state, { database }) => {
        return adapter.removeOne(database.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectDatabaseIds = selectIds;
export const selectDatabaseEntities = selectEntities;
export const selectAllDatabases = selectAll;
export const selectDatabaseTotal = selectTotal;

export const selectDatabaseListIsLoading = (state: State) => state.databaseListIsLoading;
export const selectDatabaseListIsLoaded = (state: State) => state.databaseListIsLoaded;
