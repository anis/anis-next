/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromAttribute from './attribute.reducer';
import * as attributeActions from '../actions/attribute.actions';
import { ATTRIBUTE, ATTRIBUTE_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] Attribute reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromAttribute;
        const action = { type: 'Unknown' };
        const state = fromAttribute.attributeReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadAttributeList action should set attributeListIsLoading to true', () => {
        const { initialState } = fromAttribute;
        const action = attributeActions.loadAttributeList();
        const state = fromAttribute.attributeReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.attributeListIsLoading).toEqual(true);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadAttributeListSuccess action should add attribute list, set attributeListIsLoading to false and set attributeListIsLoaded to true', () => {
        const { initialState } = fromAttribute;
        const action = attributeActions.loadAttributeListSuccess({ attributes: ATTRIBUTE_LIST });
        const state = fromAttribute.attributeReducer(initialState, action);
        expect(state.ids.length).toEqual(4);
        expect(state.ids).toContain(1);
        expect(state.ids).toContain(2);
        expect(state.ids).toContain(3);
        expect(state.ids).toContain(4);
        expect(Object.keys(state.entities).length).toEqual(4);
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadAttributeListFail action should set attributeListIsLoading to false', () => {
        const { initialState } = fromAttribute;
        const action = attributeActions.loadAttributeListFail();
        const state = fromAttribute.attributeReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addAttributeSuccess action should add a attribute', () => {
        const { initialState } = fromAttribute;
        const action = attributeActions.addAttributeSuccess({ attribute: ATTRIBUTE });
        const state = fromAttribute.attributeReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editAttributeSuccess action should modify a attribute', () => {
        const initialState = {
            ...fromAttribute.initialState,
            ids: [1],
            entities: { 1: { ...ATTRIBUTE, label: 'label' }}
        };
        const action = attributeActions.editAttributeSuccess({ attribute: ATTRIBUTE });
        const state = fromAttribute.attributeReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[1]).toEqual(ATTRIBUTE);
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteAttributeSuccess action should modify a attribute', () => {
        const initialState = {
            ...fromAttribute.initialState,
            ids: [1],
            entities: { 1: ATTRIBUTE }
        };
        const action = attributeActions.deleteAttributeSuccess({ attribute: ATTRIBUTE });
        const state = fromAttribute.attributeReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get attributeListIsLoading', () => {
        const action = {} as Action;
        const state =  fromAttribute.attributeReducer(undefined, action);

        expect(fromAttribute.selectAttributeListIsLoading(state)).toEqual(false);
    });

    it('should get attributeListIsLoaded', () => {
        const action = {} as Action;
        const state = fromAttribute.attributeReducer(undefined, action);

        expect(fromAttribute.selectAttributeListIsLoaded(state)).toEqual(false);
    });
});
