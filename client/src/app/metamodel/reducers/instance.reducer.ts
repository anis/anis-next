/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Instance } from '../models';
import * as instanceActions from '../actions/instance.actions';

/**
 * Interface for instance state.
 *
 * @interface State
 */
export interface State extends EntityState<Instance> {
    instanceListIsLoading: boolean;
    instanceListIsLoaded: boolean;
}

export const adapter: EntityAdapter<Instance> = createEntityAdapter<Instance>({
    selectId: (instance: Instance) => instance.name,
    sortComparer: (a: Instance, b: Instance) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    instanceListIsLoading: false,
    instanceListIsLoaded: false
});

export const instanceReducer = createReducer(
    initialState,
    on(instanceActions.loadInstanceList, (state) => {
        return {
            ...state,
            instanceListIsLoading: true
        }
    }),
    on(instanceActions.loadInstanceListSuccess, (state, { instances }) => {
        return adapter.setAll(
            instances,
            {
                ...state,
                instanceListIsLoading: false,
                instanceListIsLoaded: true
            }
        );
    }),
    on(instanceActions.loadInstanceListFail, (state) => {
        return {
            ...state,
            instanceListIsLoading: false
        }
    }),
    on(instanceActions.addInstanceSuccess, (state, { instance }) => {
        return adapter.addOne(instance, state)
    }),
    on(instanceActions.editInstanceSuccess, (state, { instance }) => {
        return adapter.setOne(instance, state)
    }),
    on(instanceActions.deleteInstanceSuccess, (state, { instance }) => {
        return adapter.removeOne(instance.name, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectInstanceIds = selectIds;
export const selectInstanceEntities = selectEntities;
export const selectAllInstances = selectAll;
export const selectInstanceTotal = selectTotal;

export const selectInstanceListIsLoading = (state: State) => state.instanceListIsLoading;
export const selectInstanceListIsLoaded = (state: State) => state.instanceListIsLoaded;
