/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Dataset } from '../models';
import * as datasetActions from '../actions/dataset.actions';

/**
 * Interface for dataset state.
 *
 * @interface State
 */
export interface State extends EntityState<Dataset> {
    datasetListIsLoading: boolean;
    datasetListIsLoaded: boolean;
}

export const adapter: EntityAdapter<Dataset> = createEntityAdapter<Dataset>({
    selectId: (dataset: Dataset) => dataset.name,
    sortComparer: (a: Dataset, b: Dataset) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    datasetListIsLoading: false,
    datasetListIsLoaded: false
});

export const datasetReducer = createReducer(
    initialState,
    on(datasetActions.loadDatasetList, (state) => {
        return {
            ...state,
            datasetListIsLoading: true
        }
    }),
    on(datasetActions.loadDatasetListSuccess, (state, { datasets }) => {
        return adapter.setAll(
            datasets,
            {
                ...state,
                datasetListIsLoading: false,
                datasetListIsLoaded: true
            }
        );
    }),
    on(datasetActions.loadDatasetListFail, (state) => {
        return {
            ...state,
            datasetListIsLoading: false
        }
    }),
    on(datasetActions.addDatasetSuccess, (state, { dataset }) => {
        return adapter.addOne(dataset, state)
    }),
    on(datasetActions.editDatasetSuccess, (state, { dataset }) => {
        return adapter.setOne(dataset, state)
    }),
    on(datasetActions.deleteDatasetSuccess, (state, { dataset }) => {
        return adapter.removeOne(dataset.name, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();


export const selectDatasetIds = selectIds;
export const selectDatasetEntities = selectEntities;
export const selectAllDatasets = selectAll;
export const selectDatasetTotal = selectTotal;

export const selectDatasetListIsLoading = (state: State) => state.datasetListIsLoading;
export const selectDatasetListIsLoaded = (state: State) => state.datasetListIsLoaded;
