/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDataset from './dataset.reducer';
import * as datasetActions from '../actions/dataset.actions';
import { DATASET, DATASET_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] Dataset reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromDataset;
        const action = { type: 'Unknown' };
        const state = fromDataset.datasetReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadDatasetList action should set datasetListIsLoading to true', () => {
        const { initialState } = fromDataset;
        const action = datasetActions.loadDatasetList();
        const state = fromDataset.datasetReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetListIsLoading).toEqual(true);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadDatasetListSuccess action should add dataset list, set datasetListIsLoading to false and set datasetListIsLoaded to true', () => {
        const { initialState } = fromDataset;
        const action = datasetActions.loadDatasetListSuccess({ datasets: DATASET_LIST });
        const state = fromDataset.datasetReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain('myDataset');
        expect(state.ids).toContain('anotherDataset');
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadDatasetListFail action should set datasetListIsLoading to false', () => {
        const { initialState } = fromDataset;
        const action = datasetActions.loadDatasetListFail();
        const state = fromDataset.datasetReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addDatasetSuccess action should add a dataset', () => {
        const { initialState } = fromDataset;
        const action = datasetActions.addDatasetSuccess({ dataset: DATASET });
        const state = fromDataset.datasetReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain('myDataset');
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editDatasetSuccess action should modify a dataset', () => {
        const initialState = {
            ...fromDataset.initialState,
            ids: ['myDataset'],
            entities: { 'myDataset': { ...DATASET, label: 'label' }}
        };
        const action = datasetActions.editDatasetSuccess({ dataset: DATASET });
        const state = fromDataset.datasetReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain('myDataset');
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities['myDataset']).toEqual(DATASET);
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteDatasetSuccess action should modify a dataset', () => {
        const initialState = {
            ...fromDataset.initialState,
            ids: ['myDataset'],
            entities: { 'myDataset': DATASET }
        };
        const action = datasetActions.deleteDatasetSuccess({ dataset: DATASET });
        const state = fromDataset.datasetReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get datasetListIsLoading', () => {
        const action = {} as Action;
        const state =  fromDataset.datasetReducer(undefined, action);

        expect(fromDataset.selectDatasetListIsLoading(state)).toEqual(false);
    });

    it('should get datasetListIsLoaded', () => {
        const action = {} as Action;
        const state = fromDataset.datasetReducer(undefined, action);

        expect(fromDataset.selectDatasetListIsLoaded(state)).toEqual(false);
    });
});
