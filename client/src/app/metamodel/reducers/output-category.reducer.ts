/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { OutputCategory } from '../models';
import * as outputCategoryActions from '../actions/output-category.actions';

/**
 * Interface for output category state.
 *
 * @interface State
 */
export interface State extends EntityState<OutputCategory> {
    outputCategoryListIsLoading: boolean;
    outputCategoryListIsLoaded: boolean;
}

export const adapter: EntityAdapter<OutputCategory> = createEntityAdapter<OutputCategory>({
    selectId: (outputCategory: OutputCategory) => outputCategory.id,
    sortComparer: (a: OutputCategory, b: OutputCategory) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    outputCategoryListIsLoading: false,
    outputCategoryListIsLoaded: false
});

export const outputCategoryReducer = createReducer(
    initialState,
    on(outputCategoryActions.loadOutputCategoryList, (state) => {
        return {
            ...state,
            outputCategoryListIsLoading: true
        }
    }),
    on(outputCategoryActions.loadOutputCategoryListSuccess, (state, { outputCategories }) => {
        return adapter.setAll(
            outputCategories,
            {
                ...state,
                outputCategoryListIsLoading: false,
                outputCategoryListIsLoaded: true
            }
        );
    }),
    on(outputCategoryActions.loadOutputCategoryListFail, (state) => {
        return {
            ...state,
            outputCategoryListIsLoading: false
        }
    }),
    on(outputCategoryActions.addOutputCategorySuccess, (state, { outputCategory }) => {
        return adapter.addOne(outputCategory, state)
    }),
    on(outputCategoryActions.editOutputCategorySuccess, (state, { outputCategory }) => {
        return adapter.setOne(outputCategory, state)
    }),
    on(outputCategoryActions.deleteOutputCategorySuccess, (state, { outputCategory }) => {
        return adapter.removeOne(outputCategory.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectOutputCategoryIds = selectIds;
export const selectOutputCategoryEntities = selectEntities;
export const selectAllOutputCategories = selectAll;
export const selectOutputCategoryTotal = selectTotal;

export const selectOutputCategoryListIsLoading = (state: State) => state.outputCategoryListIsLoading;
export const selectOutputCategoryListIsLoaded = (state: State) => state.outputCategoryListIsLoaded;
