/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { OutputFamily } from '../models';
import * as outputFamilyActions from '../actions/output-family.actions';

/**
 * Interface for output family state.
 *
 * @interface State
 */
export interface State extends EntityState<OutputFamily> {
    outputFamilyListIsLoading: boolean;
    outputFamilyListIsLoaded: boolean;
}

export const adapter: EntityAdapter<OutputFamily> = createEntityAdapter<OutputFamily>({
    selectId: (outputFamily: OutputFamily) => outputFamily.id,
    sortComparer: (a: OutputFamily, b: OutputFamily) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    outputFamilyListIsLoading: false,
    outputFamilyListIsLoaded: false
});

export const outputFamilyReducer = createReducer(
    initialState,
    on(outputFamilyActions.loadOutputFamilyList, (state) => {
        return {
            ...state,
            outputFamilyListIsLoading: true
        }
    }),
    on(outputFamilyActions.loadOutputFamilyListSuccess, (state, { outputFamilies }) => {
        return adapter.setAll(
            outputFamilies,
            {
                ...state,
                outputFamilyListIsLoading: false,
                outputFamilyListIsLoaded: true
            }
        );
    }),
    on(outputFamilyActions.loadOutputFamilyListFail, (state) => {
        return {
            ...state,
            outputFamilyListIsLoading: false
        }
    }),
    on(outputFamilyActions.addOutputFamilySuccess, (state, { outputFamily }) => {
        return adapter.addOne(outputFamily, state)
    }),
    on(outputFamilyActions.editOutputFamilySuccess, (state, { outputFamily }) => {
        return adapter.setOne(outputFamily, state)
    }),
    on(outputFamilyActions.deleteOutputFamilySuccess, (state, { outputFamily }) => {
        return adapter.removeOne(outputFamily.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectOutputFamilyIds = selectIds;
export const selectOutputFamilyEntities = selectEntities;
export const selectAllOutputFamilies = selectAll;
export const selectOutputFamilyTotal = selectTotal;

export const selectOutputFamilyListIsLoading = (state: State) => state.outputFamilyListIsLoading;
export const selectOutputFamilyListIsLoaded = (state: State) => state.outputFamilyListIsLoaded;
