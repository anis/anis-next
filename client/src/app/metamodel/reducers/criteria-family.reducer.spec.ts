/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromCriteriaFamily from './criteria-family.reducer';
import * as criteriaFamilyActions from '../actions/criteria-family.actions';
import { CRITERIA_FAMILY, CRITERIA_FAMILY_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] CriteriaFamily reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromCriteriaFamily;
        const action = { type: 'Unknown' };
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadCriteriaFamilyList action should set criteriaFamilyListIsLoading to true', () => {
        const { initialState } = fromCriteriaFamily;
        const action = criteriaFamilyActions.loadCriteriaFamilyList();
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.criteriaFamilyListIsLoading).toEqual(true);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadCriteriaFamilyListSuccess action should add criteriaFamily list, set criteriaFamilyListIsLoading to false and set criteriaFamilyListIsLoaded to true', () => {
        const { initialState } = fromCriteriaFamily;
        const action = criteriaFamilyActions.loadCriteriaFamilyListSuccess({ criteriaFamilies: CRITERIA_FAMILY_LIST });
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(1);
        expect(state.ids).toContain(2);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadCriteriaFamilyListFail action should set criteriaFamilyListIsLoading to false', () => {
        const { initialState } = fromCriteriaFamily;
        const action = criteriaFamilyActions.loadCriteriaFamilyListFail();
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addCriteriaFamilySuccess action should add a criteriaFamily', () => {
        const { initialState } = fromCriteriaFamily;
        const action = criteriaFamilyActions.addCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editCriteriaFamilySuccess action should modify a criteriaFamily', () => {
        const initialState = {
            ...fromCriteriaFamily.initialState,
            ids: [1],
            entities: { 1: { ...CRITERIA_FAMILY, label: 'label' }}
        };
        const action = criteriaFamilyActions.editCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[1]).toEqual(CRITERIA_FAMILY);
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteCriteriaFamilySuccess action should modify a criteriaFamily', () => {
        const initialState = {
            ...fromCriteriaFamily.initialState,
            ids: [1],
            entities: { 1: CRITERIA_FAMILY }
        };
        const action = criteriaFamilyActions.deleteCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get criteriaFamilyListIsLoading', () => {
        const action = {} as Action;
        const state =  fromCriteriaFamily.criteriaFamilyReducer(undefined, action);

        expect(fromCriteriaFamily.selectCriteriaFamilyListIsLoading(state)).toEqual(false);
    });

    it('should get criteriaFamilyListIsLoaded', () => {
        const action = {} as Action;
        const state = fromCriteriaFamily.criteriaFamilyReducer(undefined, action);

        expect(fromCriteriaFamily.selectCriteriaFamilyListIsLoaded(state)).toEqual(false);
    });
});
