/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { WebpageFamily } from '../models';
import * as webpageFamilyActions from '../actions/webpage-family.actions';

/**
 * Interface for webpage family state.
 *
 * @interface State
 */
export interface State extends EntityState<WebpageFamily> {
    webpageFamilyListIsLoading: boolean;
    webpageFamilyListIsLoaded: boolean;
}

export const adapter: EntityAdapter<WebpageFamily> = createEntityAdapter<WebpageFamily>({
    selectId: (webpageFamily: WebpageFamily) => webpageFamily.id,
    sortComparer: (a: WebpageFamily, b: WebpageFamily) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    webpageFamilyListIsLoading: false,
    webpageFamilyListIsLoaded: false
});

export const webpageFamilyReducer = createReducer(
    initialState,
    on(webpageFamilyActions.emptyWebpageFamilyList, () => {
        return {
            ...initialState
        };
    }),
    on(webpageFamilyActions.loadWebpageFamilyList, (state) => {
        return {
            ...state,
            webpageFamilyListIsLoading: true
        };
    }),
    on(webpageFamilyActions.loadWebpageFamilyListSuccess, (state, { webpageFamilies }) => {
        return adapter.setAll(
            webpageFamilies,
            {
                ...state,
                webpageFamilyListIsLoading: false,
                webpageFamilyListIsLoaded: true
            }
        );
    }),
    on(webpageFamilyActions.loadWebpageFamilyListFail, (state) => {
        return {
            ...state,
            webpageFamilyListIsLoading: false
        };
    }),
    on(webpageFamilyActions.addWebpageFamilySuccess, (state, { webpageFamily }) => {
        return adapter.addOne(webpageFamily, state);
    }),
    on(webpageFamilyActions.editWebpageFamilySuccess, (state, { webpageFamily }) => {
        return adapter.setOne(webpageFamily, state);
    }),
    on(webpageFamilyActions.deleteWebpageFamilySuccess, (state, { webpageFamily }) => {
        return adapter.removeOne(webpageFamily.id, state);
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectWebpageFamilyIds = selectIds;
export const selectWebpageFamilyEntities = selectEntities;
export const selectAllWebpageFamilies = selectAll;
export const selectWebpageFamilyTotal = selectTotal;

export const selectWebpageFamilyListIsLoading = (state: State) => state.webpageFamilyListIsLoading;
export const selectWebpageFamilyListIsLoaded = (state: State) => state.webpageFamilyListIsLoaded;
