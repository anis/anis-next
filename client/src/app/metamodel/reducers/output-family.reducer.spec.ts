/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromOutputFamily from './output-family.reducer';
import * as outputFamilyActions from '../actions/output-family.actions';
import { OUTPUT_FAMILY, OUTPUT_FAMILY_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] OutputFamily reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromOutputFamily;
        const action = { type: 'Unknown' };
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadOutputFamilyList action should set outputFamilyListIsLoading to true', () => {
        const { initialState } = fromOutputFamily;
        const action = outputFamilyActions.loadOutputFamilyList();
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputFamilyListIsLoading).toEqual(true);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadOutputFamilyListSuccess action should add outputFamily list, set outputFamilyListIsLoading to false and set outputFamilyListIsLoaded to true', () => {
        const { initialState } = fromOutputFamily;
        const action = outputFamilyActions.loadOutputFamilyListSuccess({ outputFamilies: OUTPUT_FAMILY_LIST });
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(1);
        expect(state.ids).toContain(2);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadOutputFamilyListFail action should set outputFamilyListIsLoading to false', () => {
        const { initialState } = fromOutputFamily;
        const action = outputFamilyActions.loadOutputFamilyListFail();
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addOutputFamilySuccess action should add a outputFamily', () => {
        const { initialState } = fromOutputFamily;
        const action = outputFamilyActions.addOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editOutputFamilySuccess action should modify a outputFamily', () => {
        const initialState = {
            ...fromOutputFamily.initialState,
            ids: [1],
            entities: { 1: { ...OUTPUT_FAMILY, label: 'label' }}
        };
        const action = outputFamilyActions.editOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[1]).toEqual(OUTPUT_FAMILY);
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteOutputFamilySuccess action should modify a outputFamily', () => {
        const initialState = {
            ...fromOutputFamily.initialState,
            ids: [1],
            entities: { 1: OUTPUT_FAMILY }
        };
        const action = outputFamilyActions.deleteOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get outputFamilyListIsLoading', () => {
        const action = {} as Action;
        const state =  fromOutputFamily.outputFamilyReducer(undefined, action);

        expect(fromOutputFamily.selectOutputFamilyListIsLoading(state)).toEqual(false);
    });

    it('should get outputFamilyListIsLoaded', () => {
        const action = {} as Action;
        const state = fromOutputFamily.outputFamilyReducer(undefined, action);

        expect(fromOutputFamily.selectOutputFamilyListIsLoaded(state)).toEqual(false);
    });
});
