/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { File } from '../models';
import * as fileActions from '../actions/file.actions';

/**
 * Interface for file state.
 *
 * @interface State
 */
export interface State extends EntityState<File> {
    fileListIsLoading: boolean;
    fileListIsLoaded: boolean;
}

export const adapter: EntityAdapter<File> = createEntityAdapter<File>({
    selectId: (file: File) => file.id,
    sortComparer: (a: File, b: File) => a.id - b.id
});

export const initialState: State = adapter.getInitialState({
    fileListIsLoading: false,
    fileListIsLoaded: false
});

export const fileReducer = createReducer(
    initialState,
    on(fileActions.loadFileList, (state) => {
        return {
            ...state,
            fileListIsLoading: true
        }
    }),
    on(fileActions.loadFileListSuccess, (state, { files }) => {
        return adapter.setAll(
            files,
            {
                ...state,
                fileListIsLoading: false,
                fileListIsLoaded: true
            }
        );
    }),
    on(fileActions.loadFileListFail, (state) => {
        return {
            ...state,
            fileListIsLoading: false
        }
    }),
    on(fileActions.addFileSuccess, (state, { file }) => {
        return adapter.addOne(file, state)
    }),
    on(fileActions.editFileSuccess, (state, { file }) => {
        return adapter.setOne(file, state)
    }),
    on(fileActions.deleteFileSuccess, (state, { file }) => {
        return adapter.removeOne(file.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectFileIds = selectIds;
export const selectFileEntities = selectEntities;
export const selectAllFiles = selectAll;
export const selectFileTotal = selectTotal;

export const selectFileListIsLoading = (state: State) => state.fileListIsLoading;
export const selectFileListIsLoaded = (state: State) => state.fileListIsLoaded;
