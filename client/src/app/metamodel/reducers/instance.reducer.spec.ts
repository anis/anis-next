/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromInstance from './instance.reducer';
import * as instanceActions from '../actions/instance.actions';
import { INSTANCE, INSTANCE_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] Instance reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromInstance;
        const action = { type: 'Unknown' };
        const state = fromInstance.instanceReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadInstanceList action should set instanceListIsLoading to true', () => {
        const { initialState } = fromInstance;
        const action = instanceActions.loadInstanceList();
        const state = fromInstance.instanceReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceListIsLoading).toEqual(true);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadInstanceListSuccess action should add instance list, set instanceListIsLoading to false and set instanceListIsLoaded to true', () => {
        const { initialState } = fromInstance;
        const action = instanceActions.loadInstanceListSuccess({ instances: INSTANCE_LIST });
        const state = fromInstance.instanceReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain('myInstance');
        expect(state.ids).toContain('myOtherInstance');
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadInstanceListFail action should set instanceListIsLoading to false', () => {
        const { initialState } = fromInstance;
        const action = instanceActions.loadInstanceListFail();
        const state = fromInstance.instanceReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addInstanceSuccess action should add a instance', () => {
        const { initialState } = fromInstance;
        const action = instanceActions.addInstanceSuccess({ instance: INSTANCE });
        const state = fromInstance.instanceReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain('myInstance');
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editInstanceSuccess action should modify a instance', () => {
        const initialState = {
            ...fromInstance.initialState,
            ids: ['myInstance'],
            entities: { 'myInstance': { ...INSTANCE, label: 'label' }}
        };
        const action = instanceActions.editInstanceSuccess({ instance: INSTANCE });
        const state = fromInstance.instanceReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain('myInstance');
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities['myInstance']).toEqual(INSTANCE);
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteInstanceSuccess action should modify a instance', () => {
        const initialState = {
            ...fromInstance.initialState,
            ids: ['myInstance'],
            entities: { 'myInstance': INSTANCE }
        };
        const action = instanceActions.deleteInstanceSuccess({ instance: INSTANCE });
        const state = fromInstance.instanceReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get instanceListIsLoading', () => {
        const action = {} as Action;
        const state =  fromInstance.instanceReducer(undefined, action);

        expect(fromInstance.selectInstanceListIsLoading(state)).toEqual(false);
    });

    it('should get instanceListIsLoaded', () => {
        const action = {} as Action;
        const state = fromInstance.instanceReducer(undefined, action);

        expect(fromInstance.selectInstanceListIsLoaded(state)).toEqual(false);
    });
});
