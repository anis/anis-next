/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDatasetGroup from './dataset-group.reducer';
import * as datasetGroupActions from '../actions/dataset-group.actions';
import { GROUP, GROUP_LIST } from '../../../test-data';

describe('[Metamodel][Reducers] Dataset group reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromDatasetGroup;
        const action = { type: 'Unknown' };
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadDatasetGroupList action should set datasetGroupListIsLoading to true', () => {
        const { initialState } = fromDatasetGroup;
        const action = datasetGroupActions.loadDatasetGroupList();
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetGroupListIsLoading).toEqual(true);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadDatasetGroupListSuccess action should add dataset group list, set datasetGroupListIsLoading to false and set datasetGroupListIsLoaded to true', () => {
        const { initialState } = fromDatasetGroup;
        const action = datasetGroupActions.loadDatasetGroupListSuccess({ datasetGroups: GROUP_LIST });
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(1);
        expect(state.ids).toContain(2);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadDatasetGroupListFail action should set datasetGroupListIsLoading to false', () => {
        const { initialState } = fromDatasetGroup;
        const action = datasetGroupActions.loadDatasetGroupListFail();
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('addDatasetGroupSuccess action should add a dataset group', () => {
        const { initialState } = fromDatasetGroup;
        const action = datasetGroupActions.addDatasetGroupSuccess({ datasetGroup: GROUP });
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('editDatasetGroupSuccess action should modify a dataset group', () => {
        const initialState = {
            ...fromDatasetGroup.initialState,
            ids: [1],
            entities: { 1: { ...GROUP, role: 'role' }}
        };
        const action = datasetGroupActions.editDatasetGroupSuccess({ datasetGroup: GROUP });
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);
        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(1);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[1]).toEqual(GROUP);
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteDatasetGroupSuccess action should modify a dataset group', () => {
        const initialState = {
            ...fromDatasetGroup.initialState,
            ids: [1],
            entities: { 1: GROUP }
        };
        const action = datasetGroupActions.deleteDatasetGroupSuccess({ datasetGroup: GROUP });
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual( { });
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get datasetGroupListIsLoading', () => {
        const action = {} as Action;
        const state =  fromDatasetGroup.datasetGroupReducer(undefined, action);

        expect(fromDatasetGroup.selectDatasetGroupListIsLoading(state)).toEqual(false);
    });

    it('should get datasetGroupListIsLoaded', () => {
        const action = {} as Action;
        const state = fromDatasetGroup.datasetGroupReducer(undefined, action);

        expect(fromDatasetGroup.selectDatasetGroupListIsLoaded(state)).toEqual(false);
    });
});
