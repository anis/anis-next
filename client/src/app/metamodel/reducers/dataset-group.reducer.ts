/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { DatasetGroup } from '../models';
import * as datasetGroupActions from '../actions/dataset-group.actions';

/**
 * Interface for dataset group state.
 *
 * @interface State
 */
export interface State extends EntityState<DatasetGroup> {
    datasetGroupListIsLoading: boolean;
    datasetGroupListIsLoaded: boolean;
}

export const adapter: EntityAdapter<DatasetGroup> = createEntityAdapter<DatasetGroup>();

export const initialState: State = adapter.getInitialState({
    datasetGroupListIsLoading: false,
    datasetGroupListIsLoaded: false
});

export const datasetGroupReducer = createReducer(
    initialState,
    on(datasetGroupActions.loadDatasetGroupList, (state) => {
        return {
            ...state,
            datasetGroupListIsLoading: true
        }
    }),
    on(datasetGroupActions.loadDatasetGroupListSuccess, (state, { datasetGroups }) => {
        return adapter.setAll(
            datasetGroups,
            {
                ...state,
                datasetGroupListIsLoading: false,
                datasetGroupListIsLoaded: true
            }
        );
    }),
    on(datasetGroupActions.loadDatasetGroupListFail, (state) => {
        return {
            ...state,
            datasetGroupListIsLoading: false
        }
    }),
    on(datasetGroupActions.addDatasetGroupSuccess, (state, { datasetGroup }) => {
        return adapter.addOne(datasetGroup, state)
    }),
    on(datasetGroupActions.editDatasetGroupSuccess, (state, { datasetGroup }) => {
        return adapter.setOne(datasetGroup, state)
    }),
    on(datasetGroupActions.deleteDatasetGroupSuccess, (state, { datasetGroup }) => {
        return adapter.removeOne(datasetGroup.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();


export const selectDatasetGroupIds = selectIds;
export const selectDatasetGroupEntities = selectEntities;
export const selectAllDatasetGroups = selectAll;
export const selectDatasetGroupTotal = selectTotal;

export const selectDatasetGroupListIsLoading = (state: State) => state.datasetGroupListIsLoading;
export const selectDatasetGroupListIsLoaded = (state: State) => state.datasetGroupListIsLoaded;
