/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import { AliasConfig } from '../models';
import * as aliasActions from '../actions/alias-config.actions';

/**
 * Interface for alias state.
 *
 * @interface State
 */
export interface State {
    aliasConfig: AliasConfig;
    aliasConfigIsLoading: boolean;
    aliasConfigIsLoaded: boolean;
}

export const initialState: State = {
    aliasConfig: null,
    aliasConfigIsLoading: false,
    aliasConfigIsLoaded: false
};

export const aliasConfigReducer = createReducer(
    initialState,
    on(aliasActions.resetAliasConfig, () => {
        return {
            ...initialState
        }
    }),
    on(aliasActions.loadAliasConfig, (state) => {
        return {
            ...state,
            aliasConfig: null,
            aliasConfigIsLoading: true,
            aliasConfigIsLoaded: false
        }
    }),
    on(aliasActions.loadAliasConfigSuccess, (state, { aliasConfig }) => {
        return {
            ...state,
            aliasConfig,
            aliasConfigIsLoading: false,
            aliasConfigIsLoaded: true
        }
    }),
    on(aliasActions.loadAliasConfigFail, (state) => {
        return {
            ...state,
            aliasConfigIsLoading: false
        }
    }),
    on(aliasActions.addAliasConfigSuccess, (state, { aliasConfig }) => {
        return {
            ...state,
            aliasConfig
        }
    }),
    on(aliasActions.editAliasConfigSuccess, (state, { aliasConfig }) => {
        return {
            ...state,
            aliasConfig
        }
    })
);

export const selectAliasConfig = (state: State) => state.aliasConfig;
export const selectAliasConfigIsLoading = (state: State) => state.aliasConfigIsLoading;
export const selectAliasConfigIsLoaded = (state: State) => state.aliasConfigIsLoaded;
