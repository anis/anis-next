/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import { ConeSearchConfig } from '../models';
import * as coneSearchActions from '../actions/cone-search-config.actions';

/**
 * Interface for coneSearch state.
 *
 * @interface State
 */
export interface State {
    coneSearchConfig: ConeSearchConfig;
    coneSearchConfigIsLoading: boolean;
    coneSearchConfigIsLoaded: boolean;
}

export const initialState: State = {
    coneSearchConfig: null,
    coneSearchConfigIsLoading: false,
    coneSearchConfigIsLoaded: false
};

export const coneSearchConfigReducer = createReducer(
    initialState,
    on(coneSearchActions.resetConeSearchConfig, () => {
        return {
            ...initialState
        }
    }),
    on(coneSearchActions.loadConeSearchConfig, (state) => {
        return {
            ...state,
            coneSearchConfig: null,
            coneSearchConfigIsLoading: true,
            coneSearchConfigIsLoaded: false
        }
    }),
    on(coneSearchActions.loadConeSearchConfigSuccess, (state, { coneSearchConfig }) => {
        return {
            ...state,
            coneSearchConfig,
            coneSearchConfigIsLoading: false,
            coneSearchConfigIsLoaded: true
        }
    }),
    on(coneSearchActions.loadConeSearchConfigFail, (state) => {
        return {
            ...state,
            coneSearchConfigIsLoading: false
        }
    }),
    on(coneSearchActions.addConeSearchConfigSuccess, (state, { coneSearchConfig }) => {
        return {
            ...state,
            coneSearchConfig
        }
    }),
    on(coneSearchActions.editConeSearchConfigSuccess, (state, { coneSearchConfig }) => {
        return {
            ...state,
            coneSearchConfig
        }
    })
);

export const selectConeSearchConfig = (state: State) => state.coneSearchConfig;
export const selectConeSearchConfigIsLoading = (state: State) => state.coneSearchConfigIsLoading;
export const selectConeSearchConfigIsLoaded = (state: State) => state.coneSearchConfigIsLoaded;
