/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Webpage } from '../models';
import * as webpageActions from '../actions/webpage.actions';

/**
 * Interface for webpage state.
 *
 * @interface State
 */
export interface State extends EntityState<Webpage> {
    webpageListIsLoading: boolean;
    webpageListIsLoaded: boolean;
}

export const adapter: EntityAdapter<Webpage> = createEntityAdapter<Webpage>({
    selectId: (webpage: Webpage) => webpage.id,
    sortComparer: (a: Webpage, b: Webpage) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    webpageListIsLoading: false,
    webpageListIsLoaded: false
});

export const webpageReducer = createReducer(
    initialState,
    on(webpageActions.emptyWebpageList, () => {
        return {
            ...initialState
        };
    }),
    on(webpageActions.loadWebpageList, (state) => {
        return {
            ...state,
            webpageListIsLoading: true
        }
    }),
    on(webpageActions.loadWebpageListSuccess, (state, { webpages }) => {
        return adapter.setAll(
            webpages,
            {
                ...state,
                webpageListIsLoading: false,
                webpageListIsLoaded: true
            }
        );
    }),
    on(webpageActions.loadWebpageListFail, (state) => {
        return {
            ...state,
            webpageListIsLoading: false
        }
    }),
    on(webpageActions.addWebpageSuccess, (state, { webpage }) => {
        return adapter.addOne(webpage, state)
    }),
    on(webpageActions.editWebpageSuccess, (state, { webpage }) => {
        return adapter.setOne(webpage, state)
    }),
    on(webpageActions.deleteWebpageSuccess, (state, { webpage }) => {
        return adapter.removeOne(webpage.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectWebpageIds = selectIds;
export const selectWebpageEntities = selectEntities;
export const selectAllWebpages = selectAll;
export const selectWebpageTotal = selectTotal;

export const selectWebpageListIsLoading = (state: State) => state.webpageListIsLoading;
export const selectWebpageListIsLoaded = (state: State) => state.webpageListIsLoaded;
