/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { DetailConfig } from '../models';

export const loadDetailConfig = createAction('[Metamodel] Load Detail Config');
export const loadDetailConfigSuccess = createAction('[Metamodel] Load Detail Config Success', props<{ detailConfig: DetailConfig }>());
export const loadDetailConfigFail = createAction('[Metamodel] Load Detail Config Fail');
export const addDetailConfig = createAction('[Metamodel] Add Detail Config', props<{ detailConfig: DetailConfig }>());
export const addDetailConfigSuccess = createAction('[Metamodel] Add Detail Config Success', props<{ detailConfig: DetailConfig }>());
export const addDetailConfigFail = createAction('[Metamodel] Add Detail Config Fail');
export const editDetailConfig = createAction('[Metamodel] Edit Detail Config', props<{ detailConfig: DetailConfig }>());
export const editDetailConfigSuccess = createAction('[Metamodel] Edit Detail Config Success', props<{ detailConfig: DetailConfig }>());
export const editDetailConfigFail = createAction('[Metamodel] Edit Detail Config Fail');
