/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { Dataset } from '../models';

export const loadDatasetList = createAction('[Metamodel] Load Dataset List');
export const loadDatasetListSuccess = createAction('[Metamodel] Load Dataset List Success', props<{ datasets: Dataset[] }>());
export const loadDatasetListFail = createAction('[Metamodel] Load Dataset List Fail');
export const addDataset = createAction('[Metamodel] Add Dataset', props<{ dataset: Dataset }>());
export const addDatasetSuccess = createAction('[Metamodel] Add Dataset Success', props<{ dataset: Dataset }>());
export const addDatasetFail = createAction('[Metamodel] Add Dataset Fail');
export const editDataset = createAction('[Metamodel] Edit Dataset', props<{ dataset: Dataset }>());
export const editDatasetSuccess = createAction('[Metamodel] Edit Dataset Success', props<{ dataset: Dataset }>());
export const editDatasetFail = createAction('[Metamodel] Edit Dataset Fail');
export const deleteDataset = createAction('[Metamodel] Delete Dataset', props<{ dataset: Dataset }>());
export const deleteDatasetSuccess = createAction('[Metamodel] Delete Dataset Success', props<{ dataset: Dataset }>());
export const deleteDatasetFail = createAction('[Metamodel] Delete Dataset Fail');
