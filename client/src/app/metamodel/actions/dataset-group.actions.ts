/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { DatasetGroup } from '../models';

export const loadDatasetGroupList = createAction('[Metamodel] Load Dataset Group List');
export const loadDatasetGroupListSuccess = createAction('[Metamodel] Load Dataset Group List Success', props<{ datasetGroups: DatasetGroup[] }>());
export const loadDatasetGroupListFail = createAction('[Metamodel] Load Dataset Group List Fail');
export const addDatasetGroup = createAction('[Metamodel] Add Dataset Group', props<{ datasetGroup: DatasetGroup }>());
export const addDatasetGroupSuccess = createAction('[Metamodel] Add Dataset Group Success', props<{ datasetGroup: DatasetGroup }>());
export const addDatasetGroupFail = createAction('[Metamodel] Add Dataset Group Fail');
export const editDatasetGroup = createAction('[Metamodel] Edit Dataset Group', props<{ datasetGroup: DatasetGroup }>());
export const editDatasetGroupSuccess = createAction('[Metamodel] Edit Dataset Group Success', props<{ datasetGroup: DatasetGroup }>());
export const editDatasetGroupFail = createAction('[Metamodel] Edit Dataset Group Fail');
export const deleteDatasetGroup = createAction('[Metamodel] Delete Dataset Group', props<{ datasetGroup: DatasetGroup }>());
export const deleteDatasetGroupSuccess = createAction('[Metamodel] Delete Dataset Group Success', props<{ datasetGroup: DatasetGroup }>());
export const deleteDatasetGroupFail = createAction('[Metamodel] Delete Dataset Group Fail');
