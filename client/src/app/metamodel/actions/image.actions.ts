/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { Image } from '../models';

export const loadImageList = createAction('[Metamodel] Load Image List');
export const loadImageListSuccess = createAction('[Metamodel] Load Image List Success', props<{ images: Image[] }>());
export const loadImageListFail = createAction('[Metamodel] Load Image List Fail');
export const addImage = createAction('[Metamodel] Add Image', props<{ image: Image }>());
export const addImageSuccess = createAction('[Metamodel] Add Image Success', props<{ image: Image }>());
export const addImageFail = createAction('[Metamodel] Add Image Fail');
export const editImage = createAction('[Metamodel] Edit Image', props<{ image: Image }>());
export const editImageSuccess = createAction('[Metamodel] Edit Image Success', props<{ image: Image }>());
export const editImageFail = createAction('[Metamodel] Edit Image Fail');
export const deleteImage = createAction('[Metamodel] Delete Image', props<{ image: Image }>());
export const deleteImageSuccess = createAction('[Metamodel] Delete Image Success', props<{ image: Image }>());
export const deleteImageFail = createAction('[Metamodel] Delete Image Fail');