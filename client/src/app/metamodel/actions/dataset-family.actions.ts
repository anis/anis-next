/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { DatasetFamily } from '../models';

export const loadDatasetFamilyList = createAction('[Metamodel] Load Dataset Family List');
export const loadDatasetFamilyListSuccess = createAction('[Metamodel] Load Dataset Family List Success', props<{ datasetFamilies: DatasetFamily[] }>());
export const loadDatasetFamilyListFail = createAction('[Metamodel] Load Dataset Family List Fail');
export const addDatasetFamily = createAction('[Metamodel] Add Dataset Family', props<{ datasetFamily: DatasetFamily }>());
export const addDatasetFamilySuccess = createAction('[Metamodel] Add Dataset Family Success', props<{ datasetFamily: DatasetFamily }>());
export const addDatasetFamilyFail = createAction('[Metamodel] Add Dataset Family Fail');
export const editDatasetFamily = createAction('[Metamodel] Edit Dataset Family', props<{ datasetFamily: DatasetFamily }>());
export const editDatasetFamilySuccess = createAction('[Metamodel] Edit Dataset Family Success', props<{ datasetFamily: DatasetFamily }>());
export const editDatasetFamilyFail = createAction('[Metamodel] Edit Dataset Family Fail');
export const deleteDatasetFamily = createAction('[Metamodel] Delete Dataset Family', props<{ datasetFamily: DatasetFamily }>());
export const deleteDatasetFamilySuccess = createAction('[Metamodel] Delete Dataset Family Success', props<{ datasetFamily: DatasetFamily }>());
export const deleteDatasetFamilyFail = createAction('[Metamodel] Delete Dataset Family Fail');
