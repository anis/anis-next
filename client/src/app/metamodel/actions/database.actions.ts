/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { Database } from '../models';

export const loadDatabaseList = createAction('[Metamodel] Load Database List');
export const loadDatabaseListSuccess = createAction('[Metamodel] Load Database List Success', props<{ databases: Database[] }>());
export const loadDatabaseListFail = createAction('[Metamodel] Load Database List Fail');
export const addDatabase = createAction('[Metamodel] Add Database', props<{ database: Database }>());
export const addDatabaseSuccess = createAction('[Metamodel] Add Database Success', props<{ database: Database }>());
export const addDatabaseFail = createAction('[Metamodel] Add Database Fail');
export const editDatabase = createAction('[Metamodel] Edit Database', props<{ database: Database }>());
export const editDatabaseSuccess = createAction('[Metamodel] Edit Database Success', props<{ database: Database }>());
export const editDatabaseFail = createAction('[Metamodel] Edit Database Fail');
export const deleteDatabase = createAction('[Metamodel] Delete Database', props<{ database: Database }>());
export const deleteDatabaseSuccess = createAction('[Metamodel] Delete Database Success', props<{ database: Database }>());
export const deleteDatabaseFail = createAction('[Metamodel] Delete Database Fail');
