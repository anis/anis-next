/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { WebpageFamily } from '../models';

export const emptyWebpageFamilyList = createAction('[Metamodel] Empty Webpage Family List');
export const loadWebpageFamilyList = createAction('[Metamodel] Load Webpage Family List');
export const loadWebpageFamilyListSuccess = createAction('[Metamodel] Load Webpage Family List Success', props<{ webpageFamilies: WebpageFamily[] }>());
export const loadWebpageFamilyListFail = createAction('[Metamodel] Load Webpage Family List Fail');
export const addWebpageFamily = createAction('[Metamodel] Add Webpage Family', props<{ webpageFamily: WebpageFamily }>());
export const addWebpageFamilySuccess = createAction('[Metamodel] Add Webpage Family Success', props<{ webpageFamily: WebpageFamily }>());
export const addWebpageFamilyFail = createAction('[Metamodel] Add Webpage Family Fail');
export const editWebpageFamily = createAction('[Metamodel] Edit Webpage Family', props<{ webpageFamily: WebpageFamily }>());
export const editWebpageFamilySuccess = createAction('[Metamodel] Edit Webpage Family Success', props<{ webpageFamily: WebpageFamily }>());
export const editWebpageFamilyFail = createAction('[Metamodel] Edit Webpage Family Fail');
export const deleteWebpageFamily = createAction('[Metamodel] Delete Webpage Family', props<{ webpageFamily: WebpageFamily }>());
export const deleteWebpageFamilySuccess = createAction('[Metamodel] Delete Webpage Family Success', props<{ webpageFamily: WebpageFamily }>());
export const deleteWebpageFamilyFail = createAction('[Metamodel] Delete Webpage Family Fail');
