/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { Attribute } from '../models';

export const resetAttributeList = createAction('[Metamodel] Reset Attribute List');
export const loadAttributeList = createAction('[Metamodel] Load Attribute List');
export const loadAttributeListSuccess = createAction('[Metamodel] Load Attribute List Success', props<{ attributes: Attribute[] }>());
export const loadAttributeListFail = createAction('[Metamodel] Load Attribute List Fail');
export const addAttributeList = createAction('[Metamodel] Add Attribute List', props<{ attributeList: Attribute[] }>());
export const addAttribute = createAction('[Metamodel] Add Attribute', props<{ attribute: Attribute }>());
export const addAttributeSuccess = createAction('[Metamodel] Add Attribute Success', props<{ attribute: Attribute }>());
export const addAttributeFail = createAction('[Metamodel] Add Attribute Fail');
export const editAttribute = createAction('[Metamodel] Edit Attribute', props<{ attribute: Attribute }>());
export const editAttributeSuccess = createAction('[Metamodel] Edit Attribute Success', props<{ attribute: Attribute }>());
export const editAttributeFail = createAction('[Metamodel] Edit Attribute Fail');
export const deleteAttribute = createAction('[Metamodel] Delete Attribute', props<{ attribute: Attribute }>());
export const deleteAttributeSuccess = createAction('[Metamodel] Delete Attribute Success', props<{ attribute: Attribute }>());
export const deleteAttributeFail = createAction('[Metamodel] Delete Attribute Fail');
