/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { File } from '../models';

export const loadFileList = createAction('[Metamodel] Load File List');
export const loadFileListSuccess = createAction('[Metamodel] Load File List Success', props<{ files: File[] }>());
export const loadFileListFail = createAction('[Metamodel] Load File List Fail');
export const addFile = createAction('[Metamodel] Add File', props<{ file: File }>());
export const addFileSuccess = createAction('[Metamodel] Add File Success', props<{ file: File }>());
export const addFileFail = createAction('[Metamodel] Add File Fail');
export const editFile = createAction('[Metamodel] Edit File', props<{ file: File }>());
export const editFileSuccess = createAction('[Metamodel] Edit File Success', props<{ file: File }>());
export const editFileFail = createAction('[Metamodel] Edit File Fail');
export const deleteFile = createAction('[Metamodel] Delete File', props<{ file: File }>());
export const deleteFileSuccess = createAction('[Metamodel] Delete File Success', props<{ file: File }>());
export const deleteFileFail = createAction('[Metamodel] Delete File Fail');