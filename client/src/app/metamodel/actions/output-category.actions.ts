/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { OutputCategory } from '../models';

export const loadOutputCategoryList = createAction('[Metamodel] Load Output Category List');
export const loadOutputCategoryListSuccess = createAction('[Metamodel] Load Output Category List Success', props<{ outputCategories: OutputCategory[] }>());
export const loadOutputCategoryListFail = createAction('[Metamodel] Load Output Category List Fail');
export const addOutputCategory = createAction('[Metamodel] Add Output Category', props<{ outputCategory: OutputCategory }>());
export const addOutputCategorySuccess = createAction('[Metamodel] Add Output Category Success', props<{ outputCategory: OutputCategory }>());
export const addOutputCategoryFail = createAction('[Metamodel] Add Output Category Fail');
export const editOutputCategory = createAction('[Metamodel] Edit Output Category', props<{ outputCategory: OutputCategory }>());
export const editOutputCategorySuccess = createAction('[Metamodel] Edit Output Category Success', props<{ outputCategory: OutputCategory }>());
export const editOutputCategoryFail = createAction('[Metamodel] Edit Output Category Fail');
export const deleteOutputCategory = createAction('[Metamodel] Delete Output Category', props<{ outputCategory: OutputCategory }>());
export const deleteOutputCategorySuccess = createAction('[Metamodel] Delete Output Category Success', props<{ outputCategory: OutputCategory }>());
export const deleteOutputCategoryFail = createAction('[Metamodel] Delete Output Category Fail');
