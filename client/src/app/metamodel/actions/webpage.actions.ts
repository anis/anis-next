/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { Webpage } from '../models';

export const emptyWebpageList = createAction('[Metamodel] Empty Webpage List');
export const loadWebpageList = createAction('[Metamodel] Load Webpage List');
export const loadWebpageListSuccess = createAction('[Metamodel] Load Webpage List Success', props<{ webpages: Webpage[] }>());
export const loadWebpageListFail = createAction('[Metamodel] Load Webpage List Fail');
export const addWebpage = createAction('[Metamodel] Add Webpage', props<{ webpage: Webpage }>());
export const addWebpageSuccess = createAction('[Metamodel] Add Webpage Success', props<{ webpage: Webpage }>());
export const addWebpageFail = createAction('[Metamodel] Add Webpage Fail');
export const editWebpage = createAction('[Metamodel] Edit Webpage', props<{ webpage: Webpage }>());
export const editWebpageSuccess = createAction('[Metamodel] Edit Webpage Success', props<{ webpage: Webpage }>());
export const editWebpageFail = createAction('[Metamodel] Edit Webpage Fail');
export const deleteWebpage = createAction('[Metamodel] Delete Webpage', props<{ webpage: Webpage }>());
export const deleteWebpageSuccess = createAction('[Metamodel] Delete Webpage Success', props<{ webpage: Webpage }>());
export const deleteWebpageFail = createAction('[Metamodel] Delete Webpage Fail');
