/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { OutputFamily } from '../models';

export const loadOutputFamilyList = createAction('[Metamodel] Load Output Family List');
export const loadOutputFamilyListSuccess = createAction('[Metamodel] Load Output Family List Success', props<{ outputFamilies: OutputFamily[] }>());
export const loadOutputFamilyListFail = createAction('[Metamodel] Load Output Family List Fail');
export const addOutputFamily = createAction('[Metamodel] Add Output Family', props<{ outputFamily: OutputFamily }>());
export const addOutputFamilySuccess = createAction('[Metamodel] Add Output Family Success', props<{ outputFamily: OutputFamily }>());
export const addOutputFamilyFail = createAction('[Metamodel] Add Output Family Fail');
export const editOutputFamily = createAction('[Metamodel] Edit Output Family', props<{ outputFamily: OutputFamily }>());
export const editOutputFamilySuccess = createAction('[Metamodel] Edit Output Family Success', props<{ outputFamily: OutputFamily }>());
export const editOutputFamilyFail = createAction('[Metamodel] Edit Output Family Fail');
export const deleteOutputFamily = createAction('[Metamodel] Delete Output Family', props<{ outputFamily: OutputFamily }>());
export const deleteOutputFamilySuccess = createAction('[Metamodel] Delete Output Family Success', props<{ outputFamily: OutputFamily }>());
export const deleteOutputFamilyFail = createAction('[Metamodel] Delete Output Family Fail');
