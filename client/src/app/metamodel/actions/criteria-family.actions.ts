/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { CriteriaFamily } from '../models';

export const loadCriteriaFamilyList = createAction('[Metamodel] Load Criteria Family List');
export const loadCriteriaFamilyListSuccess = createAction('[Metamodel] Load Criteria Family List Success', props<{ criteriaFamilies: CriteriaFamily[] }>());
export const loadCriteriaFamilyListFail = createAction('[Metamodel] Load Criteria Family List Fail');
export const addCriteriaFamily = createAction('[Metamodel] Add Criteria Family', props<{ criteriaFamily: CriteriaFamily }>());
export const addCriteriaFamilySuccess = createAction('[Metamodel] Add Criteria Family Success', props<{ criteriaFamily: CriteriaFamily }>());
export const addCriteriaFamilyFail = createAction('[Metamodel] Add Criteria Family Fail');
export const editCriteriaFamily = createAction('[Metamodel] Edit Criteria Family', props<{ criteriaFamily: CriteriaFamily }>());
export const editCriteriaFamilySuccess = createAction('[Metamodel] Edit Criteria Family Success', props<{ criteriaFamily: CriteriaFamily }>());
export const editCriteriaFamilyFail = createAction('[Metamodel] Edit Criteria Family Fail');
export const deleteCriteriaFamily = createAction('[Metamodel] Delete Criteria Family', props<{ criteriaFamily: CriteriaFamily }>());
export const deleteCriteriaFamilySuccess = createAction('[Metamodel] Delete Criteria Family Success', props<{ criteriaFamily: CriteriaFamily }>());
export const deleteCriteriaFamilyFail = createAction('[Metamodel] Delete Criteria Family Fail');
