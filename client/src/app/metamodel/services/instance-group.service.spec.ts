/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { InstanceGroupService } from './instance-group.service';
import { InstanceGroup } from '../models';

describe('[Metamodel][services] InstanceGroupService', () => {
    let service: InstanceGroupService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let instanceGroup: InstanceGroup;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                InstanceGroupService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(InstanceGroupService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        instanceGroup = { ...instanceGroup, id: 1 };
    }));

    it('#retrieveInstanceGroupList() should request return an Observable<InstanceGroup[]> object', () => {
        service.retrieveInstanceGroupList().subscribe((res: InstanceGroup[]) => {
            expect(res).toEqual([instanceGroup]);
        });
        const url = `${config.apiUrl}/instance-group`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush([instanceGroup]);
    });
    it('#addInstanceGroup() should request return an Observable<InstanceGroup> object', () => {
        service.addInstanceGroup(instanceGroup).subscribe((res: InstanceGroup) => {
            expect(res).toEqual(instanceGroup);
        });
        const url = `${config.apiUrl}/instance-group`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(instanceGroup);
    });
    it('#editInstanceGroup() should request return an Observable<InstanceGroup> object', () => {
        service.editInstanceGroup(instanceGroup).subscribe((res: InstanceGroup) => {
            expect(res).toEqual(instanceGroup);
        });
        const url = `${config.apiUrl}/instance-group/1`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(instanceGroup);
    });
    it('#deleteInstanceGroup() should request return an Observable<InstanceGroup> object', () => {
        service.deleteInstanceGroup(1).subscribe((res: InstanceGroup) => {
            expect(res).toEqual(instanceGroup);
        });
        const url = `${config.apiUrl}/instance-group/1`
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}`, });
        mockRequest.flush(instanceGroup);
    });
});

