/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AttributeService } from './attribute.service';
import { AppConfigService } from 'src/app/app-config.service';
import { Attribute } from '../models';
import { ATTRIBUTE } from '../../../test-data';

describe('[Instance][Metamodel][Services] AttributeService', () => {
    let service: AttributeService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                AttributeService
            ]
        });
        service = TestBed.inject(AttributeService);
    });

    it('#retrieveAttributeList() should return an Observable<Attribute[]>',
        inject([HttpTestingController, AttributeService],(httpMock: HttpTestingController, service: AttributeService) => {
                const mockResponse = [];

                service.retrieveAttributeList('myDataset').subscribe((event: Attribute[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/attribute');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addAttribute() should return an Observable<Attribute>',
        inject([HttpTestingController, AttributeService],(httpMock: HttpTestingController, service: AttributeService) => {
                const mockResponse = ATTRIBUTE;

                service.addAttribute('myDataset', ATTRIBUTE).subscribe((event: Attribute) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/attribute');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editAttribute() should return an Observable<Attribute>',
        inject([HttpTestingController, AttributeService],(httpMock: HttpTestingController, service: AttributeService) => {
                const mockResponse = ATTRIBUTE;

                service.editAttribute('myDataset', ATTRIBUTE).subscribe((event: Attribute) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/attribute/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteAttribute() should return an Observable<object>',
        inject([HttpTestingController, AttributeService],(httpMock: HttpTestingController, service: AttributeService) => {
                const mockResponse = {};

                service.deleteAttribute('myDataset', ATTRIBUTE).subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/attribute/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
