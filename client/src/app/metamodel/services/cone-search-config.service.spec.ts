/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { ConeSearchConfig } from '../models';
import { ConeSearchConfigService } from './cone-search-config.service';

describe('[Metamodel][services] ConeSearchConfigService', () => {
    let service: ConeSearchConfigService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let coneSearchConfig: ConeSearchConfig;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                ConeSearchConfigService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(ConeSearchConfigService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        coneSearchConfig = { ...coneSearchConfig, id: 1 };
    }));

    it('#retrieveConeSearchConfig() should request return an Observable<ConeSearchConfig> object', () => {
        const mockResponse = coneSearchConfig;
        service.retrieveConeSearchConfig('test').subscribe((res: ConeSearchConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/cone-search-config`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#addConeSearchConfig() should request return an Observable<ConeSearchConfig> object', () => {
        const mockResponse = coneSearchConfig;
        service.addConeSearchConfig('test', coneSearchConfig).subscribe((res: ConeSearchConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/cone-search-config`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#editConeSearchConfig() should request return an Observable<ConeSearchConfig> object', () => {
        const mockResponse = coneSearchConfig;
        service.editConeSearchConfig('test', coneSearchConfig).subscribe((res: ConeSearchConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/cone-search-config`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
});

