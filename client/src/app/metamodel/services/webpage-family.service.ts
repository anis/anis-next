/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { WebpageFamily } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Webpage family service.
 */
@Injectable()
export class WebpageFamilyService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves webpage families for the given instance.
     *
     * @param  {string} instanceName - The instance.
     *
     * @return Observable<WebpageFamily>
     */
    retrieveWebpageFamilyList(instanceName: string): Observable<WebpageFamily[]> {
        return this.http.get<WebpageFamily[]>(`${this.config.apiUrl}/instance/${instanceName}/webpage-family`);
    }

    /**
     * Adds a new webpage family for the given instance.
     *
     * @param  {string} instanceName - The instance.
     * @param  {WebpageFamily} newWebpageFamily - The webpage family.
     *
     * @return Observable<WebpageFamily>
     */
    addWebpageFamily(instanceName: string, newWebpageFamily: WebpageFamily): Observable<WebpageFamily> {
        return this.http.post<WebpageFamily>(`${this.config.apiUrl}/instance/${instanceName}/webpage-family`, newWebpageFamily);
    }

    /**
     * Modifies a webpage family.
     *
     * @param  {WebpageFamily} webpageFamily - The webpage family.
     *
     * @return Observable<WebpageFamily>
     */
    editWebpageFamily(webpageFamily: WebpageFamily): Observable<WebpageFamily> {
        return this.http.put<WebpageFamily>(`${this.config.apiUrl}/webpage-family/${webpageFamily.id}`, webpageFamily);
    }

    /**
     * Removes a webpage family.
     *
     * @param  {number} webpageFamilyId - The webpage family ID.
     *
     * @return Observable<object>
     */
    deleteWebpageFamily(webpageFamilyId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/webpage-family/${webpageFamilyId}`);
    }
}
