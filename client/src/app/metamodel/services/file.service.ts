/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { File } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc File service.
 */
@Injectable()
export class FileService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves file list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<File[]>
     */
    retrieveFileList(datasetName: string): Observable<File[]> {
        return this.http.get<File[]>(`${this.config.apiUrl}/dataset/${datasetName}/file`);
    }

    /**
     * Adds a new file for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {File} file - The file.
     *
     * @return Observable<File>
     */
    addFile(datasetName: string, newFile: File): Observable<File> {
        return this.http.post<File>(`${this.config.apiUrl}/dataset/${datasetName}/file`, newFile);
    }

    /**
     * Modifies an file.
     *
     * @param  {File} file - The file.
     *
     * @return Observable<File>
     */
    editFile(file: File): Observable<File> {
        return this.http.put<File>(`${this.config.apiUrl}/file/${file.id}`, file);
    }

    /**
     * Removes an file.
     *
     * @param  {number} fileId - The file ID.
     *
     * @return Observable<object>
     */
    deleteFile(fileId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/file/${fileId}`);
    }
}
