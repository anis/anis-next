/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Webpage } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Webpage service.
 */
@Injectable()
export class WebpageService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves webpages for the given instance.
     *
     * @param  {string} instanceName - The instance.
     *
     * @return Observable<Webpage[]>
     */
    retrieveWebpageList(instanceName: string): Observable<Webpage[]> {
        return this.http.get<Webpage[]>(`${this.config.apiUrl}/instance/${instanceName}/webpage`);
    }

    /**
     * Adds a new webpage for the given instance.
     *
     * @param  {Webpage} newWebpage - The webpage.
     *
     * @return Observable<Webpage>
     */
    addWebpage(newWebpage: Webpage): Observable<Webpage> {
        return this.http.post<Webpage>(`${this.config.apiUrl}/webpage-family/${newWebpage.id_webpage_family}/webpage`, newWebpage);
    }

    /**
     * Modifies a webpage.
     *
     * @param  {Webpage} webpage - The Webpage.
     *
     * @return Observable<Webpage>
     */
    editWebpage(webpage: Webpage): Observable<Webpage> {
        return this.http.put<Webpage>(`${this.config.apiUrl}/webpage/${webpage.id}`, webpage);
    }

    /**
     * Removes a webpage.
     *
     * @param  {number} webpageId - The webpage ID.
     *
     * @return Observable<object>
     */
    deleteWebpage(webpageId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/webpage/${webpageId}`);
    }
}
