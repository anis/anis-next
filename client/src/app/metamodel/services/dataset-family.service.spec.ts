/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DatasetFamilyService } from './dataset-family.service';
import { AppConfigService } from 'src/app/app-config.service';
import { DatasetFamily } from '../models';
import { DATASET_FAMILY, GROUP } from '../../../test-data';

describe('[Instance][Metamodel][Services] DatasetFamilyService', () => {
    let service: DatasetFamilyService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                DatasetFamilyService
            ]
        });
        service = TestBed.inject(DatasetFamilyService);
    });

    it('#retrieveDatasetFamilyList() should return an Observable<DatasetFamily[]>',
        inject([HttpTestingController, DatasetFamilyService],(httpMock: HttpTestingController, service: DatasetFamilyService) => {
                const mockResponse = [];

                service.retrieveDatasetFamilyList('myInstance').subscribe((event: DatasetFamily[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/instance/myInstance/dataset-family');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addDatasetFamily() should return an Observable<DatasetFamily>',
        inject([HttpTestingController, DatasetFamilyService],(httpMock: HttpTestingController, service: DatasetFamilyService) => {
                const mockResponse = DATASET_FAMILY;

                service.addDatasetFamily('myInstance', DATASET_FAMILY).subscribe((event: DatasetFamily) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/instance/myInstance/dataset-family');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editDatasetFamily() should return an Observable<DatasetFamily>',
        inject([HttpTestingController, DatasetFamilyService],(httpMock: HttpTestingController, service: DatasetFamilyService) => {
                const mockResponse = DATASET_FAMILY;

                service.editDatasetFamily(DATASET_FAMILY).subscribe((event: DatasetFamily) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset-family/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteDatasetFamily() should return an Observable<object>',
        inject([HttpTestingController, DatasetFamilyService],(httpMock: HttpTestingController, service: DatasetFamilyService) => {
                const mockResponse = {};

                service.deleteDatasetFamily(1).subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset-family/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
