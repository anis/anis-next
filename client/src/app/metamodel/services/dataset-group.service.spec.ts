/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DatasetGroupService } from './dataset-group.service';
import { AppConfigService } from 'src/app/app-config.service';
import { DatasetGroup } from '../models';
import { GROUP } from '../../../test-data';

describe('[Instance][Metamodel][Services] DatasetGroupService', () => {
    let service: DatasetGroupService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                DatasetGroupService
            ]
        });
        service = TestBed.inject(DatasetGroupService);
    });

    it('#retrieveDatasetGroupList() should return an Observable<Group[]>',
        inject([HttpTestingController, DatasetGroupService],(httpMock: HttpTestingController, service: DatasetGroupService) => {
                const mockResponse = [];

                service.retrieveDatasetGroupList('myInstance').subscribe((event: DatasetGroup[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/instance/myInstance/dataset-group');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addDatasetGroup() should return an Observable<DatasetGroup>',
        inject([HttpTestingController, DatasetGroupService],(httpMock: HttpTestingController, service: DatasetGroupService) => {
                const mockResponse = GROUP;

                service.addDatasetGroup('myInstance', GROUP).subscribe((event: DatasetGroup) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/instance/myInstance/dataset-group');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editDatasetGroup() should return an Observable<DatasetGroup>',
        inject([HttpTestingController, DatasetGroupService],(httpMock: HttpTestingController, service: DatasetGroupService) => {
                const mockResponse = GROUP;

                service.editDatasetGroup(GROUP).subscribe((event: DatasetGroup) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset-group/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteDatasetGroup() should return an Observable<object>',
        inject([HttpTestingController, DatasetGroupService],(httpMock: HttpTestingController, service: DatasetGroupService) => {
                const mockResponse = {};

                service.deleteDatasetGroup(1).subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset-group/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
