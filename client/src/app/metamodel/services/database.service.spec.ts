/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DatabaseService } from './database.service';
import { AppConfigService } from 'src/app/app-config.service';
import { Database } from '../models';
import { DATABASE } from '../../../test-data';

describe('[Instance][Metamodel][Services] DatabaseService', () => {
    let service: DatabaseService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                DatabaseService
            ]
        });
        service = TestBed.inject(DatabaseService);
    });

    it('#retrieveDatabaseList() should return an Observable<Database[]>',
        inject([HttpTestingController, DatabaseService],(httpMock: HttpTestingController, service: DatabaseService) => {
                const mockResponse = [];

                service.retrieveDatabaseList().subscribe((event: Database[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/database');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addDatabase() should return an Observable<Database>',
        inject([HttpTestingController, DatabaseService],(httpMock: HttpTestingController, service: DatabaseService) => {
                const mockResponse = DATABASE;

                service.addDatabase(DATABASE).subscribe((event: Database) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/database');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editDatabase() should return an Observable<Database>',
        inject([HttpTestingController, DatabaseService],(httpMock: HttpTestingController, service: DatabaseService) => {
                const mockResponse = DATABASE;

                service.editDatabase(DATABASE).subscribe((event: Database) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/database/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteDatabase() should return an Observable<object>',
        inject([HttpTestingController, DatabaseService],(httpMock: HttpTestingController, service: DatabaseService) => {
                const mockResponse = {};

                service.deleteDatabase(1).subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/database/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
