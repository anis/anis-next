/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { ConeSearchConfig } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc cone-search configuration service.
 */
@Injectable()
export class ConeSearchConfigService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves cone-search configuration
     *
     * @return Observable<ConeSearchConfig>
     */
    retrieveConeSearchConfig(datasetName: string): Observable<ConeSearchConfig> {
        return this.http.get<ConeSearchConfig>(`${this.config.apiUrl}/dataset/${datasetName}/cone-search-config`);
    }

    /**
     * Adds a new cone-search configuration for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {ConeSearch} newConeSearchConfig - The cone-search configuration.
     *
     * @return Observable<ConeSearchConfig>
     */
    addConeSearchConfig(datasetName: string, newConeSearchConfig: ConeSearchConfig): Observable<ConeSearchConfig> {
        return this.http.post<ConeSearchConfig>(`${this.config.apiUrl}/dataset/${datasetName}/cone-search-config`, newConeSearchConfig);
    }

    /**
     * Modifies cone-search configuration.
     *
     * @param  {ConeSearch} coneSearchConfig - The cone-search cofiguration.
     *
     * @return Observable<ConeSearchConfig>
     */
    editConeSearchConfig(datasetName: string, coneSearchConfig: ConeSearchConfig): Observable<ConeSearchConfig> {
        return this.http.put<ConeSearchConfig>(`${this.config.apiUrl}/dataset/${datasetName}/cone-search-config`, coneSearchConfig);
    }
}
