/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { AliasConfigService } from './alias-config.service';
import { AliasConfig } from '../models';

describe('[Metamodel][services] AliasConfigService', () => {
    let service: AliasConfigService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let aliasConfig: AliasConfig;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                AliasConfigService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(AliasConfigService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        aliasConfig = { column_alias: 'test', column_alias_long: 'test', column_name: 'test', table_alias: 'test' };
    }));

    it('#retrieveAliasConfig() should request return an Observable<AliasCongig> object', () => {
        const mockResponse = aliasConfig;
        service.retrieveAliasConfig('test').subscribe((res: AliasConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/alias-config`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#addAliasConfig() should request return an Observable<AliasCongig> object', () => {
        const mockResponse = aliasConfig;
        service.addAliasConfig('test', aliasConfig).subscribe((res: AliasConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/alias-config`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#editAliasConfig() should request return an Observable<AliasCongig> object', () => {
        const mockResponse = aliasConfig;
        service.editAliasConfig('test', aliasConfig).subscribe((res: AliasConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/alias-config`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
});

