/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { OutputFamilyService } from './output-family.service';
import { AppConfigService } from 'src/app/app-config.service';
import { OutputFamily } from '../models';
import { OUTPUT_FAMILY } from '../../../test-data';

describe('[Instance][Metamodel][Services] OutputFamilyService', () => {
    let service: OutputFamilyService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                OutputFamilyService
            ]
        });
        service = TestBed.inject(OutputFamilyService);
    });

    it('#retrieveOutputFamilyList() should return an Observable<OutputFamily[]>',
        inject([HttpTestingController, OutputFamilyService],(httpMock: HttpTestingController, service: OutputFamilyService) => {
                const mockResponse = [];

                service.retrieveOutputFamilyList('myDataset').subscribe((event: OutputFamily[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/output-family');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addOutputFamily() should return an Observable<OutputFamily>',
        inject([HttpTestingController, OutputFamilyService],(httpMock: HttpTestingController, service: OutputFamilyService) => {
                const mockResponse = OUTPUT_FAMILY;

                service.addOutputFamily('myDataset', OUTPUT_FAMILY).subscribe((event: OutputFamily) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/output-family');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editOutputFamily() should return an Observable<OutputFamily>',
        inject([HttpTestingController, OutputFamilyService],(httpMock: HttpTestingController, service: OutputFamilyService) => {
                const mockResponse = OUTPUT_FAMILY;

                service.editOutputFamily(OUTPUT_FAMILY).subscribe((event: OutputFamily) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/output-family/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteOutputFamily() should return an Observable<object>',
        inject([HttpTestingController, OutputFamilyService],(httpMock: HttpTestingController, service: OutputFamilyService) => {
                const mockResponse = {};

                service.deleteOutputFamily(1).subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/output-family/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
