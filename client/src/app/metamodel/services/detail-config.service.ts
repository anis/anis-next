/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { DetailConfig } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc detail configuration service.
 */
@Injectable()
export class DetailConfigService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves detail configuration
     *
     * @return Observable<DetailConfig>
     */
    retrieveDetailConfig(datasetName: string): Observable<DetailConfig> {
        return this.http.get<DetailConfig>(`${this.config.apiUrl}/dataset/${datasetName}/detail-config`);
    }

    /**
     * Adds a new detail configuration for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {Detail} newDetailConfig - The detail configuration.
     *
     * @return Observable<DetailConfig>
     */
    addDetailConfig(datasetName: string, newDetailConfig: DetailConfig): Observable<DetailConfig> {
        return this.http.post<DetailConfig>(`${this.config.apiUrl}/dataset/${datasetName}/detail-config`, newDetailConfig);
    }

    /**
     * Modifies detail configuration.
     *
     * @param  {Detail} detailConfig - The detail cofiguration.
     *
     * @return Observable<DetailConfig>
     */
    editDetailConfig(datasetName: string, detailConfig: DetailConfig): Observable<DetailConfig> {
        return this.http.put<DetailConfig>(`${this.config.apiUrl}/dataset/${datasetName}/detail-config`, detailConfig);
    }
}
