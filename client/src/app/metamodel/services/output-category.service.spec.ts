/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { OutputCategoryService } from './output-category.service';
import { AppConfigService } from 'src/app/app-config.service';
import { OutputCategory, OutputFamily } from '../models';
import { CATEGORY, OUTPUT_FAMILY } from '../../../test-data';

describe('[Instance][Metamodel][Services] OutputCategoryService', () => {
    let service: OutputCategoryService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                OutputCategoryService
            ]
        });
        service = TestBed.inject(OutputCategoryService);
    });

    it('#retrieveOutputCategoryList() should return an Observable<OutputCategory[]>',
        inject([HttpTestingController, OutputCategoryService],(httpMock: HttpTestingController, service: OutputCategoryService) => {
                const mockResponse = [];

                service.retrieveOutputCategoryList('myDataset').subscribe((event: OutputCategory[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/output-category');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addOutputCategory() should return an Observable<OutputCategory>',
        inject([HttpTestingController, OutputCategoryService],(httpMock: HttpTestingController, service: OutputCategoryService) => {
                const mockResponse = CATEGORY;

                service.addOutputCategory(CATEGORY).subscribe((event: OutputCategory) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/output-family/1/output-category');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editOutputCategory() should return an Observable<OutputCategory>',
        inject([HttpTestingController, OutputCategoryService],(httpMock: HttpTestingController, service: OutputCategoryService) => {
                const mockResponse = CATEGORY;

                service.editOutputCategory(CATEGORY).subscribe((event: OutputCategory) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/output-category/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteOutputCategory() should return an Observable<object>',
        inject([HttpTestingController, OutputCategoryService],(httpMock: HttpTestingController, service: OutputCategoryService) => {
                const mockResponse = {};

                service.deleteOutputCategory(1).subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/output-category/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
