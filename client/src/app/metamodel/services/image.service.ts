/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Image } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Image service.
 */
@Injectable()
export class ImageService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves image list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<Image[]>
     */
    retrieveImageList(datasetName: string): Observable<Image[]> {
        return this.http.get<Image[]>(`${this.config.apiUrl}/dataset/${datasetName}/image`);
    }

    /**
     * Adds a new image for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {Image} image - The image.
     *
     * @return Observable<Image>
     */
    addImage(datasetName: string, newImage: Image): Observable<Image> {
        return this.http.post<Image>(`${this.config.apiUrl}/dataset/${datasetName}/image`, newImage);
    }

    /**
     * Modifies an image.
     *
     * @param  {Image} image - The image.
     *
     * @return Observable<Image>
     */
    editImage(image: Image): Observable<Image> {
        return this.http.put<Image>(`${this.config.apiUrl}/image/${image.id}`, image);
    }

    /**
     * Removes an image.
     *
     * @param  {number} imageId - The image ID.
     *
     * @return Observable<object>
     */
    deleteImage(imageId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/image/${imageId}`);
    }
}
