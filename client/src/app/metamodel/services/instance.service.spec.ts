/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { InstanceService } from './instance.service';
import { Instance } from '../models';

describe('[Metamodel][services] InstanceService', () => {
    let service: InstanceService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let instance: Instance;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                InstanceService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(InstanceService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        instance = { ...instance, name: 'test' };
    }));

    it('#retrieveInstanceList() should request return an Observable<Instance[]> object', () => {
        service.retrieveInstanceList().subscribe((res: Instance[]) => {
            expect(res).toEqual([instance]);
        });
        const url = `${config.apiUrl}/instance`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush([instance]);

    });
    it('#addInstance() should request return an Observable<InstanceGroup> object', () => {
        service.addInstance(instance).subscribe((res: Instance) => {
            expect(res).toEqual(instance);
        });
        const url = `${config.apiUrl}/instance`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });

        mockRequest.flush(instance);
    });
    it('#editInstance() should request return an Observable<Instance> object', () => {
        service.editInstance(instance).subscribe((res: Instance) => {
            expect(res).toEqual(instance);
        });
        const url = `${config.apiUrl}/instance/test`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(instance);
    });
    it('#deleteInstanceGroup() should request return an Observable<Instance> object', () => {
        service.deleteInstance('test').subscribe((res: Instance) => {
            expect(res).toEqual(instance);
        });
        const url = `${config.apiUrl}/instance/test`
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}`, });
        mockRequest.flush(instance);
    });
});
