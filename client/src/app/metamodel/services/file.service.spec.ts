/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { FileService } from './file.service';
import { File } from '../models';

describe('[Metamodel][services] FileService', () => {
    let service: FileService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let file: File;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                FileService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(FileService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        file = { ...file, id: 1 };
    }));

    it('#retrieveFileList() should request return an Observable<File[]> object', () => {
        const mockResponse = file;
        service.retrieveFileList('test').subscribe((res: File[]) => {
            expect(res).toEqual([mockResponse]);
        });
        const url = `${config.apiUrl}/dataset/test/file`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush([mockResponse]);
    });
    it('#addFile() should request return an Observable<File> object', () => {
        const mockResponse = file;
        service.addFile('test', file).subscribe((res: File) => {
            expect(res).toEqual(file);
        });
        const url = `${config.apiUrl}/dataset/test/file`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(file);
    });
    it('#editFile() should request return an Observable<File> object', () => {
        const mockResponse = file;
        service.editFile(file).subscribe((res: File) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/file/1`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#deleteFile() should request return an Observable<File> object', () => {
        const mockResponse = file;
        service.deleteFile(1).subscribe((res: File) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/file/1`
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
});

