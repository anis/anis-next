/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CriteriaFamilyService } from './criteria-family.service';
import { AppConfigService } from 'src/app/app-config.service';
import { CriteriaFamily } from '../models';
import { CRITERIA_FAMILY } from '../../../test-data';

describe('[Instance][Metamodel][Services] CriteriaFamilyService', () => {
    let service: CriteriaFamilyService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                CriteriaFamilyService
            ]
        });
        service = TestBed.inject(CriteriaFamilyService);
    });

    it('#retrieveCriteriaFamilyList() should return an Observable<CriteriaFamily[]>',
        inject([HttpTestingController, CriteriaFamilyService],(httpMock: HttpTestingController, service: CriteriaFamilyService) => {
                const mockResponse = [];

                service.retrieveCriteriaFamilyList('myDataset').subscribe((event: CriteriaFamily[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/criteria-family');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addCriteriaFamily() should return an Observable<CriteriaFamily>',
        inject([HttpTestingController, CriteriaFamilyService],(httpMock: HttpTestingController, service: CriteriaFamilyService) => {
                const mockResponse = CRITERIA_FAMILY;

                service.addCriteriaFamily('myDataset', CRITERIA_FAMILY).subscribe((event: CriteriaFamily) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/criteria-family');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editCriteriaFamily() should return an Observable<CriteriaFamily>',
        inject([HttpTestingController, CriteriaFamilyService],(httpMock: HttpTestingController, service: CriteriaFamilyService) => {
                const mockResponse = CRITERIA_FAMILY;

                service.editCriteriaFamily(CRITERIA_FAMILY).subscribe((event: CriteriaFamily) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/criteria-family/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteCriteriaFamily() should return an Observable<object>',
        inject([HttpTestingController, CriteriaFamilyService],(httpMock: HttpTestingController, service: CriteriaFamilyService) => {
                const mockResponse = {};

                service.deleteCriteriaFamily(1).subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/criteria-family/1');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
