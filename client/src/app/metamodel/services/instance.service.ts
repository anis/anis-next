/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Instance } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

@Injectable()
export class InstanceService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    retrieveInstanceList(): Observable<Instance[]> {
        return this.http.get<Instance[]>(`${this.config.apiUrl}/instance`);
    }

    addInstance(newInstance: Instance): Observable<Instance> {
        return this.http.post<Instance>(`${this.config.apiUrl}/instance`, newInstance);
    }

    editInstance(instance: Instance): Observable<Instance> {
        return this.http.put<Instance>(`${this.config.apiUrl}/instance/${instance.name}`, instance);
    }

    deleteInstance(instanceName: string) {
        return this.http.delete(`${this.config.apiUrl}/instance/${instanceName}`);
    }
}
