/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AliasConfig } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Alias configuration service.
 */
@Injectable()
export class AliasConfigService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves alias configuration
     *
     * @return Observable<AliasConfig>
     */
    retrieveAliasConfig(datasetName: string): Observable<AliasConfig> {
        return this.http.get<AliasConfig>(`${this.config.apiUrl}/dataset/${datasetName}/alias-config`);
    }

    /**
     * Adds a new alias configuration for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {Alias} newAliasConfig - The alias configuration.
     *
     * @return Observable<AliasConfig>
     */
    addAliasConfig(datasetName: string, newAliasConfig: AliasConfig): Observable<AliasConfig> {
        return this.http.post<AliasConfig>(`${this.config.apiUrl}/dataset/${datasetName}/alias-config`, newAliasConfig);
    }

    /**
     * Modifies alias configuration.
     *
     * @param  {Alias} aliasConfig - The alias cofiguration.
     *
     * @return Observable<AliasConfig>
     */
    editAliasConfig(datasetName: string, aliasConfig: AliasConfig): Observable<AliasConfig> {
        return this.http.put<AliasConfig>(`${this.config.apiUrl}/dataset/${datasetName}/alias-config`, aliasConfig);
    }
}
