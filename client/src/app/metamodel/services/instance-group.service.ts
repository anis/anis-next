/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { InstanceGroup } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc InstanceGroup service.
 */
@Injectable()
export class InstanceGroupService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves instance group list
     *
     * @return Observable<InstanceGroup[]>
     */
    retrieveInstanceGroupList(): Observable<InstanceGroup[]> {
        return this.http.get<InstanceGroup[]>(`${this.config.apiUrl}/instance-group`);
    }

    /**
     * Adds a new instance group
     *
     * @param  {InstanceGroup} newInstanceGroup - The instance group.
     *
     * @return Observable<InstanceGroup>
     */
    addInstanceGroup(newInstanceGroup: InstanceGroup): Observable<InstanceGroup> {
        return this.http.post<InstanceGroup>(`${this.config.apiUrl}/instance-group`, newInstanceGroup);
    }

    /**
     * Modifies an instance group.
     *
     * @param  {InstanceGroup} instanceGroup - The instance group.
     *
     * @return Observable<InstanceGroup>
     */
    editInstanceGroup(instanceGroup: InstanceGroup): Observable<InstanceGroup> {
        return this.http.put<InstanceGroup>(`${this.config.apiUrl}/instance-group/${instanceGroup.id}`, instanceGroup);
    }

    /**
     * Removes an instance group.
     *
     * @param  {number} instanceGroupId - The instance Group ID.
     *
     * @return Observable<object>
     */
    deleteInstanceGroup(instanceGroupId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/instance-group/${instanceGroupId}`);
    }
}
