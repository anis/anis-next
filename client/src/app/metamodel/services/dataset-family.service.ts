/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { DatasetFamily } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Dataset family service.
 */
@Injectable()
export class DatasetFamilyService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves dataset families for the given instance.
     *
     * @param  {string} instanceName - The instance.
     *
     * @return Observable<DatasetFamily>
     */
    retrieveDatasetFamilyList(instanceName: string): Observable<DatasetFamily[]> {
        return this.http.get<DatasetFamily[]>(`${this.config.apiUrl}/instance/${instanceName}/dataset-family`);
    }

    /**
     * Adds a new dataset family for the given instance.
     *
     * @param  {string} instanceName - The instance.
     * @param  {DatasetFamily} newDatasetFamily - The dataset family.
     *
     * @return Observable<DatasetFamily>
     */
    addDatasetFamily(instanceName: string, newDatasetFamily: DatasetFamily): Observable<DatasetFamily> {
        return this.http.post<DatasetFamily>(`${this.config.apiUrl}/instance/${instanceName}/dataset-family`, newDatasetFamily);
    }

    /**
     * Modifies a dataset family.
     *
     * @param  {DatasetFamily} datasetFamily - The dataset family.
     *
     * @return Observable<DatasetFamily>
     */
    editDatasetFamily(datasetFamily: DatasetFamily): Observable<DatasetFamily> {
        return this.http.put<DatasetFamily>(`${this.config.apiUrl}/dataset-family/${datasetFamily.id}`, datasetFamily);
    }

    /**
     * Removes a dataset family.
     *
     * @param  {number} datasetFamilyId - The dataset family ID.
     *
     * @return Observable<object>
     */
    deleteDatasetFamily(datasetFamilyId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/dataset-family/${datasetFamilyId}`);
    }
}
