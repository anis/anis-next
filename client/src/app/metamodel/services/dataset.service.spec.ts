/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DatasetService } from './dataset.service';
import { AppConfigService } from 'src/app/app-config.service';
import { Dataset } from '../models';
import { DATASET } from '../../../test-data';

describe('[Instance][Metamodel][Services] DatasetService', () => {
    let service: DatasetService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                DatasetService
            ]
        });
        service = TestBed.inject(DatasetService);
    });

    it('#retrieveDatasetList() should return an Observable<Dataset[]>',
        inject([HttpTestingController, DatasetService],(httpMock: HttpTestingController, service: DatasetService) => {
                const mockResponse = [];

                service.retrieveDatasetList('myInstance').subscribe((event: Dataset[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/instance/myInstance/dataset');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#addDataset() should return an Observable<Dataset>',
        inject([HttpTestingController, DatasetService],(httpMock: HttpTestingController, service: DatasetService) => {
                const mockResponse = DATASET;

                service.addDataset(DATASET).subscribe((event: Dataset) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset-family/1/dataset');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('POST');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#editDataset() should return an Observable<Dataset>',
        inject([HttpTestingController, DatasetService],(httpMock: HttpTestingController, service: DatasetService) => {
                const mockResponse = DATASET;

                service.editDataset(DATASET).subscribe((event: Dataset) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('PUT');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );

    it('#deleteDataset() should return an Observable<object>',
        inject([HttpTestingController, DatasetService],(httpMock: HttpTestingController, service: DatasetService) => {
                const mockResponse = {};

                service.deleteDataset('myDataset').subscribe((event: object) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('DELETE');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
