/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Dataset } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Dataset service.
 */
@Injectable()
export class DatasetService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves dataset list for the given instance.
     *
     * @param  {string} instanceName - The instance name.
     *
     * @return Observable<Dataset[]>
     */
    retrieveDatasetList(instanceName: string): Observable<Dataset[]> {
        return this.http.get<Dataset[]>(`${this.config.apiUrl}/instance/${instanceName}/dataset`);
    }

    /**
     * Adds a new dataset.
     *
     * @param  {Dataset} newDataset - The dataset.
     *
     * @return Observable<Dataset>
     */
    addDataset(newDataset: Dataset): Observable<Dataset> {
        return this.http.post<Dataset>(`${this.config.apiUrl}/dataset-family/${newDataset.id_dataset_family}/dataset`, newDataset);
    }

    /**
     * Modifies a dataset.
     *
     * @param  {Dataset} dataset - The dataset.
     *
     * @return Observable<Dataset>
     */
    editDataset(dataset: Dataset): Observable<Dataset> {
        return this.http.put<Dataset>(`${this.config.apiUrl}/dataset/${dataset.name}`, dataset);
    }

    /**
     * Removes a dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<object>
     */
    deleteDataset(datasetName: string): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/dataset/${datasetName}`);
    }
}
