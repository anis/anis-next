/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Database } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Database service.
 */
@Injectable()
export class DatabaseService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves database list.
     *
     * @return Observable<Database[]>
     */
    retrieveDatabaseList(): Observable<Database[]> {
        return this.http.get<Database[]>(`${this.config.apiUrl}/database`);
    }

    /**
     * Adds a new database.
     *
     * @param  {Database} newDatabase - The database.
     *
     * @return Observable<Database>
     */
    addDatabase(newDatabase: Database): Observable<Database> {
        return this.http.post<Database>(`${this.config.apiUrl}/database`, newDatabase);
    }

    /**
     * Modifies a database.
     *
     * @param  {Database} database - The database.
     *
     * @return Observable<Database>
     */
    editDatabase(database: Database): Observable<Database> {
        return this.http.put<Database>(`${this.config.apiUrl}/database/${database.id}`, database);
    }

    /**
     * Removes a database.
     *
     * @param  {number} databaseId - The database ID.
     *
     * @return Observable<object>
     */
    deleteDatabase(databaseId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/database/${databaseId}`);
    }
}
