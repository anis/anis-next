/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatabaseService } from './database.service';
import { DatasetGroupService } from './dataset-group.service';
import { InstanceGroupService } from './instance-group.service';
import { DatasetService } from './dataset.service';
import { DatasetFamilyService } from './dataset-family.service';
import { InstanceService } from './instance.service';
import { AttributeService } from './attribute.service';
import { CriteriaFamilyService } from './criteria-family.service';
import { OutputCategoryService } from './output-category.service';
import { OutputFamilyService } from './output-family.service';
import { ImageService } from './image.service';
import { FileService } from './file.service';
import { ConeSearchConfigService } from './cone-search-config.service';
import { DetailConfigService } from './detail-config.service';
import { AliasConfigService } from './alias-config.service';
import { WebpageFamilyService } from './webpage-family.service';
import { WebpageService } from './webpage.service';

export const metamodelServices = [
    DatabaseService,
    DatasetGroupService,
    InstanceGroupService,
    DatasetService,
    DatasetFamilyService,
    InstanceService,
    AttributeService,
    CriteriaFamilyService,
    OutputCategoryService,
    OutputFamilyService,
    ImageService,
    FileService,
    ConeSearchConfigService,
    DetailConfigService,
    AliasConfigService,
    WebpageFamilyService,
    WebpageService
];
