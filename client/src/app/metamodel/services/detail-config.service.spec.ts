/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { DetailConfigService } from './detail-config.service';
import { DetailConfig } from '../models';

describe('[Metamodel][services] DetailConfigService', () => {
    let service: DetailConfigService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let detailConfig: DetailConfig;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DetailConfigService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(DetailConfigService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        detailConfig = { ...detailConfig, id: 1 };
    }));

    it('#retrieveDetailConfig() should request return an Observable<ConeSearchConfig> object', () => {
        const mockResponse = detailConfig;
        service.retrieveDetailConfig('test').subscribe((res: DetailConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/detail-config`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#addDetailConfig() should request return an Observable<ConeSearchConfig> object', () => {
        const mockResponse = detailConfig;
        service.addDetailConfig('test', detailConfig).subscribe((res: DetailConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/detail-config`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#editDetailConfig() should request return an Observable<ConeSearchConfig> object', () => {
        const mockResponse = detailConfig;
        service.editDetailConfig('test', detailConfig).subscribe((res: DetailConfig) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/detail-config`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
});

