/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { DatasetGroup } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Group service.
 */
@Injectable()
export class DatasetGroupService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves group list for the given instance.
     *
     * @param  {string} instanceName - The instance.
     *
     * @return Observable<Group[]>
     */
    retrieveDatasetGroupList(instanceName: string): Observable<DatasetGroup[]> {
        return this.http.get<DatasetGroup[]>(`${this.config.apiUrl}/instance/${instanceName}/dataset-group`);
    }

    /**
     * Adds a new dataset group for the given instance.
     *
     * @param  {string} instanceName - The instance.
     * @param  {DatasetGroup} newDatasetGroup - The dataset group.
     *
     * @return Observable<DatasetGroup>
     */
    addDatasetGroup(instanceName: string, newDatasetGroup: DatasetGroup): Observable<DatasetGroup> {
        return this.http.post<DatasetGroup>(`${this.config.apiUrl}/instance/${instanceName}/dataset-group`, newDatasetGroup);
    }

    /**
     * Modifies a dataset group.
     *
     * @param  {DatasetGroup} datasetGroup - The dataset group.
     *
     * @return Observable<DatasetGroup>
     */
    editDatasetGroup(datasetGroup: DatasetGroup): Observable<DatasetGroup> {
        return this.http.put<DatasetGroup>(`${this.config.apiUrl}/dataset-group/${datasetGroup.id}`, datasetGroup);
    }

    /**
     * Removes a dataset group.
     *
     * @param  {number} datasetGroupId - The dataset group ID.
     *
     * @return Observable<object>
     */
    deleteDatasetGroup(datasetGroupId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/dataset-group/${datasetGroupId}`);
    }
}
