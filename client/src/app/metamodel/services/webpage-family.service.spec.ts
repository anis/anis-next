/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { WebpageFamily } from '../models';
import { WebpageFamilyService } from './webpage-family.service';

describe('[Metamodel][services] WebpageFamilyService', () => {
    let service: WebpageFamilyService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let webpageFamily: WebpageFamily;
    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                WebpageFamilyService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(WebpageFamilyService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        webpageFamily = { ...webpageFamily, id: 1 };
    }));

    it('#retrieveWebpageFamilyList() should request return an Observable<WebpageFamily[]> object', () => {
        service.retrieveWebpageFamilyList('test').subscribe((res: WebpageFamily[]) => {
            expect(res).toEqual([webpageFamily]);
        });
        const url = `${config.apiUrl}/instance/test/webpage-family`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush([webpageFamily]);
    });
    it('#addWebpageFamily() should request return an Observable<WebpageFamily> object', () => {
        service.addWebpageFamily('test', webpageFamily).subscribe((res: WebpageFamily) => {
            expect(res).toEqual(webpageFamily);
        });
        const url = `${config.apiUrl}/instance/test/webpage-family`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(webpageFamily);
    });
    it('#editWebpageFamily() should request return an Observable<WebpageFamily> object', () => {
        service.editWebpageFamily(webpageFamily).subscribe((res: WebpageFamily) => {
            expect(res).toEqual(webpageFamily);
        });
        const url = `${config.apiUrl}/webpage-family/1`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(webpageFamily);
    });
    it('#deleteWebpageFamily() should request return an Observable<object> ', () => {
        service.deleteWebpageFamily(1).subscribe((res: WebpageFamily) => {
            expect(res).toEqual(webpageFamily);
        });
        const url = `${config.apiUrl}/webpage-family/1`
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}`, });
        mockRequest.flush(webpageFamily);
    });
});

