/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Attribute } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Attribute service.
 */
@Injectable()
export class AttributeService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves attribute list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<Attribute[]>
     */
    retrieveAttributeList(datasetName: string): Observable<Attribute[]> {
        return this.http.get<Attribute[]>(`${this.config.apiUrl}/dataset/${datasetName}/attribute`);
    }

    /**
     * Adds a new attribute to the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {Attribute} attribute - The attribute.
     *
     * @return Observable<Attribute>
     */
    addAttribute(datasetName: string, attribute: Attribute): Observable<Attribute> {
        return this.http.post<Attribute>(`${this.config.apiUrl}/dataset/${datasetName}/attribute`, attribute);
    }

    /**
     * Modifies an attribute to the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {Attribute} attribute - The attribute.
     *
     * @return Observable<Attribute>
     */
    editAttribute(datasetName: string, attribute: Attribute): Observable<Attribute> {
        return this.http.put<Attribute>(`${this.config.apiUrl}/dataset/${datasetName}/attribute/${attribute.id}`, attribute);
    }

    /**
     * Removes an attribute to the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {Attribute} attribute - The attribute.
     *
     * @return Observable<object>
     */
    deleteAttribute(datasetName: string, attribute: Attribute): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/dataset/${datasetName}/attribute/${attribute.id}`);
    }
}
