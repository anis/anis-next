/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { OutputFamily } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Output family service.
 */
@Injectable()
export class OutputFamilyService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves output family list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<OutputFamily[]>
     */
    retrieveOutputFamilyList(datasetName: string): Observable<OutputFamily[]> {
        return this.http.get<OutputFamily[]>(`${this.config.apiUrl}/dataset/${datasetName}/output-family`);
    }

    /**
     * Adds a new output family for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {OutputFamily} newOutputFamily - The output family.
     *
     * @return Observable<OutputFamily>
     */
    addOutputFamily(datasetName: string, newOutputFamily: OutputFamily): Observable<OutputFamily> {
        return this.http.post<OutputFamily>(`${this.config.apiUrl}/dataset/${datasetName}/output-family`, newOutputFamily);
    }

    /**
     * Modifies an output family.
     *
     * @param  {OutputFamily} outputFamily - The output family.
     *
     * @return Observable<OutputFamily>
     */
    editOutputFamily(outputFamily: OutputFamily): Observable<OutputFamily> {
        return this.http.put<OutputFamily>(`${this.config.apiUrl}/output-family/${outputFamily.id}`, outputFamily);
    }

    /**
     * Removes an output family.
     *
     * @param  {number} outputFamilyId - The output family ID.
     *
     * @return Observable<object>
     */
    deleteOutputFamily(outputFamilyId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/output-family/${outputFamilyId}`);
    }
}
