/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { CriteriaFamily } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Criteria family service.
 */
@Injectable()
export class CriteriaFamilyService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves criteria family list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<CriteriaFamily[]>
     */
    retrieveCriteriaFamilyList(datasetName: string): Observable<CriteriaFamily[]> {
        return this.http.get<CriteriaFamily[]>(`${this.config.apiUrl}/dataset/${datasetName}/criteria-family`);
    }

    /**
     * Adds a new criteria family for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {CriteriaFamily} newCriteriaFamily - The criteria family.
     *
     * @return Observable<CriteriaFamily>
     */
    addCriteriaFamily(datasetName: string, newCriteriaFamily: CriteriaFamily): Observable<CriteriaFamily> {
        return this.http.post<CriteriaFamily>(`${this.config.apiUrl}/dataset/${datasetName}/criteria-family`, newCriteriaFamily);
    }

    /**
     * Modifies a criteria family.
     *
     * @param  {CriteriaFamily} criteriaFamily - The criteria family.
     *
     * @return Observable<CriteriaFamily>
     */
    editCriteriaFamily(criteriaFamily: CriteriaFamily): Observable<CriteriaFamily> {
        return this.http.put<CriteriaFamily>(`${this.config.apiUrl}/criteria-family/${criteriaFamily.id}`, criteriaFamily);
    }

    /**
     * Removes a criteria family.
     *
     * @param  {number} criteriaFamilyId - The criteria family ID.
     *
     * @return Observable<object>
     */
    deleteCriteriaFamily(criteriaFamilyId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/criteria-family/${criteriaFamilyId}`);
    }
}
