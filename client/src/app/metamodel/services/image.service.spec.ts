/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { ImageService } from './image.service';
import { Image } from '../models';

describe('[Metamodel][services] ImageService', () => {
    let service: ImageService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let image: Image;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                ImageService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(ImageService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        image = { ...image, id: 1 };
    }));

    it('#retrieveImageList() should request return an Observable<Image[]> object', () => {
        const mockResponse = image;
        service.retrieveImageList('test').subscribe((res: Image[]) => {
            expect(res).toEqual([mockResponse]);
        });
        const url = `${config.apiUrl}/dataset/test/image`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush([mockResponse]);
    });
    it('#addImage() should request return an Observable<Image> object', () => {
        const mockResponse = image;
        service.addImage('test', image).subscribe((res: Image) => {
            expect(res).toEqual(mockResponse);
        });
        const url = `${config.apiUrl}/dataset/test/image`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(mockResponse);
    });
    it('#editImage() should request return an Observable<Image> object', () => {
        service.editImage(image).subscribe((res: Image) => {
            expect(res).toEqual(image);
        });
        const url = `${config.apiUrl}/image/1`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(image);
    });
    it('#deleteImage() should request return an Observable<File> object', () => {
        service.deleteImage(1).subscribe((res: Image) => {
            expect(res).toEqual(image);
        });
        const url = `${config.apiUrl}/image/1`
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}`, });
        mockRequest.flush(image);
    });
});

