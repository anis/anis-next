/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { OutputCategory } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Output category service.
 */
@Injectable()
export class OutputCategoryService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves output category list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<OutputCategory[]>
     */
    retrieveOutputCategoryList(datasetName: string): Observable<OutputCategory[]> {
        return this.http.get<OutputCategory[]>(`${this.config.apiUrl}/dataset/${datasetName}/output-category`);
    }

    /**
     * Adds a new output category.
     *
     * @param  {OutputCategory} newOutputCategory - The output category.
     *
     * @return Observable<OutputCategory>
     */
    addOutputCategory(newOutputCategory: OutputCategory): Observable<OutputCategory> {
        return this.http.post<OutputCategory>(`${this.config.apiUrl}/output-family/${newOutputCategory.id_output_family}/output-category`, newOutputCategory);
    }

    /**
     * Modifies an output category.
     *
     * @param  {OutputCategory} outputCategory - The output category.
     *
     * @return Observable<OutputCategory>
     */
    editOutputCategory(outputCategory: OutputCategory): Observable<OutputCategory> {
        return this.http.put<OutputCategory>(`${this.config.apiUrl}/output-category/${outputCategory.id}`, outputCategory);
    }

    /**
     * Removes an output category.
     *
     * @param  {number} outputCategoryId - The output category ID.
     *
     * @return Observable<object>
     */
    deleteOutputCategory(outputCategoryId: number): Observable<object> {
        return this.http.delete(`${this.config.apiUrl}/output-category/${outputCategoryId}`);
    }
}
