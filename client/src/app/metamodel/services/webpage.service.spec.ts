/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from '../../app-config.service';
import { Webpage } from '../models';
import { WebpageService } from './webpage.service';

describe('[Metamodel][services] WebpageFamilyService', () => {
    let service: WebpageService;
    let httpController: HttpTestingController;
    let config: AppConfigService;
    let webpage: Webpage;
    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                WebpageService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'test.fr',

                    }
                },
            ]
        });
        service = TestBed.inject(WebpageService);
        config = TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        webpage = { ...webpage, id: 1, id_webpage_family: 2 };
    }));

    it('#retrieveWebpageList() should request return an Observable<Webpage[]> object', () => {
        service.retrieveWebpageList('test').subscribe((res: Webpage[]) => {
            expect(res).toEqual([webpage]);
        });
        const url = `${config.apiUrl}/instance/test/webpage`
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        mockRequest.flush([webpage]);
    });
    it('#addWebpageFamily() should request return an Observable<Webpage> object', () => {
        service.addWebpage(webpage).subscribe((res: Webpage) => {
            expect(res).toEqual(webpage);
        });
        const url = `${config.apiUrl}/webpage-family/2/webpage`
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}`, });
        mockRequest.flush(webpage);
    });
    it('#editWebpageFamily() should request return an Observable<Webpage> object', () => {
        service.editWebpage(webpage).subscribe((res: Webpage) => {
            expect(res).toEqual(webpage);
        });
        const url = `${config.apiUrl}/webpage/1`
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}`, });
        mockRequest.flush(webpage);
    });
    it('#deleteWebpageFamily() should request return an Observable<object> ', () => {
        service.deleteWebpage(1).subscribe((res: Webpage) => {
            expect(res).toEqual(webpage);
        });
        const url = `${config.apiUrl}/webpage/1`
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}`, });
        mockRequest.flush(webpage);
    });
});

