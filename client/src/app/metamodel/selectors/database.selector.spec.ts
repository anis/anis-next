/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as databaseSelector from './database.selector';
import * as fromDatabase from '../reducers/database.reducer';
import { DATABASE } from '../../../test-data';

describe('[Metamodel][Selector] Database selector', () => {
    it('should get database state', () => {
        const state = { metamodel: { database: { ...fromDatabase.initialState }}};
        expect(databaseSelector.selectDatabaseState(state)).toEqual(state.metamodel.database);
    });

    it('should get database IDs', () => {
        const state = { metamodel: { database: { ...fromDatabase.initialState }}};
        expect(databaseSelector.selectDatabaseIds(state).length).toEqual(0);
    });

    it('should get database entities', () => {
        const state = { metamodel: { database: { ...fromDatabase.initialState }}};
        expect(databaseSelector.selectDatabaseEntities(state)).toEqual({ });
    });

    it('should get all databases', () => {
        const state = { metamodel: { database: { ...fromDatabase.initialState }}};
        expect(databaseSelector.selectAllDatabases(state).length).toEqual(0);
    });

    it('should get database count', () => {
        const state = { metamodel: { database: { ...fromDatabase.initialState }}};
        expect(databaseSelector.selectDatabaseTotal(state)).toEqual(0);
    });

    it('should get databaseListIsLoading', () => {
        const state = { metamodel: { database: { ...fromDatabase.initialState }}};
        expect(databaseSelector.selectDatabaseListIsLoading(state)).toBe(false);
    });

    it('should get databaseListIsLoaded', () => {
        const state = { metamodel: { database: { ...fromDatabase.initialState }}};
        expect(databaseSelector.selectDatabaseListIsLoaded(state)).toBe(false);
    });

    it('should get database by route', () => {
        const state = {
            router: { state: { params: { id: 1 }}},
            metamodel: {
                database: {
                    ...fromDatabase.initialState,
                    ids: [1],
                    entities: { 1: DATABASE }
                }
            }
        };
        expect(databaseSelector.selectDatabaseByRouteId(state)).toEqual(DATABASE);
    });
});
