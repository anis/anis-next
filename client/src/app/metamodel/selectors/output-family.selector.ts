/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromOutputFamily from '../reducers/output-family.reducer';

export const selectOutputFamilyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.outputFamily
);

export const selectOutputFamilyIds = createSelector(
    selectOutputFamilyState,
    fromOutputFamily.selectOutputFamilyIds
);

export const selectOutputFamilyEntities = createSelector(
    selectOutputFamilyState,
    fromOutputFamily.selectOutputFamilyEntities
);

export const selectAllOutputFamilies = createSelector(
    selectOutputFamilyState,
    fromOutputFamily.selectAllOutputFamilies
);

export const selectOutputFamilyTotal = createSelector(
    selectOutputFamilyState,
    fromOutputFamily.selectOutputFamilyTotal
);

export const selectOutputFamilyListIsLoading = createSelector(
    selectOutputFamilyState,
    fromOutputFamily.selectOutputFamilyListIsLoading
);

export const selectOutputFamilyListIsLoaded = createSelector(
    selectOutputFamilyState,
    fromOutputFamily.selectOutputFamilyListIsLoaded
);
