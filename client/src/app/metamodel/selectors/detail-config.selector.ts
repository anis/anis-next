/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromDetailConfig from '../reducers/detail-config.reducer';

export const selectDetailConfigState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.detailConfig
);

export const selectDetailConfig = createSelector(
    selectDetailConfigState,
    fromDetailConfig.selectDetailConfig
);

export const selectDetailConfigIsLoading = createSelector(
    selectDetailConfigState,
    fromDetailConfig.selectDetailConfigIsLoading
);

export const selectDetailConfigIsLoaded = createSelector(
    selectDetailConfigState,
    fromDetailConfig.selectDetailConfigIsLoaded
);
