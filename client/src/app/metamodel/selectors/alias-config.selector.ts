/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromAliasConfig from '../reducers/alias-config.reducer';

export const selectAliasConfigState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.aliasConfig
);

export const selectAliasConfig = createSelector(
    selectAliasConfigState,
    fromAliasConfig.selectAliasConfig
);

export const selectAliasConfigIsLoading = createSelector(
    selectAliasConfigState,
    fromAliasConfig.selectAliasConfigIsLoading
);

export const selectAliasConfigIsLoaded = createSelector(
    selectAliasConfigState,
    fromAliasConfig.selectAliasConfigIsLoaded
);
