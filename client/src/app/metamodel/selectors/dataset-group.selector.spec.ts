/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetGroupSelector from './dataset-group.selector';
import * as fromDatasetGroup from '../reducers/dataset-group.reducer';
import { GROUP } from '../../../test-data';

describe('[Metamodel][Selector] Group selector', () => {
    it('should get dataset group state', () => {
        const state = { metamodel: { datasetGroup: { ...fromDatasetGroup.initialState }}};
        expect(datasetGroupSelector.selectDatasetGroupState(state)).toEqual(state.metamodel.datasetGroup);
    });

    it('should get dataset group IDs', () => {
        const state = { metamodel: { datasetGroup: { ...fromDatasetGroup.initialState }}};
        expect(datasetGroupSelector.selectDatasetGroupIds(state).length).toEqual(0);
    });

    it('should get dataset group entities', () => {
        const state = { metamodel: { datasetGroup: { ...fromDatasetGroup.initialState }}};
        expect(datasetGroupSelector.selectDatasetGroupEntities(state)).toEqual({ });
    });

    it('should get all dataset groups', () => {
        const state = { metamodel: { datasetGroup: { ...fromDatasetGroup.initialState }}};
        expect(datasetGroupSelector.selectAllDatasetGroups(state).length).toEqual(0);
    });

    it('should get dataset group count', () => {
        const state = { metamodel: { datasetGroup: { ...fromDatasetGroup.initialState }}};
        expect(datasetGroupSelector.selectDatasetGroupTotal(state)).toEqual(0);
    });

    it('should get datasetGroupListIsLoading', () => {
        const state = { metamodel: { datasetGroup: { ...fromDatasetGroup.initialState }}};
        expect(datasetGroupSelector.selectDatasetGroupListIsLoading(state)).toBe(false);
    });

    it('should get datasetGroupListIsLoaded', () => {
        const state = { metamodel: { datasetGroup: { ...fromDatasetGroup.initialState }}};
        expect(datasetGroupSelector.selectDatasetGroupListIsLoaded(state)).toBe(false);
    });

    it('should get dataset group ID by route', () => {
        const state = {
            router: { state: { params: { id: 1 }}},
            metamodel: {
                datasetGroup: {
                    ...fromDatasetGroup.initialState,
                    ids: [1],
                    entities: { 1: GROUP }
                }
            }
        };
        expect(datasetGroupSelector.selectDatasetGroupByRouteId(state)).toEqual(GROUP);
    });
});
