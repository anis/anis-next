/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetFamilySelector from './dataset-family.selector';
import * as fromDatasetFamily from '../reducers/dataset-family.reducer';

describe('[Metamodel][Selector] DatasetFamily selector', () => {
    it('should get datasetFamily state', () => {
        const state = { metamodel: { datasetFamily: { ...fromDatasetFamily.initialState }}};
        expect(datasetFamilySelector.selectDatasetFamilyState(state)).toEqual(state.metamodel.datasetFamily);
    });

    it('should get datasetFamily IDs', () => {
        const state = { metamodel: { datasetFamily: { ...fromDatasetFamily.initialState }}};
        expect(datasetFamilySelector.selectDatasetFamilyIds(state).length).toEqual(0);
    });

    it('should get datasetFamily entities', () => {
        const state = { metamodel: { datasetFamily: { ...fromDatasetFamily.initialState }}};
        expect(datasetFamilySelector.selectDatasetFamilyEntities(state)).toEqual({ });
    });

    it('should get all datasetFamilies', () => {
        const state = { metamodel: { datasetFamily: { ...fromDatasetFamily.initialState }}};
        expect(datasetFamilySelector.selectAllDatasetFamilies(state).length).toEqual(0);
    });

    it('should get datasetFamily count', () => {
        const state = { metamodel: { datasetFamily: { ...fromDatasetFamily.initialState }}};
        expect(datasetFamilySelector.selectDatasetFamilyTotal(state)).toEqual(0);
    });

    it('should get datasetFamilyListIsLoading', () => {
        const state = { metamodel: { datasetFamily: { ...fromDatasetFamily.initialState }}};
        expect(datasetFamilySelector.selectDatasetFamilyListIsLoading(state)).toBe(false);
    });

    it('should get datasetFamilyListIsLoaded', () => {
        const state = { metamodel: { datasetFamily: { ...fromDatasetFamily.initialState }}};
        expect(datasetFamilySelector.selectDatasetFamilyListIsLoaded(state)).toBe(false);
    });
});
