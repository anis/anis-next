/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as outputFamilySelector from './output-family.selector';
import * as fromOutputFamily from '../reducers/output-family.reducer';

describe('[Metamodel][Selector] Output family selector', () => {
    it('should get outputFamily state', () => {
        const state = { metamodel: { outputFamily: { ...fromOutputFamily.initialState }}};
        expect(outputFamilySelector.selectOutputFamilyState(state)).toEqual(state.metamodel.outputFamily);
    });

    it('should get outputFamily IDs', () => {
        const state = { metamodel: { outputFamily: { ...fromOutputFamily.initialState }}};
        expect(outputFamilySelector.selectOutputFamilyIds(state).length).toEqual(0);
    });

    it('should get outputFamily entities', () => {
        const state = { metamodel: { outputFamily: { ...fromOutputFamily.initialState }}};
        expect(outputFamilySelector.selectOutputFamilyEntities(state)).toEqual({ });
    });

    it('should get all outputFamilies', () => {
        const state = { metamodel: { outputFamily: { ...fromOutputFamily.initialState }}};
        expect(outputFamilySelector.selectAllOutputFamilies(state).length).toEqual(0);
    });

    it('should get outputFamily count', () => {
        const state = { metamodel: { outputFamily: { ...fromOutputFamily.initialState }}};
        expect(outputFamilySelector.selectOutputFamilyTotal(state)).toEqual(0);
    });

    it('should get outputFamilyListIsLoading', () => {
        const state = { metamodel: { outputFamily: { ...fromOutputFamily.initialState }}};
        expect(outputFamilySelector.selectOutputFamilyListIsLoading(state)).toBe(false);
    });

    it('should get outputFamilyListIsLoaded', () => {
        const state = { metamodel: { outputFamily: { ...fromOutputFamily.initialState }}};
        expect(outputFamilySelector.selectOutputFamilyListIsLoaded(state)).toBe(false);
    });
});
