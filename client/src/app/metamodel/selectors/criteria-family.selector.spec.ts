/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as criteriaFamilySelector from './criteria-family.selector';
import * as fromCriteriaFamily from '../reducers/criteria-family.reducer';

describe('[Metamodel][Selector] CriteriaFamily selector', () => {
    it('should get criteriaFamily state', () => {
        const state = { metamodel: { criteriaFamily: { ...fromCriteriaFamily.initialState }}};
        expect(criteriaFamilySelector.selectCriteriaFamilyState(state)).toEqual(state.metamodel.criteriaFamily);
    });

    it('should get criteriaFamily IDs', () => {
        const state = { metamodel: { criteriaFamily: { ...fromCriteriaFamily.initialState }}};
        expect(criteriaFamilySelector.selectCriteriaFamilyIds(state).length).toEqual(0);
    });

    it('should get criteriaFamily entities', () => {
        const state = { metamodel: { criteriaFamily: { ...fromCriteriaFamily.initialState }}};
        expect(criteriaFamilySelector.selectCriteriaFamilyEntities(state)).toEqual({ });
    });

    it('should get all criteriaFamilies', () => {
        const state = { metamodel: { criteriaFamily: { ...fromCriteriaFamily.initialState }}};
        expect(criteriaFamilySelector.selectAllCriteriaFamilies(state).length).toEqual(0);
    });

    it('should get criteriaFamily count', () => {
        const state = { metamodel: { criteriaFamily: { ...fromCriteriaFamily.initialState }}};
        expect(criteriaFamilySelector.selectCriteriaFamilyTotal(state)).toEqual(0);
    });

    it('should get criteriaFamilyListIsLoading', () => {
        const state = { metamodel: { criteriaFamily: { ...fromCriteriaFamily.initialState }}};
        expect(criteriaFamilySelector.selectCriteriaFamilyListIsLoading(state)).toBe(false);
    });

    it('should get criteriaFamilyListIsLoaded', () => {
        const state = { metamodel: { criteriaFamily: { ...fromCriteriaFamily.initialState }}};
        expect(criteriaFamilySelector.selectCriteriaFamilyListIsLoaded(state)).toBe(false);
    });
});
