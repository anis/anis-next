/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromFile from '../reducers/file.reducer';

export const selectFileState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.file
);

export const selectFileIds = createSelector(
    selectFileState,
    fromFile.selectFileIds
);

export const selectFileEntities = createSelector(
    selectFileState,
    fromFile.selectFileEntities
);

export const selectAllFiles = createSelector(
    selectFileState,
    fromFile.selectAllFiles
);

export const selectFileTotal = createSelector(
    selectFileState,
    fromFile.selectFileTotal
);

export const selectFileListIsLoading = createSelector(
    selectFileState,
    fromFile.selectFileListIsLoading
);

export const selectFileListIsLoaded = createSelector(
    selectFileState,
    fromFile.selectFileListIsLoaded
);

export const selectFileByRouteId = createSelector(
    selectFileEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params.id]
);