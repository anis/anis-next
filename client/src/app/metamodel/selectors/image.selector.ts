/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromImage from '../reducers/image.reducer';

export const selectImageState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.image
);

export const selectImageIds = createSelector(
    selectImageState,
    fromImage.selectImageIds
);

export const selectImageEntities = createSelector(
    selectImageState,
    fromImage.selectImageEntities
);

export const selectAllImages = createSelector(
    selectImageState,
    fromImage.selectAllImages
);

export const selectImageTotal = createSelector(
    selectImageState,
    fromImage.selectImageTotal
);

export const selectImageListIsLoading = createSelector(
    selectImageState,
    fromImage.selectImageListIsLoading
);

export const selectImageListIsLoaded = createSelector(
    selectImageState,
    fromImage.selectImageListIsLoaded
);

export const selectImageByRouteId = createSelector(
    selectImageEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params.id]
);