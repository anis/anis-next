/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromDatasetGroup from '../reducers/dataset-group.reducer';

export const selectDatasetGroupState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.datasetGroup
);

export const selectDatasetGroupIds = createSelector(
    selectDatasetGroupState,
    fromDatasetGroup.selectDatasetGroupIds
);

export const selectDatasetGroupEntities = createSelector(
    selectDatasetGroupState,
    fromDatasetGroup.selectDatasetGroupEntities
);

export const selectAllDatasetGroups = createSelector(
    selectDatasetGroupState,
    fromDatasetGroup.selectAllDatasetGroups
);

export const selectDatasetGroupTotal = createSelector(
    selectDatasetGroupState,
    fromDatasetGroup.selectDatasetGroupTotal
);

export const selectDatasetGroupListIsLoading = createSelector(
    selectDatasetGroupState,
    fromDatasetGroup.selectDatasetGroupListIsLoading
);

export const selectDatasetGroupListIsLoaded = createSelector(
    selectDatasetGroupState,
    fromDatasetGroup.selectDatasetGroupListIsLoaded
);

export const selectDatasetGroupByRouteId = createSelector(
    selectDatasetGroupEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params.id]
);
