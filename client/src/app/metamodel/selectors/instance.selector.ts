/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromInstance from '../reducers/instance.reducer';

export const selectInstanceState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.instance
);

export const selectInstanceIds = createSelector(
    selectInstanceState,
    fromInstance.selectInstanceIds
);

export const selectInstanceEntities = createSelector(
    selectInstanceState,
    fromInstance.selectInstanceEntities
);

export const selectAllInstances = createSelector(
    selectInstanceState,
    fromInstance.selectAllInstances
);

export const selectInstanceTotal = createSelector(
    selectInstanceState,
    fromInstance.selectInstanceTotal
);

export const selectInstanceListIsLoading = createSelector(
    selectInstanceState,
    fromInstance.selectInstanceListIsLoading
);

export const selectInstanceListIsLoaded = createSelector(
    selectInstanceState,
    fromInstance.selectInstanceListIsLoaded
);

export const selectInstanceByRouteName = createSelector(
    selectInstanceEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params.iname]
);

export const selectInstanceNameByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.params.iname as string
);
