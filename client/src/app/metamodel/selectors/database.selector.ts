/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromDatabase from '../reducers/database.reducer';

export const selectDatabaseState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.database
);

export const selectDatabaseIds = createSelector(
    selectDatabaseState,
    fromDatabase.selectDatabaseIds
);

export const selectDatabaseEntities = createSelector(
    selectDatabaseState,
    fromDatabase.selectDatabaseEntities
);

export const selectAllDatabases = createSelector(
    selectDatabaseState,
    fromDatabase.selectAllDatabases
);

export const selectDatabaseTotal = createSelector(
    selectDatabaseState,
    fromDatabase.selectDatabaseTotal
);

export const selectDatabaseListIsLoading = createSelector(
    selectDatabaseState,
    fromDatabase.selectDatabaseListIsLoading
);

export const selectDatabaseListIsLoaded = createSelector(
    selectDatabaseState,
    fromDatabase.selectDatabaseListIsLoaded
);

export const selectDatabaseByRouteId = createSelector(
    selectDatabaseEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params.id]
);
