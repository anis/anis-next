/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as attributeSelector from './attribute.selector';
import * as fromAttribute from '../reducers/attribute.reducer';

describe('[Metamodel][Selector] Attribute selector', () => {
    it('should get attribute state', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.selectAttributeState(state)).toEqual(state.metamodel.attribute);
    });

    it('should get attribute IDs', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.selectAttributeIds(state).length).toEqual(0);
    });

    it('should get attribute entities', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.selectAttributeEntities(state)).toEqual({ });
    });

    it('should get all attributes', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.selectAllAttributes(state).length).toEqual(0);
    });

    it('should get attribute count', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.selectAttributeTotal(state)).toEqual(0);
    });

    it('should get attributeListIsLoading', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.selectAttributeListIsLoading(state)).toBe(false);
    });

    it('should get attributeListIsLoaded', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.selectAttributeListIsLoaded(state)).toBe(false);
    });
});
