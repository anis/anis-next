/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as outputCategorySelector from './output-category.selector';
import * as fromOutputCategory from '../reducers/output-category.reducer';

describe('[Metamodel][Selector] OutputCategory selector', () => {
    it('should get outputCategory state', () => {
        const state = { metamodel: { outputCategory: { ...fromOutputCategory.initialState }}};
        expect(outputCategorySelector.selectOutputCategoryState(state)).toEqual(state.metamodel.outputCategory);
    });

    it('should get outputCategory IDs', () => {
        const state = { metamodel: { outputCategory: { ...fromOutputCategory.initialState }}};
        expect(outputCategorySelector.selectOutputCategoryIds(state).length).toEqual(0);
    });

    it('should get outputCategory entities', () => {
        const state = { metamodel: { outputCategory: { ...fromOutputCategory.initialState }}};
        expect(outputCategorySelector.selectOutputCategoryEntities(state)).toEqual({ });
    });

    it('should get all outputCategories', () => {
        const state = { metamodel: { outputCategory: { ...fromOutputCategory.initialState }}};
        expect(outputCategorySelector.selectAllOutputCategories(state).length).toEqual(0);
    });

    it('should get outputCategory count', () => {
        const state = { metamodel: { outputCategory: { ...fromOutputCategory.initialState }}};
        expect(outputCategorySelector.selectOutputCategoryTotal(state)).toEqual(0);
    });

    it('should get outputCategoryListIsLoading', () => {
        const state = { metamodel: { outputCategory: { ...fromOutputCategory.initialState }}};
        expect(outputCategorySelector.selectOutputCategoryListIsLoading(state)).toBe(false);
    });

    it('should get outputCategoryListIsLoaded', () => {
        const state = { metamodel: { outputCategory: { ...fromOutputCategory.initialState }}};
        expect(outputCategorySelector.selectOutputCategoryListIsLoaded(state)).toBe(false);
    });
});
