/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromWebpageFamily from '../reducers/webpage-family.reducer';

export const selectWebpageFamilyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.webpageFamily
);

export const selectWebpageFamilyIds = createSelector(
    selectWebpageFamilyState,
    fromWebpageFamily.selectWebpageFamilyIds
);

export const selectWebpageFamilyEntities = createSelector(
    selectWebpageFamilyState,
    fromWebpageFamily.selectWebpageFamilyEntities
);

export const selectAllWebpageFamilies = createSelector(
    selectWebpageFamilyState,
    fromWebpageFamily.selectAllWebpageFamilies
);

export const selectWebpageFamilyTotal = createSelector(
    selectWebpageFamilyState,
    fromWebpageFamily.selectWebpageFamilyTotal
);

export const selectWebpageFamilyListIsLoading = createSelector(
    selectWebpageFamilyState,
    fromWebpageFamily.selectWebpageFamilyListIsLoading
);

export const selectWebpageFamilyListIsLoaded = createSelector(
    selectWebpageFamilyState,
    fromWebpageFamily.selectWebpageFamilyListIsLoaded
);
