/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromWebpage from '../reducers/webpage.reducer';
import * as webpageFamilySelector from './webpage-family.selector';

export const selectWebpageState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.webpage
);

export const selectWebpageIds = createSelector(
    selectWebpageState,
    fromWebpage.selectWebpageIds
);

export const selectWebpageEntities = createSelector(
    selectWebpageState,
    fromWebpage.selectWebpageEntities
);

export const selectAllWebpages = createSelector(
    selectWebpageState,
    fromWebpage.selectAllWebpages
);

export const selectWebpageTotal = createSelector(
    selectWebpageState,
    fromWebpage.selectWebpageTotal
);

export const selectWebpageListIsLoading = createSelector(
    selectWebpageState,
    fromWebpage.selectWebpageListIsLoading
);

export const selectWebpageListIsLoaded = createSelector(
    selectWebpageState,
    fromWebpage.selectWebpageListIsLoaded
);

export const selectWebpageByRouteId = createSelector(
    selectWebpageEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params.id]
);

export const selectWebpageByRouteName = createSelector(
    selectAllWebpages,
    reducer.selectRouterState,
    (webpages, router) => webpages.find(webpage => webpage.name === router.state.params.name)
);

export const selectFirstWebpage = createSelector(
    webpageFamilySelector.selectAllWebpageFamilies,
    webpageFamilySelector.selectWebpageFamilyListIsLoaded,
    selectAllWebpages,
    selectWebpageListIsLoaded,
    (webpageFamilyList, webpageFamilyListIsLoaded, webpageList, webpageListIsLoaded) => {
        if (webpageFamilyListIsLoaded && webpageListIsLoaded) {
            const firstWebpageFamily = webpageFamilyList[0];
            return webpageList.filter(webpage => webpage.id_webpage_family === firstWebpageFamily.id)[0];
        } else {
            return null;
        }
    }
);