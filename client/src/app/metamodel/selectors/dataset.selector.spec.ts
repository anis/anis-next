/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetSelector from './dataset.selector';
import * as fromDataset from '../reducers/dataset.reducer';
import { DATASET, DATASET_LIST } from '../../../test-data';

describe('[Metamodel][Selector] Dataset selector', () => {
    it('should get dataset state', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.selectDatasetState(state)).toEqual(state.metamodel.dataset);
    });

    it('should get dataset IDs', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.selectDatasetIds(state).length).toEqual(0);
    });

    it('should get dataset entities', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.selectDatasetEntities(state)).toEqual({ });
    });

    it('should get all datasets', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.selectAllDatasets(state).length).toEqual(0);
    });

    it('should get dataset count', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.selectDatasetTotal(state)).toEqual(0);
    });

    it('should get datasetListIsLoading', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.selectDatasetListIsLoading(state)).toBe(false);
    });

    it('should get datasetListIsLoaded', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.selectDatasetListIsLoaded(state)).toBe(false);
    });

    it('should get dataset by route', () => {
        const state = {
            router: { state: { params: { dname: 'myDataset' }}},
            metamodel: {
                dataset: {
                    ...fromDataset.initialState,
                    ids: ['myDataset'],
                    entities: { 'myDataset': DATASET }
                }
            }
        };
        expect(datasetSelector.selectDatasetByRouteName(state)).toEqual(DATASET);
    });

    it('should get dataset name by route', () => {
        const state = {router: { state: { params: { dname: 'myDataset' }}}};
        expect(datasetSelector.selectDatasetNameByRoute(state)).toEqual('myDataset');
    });

    it('should get datasets with cone search', () => {
        const state = {
            metamodel: {
                dataset: {
                    ...fromDataset.initialState,
                    ids: ['myDataset', 'anotherDataset'],
                    entities: {
                        'myDataset': DATASET_LIST[0],
                        'anotherDataset': DATASET_LIST[1]
                    }
                }
            }
        };
        expect(datasetSelector.selectAllConeSearchDatasets(state).length).toEqual(1);
        expect(datasetSelector.selectAllConeSearchDatasets(state)).toContain(DATASET_LIST[0]);
    });
});
