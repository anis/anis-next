/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromDatasetFamily from '../reducers/dataset-family.reducer';

export const selectDatasetFamilyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.datasetFamily
);

export const selectDatasetFamilyIds = createSelector(
    selectDatasetFamilyState,
    fromDatasetFamily.selectDatasetFamilyIds
);

export const selectDatasetFamilyEntities = createSelector(
    selectDatasetFamilyState,
    fromDatasetFamily.selectDatasetFamilyEntities
);

export const selectAllDatasetFamilies = createSelector(
    selectDatasetFamilyState,
    fromDatasetFamily.selectAllDatasetFamilies
);

export const selectDatasetFamilyTotal = createSelector(
    selectDatasetFamilyState,
    fromDatasetFamily.selectDatasetFamilyTotal
);

export const selectDatasetFamilyListIsLoading = createSelector(
    selectDatasetFamilyState,
    fromDatasetFamily.selectDatasetFamilyListIsLoading
);

export const selectDatasetFamilyListIsLoaded = createSelector(
    selectDatasetFamilyState,
    fromDatasetFamily.selectDatasetFamilyListIsLoaded
);
