/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as instanceSelector from './instance.selector';
import * as fromInstance from '../reducers/instance.reducer';
import { INSTANCE } from '../../../test-data';

describe('[Metamodel][Selector] Instance selector', () => {
    it('should get instance state', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.selectInstanceState(state)).toEqual(state.metamodel.instance);
    });

    it('should get instance IDs', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.selectInstanceIds(state).length).toEqual(0);
    });

    it('should get instance entities', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.selectInstanceEntities(state)).toEqual({ });
    });

    it('should get all instances', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.selectAllInstances(state).length).toEqual(0);
    });

    it('should get instance count', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.selectInstanceTotal(state)).toEqual(0);
    });

    it('should get instanceListIsLoading', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.selectInstanceListIsLoading(state)).toBe(false);
    });

    it('should get instanceListIsLoaded', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.selectInstanceListIsLoaded(state)).toBe(false);
    });

    it('should get instance by route', () => {
        const state = {
            router: { state: { params: { iname: 'myInstance' }}},
            metamodel: {
                instance: {
                    ...fromInstance.initialState,
                    ids: ['myInstance'],
                    entities: { 'myInstance': INSTANCE }
                }
            }
        };
        expect(instanceSelector.selectInstanceByRouteName(state)).toEqual(INSTANCE);
    });

    it('should get instance name by route', () => {
        const state = { router: { state: { params: { iname: 'myInstance' }}}};
        expect(instanceSelector.selectInstanceNameByRoute(state)).toEqual('myInstance');
    });
});
