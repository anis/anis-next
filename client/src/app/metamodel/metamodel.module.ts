/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { metamodelReducer } from './metamodel.reducer';
import { metamodelEffects } from './effects';
import { metamodelServices} from './services';

/**
 * @class
 * @classdesc Metamodel module.
 */
@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature('metamodel', metamodelReducer),
        EffectsModule.forFeature(metamodelEffects)
    ],
    providers: [
        metamodelServices
    ]
})
export class MetamodelModule { }
