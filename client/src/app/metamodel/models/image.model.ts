/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for group.
 *
 * @interface Image
 */
export interface Image {
    id: number;
    label: string;
    file_path: string;
    file_size: number;
    ra_min: number;
    ra_max: number;
    dec_min: number;
    dec_max: number;
    stretch: string;
    pmin: number;
    pmax: number;
}
