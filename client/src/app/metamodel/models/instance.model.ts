/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Logo } from './logo.model';

/**
 * Interface for instance.
 *
 * @interface Instance
 */
export interface Instance {
    name: string;
    label: string;
    description: string;
    scientific_manager: string;
    instrument: string;
    wavelength_domain: string;
    display: number;
    data_path: string;
    files_path: string;
    public: boolean;
    portal_logo: string;
    portal_color: string;
    design_background_color: string;
    design_text_color: string;
    design_font_family: string;
    design_link_color: string;
    design_link_hover_color: string;
    design_logo: string;
    design_logo_href: string;
    design_favicon: string;
    navbar_background_color: string;
    navbar_border_bottom_color: string;
    navbar_color_href: string;
    navbar_font_family: string;
    navbar_sign_in_btn_color: string;
    navbar_user_btn_color: string;
    footer_background_color: string;
    footer_border_top_color: string;
    footer_text_color: string;
    footer_logos: Logo[];
    family_border_color: string;
    family_header_background_color: string;
    family_title_color: string;
    family_title_bold: boolean;
    family_background_color: string;
    family_text_color: string;
    progress_bar_title: string;
    progress_bar_title_color: string;
    progress_bar_subtitle: string;
    progress_bar_subtitle_color: string;
    progress_bar_step_dataset_title: string;
    progress_bar_step_criteria_title: string;
    progress_bar_step_output_title: string;
    progress_bar_step_result_title: string;
    progress_bar_color: string;
    progress_bar_active_color: string;
    progress_bar_circle_color: string;
    progress_bar_circle_icon_color: string;
    progress_bar_circle_icon_active_color: string;
    progress_bar_text_color: string;
    progress_bar_text_bold: boolean;
    search_next_btn_color: string;
    search_next_btn_hover_color: string;
    search_next_btn_hover_text_color: string;
    search_back_btn_color: string;
    search_back_btn_hover_color: string;
    search_back_btn_hover_text_color: string;
    search_info_background_color: string;
    search_info_text_color: string;
    search_info_help_enabled: boolean;
    dataset_select_btn_color: string;
    dataset_select_btn_hover_color: string;
    dataset_select_btn_hover_text_color: string;
    dataset_selected_icon_color: string;
    search_criterion_background_color: string;
    search_criterion_text_color: string;
    output_columns_selected_color: string;
    output_columns_select_all_btn_color: string;
    output_columns_select_all_btn_hover_color: string;
    output_columns_select_all_btn_hover_text_color: string;
    result_panel_border_size: string;
    result_panel_border_color: string;
    result_panel_title_color: string;
    result_panel_background_color: string;
    result_panel_text_color: string;
    result_download_btn_color: string;
    result_download_btn_hover_color: string;
    result_download_btn_text_color: string;
    result_datatable_actions_btn_color: string;
    result_datatable_actions_btn_hover_color: string;
    result_datatable_actions_btn_text_color: string;
    result_datatable_bordered: boolean;
    result_datatable_bordered_radius: boolean;
    result_datatable_border_color: string;
    result_datatable_header_background_color: string;
    result_datatable_header_text_color: string;
    result_datatable_rows_background_color: string;
    result_datatable_rows_text_color: string;
    result_datatable_sorted_color: string;
    result_datatable_sorted_active_color: string;
    result_datatable_link_color: string;
    result_datatable_link_hover_color: string;
    result_datatable_rows_selected_color: string;
    result_datatable_pagination_link_color: string;
    result_datatable_pagination_active_bck_color: string;
    result_datatable_pagination_active_text_color: string;
    samp_enabled: boolean;
    back_to_portal: boolean;
    user_menu_enabled: boolean;
    search_by_criteria_allowed: boolean;
    search_by_criteria_label: string;
    search_multiple_allowed: boolean;
    search_multiple_label: string;
    search_multiple_all_datasets_selected: boolean;
    search_multiple_progress_bar_title: string;
    search_multiple_progress_bar_subtitle: string;
    search_multiple_progress_bar_step_position: string;
    search_multiple_progress_bar_step_datasets: string;
    search_multiple_progress_bar_step_result: string;
    documentation_allowed: boolean;
    documentation_label: string;
    nb_dataset_families: number;
    nb_datasets: number;
}
