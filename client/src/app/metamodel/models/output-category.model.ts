/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for output category.
 *
 * @interface OutputCategory
 */
export interface OutputCategory {
    id: number;
    label: string;
    display: number;
    id_output_family: number;
}
