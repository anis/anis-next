/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for webpage family.
 *
 * @interface WebpageFamily
 */
 export interface WebpageFamily {
    id: number;
    label: string;
    icon: string;
    display: number;
}
