/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { RendererConfig } from './renderer-config.model';

/**
 * Interface for link renderer config.
 *
 * @interface LinkRendererConfig
 * @extends RendererConfig
 */
export interface LinkRendererConfig extends RendererConfig {
    href: string;
    display: string;
    text: string;
    icon: string;
    blank: boolean;
}
