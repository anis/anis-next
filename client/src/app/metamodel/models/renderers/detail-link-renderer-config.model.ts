/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { RendererConfig } from './renderer-config.model';

/**
 * Interface for detail renderer config.
 *
 * @interface DetailLinkRendererConfig
 * @extends RendererConfig
 */
export interface DetailLinkRendererConfig extends RendererConfig {
    display: string;
    component: string;
}
