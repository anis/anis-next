/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for dataset group.
 *
 * @interface DatasetGroup
 */
export interface DatasetGroup {
    id: number;
    role: string;
    instance_name: string;
    datasets: string[];
}
