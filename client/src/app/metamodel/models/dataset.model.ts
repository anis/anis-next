/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for dataset.
 *
 * @interface Dataset
 */
export interface Dataset {
    name: string;
    table_ref: string;
    label: string;
    description: string;
    display: number;
    data_path: string;
    public: boolean;
    download_json: boolean;
    download_csv: boolean;
    download_ascii: boolean;
    download_vo: boolean;
    download_fits: boolean;
    server_link_enabled: boolean;
    datatable_enabled: boolean;
    datatable_selectable_rows: boolean;
    cone_search_config_id: number;
    id_database: number;
    id_dataset_family: number;
    full_data_path: string;
}
