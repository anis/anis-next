/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for file
 *
 * @interface File
 */
export interface File {
    id: number;
    label: string;
    file_path: string;
    file_size: number;
    type: string;
}
