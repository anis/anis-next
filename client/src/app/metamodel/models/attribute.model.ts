/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Option } from './option.model';
import { RendererConfig } from './renderers';

/**
 * Interface for attribute.
 *
 * @interface Attribute
 */
export interface Attribute {
    id: number;
    name: string;
    label: string;
    form_label: string;
    description: string;
    primary_key: boolean;
    type: string;
    search_type: string;
    operator: string;
    dynamic_operator: boolean;
    min: string;
    max: string;
    placeholder_min: string;
    placeholder_max: string;
    criteria_display: number;
    output_display: number;
    selected: boolean;
    renderer: string;
    renderer_config: RendererConfig;
    order_by: boolean;
    archive: boolean;
    detail_display: number;
    detail_renderer: string;
    detail_renderer_config: RendererConfig;
    options: Option[];
    vo_utype: string;
    vo_ucd: string;
    vo_unit: string;
    vo_description: string;
    vo_datatype: string;
    vo_size: number;
    id_criteria_family: number;
    id_output_category: number;
    id_detail_output_category: number;
}
