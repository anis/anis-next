/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for web page.
 *
 * @interface Webpage
 */
export interface Webpage {
    id: number;
    name: string;
    label: string;
    icon: string;
    display: number;
    title: string;
    content: string;
    style_sheet: string;
    type: string;
    url: string;
    id_webpage_family: number;
}
