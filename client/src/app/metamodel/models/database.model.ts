/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for database.
 *
 * @interface Database
 */
export interface Database {
    id: number;
    label: string;
    dbname: string;
    dbtype: string;
    dbhost: string;
    dbport: number;
    dblogin: string;
    dbpassword: string;
}
