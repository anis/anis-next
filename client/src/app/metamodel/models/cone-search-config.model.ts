/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for group.
 *
 * @interface ConeSearchConfig
 */
export interface ConeSearchConfig {
    id: number;
    enabled: boolean;
    opened: boolean;
    column_ra: number;
    column_dec: number;
    resolver_enabled: boolean;
    default_ra: number;
    default_dec: number;
    default_radius: number;
    default_ra_dec_unit: string;
    plot_enabled: boolean;
}
