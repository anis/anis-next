/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for output family.
 *
 * @interface OutputFamily
 */
export interface OutputFamily {
    id: number;
    label: string;
    display: number;
    opened: boolean;
}
