/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { combineReducers, createFeatureSelector } from '@ngrx/store';

import { RouterReducerState } from 'src/app/custom-route-serializer';
import * as database from './reducers/database.reducer';
import * as datasetGroup from './reducers/dataset-group.reducer';
import * as instanceGroup from './reducers/instance-group.reducer';
import * as dataset from './reducers/dataset.reducer';
import * as datasetFamily from './reducers/dataset-family.reducer';
import * as instance from './reducers/instance.reducer';
import * as attribute from './reducers/attribute.reducer';
import * as criteriaFamily from './reducers/criteria-family.reducer';
import * as outputCategory from './reducers/output-category.reducer';
import * as outputFamily from './reducers/output-family.reducer';
import * as image from './reducers/image.reducer';
import * as file from './reducers/file.reducer';
import * as coneSearchConfig from './reducers/cone-search-config.reducer';
import * as detailConfig from './reducers/detail-config.reducer';
import * as aliasConfig from './reducers/alias-config.reducer';
import * as webpageFamily from './reducers/webpage-family.reducer';
import * as webpage from './reducers/webpage.reducer';

/**
 * Interface for metamodel state.
 *
 * @interface State
 */
export interface State {
    database: database.State;
    datasetGroup: datasetGroup.State;
    instanceGroup: instanceGroup.State;
    dataset: dataset.State;
    datasetFamily: datasetFamily.State;
    instance: instance.State;
    attribute: attribute.State;
    criteriaFamily: criteriaFamily.State;
    outputCategory: outputCategory.State;
    outputFamily: outputFamily.State;
    image: image.State;
    file: file.State;
    coneSearchConfig: coneSearchConfig.State;
    detailConfig: detailConfig.State;
    aliasConfig: aliasConfig.State;
    webpageFamily: webpageFamily.State;
    webpage: webpage.State;
}

const reducers = {
    database: database.databaseReducer,
    datasetGroup: datasetGroup.datasetGroupReducer,
    instanceGroup: instanceGroup.instanceGroupReducer,
    dataset: dataset.datasetReducer,
    datasetFamily: datasetFamily.datasetFamilyReducer,
    instance: instance.instanceReducer,
    attribute: attribute.attributeReducer,
    criteriaFamily: criteriaFamily.criteriaFamilyReducer,
    outputCategory: outputCategory.outputCategoryReducer,
    outputFamily: outputFamily.outputFamilyReducer,
    image: image.imageReducer,
    file: file.fileReducer,
    coneSearchConfig: coneSearchConfig.coneSearchConfigReducer,
    detailConfig: detailConfig.detailConfigReducer,
    aliasConfig: aliasConfig.aliasConfigReducer,
    webpageFamily: webpageFamily.webpageFamilyReducer,
    webpage: webpage.webpageReducer
};

export const metamodelReducer = combineReducers(reducers);
export const getMetamodelState = createFeatureSelector<State>('metamodel');
export const selectRouterState = createFeatureSelector<RouterReducerState>('router');
