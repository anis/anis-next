/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { TitleStrategy } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NgxMatomoTrackerModule, MatomoInitializationMode } from '@ngx-matomo/tracker';
import { NgxMatomoRouterModule } from '@ngx-matomo/router';

import { environment } from '../environments/environment';
import { reducers, metaReducers } from './app.reducer';
import { CustomSerializer } from './custom-route-serializer';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { MetamodelModule } from './metamodel/metamodel.module';
import { SampModule } from './samp/samp.module';
import { AppComponent } from './core/containers/app.component';
import { AppConfigService } from './app-config.service';
import { AppTitleStrategyService } from './app-title-strategy.service';
import { appInitializer } from './app-init';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        CoreModule,
        AuthModule,
        MetamodelModule,
        SampModule,
        AppRoutingModule,
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictStateSerializability: true,
                strictActionSerializability: true,
                strictActionWithinNgZone: true,
                strictActionTypeUniqueness: true,
            }
        }),
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot({
            serializer: CustomSerializer
        }),
        StoreDevtoolsModule.instrument({
            name: 'ANIS',
            maxAge: 25,
            logOnly: environment.production
        }),
        NgxMatomoTrackerModule.forRoot({
            mode: MatomoInitializationMode.AUTO_DEFERRED
        }),
        NgxMatomoRouterModule
    ],
    providers: [
        AppConfigService,
        {
            provide: TitleStrategy,
            useClass: AppTitleStrategyService
        },
        appInitializer
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
