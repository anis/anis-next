/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatabaseListComponent } from './containers/database-list.component';
import { NewDatabaseComponent } from './containers/new-database.component';
import { EditDatabaseComponent } from './containers/edit-database.component';
import { DatabaseTitleResolver } from './database-title.resolver';

const routes: Routes = [
    { path: 'database-list', component: DatabaseListComponent, title: 'Databases list' },
    { path: 'new-database', component: NewDatabaseComponent, title: 'New database' },
    { path: 'edit-database/:id', component: EditDatabaseComponent, title: DatabaseTitleResolver }
];

/**
 * @class
 * @classdesc Database routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DatabaseRoutingModule { }

export const routedComponents = [
    DatabaseListComponent,
    NewDatabaseComponent,
    EditDatabaseComponent
];
