/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, skipWhile } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import * as databaseSelector from 'src/app/metamodel/selectors/database.selector';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';

@Injectable({
    providedIn: 'root'
})
export class DatabaseTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(databaseSelector.selectDatabaseListIsLoaded).pipe(
            map(databaseListIsLoaded => {
                if (!databaseListIsLoaded) {
                    this.store.dispatch(databaseActions.loadDatabaseList());
                }
                return databaseListIsLoaded;
            }),
            skipWhile(databaseListIsLoaded => !databaseListIsLoaded),
            switchMap(() => {
                return this.store.select(databaseSelector.selectDatabaseByRouteId).pipe(
                    map(database => `Edit database ${database.label}`)
                );
            })
        );
    }
}
