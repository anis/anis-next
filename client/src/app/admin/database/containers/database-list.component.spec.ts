/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Database } from 'src/app/metamodel/models';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import { DatabaseListComponent } from './database-list.component';

describe('[admin][Database][Containers] DatabaseListComponent', () => {
    let component : DatabaseListComponent;
    let fixture : ComponentFixture<DatabaseListComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatabaseListComponent,
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(DatabaseListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#deleteDatabase(database) should dispatch deletedatabase action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        let database : Database = {id: 1, dbhost: 'test',dblogin: 'test@test.fr', dbname:'test',dbpassword: 'test',dbport: 808080, dbtype: '',label:''};
        component.deleteDatabase(database);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(databaseActions.deleteDatabase({database}));
    });
})
