/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import { Database } from 'src/app/metamodel/models';
import { EditDatabaseComponent } from './edit-database.component';

describe('[admin][Database][Containers] EditDatabaseComponent', () => {
    let component: EditDatabaseComponent;
    let fixture: ComponentFixture<EditDatabaseComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditDatabaseComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(EditDatabaseComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('#editDatabase(database) should dispatch editdatabase action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        let database : Database = {id: 1, dbhost: 'test', dblogin: 'test@test.fr', dbname: 'test', dbpassword: 'test', dbport: 808080, dbtype: '', label: ''};
        component.editDatabase(database);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(databaseActions.editDatabase({database}));
    });
})
