/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { Database } from 'src/app/metamodel/models';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';

@Component({
    selector: 'app-new-database',
    templateUrl: 'new-database.component.html'
})
export class NewDatabaseComponent {
    constructor(private store: Store<{ }>) { }

    addNewDatabase(database: Database) {
        this.store.dispatch(databaseActions.addDatabase({ database }));
    }
}
