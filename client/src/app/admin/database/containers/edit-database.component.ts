/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component} from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Database } from 'src/app/metamodel/models';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import * as databaseSelector from 'src/app/metamodel/selectors/database.selector';

@Component({
    selector: 'app-edit-database',
    templateUrl: 'edit-database.component.html'
})
export class EditDatabaseComponent {
    public databaseListIsLoading: Observable<boolean>;
    public databaseListIsLoaded: Observable<boolean>;
    public database: Observable<Database>;

    constructor(private store: Store<{ }>) {
        this.databaseListIsLoading = store.select(databaseSelector.selectDatabaseListIsLoading);
        this.databaseListIsLoaded = store.select(databaseSelector.selectDatabaseListIsLoaded);
        this.database = store.select(databaseSelector.selectDatabaseByRouteId);
    }

    editDatabase(database: Database) {
        this.store.dispatch(databaseActions.editDatabase({ database }));
    }
}
