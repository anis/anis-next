/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Database } from 'src/app/metamodel/models';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import { NewDatabaseComponent } from './new-database.component';
import { Component } from '@angular/core';
@Component({selector: 'app-database-form'})
class DatabaseForm{}
describe('[admin][Database][Containers] NewDatabaseComponent', () => {
    let component: NewDatabaseComponent;
    let fixture: ComponentFixture<NewDatabaseComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
            NewDatabaseComponent,
            DatabaseForm
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(NewDatabaseComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('#addNewDatabase(database) should dispatch addDatabase action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        let database : Database = {id: 1, dbhost: 'test',dblogin: 'test@test.fr', dbname:'test',dbpassword: 'test',dbport: 808080, dbtype: '',label:''};
        component.addNewDatabase(database);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(databaseActions.addDatabase({database}));
    });
})
