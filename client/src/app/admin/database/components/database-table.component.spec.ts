/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DatabaseTableComponent } from './database-table.component';

describe('[admin][Database][Component] databaseTableComponent', () => {
    let component: DatabaseTableComponent;
    let fixture: ComponentFixture<DatabaseTableComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatabaseTableComponent,
            ],
            imports: [
                BrowserAnimationsModule,
            ]
        });
        fixture = TestBed.createComponent(DatabaseTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
