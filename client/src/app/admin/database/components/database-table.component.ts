/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { Database } from 'src/app/metamodel/models';

@Component({
    selector: 'app-database-table',
    templateUrl: 'database-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatabaseTableComponent {
    @Input() databaseList: Database[];
    @Output() deleteDatabase: EventEmitter<Database> = new EventEmitter();
}
