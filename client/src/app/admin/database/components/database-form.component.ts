/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { Database } from 'src/app/metamodel/models';

@Component({
    selector: 'app-database-form',
    templateUrl: 'database-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatabaseFormComponent implements OnInit {
    @Input() database: Database;
    @Output() onSubmit: EventEmitter<Database> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        dbname: new UntypedFormControl('', [Validators.required]),
        dbtype: new UntypedFormControl('', [Validators.required]),
        dbhost: new UntypedFormControl('', [Validators.required]),
        dbport: new UntypedFormControl('', [Validators.required]),
        dblogin: new UntypedFormControl('', [Validators.required]),
        dbpassword: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.database) {
            this.form.patchValue(this.database);
        }
    }

    submit() {
        if (this.database) {
            this.onSubmit.emit({
                ...this.database,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
