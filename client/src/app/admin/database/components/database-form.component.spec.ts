/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DatabaseFormComponent } from './database-form.component';

describe('[admin][Database][Component] databaseFormComponent', () => {
    let component: DatabaseFormComponent;
    let fixture: ComponentFixture<DatabaseFormComponent>;
    
    let spy;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatabaseFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ]
        });
        fixture = TestBed.createComponent(DatabaseFormComponent);
        component = fixture.componentInstance;
        component.database= {id: 1, dbhost: 'test', dblogin: 'test@test.fr', dbname:'test', dbpassword: 'test', dbport: 808080, dbtype: '', label:''};
        fixture.detectChanges();
        spy= jest.spyOn(component.onSubmit, 'emit');
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it(`should emit database and form values on subumit`, () => {
       component.submit();
       expect(spy).toHaveBeenCalledTimes(1);
    });

    it(`should emit database on subumit`, () => {
        component.database= null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
     });
});
