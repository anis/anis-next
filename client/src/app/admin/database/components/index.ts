/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatabaseTableComponent } from "./database-table.component";
import { DatabaseFormComponent } from "./database-form.component";

export const dummiesComponents = [
    DatabaseTableComponent,
    DatabaseFormComponent
];
