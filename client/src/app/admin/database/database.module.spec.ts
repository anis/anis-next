/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatabaseModule } from './database.module';

describe('[Database] DatabaseModule', () => {
    it('Test Database module', () => {
        expect(DatabaseModule.name).toEqual('DatabaseModule');
    });
});
