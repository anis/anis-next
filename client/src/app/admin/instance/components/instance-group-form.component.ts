/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { InstanceGroup, Instance } from 'src/app/metamodel/models';

@Component({
    selector: 'app-instance-group-form',
    templateUrl: 'instance-group-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceGroupFormComponent implements OnInit {
    @Input() instanceGroup: InstanceGroup;
    @Input() instanceList: Instance[];
    @Output() onSubmit: EventEmitter<InstanceGroup> = new EventEmitter();

    public instanceGroupInstances = [];
    public form = new UntypedFormGroup({
        role: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.instanceGroup) {
            this.form.patchValue(this.instanceGroup);
            this.instanceGroupInstances = [...this.instanceGroup.instances];
        }
    }

    submit() {
        if (this.instanceGroup) {
            this.onSubmit.emit({
                ...this.instanceGroup,
                ...this.form.value,
                instances: this.instanceGroupInstances
            });
        } else {
            this.onSubmit.emit({
                ...this.form.value,
                instances: this.instanceGroupInstances
            });
        }
    }

    getAvailableInstances(): Instance[] {
        return this.instanceList.filter(instance => !this.instanceGroupInstances.includes(instance.name));
    }

    addInstances(selectElement) {
        let availableInstanceSelected = [];
        for (var i = 0; i < selectElement.options.length; i++) {
            const optionElement = selectElement.options[i];
            if (optionElement.selected == true) {
                availableInstanceSelected.push(optionElement.value);
            }
        }
        this.instanceGroupInstances.push(...availableInstanceSelected);
        this.form.markAsDirty();
    }

    removeInstances(selectElement) {
        let instanceGroupInstancesSelected = [];
        for (var i = 0; i < selectElement.options.length; i++) {
            const optionElement = selectElement.options[i];
            if (optionElement.selected == true) {
                instanceGroupInstancesSelected.push(optionElement.value);
            }
        }
        this.instanceGroupInstances = [...this.instanceGroupInstances.filter(instance => !instanceGroupInstancesSelected.includes(instance))]
        this.form.markAsDirty();
    }
}
