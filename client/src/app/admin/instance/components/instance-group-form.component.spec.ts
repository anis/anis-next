/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Instance } from 'src/app/metamodel/models';
import { InstanceGroupFormComponent } from './instance-group-form.component';

describe('[admin][instance][Components] InstanceGroupFormComponent', () => {
    let component: InstanceGroupFormComponent;
    let fixture: ComponentFixture<InstanceGroupFormComponent>;
    let instance: Instance;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceGroupFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        })
        fixture = TestBed.createComponent(InstanceGroupFormComponent);
        component = fixture.componentInstance;
        component.instanceGroup = { id: 1, instances: [], role: 'test' };
        component.instanceList = [
            { ...instance, name: 'test1' },
            { ...instance, name: 'test2' },
            { ...instance, name: 'test3' },
            { ...instance, name: 'test4' }
        ]
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('submit() should emit instanceGroup and form.value and instanceGroupInstances', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({
            ...component.instanceGroup,
            ...component.form.value,
            instances: component.instanceGroupInstances
        });
    });
    it('submit() should emit form.value and instanceGroupInstances', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        component.instanceGroup = null;
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({ ...component.form.value, instances: component.instanceGroupInstances });
    });
    it('getAvailableInstances() should return a array of instances that  don t have their names in instanceGroupInstances', () => {
        component.instanceGroupInstances = ['test1', 'test2'];
        let expected = [{ ...instance, name: 'test3' }, { ...instance, name: 'test4' }];
        let result = component.getAvailableInstances();
        expect(result).toEqual(expected);
    });
    it('addInstances() should add the availableInstanceSelected in instanceGroupInstances array', () => {
        let selectElement = {
            options: [
                { selected: true, value: 'test1' }
            ],
        }
        let spy = jest.spyOn(component.form, 'markAsDirty');
        expect(component.instanceGroupInstances.length).toEqual(0);
        component.addInstances(selectElement);
        expect(component.instanceGroupInstances.length).toEqual(1);
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('removeInstances() should remove the instanceGroupInstancesSelected from instanceGroupInstances array', () => {
        let selectElement = {
            options: [
                { selected: true, value: 'test1' }
            ],
        }
        component.instanceGroupInstances = ['test1', 'test2'];
        let spy = jest.spyOn(component.form, 'markAsDirty');
        expect(component.instanceGroupInstances.length).toEqual(2);
        component.removeInstances(selectElement);
        expect(component.instanceGroupInstances.length).toEqual(1);
        expect(spy).toHaveBeenCalledTimes(1);
    });
})

