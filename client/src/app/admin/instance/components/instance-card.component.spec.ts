/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InstanceCardComponent } from './instance-card.component';

describe('[admin][instance][Components] InstanceCardComponent', () => {
    let component: InstanceCardComponent;
    let fixture: ComponentFixture<InstanceCardComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceCardComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        })
        fixture = TestBed.createComponent(InstanceCardComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
})

