/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterLogoFormComponent } from './footer-logo-form.component';

describe('[admin][instance][Components] FooterLogoFormComponent', () => {
    let component: FooterLogoFormComponent;
    let fixture: ComponentFixture<FooterLogoFormComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                FooterLogoFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        })
        fixture = TestBed.createComponent(FooterLogoFormComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
})

