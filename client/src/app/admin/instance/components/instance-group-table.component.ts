/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { InstanceGroup } from 'src/app/metamodel/models';

@Component({
    selector: 'app-instance-group-table',
    templateUrl: 'instance-group-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceGroupTableComponent {
    @Input() instanceGroupList: InstanceGroup[];
    @Output() deleteInstanceGroup: EventEmitter<InstanceGroup> = new EventEmitter();
}
