/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { Instance } from 'src/app/metamodel/models';

@Component({
    selector: 'app-instance-card',
    templateUrl: 'instance-card.component.html',
    styleUrls: [ 'instance-card.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceCardComponent {
    @Input() instance: Instance;
    @Output() deleteInstance: EventEmitter<Instance> = new EventEmitter();
}
