/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceCardComponent } from './instance-card.component';
import { InstanceFormComponent } from './instance-form.component';
import { FooterLogoFormComponent } from './footer-logo-form.component';
import { FooterLogosListComponent } from './footer-logos-list.component';
import { InstanceGroupTableComponent } from './instance-group-table.component';
import { InstanceGroupFormComponent } from './instance-group-form.component';

export const dummiesComponents = [
    InstanceCardComponent,
    InstanceFormComponent,
    FooterLogoFormComponent,
    FooterLogosListComponent,
    InstanceGroupTableComponent,
    InstanceGroupFormComponent
];
