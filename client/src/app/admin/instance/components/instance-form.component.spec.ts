/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Instance, Logo } from 'src/app/metamodel/models';
import { InstanceFormComponent } from './instance-form.component';

describe('[admin][instance][Components] InstanceFormComponent', () => {
    let component: InstanceFormComponent;
    let fixture: ComponentFixture<InstanceFormComponent>;
    let instance: Instance;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        })
        fixture = TestBed.createComponent(InstanceFormComponent);
        component = fixture.componentInstance;
        component.instance = { ...instance, label: 'test' };
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('ngOnInit() should enable search_multiple_all_datasets_selected and  search_multiple_label', () => {
        component.form.controls.search_multiple_allowed.setValue(true);
        expect(component.form.controls.search_multiple_label.enabled).toBe(false);
        expect(component.form.controls.search_multiple_all_datasets_selected.enabled).toBe(false);
        component.ngOnInit();
        expect(component.form.controls.search_multiple_label.enabled).toBe(true);
        expect(component.form.controls.search_multiple_all_datasets_selected.enabled).toBe(true);
    });
    it('ngOnInit() should enable documentation_label', () => {
        component.form.controls.documentation_allowed.setValue(true);
        expect(component.form.controls.documentation_label.enabled).toBe(false);
        component.ngOnInit();
        expect(component.form.controls.documentation_label.enabled).toBe(true);
    });
    it('isDataPathEmpty() should return false', () => {
        component.form.controls.data_path.setValue('test');
        let result = component.isDataPathEmpty();
        expect(result).toBe(false);
    });
    it('isFilesPathEmpty() should return false', () => {
        component.form.controls.files_path.setValue('test');
        let result = component.isFilesPathEmpty();
        expect(result).toBe(false);
    });
    it('onChangeFilesPath(path: string) should call emit on loadRootDirectory', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        component.form.controls.data_path.setValue('test_data_path');
        component.form.controls.files_path.setValue('test_files_path');

        component.onChangeFilesPath('test_path');
        let param = `${component.form.controls.data_path.value}${'test_path'}`
        expect(spy).toHaveBeenCalledWith(param);
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('onChangeFileSelect(path: string) should call emit on loadRootDirectory', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        component.onChangeFileSelect('test_path');
        let param = `${component.form.controls.data_path.value}${component.form.controls.files_path.value}${'test_path'}`
        expect(spy).toHaveBeenCalledWith(param);
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('getHomeConfigFormGroup should return an instance of UntypedFormGroup', () => {
        let result = component.getHomeConfigFormGroup();
        expect(result).toEqual(component.form.controls.home_component_config as UntypedFormGroup);
    });
    it('checkDisableSearchByCriteriaAllowed() should  enable search_by_criteria_label', () => {
        component.form.controls.search_by_criteria_allowed.setValue(true);
        component.form.controls.search_by_criteria_label.disable();
        expect(component.form.controls.search_by_criteria_label.enabled).toBe(false);
        component.checkDisableSearchByCriteriaAllowed();
        expect(component.form.controls.search_by_criteria_label.enabled).toBe(true);

    });
    it('checkDisableSearchByCriteriaAllowed() should  desable search_by_criteria_label', () => {
        component.form.controls.search_by_criteria_allowed.setValue(false);
        component.form.controls.search_by_criteria_label.enable();
        expect(component.form.controls.search_by_criteria_label.enabled).toBe(true);
        component.checkDisableSearchByCriteriaAllowed();
        expect(component.form.controls.search_by_criteria_label.enabled).toBe(false);

    });
    it('checkDisableAllDatasetsSelected() should  enable search_multiple_label and search_multiple_all_datasets_selected ', () => {
        component.form.controls.search_multiple_allowed.setValue(true);
        component.form.controls.search_multiple_label.disable();
        component.form.controls.search_multiple_all_datasets_selected.disable();
        expect(component.form.controls.search_multiple_label.enabled).toBe(false);
        expect(component.form.controls.search_multiple_all_datasets_selected.enabled).toBe(false);
        component.checkDisableAllDatasetsSelected();
        expect(component.form.controls.search_multiple_label.enabled).toBe(true);
        expect(component.form.controls.search_multiple_all_datasets_selected.enabled).toBe(true);

    });
    it('checkDisableDocumentationAllowed() should  desable search_multiple_label and search_multiple_all_datasets_selected ', () => {
        component.form.controls.search_multiple_allowed.setValue(false);
        component.form.controls.search_multiple_label.enable();
        component.form.controls.search_multiple_all_datasets_selected.enable();
        expect(component.form.controls.search_multiple_label.enabled).toBe(true);
        expect(component.form.controls.search_multiple_all_datasets_selected.enabled).toBe(true);
        component.checkDisableAllDatasetsSelected();
        expect(component.form.controls.search_multiple_label.enabled).toBe(false);
        expect(component.form.controls.search_multiple_all_datasets_selected.enabled).toBe(false);

    });
    it('checkDisableSearchByCriteriaAllowed() should enable documentation_label', () => {
        component.form.controls.documentation_allowed.setValue(true);
        component.form.controls.documentation_label.disable();
        expect(component.form.controls.documentation_label.enabled).toBe(false);
        component.checkDisableDocumentationAllowed();
        expect(component.form.controls.documentation_label.enabled).toBe(true);

    });
    it('checkDisableSearchByCriteriaAllowed() should  desable documentation_label', () => {
        component.form.controls.documentation_allowed.setValue(false);
        component.form.controls.documentation_label.enable();
        expect(component.form.controls.documentation_label.enabled).toBe(true);
        component.checkDisableDocumentationAllowed();
        expect(component.form.controls.documentation_label.enabled).toBe(false);

    });
    it('getFooterLogoListByDisplay() return a array of logo sorted by display ', () => {

        let footer_logos: Logo[] = [
            { display: 4, file: 'test1', title: 'test1', href: 'test1' },
            { display: 2, file: 'test2', title: 'test2', href: 'test2' },
            { display: 3, file: 'test3', title: 'test3', href: 'test4' },
        ];
        component.instance.footer_logos = footer_logos;
        let result = component.getFooterLogoListByDisplay();
        let expected = [
            { display: 2, file: 'test2', title: 'test2', href: 'test2' },
            { display: 3, file: 'test3', title: 'test3', href: 'test4' },
            { display: 4, file: 'test1', title: 'test1', href: 'test1' },
        ]
        expect(result).toEqual(expected);

    });
    it('submit() should emit instance and form.getRawValue()', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        let spyOnForm = jest.spyOn(component.form, 'markAsPristine');
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({ ...component.instance, ...component.form.getRawValue() });
        expect(spyOnForm).toHaveBeenCalledTimes(1);

    });
    it('submit() should emit  form.getRawValue()', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        component.instance = null;
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    });

})

