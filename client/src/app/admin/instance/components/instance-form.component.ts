/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormArray, UntypedFormControl, Validators } from '@angular/forms';

import { Instance } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';

@Component({
    selector: 'app-instance-form',
    templateUrl: 'instance-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceFormComponent implements OnInit {
    @Input() instance: Instance;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() onSubmit: EventEmitter<Instance> = new EventEmitter();

    footerLogosFormArray = new UntypedFormArray([]);

    public form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        description: new UntypedFormControl('', [Validators.required]),
        scientific_manager: new UntypedFormControl('', [Validators.required]),
        instrument: new UntypedFormControl('', [Validators.required]),
        wavelength_domain: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
        data_path: new UntypedFormControl(''),
        files_path: new UntypedFormControl(''),
        public: new UntypedFormControl(true, [Validators.required]),
        portal_logo: new UntypedFormControl(''),
        portal_color: new UntypedFormControl('#7AC29A', [Validators.required]),
        design_background_color: new UntypedFormControl('#FFFFFF', [Validators.required]),
        design_text_color: new UntypedFormControl('#212529'),
        design_font_family: new UntypedFormControl('Roboto, sans-serif'),
        design_link_color: new UntypedFormControl('#007BFF'),
        design_link_hover_color: new UntypedFormControl('#0056B3'),
        design_logo: new UntypedFormControl(''),
        design_logo_href: new UntypedFormControl(null),
        design_favicon: new UntypedFormControl(''),
        navbar_background_color: new UntypedFormControl('#F8F9FA'),
        navbar_border_bottom_color: new UntypedFormControl('#DEE2E6'),
        navbar_color_href: new UntypedFormControl('#000000'),
        navbar_font_family: new UntypedFormControl('auto'),
        navbar_sign_in_btn_color: new UntypedFormControl('#28A745'),
        navbar_user_btn_color: new UntypedFormControl('#7AC29A'),
        footer_background_color: new UntypedFormControl('#F8F9FA'),
        footer_border_top_color: new UntypedFormControl('#DEE2E6'),
        footer_text_color: new UntypedFormControl('#000000'),
        footer_logos: this.footerLogosFormArray,
        family_border_color: new UntypedFormControl('#DFDFDF'),
        family_header_background_color: new UntypedFormControl('#F7F7F7'),
        family_title_color: new UntypedFormControl('#007BFF'),
        family_title_bold: new UntypedFormControl(false),
        family_background_color: new UntypedFormControl('#FFFFFF'),
        family_text_color: new UntypedFormControl('#212529'),
        progress_bar_title: new UntypedFormControl('Dataset search'),
        progress_bar_title_color: new UntypedFormControl('#000000'),
        progress_bar_subtitle: new UntypedFormControl('Select a dataset, add criteria, select output columns and display the result.'),
        progress_bar_subtitle_color: new UntypedFormControl('#6C757D'),
        progress_bar_step_dataset_title: new UntypedFormControl('Dataset selection'),
        progress_bar_step_criteria_title: new UntypedFormControl('Search criteria'),
        progress_bar_step_output_title: new UntypedFormControl('Output columns'),
        progress_bar_step_result_title: new UntypedFormControl('Result table'),
        progress_bar_color: new UntypedFormControl('#E9ECEF'),
        progress_bar_active_color: new UntypedFormControl('#7AC29A'),
        progress_bar_circle_color: new UntypedFormControl('#FFFFFF'),
        progress_bar_circle_icon_color: new UntypedFormControl('#CCCCCC'),
        progress_bar_circle_icon_active_color: new UntypedFormControl('#FFFFFF'),
        progress_bar_text_color: new UntypedFormControl('#91B2BF'),
        progress_bar_text_bold: new UntypedFormControl(false),
        search_next_btn_color: new UntypedFormControl('#007BFF'),
        search_next_btn_hover_color: new UntypedFormControl('#007BFF'),
        search_next_btn_hover_text_color: new UntypedFormControl('#FFFFFF'),
        search_back_btn_color: new UntypedFormControl('#6C757D'),
        search_back_btn_hover_color: new UntypedFormControl('#6C757D'),
        search_back_btn_hover_text_color: new UntypedFormControl('#FFFFFF'),
        search_info_background_color: new UntypedFormControl('#E9ECEF'),
        search_info_text_color: new UntypedFormControl('#000000'),
        search_info_help_enabled: new UntypedFormControl(true),
        dataset_select_btn_color: new UntypedFormControl('#6C757D'),
        dataset_select_btn_hover_color: new UntypedFormControl('#6C757D'),
        dataset_select_btn_hover_text_color: new UntypedFormControl('#FFFFFF'),
        dataset_selected_icon_color: new UntypedFormControl('#28A745'),
        search_criterion_background_color: new UntypedFormControl('#7AC29A'),
        search_criterion_text_color: new UntypedFormControl('#000000'),
        output_columns_selected_color: new UntypedFormControl('#7AC29A'),
        output_columns_select_all_btn_color: new UntypedFormControl('#6C757D'),
        output_columns_select_all_btn_hover_color: new UntypedFormControl('#6C757D'),
        output_columns_select_all_btn_hover_text_color: new UntypedFormControl('#FFFFFF'),
        result_panel_border_size: new UntypedFormControl('1px'),
        result_panel_border_color: new UntypedFormControl('#DEE2E6'),
        result_panel_title_color: new UntypedFormControl('#000000'),
        result_panel_background_color: new UntypedFormControl('#FFFFFF'),
        result_panel_text_color: new UntypedFormControl('#000000'),
        result_download_btn_color: new UntypedFormControl('#007BFF'),
        result_download_btn_hover_color: new UntypedFormControl('#0069D9'),
        result_download_btn_text_color: new UntypedFormControl('#FFFFFF'),
        result_datatable_actions_btn_color: new UntypedFormControl('#007BFF'),
        result_datatable_actions_btn_hover_color: new UntypedFormControl('#0069D9'),
        result_datatable_actions_btn_text_color: new UntypedFormControl('#FFFFFF'),
        result_datatable_bordered: new UntypedFormControl(true),
        result_datatable_bordered_radius: new UntypedFormControl(false),
        result_datatable_border_color: new UntypedFormControl('#DEE2E6'),
        result_datatable_header_background_color: new UntypedFormControl('#FFFFFF'),
        result_datatable_header_text_color: new UntypedFormControl('#000000'),
        result_datatable_rows_background_color: new UntypedFormControl('#FFFFFF'),
        result_datatable_rows_text_color: new UntypedFormControl('#000000'),
        result_datatable_sorted_color: new UntypedFormControl('#C5C5C5'),
        result_datatable_sorted_active_color: new UntypedFormControl('#000000'),
        result_datatable_link_color: new UntypedFormControl('#007BFF'),
        result_datatable_link_hover_color: new UntypedFormControl('#0056B3'),
        result_datatable_rows_selected_color: new UntypedFormControl('#7AC29A'),
        result_datatable_pagination_link_color: new UntypedFormControl('#007BFF'),
        result_datatable_pagination_active_bck_color: new UntypedFormControl('#007BFF'),
        result_datatable_pagination_active_text_color: new UntypedFormControl('#007BFF'),
        samp_enabled: new UntypedFormControl(true),
        back_to_portal: new UntypedFormControl(true),
        user_menu_enabled: new UntypedFormControl(true),
        search_by_criteria_allowed: new UntypedFormControl(true),
        search_by_criteria_label: new UntypedFormControl({ value: 'Search', disabled: false }),
        search_multiple_allowed: new UntypedFormControl(false),
        search_multiple_label: new UntypedFormControl({ value: 'Search multiple', disabled: true }),
        search_multiple_all_datasets_selected: new UntypedFormControl({ value: false, disabled: true }),
        search_multiple_progress_bar_title: new UntypedFormControl('Search around a position in multiple datasets'),
        search_multiple_progress_bar_subtitle: new UntypedFormControl('Fill RA & DEC position, select datasets and display the result.'),
        search_multiple_progress_bar_step_position:new UntypedFormControl('Position'),
        search_multiple_progress_bar_step_datasets:new UntypedFormControl('Datasets'),
        search_multiple_progress_bar_step_result:new UntypedFormControl('Result'),
        documentation_allowed: new UntypedFormControl(false),
        documentation_label: new UntypedFormControl({ value: 'Documentation', disabled: true })
    });

    ngOnInit() {
        if (this.instance) {
            this.form.patchValue(this.instance);
            this.form.controls.name.disable();
            if (this.form.controls.search_multiple_allowed.value) {
                this.form.controls.search_multiple_label.enable();
                this.form.controls.search_multiple_all_datasets_selected.enable();
            }
            if (this.form.controls.search_by_criteria_allowed.value) {
                this.form.controls.search_by_criteria_label.enable();
            }
            if (this.form.controls.documentation_allowed.value) {
                this.form.controls.documentation_label.enable();
            }
        }
    }

    isDataPathEmpty() {
        return this.form.controls.data_path.value == '';
    }

    isFilesPathEmpty() {
        return this.form.controls.files_path.value == '';
    }

    onChangeFilesPath(path: string) {
        this.loadRootDirectory.emit(`${this.form.controls.data_path.value}${path}`)
    }

    onChangeFileSelect(path: string) {
        this.loadRootDirectory.emit(`${this.form.controls.data_path.value}${this.form.controls.files_path.value}${path}`);
    }

    getHomeConfigFormGroup() {
        return this.form.controls.home_component_config as UntypedFormGroup;
    }

    checkDisableSearchByCriteriaAllowed() {
        if (this.form.controls.search_by_criteria_allowed.value) {
            this.form.controls.search_by_criteria_label.enable();
        } else {
            this.form.controls.search_by_criteria_label.disable();
        }
    }

    checkDisableAllDatasetsSelected() {
        if (this.form.controls.search_multiple_allowed.value) {
            this.form.controls.search_multiple_label.enable();
            this.form.controls.search_multiple_all_datasets_selected.enable();
        } else {
            this.form.controls.search_multiple_label.disable();
            this.form.controls.search_multiple_all_datasets_selected.setValue(false);
            this.form.controls.search_multiple_all_datasets_selected.disable();
        }
    }

    checkDisableDocumentationAllowed() {
        if (this.form.controls.documentation_allowed.value) {
            this.form.controls.documentation_label.enable();
        } else {
            this.form.controls.documentation_label.disable();
        }
    }

    getFooterLogoListByDisplay() {
        if (this.instance && this.instance.footer_logos) {
            return [...this.instance.footer_logos].sort((a, b) => a.display - b.display);
        } else {
            return [];
        }
    }

    submit() {
        if (this.instance) {
            this.onSubmit.emit({
                ...this.instance,
                ...this.form.getRawValue()
            });
            this.form.markAsPristine();
        } else {
            this.onSubmit.emit(this.form.getRawValue());
        }
    }
}
