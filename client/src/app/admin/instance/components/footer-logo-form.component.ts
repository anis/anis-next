import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { UntypedFormGroup } from '@angular/forms';

@Component({
    selector: 'app-footer-logo-form',
    templateUrl: 'footer-logo-form.component.html',
    styleUrls: [ 'footer-logo-form.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterLogoFormComponent {
    @Input() form: UntypedFormGroup;
}
