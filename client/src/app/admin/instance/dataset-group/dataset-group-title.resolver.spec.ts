/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { cold } from 'jasmine-marbles';
import { DatasetGroupTitleResolver } from './dataset-group-title.resolver';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Instance } from 'src/app/metamodel/models';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';

describe('[admin][instance][dataset-group] DatasetGroupTitleResolver', () => {

class MockActivatedRouteSnapshot extends ActivatedRouteSnapshot {
    constructor() {
        super()
    }
    component: any;
}

    let datasetGroupTitleResolver: DatasetGroupTitleResolver;
    let store: MockStore;
    let mockInstanceSelectorSelectInstanceByRouteName;
    let mockInstanceSelectorSelectInstanceListIsLoaded;
    let route: MockActivatedRouteSnapshot;
    let instance: Instance;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                DatasetGroupTitleResolver,
                provideMockStore({}),
            ]
        })
        instance = { ...instance, label: 'test_label', }
        store = TestBed.inject(MockStore);
        datasetGroupTitleResolver = TestBed.inject(DatasetGroupTitleResolver);
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
    });

    it('should be created', () => {
        expect(datasetGroupTitleResolver).toBeTruthy();
    });

    it('resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) should dispatch loadInstanceList action', () => {
        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, false);
        let spy = jest.spyOn(store, 'dispatch');
        let result = cold('a', { a: datasetGroupTitleResolver.resolve(route, null) });
        const expected = cold('a', { a: [] });
        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.loadInstanceList())
    });
    it('should return test_label - Dataset-groups list', () =>{

        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        route = {
            ...route,
            component: { name: 'DatasetGroupListComponent' },
            firstChild: null,
            root: null,
            parent: null,
            children: null,
            paramMap: null,
            queryParamMap: null,
            pathFromRoot: null
        };

        let result = datasetGroupTitleResolver.resolve(route, null);
        let expected = cold('a', {a: 'test_label - Dataset-groups list'});
        expect(result).toBeObservable(expected);
    });
    it('should return test_label - New dataset-group', () =>{

        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        route = {
            ...route,
            component: { name: 'TestComponent' },
            firstChild: null,
            root: null,
            parent: null,
            children: null,
            paramMap: null,
            queryParamMap: null,
            pathFromRoot: null
        };

        let result = datasetGroupTitleResolver.resolve(route, null);
        let expected = cold('a', {a: 'test_label - New dataset-group'});
        expect(result).toBeObservable(expected);
    });
});
