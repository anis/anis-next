/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable, combineLatest } from 'rxjs';
import { map, switchMap, skipWhile } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';

@Injectable({
    providedIn: 'root'
})
export class EditDatasetGroupTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(datasetGroupSelector.selectDatasetGroupListIsLoaded).pipe(
            map(datasetGroupListIsLoaded => {
                if (!datasetGroupListIsLoaded) {
                    this.store.dispatch(datasetGroupActions.loadDatasetGroupList());
                }
                return datasetGroupListIsLoaded;
            }),
            skipWhile(datasetGroupListIsLoaded => !datasetGroupListIsLoaded),
            switchMap(() => {
                return combineLatest([
                    this.store.pipe(select(instanceSelector.selectInstanceByRouteName)),
                    this.store.pipe(select(datasetGroupSelector.selectDatasetGroupByRouteId))
                ]).pipe(
                    map(([instance, datasetGroup]) => `${instance.label} - Edit dataset-group ${datasetGroup.role}`)
                );
            })
        );
    }
}
