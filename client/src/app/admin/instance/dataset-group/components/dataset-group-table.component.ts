/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { DatasetGroup } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-group-table',
    templateUrl: 'dataset-group-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetGroupTableComponent {
    @Input() datasetGroupList: DatasetGroup[];
    @Output() deleteDatasetGroup: EventEmitter<DatasetGroup> = new EventEmitter();
}
