/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Dataset, DatasetGroup } from 'src/app/metamodel/models';
import { DatasetGroupFormComponent } from './dataset-group-form.component';

describe('[admin][instance][dataset-group][components] DatasetGroupFormComponent', () => {
    let component: DatasetGroupFormComponent;
    let fixture: ComponentFixture<DatasetGroupFormComponent>;
    let store: MockStore;
    let spy;
    let datasetGroup: DatasetGroup = { datasets: [], id: 1, instance_name: 'test', role: '' };
    let dataset: Dataset = {
        cone_search_config_id: 1,
        data_path: '',
        datatable_enabled: false,
        datatable_selectable_rows: true,
        description: '',
        display: 10,
        download_ascii: true,
        download_csv: false,
        download_json: false,
        download_vo: false,
        download_fits: false,
        full_data_path: '',
        id_database: 10,
        id_dataset_family: 1,
        label: '',
        name: '',
        public: true,
        server_link_enabled: false,
        table_ref: ''
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetGroupFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(DatasetGroupFormComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);

        component.datasetList = [];
        component.datasetGroup = datasetGroup;
        spy = jest.spyOn(component.onSubmit, 'emit');
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('submit() should emit datasetgroup, form.value and datasets', () => {
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...datasetGroup, ...component.form.value, datasets: component.groupDatasets })
    });

    it('submit() should emit form.value and datasets', () => {
        component.datasetGroup = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.value, datasets: component.groupDatasets })
    });

    it('getAvailableDatasets() should return an array with 2 elements', () => {
        component.datasetList = [{ ...dataset, name: 'test1' }, { ...dataset, name: 'test2' }, { ...dataset, name: 'test1' }];
        component.groupDatasets = ['test2'];
        let result = component.getAvailableDatasets();
        expect(result.length).toEqual(2);
    });

    it('addDatasets(selectElement) should add  the  dataset in groupDataSets array', () => {
        let selectElement = { options: [{ selected: true, value: { ...dataset, name: 'test' } }] };
        component.groupDatasets = [];
        expect(component.groupDatasets.length).toEqual(0)
        component.addDatasets(selectElement);
        expect(component.groupDatasets.length).toEqual(1);
        expect(component.groupDatasets[0]).toEqual(selectElement.options[0].value)
    });

    it('removeDatasets(selectElement)  should remove the dataset from groupDataSets', () => {
        let selectElement = { options: [{ selected: true, value: dataset }] };
        component.groupDatasets = [dataset];
        expect(component.groupDatasets.length).toEqual(1);
        component.removeDatasets(selectElement)
        expect(component.groupDatasets.length).toEqual(0);
    });
})
