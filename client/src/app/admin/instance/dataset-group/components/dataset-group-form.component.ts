/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { DatasetGroup, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-group-form',
    templateUrl: 'dataset-group-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetGroupFormComponent implements OnInit {
    @Input() datasetGroup: DatasetGroup;
    @Input() datasetList: Dataset[];
    @Output() onSubmit: EventEmitter<DatasetGroup> = new EventEmitter();

    public groupDatasets = [];
    public form = new UntypedFormGroup({
        role: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.datasetGroup) {
            this.form.patchValue(this.datasetGroup);
            this.groupDatasets = [...this.datasetGroup.datasets];
        }
    }

    submit() {
        if (this.datasetGroup) {
            this.onSubmit.emit({
                ...this.datasetGroup,
                ...this.form.value,
                datasets: this.groupDatasets
            });
        } else {
            this.onSubmit.emit({
                ...this.form.value,
                datasets: this.groupDatasets
            });
        }
    }

    getAvailableDatasets(): Dataset[] {
        return this.datasetList.filter(dataset => !this.groupDatasets.includes(dataset.name));
    }

    addDatasets(selectElement) {
        let availableDatasetsSelected = [];
        for (var i = 0; i < selectElement.options.length; i++) {
            const optionElement = selectElement.options[i];
            if (optionElement.selected == true) {
                availableDatasetsSelected.push(optionElement.value);
            }
        }
        this.groupDatasets.push(...availableDatasetsSelected);
        this.form.markAsDirty();
    }

    removeDatasets(selectElement) {
        let groupDatasetsSelected = [];
        for (var i = 0; i < selectElement.options.length; i++) {
            const optionElement = selectElement.options[i];
            if (optionElement.selected == true) {
                groupDatasetsSelected.push(optionElement.value);
            }
        }
        this.groupDatasets = [...this.groupDatasets.filter(dataset => !groupDatasetsSelected.includes(dataset))]
        this.form.markAsDirty();
    }
}
