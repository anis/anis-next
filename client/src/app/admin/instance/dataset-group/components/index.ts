/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetGroupTableComponent } from "./dataset-group-table.component";
import { DatasetGroupFormComponent } from "./dataset-group-form.component";

export const dummiesComponents = [
    DatasetGroupTableComponent,
    DatasetGroupFormComponent
];
