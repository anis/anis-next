/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatasetGroupListComponent } from './containers/dataset-group-list.component';
import { NewDatasetGroupComponent } from './containers/new-dataset-group.component';
import { EditDatasetGroupComponent } from './containers/edit-dataset-group.component';
import { DatasetGroupTitleResolver } from './dataset-group-title.resolver';
import { EditDatasetGroupTitleResolver } from './edit-dataset-group-title.resolver';

const routes: Routes = [
    { path: '', component: DatasetGroupListComponent, title: DatasetGroupTitleResolver },
    { path: 'new-group', component: NewDatasetGroupComponent, title: DatasetGroupTitleResolver },
    { path: 'edit-group/:id', component: EditDatasetGroupComponent, title: EditDatasetGroupTitleResolver },
];

/**
 * @class
 * @classdesc Dataset group routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DatasetGroupRoutingModule { }

export const routedComponents = [
    DatasetGroupListComponent,
    NewDatasetGroupComponent,
    EditDatasetGroupComponent
];
