/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { cold, hot } from 'jasmine-marbles';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { DatasetGroup, Instance } from 'src/app/metamodel/models';
import { EditDatasetGroupTitleResolver } from './edit-dataset-group-title.resolver';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';

describe('[admin][instance][dataset-group] DatasetGroupTitleResolver', () => {
    let editDatasetGroupTitleResolver: EditDatasetGroupTitleResolver;
    let store: MockStore;
    let mockDatasetGroupSelectorGroupListIsLoaded;
    let mockInstanceSelectorInstanceByRouteName;
    let mockDatasetGroupSelectorDatasetGroupByRouteId;
    let instance: Instance;
    let datasetGroup: DatasetGroup;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                EditDatasetGroupTitleResolver,
                provideMockStore({}),
            ]
        })
        instance = { ...instance, label: 'instance_test_label' }

        store = TestBed.inject(MockStore);
        datasetGroup = { datasets: [], id: 1, instance_name: 'test', role: 'test' };

        editDatasetGroupTitleResolver = TestBed.inject(EditDatasetGroupTitleResolver);
        mockDatasetGroupSelectorDatasetGroupByRouteId = store.overrideSelector(datasetGroupSelector.selectDatasetGroupByRouteId, datasetGroup);
        mockDatasetGroupSelectorGroupListIsLoaded = store.overrideSelector(datasetGroupSelector.selectDatasetGroupListIsLoaded, false);
        mockInstanceSelectorInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
    });

    it('should be created', () => {
        expect(editDatasetGroupTitleResolver).toBeTruthy();
    });

    it('shoud dispatch datasetGroupActions.loadDatasetGroupList() action and return datasetGroupListIsLoaded', () => {
        const expected = cold('a', { a: [] });
        let spy = jest.spyOn(store, 'dispatch');
        let result = hot('a', { a: editDatasetGroupTitleResolver.resolve(null, null) });

        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should return test - Edit dataset-group test', () => {
        mockDatasetGroupSelectorGroupListIsLoaded = store.overrideSelector(datasetGroupSelector.selectDatasetGroupListIsLoaded, true);
        let result = editDatasetGroupTitleResolver.resolve(null, null);
        const expected = cold('a', { a: instance.label + " - Edit dataset-group " + datasetGroup.role });
        expect(result).toBeObservable(expected);
    });
});
