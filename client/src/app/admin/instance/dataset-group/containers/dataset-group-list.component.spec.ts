/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { DatasetGroup } from 'src/app/metamodel/models';
import { DatasetGroupListComponent } from './dataset-group-list.component';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';

@Component({
    template: ''
})
class DummyComponent { }

describe('[admin][instance][dataset-group][containers] DatasetGroupListComponent', () => {
    let component: DatasetGroupListComponent;
    let fixture: ComponentFixture<DatasetGroupListComponent>;
    let store: MockStore;
    let spy;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetGroupListComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule.withRoutes([
                    { path: 'test', component: DummyComponent }
                ])
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(DatasetGroupListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('deleteDatasetGroup(datasetGroup: DatasetGroup) should dispatch deleteDatasetGroup action', () => {
        let datasetGroup: DatasetGroup = { datasets: [], id: 1, instance_name: 'test', role: 'test' };
        spy = jest.spyOn(store, 'dispatch');
        component.deleteDatasetGroup(datasetGroup);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetGroupActions.deleteDatasetGroup({ datasetGroup }))
    });
})
