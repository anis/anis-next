/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { DatasetGroup, Dataset } from 'src/app/metamodel/models';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

@Component({
    selector: 'app-new-dataset-group',
    templateUrl: 'new-dataset-group.component.html'
})
export class NewDatasetGroupComponent {
    public instanceName: Observable<string>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;

    constructor(private store: Store<{ }>) {
        this.instanceName = store.select(instanceSelector.selectInstanceNameByRoute);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
        this.datasetList = store.select(datasetSelector.selectAllDatasets);
    }

    addNewDatasetGroup(datasetGroup: DatasetGroup) {
        this.store.dispatch(datasetGroupActions.addDatasetGroup({ datasetGroup }));
    }
}
