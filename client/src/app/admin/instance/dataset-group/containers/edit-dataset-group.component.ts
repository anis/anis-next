/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { DatasetGroup, Dataset } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

@Component({
    selector: 'app-edit-dataset-group',
    templateUrl: 'edit-dataset-group.component.html'
})
export class EditDatasetGroupComponent {
    public instanceName: Observable<string>;
    public datasetGroupListIsLoading: Observable<boolean>;
    public datasetGroupListIsLoaded: Observable<boolean>;
    public datasetGroup: Observable<DatasetGroup>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;

    constructor(private store: Store<{ }>) {
        this.instanceName = store.select(instanceSelector.selectInstanceNameByRoute);
        this.datasetGroupListIsLoading = store.select(datasetGroupSelector.selectDatasetGroupListIsLoading);
        this.datasetGroupListIsLoaded = store.select(datasetGroupSelector.selectDatasetGroupListIsLoaded);
        this.datasetGroup = store.select(datasetGroupSelector.selectDatasetGroupByRouteId);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
        this.datasetList = store.select(datasetSelector.selectAllDatasets);
    }

    editDatasetGroup(datasetGroup: DatasetGroup) {
        this.store.dispatch(datasetGroupActions.editDatasetGroup({ datasetGroup }));
    }
}
