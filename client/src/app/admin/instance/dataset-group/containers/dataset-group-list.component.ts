/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { DatasetGroup } from 'src/app/metamodel/models';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-dataset-group-list',
    templateUrl: 'dataset-group-list.component.html'
})
export class DatasetGroupListComponent {
    public instanceName: Observable<string>;
    public datasetGroupListIsLoading: Observable<boolean>;
    public datasetGroupListIsLoaded: Observable<boolean>;
    public datasetGroupList: Observable<DatasetGroup[]>;

    constructor(private store: Store<{ }>) {
        this.instanceName = store.select(instanceSelector.selectInstanceNameByRoute);
        this.datasetGroupListIsLoading = store.select(datasetGroupSelector.selectDatasetGroupListIsLoading);
        this.datasetGroupListIsLoaded = store.select(datasetGroupSelector.selectDatasetGroupListIsLoaded);
        this.datasetGroupList = store.select(datasetGroupSelector.selectAllDatasetGroups);
    }

    deleteDatasetGroup(datasetGroup: DatasetGroup) {
        this.store.dispatch(datasetGroupActions.deleteDatasetGroup({ datasetGroup }));
    }
}
