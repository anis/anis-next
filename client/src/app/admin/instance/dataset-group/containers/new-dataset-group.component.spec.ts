/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { DatasetGroup } from 'src/app/metamodel/models';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';
import { NewDatasetGroupComponent } from './new-dataset-group.component';
import { Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

@Component({
    template: ''
})
class DummyComponent { }

describe('[admin][instance][dataset-group][containers] NewDatasetGroupComponent', () => {
    let component: NewDatasetGroupComponent;
    let fixture: ComponentFixture<NewDatasetGroupComponent>;
    let store: MockStore;
    let spy;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewDatasetGroupComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule.withRoutes([
                    { path: 'test', component: DummyComponent }
                ])
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(NewDatasetGroupComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('deleteDatasetGroup(datasetGroup: DatasetGroup) should dispatch deleteDatasetGroup action', () => {
        let datasetGroup: DatasetGroup = { datasets: [], id: 1, instance_name: 'test', role: 'test' };
        spy = jest.spyOn(store, 'dispatch');
        component.addNewDatasetGroup(datasetGroup);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetGroupActions.addDatasetGroup({ datasetGroup }))
    });
})
