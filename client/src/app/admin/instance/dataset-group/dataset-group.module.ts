/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { DatasetGroupRoutingModule, routedComponents } from './dataset-group-routing.module';
import { dummiesComponents } from './components';

import { AdminSharedModule } from '../../admin-shared/admin-shared.module';

/**
 * @class
 * @classdesc Instance module.
 */
@NgModule({
    imports: [
        SharedModule,
        DatasetGroupRoutingModule,
        AdminSharedModule
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ]
})
export class DatasetGroupModule { }
