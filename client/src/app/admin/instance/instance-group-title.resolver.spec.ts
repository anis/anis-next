/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { InstanceGroupTitleResolver } from './instance-group-title.resolver';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';
import { InstanceGroup } from 'src/app/metamodel/models';
import { cold } from 'jasmine-marbles';

describe('[admin][instance] InstanceGroupTitleResolver', () => {
    let instanceGroupTitleResolver: InstanceGroupTitleResolver;
    let store: MockStore;
    let mockInstanceGroupSelectorInstanceGroup;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                InstanceGroupTitleResolver,
                provideMockStore({}),
            ]
        })

        store = TestBed.inject(MockStore);
        instanceGroupTitleResolver = TestBed.inject(InstanceGroupTitleResolver);
        let instanceGroup: InstanceGroup = { id: 1, instances: [], role: 'test_instance_group' };
        mockInstanceGroupSelectorInstanceGroup = store.overrideSelector(instanceGroupSelector.selectInstanceGroupByRouteId, instanceGroup);
    });
    it('should be created', () => {
        expect(InstanceGroupTitleResolver).toBeTruthy();
    });
    it('should return "Edit database test"', () => {
        let result = instanceGroupTitleResolver.resolve(null, null);
        const expected = cold('a', { a: 'Edit instance-group test_instance_group' });
        expect(result).toBeObservable(expected);
    });
});
