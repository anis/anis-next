/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Instance } from 'src/app/metamodel/models';
import { InstanceListComponent } from './instance-list.component';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';

describe('[admin][instance][Containers] InstanceListComponent', () => {
    let component: InstanceListComponent;
    let fixture: ComponentFixture<InstanceListComponent>;
    let store: MockStore;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceListComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(InstanceListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('deleteInstance(instance: Instance) should dispatch deleteInstance action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let instance: Instance;
        component.deleteInstance({ ...instance, name: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.deleteInstance({ instance: { ...instance, name: 'test' } }));
    })
})
