/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { InstanceGroupListComponent } from './instance-group-list.component';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';

describe('[admin][instance][Containers] InstanceGroupListComponent', () => {
    let component: InstanceGroupListComponent;
    let fixture: ComponentFixture<InstanceGroupListComponent>;
    let store: MockStore;
    let mockInstanceGroupSelectorInstanceGroupList;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceGroupListComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(InstanceGroupListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        mockInstanceGroupSelectorInstanceGroupList = store.overrideSelector(instanceGroupSelector.selectAllInstanceGroups, [
            { id: 1, instances: [], role: 'test' }
        ])
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('deleteInstanceGroup(instanceGroup: InstanceGroup) should dispatch deleteInstanceGroup action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.deleteInstanceGroup({ id: 1, instances: ['test'], role: 'test1' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceGroupActions.deleteInstanceGroup({
            instanceGroup: { id: 1, instances: ['test'], role: 'test1' }
        }));
    })
})
