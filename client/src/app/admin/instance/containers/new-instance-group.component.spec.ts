/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { NewInstanceGroupComponent } from './new-instance-group.component';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';

describe('[admin][instance][Containers] NewInstanceGroupComponent', () => {
    let component: NewInstanceGroupComponent;
    let fixture: ComponentFixture<NewInstanceGroupComponent>;
    let store: MockStore;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewInstanceGroupComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(NewInstanceGroupComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('addNewInstanceGroup(instanceGroup: InstanceGroup) should dispatch addInstanceGroup action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.addNewInstanceGroup({ id: 1, instances: ['test'], role: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceGroupActions.addInstanceGroup({
            instanceGroup: { id: 1, instances: ['test'], role: 'test' }
        }));
    })
})
