/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { InstanceGroup, Instance } from 'src/app/metamodel/models';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-new-instance-group',
    templateUrl: 'new-instance-group.component.html'
})
export class NewInstanceGroupComponent {
    public instanceListIsLoading: Observable<boolean>;
    public instanceListIsLoaded: Observable<boolean>;
    public instanceList: Observable<Instance[]>;

    constructor(private store: Store<{ }>) {
        this.instanceListIsLoading = store.select(instanceSelector.selectInstanceListIsLoading);
        this.instanceListIsLoaded = store.select(instanceSelector.selectInstanceListIsLoaded);
        this.instanceList = store.select(instanceSelector.selectAllInstances);
    }

    addNewInstanceGroup(instanceGroup: InstanceGroup) {
        this.store.dispatch(instanceGroupActions.addInstanceGroup({ instanceGroup }));
    }
}
