/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance } from 'src/app/metamodel/models';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-instance-list',
    templateUrl: 'instance-list.component.html',
    styleUrls: [ 'instance-list.component.scss' ]
})
export class InstanceListComponent {
    public instanceList: Observable<Instance[]>;

    constructor(private store: Store<{ }>) {
        this.instanceList = store.select(instanceSelector.selectAllInstances);
    }

    deleteInstance(instance: Instance) {
        this.store.dispatch(instanceActions.deleteInstance({ instance }));
    }
}
