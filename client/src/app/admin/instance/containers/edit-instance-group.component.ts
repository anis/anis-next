/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { InstanceGroup, Instance } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';

@Component({
    selector: 'app-edit-instance-group',
    templateUrl: 'edit-instance-group.component.html'
})
export class EditInstanceGroupComponent {
    public instanceGroup: Observable<InstanceGroup>;
    public instanceListIsLoading: Observable<boolean>;
    public instanceListIsLoaded: Observable<boolean>;
    public instanceList: Observable<Instance[]>;

    constructor(private store: Store<{ }>) {
        this.instanceGroup = store.select(instanceGroupSelector.selectInstanceGroupByRouteId);
        this.instanceListIsLoading = store.select(instanceSelector.selectInstanceListIsLoading);
        this.instanceListIsLoaded = store.select(instanceSelector.selectInstanceListIsLoaded);
        this.instanceList = store.select(instanceSelector.selectAllInstances);
    }

    editInstanceGroup(instanceGroup: InstanceGroup) {
        this.store.dispatch(instanceGroupActions.editInstanceGroup({ instanceGroup }));
    }
}
