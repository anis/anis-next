/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EditInstanceGroupComponent } from './edit-instance-group.component';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';
import { InstanceGroup } from 'src/app/metamodel/models';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';

describe('[admin][instance][Containers] EditInstanceGroupComponent', () => {
    let component: EditInstanceGroupComponent;
    let fixture: ComponentFixture<EditInstanceGroupComponent>;
    let store: MockStore;
    let mockInstanceGroupSelectorInstanceGroup;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditInstanceGroupComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(EditInstanceGroupComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        mockInstanceGroupSelectorInstanceGroup = store.overrideSelector(instanceGroupSelector.selectInstanceGroupByRouteId,
            { id: 1, instances: [], role: 'test' });
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('editInstanceGroup(instanceGroup: InstanceGroup) should dispatch editInstanceGroup action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let instanceGroup: InstanceGroup = { id: 1, instances: ['test1'], role: 'test' };
        component.editInstanceGroup(instanceGroup);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceGroupActions.editInstanceGroup({ instanceGroup }));
    });
})
