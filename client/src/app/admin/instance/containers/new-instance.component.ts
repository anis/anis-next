/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs';

import { Instance } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as adminFileExplorerSelector from 'src/app/admin/store/selectors/admin-file-explorer.selector';

@Component({
    selector: 'app-new-instance',
    templateUrl: 'new-instance.component.html'
})
export class NewInstanceComponent {
    public files: Observable<FileInfo[]>;
    public filesIsLoading: Observable<boolean>;
    public filesIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.files = store.select(adminFileExplorerSelector.selectFiles);
        this.filesIsLoading = store.select(adminFileExplorerSelector.selectFilesIsLoading);
        this.filesIsLoaded = store.select(adminFileExplorerSelector.selectFilesIsLoaded);
    }

    addNewInstance(instance: Instance) {
        this.store.dispatch(instanceActions.addInstance({ instance }));
    }

    loadRootDirectory(path: string) {
        this.store.dispatch(adminFileExplorerActions.loadFiles({ path }));
    }
}
