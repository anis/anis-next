/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EditInstanceComponent } from './edit-instance.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Instance } from 'src/app/metamodel/models';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import { Component } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';

@Component({
    selector: 'app-instance-form',
})
export class InstanceFormComponent {
    public form = new UntypedFormGroup({})
}
describe('[admin][instance][Containers] EditInstanceComponent', () => {
    let component: EditInstanceComponent;
    let fixture: ComponentFixture<EditInstanceComponent>;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let instance: Instance;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditInstanceComponent,
                InstanceFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(EditInstanceComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, name: 'test' });
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('editInstance(instance: Instance)  should dispatch editInstance action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.editInstance({ ...instance, name: 'test1' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.editInstance({ instance: { ...instance, name: 'test1' } }));
    });
    it(' loadRootDirectory(path: string) should dispatch loadFiles action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadRootDirectory('test');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path: 'test' }));
    });
})
