/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as datasetFamilyActions from 'src/app/metamodel/actions/dataset-family.actions';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';
import * as webpageFamilyActions from 'src/app/metamodel/actions/webpage-family.actions';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';

@Component({
    selector: 'app-configure-instance',
    templateUrl: 'configure-instance.component.html',
})
export class ConfigureInstanceComponent implements OnInit {
    constructor(private store: Store<{ }>) { }

    ngOnInit() {
        Promise.resolve(null).then(() => this.store.dispatch(datasetFamilyActions.loadDatasetFamilyList()));
        Promise.resolve(null).then(() => this.store.dispatch(datasetActions.loadDatasetList()));
        Promise.resolve(null).then(() => this.store.dispatch(datasetGroupActions.loadDatasetGroupList()));
        Promise.resolve(null).then(() => this.store.dispatch(webpageFamilyActions.loadWebpageFamilyList()));
        Promise.resolve(null).then(() => this.store.dispatch(webpageActions.loadWebpageList()));
    }
}
