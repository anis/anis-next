/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { InstanceGroup } from 'src/app/metamodel/models';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';

@Component({
    selector: 'app-instance-group-list',
    templateUrl: 'instance-group-list.component.html'
})
export class InstanceGroupListComponent {
    public instanceGroupList: Observable<InstanceGroup[]>;

    constructor(private store: Store<{ }>) {
        this.instanceGroupList = store.select(instanceGroupSelector.selectAllInstanceGroups);
    }

    deleteInstanceGroup(instanceGroup: InstanceGroup) {
        this.store.dispatch(instanceGroupActions.deleteInstanceGroup({ instanceGroup }));
    }
}
