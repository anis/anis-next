/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Instance } from 'src/app/metamodel/models';
import { NewInstanceComponent } from './new-instance.component';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import { Component } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
    selector: 'app-instance-form',
})
export class InstanceFormComponent {
    public form = new UntypedFormGroup({})
}
describe('[admin][instance][Containers] NewInstanceComponent', () => {
    let component: NewInstanceComponent;
    let fixture: ComponentFixture<NewInstanceComponent>;
    let store: MockStore;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewInstanceComponent,
                InstanceFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })
        fixture = TestBed.createComponent(NewInstanceComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('addNewInstance(instance: Instance) should dispatch addInstance action', () => {
        let instance: Instance;
        let spy = jest.spyOn(store, 'dispatch');
        component.addNewInstance({ ...instance, name: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.addInstance({
            instance: { ...instance, name: 'test' }
        }));
    });
    it('loadRootDirectory(path: string)should dispatch loadFiles action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadRootDirectory('test');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path: 'test' }));
    });
})
