/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatasetListComponent } from './containers/dataset-list.component';
import { NewDatasetComponent } from './containers/new-dataset.component';
import { EditDatasetComponent } from './containers/edit-dataset.component';
import { ConfigureDatasetComponent } from './containers/configure-dataset.component';
import { DatasetTitleResolver } from './dataset-title.resolver';
import { ConfigureDatasetTitleResolver } from './configure-dataset-title.resolver';

const routes: Routes = [
    { path: 'dataset-list', component: DatasetListComponent, title: DatasetTitleResolver },
    { path: 'new-dataset', component: NewDatasetComponent, title: DatasetTitleResolver },
    { path: 'edit-dataset/:dname', component: EditDatasetComponent, title: ConfigureDatasetTitleResolver },
    { path: 'configure-dataset/:dname', component: ConfigureDatasetComponent, title: ConfigureDatasetTitleResolver }
];

/**
 * @class
 * @classdesc Dataset routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DatasetRoutingModule { }

export const routedComponents = [
    DatasetListComponent,
    NewDatasetComponent,
    EditDatasetComponent,
    ConfigureDatasetComponent
];
