/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Dataset, Image, Instance } from 'src/app/metamodel/models';
import { FileInfo, FitsImageLimits } from 'src/app/admin/store/models';

@Component({
    selector: 'app-image-list',
    templateUrl: 'image-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageListComponent {
    @Input() imageList: Image[];
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Input() fitsImageLimits: FitsImageLimits;
    @Input() fitsImageLimitsIsLoading: boolean;
    @Input() fitsImageLimitsIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() retrieveFitsImageLimits: EventEmitter<string> = new EventEmitter();
    @Output() add: EventEmitter<Image> = new EventEmitter();
    @Output() edit: EventEmitter<Image> = new EventEmitter();
    @Output() delete: EventEmitter<Image> = new EventEmitter();
}
