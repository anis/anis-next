/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FileInfo, FitsImageLimits } from 'src/app/admin/store/models';
import { Dataset, Instance } from 'src/app/metamodel/models';
import { ImageFormComponent } from './image-form.component';

describe('[admin][instance][dataset][components][image] ImageFormComponent', () => {
    let component: ImageFormComponent;
    let fixture: ComponentFixture<ImageFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ImageFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(ImageFormComponent);
        component = fixture.componentInstance;
        component.image = {
            dec_max: 10,
            dec_min: 1,
            file_path: 'test_path',
            file_size: 1000,
            id: 1,
            label: 'test',
            pmax: 1,
            pmin: 0,
            ra_max: 3,
            ra_min: 1,
            stretch: 'test'
        };
        let instance: Instance;
        let dataset: Dataset;
        component.instance = { ...instance, data_path: 'test3' };
        component.dataset = { ...dataset, data_path: 'test_dataset' }
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('onChangeFileSelect(path: string) should emit instance.data_path, dataset.data_path and path', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        let path: string = 'test1';
        component.onChangeFileSelect(path);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(component.instance.data_path + component.dataset.data_path + path);
    });

    it('onFileSelect(fileInfo: FileInfo) should set form.file_size to 2000 and emit file_path value', () => {
        let spy = jest.spyOn(component.retrieveFitsImageLimits, 'emit');
        expect(component.form.controls.file_size.value).toEqual(1000);
        let fileInfo: FileInfo = { mimetype: 'test', name: 'test', size: 2000, type: 'test' };
        component.onFileSelect(fileInfo)
        expect(component.form.controls.file_size.value).toEqual(2000);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test_path');
    });

    it('submit() should emit file and form.getRawValue()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.image, ...component.form.getRawValue() });
    });

    it('submit() should emit only form.getRawValue()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.image = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    });

    it('should call ngOnChanges', () => {
        let spy = jest.spyOn(component, 'ngOnChanges');
        let fitsImageLimits: FitsImageLimits = { dec_max: 10, dec_min: 5, ra_max: 2, ra_min: 1 };
        component.ngOnChanges(
            {
                fitsImageLimits: new SimpleChange(undefined, fitsImageLimits, false),
            }
        )
        fixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
    })
});
