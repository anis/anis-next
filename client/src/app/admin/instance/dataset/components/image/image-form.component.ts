/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, OnInit, OnChanges, EventEmitter, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { Image, Dataset, Instance } from 'src/app/metamodel/models';
import { FileInfo, FitsImageLimits } from 'src/app/admin/store/models';

@Component({
    selector: 'app-image-form',
    templateUrl: 'image-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageFormComponent implements OnInit, OnChanges {
    @Input() image: Image;
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Input() fitsImageLimits: FitsImageLimits;
    @Input() fitsImageLimitsIsLoading: boolean;
    @Input() fitsImageLimitsIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() retrieveFitsImageLimits: EventEmitter<string> = new EventEmitter();
    @Output() onSubmit: EventEmitter<Image> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        file_path: new UntypedFormControl('', [Validators.required]),
        file_size: new UntypedFormControl('', [Validators.required]),
        ra_min: new UntypedFormControl('', [Validators.required]),
        ra_max: new UntypedFormControl('', [Validators.required]),
        dec_min: new UntypedFormControl('', [Validators.required]),
        dec_max: new UntypedFormControl('', [Validators.required]),
        stretch: new UntypedFormControl('linear', [Validators.required]),
        pmin: new UntypedFormControl(0.25, [Validators.required]),
        pmax: new UntypedFormControl(99.75, [Validators.required])
    });

    ngOnInit() {
        if (this.image) {
            this.form.patchValue(this.image);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.fitsImageLimits && changes.fitsImageLimits.currentValue) {
            this.form.patchValue(this.fitsImageLimits);
        }
    }

    onChangeFileSelect(path: string) {
        this.loadRootDirectory.emit(`${this.instance.data_path}${this.dataset.data_path}${path}`);
    }

    onFileSelect(fileInfo: FileInfo) {
        this.form.controls.file_size.setValue(fileInfo.size);
        this.retrieveFitsImageLimits.emit(this.form.controls.file_path.value);
    }

    submit() {
        if (this.image) {
            this.onSubmit.emit({
                ...this.image,
                ...this.form.getRawValue()
            });
        } else {
            this.onSubmit.emit(this.form.getRawValue());
        }
    }
}
