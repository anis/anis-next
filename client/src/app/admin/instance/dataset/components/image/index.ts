/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ImageListComponent } from './image-list.component';
import { ImageFormComponent } from './image-form.component';
import { AddImageComponent } from './add-image.component';
import { EditImageComponent } from './edit-image.component';

export const imageComponents = [
    ImageListComponent,
    ImageFormComponent,
    AddImageComponent,
    EditImageComponent
];
