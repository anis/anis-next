/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';

import { AliasConfig, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-alias-config',
    templateUrl: 'alias-config.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AliasConfigComponent implements OnInit {
    @Input() dataset: Dataset;
    @Input() aliasConfig: AliasConfig;
    @Input() tableList: string[];
    @Input() tableListIsLoading: boolean;
    @Input() tableListIsLoaded: boolean;    
    @Output() loadTableList: EventEmitter<number> = new EventEmitter();
    @Output() addAliasConfig: EventEmitter<AliasConfig> = new EventEmitter();
    @Output() editAliasConfig: EventEmitter<AliasConfig> = new EventEmitter();

    ngOnInit() {
        Promise.resolve(null).then(() => this.loadTableList.emit(this.dataset.id_database));
    }

    save(aliasConfig: AliasConfig) {
        if (this.aliasConfig) {
            this.editAliasConfig.emit({
                ...this.aliasConfig,
                ...aliasConfig
            });
        } else {
            this.addAliasConfig.emit(aliasConfig);
        }
    }
}
