/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { UntypedFormGroup } from "@angular/forms";
import { AliasConfig } from 'src/app/metamodel/models';
import { AliasConfigComponent } from "./alias-config.component";

describe('[admin][instance][dataset][components] AliasConfigComponent', () => {
    @Component({
        selector: 'app-alias-config-form',
    })
    class AliasConfigFormComponent {
        public form = new UntypedFormGroup({});
    }
    let component: AliasConfigComponent;
    let fixture: ComponentFixture<AliasConfigComponent>;
    let aliasConfig: AliasConfig;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AliasConfigComponent,
                AliasConfigFormComponent
            ]
        })
        fixture = TestBed.createComponent(AliasConfigComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('save(aliasConfig: AliasConfig) should emit editAliasConfig', () => {
        let spy = jest.spyOn(component.editAliasConfig, 'emit');
        component.aliasConfig = { ...aliasConfig, column_alias: 'test' };
        component.save({ ...aliasConfig, column_alias: 'test2' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...aliasConfig, column_alias: 'test2' });
    });
    it('save(aliasConfig: AliasConfig) should emit addAliasConfig', () => {
        let spy = jest.spyOn(component.addAliasConfig, 'emit');
        component.aliasConfig = null;
        component.save({ ...aliasConfig, column_alias: 'test2' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...aliasConfig, column_alias: 'test2' });
    });

});