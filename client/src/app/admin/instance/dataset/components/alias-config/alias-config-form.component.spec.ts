/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AliasConfig } from "src/app/metamodel/models";
import { AliasConfigFormComponent } from "./alias-config-form.component";

describe('[admin][instance][dataset][components] AliasConfigFormComponent', () => {
    let component: AliasConfigFormComponent;
    let fixture: ComponentFixture<AliasConfigFormComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AliasConfigFormComponent]
        })
        fixture = TestBed.createComponent(AliasConfigFormComponent);
        component = fixture.componentInstance;
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('ngOnInit() should call form.patchValue', () => {
        let spy = jest.spyOn(component.form, 'patchValue');
        let aliasConfig: AliasConfig = {
            table_alias: "",
            column_alias: "",
            column_name: "",
            column_alias_long: ""
        };
        component.aliasConfig = aliasConfig;
        component.ngOnInit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(aliasConfig);
    });
    it('submit() should raises onSubmit event and call form.markAsPristine', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        let spyOnForm = jest.spyOn(component.form, 'markAsPristine');
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnForm).toHaveBeenCalledTimes(1);

    });
});