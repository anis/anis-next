/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, OnInit, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { File, Dataset, Instance } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';

@Component({
    selector: 'app-file-form',
    templateUrl: 'file-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileFormComponent implements OnInit {
    @Input() file: File;
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() onSubmit: EventEmitter<File> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        file_path: new UntypedFormControl('', [Validators.required]),
        file_size: new UntypedFormControl('', [Validators.required]),
        type: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.file) {
            this.form.patchValue(this.file);
        }
    }

    onChangeFileSelect(path: string) {
        this.loadRootDirectory.emit(`${this.instance.data_path}${this.dataset.data_path}${path}`);
    }

    onFileSelect(fileInfo: FileInfo) {
        this.form.controls.file_size.setValue(fileInfo.size);
    }

    submit() {
        if (this.file) {
            this.onSubmit.emit({
                ...this.file,
                ...this.form.getRawValue()
            });
        } else {
            this.onSubmit.emit(this.form.getRawValue());
        }
    }
}
