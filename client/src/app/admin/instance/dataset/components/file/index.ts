/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FileListComponent } from './file-list.component';
import { FileFormComponent } from './file-form.component';
import { AddFileComponent } from './add-file.component';
import { EditFileComponent } from './edit-file.component';

export const fileComponents = [
    FileListComponent,
    FileFormComponent,
    AddFileComponent,
    EditFileComponent
];
