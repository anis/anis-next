/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, TemplateRef, Input, Output, EventEmitter } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Dataset, File, Instance } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';

@Component({
    selector: 'app-add-file',
    templateUrl: 'add-file.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddFileComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() add: EventEmitter<File> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
