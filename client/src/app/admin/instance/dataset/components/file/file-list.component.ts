/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Dataset, File, Instance } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';

@Component({
    selector: 'app-file-list',
    templateUrl: 'file-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileListComponent {
    @Input() fileList: File[];
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() add: EventEmitter<File> = new EventEmitter();
    @Output() edit: EventEmitter<File> = new EventEmitter();
    @Output() delete: EventEmitter<File> = new EventEmitter();
}
