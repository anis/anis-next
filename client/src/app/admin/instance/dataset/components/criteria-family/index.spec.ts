/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { criteriaFamilyComponents } from './index';

describe('[admin][instance][dataset][components][criteria-family] index', () => {
    it('should test criteria-family index components', () => {
        expect(criteriaFamilyComponents.length).toEqual(4);
    });
});
