/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CriteriaFamilyListComponent } from './criteria-family-list.component';

@Component({
    selector: 'app-add-criteria-family',
    templateUrl: 'add-criteria-family.component.html',
})
class AddCriteriaFamilyComponent { }

describe('[admin][instance][dataset][components][criteria-family] CriteriaFamilyListComponent', () => {
    let component: CriteriaFamilyListComponent;
    let fixture: ComponentFixture<CriteriaFamilyListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaFamilyListComponent,
                AddCriteriaFamilyComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(CriteriaFamilyListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
