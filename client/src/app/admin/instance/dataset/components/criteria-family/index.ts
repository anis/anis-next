/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { CriteriaFamilyListComponent } from './criteria-family-list.component';
import { CriteriaFamilyFormComponent } from './criteria-family-form.component';
import { AddCriteriaFamilyComponent } from './add-criteria-family.component';
import { EditCriteriaFamilyComponent } from './edit-criteria-family.component';

export const criteriaFamilyComponents = [
    CriteriaFamilyListComponent,
    CriteriaFamilyFormComponent,
    AddCriteriaFamilyComponent,
    EditCriteriaFamilyComponent
];
