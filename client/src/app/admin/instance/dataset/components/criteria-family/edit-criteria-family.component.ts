/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, TemplateRef, Output, EventEmitter } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CriteriaFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-edit-criteria-family',
    templateUrl: 'edit-criteria-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCriteriaFamilyComponent {
    @Input() criteriaFamily: CriteriaFamily;
    @Output() edit: EventEmitter<CriteriaFamily> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
