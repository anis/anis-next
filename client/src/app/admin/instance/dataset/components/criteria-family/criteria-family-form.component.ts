/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { CriteriaFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-criteria-family-form',
    templateUrl: 'criteria-family-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriteriaFamilyFormComponent {
    @Input() criteriaFamily: CriteriaFamily;
    @Output() onSubmit: EventEmitter<CriteriaFamily> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
        opened: new UntypedFormControl(true)
    });

    ngOnInit() {
        if (this.criteriaFamily) {
            this.form.patchValue(this.criteriaFamily);
        }
    }

    submit() {
        if (this.criteriaFamily) {
            this.onSubmit.emit({
                ...this.criteriaFamily,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
