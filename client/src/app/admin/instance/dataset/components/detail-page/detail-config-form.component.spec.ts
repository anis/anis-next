/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DetailConfigFormComponent } from './detail-config-form.component';

@Component({
    selector: 'app-webpage-form-content'
})
class WebpageFormContentComponent {
    form: UntypedFormGroup;
}

describe('[admin][instance][dataset][components][detail-page] DetailConfigFormComponent', () => {
    let component: DetailConfigFormComponent;
    let fixture: ComponentFixture<DetailConfigFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailConfigFormComponent,
                WebpageFormContentComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(DetailConfigFormComponent);
        component = fixture.componentInstance;
        component.detailConfig = { content: '', id: 1, style_sheet: 'test' };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('submit() should emit databaseFamily and form.value', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        let spyOnForm = jest.spyOn(component.form, 'markAsPristine');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spyOnForm).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    });
});
