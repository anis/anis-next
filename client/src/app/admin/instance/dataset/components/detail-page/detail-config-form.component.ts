/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';

import { DetailConfig } from 'src/app/metamodel/models';

@Component({
    selector: 'app-detail-config-form',
    templateUrl: 'detail-config-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailConfigFormComponent implements OnInit {
    @Input() detailConfig: DetailConfig;
    @Output() onSubmit: EventEmitter<DetailConfig> = new EventEmitter();

    public form = new UntypedFormGroup({
        content: new UntypedFormControl(),
        style_sheet: new UntypedFormControl(null)
    });

    ngOnInit() {
        this.form.patchValue(this.detailConfig);
    }

    submit() {
        this.onSubmit.emit({
            ...this.form.getRawValue()
        });
        this.form.markAsPristine();
    }
}
