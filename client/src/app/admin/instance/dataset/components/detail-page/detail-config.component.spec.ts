/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { DetailConfig } from 'src/app/metamodel/models';
import { DetailConfigComponent } from './detail-config.component';

@Component({
    selector: 'app-detail-config-form',
    templateUrl: 'detail-config-form.component.html',
})
class DetailConfigFormComponent  {
    form = new UntypedFormGroup({
        content: new UntypedFormControl(),
        style_sheet: new UntypedFormControl(null)
    });
}

describe('[admin][instance][dataset][components][detail-page] DetailConfigComponent', () => {
    let component: DetailConfigComponent;
    let fixture: ComponentFixture<DetailConfigComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailConfigComponent,
                DetailConfigFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(DetailConfigComponent);
        component = fixture.componentInstance;
        component.detailConfig = { content: '', id: 1, style_sheet: 'test' };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('save(detailConfig) should emit component.detailConfig and detaiConfig', () => {
        let spy = jest.spyOn(component.editDetailConfig, 'emit');
        let detailConfig: DetailConfig = { content: '', id: 1, style_sheet: 'test2' };
        component.save(detailConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.detailConfig, ...detailConfig });
    });

    it('save(detailConfig) should emit  detaiConfig only', () => {
        let spy = jest.spyOn(component.addDetailConfig, 'emit');
        let detailConfig: DetailConfig = { content: '', id: 1, style_sheet: 'test2' };
        component.detailConfig = null;
        component.save(detailConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...detailConfig });
    });
});
