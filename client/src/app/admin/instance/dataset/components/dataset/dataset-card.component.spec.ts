/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { Dataset, Instance } from 'src/app/metamodel/models';
import { DatasetCardComponent } from './dataset-card.component';

@Component({
    selector: 'app-delete-btn',
})
export class DeleteBtnComponent { }

describe('[admin][instance][dataset][components][dataset] DatasetCardComponent', () => {
    let component: DatasetCardComponent;
    let fixture: ComponentFixture<DatasetCardComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetCardComponent,
                DeleteBtnComponent
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule.withRoutes([
                    { path: 'test', component: DeleteBtnComponent }
                   ])
            ],
        });
        fixture = TestBed.createComponent(DatasetCardComponent);
        component = fixture.componentInstance;
        let dataset: Dataset;
        let instance: Instance;
        component.dataset = { ...dataset, label: 'test' };
        component.instance = { ...instance, name: 'test' }
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
