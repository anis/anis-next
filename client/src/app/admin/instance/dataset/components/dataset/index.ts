/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetCardComponent } from './dataset-card.component';
import { DatasetFormComponent } from './dataset-form.component';

export const datasetComponents = [
    DatasetCardComponent,
    DatasetFormComponent
];
