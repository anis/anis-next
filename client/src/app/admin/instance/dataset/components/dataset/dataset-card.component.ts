/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { Instance, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-card',
    templateUrl: 'dataset-card.component.html',
    styleUrls: [ 'dataset-card.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetCardComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Output() deleteDataset: EventEmitter<Dataset> = new EventEmitter();
}
