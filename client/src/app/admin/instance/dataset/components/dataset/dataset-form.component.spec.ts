/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DatasetFormComponent } from './dataset-form.component';
import { Dataset, Instance } from 'src/app/metamodel/models';

describe('[admin][instance][dataset][components][dataset] DatasetFormComponent', () => {
    let component: DatasetFormComponent;
    let fixture: ComponentFixture<DatasetFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                AccordionModule
            ],
        });
        fixture = TestBed.createComponent(DatasetFormComponent);
        component = fixture.componentInstance;
        let dataset: Dataset;
        let instance: Instance;
        component.instance = { ...instance, data_path: 'test' };
        component.dataset = { ...dataset, label: 'test' };
        component.idDatasetFamily = 1;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('submit() should emit dataset and form.getRawValue()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.dataset, ...component.form.getRawValue() });
    });

    it('submit() should emit form.getRawValue() only', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.dataset = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    });

    it('onChangeDatabase() should call disable on form table_ref raw', () => {
        let spy = jest.spyOn(component.form.controls.table_ref, 'disable');
        component.onChangeDatabase();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('onChangeDatabase() should emit idDatabase', () => {
        let spy = jest.spyOn(component.changeDatabase, 'emit');
        component.form.controls.id_database.setValue(1);
        component.onChangeDatabase();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(1);
    });

    it('onChangeDataPath(path: string) should emit the instance data_path property', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        let path: string = 'test';
        component.onChangeDataPath(path);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(`${component.instance.data_path}${path}`);
    });

    it('checkDatatableDisablOpened() should call enable on  form datatable_selectable_rows', () => {
        let spy = jest.spyOn(component.form.controls.datatable_selectable_rows, 'enable');
        component.checkDatatableDisablOpened();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('checkDatatableDisablOpened() should call desable on  form datatable_selectable_rows and set it value to false', () => {
        let spy = jest.spyOn(component.form.controls.datatable_selectable_rows, 'disable');
        component.form.controls.datatable_enabled.setValue(false);
        component.checkDatatableDisablOpened();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(component.form.controls.datatable_selectable_rows.value).toEqual(false);
    });

    it('should call ngOnChanges and enable on table_ref ', () => {
        let spy = jest.spyOn(component, 'ngOnChanges');
        let spyOnTable_ref = jest.spyOn(component.form.controls.table_ref, 'enable');
        let tableListIsLoaded: boolean = true;
        component.ngOnChanges(
            {
                tableListIsLoaded: new SimpleChange(undefined, tableListIsLoaded, false),
            }
        )
        fixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spyOnTable_ref).toHaveBeenCalledTimes(1);
    });

    it('should call ngOnChanges and desable on table_ref ', () => {
        let spy = jest.spyOn(component, 'ngOnChanges');
        let spyOnTable_ref = jest.spyOn(component.form.controls.table_ref, 'disable');
        let tableListIsLoaded: boolean = false;
        component.ngOnChanges(
            {
                tableListIsLoaded: new SimpleChange(undefined, tableListIsLoaded, false),
            }
        )
        fixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spyOnTable_ref).toHaveBeenCalledTimes(1);
    });
});
