/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { Instance, Dataset, DatasetFamily, Database } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';

@Component({
    selector: 'app-dataset-form',
    templateUrl: 'dataset-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetFormComponent implements OnInit, OnChanges {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() databaseList: Database[];
    @Input() tableListIsLoading: boolean;
    @Input() tableListIsLoaded: boolean;
    @Input() tableList: string[];
    @Input() datasetFamilyList: DatasetFamily[];
    @Input() idDatasetFamily: number;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Output() changeDatabase: EventEmitter<number> = new EventEmitter();
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() onSubmit: EventEmitter<Dataset> = new EventEmitter();

    public isNewDataset = true;

    public form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required]),
        table_ref: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        description: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
        data_path: new UntypedFormControl(''),
        public: new UntypedFormControl('', [Validators.required]),
        id_database: new UntypedFormControl('', [Validators.required]),
        id_dataset_family: new UntypedFormControl('', [Validators.required]),
        download_json: new UntypedFormControl(true),
        download_csv: new UntypedFormControl(true),
        download_ascii: new UntypedFormControl(true),
        download_vo: new UntypedFormControl(false),
        download_fits: new UntypedFormControl(false),
        server_link_enabled: new UntypedFormControl(false),
        datatable_enabled: new UntypedFormControl(true),
        datatable_selectable_rows: new UntypedFormControl(false)
    });

    ngOnInit() {
        if (this.dataset) {
            this.isNewDataset = false;
            this.form.patchValue(this.dataset);
        }
        this.form.controls.table_ref.disable();
        if (this.idDatasetFamily) {
            this.form.controls.id_dataset_family.setValue(this.idDatasetFamily);
        }

        this.checkDatatableDisablOpened();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.tableListIsLoaded && changes.tableListIsLoaded.currentValue) {
            this.form.controls.table_ref.enable();
        } else {
            this.form.controls.table_ref.disable();
        }
    }

    submit() {
        if (this.dataset) {
            this.onSubmit.emit({
                ...this.dataset,
                ...this.form.getRawValue()
            });
        } else {
            this.onSubmit.emit(this.form.getRawValue());
        }
    }

    onChangeDatabase() {
        const idDatabase = this.form.controls.id_database.value;
        if (!idDatabase) {
            this.form.controls.table_ref.disable();
        } else {
            this.changeDatabase.emit(idDatabase);
        }
    }

    onChangeDataPath(path: string) {
        this.loadRootDirectory.emit(`${this.instance.data_path}${path}`);
    }

    checkDatatableDisablOpened() {
        if (this.form.controls.datatable_enabled.value) {
            this.form.controls.datatable_selectable_rows.enable();
        } else {
            this.form.controls.datatable_selectable_rows.setValue(false);
            this.form.controls.datatable_selectable_rows.disable();
        }
    }
}
