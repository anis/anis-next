/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { datasetComponents } from './index';

describe('[admin][instance][dataset][components][dataset] index', () => {
    it('should test dataset index components', () => {
        expect(datasetComponents.length).toEqual(2);
    });
});
