/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Attribute, ConeSearchConfig, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-cone-search-config',
    templateUrl: 'cone-search-config.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchConfigComponent {
    @Input() coneSearchConfig: ConeSearchConfig;
    @Input() attributeList: Attribute[];
    @Output() addConeSearchConfig: EventEmitter<ConeSearchConfig> = new EventEmitter();
    @Output() editConeSearchConfig: EventEmitter<ConeSearchConfig> = new EventEmitter();

    save(coneSearchConfig: ConeSearchConfig) {
        if (this.coneSearchConfig) {
            this.editConeSearchConfig.emit({
                ...this.coneSearchConfig,
                ...coneSearchConfig
            });
        } else {
            this.addConeSearchConfig.emit(coneSearchConfig);
        }
    }
}
