/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ConeSearchConfigComponent } from "./cone-search-config.component";
import { ConeSearchConfigFormComponent } from "./cone-search-config-form.component";

export const coneSearchConfigComponents = [
    ConeSearchConfigComponent,
    ConeSearchConfigFormComponent
];
