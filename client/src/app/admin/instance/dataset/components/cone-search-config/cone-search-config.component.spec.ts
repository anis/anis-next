/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConeSearchConfig } from 'src/app/metamodel/models';
import { ConeSearchConfigComponent } from './cone-search-config.component';

@Component({
    selector: 'app-cone-search-config-form',

})
class ConeSearchConfigFormComponent {
    form = new UntypedFormGroup({});
    attributeList = []
    coneSearchConfig: ConeSearchConfig;
}

describe('[admin][instance][dataset][components][cone-search-config] ConeSearchConfigComponent', () => {
    let component: ConeSearchConfigComponent;
    let fixture: ComponentFixture<ConeSearchConfigComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchConfigComponent,
                ConeSearchConfigFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(ConeSearchConfigComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('save(coneSearchConfig: ConeSearchConfig) should emit the current coneSearchConfig, coneSearchConfig in args', () => {
        let spy = jest.spyOn(component.editConeSearchConfig, 'emit');
        let coneSearchConfig: ConeSearchConfig = {
            id: 1,
            enabled: false,
            opened: false,
            column_ra: 5,
            column_dec: 10,
            resolver_enabled: false,
            default_ra: null,
            default_dec: null,
            default_radius: null,
            default_ra_dec_unit: 'degrees',
            plot_enabled: false
        };
        component.coneSearchConfig = coneSearchConfig;
        let param: ConeSearchConfig = { ...coneSearchConfig, id: 2 }
        component.save(param);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.coneSearchConfig, ...param })
    });

    it('save(coneSearchConfig: ConeSearchConfig) should emit the coneSearchConfig passed in args', () => {
        let spy = jest.spyOn(component.addConeSearchConfig, 'emit');
        let coneSearchConfig: ConeSearchConfig = {
            id: 1,
            enabled: false,
            opened: false,
            column_ra: 5,
            column_dec: 10,
            resolver_enabled: false,
            default_ra: null,
            default_dec: null,
            default_radius: null,
            default_ra_dec_unit: 'degrees',
            plot_enabled: false
        };
        component.save(coneSearchConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchConfig);
    });
});
