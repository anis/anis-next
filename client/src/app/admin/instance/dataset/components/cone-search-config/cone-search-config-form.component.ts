/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';

import { Attribute, ConeSearchConfig } from 'src/app/metamodel/models';

@Component({
    selector: 'app-cone-search-config-form',
    templateUrl: 'cone-search-config-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchConfigFormComponent implements OnInit {
    @Input() coneSearchConfig: ConeSearchConfig;
    @Input() attributeList: Attribute[];
    @Output() onSubmit: EventEmitter<ConeSearchConfig> = new EventEmitter();

    public form = new UntypedFormGroup({
        enabled: new UntypedFormControl(false),
        opened: new UntypedFormControl({value: false, disabled: true}),
        column_ra: new UntypedFormControl({value: false, disabled: true}),
        column_dec: new UntypedFormControl({value: false, disabled: true}),
        resolver_enabled: new UntypedFormControl({value: false, disabled: true}),
        default_ra: new UntypedFormControl({value: null, disabled: true}),
        default_dec: new UntypedFormControl({value: null, disabled: true}),
        default_radius: new UntypedFormControl({value: 2.0, disabled: true}),
        default_ra_dec_unit: new UntypedFormControl({value: 'degree', disabled: true}),
        plot_enabled: new UntypedFormControl({value: false, disabled: true})
    });

    ngOnInit() {
        this.form.patchValue(this.coneSearchConfig);
        this.checkConeSearchDisableOpened();
    }

    checkConeSearchDisableOpened() {
        if (this.form.controls.enabled.value) {
            this.form.controls.opened.enable();
            this.form.controls.column_ra.enable();
            this.form.controls.column_dec.enable();
            this.form.controls.resolver_enabled.enable();
            this.form.controls.default_ra.enable();
            this.form.controls.default_dec.enable();
            this.form.controls.default_radius.enable();
            this.form.controls.default_ra_dec_unit.enable();
            this.form.controls.plot_enabled.enable();
        } else {
            this.form.controls.opened.setValue(false);
            this.form.controls.opened.disable();
            this.form.controls.column_ra.disable();
            this.form.controls.column_dec.disable();
            this.form.controls.resolver_enabled.disable();
            this.form.controls.default_ra.disable();
            this.form.controls.default_dec.disable();
            this.form.controls.default_radius.disable();
            this.form.controls.default_ra_dec_unit.disable();
            this.form.controls.plot_enabled.disable();
        }
    }

    submit() {
        this.onSubmit.emit({
            ...this.form.getRawValue()
        });
        this.form.markAsPristine();
    }
}
