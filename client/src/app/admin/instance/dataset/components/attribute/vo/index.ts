/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TableVoComponent } from './table-vo.component';
import { TrVoComponent } from './tr-vo.component';

export const voComponents = [
    TableVoComponent,
    TrVoComponent
];
