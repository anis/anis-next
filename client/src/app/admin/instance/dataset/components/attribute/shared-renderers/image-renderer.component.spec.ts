/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

 import { ComponentFixture, TestBed } from '@angular/core/testing';
 import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
 import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImageRendererComponent } from './image-renderer.component';

 describe('[admin][instance][dataset][components][attribute][result][renderers] ImageRendererComponent', () => {
     let component: ImageRendererComponent;
     let fixture: ComponentFixture<ImageRendererComponent>;
 
     beforeEach(() => {
         TestBed.configureTestingModule({
             declarations: [
                ImageRendererComponent,
             ],
             imports: [
                 BrowserAnimationsModule,
                 ReactiveFormsModule
             ],
         });
         fixture = TestBed.createComponent(ImageRendererComponent);
         component = fixture.componentInstance;
         let form = new UntypedFormGroup({
            type: new UntypedFormControl(),
             display: new UntypedFormControl(),
           
 
         });
         component.form = form;
         fixture.detectChanges();
     });
 
     it('should create the component', () => {
         expect(component).toBeTruthy();
     });
 });
 
 
 