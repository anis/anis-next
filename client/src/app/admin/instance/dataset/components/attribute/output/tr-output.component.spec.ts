/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Attribute, OutputFamily } from 'src/app/metamodel/models';
import { TrOutputComponent } from './tr-output.component';

describe('[admin][instance][dataset][components][attribute][] TrOutputComponent', () => {
    let component: TrOutputComponent;
    let fixture: ComponentFixture<TrOutputComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TrOutputComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(TrOutputComponent);
        component = fixture.componentInstance;
        let attribute: Attribute;
        component.attribute = { ...attribute, name: 'test', output_display: 0, id_output_category: 0, selected: true };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('outputCategoryOnChange() should set form.id_output_category to null', () => {
        component.form.controls.id_output_category.setValue('');
        expect(component.form.controls.id_output_category.value).toEqual('');
        component.outputCategoryOnChange();
        expect(component.form.controls.id_output_category.value).toEqual(null);
    });
    it('getOutputFamilyLabel(idOutputFamilly: number) should return test2', () => {
        let outputFamilyList: OutputFamily[] = [
            { display: 0, id: 1, label: 'test0', opened: false },
            { display: 0, id: 2, label: 'test1', opened: false },
            { display: 0, id: 3, label: 'test2', opened: false }
        ]
        component.outputFamilyList = outputFamilyList;
        let result: string = component.getOutputFamilyLabel(3);
        expect(result).toEqual('test2');
    })
    it('submit() should emit with attribute and form.value', () => {
        let spy = jest.spyOn(component.save, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.attribute, ...component.form.value });
    })
});

