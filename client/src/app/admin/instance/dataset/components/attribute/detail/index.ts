/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TableDetailComponent } from './table-detail.component';
import { TrDetailComponent } from './tr-detail.component';
import { detailRenderers } from './renderers';

export const detailComponents = [
    TableDetailComponent,
    TrDetailComponent,
    detailRenderers
];
