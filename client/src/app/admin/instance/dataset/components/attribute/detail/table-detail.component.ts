/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, ViewChildren, QueryList, Input, Output, EventEmitter } from '@angular/core';

import { Attribute, OutputCategory, OutputFamily } from 'src/app/metamodel/models';
import { TrDetailComponent } from './tr-detail.component';

@Component({
    selector: 'app-table-detail',
    templateUrl: 'table-detail.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableDetailComponent {
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: OutputFamily[];
    @Input() outputCategoryList: OutputCategory[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @ViewChildren(TrDetailComponent) private trDetailList: QueryList<TrDetailComponent>;

    saveAll() {
        this.trDetailList.forEach(trDetail => {
            if (trDetail.form.dirty && trDetail.form.valid) {
                trDetail.submit();
            }
        });
    }

    saveAllDisabled() {
        let disabled = true;
        if (this.trDetailList) {
            disabled = this.trDetailList.filter(trDetail => trDetail.form.dirty && trDetail.form.valid).length === 0;
        }
        return disabled;
    }
}
