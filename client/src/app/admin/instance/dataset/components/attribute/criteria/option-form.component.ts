/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { UntypedFormGroup } from '@angular/forms';

@Component({
    selector: 'app-option-form',
    templateUrl: 'option-form.component.html',
    styleUrls: [ 'option-form.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionFormComponent {
    @Input() form: UntypedFormGroup;
}
