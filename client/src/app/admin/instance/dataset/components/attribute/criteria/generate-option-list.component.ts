/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-generate-option-list',
    templateUrl: 'generate-option-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenerateOptionListComponent {
    @Input() optionListIsEmpty: boolean;
    @Input() attributeDistinctList: string[];
    @Input() attributeDistinctListIsLoading: boolean;
    @Input() attributeDistinctListIsLoaded: boolean;
    @Output() loadAttributeDistinctList: EventEmitter<{}> = new EventEmitter();
    @Output() generateOptionList: EventEmitter<string[]> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
        this.loadAttributeDistinctList.emit();
    }

    generate() {
        this.generateOptionList.emit(this.attributeDistinctList);
        this.modalRef.hide();
    }
}
