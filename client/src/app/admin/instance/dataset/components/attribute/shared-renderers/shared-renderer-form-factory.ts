/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { UntypedFormControl } from "@angular/forms";

export abstract class SharedRendererFormFactory {
    static create(renderer: string) {
        switch (renderer) {
            case 'link':
                return {
                    href: new UntypedFormControl('$value'),
                    display: new UntypedFormControl('text'),
                    text: new UntypedFormControl('$value'),
                    icon: new UntypedFormControl('fas fa-link'),
                    blank: new UntypedFormControl(true)
                };
            case 'download':
                return {
                    display: new UntypedFormControl('icon-button'),
                    text: new UntypedFormControl('DOWNLOAD'),
                    icon: new UntypedFormControl('fas fa-download')
                };
            case 'image':
                return {
                    type: new UntypedFormControl('fits'),
                    display: new UntypedFormControl('modal'),
                    width: new UntypedFormControl(''),
                    height: new UntypedFormControl('')
                };
            default:
                return {};
        }
    }
}
