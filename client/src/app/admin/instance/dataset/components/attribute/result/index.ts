/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TableResultComponent } from './table-result.component';
import { TrResultComponent } from './tr-result.component';
import { renderers } from './renderers';

export const resultComponents = [
    TableResultComponent,
    TrResultComponent,
    renderers
];
