/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { criteriaComponents } from './index';

describe('[admin][instance][dataset][components][attribute][criteria] index', () => {
    it('Test criteria index components', () => {
        expect(criteriaComponents.length).toEqual(5);
    });
});
