/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, ViewChildren, QueryList, Input, Output, EventEmitter } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';
import { TrResultComponent } from './tr-result.component';

@Component({
    selector: 'app-table-result',
    templateUrl: 'table-result.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableResultComponent {
    @Input() attributeList: Attribute[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @ViewChildren(TrResultComponent) private trResultList: QueryList<TrResultComponent>;

    saveAll() {
        this.trResultList.forEach(trResult => {
            if (trResult.form.dirty && trResult.form.valid) {
                trResult.submit();
            }
        });
    }

    saveAllDisabled() {
        let disabled = true;
        if (this.trResultList) {
            disabled = this.trResultList.filter(trResult => trResult.form.dirty && trResult.form.valid).length === 0;
        }
        return disabled;
    }
}
