/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { GenerateOptionListComponent } from './generate-option-list.component';

describe('[admin][instance][dataset][components][attribute][criteria] GenerateOptionListComponent', () => {
    let component: GenerateOptionListComponent;
    let fixture: ComponentFixture<GenerateOptionListComponent>;

    const modalServiceStub = {
        show: jest.fn().mockReturnThis(),
        hide: jest.fn()
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                GenerateOptionListComponent,
            ],
            providers: [
                BsModalRef,
                { provide: BsModalService, useValue: modalServiceStub }
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(GenerateOptionListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call modalRef.show(template) and  loadAttributeDistinctList.emit', () => {
        let template: TemplateRef<any> = null;
        let spyOnModal = jest.spyOn(modalServiceStub, 'show');
        let spyOnloadAttributeDistinctList = jest.spyOn(component.loadAttributeDistinctList, 'emit');
        component.openModal(template);
        expect(spyOnModal).toHaveBeenCalledTimes(1);
        expect(spyOnModal).toHaveBeenCalledWith(template);
        expect(spyOnloadAttributeDistinctList).toHaveBeenCalledTimes(1);
    });

    it('should emit attributeDistinctList and call  modalRef.hide()', () => {
        let template: TemplateRef<any> = null;
        let spyOnModal = jest.spyOn(modalServiceStub, 'hide');
        let spyOngenerateOptionList = jest.spyOn(component.generateOptionList, 'emit');
        let attributeDistinctList: string[] = ['test'];
        component.attributeDistinctList = attributeDistinctList;
        component.openModal(template);
        component.generate();
        expect(spyOngenerateOptionList).toHaveBeenCalledTimes(1);
        expect(spyOngenerateOptionList).toHaveBeenCalledWith(attributeDistinctList);
        expect(spyOnModal).toHaveBeenCalledTimes(1);
    });
});
