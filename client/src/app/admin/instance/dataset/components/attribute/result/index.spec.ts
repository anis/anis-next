/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { resultComponents } from './index';

describe('[admin][instance][dataset][components][attribute][result] index', () => {
    it('Test result index components', () => {
        expect(resultComponents.length).toEqual(3);
    });
});

