/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Attribute } from 'src/app/metamodel/models';
import { RendererFormFactory } from './renderers';
import { TrResultComponent } from './tr-result.component';


describe('[admin][instance][dataset][components][attribute][result] TrResultComponent', () => {
    let component: TrResultComponent;
    let fixture: ComponentFixture<TrResultComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TrResultComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(TrResultComponent);
        component = fixture.componentInstance;
        let attribute: Attribute;
        component.attribute = { ...attribute, name: 'test', renderer: '', order_by: false, archive: true };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('getRendererConfigForm() should form.controls["renderer_config"] as an instance of UntypedFormGroup', () => {
        let result = component.getRendererConfigForm();
        expect(result).toBeInstanceOf(UntypedFormGroup);

    })
    it(' rendererOnChange() should set form.controls.renderer to nul and call setcontrol on the form ', () => {
        let spy = jest.spyOn(component.form, 'setControl');
        expect(component.form.controls.renderer.value).toEqual('');
        component.rendererOnChange();
        expect(component.form.controls.renderer.value).toEqual(null);
        expect(spy).toHaveBeenCalledTimes(1);


    });
    it('rendererOnChange() should call RenderFormFactory.create', () => {
        component.form.controls.renderer.setValue('test');
        let spy = jest.spyOn(RendererFormFactory, 'create');
        component.rendererOnChange()
        expect(spy).toHaveBeenCalledTimes(1);

    });
    it('submit() should emit with attribute and form.value', () => {
        let spy = jest.spyOn(component.save, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.attribute, ...component.form.value });
    })


});

