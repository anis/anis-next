/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { Attribute, OutputCategory, OutputFamily } from 'src/app/metamodel/models';

@Component({
    selector: '[output]',
    templateUrl: 'tr-output.component.html',
    styleUrls: ['../tr.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrOutputComponent implements OnInit {
    @Input() attribute: Attribute;
    @Input() outputCategoryList: OutputCategory[];
    @Input() outputFamilyList: OutputFamily[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl({ value: '', disabled: true }),
        output_display: new UntypedFormControl(),
        id_output_category: new UntypedFormControl(),
        selected: new UntypedFormControl()
    });

    ngOnInit() {
        if (this.attribute) {
            this.form.patchValue(this.attribute);
        }
    }

    outputCategoryOnChange(): void {
        if (this.form.controls.id_output_category.value === '') {
            this.form.controls.id_output_category.setValue(null);
        }
    }

    getOutputFamilyLabel(idOutputFamilly: number): string {
        return this.outputFamilyList.find(outputFamilly => outputFamilly.id === idOutputFamilly).label;
    }

    submit(): void {
        this.save.emit({
            ...this.attribute,
            ...this.form.value
        });
        this.form.markAsPristine();
    }
}
