/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { Attribute, OutputCategory, OutputFamily } from 'src/app/metamodel/models';
import { DetailRendererFormFactory, detailRendererList } from './renderers';

@Component({
    selector: '[detail]',
    templateUrl: 'tr-detail.component.html',
    styleUrls: [ '../tr.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrDetailComponent implements OnInit {
    @Input() attribute: Attribute
    @Input() outputCategoryList: OutputCategory[];
    @Input() outputFamilyList: OutputFamily[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();

    detailRendererList = detailRendererList;

    public form = new UntypedFormGroup({
        name: new UntypedFormControl({ value: '', disabled: true }),
        id_detail_output_category: new UntypedFormControl(),
        detail_display: new UntypedFormControl(),
        detail_renderer: new UntypedFormControl(null)
    });

    ngOnInit() {
        if (this.attribute) {
            const detailRendererConfigForm = DetailRendererFormFactory.create(this.attribute.detail_renderer);
            this.form.addControl('detail_renderer_config', new UntypedFormGroup(detailRendererConfigForm));
            this.form.patchValue(this.attribute);
        }
    }

    getDetailRendererConfigForm() {
        return this.form.controls['detail_renderer_config'] as UntypedFormGroup;
    }

    detailRendererOnChange() {
        if (this.form.controls.detail_renderer.value === '') {
            this.form.controls.detail_renderer.setValue(null);
            this.form.setControl('detail_renderer_config', new UntypedFormGroup({}));
        } else {
            this.form.setControl('detail_renderer_config', new UntypedFormGroup(
                DetailRendererFormFactory.create(this.form.controls.detail_renderer.value)
            ));
        }
    }

    detailOutputCategoryOnChange(): void {
        if (this.form.controls.id_detail_output_category.value === '') {
            this.form.controls.id_detail_output_category.setValue(null);
        }
    }

    getOutputFamilyLabel(idOutputFamilly: number): string {
        return this.outputFamilyList.find(outputFamilly => outputFamilly.id === idOutputFamilly).label;
    }

    submit(): void {
        this.save.emit({
            ...this.attribute,
            ...this.form.value
        });
        this.form.markAsPristine();
    }
}
