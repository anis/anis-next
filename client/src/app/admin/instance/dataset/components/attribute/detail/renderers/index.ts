/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { sharedRendererList } from '../../shared-renderers';

export const detailRenderers = [
    
];

export * from './detail-renderer-form-factory';

export const detailRendererList = [
    ...sharedRendererList
];
