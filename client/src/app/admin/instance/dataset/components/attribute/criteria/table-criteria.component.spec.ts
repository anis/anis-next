/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { QueryList } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TableCriteriaComponent } from './table-criteria.component';
import { TrCriteriaComponent } from './tr-criteria.component';

describe('[admin][instance][dataset][components][attribute][criteria] TableCriteriaComponent', () => {
    let component: TableCriteriaComponent;
    let fixture: ComponentFixture<TableCriteriaComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TableCriteriaComponent,
            ],
            providers: [],
            imports: [
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(TableCriteriaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('saveAll() should call the submit method one time', () => {
        let trCriteriaComponent1 = new TrCriteriaComponent();
        trCriteriaComponent1.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trCriteriaComponent1.form.markAsDirty();
        let spy = jest.spyOn(trCriteriaComponent1, 'submit');
        Object.assign((component as any).trCriteriaList, { _results: [{ ...trCriteriaComponent1 }] });
        component.saveAll();
        expect(spy).toHaveBeenCalledTimes(1);

    });
    it('saveAllDisabled() should return false', () => {
        let trCriteriaComponent1 = new TrCriteriaComponent();
        trCriteriaComponent1.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trCriteriaComponent1.form.markAsDirty();
        Object.assign((component as any).trCriteriaList, { _results: [trCriteriaComponent1] });
        expect(component.saveAllDisabled()).toBe(false);

    });
});
