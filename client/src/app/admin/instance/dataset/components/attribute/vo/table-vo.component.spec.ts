/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableVoComponent } from './table-vo.component';
import { TrVoComponent } from './tr-vo.component';

describe('[admin][instance][dataset][components][attribute][vo] TableVoComponent', () => {
    let component: TableVoComponent;
    let fixture: ComponentFixture<TableVoComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TableVoComponent,
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(TableVoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('saveAll() should call the submit method one time', () => {
        let trVoComponent = new TrVoComponent();
        trVoComponent.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trVoComponent.form.markAsDirty();
        let spy = jest.spyOn(trVoComponent, 'submit');
        Object.assign((component as any).trVoList, { _results: [{ ...trVoComponent }] });
        component.saveAll();
        expect(spy).toHaveBeenCalledTimes(1);

    });
    it('saveAllDisabled() should return false', () => {
        let trVoComponent = new TrVoComponent();
        trVoComponent.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trVoComponent.form.markAsDirty();
        Object.assign((component as any).trVoList, { _results: [trVoComponent] });
        expect(component.saveAllDisabled()).toBe(false);

    });
});

