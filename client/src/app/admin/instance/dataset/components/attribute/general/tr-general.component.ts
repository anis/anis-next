/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: '[general]',
    templateUrl: 'tr-general.component.html',
    styleUrls: [ '../tr.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrGeneralComponent implements OnInit {
    @Input() attribute: Attribute
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @Output() delete: EventEmitter<Attribute> = new EventEmitter();

    public form = new UntypedFormGroup({
        id: new UntypedFormControl({value: '', disabled: true}),
        name: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        form_label: new UntypedFormControl('', [Validators.required]),
        description: new UntypedFormControl(),
        primary_key: new UntypedFormControl()
    });

    ngOnInit() {
        if (this.attribute) {
            this.form.patchValue(this.attribute);
        }
    }

    submit(): void {
        this.save.emit({
            ...this.attribute,
            ...this.form.value
        });
        this.form.markAsPristine();
    }
}
