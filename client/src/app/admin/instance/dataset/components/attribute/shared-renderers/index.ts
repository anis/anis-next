import { DownloadRendererComponent } from './download-renderer.component';
import { ImageRendererComponent } from './image-renderer.component';
import { LinkRendererComponent } from './link-renderer.component';

export const sharedRenderers = [
    DownloadRendererComponent,
    ImageRendererComponent,
    LinkRendererComponent
];

export * from './shared-renderer-form-factory';

export const sharedRendererList = [
    { label: 'Download file', value: 'download', display: 20 },
    { label: 'Link (file, web page...)', value: 'link', display: 30 },
    { label: 'Display image (png, jpg...)', value: 'image', display: 40 },
    { label: 'Display json', value: 'json', display: 50 }
];
