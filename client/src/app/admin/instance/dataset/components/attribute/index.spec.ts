/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { attributeComponents } from './index';

describe('[admin][instance][dataset][components][attribute] index', () => {
    it('Test output index components', () => {
        expect(attributeComponents.length).toEqual(7);
    });
});
