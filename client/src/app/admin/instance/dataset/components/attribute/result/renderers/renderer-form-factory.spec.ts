/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { UntypedFormControl } from '@angular/forms';
import { RendererFormFactory } from './renderer-form-factory';

class TestClass extends RendererFormFactory { }
describe('[admin][instance][dataset][components][attribute][result][renderers] RendererFormFactory', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TestClass],
        })
    });

    it('should test case detail-link', () => {
        let spy = jest.spyOn(TestClass, 'create');
        let form = { display: new UntypedFormControl('text') };
        let result = TestClass.create('detail-link');
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));
    })

    it('should test case link', () => {
        let spy = jest.spyOn(TestClass, 'create');
        let result = TestClass.create('link');
        let form = {
            href: new UntypedFormControl('$value'),
            display: new UntypedFormControl('text'),
            text: new UntypedFormControl('$value'),
            icon: new UntypedFormControl('fas fa-link'),
            blank: new UntypedFormControl(true)
        };
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));
    })

    it('should test case download', () => {
        let spy = jest.spyOn(TestClass, 'create');
        let result = TestClass.create('download');
        let form = {
            display: new UntypedFormControl('icon-button'),
            text: new UntypedFormControl('DOWNLOAD'),
            icon: new UntypedFormControl('fas fa-download')
        };
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));
    })

    it('should test case image', () => {
        let spy = jest.spyOn(TestClass, 'create');
        let result = TestClass.create('image');
        let form = {
            type: new UntypedFormControl('fits'),
            display: new UntypedFormControl('modal'),
            width: new UntypedFormControl(''),
            height: new UntypedFormControl('')
        }
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));

    })

    it('should test case default', () => {
        let spy = jest.spyOn(TestClass, 'create');
        let result = TestClass.create('');
        expect(spy).toHaveBeenCalled();
        expect(result).toEqual({});
    })
});

