/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';

import { Attribute, CriteriaFamily } from 'src/app/metamodel/models';
import { TrCriteriaComponent } from './tr-criteria.component';

@Component({
    selector: 'app-table-criteria',
    templateUrl: 'table-criteria.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableCriteriaComponent {
    @Input() attributeList: Attribute[];
    @Input() criteriaFamilyList: CriteriaFamily[];
    @Input() attributeDistinctList: string[]
    @Input() attributeDistinctListIsLoading: boolean;
    @Input() attributeDistinctListIsLoaded: boolean;
    @Output() loadAttributeDistinctList: EventEmitter<Attribute> = new EventEmitter();
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @ViewChildren(TrCriteriaComponent) private trCriteriaList: QueryList<TrCriteriaComponent>;

    saveAll() {
        this.trCriteriaList.forEach(trCriteria => {
            if (trCriteria.form.dirty && trCriteria.form.valid) {
                trCriteria.submit();
            }
        });
    }

    saveAllDisabled() {
        let disabled = true;
        if (this.trCriteriaList) {
            disabled = this.trCriteriaList.filter(trCriteria => trCriteria.form.dirty && trCriteria.form.valid).length === 0;
        }
        return disabled;
    }
}
