/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DownloadRendererComponent } from './download-renderer.component';

describe('[admin][instance][dataset][components][attribute][result][renderers] DownloadRendererComponent', () => {
    let component: DownloadRendererComponent;
    let fixture: ComponentFixture<DownloadRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DownloadRendererComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(DownloadRendererComponent);
        component = fixture.componentInstance;
        let form = new UntypedFormGroup({
            display: new UntypedFormControl(),
            text: new UntypedFormControl()

        });
        component.form = form;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});


