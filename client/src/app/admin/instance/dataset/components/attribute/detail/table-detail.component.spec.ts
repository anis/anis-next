/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableDetailComponent } from './table-detail.component';
import { TrDetailComponent } from './tr-detail.component';

describe('[admin][instance][dataset][components][attribute][detail] TableDetailComponent', () => {
    let component: TableDetailComponent;
    let fixture: ComponentFixture<TableDetailComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TableDetailComponent,
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(TableDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('saveAll() should call the submit method one time', () => {
        let trDetailComponent = new TrDetailComponent();
        trDetailComponent.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trDetailComponent.form.markAsDirty();
        let spy = jest.spyOn(trDetailComponent, 'submit');
        Object.assign((component as any).trDetailList, { _results: [{ ...trDetailComponent }] });
        component.saveAll();
        expect(spy).toHaveBeenCalledTimes(1);

    });
    it('saveAllDisabled() should return false', () => {
        let trDetailComponent = new TrDetailComponent();
        trDetailComponent.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trDetailComponent.form.markAsDirty();
        Object.assign((component as any).trDetailList, { _results: [{ ...trDetailComponent }] })
        expect(component.saveAllDisabled()).toBe(false);

    });
});

