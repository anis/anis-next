/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Attribute } from 'src/app/metamodel/models';
import { TrCriteriaComponent } from './tr-criteria.component';

describe('[admin][instance][dataset][components][attribute][criteria] TrCriteriaComponent', () => {
    let component: TrCriteriaComponent;
    let fixture: ComponentFixture<TrCriteriaComponent>;
    let form;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TrCriteriaComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(TrCriteriaComponent);
        component = fixture.componentInstance;
    });

    beforeEach(() => {
        form = new UntypedFormGroup({
            name: new UntypedFormControl({ value: '', disabled: true }),
            type: new UntypedFormControl({ value: '', disabled: true }),
            id_criteria_family: new UntypedFormControl(''),
            search_type: new UntypedFormControl(''),
            operator: new UntypedFormControl(''),
            dynamic_operator: new UntypedFormControl(),
            min: new UntypedFormControl(''),
            max: new UntypedFormControl(''),
            options: new UntypedFormArray([]),
            placeholder_min: new UntypedFormControl(''),
            placeholder_max: new UntypedFormControl(''),
            criteria_display: new UntypedFormControl('')
        });
        component.attribute = { ...attribute, name: 'test', id_criteria_family: 0, options: [] };
        fixture.detectChanges();
    })

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('criteriaFamilyOnChange() should set id_criteria_family to null and call searchTypeOnChange()', () => {
        let spy = jest.spyOn(component, 'searchTypeOnChange');
        component.form = form;
        expect(form.controls.id_criteria_family.value).toEqual('');
        component.criteriaFamilyOnChange();
        expect(form.controls.id_criteria_family.value).toEqual(null);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('searchTypeOnChange() should set search_type to null and call operatorOnChange()', () => {
        let spy = jest.spyOn(component, 'operatorOnChange');
        component.form = form;
        expect(form.controls.search_type.value).toEqual('');
        component.searchTypeOnChange();
        expect(form.controls.search_type.value).toEqual(null);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should set operator, min, max, palceholder_min, palceholder_max to null and call clear method on optionsFormArray', () => {
        let spy = jest.spyOn(component.optionsFormArray, 'clear');
        component.form = form;
        expect(component.form.controls.operator.value).toEqual('');
        component.operatorOnChange();
        expect(component.form.controls.operator.value).toEqual(null);
        expect(component.form.controls.min.value).toEqual(null);
        expect(component.form.controls.max.value).toEqual(null);
        expect(component.form.controls.placeholder_min.value).toEqual(null);
        expect(component.form.controls.placeholder_max.value).toEqual(null);
        expect(spy).toHaveBeenCalledTimes(1)
    });
    
    it('getMinValuePlaceholder(searchType: string) should return Default min value (optional)', () => {
        let searchType: string = 'between-date';
        let result = component.getMinValuePlaceholder(searchType);
        expect(result).toEqual('Default min value (optional)')
    });

    it('getMinValuePlaceholder(searchType: string) should return Default value (optional)', () => {
        let searchType: string = 'test';
        let result = component.getMinValuePlaceholder(searchType);
        expect(result).toEqual('Default value (optional)')
    });

    it('submit() should emit component.attribute and component.form.value', () => {
        let spy = jest.spyOn(component.save, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.attribute, ...component.form.value })
    });
});
