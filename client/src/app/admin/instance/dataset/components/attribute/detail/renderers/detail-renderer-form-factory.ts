/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SharedRendererFormFactory } from '../../shared-renderers';

export abstract class DetailRendererFormFactory {
    static create(renderer: string) {
        const sharedRendererForm = SharedRendererFormFactory.create(renderer);

        let detailRendererForm = null;
        switch (renderer) {

        }
        
        return (detailRendererForm) ? detailRendererForm : sharedRendererForm;
    }
}
