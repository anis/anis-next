/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: '[vo]',
    templateUrl: 'tr-vo.component.html',
    styleUrls: [ '../tr.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrVoComponent implements OnInit {
    @Input() attribute: Attribute
    @Output() save: EventEmitter<Attribute> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl({ value: '', disabled: true }),
        vo_utype: new UntypedFormControl(),
        vo_ucd: new UntypedFormControl(),
        vo_unit: new UntypedFormControl(),
        vo_description: new UntypedFormControl(),
        vo_datatype: new UntypedFormControl(),
        vo_size: new UntypedFormControl()
    });

    ngOnInit() {
        if (this.attribute) {
            this.form.patchValue(this.attribute);
        }
    }

    submit(): void {
        this.save.emit({
            ...this.attribute,
            ...this.form.value
        });
        this.form.markAsPristine();
    }
}
