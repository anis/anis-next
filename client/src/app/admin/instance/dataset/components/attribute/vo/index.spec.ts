/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { voComponents } from './index';

describe('[admin][instance][dataset][components][attribute][vo] index', () => {
    it('Test output index components', () => {
        expect(voComponents.length).toEqual(2);
    });
});


