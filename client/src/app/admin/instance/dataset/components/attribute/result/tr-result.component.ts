/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { Attribute } from 'src/app/metamodel/models';
import { RendererFormFactory, rendererList } from './renderers';

@Component({
    selector: '[result]',
    templateUrl: 'tr-result.component.html',
    styleUrls: [ '../tr.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrResultComponent implements OnInit {
    @Input() attribute: Attribute;
    @Output() save: EventEmitter<Attribute> = new EventEmitter();

    rendererList = rendererList;

    public form = new UntypedFormGroup({
        name: new UntypedFormControl({ value: '', disabled: true }),
        renderer: new UntypedFormControl(),
        order_by: new UntypedFormControl(),
        archive: new UntypedFormControl()
    });

    ngOnInit() {
        if (this.attribute) {
            const rendererConfigForm = RendererFormFactory.create(this.attribute.renderer);
            this.form.addControl('renderer_config', new UntypedFormGroup(rendererConfigForm));
            this.form.patchValue(this.attribute);
        }
    }

    getRendererConfigForm() {
        return this.form.controls['renderer_config'] as UntypedFormGroup;
    }

    rendererOnChange() {
        if (this.form.controls.renderer.value === '') {
            this.form.controls.renderer.setValue(null);
            this.form.setControl('renderer_config', new UntypedFormGroup({}));
        } else {
            this.form.setControl('renderer_config', new UntypedFormGroup(RendererFormFactory.create(this.form.controls.renderer.value)));
        }
    }

    submit(): void {
        this.save.emit({
            ...this.attribute,
            ...this.form.value
        });
        this.form.markAsPristine();
    }
}
