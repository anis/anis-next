/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { detailComponents } from './index';

describe('[admin][instance][dataset][components][attribute][detail] index', () => {
    it('Test detail index components', () => {
        expect(detailComponents.length).toEqual(3);
    });
});

