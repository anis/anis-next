/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { outputComponents } from './index';

describe('[admin][instance][dataset][components][attribute][output] index', () => {
    it('Test output index components', () => {
        expect(outputComponents.length).toEqual(2);
    });
});

