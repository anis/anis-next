/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, ViewChildren, QueryList, Input, Output, EventEmitter } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';
import { TrVoComponent } from './tr-vo.component';

@Component({
    selector: 'app-table-vo',
    templateUrl: 'table-vo.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableVoComponent {
    @Input() attributeList: Attribute[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @ViewChildren(TrVoComponent) private trVoList: QueryList<TrVoComponent>;

    saveAll() {
        this.trVoList.forEach(trVo => {
            if (trVo.form.dirty && trVo.form.valid) {
                trVo.submit();
            }
        });
    }

    saveAllDisabled() {
        let disabled = true;
        if (this.trVoList) {
            disabled = this.trVoList.filter(trVo => trVo.form.dirty && trVo.form.valid).length === 0;
        }
        return disabled;
    }
}
