/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, ViewChildren, QueryList, Input, Output, EventEmitter } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';
import { TrGeneralComponent } from './tr-general.component';

@Component({
    selector: 'app-table-general',
    templateUrl: 'table-general.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableGeneralComponent {
    @Input() attributeList: Attribute[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @Output() delete: EventEmitter<Attribute> = new EventEmitter();
    @ViewChildren(TrGeneralComponent) private trGeneralList: QueryList<TrGeneralComponent>;

    saveAll() {
        this.trGeneralList.forEach(trGeneral => {
            if (trGeneral.form.dirty && trGeneral.form.valid) {
                trGeneral.submit();
            }
        });
    }

    saveAllDisabled() {
        let disabled = true;
        if (this.trGeneralList) {
            disabled = this.trGeneralList.filter(trGeneral => trGeneral.form.dirty && trGeneral.form.valid).length === 0;
        }
        return disabled;
    }
}
