/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

 import { renderers, rendererList } from './index';

 describe('[admin][instance][dataset][components][attribute][result][renderers] index', () => {
     it('Test renderers index ', () => {
         expect(renderers.length).toEqual(1);
         expect(rendererList.length).toEqual(5);
     });
 });
 
 