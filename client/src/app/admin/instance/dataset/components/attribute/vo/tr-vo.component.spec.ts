/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Attribute } from 'src/app/metamodel/models';
import { TrVoComponent } from './tr-vo.component';

describe('[admin][instance][dataset][components][attribute][vo] TrVoComponent', () => {
    let component: TrVoComponent;
    let fixture: ComponentFixture<TrVoComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TrVoComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(TrVoComponent);
        component = fixture.componentInstance;
        let attribute: Attribute;
        component.attribute = { ...attribute, name: 'test', vo_utype: '', vo_ucd: '', vo_unit: '', vo_description: '', vo_datatype: '', vo_size: 1 };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('submit() should emit with attribute and form.value', () => {
        let spy = jest.spyOn(component.save, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.attribute, ...component.form.value });
    })
});

