/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableOutputComponent } from './table-output.component';
import { TrOutputComponent } from './tr-output.component';

describe('[admin][instance][dataset][components][attribute][output] TableOutputComponent', () => {
    let component: TableOutputComponent;
    let fixture: ComponentFixture<TableOutputComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TableOutputComponent,
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(TableOutputComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('saveAll() should call the submit method one time', () => {
        let trOutputComponent1 = new TrOutputComponent();
        trOutputComponent1.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trOutputComponent1.form.markAsDirty();
        let spy = jest.spyOn(trOutputComponent1, 'submit');
        Object.assign((component as any).trOutputList, { _results: [{ ...trOutputComponent1 }] });
        component.saveAll();
        expect(spy).toHaveBeenCalledTimes(1);

    });
    it('saveAllDisabled() should return false', () => {
        let trOutputComponent1 = new TrOutputComponent();
        trOutputComponent1.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trOutputComponent1.form.markAsDirty();
        Object.assign((component as any).trOutputList, { _results: [trOutputComponent1] });
        expect(component.saveAllDisabled()).toBe(false);

    });
});

