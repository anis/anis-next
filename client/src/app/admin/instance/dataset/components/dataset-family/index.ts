/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetFamilyCardComponent } from "./dataset-family-card.component";
import { EditDatasetFamilyComponent } from "./edit-dataset-family.component";
import { DatasetFamilyFormComponent } from './dataset-family-form.component';

export const datasetFamilyComponents = [
    DatasetFamilyCardComponent,
    EditDatasetFamilyComponent,
    DatasetFamilyFormComponent
];
