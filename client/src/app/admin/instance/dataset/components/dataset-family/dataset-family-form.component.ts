/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { DatasetFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-family-form',
    templateUrl: 'dataset-family-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetFamilyFormComponent implements OnInit {
    @Input() datasetFamily: DatasetFamily;
    @Output() onSubmit: EventEmitter<DatasetFamily> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
        opened: new UntypedFormControl(true)
    });

    ngOnInit() {
        if (this.datasetFamily) {
            this.form.patchValue(this.datasetFamily);
        }
    }

    submit() {
        if (this.datasetFamily) {
            this.onSubmit.emit({
                ...this.datasetFamily,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
