/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { datasetFamilyComponents } from './index';

describe('[admin][instance][dataset][components][dataset-family] index', () => {
    it('should test dataset index components', () => {
        expect(datasetFamilyComponents.length).toEqual(3);
    });
});
