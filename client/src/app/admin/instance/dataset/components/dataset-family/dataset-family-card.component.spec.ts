/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component, Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Dataset } from 'src/app/metamodel/models';
import { DatasetFamilyCardComponent } from './dataset-family-card.component';

@Pipe({ name: 'datasetListByFamily' })
class DatasetListByFamilyPipe implements PipeTransform {
    transform(datasetList: Dataset[], idDatasetFamily: number): Dataset[] {
        return datasetList.filter(dataset => dataset.id_dataset_family === idDatasetFamily);
    }
}

@Component({
    selector: 'app-edit-dataset-family',
})
export class EditDatasetFamilyComponent { }

@Component({
    selector: 'app-delete-btn',
})
export class DeleteBtnComponent { }

describe('[admin][instance][dataset][components][dataset-family] DatasetFamilyCardComponent', () => {
    let component: DatasetFamilyCardComponent;
    let fixture: ComponentFixture<DatasetFamilyCardComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetFamilyCardComponent,
                DatasetListByFamilyPipe,
                EditDatasetFamilyComponent,
                DeleteBtnComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(DatasetFamilyCardComponent);
        component = fixture.componentInstance;
        let dataset: Dataset;
        component.datasetList = [{ ...dataset, id_dataset_family: 1, label: 'test' }, { ...dataset, id_dataset_family: 2, label: 'test' }]
        component.datasetFamily = { display: 10, id: 2, label: 'test', opened: false };
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('nbDatasetsByDatasetFamily() should return 1', () => {

        let result = component.nbDatasetsByDatasetFamily();
        expect(result).toEqual(1);
    })
});
