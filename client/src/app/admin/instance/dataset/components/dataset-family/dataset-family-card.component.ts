/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { Instance, DatasetFamily, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-family-card',
    templateUrl: 'dataset-family-card.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetFamilyCardComponent {
    @Input() instance: Instance;
    @Input() datasetFamily: DatasetFamily;
    @Input() datasetList: Dataset[];
    @Output() editDatasetFamily: EventEmitter<DatasetFamily> = new EventEmitter();
    @Output() deleteDatasetFamily: EventEmitter<DatasetFamily> = new EventEmitter();
    @Output() deleteDataset: EventEmitter<Dataset> = new EventEmitter();

    nbDatasetsByDatasetFamily(): number {
        return this.datasetList.filter(dataset => dataset.id_dataset_family === this.datasetFamily.id).length;
    }
}
