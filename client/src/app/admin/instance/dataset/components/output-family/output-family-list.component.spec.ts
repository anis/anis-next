/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OutputFamilyListComponent } from './output-family-list.component';

@Component({
    selector: 'app-add-output-family',

})
class AddOutputFamilyComponent { }

describe('[admin][instance][dataset][components][output-family] OutputFamilyListComponent', () => {
    let component: OutputFamilyListComponent;
    let fixture: ComponentFixture<OutputFamilyListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputFamilyListComponent,
                AddOutputFamilyComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(OutputFamilyListComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
