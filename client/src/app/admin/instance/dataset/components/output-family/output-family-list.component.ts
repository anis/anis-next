/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { OutputFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-output-family-list',
    templateUrl: 'output-family-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputFamilyListComponent {
    @Input() outputFamilyList: OutputFamily[];
    @Output() add: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() edit: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() delete: EventEmitter<OutputFamily> = new EventEmitter();
}
