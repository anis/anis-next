/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { outputFamilyComponents } from './index';

describe('[admin][instance][dataset][components][output-family] index', () => {
    it('should test output-family index components', () => {
        expect(outputFamilyComponents.length).toEqual(4);
    });
});



