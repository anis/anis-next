/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OutputFamilyListComponent } from './output-family-list.component';
import { OutputFamilyFormComponent } from './output-family-form.component';
import { AddOutputFamilyComponent } from './add-output-family.component';
import { EditOutputFamilyComponent } from './edit-output-family.component';

export const outputFamilyComponents = [
    OutputFamilyListComponent,
    OutputFamilyFormComponent,
    AddOutputFamilyComponent,
    EditOutputFamilyComponent
];
