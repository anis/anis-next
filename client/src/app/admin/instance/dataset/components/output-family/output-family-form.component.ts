/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { OutputFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-output-family-form',
    templateUrl: 'output-family-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputFamilyFormComponent {
    @Input() outputFamily: OutputFamily;
    @Output() onSubmit: EventEmitter<OutputFamily> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
        opened: new UntypedFormControl(true)
    });

    ngOnInit() {
        if (this.outputFamily) {
            this.form.patchValue(this.outputFamily);
        }
    }

    submit() {
        if (this.outputFamily) {
            this.onSubmit.emit({
                ...this.outputFamily,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
