/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Instance } from 'src/app/metamodel/models';
import { InstanceButtonsComponent } from './instance-buttons.component';

describe('[admin][instance][dataset][components] InstanceButtonsComponent', () => {
    let component: InstanceButtonsComponent;
    let fixture: ComponentFixture<InstanceButtonsComponent>;
    const modalServiceStub = {
        show: jest.fn(),
    };
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceButtonsComponent,
            ],
            providers: [
                BsModalRef,
                { provide: BsModalService, useValue: modalServiceStub }
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(InstanceButtonsComponent);
        component = fixture.componentInstance;
        component.instance = { ...instance, name: 'test' };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call modalRef.show(template)', () => {
        let template: TemplateRef<any> = null;
        let spy = jest.spyOn(modalServiceStub, 'show');
        component.openModal(template);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(template);
    });
});
