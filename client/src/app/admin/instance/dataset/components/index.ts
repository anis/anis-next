/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { attributeComponents } from './attribute';
import { criteriaFamilyComponents } from './criteria-family';
import { datasetComponents } from './dataset';
import { datasetFamilyComponents } from './dataset-family';
import { imageComponents } from './image';
import { fileComponents } from './file';
import { coneSearchConfigComponents } from './cone-search-config';
import { detailConfigComponents } from './detail-page';
import { aliasConfigComponents } from './alias-config';
import { outputCategoryComponents } from './output-category';
import { outputFamilyComponents } from './output-family';
import { AddAttributeComponent } from './add-attribute.component';
import { InstanceButtonsComponent } from './instance-buttons.component';

export const dummiesComponents = [
    attributeComponents,
    criteriaFamilyComponents,
    datasetComponents,
    datasetFamilyComponents,
    imageComponents,
    fileComponents,
    coneSearchConfigComponents,
    detailConfigComponents,
    aliasConfigComponents,
    outputCategoryComponents,
    outputFamilyComponents,
    AddAttributeComponent,
    InstanceButtonsComponent
];
