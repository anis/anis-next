/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Column } from 'src/app/admin/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { AddAttributeComponent } from './add-attribute.component';

describe('[admin][instance][dataset][components] AddAttributeComponent', () => {
    let component: AddAttributeComponent;
    let fixture: ComponentFixture<AddAttributeComponent>;
    let attribute: Attribute;
    const modalServiceStub = {
        show: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AddAttributeComponent,
            ],
            providers: [
                BsModalRef,
                { provide: BsModalService, useValue: modalServiceStub }
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(AddAttributeComponent);
        component = fixture.componentInstance;
        let attribute: Attribute;
        component.attributeList = [{ ...attribute, name: 'test1' }, { ...attribute, name: 'test2test' }]
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call modalRef.show(template)', () => {
        let template: TemplateRef<any> = null;
        let spyOnLoadColumnList = jest.spyOn(component.loadColumnList, 'emit');
        let spy = jest.spyOn(modalServiceStub, 'show');
        component.openModal(template);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(template);
        expect(spyOnLoadColumnList).toHaveBeenCalledTimes(1);
    });
    it('alreadyExists(columnName: string) should return true', () => {
        expect(component.alreadyExists('test1')).toEqual(true);
        expect(component.alreadyExists('test')).toEqual(false);
    });
    it('alreadyExists(columnName: string) should call emit on add', () => {
        let spy = jest.spyOn(component.addAttribute, 'emit');
        let column: Column = { name: 'test', type: 'test' };
        component.addNewAttribute(column);
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('addAllAttributes() should emit addAttributeList event', () => {
        let spy = jest.spyOn(component.addAttributeList, 'emit');
        component.attributeList = [{ ...attribute, name: 'test1' }];
        component.getMaxId = jest.fn().mockImplementation(() => 1);
        component.getAttributeFromColumn = jest.fn().mockImplementation(() => { return { ...attribute, name: 'test' } })
        component.columnList = [{ name: 'test', type: 'test' }];
        component.addAllAttributes();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith([{ ...attribute, name: 'test' }]);

    });
    it('addAllBtnDisabled() should return true', () => {
        component.attributeList = [{ ...attribute, name: 'test1' }];
        component.columnList = [{ name: 'test', type: 'test' }];
        expect(component.addAllBtnDisabled()).toBe(true);
    })

});
