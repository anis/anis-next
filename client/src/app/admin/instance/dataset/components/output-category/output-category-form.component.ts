/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { OutputCategory, OutputFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-output-category-form',
    templateUrl: 'output-category-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputCategoryFormComponent {
    @Input() outputCategory: OutputCategory;
    @Input() outputFamilyList: OutputFamily[];
    @Output() onSubmit: EventEmitter<OutputCategory> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
        id_output_family: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.outputCategory) {
            this.form.patchValue(this.outputCategory);
        }
    }

    submit() {
        if (this.outputCategory) {
            this.onSubmit.emit({
                ...this.outputCategory,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
