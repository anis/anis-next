/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { OutputCategory, OutputFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-output-category-list',
    templateUrl: 'output-category-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputCategoryListComponent {
    @Input() outputCategoryList: OutputCategory[];
    @Input() outputFamilyList: OutputFamily[];
    @Output() add: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() edit: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() delete: EventEmitter<OutputCategory> = new EventEmitter();
}
