/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OutputCategoryFormComponent } from './output-category-form.component';

describe('[admin][instance][dataset][components][output-category] OutputCategoryFormComponent', () => {
    let component: OutputCategoryFormComponent;
    let fixture: ComponentFixture<OutputCategoryFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputCategoryFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(OutputCategoryFormComponent);
        component = fixture.componentInstance;
        component.outputCategory = {
            display: 10,
            id: 1,
            id_output_family: 1,
            label: 'test'
        };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('submit() should emit outputCategory and form.value()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.outputCategory, ...component.form.value });
    });

    it('submit() should emit only form.value()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.outputCategory = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.value });
    })
});
