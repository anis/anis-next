/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OutputCategoryListComponent } from './output-category-list.component';

describe('[admin][instance][dataset][components][output-category] OutputCategoryListComponent', () => {
    let component: OutputCategoryListComponent;
    let fixture: ComponentFixture<OutputCategoryListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputCategoryListComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(OutputCategoryListComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
