/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

 import { outputCategoryComponents } from './index';

 describe('[admin][instance][dataset][components][output-category] index', () => {
     it('should test output-category index components', () => {
         expect(outputCategoryComponents.length).toEqual(4);
     });
 });
 
 
 
 