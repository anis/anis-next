/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, DatasetFamily, Dataset } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetFamilyActions from 'src/app/metamodel/actions/dataset-family.actions';
import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

@Component({
    selector: 'app-dataset-list',
    templateUrl: 'dataset-list.component.html'
})
export class DatasetListComponent {
    public instance: Observable<Instance>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
        this.datasetList = store.select(datasetSelector.selectAllDatasets);
    }

    addDatasetFamily(datasetFamily: DatasetFamily) {
        this.store.dispatch(datasetFamilyActions.addDatasetFamily({ datasetFamily }));
    }

    editDatasetFamily(datasetFamily: DatasetFamily) {
        this.store.dispatch(datasetFamilyActions.editDatasetFamily({ datasetFamily }));
    }

    deleteDatasetFamily(datasetFamily: DatasetFamily) {
        this.store.dispatch(datasetFamilyActions.deleteDatasetFamily({ datasetFamily }));
    }

    deleteDataset(dataset: Dataset) {
        this.store.dispatch(datasetActions.deleteDataset({ dataset }));
    }
}
