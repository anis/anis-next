/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { Instance, DatasetFamily, Dataset, Database } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';
import * as tableActions from 'src/app/admin/store/actions/table.actions';
import * as tableSelector from 'src/app/admin/store/selectors/table.selector';
import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as adminFileExplorerSelector from 'src/app/admin/store/selectors/admin-file-explorer.selector';
import * as databaseSelector from 'src/app/metamodel/selectors/database.selector';

@Component({
    selector: 'app-new-dataset',
    templateUrl: 'new-dataset.component.html'
})
export class NewDatasetComponent implements OnInit {
    public instance: Observable<Instance>;
    public databaseList: Observable<Database[]>;
    public databaseListIsLoading: Observable<boolean>;
    public databaseListIsLoaded: Observable<boolean>;
    public tableListIsLoading: Observable<boolean>;
    public tableListIsLoaded: Observable<boolean>;
    public tableList: Observable<string[]>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public idDatasetFamily: Observable<number>;
    public files: Observable<FileInfo[]>;
    public filesIsLoading: Observable<boolean>;
    public filesIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>, private route: ActivatedRoute) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.databaseListIsLoading = store.select(databaseSelector.selectDatabaseListIsLoading);
        this.databaseListIsLoaded = store.select(databaseSelector.selectDatabaseListIsLoaded);
        this.databaseList = store.select(databaseSelector.selectAllDatabases);
        this.tableListIsLoading = store.select(tableSelector.selectTableListIsLoading);
        this.tableListIsLoaded = store.select(tableSelector.selectTableListIsLoaded);
        this.tableList = store.select(tableSelector.selectAllTables);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.files = store.select(adminFileExplorerSelector.selectFiles);
        this.filesIsLoading = store.select(adminFileExplorerSelector.selectFilesIsLoading);
        this.filesIsLoaded = store.select(adminFileExplorerSelector.selectFilesIsLoaded);
    }

    ngOnInit() {
        this.idDatasetFamily = this.route.queryParamMap.pipe(
            map(params => +params.get('id_dataset_family'))
        );
    }

    loadTableList(idDatabase: number) {
        this.store.dispatch(tableActions.loadTableList({ idDatabase }));
    }

    loadRootDirectory(path: string) {
        this.store.dispatch(adminFileExplorerActions.loadFiles({ path }));
    }

    addNewDataset(dataset: Dataset) {
        this.store.dispatch(datasetActions.addDataset({ dataset }));
    }
}
