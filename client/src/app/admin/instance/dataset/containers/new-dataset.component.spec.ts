/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { firstValueFrom, of } from 'rxjs';

import { NewDatasetComponent } from './new-dataset.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Dataset, Instance } from 'src/app/metamodel/models';
import { RouterTestingModule } from '@angular/router/testing';
import * as tableActions from 'src/app/admin/store/actions/table.actions';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';

describe('[admin][instance][dataset][containers] NewDatasetComponent', () => {
    let component: NewDatasetComponent;
    let fixture: ComponentFixture<NewDatasetComponent>;
    let store: MockStore;
    let mockInstanceSelectorSelectInstanceByRouteName;
    let mockparamMap = {
        get: jest.fn().mockImplementation((name: string) => 0)
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewDatasetComponent,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        queryParamMap: of(mockparamMap)
                    }
                },
                provideMockStore({})
            ],

            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                RouterTestingModule.withRoutes([])
            ]
        });

        fixture = TestBed.createComponent(NewDatasetComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        let instance: Instance;
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, name: 'test' });
        fixture.detectChanges();
        TestBed.inject(ActivatedRoute);
    });

    it('should create the component', async() => {
        expect(component).toBeTruthy()
        expect(await firstValueFrom(component.idDatasetFamily)).toEqual(0);
    });

    it('store should dispatch tableActions.loadTableList', () => {
        let idDatabase: number = 1;
        let spy = jest.spyOn(store, 'dispatch');
        component.loadTableList(idDatabase);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(tableActions.loadTableList({ idDatabase }));
    });

    it('store should dispatch adminFileExplorerActions.loadFiles', () => {
        let path: string = 'test';
        let spy = jest.spyOn(store, 'dispatch');
        component.loadRootDirectory(path);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path }));
    });

    it('store should dispatch datasetActions.addDataset', () => {
        let dataset: Dataset = {
            display: 10,
            label: 'test',
            cone_search_config_id: 1,
            data_path: 'test',
            datatable_enabled: false,
            datatable_selectable_rows: false,
            description: 'test',
            download_ascii: false,
            download_csv: false,
            download_json: true,
            download_vo: true,
            download_fits: true,
            full_data_path: 'test',
            id_database: 10,
            id_dataset_family: 1,
            name: 'test',
            public: false,
            server_link_enabled: false,
            table_ref: 'test'
        };

        let spy = jest.spyOn(store, 'dispatch');
        component.addNewDataset(dataset);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetActions.addDataset({ dataset }));
    });
});
