/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';;

import { EditDatasetComponent } from './edit-dataset.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Dataset, Instance } from 'src/app/metamodel/models';
import * as tableActions from 'src/app/admin/store/actions/table.actions';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';

describe('[admin][instance][dataset][containers] DatasetListComponent', () => {
    let component: EditDatasetComponent;
    let fixture: ComponentFixture<EditDatasetComponent>;
    let store: MockStore;
    let mockinstanceSelectorInstanceByRouteName;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditDatasetComponent
            ],
            providers: [
                provideMockStore({}),
            ],

            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
            ]
        });

        fixture = TestBed.createComponent(EditDatasetComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        let instance: Instance;
        mockinstanceSelectorInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, name: 'test' })
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('store should dispatch tableActions.loadTableList', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let idDatabase: number = 1;
        component.loadTableList(idDatabase);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(tableActions.loadTableList({ idDatabase }));
    });

    it('store should dispatch adminFileExplorerActions.loadFiles', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let path: string = 'test';
        component.loadRootDirectory(path);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path }));
    });

    it('store should dispatch datasetActions.editDataset', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let dataset: Dataset = {
            display: 10,
            label: 'test',
            cone_search_config_id: 1,
            data_path: 'test',
            datatable_enabled: false,
            datatable_selectable_rows: false,
            description: 'test',
            download_ascii: false,
            download_csv: false,
            download_json: true,
            download_vo: true,
            download_fits: true,
            full_data_path: 'test',
            id_database: 10,
            id_dataset_family: 1,
            name: 'test',
            public: false,
            server_link_enabled: false,
            table_ref: 'test'
        };
        component.editDataset(dataset);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetActions.editDataset({ dataset }));
    });
});
