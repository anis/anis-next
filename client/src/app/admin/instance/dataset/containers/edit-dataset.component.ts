/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, DatasetFamily, Dataset, Database } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import * as tableActions from 'src/app/admin/store/actions/table.actions';
import * as tableSelector from 'src/app/admin/store/selectors/table.selector';
import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as adminFileExplorerSelector from 'src/app/admin/store/selectors/admin-file-explorer.selector';
import * as imageActions from 'src/app/metamodel/actions/image.actions';
import * as databaseSelector from 'src/app/metamodel/selectors/database.selector';

@Component({
    selector: 'app-edit-dataset',
    templateUrl: 'edit-dataset.component.html'
})
export class EditDatasetComponent implements OnInit {
    public instance: Observable<Instance>;
    public datasetSelected: Observable<string>;
    public datasetList: Observable<Dataset[]>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public databaseList: Observable<Database[]>;
    public databaseListIsLoading: Observable<boolean>;
    public databaseListIsLoaded: Observable<boolean>;
    public tableListIsLoading: Observable<boolean>;
    public tableListIsLoaded: Observable<boolean>;
    public tableList: Observable<string[]>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public files: Observable<FileInfo[]>;
    public filesIsLoading: Observable<boolean>;
    public filesIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.datasetSelected = store.select(datasetSelector.selectDatasetNameByRoute);
        this.datasetList = store.select(datasetSelector.selectAllDatasets);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
        this.databaseListIsLoading = store.select(databaseSelector.selectDatabaseListIsLoading);
        this.databaseListIsLoaded = store.select(databaseSelector.selectDatabaseListIsLoaded);
        this.databaseList = store.select(databaseSelector.selectAllDatabases);
        this.tableListIsLoading = store.select(tableSelector.selectTableListIsLoading);
        this.tableListIsLoaded = store.select(tableSelector.selectTableListIsLoaded);
        this.tableList = store.select(tableSelector.selectAllTables);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.files = store.select(adminFileExplorerSelector.selectFiles);
        this.filesIsLoading = store.select(adminFileExplorerSelector.selectFilesIsLoading);
        this.filesIsLoaded = store.select(adminFileExplorerSelector.selectFilesIsLoaded);
    }

    ngOnInit() {
        this.store.dispatch(imageActions.loadImageList());
    }

    loadTableList(idDatabase: number) {
        this.store.dispatch(tableActions.loadTableList({ idDatabase }));
    }

    loadRootDirectory(path: string) {
        this.store.dispatch(adminFileExplorerActions.loadFiles({ path }));
    }

    editDataset(dataset: Dataset) {
        this.store.dispatch(datasetActions.editDataset({ dataset }));
    }
}
