/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { firstValueFrom, of } from 'rxjs';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { ConfigureDatasetComponent } from './configure-dataset.component';
import * as fitsImageActions from 'src/app/admin/store/actions/fits-image.actions';
import * as coneSearchConfigActions from 'src/app/metamodel/actions/cone-search-config.actions';
import * as detailConfigActions from 'src/app/metamodel/actions/detail-config.actions';
import * as columnActions from 'src/app/admin/store/actions/column.actions';
import * as attributeDistinctActions from 'src/app/admin/store/actions/attribute-distinct.actions';
import * as criteriaFamilyActions from 'src/app/metamodel/actions/criteria-family.actions';
import * as outputFamilyActions from 'src/app/metamodel/actions/output-family.actions';
import * as outputCategoryActions from 'src/app/metamodel/actions/output-category.actions';
import * as attributeActions from 'src/app/metamodel/actions/attribute.actions';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as imageActions from 'src/app/metamodel/actions/image.actions';
import * as fileActions from 'src/app/metamodel/actions/file.actions';
import * as tableActions from 'src/app/admin/store/actions/table.actions';
import * as aliasConfigActions from 'src/app/metamodel/actions/alias-config.actions';
import { AliasConfig, Attribute, ConeSearchConfig, CriteriaFamily, DetailConfig, File, Image, OutputCategory, OutputFamily } from 'src/app/metamodel/models';

describe('[admin][instance][dataset][containers] ConfigureDatasetComponent', () => {
    let component: ConfigureDatasetComponent;
    let fixture: ComponentFixture<ConfigureDatasetComponent>;
    let store: MockStore;
    let attribute: Attribute;
    let mockparamMap = {
        get: jest.fn().mockImplementation((name: string) => name)
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConfigureDatasetComponent,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        queryParamMap: of(mockparamMap),
                    },
                },
                provideMockStore({}),
            ],

            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
            ]
        });
        fixture = TestBed.createComponent(ConfigureDatasetComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it('should create the component and init tabSelect', async () => {
        expect(component).toBeTruthy();
        expect(await firstValueFrom(component.tabSelected)).toEqual('tab_selected');
    });

    it('store should dispatch loadTableList action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadTableList(1)
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(tableActions.loadTableList({ idDatabase: 1 }));
    });
    it('store should dispatch loadColumnList action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadColumnList()
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(columnActions.loadColumnList());
    });

    it('store should dispatch loadAttributeDistinctList action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadAttributeDistinctList(attribute);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(attributeDistinctActions.loadAttributeDistinctList({ attribute }));
    })

    it('getVoEnabled() should return true', () => {

        let result = component.getVoEnabled();
        expect(result).toEqual(true);
    });

    it('store should dispatch addCriteriaFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let criteriaFamily: CriteriaFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.addCriteriaFamily(criteriaFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criteriaFamilyActions.addCriteriaFamily({ criteriaFamily }));
    });

    it('store should dispatch editCriteriaFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let criteriaFamily: CriteriaFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.editCriteriaFamily(criteriaFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criteriaFamilyActions.editCriteriaFamily({ criteriaFamily }));
    });

    it('store should dispatch deleteCriteriaFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let criteriaFamily: CriteriaFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.deleteCriteriaFamily(criteriaFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criteriaFamilyActions.deleteCriteriaFamily({ criteriaFamily }));
    });

    it('store should dispatch addOutputFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let outputFamily: OutputFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.addOutputFamily(outputFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(outputFamilyActions.addOutputFamily({ outputFamily }));
    });

    it('store should dispatch editOutputFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let outputFamily: OutputFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.editOutputFamily(outputFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(outputFamilyActions.editOutputFamily({ outputFamily }));
    });

    it('store should dispatch deleteOutputFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let outputFamily: OutputFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.deleteOutputFamily(outputFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(outputFamilyActions.deleteOutputFamily({ outputFamily }));
    });

    it('store should dispatch addOutputCategory action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let outputCategory: OutputCategory = { display: 10, id: 1, label: 'test', id_output_family: 1 };
        component.addOutputCategory(outputCategory);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(outputCategoryActions.addOutputCategory({ outputCategory }));
    });

    it('store should dispatch editOutputCategory action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let outputCategory: OutputCategory = { display: 10, id: 1, label: 'test', id_output_family: 1 };
        component.editOutputCategory(outputCategory);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(outputCategoryActions.editOutputCategory({ outputCategory }));
    });

    it('store should dispatch deleteOutputCategory action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let outputCategory: OutputCategory = { display: 10, id: 1, label: 'test', id_output_family: 1 };
        component.deleteOutputCategory(outputCategory);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(outputCategoryActions.deleteOutputCategory({ outputCategory }));
    });

    it('store should dispatch addAttributeList action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.addAttributeList([]);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(attributeActions.addAttributeList({ attributeList: [] }));
    });
    it('store should dispatch addAttribute action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.addAttribute(attribute);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(attributeActions.addAttribute({ attribute }));
    });

    it('store should dispatch editAttribute action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.editAttribute(attribute);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(attributeActions.editAttribute({ attribute }));
    });

    it('store should dispatch deleteAttribute action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.deleteAttribute(attribute);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(attributeActions.deleteAttribute({ attribute }));
    });

    it('store should dispatch adminFileExplorerActions.loadFiles action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let path: string = 'test';
        component.loadRootDirectory(path);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path }));
    });

    it('store should dispatch fitsImageActions.retrieveFitsImageLimits action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let filePath: string = 'test';
        component.retrieveFitsImageLimits(filePath);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(fitsImageActions.retrieveFitsImageLimits({ filePath }));
    });

    it('store should dispatch imageActions.addImage action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let image: Image = { dec_max: 1, dec_min: 0, file_path: 'test', file_size: 10, id: 1, label: 'test', pmax: 10, pmin: 1, ra_max: 10, ra_min: 10, stretch: 'test' };
        component.addImage(image);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(imageActions.addImage({ image }));
    });

    it('store should dispatch imageActions.editImage action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let image: Image = { dec_max: 1, dec_min: 0, file_path: 'test', file_size: 10, id: 1, label: 'test', pmax: 10, pmin: 1, ra_max: 10, ra_min: 10, stretch: 'test' };
        component.editImage(image);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(imageActions.editImage({ image }));
    });

    it('store should dispatch imageActions.deleteImage action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let image: Image = { dec_max: 1, dec_min: 0, file_path: 'test', file_size: 10, id: 1, label: 'test', pmax: 10, pmin: 1, ra_max: 10, ra_min: 10, stretch: 'test' };
        component.deleteImage(image);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(imageActions.deleteImage({ image }));
    });

    it('store should dispatch fileActions.addFile action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let file: File = { file_path: 'test', file_size: 10, id: 1, label: 'test', type: 'test' }
        component.addFile(file);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(fileActions.addFile({ file }));
    });

    it('store should dispatch fileActions.editFile action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let file: File = { file_path: 'test', file_size: 10, id: 1, label: 'test', type: 'test' }
        component.editFile(file);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(fileActions.editFile({ file }));
    });

    it('store should dispatch fileActions.deleteFile action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let file: File = { file_path: 'test', file_size: 10, id: 1, label: 'test', type: 'test' }
        component.deleteFile(file);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(fileActions.deleteFile({ file }));
    });

    it('store should dispatch coneSearchConfigActions.addConeSearchConfig action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let coneSearchConfig: ConeSearchConfig = {
            id: 1,
            enabled: false,
            opened: false,
            column_dec: 10,
            column_ra: 10,
            resolver_enabled: false,
            default_ra: null,
            default_dec: null,
            default_radius: null,
            default_ra_dec_unit: 'degrees',
            plot_enabled: false
        };
        component.addConeSearchConfig(coneSearchConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchConfigActions.addConeSearchConfig({ coneSearchConfig }));
    });

    it('store should dispatch coneSearchConfigActions.editConeSearchConfig action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let coneSearchConfig: ConeSearchConfig = {
            id: 1,
            enabled: false,
            opened: false,
            column_dec: 10,
            column_ra: 10,
            resolver_enabled: false,
            default_ra: null,
            default_dec: null,
            default_radius: null,
            default_ra_dec_unit: 'degrees',
            plot_enabled: false
        };
        component.editConeSearchConfig(coneSearchConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchConfigActions.editConeSearchConfig({ coneSearchConfig }));
    });

    it('store should dispatch detailConfigActions.addDetailConfig action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let detailConfig: DetailConfig = { content: 'test', id: 1, style_sheet: 'test' };
        component.addDetailConfig(detailConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(detailConfigActions.addDetailConfig({ detailConfig }));
    });

    it('store should dispatch detailConfigActions.editDetailConfig action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let detailConfig: DetailConfig = { content: 'test', id: 1, style_sheet: 'test' };
        component.editDetailConfig(detailConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(detailConfigActions.editDetailConfig({ detailConfig }));
    });
    it('store should dispatch aliasConfigActions.addAliasConfig action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let aliasConfig: AliasConfig = { column_alias: '' , column_alias_long: '', column_name :'' , table_alias: 'test' };
        component.addAliasConfig(aliasConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(aliasConfigActions.addAliasConfig({ aliasConfig }));
    });
    it('store should dispatch aliasConfigActions.editAliasConfig action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let aliasConfig: AliasConfig = { column_alias: '' , column_alias_long: '', column_name :'' , table_alias: 'test' };
        component.editAliasConfig(aliasConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(aliasConfigActions.editAliasConfig({ aliasConfig }));
    });
});
