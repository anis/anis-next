/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { DatasetListComponent } from './dataset-list.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Dataset, DatasetFamily, Instance } from 'src/app/metamodel/models';
import * as datasetFamilyActions from 'src/app/metamodel/actions/dataset-family.actions';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';

describe('[admin][instance][dataset][containers] DatasetListComponent', () => {
    let component: DatasetListComponent;
    let fixture: ComponentFixture<DatasetListComponent>;
    let store: MockStore;
    let mockinstanceSelectorInstanceByRouteName;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [

                DatasetListComponent,
            ],
            providers: [
                provideMockStore({}),
            ],

            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
            ]
        });

        fixture = TestBed.createComponent(DatasetListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        let instance: Instance;
        mockinstanceSelectorInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, label: 'test' })
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('store should dispatch datasetFamilyActions.addDatasetFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let datasetFamily: DatasetFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.addDatasetFamily(datasetFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetFamilyActions.addDatasetFamily({ datasetFamily }));
    });

    it('store should dispatch datasetFamilyActions.editDatasetFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let datasetFamily: DatasetFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.editDatasetFamily(datasetFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetFamilyActions.editDatasetFamily({ datasetFamily }));
    });

    it('store should dispatch datasetFamilyActions.deleteDatasetFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let datasetFamily: DatasetFamily = { display: 10, id: 1, label: 'test', opened: false };
        component.deleteDatasetFamily(datasetFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetFamilyActions.deleteDatasetFamily({ datasetFamily }));
    });

    it('store should dispatch datasetActions.deleteDataset action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let dataset: Dataset = {
            display: 10,
            label: 'test',
            cone_search_config_id: 1,
            data_path: 'test',
            datatable_enabled: false,
            datatable_selectable_rows: false,
            description: 'test',
            download_ascii: false,
            download_csv: false,
            download_json: true,
            download_vo: true,
            download_fits: true,
            full_data_path: 'test',
            id_database: 10,
            id_dataset_family: 1,
            name: 'test',
            public: false,
            server_link_enabled: false,
            table_ref: 'test'
        };
        component.deleteDataset(dataset);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetActions.deleteDataset({ dataset }));
    });
});
