/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { cold, hot } from 'jasmine-marbles';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import { ConfigureDatasetTitleResolver } from './configure-dataset-title.resolver';
import { Dataset, Instance } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Type } from '@angular/core';

class MockActivatedRouteSnapshot extends ActivatedRouteSnapshot{
    constructor() {
        super()
    }
    component: any;

}
describe('[admin][instance][dataset] ConfigureDatasetTitleResolver', () => {
    let configureDatasetTitleResolver: ConfigureDatasetTitleResolver;
    let store: MockStore;
    let mockDatabaseSelectorDatasetListIsLoaded;
    let mockDatabaseSelectorDatasetByRouteName;
    let mockInstanceSelectorInstanceByRouteName;
    let dataset: Dataset;
    dataset = { ...dataset, label: 'test_dataset_label' };
    let instance: Instance;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ConfigureDatasetTitleResolver,
                provideMockStore({}),
            ]
        })

        store = TestBed.inject(MockStore);
        configureDatasetTitleResolver = TestBed.inject(ConfigureDatasetTitleResolver);
        mockDatabaseSelectorDatasetListIsLoaded = store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, false);
        mockDatabaseSelectorDatasetByRouteName = store.overrideSelector(datasetSelector.selectDatasetByRouteName, dataset);

    });

    it('should be created', () => {
        expect(configureDatasetTitleResolver).toBeTruthy();
    });

    it('should dispatch datasetActions loadDatasetList action and return datasetListIsLoaded ', () => {
        const expected = cold('a', { a: [] });
        let spy = jest.spyOn(store, 'dispatch');
        let result = hot('a', { a: configureDatasetTitleResolver.resolve(null, null) });

        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetActions.loadDatasetList());

    });
    it('should return test_instance_label - Edit dataset test_dataset_label ', () => {
        const expected = cold('a', { a: "test_instance_label - Edit dataset test_dataset_label" });
        instance = {...instance, label: 'test_instance_label'}
        mockDatabaseSelectorDatasetListIsLoaded = store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        mockInstanceSelectorInstanceByRouteName =  store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        let route: MockActivatedRouteSnapshot;
        route = { ...route, component: { name: 'test'}, firstChild: null,root: null, parent: null, children: null, paramMap: null, queryParamMap: null, pathFromRoot: null}
        let result = configureDatasetTitleResolver.resolve(route, null);
        expect(result).toBeObservable(expected);

    });
    it('should return test_instance_label - Configure dataset test_dataset_label', () => {
        const expected = cold('a', { a: 'test_instance_label - Configure dataset test_dataset_label' });
        instance = {...instance, label: 'test_instance_label'}
        mockDatabaseSelectorDatasetListIsLoaded = store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        mockInstanceSelectorInstanceByRouteName =  store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        let route: MockActivatedRouteSnapshot;
        route = { ...route, component: { name: 'ConfigureDatasetComponent'}, firstChild: null,root: null, parent: null, children: null, paramMap: null, queryParamMap: null, pathFromRoot: null}
        let result = configureDatasetTitleResolver.resolve(route, null);
        expect(result).toBeObservable(expected);

    });
});

