/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';

@Injectable({
    providedIn: 'root'
})
export class InstanceGroupTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(instanceGroupSelector.selectInstanceGroupByRouteId).pipe(
            map(instanceGroup => `Edit instance-group ${instanceGroup.role}`)
        );
    }
}
