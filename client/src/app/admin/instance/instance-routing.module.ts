/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstanceListComponent } from './containers/instance-list.component';
import { NewInstanceComponent } from './containers/new-instance.component';
import { EditInstanceComponent } from './containers/edit-instance.component';
import { ConfigureInstanceComponent } from './containers/configure-instance.component';
import { InstanceGroupListComponent } from './containers/instance-group-list.component';
import { NewInstanceGroupComponent } from './containers/new-instance-group.component';
import { EditInstanceGroupComponent } from './containers/edit-instance-group.component';
import { InstanceTitleResolver } from './instance-title.resolver';
import { InstanceGroupTitleResolver } from './instance-group-title.resolver';

const routes: Routes = [
    { path: 'instance-list', component: InstanceListComponent, title: 'Instances list' },
    { path: 'new-instance', component: NewInstanceComponent, title: 'New instance' },
    { path: 'edit-instance/:iname', component: EditInstanceComponent, title: InstanceTitleResolver },
    { path: 'instance-group', component: InstanceGroupListComponent, title: 'Instance-groups list' },
    { path: 'instance-group/new-group', component: NewInstanceGroupComponent, title: 'New instance-group' },
    { path: 'instance-group/edit-group/:id', component: EditInstanceGroupComponent, title: InstanceGroupTitleResolver },
    { path: 'configure-instance/:iname', component: ConfigureInstanceComponent, children: 
        [
            { path: '', redirectTo: 'dataset/dataset-list', pathMatch: 'full' },
            { path: 'dataset', loadChildren: () => import('./dataset/dataset.module').then(m => m.DatasetModule) },
            { path: 'dataset-group', loadChildren: () => import('./dataset-group/dataset-group.module').then(m => m.DatasetGroupModule) },
            { path: 'webpage', loadChildren: () => import('./webpage/webpage.module').then(m => m.WebpageModule) },
        ]
    },
];

/**
 * @class
 * @classdesc Instance routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InstanceRoutingModule { }

export const routedComponents = [
    InstanceListComponent,
    NewInstanceComponent,
    EditInstanceComponent,
    ConfigureInstanceComponent,
    InstanceGroupListComponent,
    NewInstanceGroupComponent,
    EditInstanceGroupComponent
];
