/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, TemplateRef, Output, EventEmitter } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { WebpageFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-edit-webpage-family',
    templateUrl: 'edit-webpage-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditWebpageFamilyComponent {
    @Input() webpageFamily: WebpageFamily;
    @Output() onSubmit: EventEmitter<WebpageFamily> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
