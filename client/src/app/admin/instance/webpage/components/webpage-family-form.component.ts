/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { WebpageFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-webpage-family-form',
    templateUrl: 'webpage-family-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageFamilyFormComponent implements OnInit {
    @Input() webpageFamily: WebpageFamily;
    @Output() onSubmit: EventEmitter<WebpageFamily> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        icon: new UntypedFormControl(null),
        display: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.webpageFamily) {
            this.form.patchValue(this.webpageFamily);
        }
    }

    submit() {
        if (this.webpageFamily) {
            this.onSubmit.emit({
                ...this.webpageFamily,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
