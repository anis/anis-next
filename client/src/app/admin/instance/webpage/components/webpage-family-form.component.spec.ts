/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WebpageFamily } from 'src/app/metamodel/models';
import { WebpageFamilyFormComponent } from './webpage-family-form.component';

describe('[admin][instance][webpage][components] WebpageFamilyFormComponent', () => {
    let component: WebpageFamilyFormComponent;
    let fixture: ComponentFixture<WebpageFamilyFormComponent>;
    let spy;
    let webpageFamilly: WebpageFamily = { display: 10, icon: 'test', id: 0, label: 'webpageFamilly test label' };
    let form = new UntypedFormGroup({
        label: new UntypedFormControl('test', [Validators.required]),
        icon: new UntypedFormControl(null),
        display: new UntypedFormControl(10, [Validators.required])
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageFamilyFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ]
        });
        fixture = TestBed.createComponent(WebpageFamilyFormComponent);
        component = fixture.componentInstance;
        component.webpageFamily = webpageFamilly;
        component.form = form;
        spy = jest.spyOn(component.onSubmit, 'emit');
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('it should emit webpageFamily and form.value', () => {
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...webpageFamilly, ...form.value })
    });

    it('it should emit  form.value only', () => {
        component.webpageFamily = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...form.value })
    });
});
