/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WebpageCardComponent } from './webpage-card.component';

@Component({
    selector: 'app-delete-btn',
})
export class DeleteBtnComponent { }

describe('[admin][instance][webpage][components]WebpageCardComponent ', () => {
    let component: WebpageCardComponent;
    let fixture: ComponentFixture<WebpageCardComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageCardComponent,
                DeleteBtnComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(WebpageCardComponent);
        component = fixture.componentInstance;
        component.webpage = { icon: '', content: '', display: 10, id: 0, id_webpage_family: 0, label: '', title: '', name: '', style_sheet: '', type: "", url: "" };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
