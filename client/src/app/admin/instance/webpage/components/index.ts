/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { WebpageButtonsComponent } from './webpage-buttons.component';
import { WebpageFamilyCardComponent } from './webpage-family-card.component';
import { EditWebpageFamilyComponent } from './edit-webpage-family.component';
import { WebpageFamilyFormComponent } from './webpage-family-form.component';
import { WebpageCardComponent } from './webpage-card.component';
import { WebpageFormComponent } from './webpage-form.component';

export const dummiesComponents = [
    WebpageButtonsComponent,
    WebpageFamilyCardComponent,
    EditWebpageFamilyComponent,
    WebpageFamilyFormComponent,
    WebpageCardComponent,
    WebpageFormComponent
];
