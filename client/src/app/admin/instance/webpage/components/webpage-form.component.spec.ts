/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Webpage } from 'src/app/metamodel/models';
import { WebpageFormComponent } from './webpage-form.component';

@Component({ selector: 'app-webpage-form-content' })
class WebpageFormContentComponent { }

describe('[admin][instance][webpage][components]WebpageFormComponent ', () => {
    let component: WebpageFormComponent;
    let fixture: ComponentFixture<WebpageFormComponent>;
    let spy;
    let form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        icon: new UntypedFormControl(null),
        display: new UntypedFormControl('', [Validators.required]),
        title: new UntypedFormControl('', [Validators.required]),
        content: new UntypedFormControl('', [Validators.required]),
        type: new UntypedFormControl('', [Validators.required]),
        style_sheet: new UntypedFormControl(null),
        url: new UntypedFormControl('', [Validators.required]),
        id_webpage_family: new UntypedFormControl('webpage', [Validators.required])

    });
    let webpage: Webpage = {
        icon: '', content: '', display: 10, id: 0, id_webpage_family: 0, label: '', title: '', name: '', style_sheet: '', type: '', url: ''
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageFormComponent,
                WebpageFormContentComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(WebpageFormComponent);
        component = fixture.componentInstance;
        spy = jest.spyOn(component.onSubmit, 'emit');
        component.webpage = webpage;
        component.form = form;
        component.idWebpageFamily = 1;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should emit with webpage and form.getRawValue()', () => {
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.webpage, ...component.form.getRawValue() });
    })

    it('should emit with webpage only', () => {
        component.webpage = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    })

    it('should set id_webpage_family form value with idWebpageFamily ', () => {
        expect(component.form.controls['id_webpage_family'].value).toEqual(component.idWebpageFamily);
    })
});
