/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { WebpageFamily, Webpage } from 'src/app/metamodel/models';

@Component({
    selector: 'app-webpage-family-card',
    templateUrl: 'webpage-family-card.component.html',
    styleUrls: [ 'webpage-family-card.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageFamilyCardComponent {
    @Input() webpageFamily: WebpageFamily;
    @Input() webpageList: Webpage[];
    @Output() editWebpageFamily: EventEmitter<WebpageFamily> = new EventEmitter();
    @Output() deleteWebpageFamily: EventEmitter<WebpageFamily> = new EventEmitter();
    @Output() deleteWebpage: EventEmitter<Webpage> = new EventEmitter();

    nbWebpagesByWebpageFamily(): number {
        return this.webpageList.filter(webpage => webpage.id_webpage_family === this.webpageFamily.id).length;
    }
}
