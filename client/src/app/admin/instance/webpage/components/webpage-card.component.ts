/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Webpage } from 'src/app/metamodel/models';

@Component({
    selector: 'app-webpage-card',
    templateUrl: 'webpage-card.component.html',
    styleUrls: [ 'webpage-card.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageCardComponent {
    @Input() webpage: Webpage;
    @Output() deleteWebpage: EventEmitter<Webpage> = new EventEmitter();
}
