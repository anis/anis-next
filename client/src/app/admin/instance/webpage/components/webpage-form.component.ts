/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { Webpage, WebpageFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-webpage-form',
    templateUrl: 'webpage-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageFormComponent implements OnInit {
    @Input() webpage: Webpage;
    @Input() webpageFamilyList: WebpageFamily[];
    @Input() idWebpageFamily: number;
    @Output() onSubmit: EventEmitter<Webpage> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        icon: new UntypedFormControl(null),
        display: new UntypedFormControl('', [Validators.required]),
        title: new UntypedFormControl(''),
        content: new UntypedFormControl(''),
        style_sheet: new UntypedFormControl(null),
        type: new UntypedFormControl('webpage', [Validators.required]),
        url: new UntypedFormControl('', [Validators.pattern('https?://.+')]),
        id_webpage_family: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {

        if (this.webpage) {
            this.form.patchValue(this.webpage);
        }
        if (this.idWebpageFamily) {
            this.form.controls.id_webpage_family.setValue(this.idWebpageFamily);
        }
      this.manageFormValidators();
    }

    submit() {
        if (this.webpage) {
            this.onSubmit.emit({
                ...this.webpage,
                ...this.form.getRawValue()
            });
            this.form.markAsPristine();
        } else {
            this.onSubmit.emit(this.form.getRawValue());
        }
    }
    manageFormValidators() {
        if (this.form.controls.type.value === "ext_link") {
            this.form.controls.url.addValidators(Validators.required);
            this.form.controls.title.removeValidators(Validators.required);
            this.form.controls.content.removeValidators(Validators.required);
            this.form.controls.content.updateValueAndValidity();
            this.form.controls.title.updateValueAndValidity();
            this.form.controls.url.updateValueAndValidity();

        } else {
            this.form.controls.url.removeValidators(Validators.required);
            this.form.controls.title.addValidators(Validators.required);
            this.form.controls.content.addValidators(Validators.required);
            this.form.controls.content.updateValueAndValidity();
            this.form.controls.title.updateValueAndValidity();
            this.form.controls.url.updateValueAndValidity();
        }
    }
}
