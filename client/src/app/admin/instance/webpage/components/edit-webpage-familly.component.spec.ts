/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { EditWebpageFamilyComponent } from './edit-webpage-family.component';

describe('[admin][instance][webpage][components]EditWebpageFamilyComponent ', () => {
    let component: EditWebpageFamilyComponent;
    let fixture: ComponentFixture<EditWebpageFamilyComponent>;

    const modalServiceStub = {
        show: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditWebpageFamilyComponent,
            ],
            providers: [
                BsModalRef,
                { provide: BsModalService, useValue: modalServiceStub }
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(EditWebpageFamilyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call modalService.show', () => {
        let template: TemplateRef<any> = null;
        const spy = jest.spyOn(modalServiceStub, 'show');
        component.openModal(template);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(template);
    })
});
