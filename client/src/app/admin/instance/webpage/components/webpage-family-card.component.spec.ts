/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { Webpage } from 'src/app/metamodel/models';
import { WebpageFamilyCardComponent } from './webpage-family-card.component';

// mock webpageListByFamilyPipe
@Pipe({ name: 'webpageListByFamily' })
class MockPipe implements PipeTransform {
    transform(webpageList: Webpage[], idWebpageFamily: number): Webpage[] {
        return webpageList.filter(webpage => webpage.id_webpage_family === idWebpageFamily);
    }

}

class DummyComponent { }

@Component({
    selector: 'app-edit-webpage'
})
class EditWebpageComponent { }

@Component({
    selector: 'app-delete-btn'
})
class DeleteBtnComponenent { }

@Component({
    selector: 'app-webpage-card'
})
class WebPageComponent { }

describe('[admin][instance][webpage][components] WebpageFamilyCardComponent ', () => {
    let component: WebpageFamilyCardComponent;
    let fixture: ComponentFixture<WebpageFamilyCardComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageFamilyCardComponent,
                MockPipe,
                EditWebpageComponent,
                DeleteBtnComponenent,
                WebPageComponent
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule.withRoutes([
                    { path: 'test', component: DummyComponent }
                ])
            ],
        });
        fixture = TestBed.createComponent(WebpageFamilyCardComponent);
        component = fixture.componentInstance;
        component.webpageFamily = { display: 10, icon: 'test', id: 0, label: 'webpageFamilly test label' };
        component.webpageList = [
            { icon: 'test1', content: 'test1', display: 10, id: 0, id_webpage_family: 0, label: 'test1', title: 'test-title1', name: 'test1', style_sheet: '', type: "", url: "" },
            { icon: 'test2', content: 'test2', display: 10, id: 0, id_webpage_family: 0, label: 'test2', title: 'test-title2', name: 'test2', style_sheet: '', type: "", url: "" },
            { icon: 'test3', content: 'test3', display: 10, id: 0, id_webpage_family: 1, label: 'test3', title: 'test-title3', name: 'test3', style_sheet: '', type: "", url: "" }
        ];
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it(' nbWebpagesByWebpageFamily should return 2', () => {
        expect(component.nbWebpagesByWebpageFamily()).toEqual(2);
    })
});
