/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Instance } from 'src/app/metamodel/models';
import { WebpageTitleResolver } from './webpage-title.resolver';
import { WebpageListComponent } from './containers/webpage-list.component';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import { Component, Type } from '@angular/core';

describe('[Webpage] WebpageTitleResolver', () => {
    class MockActivatedRouteSnapshot extends ActivatedRouteSnapshot { }
    @Component({})
    class StubComponent { }
    let webpageTitleResolver: WebpageTitleResolver;
    let store: MockStore;
    let component;
    let instance: Instance;
    let mockWebPageSelectorInstanceByRouteName;
    let mockInstanceSelectorSelectInstanceListIsLoaded;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [WebpageListComponent],
            providers: [
                WebpageTitleResolver,
                provideMockStore({}),
            ]
        })
        instance = { ...instance, label: 'test_label', }
        store = TestBed.inject(MockStore);
        webpageTitleResolver = TestBed.inject(WebpageTitleResolver);
        mockWebPageSelectorInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
    });

    it('should be created', () => {
        expect(webpageTitleResolver).toBeTruthy();
    });
    it('resolve() should dispatch loadInstanceList action and return []', () => {
        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, false);
        let spy = jest.spyOn(store, 'dispatch');
        let result = hot('a', { a: webpageTitleResolver.resolve(null, null) });
        let expected = cold('a', { a: [] });
        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledWith(instanceActions.loadInstanceList());

    });
    it('resolve() should return "test_label - Webpages list"', () => {
        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        let route = new MockActivatedRouteSnapshot();
        route.component = WebpageListComponent;

        let result = webpageTitleResolver.resolve(route, null);
        const expected = cold('a', { a: 'test_label - Webpages list' });
        expect(result).toBeObservable(expected);
    });
    it('resolve() should return "test_label - Webpages list"', () => {
        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        let route = new MockActivatedRouteSnapshot();
        route.component = StubComponent;

        let result = webpageTitleResolver.resolve(route, null);
        const expected = cold('a', { a: 'test_label - New webpage' });
        expect(result).toBeObservable(expected);
    });

});
