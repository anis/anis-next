/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WebpageListComponent } from './containers/webpage-list.component';
import { NewWebpageComponent } from './containers/new-webpage.component';
import { EditWebpageComponent } from './containers/edit-webpage.component';
import { WebpageTitleResolver } from './webpage-title.resolver';
import { EditWebpageTitleResolver } from './edit-webpage-title.resolver';

const routes: Routes = [
    { path: '', component: WebpageListComponent, title: WebpageTitleResolver },
    { path: 'new-webpage', component: NewWebpageComponent, title: WebpageTitleResolver },
    { path: 'edit-webpage/:id', component: EditWebpageComponent, title: EditWebpageTitleResolver }
];

/**
 * @class
 * @classdesc Dataset routing module.
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WebpageRoutingModule { }

export const routedComponents = [
    WebpageListComponent,
    NewWebpageComponent,
    EditWebpageComponent
];
