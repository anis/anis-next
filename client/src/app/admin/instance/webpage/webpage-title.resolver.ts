/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { map, switchMap, skipWhile } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Injectable({
    providedIn: 'root'
})
export class WebpageTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(instanceSelector.selectInstanceListIsLoaded).pipe(
            map(instanceListIsLoaded => {
                if (!instanceListIsLoaded) {
                    this.store.dispatch(instanceActions.loadInstanceList());
                }
                return instanceListIsLoaded;
            }),
            skipWhile(instanceListIsLoaded => !instanceListIsLoaded),
            switchMap(() => {
                return this.store.select(instanceSelector.selectInstanceByRouteName).pipe(
                    map(instance => {
                        if (route.component.name === 'WebpageListComponent') {
                            return `${instance.label} - Webpages list`;
                        } else {
                            return `${instance.label} - New webpage`;
                        }
                    })
                );
            })
        );
    }
}
