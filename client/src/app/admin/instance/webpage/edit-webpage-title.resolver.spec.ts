/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';

import { EditWebpageTitleResolver } from './edit-webpage-title.resolver';
import * as webpageSelector from 'src/app/metamodel/selectors/webpage.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Instance, Webpage } from 'src/app/metamodel/models';

describe('[Webpage] EditWebpageTitleResolver', () => {
    let editWebpageTitleResolver: EditWebpageTitleResolver;
    let store: MockStore;
    let mockWebPageSelectorWebPageListIsLoaded;
    let mockWebPageSelectorInstanceByRouteName;
    let instanceSelectorSelectWebpageByRouteId

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                EditWebpageTitleResolver,
                provideMockStore({}),
            ]
        })
        let webpage: Webpage = { icon: '', content: '', display: 10, id: 0, id_webpage_family: 0, label: 'webpage_test_label', title: '', name: '', style_sheet: '', type: "", url: "" };
        let instance: Instance;
        instance = { ...instance, label: 'instance_test_label' }

        store = TestBed.inject(MockStore);

        editWebpageTitleResolver = TestBed.inject(EditWebpageTitleResolver);
        mockWebPageSelectorWebPageListIsLoaded = store.overrideSelector(webpageSelector.selectWebpageListIsLoaded, false);
        mockWebPageSelectorInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        instanceSelectorSelectWebpageByRouteId = store.overrideSelector(webpageSelector.selectWebpageByRouteId, webpage);

    });

    it('should be created', () => {
        expect(editWebpageTitleResolver).toBeTruthy();
    });

    it('shou dispatch databaseActions loadDatabaseList action and return databaseListIsLoaded ', () => {
        const expected = cold('a', { a: [] });
        let spy = jest.spyOn(store, 'dispatch');
        let result = hot('a', { a: editWebpageTitleResolver.resolve(null, null) });

        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should return label ', () => {
        mockWebPageSelectorWebPageListIsLoaded = store.overrideSelector(webpageSelector.selectWebpageListIsLoaded, true);

        let spy = jest.spyOn(store, 'dispatch');
        let result = editWebpageTitleResolver.resolve(null, null);
        const expected = cold('a', { a: "instance_test_label - Edit webpage webpage_test_label" });
        expect(result).toBeObservable(expected);
    });
});
