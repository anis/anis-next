/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable, combineLatest } from 'rxjs';
import { map, switchMap, skipWhile } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as webpageSelector from 'src/app/metamodel/selectors/webpage.selector';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';

@Injectable({
    providedIn: 'root'
})
export class EditWebpageTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(webpageSelector.selectWebpageListIsLoaded).pipe(
            map(webpageListIsLoaded => {
                if (!webpageListIsLoaded) {
                    this.store.dispatch(webpageActions.loadWebpageList());
                }
                return webpageListIsLoaded;
            }),
            skipWhile(webpageListIsLoaded => !webpageListIsLoaded),
            switchMap(() => {
                return combineLatest([
                    this.store.pipe(select(instanceSelector.selectInstanceByRouteName)),
                    this.store.pipe(select(webpageSelector.selectWebpageByRouteId))
                ]).pipe(
                    map(([instance, webpage]) => `${instance.label} - Edit webpage ${webpage.label}`)
                );
            })
        );
    }
}
