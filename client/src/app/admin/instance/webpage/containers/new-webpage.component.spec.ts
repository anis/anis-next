/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { firstValueFrom, of } from 'rxjs';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Webpage } from 'src/app/metamodel/models';
import { NewWebpageComponent } from './new-webpage.component';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';

describe('[admin][instance][webpage][containers] NewWebpageComponent ', () => {
    let component: NewWebpageComponent;
    let fixture: ComponentFixture<NewWebpageComponent>;
    let store: MockStore;
    let spy;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewWebpageComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        queryParamMap: of({ get: jest.fn().mockImplementation(() => 1) }),
                    },
                },
                provideMockStore({}),
            ]
        });
        fixture = TestBed.createComponent(NewWebpageComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        spy = jest.spyOn(store, 'dispatch');
        fixture.detectChanges();
    });

    it('should create the component', async () => {
        expect(component).toBeTruthy();
        expect(await firstValueFrom(component.idWebpageFamily)).toEqual(1);
    });

    it('dispatch webpageActions.addWebPage with the new webpage values', () => {
        let webpage: Webpage = { icon: '', content: '', display: 10, id: 0, id_webpage_family: 0, label: '', title: '', name: '', style_sheet: '', type: "", url: "" };
        component.addNewWebpage(webpage);
        expect(spy).toHaveBeenCalledTimes(2);
        expect(spy).toHaveBeenCalledWith(webpageActions.addWebpage({ webpage }));
    });

});
