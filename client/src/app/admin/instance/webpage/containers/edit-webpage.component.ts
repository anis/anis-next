/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { WebpageFamily, Webpage } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';
import * as webpageSelector from 'src/app/metamodel/selectors/webpage.selector';
import * as webpageFamilySelector from 'src/app/metamodel/selectors/webpage-family.selector';
import * as monacoEditorActions from 'src/app/admin/store/actions/monaco-editor.actions';
import * as monacoEditorSelector from 'src/app/admin/store/selectors/monaco-editor.selector';

@Component({
    selector: 'app-edit-webpage',
    templateUrl: 'edit-webpage.component.html'
})
export class EditWebpageComponent implements OnInit {
    public instanceName: Observable<string>;
    public webpageListIsLoading: Observable<boolean>;
    public webpageListIsLoaded: Observable<boolean>;
    public webpage: Observable<Webpage>;
    public webpageFamilyListIsLoading: Observable<boolean>;
    public webpageFamilyListIsLoaded: Observable<boolean>;
    public webpageFamilyList: Observable<WebpageFamily[]>;
    public monacoEditorIsLoading: Observable<boolean>;
    public monacoEditorIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.instanceName = this.store.select(instanceSelector.selectInstanceNameByRoute);
        this.webpageListIsLoading = store.select(webpageSelector.selectWebpageListIsLoading);
        this.webpageListIsLoaded = store.select(webpageSelector.selectWebpageListIsLoaded);
        this.webpage = this.store.select(webpageSelector.selectWebpageByRouteId);
        this.webpageFamilyListIsLoading = store.select(webpageFamilySelector.selectWebpageFamilyListIsLoading);
        this.webpageFamilyListIsLoaded = store.select(webpageFamilySelector.selectWebpageFamilyListIsLoaded);
        this.webpageFamilyList = store.select(webpageFamilySelector.selectAllWebpageFamilies);
        this.monacoEditorIsLoading = store.select(monacoEditorSelector.selectMonacoEditorIsLoading);
        this.monacoEditorIsLoaded = store.select(monacoEditorSelector.selectMonacoEditorIsLoaded);
    }

    ngOnInit(): void {
        Promise.resolve(null).then(() => this.store.dispatch(monacoEditorActions.loadMonacoEditor()));
    }

    editWebpage(webpage: Webpage) {
        this.store.dispatch(webpageActions.editWebpage({ webpage }));
    }
}
