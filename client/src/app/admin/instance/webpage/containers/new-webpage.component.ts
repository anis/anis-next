/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { WebpageFamily, Webpage } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as webpageFamilySelector from 'src/app/metamodel/selectors/webpage-family.selector';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';
import * as monacoEditorActions from 'src/app/admin/store/actions/monaco-editor.actions';
import * as monacoEditorSelector from 'src/app/admin/store/selectors/monaco-editor.selector';

@Component({
    selector: 'app-new-webpage',
    templateUrl: 'new-webpage.component.html'
})
export class NewWebpageComponent implements OnInit {
    public instanceName: Observable<string>;
    public webpageFamilyListIsLoading: Observable<boolean>;
    public webpageFamilyListIsLoaded: Observable<boolean>;
    public webpageFamilyList: Observable<WebpageFamily[]>;
    public idWebpageFamily: Observable<number>;
    public monacoEditorIsLoading: Observable<boolean>;
    public monacoEditorIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>, private route: ActivatedRoute) {
        this.instanceName = this.store.select(instanceSelector.selectInstanceNameByRoute);
        this.webpageFamilyListIsLoading = store.select(webpageFamilySelector.selectWebpageFamilyListIsLoading);
        this.webpageFamilyListIsLoaded = store.select(webpageFamilySelector.selectWebpageFamilyListIsLoaded);
        this.webpageFamilyList = store.select(webpageFamilySelector.selectAllWebpageFamilies);
        this.monacoEditorIsLoading = store.select(monacoEditorSelector.selectMonacoEditorIsLoading);
        this.monacoEditorIsLoaded = store.select(monacoEditorSelector.selectMonacoEditorIsLoaded);
    }

    ngOnInit() {
        this.idWebpageFamily = this.route.queryParamMap.pipe(
            map(params => +params.get('id_webpage_family'))
        );
        Promise.resolve(null).then(() => this.store.dispatch(monacoEditorActions.loadMonacoEditor()));
    }

    addNewWebpage(webpage: Webpage) {
        this.store.dispatch(webpageActions.addWebpage({ webpage }));
    }
}
