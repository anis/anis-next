/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Webpage } from 'src/app/metamodel/models';
import { EditWebpageComponent } from './edit-webpage.component';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';

describe('[admin][instance][webpage][containers] EditWebpageComponent ', () => {
    let component: EditWebpageComponent;
    let fixture: ComponentFixture<EditWebpageComponent>;
    let store: MockStore;
    let spy;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditWebpageComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
            providers: [
                provideMockStore({}),
            ]
        });
        fixture = TestBed.createComponent(EditWebpageComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        spy = jest.spyOn(store, 'dispatch');
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('dispatch webpageActions.editWebPage with the new webpage values', () => {
        let webpage: Webpage = { icon: '', content: '', display: 10, id: 0, id_webpage_family: 0, label: '', title: '', name: '', style_sheet: '', type: "", url: "" };
        component.editWebpage(webpage);
        expect(spy).toHaveBeenCalledTimes(2);
        expect(spy).toHaveBeenCalledWith(webpageActions.editWebpage({ webpage }));
    });
});
