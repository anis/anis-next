/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { WebpageListComponent } from './webpage-list.component';
import { Webpage, WebpageFamily } from 'src/app/metamodel/models';
import * as webpageFamilyActions from 'src/app/metamodel/actions/webpage-family.actions';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';

@Component({
    selector: 'app-webpage-buttons',
})
class WebpageButtonsComponent { }

@Component({
    selector: 'app-webpage-card'
})
class WebpageCardComponent { }

class DummyComponent { }

describe('[admin][instance][webpage][containers] WebpageListComponent ', () => {
    let component: WebpageListComponent;
    let fixture: ComponentFixture<WebpageListComponent>;
    let store: MockStore;
    let spy;
    let webpageFamily: WebpageFamily = { icon: 'test', id: 0, label: 'test', display: 10};

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageListComponent,
                WebpageButtonsComponent,
                WebpageCardComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                RouterTestingModule.withRoutes([
                    { path: 'settings/:collection/edit/:item', component: DummyComponent }
                ])
            ],
            providers: [
                provideMockStore({}),
            ]
        });
        fixture = TestBed.createComponent(WebpageListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        spy = jest.spyOn(store, 'dispatch');
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('dispatch webpageFamilyActions.addWebpageFamily with the new webpage values', () => {
        component.addWebpageFamily(webpageFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(webpageFamilyActions.addWebpageFamily({webpageFamily}));
    });

    it('dispatch webpageFamilyActions.editWebpageFamily with the new webpage values', () => {
        component.editWebpageFamily(webpageFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(webpageFamilyActions.editWebpageFamily({webpageFamily}));
    });

    it('dispatch webpageFamilyActions.deleteWebpageFamily', () => {
      
        component.deleteWebpageFamily(webpageFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(webpageFamilyActions.deleteWebpageFamily({webpageFamily}));
    });

    it('dispatch webpageFamilyActions.deleteWebpage', () => {
        let webpage: Webpage = {
            icon: '', content: '', display: 10, id: 0, id_webpage_family: 0, label: '', title: '', name: '', style_sheet: '',
            type: '',
            url: ''
        };
        component.deleteWebpage(webpage);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(webpageActions.deleteWebpage({webpage}));
    });
});
