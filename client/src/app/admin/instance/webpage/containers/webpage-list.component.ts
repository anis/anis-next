/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { WebpageFamily, Webpage } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as webpageFamilyActions from 'src/app/metamodel/actions/webpage-family.actions';
import * as webpageFamilySelector from 'src/app/metamodel/selectors/webpage-family.selector';
import * as webpageActions from 'src/app/metamodel/actions/webpage.actions';
import * as webpageSelector from 'src/app/metamodel/selectors/webpage.selector';

@Component({
    selector: 'app-webpage-list',
    templateUrl: 'webpage-list.component.html',
    styleUrls: ['webpage-list.component.scss']
})
export class WebpageListComponent {
    public instanceName: Observable<string>;
    public webpageFamilyListIsLoading: Observable<boolean>;
    public webpageFamilyListIsLoaded: Observable<boolean>;
    public webpageFamilyList: Observable<WebpageFamily[]>;
    public webpageListIsLoading: Observable<boolean>;
    public webpageListIsLoaded: Observable<boolean>;
    public webpageList: Observable<Webpage[]>;

    constructor(private store: Store<{ }>) {
        this.instanceName = this.store.select(instanceSelector.selectInstanceNameByRoute);
        this.webpageFamilyListIsLoading = store.select(webpageFamilySelector.selectWebpageFamilyListIsLoading);
        this.webpageFamilyListIsLoaded = store.select(webpageFamilySelector.selectWebpageFamilyListIsLoaded);
        this.webpageFamilyList = store.select(webpageFamilySelector.selectAllWebpageFamilies);
        this.webpageListIsLoading = store.select(webpageSelector.selectWebpageListIsLoading);
        this.webpageListIsLoaded = store.select(webpageSelector.selectWebpageListIsLoaded);
        this.webpageList = store.select(webpageSelector.selectAllWebpages);
    }

    addWebpageFamily(webpageFamily: WebpageFamily) {
        this.store.dispatch(webpageFamilyActions.addWebpageFamily({ webpageFamily }));
    }

    editWebpageFamily(webpageFamily: WebpageFamily) {
        this.store.dispatch(webpageFamilyActions.editWebpageFamily({ webpageFamily }));
    }

    deleteWebpageFamily(webpageFamily: WebpageFamily) {
        this.store.dispatch(webpageFamilyActions.deleteWebpageFamily({ webpageFamily }));
    }

    deleteWebpage(webpage: Webpage) {
        this.store.dispatch(webpageActions.deleteWebpage({ webpage }));
    }
}
