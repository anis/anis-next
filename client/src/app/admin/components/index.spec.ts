/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { dummiesComponents } from './index';

describe('[admin][components] index', () => {
    it('Test admin  index components', () => {
        expect(dummiesComponents.length).toEqual(1);
    });
});

