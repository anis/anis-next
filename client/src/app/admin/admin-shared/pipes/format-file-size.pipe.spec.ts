import { FormatFileSizePipe } from "./format-file-size.pipe";

describe('FormatFileSizePipe', () => {
    const formatFileSizePipe = new FormatFileSizePipe();
    it('transforms "1024" to "1 kb"', () => {
        expect(formatFileSizePipe.transform(1024, false)).toEqual('1 KB');
    });
    it('transforms "1048576" to 1 Megabytes', () => {
        expect(formatFileSizePipe.transform(1024 * 1024, true)).toEqual('1 Megabytes');
    });

});