/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AppConfigService } from "src/app/app-config.service";
import { MonacoEditorService } from "./monaco-editor.service";

describe('[admin][admin-shared][services]MonacoEditorService', () => {
    class mockAppConfigService extends AppConfigService { }

    let config = new mockAppConfigService();
    let service: MonacoEditorService;
    beforeEach(() => {
        service = new MonacoEditorService(config);
    });
    it('should create service', () => {
        expect(service).toBeTruthy();
    });
    it('load should should return undefined', () => {
        service.loading = true;
        expect(service.load()).toBe(undefined);
    });
    it('load should should return undefined', () => {
        let spy = jest.spyOn((service as any), 'finishLoading');
        service.loading = false;
        (window as any).monaco = new Object();
        service.load();
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('load should should return undefined', () => {
        let spy = jest.spyOn(document, 'createElement');
        service.loading = false;
        (window as any).require = null;
        (window as any).monaco = undefined;
        service.load();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('script');
    });
    it('should call onGotAmdLoader()', () => {
        (window as any).require = jest.fn();
        (window as any).require.config = jest.fn();
        let spyOnRequire = jest.spyOn((window as any), 'require');
        let spyOnConfig = jest.spyOn((window as any).require, 'config');
        (window as any).monaco = undefined;
        service.load()
        expect(spyOnRequire).toHaveBeenCalledTimes(1);
        expect(spyOnConfig).toHaveBeenCalledTimes(1);

    });

});