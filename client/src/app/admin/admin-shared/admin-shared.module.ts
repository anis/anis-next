/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { adminSharedComponents } from './components';
import { adminPipes } from './pipes';
import { adminServices } from './services';

/**
 * @class
 * @classdesc Home module.
 */
@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        adminSharedComponents,
        adminPipes
    ],
    providers: [
        adminServices
    ],
    exports: [
        adminSharedComponents,
        adminPipes
    ]
})
export class AdminSharedModule { }
