import { Component, Input, Output, AfterViewInit, ViewChild, ElementRef, EventEmitter, OnDestroy } from '@angular/core';

declare var emmetMonaco: any;

@Component({
    selector: 'app-monaco-editor',
    templateUrl: './monaco-editor.component.html',
    styleUrls: [ './monaco-editor.component.scss' ]
})
export class MonacoEditorComponent implements AfterViewInit, OnDestroy {
    @Input() value: string;
    @Input() codeType: string;
    @Output() onChange: EventEmitter<string> = new EventEmitter();

    public editor: any;
    @ViewChild('editorContainer', { static: true }) _editorContainer: ElementRef;

    ngAfterViewInit() {
        this.initMonaco();
    }

    private initMonaco(): void {
        const monaco = ((<any>window)).monaco;

        if (this.codeType === 'html') {
            emmetMonaco.emmetHTML(monaco);
        }
        this.editor = monaco.editor.create(
            this._editorContainer.nativeElement,
            {
                theme: 'vs-dark',
                language: this.codeType
            }
        );
        this.editor.setValue(this.value);
        this.editor.onDidChangeModelContent((e: any) => {
            const value = this.editor.getValue();

            this.onChange.emit(value);
        });
    }

    ngOnDestroy(): void {
        this.editor.dispose();
        this.editor = undefined;
    }
}
