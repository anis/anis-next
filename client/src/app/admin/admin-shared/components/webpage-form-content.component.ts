/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
    selector: 'app-webpage-form-content',
    templateUrl: 'webpage-form-content.component.html',
    styleUrls: ['webpage-form-content.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageFormContentComponent {
    @Input() form: UntypedFormGroup;
    @Input() controlName: string;
    @Input() controlLabel: string;
    @Input() codeType: string;

    getValue() {
        return this.form.value[this.controlName];
    }

    onChange(value: string) {
        this.form.controls[this.controlName].setValue(value);
        this.form.controls[this.controlName].markAsDirty();
    }
}
