/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WebpageFormContentComponent } from './webpage-form-content.component';

describe('[admin][admin-shared][components] WebpageFormContentComponent', () => {
    let component: WebpageFormContentComponent;
    let fixture: ComponentFixture<WebpageFormContentComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageFormContentComponent,
            ],

            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(WebpageFormContentComponent);
        component = fixture.componentInstance;
        component.controlName = 'test';
        component.form = new FormGroup({
            test: new FormControl('test')
        });
        fixture.detectChanges();
    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('onChange(value: string) should set test value to test1 and call markAsDirty()', () => {
        let spy = jest.spyOn(component.form, 'markAsDirty');
        expect(component.form.controls.test.value).not.toEqual('test1');
        component.onChange('test1');
        expect(component.form.controls.test.value).toEqual('test1');
        expect(spy).toHaveBeenCalledTimes(1);

    });
})
