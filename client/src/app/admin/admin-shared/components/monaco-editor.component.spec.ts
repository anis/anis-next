/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MonacoEditorComponent } from "./monaco-editor.component";

describe('[admin][admin-shared][component] MonacoEditorComponent', () => {
    let component: MonacoEditorComponent;
    let fixture: ComponentFixture<MonacoEditorComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MonacoEditorComponent]
        })
        fixture = TestBed.createComponent(MonacoEditorComponent);
        component = fixture.componentInstance;
        component.editor = { dispose: jest.fn() }

    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('ngAfterViewInit should call initMonaco', () => {
        let spy = jest.spyOn((component as any), 'initMonaco').mockImplementation(() => { });
        component.ngAfterViewInit();
        expect(spy).toHaveBeenCalledTimes(1);
    })

});