/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-delete-btn',
    templateUrl: 'delete-btn.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteBtnComponent {
    @Input() type: string;
    @Input() label: string;
    @Input() disabled: boolean = false;
    @Output() confirm: EventEmitter<{}> = new EventEmitter();
    @Output() abort: EventEmitter<{}> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
