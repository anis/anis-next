/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, TemplateRef, EventEmitter, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { FileInfo } from 'src/app/admin/store/models';

@Component({
    selector: 'app-path-select-form-control',
    templateUrl: 'path-select-form-control.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PathSelectFormControlComponent implements OnChanges {
    @Input() form: UntypedFormGroup;
    @Input() disabled: boolean = false;
    @Input() controlName: string;
    @Input() controlLabel: string;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Input() selectType: string;
    @Output() loadDirectory: EventEmitter<string> = new EventEmitter();
    @Output() select: EventEmitter<FileInfo> = new EventEmitter();

    modalRef: BsModalRef;
    fileExplorerPath = '';
    fileSelected = null;
    fileInfoSelected = null;

    constructor(private modalService: BsModalService) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.disabled && changes.disabled.currentValue) {
            this.form.controls[this.controlName].disable();
        }

        if (changes.disabled && !changes.disabled.currentValue) {
            this.form.controls[this.controlName].enable();
        }
    }

    openModal(template: TemplateRef<any>) {
        const lastIndexOf = this.form.controls[this.controlName].value.lastIndexOf("/");
        this.fileExplorerPath = this.form.controls[this.controlName].value.substr(0, lastIndexOf);
        if (!this.fileExplorerPath) {
            this.fileExplorerPath = '';
        }
        this.modalRef = this.modalService.show(template);
        this.loadDirectory.emit(this.fileExplorerPath);
    }

    click(fileInfo: FileInfo): void {
        if (fileInfo.name === '.') {
            return;
        }

        if (fileInfo.type === 'file') {
            this.fileSelected = this.buildFilePath(fileInfo);
            this.fileInfoSelected = fileInfo;
            return;
        }

        if (fileInfo.name === '..') {
            this.fileExplorerPath = this.fileExplorerPath.substr(0, this.fileExplorerPath.lastIndexOf("/"));
        } else {
            this.fileExplorerPath += `/${fileInfo.name}`;
        }
        this.loadDirectory.emit(this.fileExplorerPath);
    }

    buildFilePath(fileInfo: FileInfo) {
        let fullPath = '';
        if (this.fileExplorerPath !== '') {
            fullPath += `${this.fileExplorerPath}/`;
        } else {
            fullPath += '/';
        }
        return `${fullPath}${fileInfo.name}`;
    }

    checkFileSelected(fileInfo: FileInfo) {
        return this.buildFilePath(fileInfo) === this.fileSelected && this.selectType === 'file';
    }

    checkPointer(fileInfo: FileInfo) {
        if (this.selectType === 'file' && fileInfo.name !== '.') {
            return true;
        } else if (this.selectType === 'directory' && fileInfo.type === 'dir' && fileInfo.name !== '.') {
            return true;
        } else {
            return false;
        }
    }

    onSubmit() {
        if (this.selectType === 'file') {
            this.form.controls[this.controlName].setValue(this.fileSelected);
        } else {
            this.form.controls[this.controlName].setValue(this.fileExplorerPath);
        }
        
        this.form.markAsDirty();
        this.select.emit(this.fileInfoSelected);
        this.modalRef.hide();
    }
}
