/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SimpleChange, TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FileInfo } from '../../store/models';
import { PathSelectFormControlComponent } from './path-select-form-control.component';

describe('[admin][admin-shared][components] PathSelectFormControlComponent', () => {
    let component: PathSelectFormControlComponent;
    let fixture: ComponentFixture<PathSelectFormControlComponent>;
    const modalServiceStub = {
        show: jest.fn().mockReturnThis(),
        hide: jest.fn()
    };
    let fileInfo: FileInfo = { mimetype: 'test', name: '', size: 1000, type: '' };
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                PathSelectFormControlComponent,
            ],
            providers: [
                BsModalRef,
                { provide: BsModalService, useValue: modalServiceStub }
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(PathSelectFormControlComponent);
        component = fixture.componentInstance;
        component.controlName = 'test';
        component.form = new FormGroup({
            test: new FormControl('')
        });
        fixture.detectChanges();
    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('ngOnChanges(changes: SimpleChanges disable form.controls[this.controlName]', () => {
        let spy = jest.spyOn(component.form.controls[component.controlName], 'disable');
        component.ngOnChanges(
            { disabled: new SimpleChange(false, true, false) }
        );
        fixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('ngOnChanges(changes: SimpleChanges enable form.controls[this.controlName]', () => {
        let spy = jest.spyOn(component.form.controls[component.controlName], 'enable');
        component.ngOnChanges(
            { disabled: new SimpleChange(true, false, false) }
        );
        fixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('openModal(template: TemplateRef<any>) should call modalService.show', () => {
        let template: TemplateRef<any> = null;
        let spyOnLoadDirectory = jest.spyOn(component.loadDirectory, 'emit');
        const spy = jest.spyOn(modalServiceStub, 'show');
        fixture.detectChanges();
        component.openModal(template);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(template);
        expect(spyOnLoadDirectory).toHaveBeenCalledTimes(1);
    });
    it('click(fileInfo: FileInfo) return undefined', () => {
        fileInfo = { ...fileInfo, name: '.' };
        let result = component.click(fileInfo);
        expect(result).toBe(undefined);
    });
    it('click(fileInfo: FileInfo) should call buildFilePath(fileInfo) and return undefined', () => {
        fileInfo = { ...fileInfo, type: 'file', name: '' };
        let spy = jest.spyOn(component, 'buildFilePath');
        let result = component.click(fileInfo);
        expect(result).toBe(undefined);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(fileInfo);
        expect(component.fileInfoSelected).toEqual(fileInfo);
    });
    it('fileExplorerPath should equal test/test', () => {
        fileInfo = { ...fileInfo, name: '..', type: '' };
        component.fileExplorerPath = 'test/test/';
        component.click(fileInfo);
        expect(component.fileExplorerPath).toEqual('test/test');
    });
    it('fileExplorerPath should equal test/test', () => {
        fileInfo = { ...fileInfo, name: 'test', type: '' };
        component.click(fileInfo);
        expect(component.fileExplorerPath).toEqual(`/${fileInfo.name}`);
    });
    it('buildFilePath(fileInfo: FileInfo) should return test/test', () => {
        fileInfo = { ...fileInfo, name: 'test', type: '' };
        component.fileExplorerPath = 'test';
        let result = component.buildFilePath(fileInfo);
        expect(result).toEqual('test/test');
    });
    it('buildFilePath(fileInfo: FileInfo) should return /', () => {
        fileInfo = { ...fileInfo, name: 'test', type: '' };
        component.fileExplorerPath = '';
        let result = component.buildFilePath(fileInfo);
        expect(result).toEqual('/test');
    });
    it('checkFileSelected(fileInfo: FileInfo) should return true', () => {
        component.selectType = 'file';
        fileInfo = { ...fileInfo, name: 'test', type: 'file' };
        component.fileSelected = '/test';
        let result = component.checkFileSelected(fileInfo);
        expect(result).toBe(true);
    });
    it('checkPointer(fileInfo: FileInfo) should return true', () => {
        component.selectType = 'file';
        fileInfo = { ...fileInfo, name: 'test', type: 'file' };
        let result = component.checkPointer(fileInfo);
        expect(result).toBe(true);
    });
    it('checkPointer(fileInfo: FileInfo) should return true on selectType = directory', () => {
        component.selectType = 'directory';
        fileInfo = { ...fileInfo, name: 'test', type: 'dir' };
        let result = component.checkPointer(fileInfo);
        expect(result).toBe(true);
    });
    it('checkPointer(fileInfo: FileInfo) should return false', () => {
        component.selectType = '';
        fileInfo = { ...fileInfo, name: '', type: '' };
        let result = component.checkPointer(fileInfo);
        expect(result).toBe(false);
    });
    it('OnSubmit() should emit fileInfoSelected and form.controls[this.controlName].value should equal fileSelected', () => {
        let template: TemplateRef<any> = null;
        let spyOnForm = jest.spyOn(component.form, 'markAsDirty');
        let spyOnSelect = jest.spyOn(component.select, 'emit');
        let spyOnModalServiceStub = jest.spyOn(modalServiceStub, 'hide');
        component.selectType = 'file';
        component.openModal(template);
        component.onSubmit();
        expect(component.form.controls[component.controlName].value).toEqual(component.fileInfoSelected);
        expect(spyOnForm).toHaveBeenCalledTimes(1);
        expect(spyOnModalServiceStub).toHaveBeenCalledTimes(1);
        expect(spyOnSelect).toHaveBeenCalledTimes(1);
        expect(spyOnSelect).toHaveBeenCalledWith(component.fileInfoSelected);
    })
    it('OnSubmit() should emit fileInfoSelectedform.controls[this.controlName].value should equal fileExplorerPath', () => {
        let template: TemplateRef<any> = null;
        component.selectType = '';
        component.openModal(template);
        component.onSubmit();
        expect(component.form.controls[component.controlName].value).toEqual(component.fileExplorerPath);
    })
});
