/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DeleteBtnComponent } from './delete-btn.component';

describe('[admin][admin-shared][components] DeleteBtnComponent', () => {
    let component: DeleteBtnComponent;
    let fixture: ComponentFixture<DeleteBtnComponent>;
    const modalServiceStub = {
        show: jest.fn(),
    };
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DeleteBtnComponent,
            ],
            providers: [
                BsModalRef,
                { provide: BsModalService, useValue: modalServiceStub }
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(DeleteBtnComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('openModal(template: TemplateRef<any>) should call modalService.show', () => {
        let template: TemplateRef<any> = null;
        const spy = jest.spyOn(modalServiceStub, 'show');
        component.openModal(template);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(template);
    })
});
