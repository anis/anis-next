/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { AdminAuthGuard } from './admin-auth.guard';

const routes: Routes = [
    { 
        path: '', component: AdminComponent, canActivate: [AdminAuthGuard], children: [
            { path: '', redirectTo: 'instance/instance-list', pathMatch: 'full' },
            { path: 'instance', loadChildren: () => import('./instance/instance.module').then(m => m.InstanceModule) },
            { path: 'database', loadChildren: () => import('./database/database.module').then(m => m.DatabaseModule) }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }

export const routedComponents = [
    AdminComponent
];
