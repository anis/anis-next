/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { UserProfile } from 'src/app/auth/user-profile.model';
import * as authActions from 'src/app/auth/auth.actions';
import * as authSelector from 'src/app/auth/auth.selector';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import { AppConfigService } from 'src/app/app-config.service';
import { StyleService } from 'src/app/shared/services/style.service';

@Component({
    selector: 'app-admin',
    templateUrl: 'admin.component.html'
})
/**
 * @class
 * @classdesc Admin container.
 *
 * @implements OnInit
 */
export class AdminComponent implements OnInit {
    public favIcon: HTMLLinkElement = document.querySelector('#favicon');
    public body: HTMLBodyElement = document.querySelector('body');
    public isAuthenticated: Observable<boolean>;
    public userProfile: Observable<UserProfile>;

    constructor(private store: Store<{ }>, private config: AppConfigService, private style: StyleService) {
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.userProfile = store.select(authSelector.selectUserProfile);
    }

    ngOnInit() {
        this.favIcon.href = 'favicon.ico';
        this.body.style.backgroundColor = 'white';
        Promise.resolve(null).then(() => this.store.dispatch(databaseActions.loadDatabaseList()));
        this.style.setStyles('.footer', {
            'background-color': '#F8F9FA',
            'border-top': 'none',
            'color': 'black',
        });
    }

    getAuthenticationEnabled() {
        return this.config.authenticationEnabled;
    }

    login(): void {
        this.store.dispatch(authActions.login({ redirectUri: window.location.toString() }));
    }

    logout(): void {
        this.store.dispatch(authActions.logout());
    }

    openEditProfile(): void {
        this.store.dispatch(authActions.openEditProfile());
    }
}
