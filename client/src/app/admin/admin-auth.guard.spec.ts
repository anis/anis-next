/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AppConfigService } from '../app-config.service';
import { AdminAuthGuard } from './admin-auth.guard';
import * as authSelector from 'src/app/auth/auth.selector';
import { cold } from 'jasmine-marbles';

describe('[admin] AdminAuthGuard', () => {
    let store: MockStore;
    let mockAuthSelectorselectIsAuthenticated;
    let mockAuthSelectorselectUserRoles;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockStore({}),],
        })
    });
    it('should create component', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true } })
        store = TestBed.inject(MockStore);
        mockAuthSelectorselectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockAuthSelectorselectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        expect(adminAuthGuard).toBeTruthy();
    });
    it('canActivate() should return true when authentifiacation is not enabled', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: false } })
        store = TestBed.inject(MockStore);
        mockAuthSelectorselectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockAuthSelectorselectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        let result = adminAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);

    });
    it('canActivate() should return false when authentifiacationEnabled is true and user is not Admin ', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true, adminRoles: [] } })
        store = TestBed.inject(MockStore);
        mockAuthSelectorselectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockAuthSelectorselectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        let result = adminAuthGuard.canActivate();
        let expected = cold('a', { a: false });
        expect(result).toBeObservable(expected);

    });
    it('canActivate() should return false when authentifiacationEnabled is true and user is  Admin ', () => {
        TestBed.overrideProvider(AppConfigService, { useValue: { authenticationEnabled: true, adminRoles: ['test'] } })
        store = TestBed.inject(MockStore);
        mockAuthSelectorselectIsAuthenticated = store.overrideSelector(authSelector.selectIsAuthenticated, true);
        mockAuthSelectorselectUserRoles = store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        let result = adminAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);

    });
})

