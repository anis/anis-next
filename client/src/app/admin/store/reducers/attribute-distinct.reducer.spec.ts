/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromAttributeDistinct from './attribute-distinct.reducer';
import * as attributeDistinctActions from '../actions/attribute-distinct.actions';
import { ATTRIBUTE } from 'src/test-data';

describe('[Metamodel][Reducers] AttributeDistinct reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromAttributeDistinct;
        const action = { type: 'Unknown' };
        const state = fromAttributeDistinct.attributeDistinctReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadAttributeDistinctList action should set attributeDistinctListIsLoading to true', () => {
        const { initialState } = fromAttributeDistinct;
        const action = attributeDistinctActions.loadAttributeDistinctList({ attribute: ATTRIBUTE });
        const state = fromAttributeDistinct.attributeDistinctReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.attributeDistinctListIsLoading).toEqual(true);
        expect(state.attributeDistinctListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadAttributeDistinctListSuccess action should add attributeDistinct list, set attributeDistinctListIsLoading to false and set attributeDistinctListIsLoaded to true', () => {
        const { initialState } = fromAttributeDistinct;
        const action = attributeDistinctActions.loadAttributeDistinctListSuccess({ values: ['val-one', 'val-two'] });
        const state = fromAttributeDistinct.attributeDistinctReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain('val-one');
        expect(state.ids).toContain('val-two');
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.attributeDistinctListIsLoading).toEqual(false);
        expect(state.attributeDistinctListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadAttributeDistinctListFail action should set attributeDistinctListIsLoading to false', () => {
        const { initialState } = fromAttributeDistinct;
        const action = attributeDistinctActions.loadAttributeDistinctListFail();
        const state = fromAttributeDistinct.attributeDistinctReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.attributeDistinctListIsLoading).toEqual(false);
        expect(state.attributeDistinctListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get attributeDistinctListIsLoading', () => {
        const action = {} as Action;
        const state =  fromAttributeDistinct.attributeDistinctReducer(undefined, action);

        expect(fromAttributeDistinct.selectAttributeDistinctListIsLoading(state)).toEqual(false);
    });

    it('should get attributeDistinctListIsLoaded', () => {
        const action = {} as Action;
        const state = fromAttributeDistinct.attributeDistinctReducer(undefined, action);

        expect(fromAttributeDistinct.selectAttributeDistinctListIsLoaded(state)).toEqual(false);
    });
});
