/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromColumn from './column.reducer';
import * as columnActions from '../actions/column.actions';
import { COLUMN_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] Column reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromColumn;
        const action = { type: 'Unknown' };
        const state = fromColumn.columnReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadColumnList action should set columnListIsLoading to true', () => {
        const { initialState } = fromColumn;
        const action = columnActions.loadColumnList();
        const state = fromColumn.columnReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.columnListIsLoading).toEqual(true);
        expect(state.columnListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadColumnListSuccess action should add column list, set columnListIsLoading to false and set columnListIsLoaded to true', () => {
        const { initialState } = fromColumn;
        const action = columnActions.loadColumnListSuccess({ columns: COLUMN_LIST });
        const state = fromColumn.columnReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain('myCol');
        expect(state.ids).toContain('anotherCol');
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.columnListIsLoading).toEqual(false);
        expect(state.columnListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadColumnListFail action should set columnListIsLoading to false', () => {
        const { initialState } = fromColumn;
        const action = columnActions.loadColumnListFail();
        const state = fromColumn.columnReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.columnListIsLoading).toEqual(false);
        expect(state.columnListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get columnListIsLoading', () => {
        const action = {} as Action;
        const state =  fromColumn.columnReducer(undefined, action);

        expect(fromColumn.selectColumnListIsLoading(state)).toEqual(false);
    });

    it('should get columnListIsLoaded', () => {
        const action = {} as Action;
        const state = fromColumn.columnReducer(undefined, action);

        expect(fromColumn.selectColumnListIsLoaded(state)).toEqual(false);
    });
});
