/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as tableActions from '../actions/table.actions';

/**
 * Interface for table state.
 *
 * @interface State
 */
export interface State extends EntityState<string> {
    tableListIsLoading: boolean;
    tableListIsLoaded: boolean;
}

export const adapter: EntityAdapter<string> = createEntityAdapter<string>({
    selectId: (table: string) => table,
    sortComparer: (a: string, b: string) => a.localeCompare(b)
});

export const initialState: State = adapter.getInitialState({
    tableListIsLoading: false,
    tableListIsLoaded: false
});

export const tableReducer = createReducer(
    initialState,
    on(tableActions.loadTableList, (state) => {
        return {
            ...state,
            tableListIsLoading: true,
            tableListIsLoaded: false
        }
    }),
    on(tableActions.loadTableListSuccess, (state, { tables }) => {
        return adapter.setAll(
            tables,
            {
                ...state,
                tableListIsLoading: false,
                tableListIsLoaded: true
            }
        );
    }),
    on(tableActions.loadTableListFail, (state) => {
        return {
            ...state,
            tableListIsLoading: false
        }
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectTableIds = selectIds;
export const selectTableEntities = selectEntities;
export const selectAllTables = selectAll;
export const selectTableTotal = selectTotal;

export const selectTableListIsLoading = (state: State) => state.tableListIsLoading;
export const selectTableListIsLoaded = (state: State) => state.tableListIsLoaded;
