/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as attributeDistinctActions from '../actions/attribute-distinct.actions';

/**
 * Interface for attribute distinct state.
 *
 * @interface State
 */
export interface State extends EntityState<string> {
    attributeDistinctListIsLoading: boolean;
    attributeDistinctListIsLoaded: boolean;
}

export const adapter: EntityAdapter<string> = createEntityAdapter<string>({
    selectId: (attributeDistinct: string) => attributeDistinct,
    sortComparer: (a: string, b: string) => a.localeCompare(b)
});

export const initialState: State = adapter.getInitialState({
    attributeDistinctListIsLoading: false,
    attributeDistinctListIsLoaded: false
});

export const attributeDistinctReducer = createReducer(
    initialState,
    on(attributeDistinctActions.loadAttributeDistinctList, (state) => {
        return {
            ...state,
            attributeDistinctListIsLoading: true
        }
    }),
    on(attributeDistinctActions.loadAttributeDistinctListSuccess, (state, { values }) => {
        return adapter.setAll(
            values,
            {
                ...state,
                attributeDistinctListIsLoading: false,
                attributeDistinctListIsLoaded: true
            }
        );
    }),
    on(attributeDistinctActions.loadAttributeDistinctListFail, (state) => {
        return {
            ...state,
            attributeDistinctListIsLoading: false
        }
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectAttributeDistinctIds = selectIds;
export const selectAttributeDistinctEntities = selectEntities;
export const selectAllAttributeDistincts = selectAll;
export const selectAttributeDistinctTotal = selectTotal;

export const selectAttributeDistinctListIsLoading = (state: State) => state.attributeDistinctListIsLoading;
export const selectAttributeDistinctListIsLoaded = (state: State) => state.attributeDistinctListIsLoaded;
