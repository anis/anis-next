/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromTable from './table.reducer';
import * as tableActions from '../actions/table.actions';

describe('[Metamodel][Reducers] Table reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromTable;
        const action = { type: 'Unknown' };
        const state = fromTable.tableReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it('loadTableList action should set tableListIsLoading to true', () => {
        const { initialState } = fromTable;
        const action = tableActions.loadTableList({ idDatabase: 1 });
        const state = fromTable.tableReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.tableListIsLoading).toEqual(true);
        expect(state.tableListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('loadTableListSuccess action should add table list, set tableListIsLoading to false and set tableListIsLoaded to true', () => {
        const { initialState } = fromTable;
        const action = tableActions.loadTableListSuccess({ tables: ['table-one', 'table-two'] });
        const state = fromTable.tableReducer(initialState, action);
        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain('table-one');
        expect(state.ids).toContain('table-two');
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.tableListIsLoading).toEqual(false);
        expect(state.tableListIsLoaded).toEqual(true);
        expect(state).not.toBe(initialState);
    });

    it('loadTableListFail action should set tableListIsLoading to false', () => {
        const { initialState } = fromTable;
        const action = tableActions.loadTableListFail();
        const state = fromTable.tableReducer(initialState, action);
        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.tableListIsLoading).toEqual(false);
        expect(state.tableListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get tableListIsLoading', () => {
        const action = {} as Action;
        const state =  fromTable.tableReducer(undefined, action);

        expect(fromTable.selectTableListIsLoading(state)).toEqual(false);
    });

    it('should get tableListIsLoaded', () => {
        const action = {} as Action;
        const state = fromTable.tableReducer(undefined, action);

        expect(fromTable.selectTableListIsLoaded(state)).toEqual(false);
    });
});
