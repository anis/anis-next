/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Column } from '../models';
import * as columnActions from '../actions/column.actions';

/**
 * Interface for column state.
 *
 * @interface State
 */
export interface State extends EntityState<Column> {
    columnListIsLoading: boolean;
    columnListIsLoaded: boolean;
}

export const adapter: EntityAdapter<Column> = createEntityAdapter<Column>({
    selectId: (column: Column) => column.name
});

export const initialState: State = adapter.getInitialState({
    columnListIsLoading: false,
    columnListIsLoaded: false
});

export const columnReducer = createReducer(
    initialState,
    on(columnActions.loadColumnList, (state) => {
        return {
            ...state,
            columnListIsLoading: true
        }
    }),
    on(columnActions.loadColumnListSuccess, (state, { columns }) => {
        return adapter.setAll(
            columns,
            {
                ...state,
                columnListIsLoading: false,
                columnListIsLoaded: true
            }
        );
    }),
    on(columnActions.loadColumnListFail, (state) => {
        return {
            ...state,
            columnListIsLoading: false
        }
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectColumnIds = selectIds;
export const selectColumnEntities = selectEntities;
export const selectAllColumns = selectAll;
export const selectColumnTotal = selectTotal;

export const selectColumnListIsLoading = (state: State) => state.columnListIsLoading;
export const selectColumnListIsLoaded = (state: State) => state.columnListIsLoaded;
