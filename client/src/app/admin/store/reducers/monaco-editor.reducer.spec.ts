/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';
import * as fromMonacoEditor from './monaco-editor.reducer';
import * as monacoEditorActions from '../actions/monaco-editor.actions';

describe('[Metamodel][Reducers] monacoEditor reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromMonacoEditor;
        const action = { type: 'Unknown' };
        const state = fromMonacoEditor.monacoEditorReducer(initialState, action);
        expect(state).toBe(initialState);
    });
    it('loadMonacoEditorInProgress action should set monacoEditorIsLoading to true', () => {
        const { initialState } = fromMonacoEditor;
        const action = monacoEditorActions.loadMonacoEditorInProgress();
        const state = fromMonacoEditor.monacoEditorReducer(initialState, action);
        expect(state.monacoEditorIsLoaded).toBe(false);
        expect(state.monacoEditorIsLoading).toEqual(true);
        expect(state).not.toBe(initialState);
    });
    it('loadMonacoEditorSuccess action should set monacoEditorIsLoaded to true', () => {
        const { initialState } = fromMonacoEditor;
        const action = monacoEditorActions.loadMonacoEditorSuccess();
        const state = fromMonacoEditor.monacoEditorReducer(initialState, action);
        expect(state.monacoEditorIsLoaded).toBe(true);
        expect(state.monacoEditorIsLoading).toEqual(false);
        expect(state).not.toBe(initialState);
    });
    it('should get monacoEditorIsLoading', () => {
        const action = {} as Action;
        const state = fromMonacoEditor.monacoEditorReducer(undefined, action);
        expect(fromMonacoEditor.selectMonacoEditorIsLoading(state)).toEqual(false);
    });
    it('should get monacoEditorIsLoaded', () => {
        const action = {} as Action;
        const state = fromMonacoEditor.monacoEditorReducer(undefined, action);
        expect(fromMonacoEditor.selectMonacoEditorIsLoaded(state)).toEqual(false);
    });

}); 