/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fromAdminFileExplorer from './admin-file-explorer.reducer';
import * as adminFileExplorerActions from '../actions/admin-file-explorer.actions';
import { Action } from '@ngrx/store';
import { FileInfo } from '../models';

describe('[admin][store][reducers] adminFileExplorerReducer reducer', () => {
    it('loadFiles action should set filesIsLoading to true ', () => {
        const { initialState } = fromAdminFileExplorer;
        const action = adminFileExplorerActions.loadFiles({ path: 'test' });
        const state = fromAdminFileExplorer.adminFileExplorerReducer(initialState, action);
        expect(state.filesIsLoading).toBe(true);
        expect(state.filesIsLoaded).toBe(false);
        expect(state).not.toBe(initialState);
    });
    it('loadFilesSuccess action should set filesIsLoaded to true and filesIsLoading to false ', () => {
        const { initialState } = fromAdminFileExplorer;
        let files: FileInfo[] = [{ mimetype: 'test', name: 'test', size: 1000, type: 'json' }];
        const action = adminFileExplorerActions.loadFilesSuccess({ files });
        const state = fromAdminFileExplorer.adminFileExplorerReducer(initialState, action);
        expect(state.filesIsLoading).toBe(false);
        expect(state.filesIsLoaded).toBe(true);
        expect(state).not.toBe(initialState);
    });
    it('loadFilesFail action should set filesIsLoading to false and filesIsLoaded to false ', () => {
        const { initialState } = fromAdminFileExplorer;
        const action = adminFileExplorerActions.loadFilesFail();
        const state = fromAdminFileExplorer.adminFileExplorerReducer(initialState, action);
        expect(state.filesIsLoading).toBe(false);
        expect(state.filesIsLoaded).toBe(false);
    });
    it('should get files', () => {
        const action = {} as Action;
        const state = fromAdminFileExplorer.adminFileExplorerReducer(undefined, action);
        expect(fromAdminFileExplorer.selectFiles(state)).toEqual(null);
    });
    it('should get filesIsLoading', () => {
        const action = {} as Action;
        const state = fromAdminFileExplorer.adminFileExplorerReducer(undefined, action);
        expect(fromAdminFileExplorer.selectFilesIsLoading(state)).toEqual(false);
    });
    it('should get filesIsLoaded', () => {
        const action = {} as Action;
        const state = fromAdminFileExplorer.adminFileExplorerReducer(undefined, action);
        expect(fromAdminFileExplorer.selectFilesIsLoaded(state)).toEqual(false);
    });
});
