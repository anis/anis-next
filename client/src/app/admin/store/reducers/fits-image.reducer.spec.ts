/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fromFitsImage from './fits-image.reducer';
import * as fitsImageActions from '../actions/fits-image.actions';
import { FitsImageLimits } from '../models';
import { Action } from '@ngrx/store';

describe('[admin][store][reducers] fitsImageReducer reducer', () => {
    it('retrieveFitsImageLimits action should set fitsImageLimitsIsLoading to true ', () => {
        const { initialState } = fromFitsImage;
        const action = fitsImageActions.retrieveFitsImageLimits({ filePath: 'test' });
        const state = fromFitsImage.fitsImageReducer(initialState, action);
        expect(state.fitsImageLimitsIsLoading).toBe(true);
        expect(state.fitsImageLimitsIsLoaded).toBe(false);
        expect(state).not.toBe(initialState);
    });
    it('retrieveFitsImageLimitsSuccess action should set fitsImageLimitsIsLoaded to true and fitsImageLimitsIsLoading to false ', () => {
        const { initialState } = fromFitsImage;
        let fitsImageLimits: FitsImageLimits = { dec_max: 10, dec_min: 5, ra_max: 5, ra_min: 2 };
        const action = fitsImageActions.retrieveFitsImageLimitsSuccess({ fitsImageLimits });
        const state = fromFitsImage.fitsImageReducer(initialState, action);
        expect(state.fitsImageLimitsIsLoading).toBe(false);
        expect(state.fitsImageLimitsIsLoaded).toBe(true);
        expect(state).not.toBe(initialState);
    });
    it('retrieveFitsImageLimitsFail action should set fitsImageLimitsIsLoaded to false and fitsImageLimitsIsLoading to false ', () => {
        const { initialState } = fromFitsImage;
        const action = fitsImageActions.retrieveFitsImageLimitsFail();
        const state = fromFitsImage.fitsImageReducer(initialState, action);
        expect(state.fitsImageLimitsIsLoading).toBe(false);
        expect(state.fitsImageLimitsIsLoaded).toBe(false);
    });
    it('should get fitsImageLimitsIs', () => {
        const action = {} as Action;
        const state = fromFitsImage.fitsImageReducer(undefined, action);
        expect(fromFitsImage.selectFitsImageLimits(state)).toEqual(null);
    });
    it('should get fitsImageLimitsIsLoading', () => {
        const action = {} as Action;
        const state = fromFitsImage.fitsImageReducer(undefined, action);
        expect(fromFitsImage.selectFitsImageLimitsIsLoading(state)).toEqual(false);
    });
    it('should get fitsImageLimitsIsLoaded', () => {
        const action = {} as Action;
        const state = fromFitsImage.fitsImageReducer(undefined, action);
        expect(fromFitsImage.selectFitsImageLimitsIsLoaded(state)).toEqual(false);
    });

}); 