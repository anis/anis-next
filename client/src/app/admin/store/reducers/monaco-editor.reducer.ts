/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as monacoEditorActions from '../actions/monaco-editor.actions';

/**
 * Interface for monaco editor state.
 *
 * @interface State
 */
export interface State {
    monacoEditorIsLoading: boolean;
    monacoEditorIsLoaded: boolean;
}

export const initialState: State = {
    monacoEditorIsLoading: false,
    monacoEditorIsLoaded: false
}

export const monacoEditorReducer = createReducer(
    initialState,
    on(monacoEditorActions.loadMonacoEditorInProgress, state => ({
        ...state,
        monacoEditorIsLoading: true
    })),
    on(monacoEditorActions.loadMonacoEditorSuccess, state => ({
        ...state,
        monacoEditorIsLoading: false,
        monacoEditorIsLoaded: true
    }))
);

export const selectMonacoEditorIsLoading = (state: State) => state.monacoEditorIsLoading;
export const selectMonacoEditorIsLoaded = (state: State) => state.monacoEditorIsLoaded;
