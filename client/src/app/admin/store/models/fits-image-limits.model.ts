/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


/**
 * Interface for fits image limits.
 *
 * @interface FitsImageLimits
 */
export interface FitsImageLimits {
    ra_min: number;
    ra_max: number;
    dec_min: number;
    dec_max: number;
}
