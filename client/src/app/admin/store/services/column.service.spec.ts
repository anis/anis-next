/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ColumnService } from './column.service';
import { AppConfigService } from 'src/app/app-config.service';
import { Column } from '../models';

describe('[Instance][Metamodel][Services] ColumnService', () => {
    let service: ColumnService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                ColumnService
            ]
        });
        service = TestBed.inject(ColumnService);
    });

    it('#retrieveColumns() should return an Observable<Column[]>',
        inject([HttpTestingController, ColumnService],(httpMock: HttpTestingController, service: ColumnService) => {
                const mockResponse = [];

                service.retrieveColumns('myDataset').subscribe((event: Column[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/column');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
