/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Table service.
 */
@Injectable()
export class TableService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves results for the given parameters.
     *
     * @param  {number} idDatabase - The database ID.
     *
     * @return Observable<string[]>
     */
    retrieveTableList(idDatabase: number): Observable<string[]> {
        return this.http.get<string[]>(`${this.config.apiUrl}/database/${idDatabase}/table`);
    }
}
