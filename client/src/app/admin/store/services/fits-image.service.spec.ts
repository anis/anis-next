/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from 'src/app/app-config.service';
import { FitsImageService } from './fits-image.service';
import { FitsImageLimits } from '../models';

describe('[admin][store][Services] FitsImageService', () => {
    let service: FitsImageService;
    let httpController: HttpTestingController;
    let config: AppConfigService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                FitsImageService,
            ]
        });
        service = TestBed.inject(FitsImageService);
        httpController = TestBed.inject(HttpTestingController);
        config = TestBed.inject(AppConfigService);
    });
    it('retrieveFitsImageLimits(datasetName: string, filePath: string) should return an Observable<FitsImageLimits>', () => {
        const mockResponse: FitsImageLimits = { dec_max: 10, dec_min: 5, ra_max: 10, ra_min: 5 };
        service.retrieveFitsImageLimits('test', 'test').subscribe((fitsImageLimits) => {
            expect(fitsImageLimits).toEqual(mockResponse);
        });
        const req = httpController.expectOne(`${config.servicesUrl}/get-fits-image-limits/test?filename=test`);
        expect(req.request.method).toBe("GET");
        req.flush(mockResponse);
    });

});
