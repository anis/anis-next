/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfigService } from 'src/app/app-config.service';
import { AdminFileExplorerService } from './admin-file-explorer.service';
import { FileInfo } from '../models';

describe('[admin][store][Services] AdminFileExplorerService', () => {
    let service: AdminFileExplorerService;
    let httpController: HttpTestingController;
    let config: AppConfigService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                AdminFileExplorerService,
            ]
        });
        service = TestBed.inject(AdminFileExplorerService);
        httpController = TestBed.inject(HttpTestingController);
        config = TestBed.inject(AppConfigService);
    });
    it('retrieveFiles(path: string) should return an Observable<FileInfo[]>', () => {
        const mockResponse: FileInfo[] = [
            { mimetype: 'test', name: 'test', size: 1000, type: 'test' }
        ];
        let path: string = 'test';
        service.retrieveFiles(path).subscribe((fileInfos) => {
            expect(fileInfos).toEqual([]);
        });
        const req = httpController.expectOne(`${config.apiUrl}/admin-file-explorer${path}`);
        expect(req.request.method).toBe("GET");
        req.flush(mockResponse);

    });

});
