/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Attribute distinct service.
 */
@Injectable()
export class AttributeDistinctService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves distinct attribute list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     * @param  {Attribute} attribute - The attribute.
     *
     * @return Observable<string[]>
     */
    retrieveAttributeDistinctList(datasetName: string, attribute: Attribute): Observable<string[]> {
        return this.http.get<string[]>(`${this.config.apiUrl}/dataset/${datasetName}/attribute/${attribute.id}/distinct`);
    }
}
