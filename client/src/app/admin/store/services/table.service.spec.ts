/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TableService } from './table.service';
import { AppConfigService } from 'src/app/app-config.service';

describe('[Instance][Metamodel][Services] TableService', () => {
    let service: TableService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                TableService
            ]
        });
        service = TestBed.inject(TableService);
    });

    it('#retrieveTableList() should return an Observable<string[]>',
        inject([HttpTestingController, TableService],(httpMock: HttpTestingController, service: TableService) => {
                const mockResponse = ['Table'];

                service.retrieveTableList(1).subscribe((event: string[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/database/1/table');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
