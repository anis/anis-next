/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AttributeDistinctService } from './attribute-distinct.service';
import { AppConfigService } from 'src/app/app-config.service';
import { ATTRIBUTE } from 'src/test-data';

describe('[Instance][Metamodel][Services] AttributeDistinctService', () => {
    let service: AttributeDistinctService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: AppConfigService, useValue: { apiUrl: 'http://testing.com' } },
                AttributeDistinctService
            ]
        });
        service = TestBed.inject(AttributeDistinctService);
    });

    it('#retrieveAttributeDistinctList() should return an Observable<string[]>',
        inject([HttpTestingController, AttributeDistinctService],(httpMock: HttpTestingController, service: AttributeDistinctService) => {
                const mockResponse = [];

                service.retrieveAttributeDistinctList('myDataset', ATTRIBUTE).subscribe((event: string[]) => {
                    expect(event).toEqual(mockResponse);
                });

                const mockRequest = httpMock.expectOne('http://testing.com/dataset/myDataset/attribute/1/distinct');

                expect(mockRequest.cancelled).toBeFalsy();
                expect(mockRequest.request.method).toEqual('GET');
                expect(mockRequest.request.responseType).toEqual('json');
                mockRequest.flush(mockResponse);

                httpMock.verify();
            }
        )
    );
});
