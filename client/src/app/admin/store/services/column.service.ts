/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Column } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Column service.
 */
@Injectable()
export class ColumnService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves column list for the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     *
     * @return Observable<Column[]>
     */
    retrieveColumns(datasetName: string): Observable<Column[]> {
        return this.http.get<Column[]>(`${this.config.apiUrl}/dataset/${datasetName}/column`);
    }
}
