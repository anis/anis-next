/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';
import { FitsImageLimits } from '../models';

/**
 * @class
 * @classdesc FitsImage service.
 */
@Injectable()
export class FitsImageService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves fits image limits for the given parameters.
     *
     * @param  {string} datasetName - The current dataset name
     * @param  {string} filePath    - The path to the fits file
     *
     * @return Observable<FitsImageLimits>
     */
    retrieveFitsImageLimits(datasetName: string, filePath: string): Observable<FitsImageLimits> {
        return this.http.get<FitsImageLimits>(`${this.config.servicesUrl}/get-fits-image-limits/${datasetName}?filename=${filePath}`);
    }
}
