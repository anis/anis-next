/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { FileInfo } from '../models';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc File explorer service.
 */
@Injectable()
export class AdminFileExplorerService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves files with the given path.
     *
     * @param  {string} path - The path.
     *
     * @return Observable<FileInfo[]>
     */
    retrieveFiles(path: string): Observable<FileInfo[]> {
        return this.http.get<FileInfo[]>(`${this.config.apiUrl}/admin-file-explorer${path}`);
    }
}
