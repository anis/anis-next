/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AttributeDistinctService } from './attribute-distinct.service';
import { ColumnService } from './column.service';
import { TableService } from './table.service';
import { AdminFileExplorerService } from './admin-file-explorer.service';
import { FitsImageService } from './fits-image.service';

export const adminServices = [
    AttributeDistinctService,
    ColumnService,
    TableService,
    AdminFileExplorerService,
    FitsImageService
];
