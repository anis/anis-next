/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { Column } from '../models';

export const loadColumnList = createAction('[Admin] Load Column List');
export const loadColumnListSuccess = createAction('[Admin] Load Column List Success', props<{ columns: Column[] }>());
export const loadColumnListFail = createAction('[Admin] Load Column List Fail');
