/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { FitsImageLimits } from '../models';

export const retrieveFitsImageLimits = createAction('[Admin] Retrieve Fits Image Limits', props<{ filePath: string }>());
export const retrieveFitsImageLimitsSuccess = createAction('[Admin] Retrieve Fits Image Limits Success', props<{ fitsImageLimits: FitsImageLimits }>());
export const retrieveFitsImageLimitsFail = createAction('[Admin] Retrieve Fits Image Limits Fail');
