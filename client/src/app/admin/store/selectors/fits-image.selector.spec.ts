/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fitsImageSelector from './fits-image.selector';
import * as fromFitsImage from '../reducers/fits-image.reducer';

describe('[Admin][Selector] fitsImage selector', () => {
    let state = { admin: { fitsImage: { ...fromFitsImage.initialState } } };
    it('should get fitsImage state', () => {
        expect(fitsImageSelector.selectFitsImageState(state)).toEqual(state.admin.fitsImage);
    });
    it('should get fitsImageLimits', () => {
        expect(fitsImageSelector.selectFitsImageLimits(state)).toEqual(null);
    });
    it('should get fitsImageLimitsIsLoaded', () => {
        expect(fitsImageSelector.selectFitsImageLimitsIsLoaded(state)).toEqual(false);
    });
    it('should get fitsImageLimitsIsLoading', () => {
        expect(fitsImageSelector.selectFitsImageLimitsIsLoading(state)).toEqual(false);
    });
});
