/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as attributeDistinctSelector from './attribute-distinct.selector';
import * as fromAttributeDistinct from '../reducers/attribute-distinct.reducer';

describe('[Admin][Selector] AttributeDistinct selector', () => {
    it('should get attributeDistinct state', () => {
        const state = { admin: { attributeDistinct: { ...fromAttributeDistinct.initialState }}};
        expect(attributeDistinctSelector.selectAttributeDistinctState(state)).toEqual(state.admin.attributeDistinct);
    });

    it('should get attributeDistinct IDs', () => {
        const state = { admin: { attributeDistinct: { ...fromAttributeDistinct.initialState }}};
        expect(attributeDistinctSelector.selectAttributeDistinctIds(state).length).toEqual(0);
    });

    it('should get attributeDistinct entities', () => {
        const state = { admin: { attributeDistinct: { ...fromAttributeDistinct.initialState }}};
        expect(attributeDistinctSelector.selectAttributeDistinctEntities(state)).toEqual({ });
    });

    it('should get all attributeDistincts', () => {
        const state = { admin: { attributeDistinct: { ...fromAttributeDistinct.initialState }}};
        expect(attributeDistinctSelector.selectAllAttributeDistincts(state).length).toEqual(0);
    });

    it('should get attributeDistinct count', () => {
        const state = { admin: { attributeDistinct: { ...fromAttributeDistinct.initialState }}};
        expect(attributeDistinctSelector.selectAttributeDistinctTotal(state)).toEqual(0);
    });

    it('should get attributeDistinctListIsLoading', () => {
        const state = { admin: { attributeDistinct: { ...fromAttributeDistinct.initialState }}};
        expect(attributeDistinctSelector.selectAttributeDistinctListIsLoading(state)).toBe(false);
    });

    it('should get attributeDistinctListIsLoaded', () => {
        const state = { admin: { attributeDistinct: { ...fromAttributeDistinct.initialState }}};
        expect(attributeDistinctSelector.selectAttributeDistinctListIsLoaded(state)).toBe(false);
    });
});
