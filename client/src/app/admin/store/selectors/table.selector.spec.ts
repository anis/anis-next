/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as tableSelector from './table.selector';
import * as fromTable from '../reducers/table.reducer';

describe('[Admin][Selector] Table selector', () => {
    it('should get table state', () => {
        const state = { admin: { table: { ...fromTable.initialState }}};
        expect(tableSelector.selectTableState(state)).toEqual(state.admin.table);
    });

    it('should get table IDs', () => {
        const state = { admin: { table: { ...fromTable.initialState }}};
        expect(tableSelector.selectTableIds(state).length).toEqual(0);
    });

    it('should get table entities', () => {
        const state = { admin: { table: { ...fromTable.initialState }}};
        expect(tableSelector.selectTableEntities(state)).toEqual({ });
    });

    it('should get all tables', () => {
        const state = { admin: { table: { ...fromTable.initialState }}};
        expect(tableSelector.selectAllTables(state).length).toEqual(0);
    });

    it('should get table count', () => {
        const state = { admin: { table: { ...fromTable.initialState }}};
        expect(tableSelector.selectTableTotal(state)).toEqual(0);
    });

    it('should get tableListIsLoading', () => {
        const state = { admin: { table: { ...fromTable.initialState }}};
        expect(tableSelector.selectTableListIsLoading(state)).toBe(false);
    });

    it('should get tableListIsLoaded', () => {
        const state = { admin: { table: { ...fromTable.initialState }}};
        expect(tableSelector.selectTableListIsLoaded(state)).toBe(false);
    });
});
