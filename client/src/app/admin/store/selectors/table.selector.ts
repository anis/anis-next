/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../admin.reducer';
import * as fromTable from '../reducers/table.reducer';

export const selectTableState = createSelector(
    reducer.getAdminState,
    (state: reducer.State) => state.table
);

export const selectTableIds = createSelector(
    selectTableState,
    fromTable.selectTableIds
);

export const selectTableEntities = createSelector(
    selectTableState,
    fromTable.selectTableEntities
);

export const selectAllTables = createSelector(
    selectTableState,
    fromTable.selectAllTables
);

export const selectTableTotal = createSelector(
    selectTableState,
    fromTable.selectTableTotal
);

export const selectTableListIsLoading = createSelector(
    selectTableState,
    fromTable.selectTableListIsLoading
);

export const selectTableListIsLoaded = createSelector(
    selectTableState,
    fromTable.selectTableListIsLoaded
);
