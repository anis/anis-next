/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../admin.reducer';
import * as fromAttributeDistinct from '../reducers/attribute-distinct.reducer';

export const selectAttributeDistinctState = createSelector(
    reducer.getAdminState,
    (state: reducer.State) => state.attributeDistinct
);

export const selectAttributeDistinctIds = createSelector(
    selectAttributeDistinctState,
    fromAttributeDistinct.selectAttributeDistinctIds
);

export const selectAttributeDistinctEntities = createSelector(
    selectAttributeDistinctState,
    fromAttributeDistinct.selectAttributeDistinctEntities
);

export const selectAllAttributeDistincts = createSelector(
    selectAttributeDistinctState,
    fromAttributeDistinct.selectAllAttributeDistincts
);

export const selectAttributeDistinctTotal = createSelector(
    selectAttributeDistinctState,
    fromAttributeDistinct.selectAttributeDistinctTotal
);

export const selectAttributeDistinctListIsLoading = createSelector(
    selectAttributeDistinctState,
    fromAttributeDistinct.selectAttributeDistinctListIsLoading
);

export const selectAttributeDistinctListIsLoaded = createSelector(
    selectAttributeDistinctState,
    fromAttributeDistinct.selectAttributeDistinctListIsLoaded
);
