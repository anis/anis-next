/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as columnSelector from './column.selector';
import * as fromColumn from '../reducers/column.reducer';

describe('[Admin][Selector] Column selector', () => {
    it('should get column state', () => {
        const state = { admin: { column: { ...fromColumn.initialState }}};
        expect(columnSelector.selectColumnState(state)).toEqual(state.admin.column);
    });

    it('should get column IDs', () => {
        const state = { admin: { column: { ...fromColumn.initialState }}};
        expect(columnSelector.selectColumnIds(state).length).toEqual(0);
    });

    it('should get column entities', () => {
        const state = { admin: { column: { ...fromColumn.initialState }}};
        expect(columnSelector.selectColumnEntities(state)).toEqual({ });
    });

    it('should get all columns', () => {
        const state = { admin: { column: { ...fromColumn.initialState }}};
        expect(columnSelector.selectAllColumns(state).length).toEqual(0);
    });

    it('should get column count', () => {
        const state = { admin: { column: { ...fromColumn.initialState }}};
        expect(columnSelector.selectColumnTotal(state)).toEqual(0);
    });

    it('should get columnListIsLoading', () => {
        const state = { admin: { column: { ...fromColumn.initialState }}};
        expect(columnSelector.selectColumnListIsLoading(state)).toBe(false);
    });

    it('should get columnListIsLoaded', () => {
        const state = { admin: { column: { ...fromColumn.initialState }}};
        expect(columnSelector.selectColumnListIsLoaded(state)).toBe(false);
    });
});
