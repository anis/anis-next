/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../admin.reducer';
import * as fromFileExplorer from '../reducers/admin-file-explorer.reducer';

export const selectAdminFileExplorerState = createSelector(
    reducer.getAdminState,
    (state: reducer.State) => state.adminFileExplorer
);

export const selectFiles = createSelector(
    selectAdminFileExplorerState,
    fromFileExplorer.selectFiles
);

export const selectFilesIsLoading = createSelector(
    selectAdminFileExplorerState,
    fromFileExplorer.selectFilesIsLoading
);

export const selectFilesIsLoaded = createSelector(
    selectAdminFileExplorerState,
    fromFileExplorer.selectFilesIsLoaded
);
