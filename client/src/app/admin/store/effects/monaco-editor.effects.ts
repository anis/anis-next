/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable, NgZone } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';

import * as monacoEditorActions from '../actions/monaco-editor.actions';
import * as monacoEditorSelector from '../selectors/monaco-editor.selector';
import { AppConfigService } from 'src/app/app-config.service';

/**
 * @class
 * @classdesc Monaco editor effects.
 */
@Injectable()
export class MonacoEditorEffects {
    /**
     * Calls action to test if monaco editor need to be load
     */
    loadMonacoEditor$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(monacoEditorActions.loadMonacoEditor),
            concatLatestFrom(() => this.store.select(monacoEditorSelector.selectMonacoEditorIsLoaded)),
            mergeMap(([, monacoEditorIsLoaded]) => {
                if (monacoEditorIsLoaded) {
                    return of({ type: '[No Action] Monaco Editor is already loaded' });
                } else {
                    return of(monacoEditorActions.loadMonacoEditorInProgress());
                }
            })
        )
    );

    /**
     * Calls action to load monaco editor 
     */
    loadMonacoEditorInProgress$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(monacoEditorActions.loadMonacoEditor),
            concatLatestFrom(() => this.store.select(monacoEditorSelector.selectMonacoEditorIsLoaded)),
            tap(([, monacoEditorIsLoaded]) => {
                // load the assets
                let baseUrl = '/assets/monaco-editor/min/vs';
                if (this.config.baseHref !== '/') {
                    baseUrl = this.config.baseHref + baseUrl;
                }

                // Monaco editor loading function
                const onGotAmdLoader: any = () => {
                    if (monacoEditorIsLoaded) {
                        return;
                    }

                    // load Monaco
                    (<any>window).require.config({ paths: { vs: `${baseUrl}` } });
                    (<any>window).require([`vs/editor/editor.main`], () => {
                        this.ngZone.run(() => {
                            this.store.dispatch(monacoEditorActions.loadMonacoEditorSuccess());
                        });
                    });
                };

                // AMD loader
                const loaderScript: HTMLScriptElement = document.createElement('script');
                loaderScript.type = 'text/javascript';
                loaderScript.src = `${baseUrl}/loader.js`;
                loaderScript.addEventListener('load', onGotAmdLoader);
                document.body.appendChild(loaderScript);
            })
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private store: Store<{ }>,
        private ngZone: NgZone,
        private config: AppConfigService
    ) {}
}
