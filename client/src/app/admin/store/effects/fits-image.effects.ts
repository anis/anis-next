/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as fitsImageActions from '../actions/fits-image.actions';
import { FitsImageService } from '../services/fits-image.service';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

/**
 * @class
 * @classdesc Fits Image effects.
 */
@Injectable()
export class FitsImageEffects {
    /**
     * Calls action to retrieve fits image limits for a fits file
     */
    retrieveFitsImageLimits$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(fitsImageActions.retrieveFitsImageLimits),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.fitsImageService.retrieveFitsImageLimits(datasetName, action.filePath)
                .pipe(
                    map(fitsImageLimits => fitsImageActions.retrieveFitsImageLimitsSuccess({ fitsImageLimits })),
                    catchError(() => of(fitsImageActions.retrieveFitsImageLimitsFail()))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private fitsImageService: FitsImageService,
        private store: Store<{ }>
    ) {}
}
