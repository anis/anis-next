/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { MonacoEditorEffects } from './monaco-editor.effects';
import { AppConfigService } from 'src/app/app-config.service';
import * as monacoEditorSelector from '../selectors/monaco-editor.selector';
import * as monacoEditorActions from '../actions/monaco-editor.actions';
import { cold, hot } from 'jasmine-marbles';

describe('[admin][store][Effects] MonacoEditorEffects', () => {
    let actions = new Observable();
    let effects: MonacoEditorEffects;
    let metadata: EffectsMetadata<MonacoEditorEffects>;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let router: Router;
    let mockMonacoEditorSelectorMonacoEditorIsLoaded;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                MonacoEditorEffects,
                { provide: Router, useValue: { navigate: jest.fn() } },
                { provide: AppConfigService, useValue: {} },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(MonacoEditorEffects);
        metadata = getEffectsMetadata(effects);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);

    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    describe('loadMonacoEditor$ effect', () => {
        it('should should dispatch an action of type "[No Action] Monaco Editor is already loaded"', () => {
            const action = monacoEditorActions.loadMonacoEditor();
            mockMonacoEditorSelectorMonacoEditorIsLoaded = store.overrideSelector(monacoEditorSelector.selectMonacoEditorIsLoaded, true);
            actions = hot('a', { a: action });
            const expected = cold('a', { a: { type: '[No Action] Monaco Editor is already loaded' } });
            expect(effects.loadMonacoEditor$).toBeObservable(expected);
        });
        it('loadMonacoEditor$ effect should dispatch  monacoEditorActions.loadMonacoEditorInProgress()', () => {
            const action = monacoEditorActions.loadMonacoEditor();
            mockMonacoEditorSelectorMonacoEditorIsLoaded = store.overrideSelector(monacoEditorSelector.selectMonacoEditorIsLoaded, false);
            actions = hot('a', { a: action });
            const expected = cold('a', { a: monacoEditorActions.loadMonacoEditorInProgress() });
            expect(effects.loadMonacoEditor$).toBeObservable(expected);
        });
    });
    describe('loadMonacoEditorInProgress$ effect', () => {
        it('should  call document.createElement', () => {
            let spy = jest.spyOn(document, 'createElement');
            const action = monacoEditorActions.loadMonacoEditor();
            mockMonacoEditorSelectorMonacoEditorIsLoaded = store.overrideSelector(monacoEditorSelector.selectMonacoEditorIsLoaded, true);
            actions = hot('a', { a: action });
            const expected = cold('a', { a: [{ "type": "[Admin] Load Monaco Editor" }, true] });
            expect(effects.loadMonacoEditorInProgress$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledWith('script');
        });

    });
});