/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';

import { TableEffects } from './table.effects';
import { TableService } from '../services/table.service';
import * as tableActions from '../actions/table.actions';

describe('[Metamodel][Effects] TableEffects', () => {
    let actions = new Observable();
    let effects: TableEffects;
    let metadata: EffectsMetadata<TableEffects>;
    let service: TableService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                TableEffects,
                { provide: TableService, useValue: { }},
                provideMockActions(() => actions)
            ]
        }).compileComponents();
        effects = TestBed.inject(TableEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(TableService);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadTables$ effect', () => {
        it('should dispatch the loadTableListSuccess action on success', () => {
            const action = tableActions.loadTableList({ idDatabase: 1 });
            const outcome = tableActions.loadTableListSuccess({ tables: ['table1', 'table2'] });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: ['table1', 'table2'] });
            const expected = cold('--b', { b: outcome });
            service.retrieveTableList = jest.fn(() => response);

            expect(effects.loadTables$).toBeObservable(expected);
        });

        it('should dispatch the loadTableListFail action on HTTP failure', () => {
            const action = tableActions.loadTableList({ idDatabase: 1 });
            const error = new Error();
            const outcome = tableActions.loadTableListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveTableList = jest.fn(() => response);

            expect(effects.loadTables$).toBeObservable(expected);
        });
    });
});
