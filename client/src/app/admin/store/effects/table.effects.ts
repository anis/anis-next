/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as tableActions from '../actions/table.actions';
import { TableService } from '../services/table.service';

/**
 * @class
 * @classdesc Table effects.
 */
@Injectable()
export class TableEffects {

    /**
     * Calls action to retrieve table list.
     */
    loadTables$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(tableActions.loadTableList),
            mergeMap(action => this.tableService.retrieveTableList(action.idDatabase)
                .pipe(
                    map(tables => tableActions.loadTableListSuccess({ tables })),
                    catchError(() => of(tableActions.loadTableListFail()))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private tableService: TableService
    ) { }
}
