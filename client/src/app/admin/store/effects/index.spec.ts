/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { adminEffects } from './index';

describe('[admin][store][effects] Index', () => {
    it('Test index', () => {
        expect(adminEffects.length).toEqual(6);
    });
});

