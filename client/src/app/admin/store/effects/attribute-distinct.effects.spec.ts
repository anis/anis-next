/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { AttributeDistinctEffects } from './attribute-distinct.effects';
import { AttributeDistinctService } from '../services/attribute-distinct.service';
import * as attributeDistinctActions from '../actions/attribute-distinct.actions';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import { ATTRIBUTE } from 'src/test-data';

describe('[Metamodel][Effects] AttributeDistinctEffects', () => {
    let actions = new Observable();
    let effects: AttributeDistinctEffects;
    let metadata: EffectsMetadata<AttributeDistinctEffects>;
    let service: AttributeDistinctService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: { } };
    let mockDatasetSelectorSelectDatasetNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AttributeDistinctEffects,
                { provide: AttributeDistinctService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }},
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(AttributeDistinctEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(AttributeDistinctService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute,''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadAttributeDistinct$ effect', () => {
        it('should dispatch the loadAttributeDistinctListSuccess action on success', () => {
            mockDatasetSelectorSelectDatasetNameByRoute = store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute, 'myDataset'
            );

            const action = attributeDistinctActions.loadAttributeDistinctList({ attribute: ATTRIBUTE });
            const outcome = attributeDistinctActions.loadAttributeDistinctListSuccess({ values: [] });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: [] });
            const expected = cold('--b', { b: outcome });
            service.retrieveAttributeDistinctList = jest.fn(() => response);

            expect(effects.loadAttributeDistinct$).toBeObservable(expected);
            expect(service.retrieveAttributeDistinctList).toHaveBeenCalledWith('myDataset', ATTRIBUTE);
        });

        it('should dispatch the loadAttributeDistinctListFail action on HTTP failure', () => {
            const action = attributeDistinctActions.loadAttributeDistinctList({ attribute: ATTRIBUTE });
            const error = new Error();
            const outcome = attributeDistinctActions.loadAttributeDistinctListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveAttributeDistinctList = jest.fn(() => response);

            expect(effects.loadAttributeDistinct$).toBeObservable(expected);
        });
    });
});
