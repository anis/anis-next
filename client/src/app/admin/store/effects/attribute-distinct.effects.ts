/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as attributeDistinctActions from '../actions/attribute-distinct.actions';
import { AttributeDistinctService } from '../services/attribute-distinct.service';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

/**
 * @class
 * @classdesc Attribute distinct effects.
 */
@Injectable()
export class AttributeDistinctEffects {
    /**
     * Calls action to retrieve distinct attribute list.
     */
    loadAttributeDistinct$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(attributeDistinctActions.loadAttributeDistinctList),
            concatLatestFrom(() => this.store.select(datasetSelector.selectDatasetNameByRoute)),
            mergeMap(([action, datasetName]) => this.attributeDistinctService.retrieveAttributeDistinctList(datasetName, action.attribute)
                .pipe(
                    map(values => attributeDistinctActions.loadAttributeDistinctListSuccess({ values })),
                    catchError(() => of(attributeDistinctActions.loadAttributeDistinctListFail()))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private attributeDistinctService: AttributeDistinctService,
        private store: Store<{ }>
    ) {}
}
