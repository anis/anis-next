/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AttributeDistinctEffects } from './attribute-distinct.effects';
import { ColumnEffects } from './column.effects';
import { TableEffects } from './table.effects';
import { AdminFileExplorerEffects } from './admin-file-explorer.effects';
import { FitsImageEffects } from './fits-image.effects';
import { MonacoEditorEffects } from './monaco-editor.effects';

export const adminEffects = [
    AttributeDistinctEffects,
    ColumnEffects,
    TableEffects,
    AdminFileExplorerEffects,
    FitsImageEffects,
    MonacoEditorEffects
];
