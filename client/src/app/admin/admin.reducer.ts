/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { combineReducers, createFeatureSelector } from '@ngrx/store';

import * as attributeDistinct from './store/reducers/attribute-distinct.reducer';
import * as column from './store/reducers/column.reducer';
import * as table from './store/reducers/table.reducer';
import * as adminFileExplorer from './store/reducers/admin-file-explorer.reducer';
import * as fitsImage from './store/reducers/fits-image.reducer';
import * as monacoEditor from './store/reducers/monaco-editor.reducer';

/**
 * Interface for metamodel state.
 *
 * @interface State
 */
export interface State {
    attributeDistinct: attributeDistinct.State,
    column: column.State,
    table: table.State,
    adminFileExplorer: adminFileExplorer.State,
    fitsImage: fitsImage.State,
    monacoEditor: monacoEditor.State
}

const reducers = {
    attributeDistinct: attributeDistinct.attributeDistinctReducer,
    column: column.columnReducer,
    table: table.tableReducer,
    adminFileExplorer: adminFileExplorer.adminFileExplorerReducer,
    fitsImage: fitsImage.fitsImageReducer,
    monacoEditor: monacoEditor.monacoEditorReducer
};

export const adminReducer = combineReducers(reducers);
export const getAdminState = createFeatureSelector<State>('admin');