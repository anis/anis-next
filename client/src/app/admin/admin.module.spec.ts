/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AdminModule } from './admin.module';

describe('[admin] AdminModule', () => {
    it('Test admin module', () => {
        expect(AdminModule.name).toEqual('AdminModule');
    });
});

