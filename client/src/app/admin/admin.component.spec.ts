/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AppConfigService } from '../app-config.service';
import { StyleService } from '../shared/services/style.service';
import { AdminComponent } from './admin.component';
import * as authActions from 'src/app/auth/auth.actions';

describe('[admin] AdminComponent', () => {
    let component: AdminComponent;
    let fixture: ComponentFixture<AdminComponent>;
    let store: MockStore;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AdminComponent
            ],
            providers: [
                provideMockStore({}),
                { provide: AppConfigService, useValue: { authenticationEnabled: false } },
                { provide: StyleService, useValue: { setStyles: jest.fn() } }
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        })
        fixture = TestBed.createComponent(AdminComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        let favIcon: HTMLLinkElement;
        let body: HTMLBodyElement;
        let style: CSSStyleDeclaration;
        component.favIcon = { ...favIcon, href: '' }
        component.body = { ...body, style: { ...style, backgroundColor: '' } }
        fixture.detectChanges();
    });
    it('should create component', () => {
        expect(component).toBeTruthy();
    });
    it('getAuthenticationEnabled() should return false', () => {
        let result = component.getAuthenticationEnabled();
        expect(result).toBe(false);
    });
    it('login() should dispatch login action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.login();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.login({ redirectUri: window.location.toString() }));
    });
    it('logout() should dispatch logout action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.logout();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.logout());
    });
    it('openEditProfile() should dispatch logout action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.openEditProfile();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.openEditProfile());
    });
})

