/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { PortalRoutingModule, routedComponents } from './portal-routing.module';
import { dummiesComponents } from './components';

/**
 * @class
 * @classdesc Portal module.
 */
@NgModule({
    imports: [
        SharedModule,
        PortalRoutingModule
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ]
})
export class PortalModule { }
