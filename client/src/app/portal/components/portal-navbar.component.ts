/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { UserProfile } from 'src/app/auth/user-profile.model';
import { isAdmin } from 'src/app/shared/utils';

@Component({
    selector: 'app-portal-navbar',
    templateUrl: 'portal-navbar.component.html',
    styleUrls: [ 'portal-navbar.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortalNavbarComponent {
    @Input() isAuthenticated: boolean;
    @Input() userProfile: UserProfile = null;
    @Input() userRoles: string[];
    @Input() authenticationEnabled: boolean;
    @Input() adminRoles: string[];
    @Output() login: EventEmitter<any> = new EventEmitter();
    @Output() logout: EventEmitter<any> = new EventEmitter();
    @Output() openEditProfile: EventEmitter<any> = new EventEmitter();

    /**
     * Returns true if user is admin
     * 
     * @returns boolean
     */
    isAdmin() {
        return isAdmin(this.adminRoles, this.userRoles);
    }
}
