/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { InstanceCardComponent } from './instance-card.component';

describe('[Instance][Portal][Component] InstanceCardComponent', () => {
    let component: InstanceCardComponent;
    let fixture: ComponentFixture<InstanceCardComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [InstanceCardComponent]
        }).compileComponents();
        fixture = TestBed.createComponent(InstanceCardComponent);
        component = fixture.componentInstance;
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });
});
