/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Instance, InstanceGroup } from 'src/app/metamodel/models';
import { isAdmin } from 'src/app/shared/utils';

/**
 * @class
 * @classdesc Instance card component.
 */
@Component({
    selector: 'app-instance-card',
    templateUrl: 'instance-card.component.html',
    styleUrls: ['instance-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceCardComponent {
    @Input() instance: Instance;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];
    @Input() instanceGroupList: InstanceGroup[];
    @Input() apiUrl: string;

    /**
     * Returns the logo url.
     *
     * @return string
     */
    getLogoSrc(): string {
        return `${this.apiUrl}/instance/${this.instance.name}/file-explorer${this.instance.portal_logo}`;
    }

    isInstanceAccessible() {
        let accessible = true;

        if (this.authenticationEnabled && !this.instance.public && !this.isAdmin()) {
            accessible = false;
            if (this.isAuthenticated) {
                accessible = this.instanceGroupList
                    .filter(instanceGroup => instanceGroup.instances.includes(this.instance.name))
                    .filter(instanceGroup => this.userRoles.includes(instanceGroup.role))
                    .length > 0;
            }
        }

        return accessible;
    }

    /**
     * Returns true if user is admin
     * 
     * @returns boolean
     */
    isAdmin() {
        return isAdmin(this.adminRoles, this.userRoles);
    }
}
