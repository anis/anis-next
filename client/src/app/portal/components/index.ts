/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceCardComponent } from './instance-card.component';
import { PortalNavbarComponent } from './portal-navbar.component';

export const dummiesComponents = [
    InstanceCardComponent,
    PortalNavbarComponent
];
