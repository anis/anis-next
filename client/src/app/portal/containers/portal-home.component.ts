/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { UserProfile } from 'src/app/auth/user-profile.model';
import { Instance, InstanceGroup } from 'src/app/metamodel/models';
import * as authActions from 'src/app/auth/auth.actions';
import * as authSelector from 'src/app/auth/auth.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';
import { AppConfigService } from 'src/app/app-config.service';
import { StyleService } from 'src/app/shared/services/style.service';

/**
 * @class
 * @classdesc Portal home container.
 *
 * @implements OnInit
 * @implements OnDestroy
 */
@Component({
    selector: 'app-portal-home',
    templateUrl: 'portal-home.component.html'
})
export class PortalHomeComponent implements OnInit {
    public favIcon: HTMLLinkElement = document.querySelector('#favicon');
    public body: HTMLBodyElement = document.querySelector('body');
    public isAuthenticated: Observable<boolean>;
    public userProfile: Observable<UserProfile>;
    public userRoles: Observable<string[]>;
    public instanceList: Observable<Instance[]>;
    public instanceGroupList: Observable<InstanceGroup[]>;
    public userRolesSubscription: Subscription;

    constructor(private store: Store<{ }>, private config: AppConfigService, private style: StyleService) {
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.userProfile = store.select(authSelector.selectUserProfile);
        this.userRoles = store.select(authSelector.selectUserRoles);
        this.instanceList = store.select(instanceSelector.selectAllInstances);
        this.instanceGroupList = store.select(instanceGroupSelector.selectAllInstanceGroups);
    }

    ngOnInit() {
        this.favIcon.href = 'favicon.ico';
        this.body.style.backgroundColor = 'white';
        this.style.setStyles('.footer', {
            'background-color': '#F8F9FA',
            'border-top': 'none',
            'color': 'black',
        });
    }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }

    /**
     * Returns admin roles list
     * 
     * @returns string[]
     */
    getAdminRoles(): string[] {
        return this.config.adminRoles;
    }

    /**
     * Returns ANIS Server API URL
     * 
     * @returns string
     */
    getApiUrl(): string {
        return this.config.apiUrl;
    }

    /**
     * Dispatches action to log in.
     */
    login(): void {
        this.store.dispatch(authActions.login({ redirectUri: window.location.toString() }));
    }

    /**
     * Dispatches action to log out.
     */
    logout(): void {
        this.store.dispatch(authActions.logout());
    }

    /**
     * Dispatches action to open profile editor.
     */
    openEditProfile(): void {
        this.store.dispatch(authActions.openEditProfile());
    }
}
