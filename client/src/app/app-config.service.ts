/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

@Injectable()
export class AppConfigService {
    public apiUrl: string;
    public servicesUrl: string;
    public baseHref: string;
    public authenticationEnabled: boolean;
    public ssoAuthUrl: string;
    public ssoRealm: string;
    public ssoClientId: string;
    public adminRoles: string[];
    public matomoEnabled: boolean;
    public matomoSiteId: number;
    public matomoTrackerUrl: string;
}
