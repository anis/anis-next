/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

export const register = createAction('[Samp] Register');
export const registerSuccess = createAction('[Samp] Register Success');
export const registerFail = createAction('[Samp] Register Fail');
export const unregister = createAction('[Samp] Unregister');
export const broadcastVotable = createAction('[Samp] Broadcast Votable', props<{ url: string }>());
export const broadcastImage = createAction('[Samp] Broadcast Image', props<{ url: string }>());
