/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { sampReducer } from './samp.reducer';
import { SampEffects } from './samp.effects';
import { SampService } from './samp.service';

@NgModule({
    imports: [
        StoreModule.forFeature('samp', sampReducer),
        EffectsModule.forFeature([ SampEffects ])
    ],
    providers: [
        SampService
    ]
})
export class SampModule { }
