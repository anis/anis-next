/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as sampSelector from './samp.selector';
import * as fromSamp from './samp.reducer';

describe('[Instance][Store] Samp selector', () => {
    it('should get registered', () => {
        const state = { samp: { ...fromSamp.initialState }};
        expect(sampSelector.selectRegistered(state)).toBeFalsy();
    });
});
