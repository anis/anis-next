/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as sampActions from './samp.actions';

/**
 * Interface for samp state.
 *
 * @interface State
 */
export interface State {
    registered: boolean;
}

export const initialState: State = {
    registered: false
};

export const sampReducer = createReducer(
    initialState,
    on(sampActions.registerSuccess, state => ({
        ...state,
        registered: true
    })),
    on(sampActions.unregister, state => ({
        ...state,
        registered: false
    }))
);

export const selectRegistered = (state: State) => state.registered;
