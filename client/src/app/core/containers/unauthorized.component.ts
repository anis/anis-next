/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as authActions from 'src/app/auth/auth.actions';
import * as authSelector from 'src/app/auth/auth.selector';

/**
 * @class
 * @classdesc Unauthorized container.
 */
@Component({
    selector: 'app-unauthorized',
    templateUrl: 'unauthorized.component.html'
})
export class UnauthorizedComponent {
    public isAuthenticated: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.isAuthenticated = this.store.select(authSelector.selectIsAuthenticated);
    }

    login() {
        let redirectUri = sessionStorage.getItem('redirect_uri');
        if (!redirectUri) {
            redirectUri = '/';
        }
        this.store.dispatch(authActions.login({ redirectUri }));
    }
}
