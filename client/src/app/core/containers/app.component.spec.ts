/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { AppComponent } from './app.component';
import { AppConfigService } from 'src/app/app-config.service';
import * as instanceActions from '../../metamodel/actions/instance.actions';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';

describe('[Core] AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let store: MockStore;
    let config: AppConfigService

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                AppComponent
            ],
            providers: [
                provideMockStore({ }),
                AppConfigService
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        config = TestBed.inject(AppConfigService);
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(2);
            expect(spy).toHaveBeenCalledWith(instanceActions.loadInstanceList());
            expect(spy).toHaveBeenCalledWith(instanceGroupActions.loadInstanceGroupList());
            done();
        });
    });
});
