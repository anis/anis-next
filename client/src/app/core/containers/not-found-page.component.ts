/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

/**
 * @class
 * @classdesc Not found page container.
 */
@Component({
    selector: 'app-not-found-page',
    templateUrl: 'not-found-page.component.html'
})
export class NotFoundPageComponent { }
