/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './containers/app.component';
import { NotFoundPageComponent } from './containers/not-found-page.component';
import { UnauthorizedComponent } from './containers/unauthorized.component';

export const COMPONENTS = [
    AppComponent,
    NotFoundPageComponent,
    UnauthorizedComponent
];

/**
 * @class
 * @classdesc Core module.
 */
@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ToastrModule.forRoot()
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only'
            );
        }
    }
}
