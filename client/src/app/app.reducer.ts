/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ActionReducerMap, ActionReducer, MetaReducer } from '@ngrx/store'; 
import { RouterReducerState, routerReducer } from '@ngrx/router-store';

import { environment } from 'src/environments/environment';

export interface State {
    router: RouterReducerState;
}
  
export const reducers: ActionReducerMap<State> = {
    router: routerReducer
};

// console.log all actions
export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
    return function(state, action) {
        console.group(action.type);
        const nextState = reducer(state, action);
        console.log(`%c prev state`, `color: #9E9E9E; font-weight: bold`, state);
        console.log(`%c action`, `color: #03A9F4; font-weight: bold`, action);
        console.log(`%c next state`, `color: #4CAF50; font-weight: bold`, nextState);
        console.groupEnd();

        return nextState;
    };
}

export const metaReducers: MetaReducer<any>[] = !environment.production
    ? [debug]
    : [];
