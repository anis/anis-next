/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundPageComponent } from './core/containers/not-found-page.component';
import { UnauthorizedComponent } from './core/containers/unauthorized.component';

const routes: Routes = [
    { path: '', redirectTo: 'portal', pathMatch: 'full' },
    { path: 'portal', loadChildren: () => import('./portal/portal.module').then(m => m.PortalModule) },
    { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },
    { path: 'instance', loadChildren: () => import('./instance/instance.module').then(m => m.InstanceModule) },
    { path: 'unauthorized', component: UnauthorizedComponent, title: 'ANIS - Unauthorized page' },
    { path: '**', component: NotFoundPageComponent, title: 'ANIS - 404 not found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'enabled'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
