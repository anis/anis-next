/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { APP_INITIALIZER } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { KeycloakService } from 'keycloak-angular';
import { Store } from '@ngrx/store';
import { firstValueFrom } from 'rxjs';
import { MatomoInitializerService } from '@ngx-matomo/tracker';

import { AppConfigService } from './app-config.service';
import { initializeKeycloak } from 'src/app/auth/init.keycloak';
import { environment } from 'src/environments/environment';

function appInit(http: HttpClient, appConfigService: AppConfigService, keycloak: KeycloakService, store: Store<{ }>, matomo: MatomoInitializerService) {
    return () => {
        const source$ = http.get(getClientSettingsUrl());

        return firstValueFrom(source$)
            .then(data => {
                Object.assign(appConfigService, data);
                appConfigService.apiUrl = environment.apiUrl;
                appConfigService.adminRoles = data['adminRoles'].split(',');

                if (appConfigService.matomoEnabled) {
                    matomo.initializeTracker({
                        siteId: appConfigService.matomoSiteId,
                        trackerUrl: appConfigService.matomoTrackerUrl
                    });
                }
                
                return initializeKeycloak(keycloak, store, appConfigService)
            });
    }
}

function getClientSettingsUrl() {
    let url: string;
    if (environment.production) {
        url = `${document.getElementsByTagName('base')[0].getAttribute('href')}${environment.apiUrl}/client-settings`;
    } else {
        url = `${environment.apiUrl}/client-settings`;
    }
    return url;
}

export const appInitializer = {
    provide: APP_INITIALIZER,
    useFactory: appInit,
    multi: true,
    deps: [ HttpClient, AppConfigService, KeycloakService, Store, MatomoInitializerService ]
};
