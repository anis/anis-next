/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HookParser, HookPosition, HookValue, HookComponentData, HookBindings, HookFinder } from 'ngx-dynamic-hooks';

import { DynamicImageComponent } from '../dynamic-components/dynamic-image.component';

@Injectable()
export class DynamicImageParser implements HookParser {
    constructor(private hookFinder: HookFinder) { }

    public findHooks(content: string, context: any): Array<HookPosition> {
        const imageRegex = /<img [^>]*src="[^"]*"[^>]*>/gm;

        return this.hookFinder.findStandaloneHooks(content, imageRegex);
    }

    public loadComponent(hookId: number, hookValue: HookValue, context: any, childNodes: Array<Element>): HookComponentData {
        return {
            component: DynamicImageComponent
        };
    }

    public getBindings(hookId: number, hookValue: HookValue, context: any): HookBindings {
        const img = hookValue.openingTag;

        const srcAttrMatch = img.match(new RegExp('\\s+src\=\\"([^\\"]*?)\\"', 'im'));
        let src = srcAttrMatch[1];

        let css = null;
        const classAttrMatch = img.match(new RegExp('\\s+class\=\\"([^\\"]*?)\\"', 'im'));
        if (classAttrMatch) {
            css = classAttrMatch[1];
        }

        let alt = null;
        const altAttrMatch = img.match(new RegExp('\\s+alt\=\\"([^\\"]*?)\\"', 'im'));
        if (altAttrMatch) {
            alt = altAttrMatch[1];
        }

        return {
            inputs: {
                src,
                css,
                alt,
                instance: context.instance
            }
        };
    }
}
