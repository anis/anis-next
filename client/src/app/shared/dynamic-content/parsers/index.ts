/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DynamicRouterLinkParser } from './dynamic-router-link-parser';
import { DynamicImageParser } from './dynamic-image-parser';

export const hookParsers = [
    DynamicRouterLinkParser,
    DynamicImageParser
];
