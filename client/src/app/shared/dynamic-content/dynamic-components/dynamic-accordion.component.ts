/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-dynamic-accordion',
    templateUrl: 'dynamic-accordion.component.html'
})
export class DynamicAccordionComponent {
    @Input() label: string;
    @Input() isOpen: boolean;
}
