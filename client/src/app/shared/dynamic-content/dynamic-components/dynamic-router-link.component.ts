/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-dynamic-router-link',
    templateUrl: 'dynamic-router-link.component.html',
    styleUrls: [ 'dynamic-router-link.component.scss' ]
})
export class DynamicRouterLinkComponent {
    @Input() link: string;
    @Input() queryParams: {[key: string]: any};
    @Input() anchorFragment: string;
    @Input() css: string;
    @Input() target: string;

    isInternalLink() {
        return this.link.startsWith('/instance/');
    }

    isExternalLink() {
        return this.link.startsWith('http') || this.link.startsWith('mailto');
    }
}
