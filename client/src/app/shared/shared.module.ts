/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { DynamicHooksModule } from 'ngx-dynamic-hooks';

import { sharedComponents } from './components';
import { sharedPipes } from './pipes';
import { hookParsers, dynamicComponents } from './dynamic-content';
import { sharedServices } from './services';

/**
 * @class
 * @classdesc Shared module.
 */
@NgModule({
    declarations: [
        sharedComponents,
        sharedPipes,
        dynamicComponents,
    ],
    providers: [
        hookParsers,
        sharedServices
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        AccordionModule.forRoot(),
        PopoverModule.forRoot(),
        TooltipModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TabsModule.forRoot(),
        PaginationModule.forRoot(),
        ProgressbarModule.forRoot(),
        NgSelectModule,
        NgxJsonViewerModule,
        DynamicHooksModule.forRoot({}),
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BsDropdownModule,
        ModalModule,
        AccordionModule,
        PopoverModule,
        TooltipModule,
        BsDatepickerModule,
        TabsModule,
        PaginationModule,
        ProgressbarModule,
        NgSelectModule,
        NgxJsonViewerModule,
        DynamicHooksModule,
        sharedComponents,
        sharedPipes,
        dynamicComponents
    ]
})
export class SharedModule { }
