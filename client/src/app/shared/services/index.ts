import { StyleService } from './style.service';
import { PrismService } from './prism.service';

export const sharedServices = [
    StyleService,
    PrismService
];
