/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SpinnerComponent } from './spinner.component';
import { FooterComponent } from './footer.component';

import { AttributeLabelComponent } from './attribute-label.component';

export const sharedComponents = [
    SpinnerComponent,
    FooterComponent,
    AttributeLabelComponent,
];
