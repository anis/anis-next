/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnInit,
} from '@angular/core';

/**
 * @class
 * @classdesc Attribute label component. It chooses a label for a datum based
 * on the attributes of Attribute.
 * @todo Make the choice of labels more robust on the server side. A temporary
 * fix for naming data has been brought to the component on init, but the patch
 * is not robust.
 * @implements `OnInit`
 */
@Component({
    selector: 'app-attribute-label',
    templateUrl: 'attribute-label.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AttributeLabelComponent implements OnInit {
    @Input() label: string;
    @Input() description: string;
    @Input() form_label?: string;
    name: string = null;

    /**
     * @method Choose the name of the attribute by setting the property `this.name`
     * to `this.form_label` is present else to `this.label`.
     * This method enables to set the name of a datum according to the property `form_label` of
     * an `Attribute` rather than to the property `label` by default.
     */
    ngOnInit() {
        this.name = this.form_label ? this.form_label : this.label;
    }
}
