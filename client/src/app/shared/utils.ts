/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Dataset, DatasetGroup } from 'src/app/metamodel/models';

/**
 * Returns strict url address.
 *
 * @return string
 *
 * @example
 * const url: string = `${getHost()}/following-url/`;
 */
export const getHost = (apiUrl: string): string => {
    if (!apiUrl.startsWith('http')) {
        const url = window.location;
        return `${url.protocol}//${url.host}${document.getElementsByTagName('base')[0].getAttribute('href')}${apiUrl}`;
    }
    return apiUrl;
}

/**
 * Returns true if user is admin
 * 
 * @returns boolean
 */
export const isAdmin = (adminRoles: string[], userRoles: string[]): boolean => {
    let admin = false;
    for (let i = 0; i < adminRoles.length; i++) {
        admin = userRoles.includes(adminRoles[i]);
        if (admin) break;
    }

    return admin;
}

/**
 * Returns true a dataset is accessible by the user
 * 
 * @returns boolean
 */
export const isDatasetAccessible = (
    dataset: Dataset,
    authenticationEnabled: boolean,
    isAuthenticated: boolean,
    datasetGroupList: DatasetGroup[],
    adminRoles: string[],
    userRoles: string[]) => {
    let accessible = true;

    if (authenticationEnabled && !dataset.public && !isAdmin(adminRoles, userRoles)) {
        accessible = false;
        if (isAuthenticated) {
            accessible = datasetGroupList
                .filter(datasetGroup => datasetGroup.datasets.includes(dataset.name))
                .filter(datasetGroup => userRoles.includes(datasetGroup.role))
                .length > 0;
        }
    }

    return accessible;
}

export interface SearchTypeOperator {
    value: string;
    label: string;
}

/**
 * Returns the search type operators list
 * 
 * @returns SearchTypeOperator[]
 */
export const searchTypeOperators: SearchTypeOperator[] = [
    { value: 'eq', label: '=' },
    { value: 'neq', label: '≠' },
    { value: 'gt', label: '>' },
    { value: 'gte', label: '>=' },
    { value: 'lt', label: '<' },
    { value: 'lte', label: '<=' },
    { value: 'lk', label: 'like' },
    { value: 'nlk', label: 'not like' },
    { value: 'in', label: 'in' },
    { value: 'nin', label: 'not in' },
    { value: 'nl', label: 'null' },
    { value: 'nnl', label: 'not null' },
];
