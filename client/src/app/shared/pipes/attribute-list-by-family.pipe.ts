/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Returns attributes corresponding to the given criteria family ID.
 *
 * @example
 * // returns attributes that matching with the criteria family ID among the attribute list
 * {{ attributeList | attributesListByFamily:1 }}
 */
@Pipe({ name: 'attributeListByFamily' })
export class AttributeListByFamilyPipe implements PipeTransform {
    transform(attributeList: Attribute[], idCriteriaFamily: number): Attribute[] {
        return attributeList.filter(attribute => attribute.id_criteria_family === idCriteriaFamily);
    }
}
