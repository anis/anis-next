/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetByNamePipe } from './dataset-by-name.pipe';
import { DATASET_LIST } from '../../../test-data';

describe('[Shared][Pipes] DatasetByNamePipe', () => {
    let pipe = new DatasetByNamePipe();

    it('should return dataset corresponding to the given name', () => {
        expect(pipe.transform(DATASET_LIST, 'myDataset')).toEqual(DATASET_LIST[0]);
    });
});
