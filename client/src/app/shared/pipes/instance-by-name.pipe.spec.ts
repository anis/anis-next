/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceByNamePipe } from './instance-by-name.pipe';
import { INSTANCE_LIST } from '../../../test-data';

describe('[Shared][Pipes] InstanceByNamePipe', () => {
    let pipe = new InstanceByNamePipe();

    it('should return survey corresponding to the given name', () => {
        expect(pipe.transform(INSTANCE_LIST, 'myOtherInstance')).toEqual(INSTANCE_LIST[0]);
    });
});
