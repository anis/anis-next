/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

import { Instance } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Returns instance corresponding to the given name.
 *
 * @example
 * // returns instance object that matching with the 'myInstance' name among the instance list
 * {{ instanceList | instanceByName:'myInstance' }}
 */
@Pipe({ name: 'instanceByName' })
export class InstanceByNamePipe implements PipeTransform {
    transform(instanceList: Instance[], instanceName: string): Instance {
        return instanceList.find(instance => instance.name === instanceName);
    }
}
