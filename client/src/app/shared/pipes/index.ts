/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetListByFamilyPipe } from './dataset-list-by-family.pipe';
import { AttributeListByFamilyPipe } from './attribute-list-by-family.pipe';
import { OutputFamilyByIdPipe } from './output-family-by-id.pipe';
import { DatasetByNamePipe } from './dataset-by-name.pipe';
import { InstanceByNamePipe } from './instance-by-name.pipe';
import { AuthImagePipe } from './auth-image.pipe';
import { WebpageListByFamilyPipe } from './webpage-list-by-family.pipe';

export const sharedPipes = [
    DatasetListByFamilyPipe,
    AttributeListByFamilyPipe,
    OutputFamilyByIdPipe,
    DatasetByNamePipe,
    InstanceByNamePipe,
    AuthImagePipe,
    WebpageListByFamilyPipe
];
