/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AttributeListByFamilyPipe } from './attribute-list-by-family.pipe';
import { ATTRIBUTE_LIST } from '../../../test-data';

describe('[Shared][Pipes] AttributeListByFamilyPipe', () => {
    let pipe = new AttributeListByFamilyPipe();

    it('should return attributes corresponding to the given criteria family ID', () => {
        expect(pipe.transform(ATTRIBUTE_LIST, 1).length).toEqual(1);
        expect(pipe.transform(ATTRIBUTE_LIST, 1)).toContain(ATTRIBUTE_LIST[1]);
    });
});
