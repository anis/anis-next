/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { lastValueFrom } from 'rxjs';

@Pipe({ name: 'authImage' })
export class AuthImagePipe implements PipeTransform {
    constructor(private http: HttpClient) { }

    async transform(src: string): Promise<string> {
        try {
            const get$ = this.http.get(src, { responseType: 'blob' });
            const imageBlob = await lastValueFrom(get$);
            const reader = new FileReader();
            return new Promise((resolve, reject) => {
                reader.onloadend = () => resolve(reader.result as string);
                reader.readAsDataURL(imageBlob);
            });
        } catch {
            return 'assets/fallback.png';
        }
    }
}
