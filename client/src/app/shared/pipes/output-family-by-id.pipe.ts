/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

import { OutputFamily } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Returns output family corresponding to the given ID.
 *
 * @example
 * // returns output family object that matching with the ID 1 among the output family list
 * {{ outputFamilyList | outputFamilyById:1 }}
 */
@Pipe({ name: 'outputFamilyById' })
export class OutputFamilyByIdPipe implements PipeTransform {
    transform(outputFamilyList: OutputFamily[], outputFamilyId: number): OutputFamily {
        return outputFamilyList.find(outputFamily => outputFamily.id === outputFamilyId);
    }
}
