/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

import { Webpage } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Returns webpages corresponding to the given webpage family ID.
 *
 * @example
 * // returns webpages that matching with the webpage family ID among the webpage list
 * {{ webpageList | webpageListByFamily:1 }}
 */
@Pipe({ name: 'webpageListByFamily' })
export class WebpageListByFamilyPipe implements PipeTransform {
    transform(webpageList: Webpage[], idWebpageFamily: number): Webpage[] {
        return webpageList.filter(webpage => webpage.id_webpage_family === idWebpageFamily);
    }
}
