/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { from } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';

import { KeycloakService } from 'keycloak-angular';

import * as authActions from './auth.actions';
import { AppConfigService } from 'src/app/app-config.service';
 
@Injectable()
export class AuthEffects {
    login$ = createEffect(() =>
        this.actions$.pipe(
            ofType(authActions.login),
            tap(action => this.keycloak.login({ redirectUri: action.redirectUri }))    
        ),
        { dispatch: false }
    );

    logout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(authActions.logout),
            tap(_ => {
                let redirectUri = window.location.origin;
                if (this.config.baseHref !== '/') {
                    redirectUri = this.config.baseHref + redirectUri;
                }
                this.keycloak.logout(redirectUri);
            })        
        ),
        { dispatch: false }
    );

    authSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(authActions.authSuccess),
            switchMap(() => from(this.keycloak.loadUserProfile())
                .pipe(
                    switchMap(userProfile => [
                        authActions.loadUserProfileSuccess({ userProfile }),
                        authActions.loadUserRolesSuccess({ userRoles: this.keycloak.getUserRoles() }),
                    ])
                )
            )
        )
    );

    openEditProfile$ =  createEffect(() =>
        this.actions$.pipe(
            ofType(authActions.openEditProfile),
            tap(_ => window.open(`${this.config.ssoAuthUrl}/realms/${this.config.ssoRealm}/account`, '_blank'))        
        ),
        { dispatch: false }
    );
 
    constructor(
        private actions$: Actions,
        private keycloak: KeycloakService,
        private config: AppConfigService
    ) {}
}
