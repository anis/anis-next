/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as fromAuth from './auth.reducer';

export const selectAuthState = createFeatureSelector<fromAuth.State>('auth');

export const selectIsAuthenticated = createSelector(
    selectAuthState,
    fromAuth.selectIsAuthenticated
);

export const selectUserProfile = createSelector(
    selectAuthState,
    fromAuth.selectUserProfile
);

export const selectUserRoles = createSelector(
    selectAuthState,
    fromAuth.selectUserRoles
);
