/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as authActions from './auth.actions';
import { UserProfile } from './user-profile.model';

export interface State {
    isAuthenticated: boolean;
    userProfile: UserProfile;
    userRoles: string[];
}

export const initialState: State = {
    isAuthenticated: false,
    userProfile: null,
    userRoles: []
};

export const authReducer = createReducer(
    initialState,
    on(authActions.authSuccess, state => ({
        ...state,
        isAuthenticated: true
    })),
    on(authActions.loadUserProfileSuccess, (state, { userProfile }) => ({
        ...state,
        userProfile
    })),
    on(authActions.loadUserRolesSuccess, (state, { userRoles }) => ({
        ...state,
        userRoles
    }))
);

export const selectIsAuthenticated = (state: State) => state.isAuthenticated;
export const selectUserProfile = (state: State) => state.userProfile;
export const selectUserRoles = (state: State) => state.userRoles;
