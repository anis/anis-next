/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { from } from 'rxjs';

import { KeycloakService, KeycloakEventType } from 'keycloak-angular';
import { Store } from '@ngrx/store';

import * as keycloakActions from './auth.actions';
import { AppConfigService } from '../app-config.service';

export function initializeKeycloak(keycloak: KeycloakService, store: Store<{ }>, appConfigService: AppConfigService) {
    if (!appConfigService.authenticationEnabled) {
        return Promise.resolve(true);
    }

    from(keycloak.keycloakEvents$).subscribe(event => {
        if (event.type === KeycloakEventType.OnAuthLogout) {
            store.dispatch(keycloakActions.logout());
        }
        if (event.type === KeycloakEventType.OnAuthSuccess) {
            store.dispatch(keycloakActions.authSuccess());
        }
        if (event.type === KeycloakEventType.OnAuthRefreshError) {
            store.dispatch(keycloakActions.login({ redirectUri: window.location.toString() }));
        }
    })

    let silentCheckSsoRedirectUri = window.location.origin;
    if (appConfigService.baseHref != '/') {
        silentCheckSsoRedirectUri += appConfigService.baseHref;
    }
    silentCheckSsoRedirectUri += '/assets/silent-check-sso.html';

    return keycloak.init({
        config: {
            url: appConfigService.ssoAuthUrl,
            realm: appConfigService.ssoRealm,
            clientId: appConfigService.ssoClientId,
        },
        initOptions: {
            onLoad: 'check-sso',
            silentCheckSsoRedirectUri
        },
        loadUserProfileAtStartUp: true,
        bearerExcludedUrls: ['https://cdsweb.u-strasbg.fr/']
    });
}
