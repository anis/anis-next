/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { KeycloakAngularModule } from 'keycloak-angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { authReducer } from './auth.reducer';
import { AuthEffects } from './auth.effects';

@NgModule({
    imports: [
        KeycloakAngularModule,
        StoreModule.forFeature('auth', authReducer),
        EffectsModule.forFeature([ AuthEffects ])
    ]
})
export class AuthModule { }
