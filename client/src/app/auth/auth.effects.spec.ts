/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import * as authActions from './auth.actions';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { AuthEffects } from './auth.effects';
import { AppConfigService } from '../app-config.service';
import { KeycloakService } from 'keycloak-angular';

describe('auth] AuthEffects', () => {
    let actions = new Observable();
    let effects: AuthEffects;
    let appConfigService: AppConfigService;
    let keycloakService: KeycloakService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AuthEffects,
                AppConfigService,
                { provide: Router, useValue: { navigate: jest.fn() } },
                KeycloakService,
                provideMockActions(() => actions),
            ]
        }).compileComponents();
        effects = TestBed.inject(AuthEffects);
        appConfigService = TestBed.inject(AppConfigService);
        keycloakService = TestBed.inject(KeycloakService);
        router = TestBed.inject(Router);
    });
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
    it('login$ should call keycloak.login()', () => {
        const action = authActions.login({ redirectUri: 'test' });
        let spy = jest.spyOn(keycloakService, 'login');
        actions = hot('a', { a: action });
        const expected = cold('a', { a: action });
        expect(effects.login$).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('logout$ should call keycloak.logout()', () => {
        const action = authActions.logout();
        let spy = jest.spyOn(keycloakService, 'logout');
        actions = hot('a', { a: action });
        const expected = cold('a', { a: action });
        expect(effects.logout$).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('authSuccess$ should call keycloak.loadUserProfile()', () => {
        keycloakService.loadUserProfile = jest.fn().mockImplementation(() => of({}));
        keycloakService.getUserRoles = jest.fn().mockImplementation(() => ['test']);
        const action = authActions.authSuccess();
        actions = hot('-a', { a: action });
        const expected = cold('-(bc)', {
            b: authActions.loadUserProfileSuccess({ userProfile: {} }),
            c: authActions.loadUserRolesSuccess({ userRoles: keycloakService.getUserRoles() }),
        })
        expect(effects.authSuccess$).toBeObservable(expected);
    });
    it('openEditProfile$ should call window.open()', () => {
        const action = authActions.openEditProfile();
        let spy = jest.spyOn(window, 'open');
        actions = hot('a', { a: action });
        const expected = cold('a', { a: action });
        expect(effects.openEditProfile$).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
