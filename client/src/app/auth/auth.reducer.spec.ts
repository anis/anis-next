
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fromAuth from './auth.reducer';
import * as authActions from './auth.actions';
import { UserProfile } from './user-profile.model';
import { Action } from '@ngrx/store';

describe('[auth] auth reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromAuth;
        const action = { type: 'Unknown' };
        const state = fromAuth.authReducer(initialState, action);
        expect(state).toBe(initialState);
    });
    it('authSuccess action should set isAuthenticated to true', () => {
        const { initialState } = fromAuth;
        const action = authActions.authSuccess();
        const state = fromAuth.authReducer(initialState, action);
        expect(state.isAuthenticated).toBe(true);
        expect(state).not.toBe(initialState);
    });
    it('loadUserProfileSuccess action should set userProfile to new value', () => {
        const { initialState } = fromAuth;
        let userProfile: UserProfile = {
            email: 'test',
            emailVerified: true
        }
        const action = authActions.loadUserProfileSuccess({ userProfile });
        const state = fromAuth.authReducer(initialState, action);
        expect(state.userProfile).toEqual(userProfile);
        expect(state).not.toBe(initialState);
    });
    it('loadUserRolesSuccess action should set userRoles to new value', () => {
        const { initialState } = fromAuth;
        const action = authActions.loadUserRolesSuccess({userRoles: ['test']});
        const state = fromAuth.authReducer(initialState, action);
        expect(state.userRoles).toEqual(['test']);
        expect(state).not.toBe(initialState);
    });
    it('should get isAuthenticated', () => {
        const action = {} as Action;
        const state = fromAuth.authReducer(undefined, action);
        expect(fromAuth.selectIsAuthenticated(state)).toEqual(false);
    });
    it('should get userProfile', () => {
        const action = {} as Action;
        const state = fromAuth.authReducer(undefined, action);
        expect(fromAuth.selectUserProfile(state)).toEqual(null);
    });
    it('should getuserRoles', () => {
        const action = {} as Action;
        const state = fromAuth.authReducer(undefined, action);
        expect(fromAuth.selectUserRoles(state)).toEqual([]);
    });


});