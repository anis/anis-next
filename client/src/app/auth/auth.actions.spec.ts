
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as authActions from "./auth.actions"
import { UserProfile } from "./user-profile.model";

describe('auth actions', () => {
    it('should create login action', () => {
        let action = authActions.login({ redirectUri: 'test' });
        expect(action).toBeTruthy();
        expect(authActions.login.type).toEqual('[Auth] Login');
    });
    it('should create logout action', () => {
        let action = authActions.logout();
        expect(action).toBeTruthy();
        expect(authActions.logout.type).toEqual('[Auth] Logout');
    });
    it('should create authSuccess action', () => {
        let action = authActions.authSuccess();

        expect(action).toBeTruthy();
        expect(authActions.authSuccess.type).toEqual('[Auth] Auth Success');
    });
    it('should create loadUserProfileSuccess action', () => {
        let userProfile: UserProfile = {
            createdTimestamp: 10,
            email: 'test@test.com',
            emailVerified: true,
            enabled: true,
            firstName: 'test',
            id: '',
            lastName: 'test',
            totp: false,
            username: 'test'
        }
        let action = authActions.loadUserProfileSuccess({ userProfile });
        expect(action).toBeTruthy();
        expect(authActions.loadUserProfileSuccess.type).toEqual('[Auth] Load User Profile Success');
    });
    it('should create loadUserRolesSuccess action', () => {
        let userRoles: string[] = ['test1', 'test2'];
        let action = authActions.loadUserRolesSuccess({ userRoles });
        expect(action).toBeTruthy();
        expect(authActions.loadUserRolesSuccess.type).toEqual('[Auth] Load User Roles Success');
    });
    it('should create openEditProfile action', () => {

        let action = authActions.openEditProfile();
        expect(action).toBeTruthy();
        expect(authActions.openEditProfile.type).toEqual('[Auth] Edit Profile');
    });
})