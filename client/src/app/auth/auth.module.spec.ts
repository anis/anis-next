
/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AuthModule } from "./auth.module"

describe('[auth] AuthModule', () => {
    it('should test auth module', () => {
        expect(AuthModule.name).toBe('AuthModule');
    })
})