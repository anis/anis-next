/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { UserProfile } from './user-profile.model';

export const login = createAction('[Auth] Login', props<{ redirectUri: string }>());
export const logout = createAction('[Auth] Logout');
export const authSuccess = createAction('[Auth] Auth Success');
export const loadUserProfileSuccess = createAction('[Auth] Load User Profile Success', props<{ userProfile: UserProfile }>());
export const loadUserRolesSuccess = createAction('[Auth] Load User Roles Success', props<{ userRoles: string[] }>());
export const openEditProfile = createAction('[Auth] Edit Profile');
