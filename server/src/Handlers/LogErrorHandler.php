<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Handlers;

use Slim\Handlers\ErrorHandler;
use Psr\Log\LoggerInterface;

/**
 * Handler to log eror information with psr-3 logger system
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Handlers
 */
class LogErrorHandler extends ErrorHandler
{
    /**
     * The logger interface is the central access point to log information
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Set the concrete logger api based on psr-3 logger interface
     *
     * @param LoggerInterface $logger PSR-3 logger interface
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * Log error information
     *
     * @param string $error Error information
     */
    protected function logError(string $error): void
    {
        $this->logger->error($error);
    }
}
