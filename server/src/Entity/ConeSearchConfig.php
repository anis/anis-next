<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="cone_search")
 */
class ConeSearchConfig implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="enabled", nullable=false)
     */
    protected $enabled;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="opened", nullable=false)
     */
    protected $opened;

    /**
     * @var int
     *
     * @Column(type="integer", name="column_ra", nullable=true)
     */
    protected $columnRa;

    /**
     * @var int
     *
     * @Column(type="integer", name="column_dec", nullable=true)
     */
    protected $columnDec;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="resolver_enabled", nullable=false, options={"default" : true})
     */
    protected $resolverEnabled;

    /**
     * @var float
     *
     * @Column(type="float", name="default_ra", nullable=true)
     */
    protected $defaultRa;

    /**
     * @var float
     *
     * @Column(type="float", name="default_dec", nullable=true)
     */
    protected $defaultDec;

    /**
     * @var float
     *
     * @Column(type="float", name="default_radius", nullable=false, options={"default" : "2.0"}))
     */
    protected $defaultRadius;

    /**
     * @var string
     *
     * @Column(type="string", name="default_ra_dec_unit", nullable=false, options={"default" : "degree"}))
     */
    protected $defaultRaDecUnit;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="plot_enabled", nullable=false)
     */
    protected $plotEnabled;

    public function getId()
    {
        return $this->id;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public function getOpened()
    {
        return $this->opened;
    }

    public function setOpened($opened)
    {
        $this->opened = $opened;
    }

    public function getColumnRa()
    {
        return $this->columnRa;
    }

    public function setColumnRa($columnRa)
    {
        $this->columnRa = $columnRa;
    }

    public function getColumnDec()
    {
        return $this->columnDec;
    }

    public function setColumnDec($columnDec)
    {
        $this->columnDec = $columnDec;
    }

    public function getResolverEnabled()
    {
        return $this->resolverEnabled;
    }

    public function setResolverEnabled($resolverEnabled)
    {
        $this->resolverEnabled = $resolverEnabled;
    }

    public function getDefaultRa()
    {
        return $this->defaultRa;
    }

    public function setDefaultRa($defaultRa)
    {
        $this->defaultRa = $defaultRa;
    }

    public function getDefaultDec()
    {
        return $this->defaultDec;
    }

    public function setDefaultDec($defaultDec)
    {
        $this->defaultDec = $defaultDec;
    }

    public function getDefaultRadius()
    {
        return $this->defaultRadius;
    }

    public function setDefaultRadius($defaultRadius)
    {
        $this->defaultRadius = $defaultRadius;
    }

    public function getDefaultRaDecUnit()
    {
        return $this->defaultRaDecUnit;
    }

    public function setDefaultRaDecUnit($defaultRaDecUnit)
    {
        $this->defaultRaDecUnit = $defaultRaDecUnit;
    }

    public function getPlotEnabled()
    {
        return $this->plotEnabled;
    }

    public function setPlotEnabled($plotEnabled)
    {
        $this->plotEnabled = $plotEnabled;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'enabled' => $this->getEnabled(),
            'opened' => $this->getOpened(),
            'column_ra' => $this->getColumnRa(),
            'column_dec' => $this->getColumnDec(),
            'resolver_enabled' => $this->getResolverEnabled(),
            'default_ra' => $this->getDefaultRa(),
            'default_dec' => $this->getDefaultDec(),
            'default_radius' => $this->getDefaultRadius(),
            'default_ra_dec_unit' => $this->getDefaultRaDecUnit(),
            'plot_enabled' => $this->getPlotEnabled()
        ];
    }
}
