<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="webpage")
 */
class Webpage implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", name="name", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", name="label", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $icon;

    /**
     * @var int
     *
     * @Column(type="integer", name="display", nullable=false)
     */
    protected $display;

    /**
     * @var string
     *
     * @Column(type="string", name="title", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @Column(type="text", name="content", nullable=true)
     */
    protected $content;

    /**
     * @var string
     *
     * @Column(type="text", name="style_sheet", nullable=true)
     */
    protected $styleSheet;

    /**
     * @var string
     *
     * @Column(type="text", name="type", nullable=false, options={"default" : "webpage"})
     */
    protected $type;

    /**
     * @var string
     *
     * @Column(type="text", name="url", nullable=true)
     */
    protected $url;

    /**
     * @var WebpageFamily
     *
     * @ManyToOne(targetEntity="WebpageFamily")
     * @JoinColumn(name="id_webpage_family", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $webpageFamily;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getStyleSheet()
    {
        return $this->styleSheet;
    }

    public function setStyleSheet($styleSheet)
    {
        $this->styleSheet = $styleSheet;
    }

    public function getWebpageFamily()
    {
        return $this->webpageFamily;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setWebpageFamily($webpageFamily)
    {
        $this->webpageFamily = $webpageFamily;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'icon' => $this->getIcon(),
            'display' => $this->getDisplay(),
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'style_sheet' => $this->getStyleSheet(),
            'type' => $this->getType(),
            'url' => $this->getUrl(),
            'id_webpage_family' => $this->getWebpageFamily()->getId(),
        ];
    }
}
