<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="detail")
 */
class DetailConfig implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="text", name="content", nullable=false)
     */
    protected $content;

    /**
     * @var string
     *
     * @Column(type="text", name="style_sheet", nullable=true)
     */
    protected $styleSheet;

    public function getId()
    {
        return $this->id;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getStyleSheet()
    {
        return $this->styleSheet;
    }

    public function setStyleSheet($styleSheet)
    {
        $this->styleSheet = $styleSheet;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'content' => $this->getContent(),
            'style_sheet' => $this->getStyleSheet()
        ];
    }
}
