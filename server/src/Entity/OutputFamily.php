<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="output_family")
 */
class OutputFamily implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var bool
     *
     * @Column(type="boolean", nullable=false)
     */
    protected $opened;

    /**
     * @var Dataset
     *
     * @ManyToOne(targetEntity="Dataset")
     * @JoinColumn(name="dataset_name", referencedColumnName="name", nullable=false, onDelete="CASCADE")
     */
    protected $dataset;

    public function __construct(Dataset $dataset)
    {
        $this->dataset = $dataset;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function getOpened()
    {
        return $this->opened;
    }

    public function setOpened($opened)
    {
        $this->opened = $opened;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'display' => $this->getDisplay(),
            'opened' => $this->getOpened()
        ];
    }
}
