<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="instance")
 */
class Instance implements \JsonSerializable
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @Column(type="string", name="scientific_manager", nullable=true)
     */
    protected $scientificManager;

    /**
     * @var string
     *
     * @Column(type="string", name="instrument", nullable=true)
     */
    protected $instrument;

    /**
     * @var string
     *
     * @Column(type="string", name="wavelength_domain", nullable=true)
     */
    protected $wavelengthDomain;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var string
     *
     * @Column(type="string", name="data_path", nullable=true)
     */
    protected $dataPath;

    /**
     * @var string
     *
     * @Column(type="string", name="files_path", nullable=true)
     */
    protected $filesPath;

    /**
     * @var bool
     *
     * @Column(type="boolean", nullable=false)
     */
    protected $public;

    /**
     * @var string
     *
     * @Column(type="string", name="portal_logo", nullable=true)
     */
    protected $portalLogo;

    /**
     * @var string
     *
     * @Column(type="string", name="portal_color", nullable=true)
     */
    protected $portalColor;

    /**
     * @var string
     *
     * @Column(type="string", name="design_background_color", nullable=true)
     */
    protected $designBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="design_text_color", nullable=false, options={"default" : "#212529"})
     */
    protected $designTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="design_font_family", nullable=false, options={"default" : "Roboto, sans-serif"})
     */
    protected $designFontFamily;

    /**
     * @var string
     *
     * @Column(type="string", name="design_link_color", nullable=false, options={"default" : "#007BFF"})
     */
    protected $designLinkColor;

    /**
     * @var string
     *
     * @Column(type="string", name="design_link_hover_color", nullable=false, options={"default" : "#0056B3"})
     */
    protected $designLinkHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="design_logo", nullable=true)
     */
    protected $designLogo;

    /**
     * @var string
     *
     * @Column(type="string", name="design_logo_href", nullable=true)
     */
    protected $designLogoHref;

    /**
     * @var string
     *
     * @Column(type="string", name="design_favicon", nullable=true)
     */
    protected $designFavicon;

    /**
     * @var string
     *
     * @Column(type="string", name="navbar_background_color", nullable=false, options={"default" : "#F8F9FA"})
     */
    protected $navbarBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="navbar_border_bottom_color", nullable=false, options={"default" : "#DEE2E6"})
     */
    protected $navbarBorderBottomColor;

    /**
     * @var string
     *
     * @Column(type="string", name="navbar_color_href", nullable=false, options={"default" : "#000000"})
     */
    protected $navbarColorHref;

    /**
     * @var string
     *
     * @Column(type="string", name="navbar_font_family", nullable=false, options={"default" : "Roboto, sans-serif"})
     */
    protected $navbarFontFamily;

    /**
     * @var string
     *
     * @Column(type="string", name="navbar_sign_in_btn_color", nullable=false, options={"default" : "#28A745"})
     */
    protected $navbarSignInBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="navbar_user_btn_color", nullable=false, options={"default" : "#7AC29A"})
     */
    protected $navbarUserBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="footer_background_color", nullable=false, options={"default" : "#F8F9FA"})
     */
    protected $footerBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="footer_border_top_color", nullable=false, options={"default" : "#DEE2E6"})
     */
    protected $footerBorderTopColor;

    /**
     * @var string
     *
     * @Column(type="string", name="footer_text_color", nullable=false, options={"default" : "#000000"})
     */
    protected $footerTextColor;

    /**
     * @var string
     *
     * @Column(type="json", name="footer_logos", nullable=true)
     */
    protected $footerLogos;

    /**
     * @var string
     *
     * @Column(type="string", name="family_border_color", nullable=false, options={"default" : "#DFDFDF"})
     */
    protected $familyBorderColor;

    /**
     * @var string
     *
     * @Column(type="string", name="family_header_background_color", nullable=false, options={"default" : "#F7F7F7"})
     */
    protected $familyHeaderBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="family_title_color", nullable=false, options={"default" : "#007bff"})
     */
    protected $familyTitleColor;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="family_title_bold", nullable=false, options={"default" : false})
     */
    protected $familyTitleBold;

    /**
     * @var string
     *
     * @Column(type="string", name="family_background_color", nullable=false, options={"default" : "#FFFFFF"})
     */
    protected $familyBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="family_text_color", nullable=false, options={"default" : "#212529"})
     */
    protected $familyTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_title", nullable=false, options={"default" : "Dataset search"})
     */
    protected $progressBarTitle;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_title_color", nullable=false, options={"default" : "#000000"})
     */
    protected $progressBarTitleColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_subtitle", nullable=false, options={
     *     "default" : "Select a dataset, add criteria, select output columns and display the result."
     * })
     */
    protected $progressBarSubtitle;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_subtitle_color", nullable=false, options={"default" : "#6C757D"})
     */
    protected $progressBarSubtitleColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_step_dataset_title", nullable=false, options={
     *     "default" : "Dataset selection"
     * })
     */
    protected $progressBarStepDatasetTitle;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_step_criteria_title", nullable=false, options={
     *     "default" : "Search criteria"
     * })
     */
    protected $progressBarStepCriteriaTitle;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_step_output_title", nullable=false, options={
     *     "default" : "Output columns"
     * })
     */
    protected $progressBarStepOutputTitle;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_step_result_title", nullable=false, options={
     *     "default" : "Result table"
     * })
     */
    protected $progressBarStepResultTitle;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_color", nullable=false, options={"default" : "#E9ECEF"})
     */
    protected $progressBarColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_active_color", nullable=false, options={"default" : "#7AC29A"})
     */
    protected $progressBarActiveColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_circle_color", nullable=false, options={"default" : "#FFFFFF"})
     */
    protected $progressBarCircleColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_circle_icon_color", nullable=false, options={"default" : "#CCCCCC"})
     */
    protected $progressBarCircleIconColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_circle_icon_active_color", nullable=false, options={
     *     "default" : "#FFFFFF"
     * })
     */
    protected $progressBarCircleIconActiveColor;

    /**
     * @var string
     *
     * @Column(type="string", name="progress_bar_text_color", nullable=false, options={"default" : "#91B2BF"})
     */
    protected $progressBarTextColor;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="progress_bar_text_bold", nullable=false, options={"default" : false})
     */
    protected $progressBarTextBold;

    /**
     * @var string
     *
     * @Column(type="string", name="search_next_btn_color", nullable=false, options={"default" : "#007BFF"})
     */
    protected $searchNextBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_next_btn_hover_color", nullable=false, options={"default" : "#007BFF"})
     */
    protected $searchNextBtnHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_next_btn_hover_text_color", nullable=false, options={"default" : "#FFFFFF"})
     */
    protected $searchNextBtnHoverTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_back_btn_color", nullable=false, options={"default" : "#6C757D"})
     */
    protected $searchBackBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_back_btn_hover_color", nullable=false, options={"default" : "#6C757D"})
     */
    protected $searchBackBtnHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_back_btn_hover_text_color", nullable=false, options={"default" : "#FFFFFF"})
     */
    protected $searchBackBtnHoverTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_info_background_color", nullable=false, options={"default" : "#E9ECEF"})
     */
    protected $searchInfoBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_info_text_color", nullable=false, options={"default" : "#000000"})
     */
    protected $searchInfoTextColor;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="search_info_help_enabled", nullable=false, options={"default" : true})
     */
    protected $searchInfoHelpEnabled;

    /**
     * @var string
     *
     * @Column(type="string", name="dataset_select_btn_color", nullable=false, options={"default" : "#6C757D"})
     */
    protected $datasetSelectBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="dataset_select_btn_hover_color", nullable=false, options={"default" : "#6C757D"})
     */
    protected $datasetSelectBtnHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="dataset_select_btn_hover_text_color", nullable=false, options={
     *     "default" : "#FFFFFF"
     * })
     */
    protected $datasetSelectBtnHoverTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="dataset_selected_icon_color", nullable=false, options={"default" : "#28A745"})
     */
    protected $datasetSelectedIconColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_criterion_background_color", nullable=false, options={
     *     "default" : "#7AC29A"
     * })
     */
    protected $searchCriterionBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="search_criterion_text_color", nullable=false, options={
     *     "default" : "#000000"
     * })
     */
    protected $searchCriterionTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="output_columns_selected_color", nullable=false, options={"default" : "#7AC29A"})
     */
    protected $outputColumnsSelectedColor;

    /**
     * @var string
     *
     * @Column(type="string", name="output_columns_select_all_btn_color", nullable=false, options={
     *     "default" : "#6C757D"
     * })
     */
    protected $outputColumnsSelectAllBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="output_columns_select_all_btn_hover_color", nullable=false, options={
     *     "default" : "#6C757D"
     * })
     */
    protected $outputColumnsSelectAllBtnHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="output_columns_select_all_btn_hover_text_color", nullable=false, options={
     *     "default" : "#FFFFFF"
     * })
     */
    protected $outputColumnsSelectAllBtnHoverTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_panel_border_size", nullable=false, options={"default" : "1px"})
     */
    protected $resultPanelBorderSize;

    /**
     * @var string
     *
     * @Column(type="string", name="result_panel_border_color", nullable=false, options={"default" : "#DEE2E6"})
     */
    protected $resultPanelBorderColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_panel_title_color", nullable=false, options={"default" : "#000000"})
     */
    protected $resultPanelTitleColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_panel_background_color", nullable=false, options={"default" : "#FFFFFF"})
     */
    protected $resultPanelBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_panel_text_color", nullable=false, options={"default" : "#000000"})
     */
    protected $resultPanelTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_download_btn_color", nullable=false, options={"default" : "#007BFF"})
     */
    protected $resultDownloadBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_download_btn_hover_color", nullable=false, options={"default" : "#0069D9"})
     */
    protected $resultDownloadBtnHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_download_btn_text_color", nullable=false, options={"default" : "#FFFFFF"})
     */
    protected $resultDownloadBtnTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_actions_btn_color", nullable=false, options={
     *     "default" : "#007BFF"
     * })
     */
    protected $resultDatatableActionsBtnColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_actions_btn_hover_color", nullable=false, options={
     *     "default" : "#0069D9"
     * })
     */
    protected $resultDatatableActionsBtnHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_actions_btn_text_color", nullable=false, options={
     *     "default" : "#FFFFFF"
     * })
     */
    protected $resultDatatableActionsBtnTextColor;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="result_datatable_bordered", nullable=false, options={"default" : true})
     */
    protected $resultDatatableBordered;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="result_datatable_bordered_radius", nullable=false, options={"default" : false})
     */
    protected $resultDatatableBorderedRadius;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_border_color", nullable=false, options={
     *     "default" : "#DEE2E6"
     * })
     */
    protected $resultDatatableBorderColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_header_background_color", nullable=false, options={
     *     "default" : "#FFFFFF"
     * })
     */
    protected $resultDatatableHeaderBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_header_text_color", nullable=false, options={
     *     "default" : "#000000"
     * })
     */
    protected $resultDatatableHeaderTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_rows_background_color", nullable=false, options={
     *     "default" : "#FFFFFF"
     * })
     */
    protected $resultDatatableRowsBackgroundColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_rows_text_color", nullable=false, options={"default" : "#000000"})
     */
    protected $resultDatatableRowsTextColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_sorted_color", nullable=false, options={"default" : "#C5C5C5"})
     */
    protected $resultDatatableSortedColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_sorted_active_color", nullable=false, options={
     *     "default" : "#000000"
     * })
     */
    protected $resultDatatableSortedActiveColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_link_color", nullable=false, options={"default" : "#007BFF"})
     */
    protected $resultDatatableLinkColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_link_hover_color", nullable=false, options={
     *     "default" : "#0056B3"
     * })
     */
    protected $resultDatatableLinkHoverColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_rows_selected_color", nullable=false, options={
     *     "default" : "#7AC29A"
     * })
     */
    protected $resultDatatableRowsSelectedColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_pagination_link_color", nullable=false, options={
     *     "default" : "#007BFF"
     * })
     */
    protected $resultDatatablePaginationLinkColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_pagination_active_bck_color", nullable=false, options={
     *     "default" : "#007BFF"
     * })
     */
    protected $resultDatatablePaginationActiveBckColor;

    /**
     * @var string
     *
     * @Column(type="string", name="result_datatable_pagination_active_text_color", nullable=false, options={
     *     "default" : "#FFFFFF"
     * })
     */
    protected $resultDatatablePaginationActiveTextColor;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="samp_enabled", nullable=false)
     */
    protected $sampEnabled;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="back_to_portal", nullable=false)
     */
    protected $backToPortal;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="user_menu_enabled", nullable=false, options={"default" : true})
     */
    protected $userMenuEnabled;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="search_by_criteria_allowed", nullable=false)
     */
    protected $searchByCriteriaAllowed;

    /**
     * @var string
     *
     * @Column(type="string", name="search_by_criteria_label", nullable=false)
     */
    protected $searchByCriteriaLabel;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="search_multiple_allowed", nullable=false)
     */
    protected $searchMultipleAllowed;

    /**
     * @var string
     *
     * @Column(type="string", name="search_multiple_label", nullable=false)
     */
    protected $searchMultipleLabel;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="search_multiple_all_datasets_selected", nullable=false)
     */
    protected $searchMultipleAllDatasetsSelected;

    /**
     * @var string
     *
     * @Column(type="string", name="search_multiple_progress_bar_title", nullable=false, options={
     *     "default" : "Search around a position in multiple datasets"
     * })
     */
    protected $searchMultipleProgressBarTitle;

    /**
     * @var string
     *
     * @Column(type="string", name="search_multiple_progress_bar_subtitle", nullable=false, options={
     *     "default" : "Fill RA & DEC position, select datasets and display the result."
     * })
     */
    protected $searchMultipleProgressBarSubtitle;

    /**
     * @var string
     *
     * @Column(type="string", name="search_multiple_progress_bar_step_position", nullable=false, options={
     *     "default" : "Position"
     * })
     */
    protected $searchMultipleProgressBarStepPosition;

    /**
     * @var string
     *
     * @Column(type="string", name="search_multiple_progress_bar_step_datasets", nullable=false, options={
     *     "default" : "Datasets"
     * })
     */
    protected $searchMultipleProgressBarStepDatasets;

    /**
     * @var string
     *
     * @Column(type="string", name="search_multiple_progress_bar_step_result", nullable=false, options={
     *     "default" : "Result"
     * })
     */
    protected $searchMultipleProgressBarStepResult;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="documentation_allowed", nullable=false)
     */
    protected $documentationAllowed;

    /**
     * @var string
     *
     * @Column(type="string", name="documentation_label", nullable=false)
     */
    protected $documentationLabel;

    /**
     * @var DatasetFamily[]
     *
     * @OneToMany(targetEntity="DatasetFamily", mappedBy="instance")
     */
    protected $datasetFamilies;

    public function __construct(string $name, string $label)
    {
        $this->name = $name;
        $this->label = $label;
        $this->datasetFamilies = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getScientificManager()
    {
        return $this->scientificManager;
    }

    public function setScientificManager($scientificManager)
    {
        $this->scientificManager = $scientificManager;
    }

    public function getInstrument()
    {
        return $this->instrument;
    }

    public function setInstrument($instrument)
    {
        $this->instrument = $instrument;
    }

    public function getWavelengthDomain()
    {
        return $this->wavelengthDomain;
    }

    public function setWavelengthDomain($wavelengthDomain)
    {
        $this->wavelengthDomain = $wavelengthDomain;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function getDataPath()
    {
        return $this->dataPath;
    }

    public function setDataPath($dataPath)
    {
        $this->dataPath = $dataPath;
    }

    public function getFilesPath()
    {
        return $this->filesPath;
    }

    public function setFilesPath($filesPath)
    {
        $this->filesPath = $filesPath;
    }

    public function getPublic()
    {
        return $this->public;
    }

    public function setPublic($public)
    {
        $this->public = $public;
    }

    public function getPortalLogo()
    {
        return $this->portalLogo;
    }

    public function setPortalLogo($portalLogo)
    {
        $this->portalLogo = $portalLogo;
    }

    public function getPortalColor()
    {
        return $this->portalColor;
    }

    public function setPortalColor($portalColor)
    {
        $this->portalColor = $portalColor;
    }

    public function getDesignBackgroundColor()
    {
        return $this->designBackgroundColor;
    }

    public function setDesignBackgroundColor($designBackgroundColor)
    {
        $this->designBackgroundColor = $designBackgroundColor;
    }

    public function getDesignTextColor()
    {
        return $this->designTextColor;
    }

    public function setDesignTextColor($designTextColor)
    {
        return $this->designTextColor = $designTextColor;
    }

    public function getDesignFontFamily()
    {
        return $this->designFontFamily;
    }

    public function setDesignFontFamily($designFontFamily)
    {
        $this->designFontFamily = $designFontFamily;
    }

    public function getDesignLinkColor()
    {
        return $this->designLinkColor;
    }

    public function setDesignLinkColor($designLinkColor)
    {
        $this->designLinkColor = $designLinkColor;
    }

    public function getDesignLinkHoverColor()
    {
        return $this->designLinkHoverColor;
    }

    public function setDesignLinkHoverColor($designLinkHoverColor)
    {
        $this->designLinkHoverColor = $designLinkHoverColor;
    }

    public function getDesignLogo()
    {
        return $this->designLogo;
    }

    public function setDesignLogo($designLogo)
    {
        $this->designLogo = $designLogo;
    }

    public function getDesignFavicon()
    {
        return $this->designFavicon;
    }

    public function setDesignFavicon($designFavicon)
    {
        $this->designFavicon = $designFavicon;
    }

    public function getDesignLogoHref()
    {
        return $this->designLogoHref;
    }

    public function setDesignLogoHref($designLogoHref)
    {
        $this->designLogoHref = $designLogoHref;
    }

    public function getNavbarBackgroundColor()
    {
        return $this->navbarBackgroundColor;
    }

    public function setNavbarBackgroundColor($navbarBackgroundColor)
    {
        $this->navbarBackgroundColor = $navbarBackgroundColor;
    }

    public function getNavbarBorderBottomColor()
    {
        return $this->navbarBorderBottomColor;
    }

    public function setNavbarBorderBottomColor($navbarBorderBottomColor)
    {
        $this->navbarBorderBottomColor = $navbarBorderBottomColor;
    }

    public function getNavbarColorHref()
    {
        return $this->navbarColorHref;
    }

    public function setNavbarColorHref($navbarColorHref)
    {
        $this->navbarColorHref = $navbarColorHref;
    }

    public function getNavbarFontFamily()
    {
        return $this->navbarFontFamily;
    }

    public function setNavbarFontFamily($navbarFontFamily)
    {
        $this->navbarFontFamily = $navbarFontFamily;
    }

    public function getNavbarSignInBtnColor()
    {
        return $this->navbarSignInBtnColor;
    }

    public function setNavbarSignInBtnColor($navbarSignInBtnColor)
    {
        $this->navbarSignInBtnColor = $navbarSignInBtnColor;
    }

    public function getNavbarUserBtnColor()
    {
        return $this->navbarUserBtnColor;
    }

    public function setNavbarUserBtnColor($navbarUserBtnColor)
    {
        $this->navbarUserBtnColor = $navbarUserBtnColor;
    }

    public function getFooterBackgroundColor()
    {
        return $this->footerBackgroundColor;
    }

    public function setFooterBackgroundColor($footerBackgroundColor)
    {
        $this->footerBackgroundColor = $footerBackgroundColor;
    }

    public function getFooterBorderTopColor()
    {
        return $this->footerBorderTopColor;
    }

    public function setFooterBorderTopColor($footerBorderTopColor)
    {
        $this->footerBorderTopColor = $footerBorderTopColor;
    }

    public function getFooterTextColor()
    {
        return $this->footerTextColor;
    }

    public function setFooterTextColor($footerTextColor)
    {
        $this->footerTextColor = $footerTextColor;
    }

    public function getFooterLogos()
    {
        return $this->footerLogos;
    }

    public function setFooterLogos($footerLogos)
    {
        $this->footerLogos = $footerLogos;
    }

    public function getFamilyBorderColor()
    {
        return $this->familyBorderColor;
    }

    public function setFamilyBorderColor($familyBorderColor)
    {
        $this->familyBorderColor = $familyBorderColor;
    }

    public function getFamilyHeaderBackgroundColor()
    {
        return $this->familyHeaderBackgroundColor;
    }

    public function setFamilyHeaderBackgroundColor($familyHeaderBackgroundColor)
    {
        $this->familyHeaderBackgroundColor = $familyHeaderBackgroundColor;
    }

    public function getFamilyTitleColor()
    {
        return $this->familyTitleColor;
    }

    public function setFamilyTitleColor($familyTitleColor)
    {
        $this->familyTitleColor = $familyTitleColor;
    }

    public function getFamilyTitleBold()
    {
        return $this->familyTitleBold;
    }

    public function setFamilyTitleBold($familyTitleBold)
    {
        $this->familyTitleBold = $familyTitleBold;
    }

    public function getFamilyBackgroundColor()
    {
        return $this->familyBackgroundColor;
    }

    public function setFamilyBackgroundColor($familyBackgroundColor)
    {
        $this->familyBackgroundColor = $familyBackgroundColor;
    }

    public function getFamilyTextColor()
    {
        return $this->familyTextColor;
    }

    public function setFamilyTextColor($familyTextColor)
    {
        $this->familyTextColor = $familyTextColor;
    }

    public function getProgressBarTitle()
    {
        return $this->progressBarTitle;
    }

    public function setProgressBarTitle($progressBarTitle)
    {
        $this->progressBarTitle = $progressBarTitle;
    }

    public function getProgressBarTitleColor()
    {
        return $this->progressBarTitleColor;
    }

    public function setProgressBarTitleColor($progressBarTitleColor)
    {
        $this->progressBarTitleColor = $progressBarTitleColor;
    }

    public function getProgressBarSubtitle()
    {
        return $this->progressBarSubtitle;
    }

    public function setProgressBarSubtitle($progressBarSubtitle)
    {
        $this->progressBarSubtitle = $progressBarSubtitle;
    }

    public function getProgressBarSubtitleColor()
    {
        return $this->progressBarSubtitleColor;
    }

    public function setProgressBarSubtitleColor($progressBarSubtitleColor)
    {
        $this->progressBarSubtitleColor = $progressBarSubtitleColor;
    }

    public function getProgressBarStepDatasetTitle()
    {
        return $this->progressBarStepDatasetTitle;
    }

    public function setProgressBarStepDatasetTitle($progressBarStepDatasetTitle)
    {
        $this->progressBarStepDatasetTitle = $progressBarStepDatasetTitle;
    }

    public function getProgressBarStepCriteriaTitle()
    {
        return $this->progressBarStepCriteriaTitle;
    }

    public function setProgressBarStepCriteriaTitle($progressBarStepCriteriaTitle)
    {
        $this->progressBarStepCriteriaTitle = $progressBarStepCriteriaTitle;
    }

    public function getProgressBarStepOutputTitle()
    {
        return $this->progressBarStepOutputTitle;
    }

    public function setProgressBarStepOutputTitle($progressBarStepOutputTitle)
    {
        $this->progressBarStepOutputTitle = $progressBarStepOutputTitle;
    }

    public function getProgressBarStepResultTitle()
    {
        return $this->progressBarStepResultTitle;
    }

    public function setProgressBarStepResultTitle($progressBarStepResultTitle)
    {
        $this->progressBarStepResultTitle = $progressBarStepResultTitle;
    }

    public function getProgressBarColor()
    {
        return $this->progressBarColor;
    }

    public function setProgressBarColor($progressBarColor)
    {
        $this->progressBarColor = $progressBarColor;
    }

    public function getProgressBarActiveColor()
    {
        return $this->progressBarActiveColor;
    }

    public function setProgressBarActiveColor($progressBarActiveColor)
    {
        $this->progressBarActiveColor = $progressBarActiveColor;
    }

    public function getProgressBarCircleColor()
    {
        return $this->progressBarCircleColor;
    }

    public function setProgressBarCircleColor($progressBarCircleColor)
    {
        $this->progressBarCircleColor = $progressBarCircleColor;
    }

    public function getProgressBarCircleIconColor()
    {
        return $this->progressBarCircleIconColor;
    }

    public function setProgressBarCircleIconColor($progressBarCircleIconColor)
    {
        $this->progressBarCircleIconColor = $progressBarCircleIconColor;
    }

    public function getProgressBarCircleIconActiveColor()
    {
        return $this->progressBarCircleIconActiveColor;
    }

    public function setProgressBarCircleIconActiveColor($progressBarCircleIconActiveColor)
    {
        $this->progressBarCircleIconActiveColor = $progressBarCircleIconActiveColor;
    }

    public function getProgressBarTextColor()
    {
        return $this->progressBarTextColor;
    }

    public function setProgressBarTextColor($progressBarTextColor)
    {
        $this->progressBarTextColor = $progressBarTextColor;
    }

    public function getProgressBarTextBold()
    {
        return $this->progressBarTextBold;
    }

    public function setProgressBarTextBold($progressBarTextBold)
    {
        $this->progressBarTextBold = $progressBarTextBold;
    }

    public function getSearchNextBtnColor()
    {
        return $this->searchNextBtnColor;
    }

    public function setSearchNextBtnColor($searchNextBtnColor)
    {
        $this->searchNextBtnColor = $searchNextBtnColor;
    }

    public function getSearchNextBtnHoverColor()
    {
        return $this->searchNextBtnHoverColor;
    }

    public function setSearchNextBtnHoverColor($searchNextBtnHoverColor)
    {
        $this->searchNextBtnHoverColor = $searchNextBtnHoverColor;
    }

    public function getSearchNextBtnHoverTextColor()
    {
        return $this->searchNextBtnHoverTextColor;
    }

    public function setSearchNextBtnHoverTextColor($searchNextBtnHoverTextColor)
    {
        $this->searchNextBtnHoverTextColor = $searchNextBtnHoverTextColor;
    }

    public function getSearchBackBtnColor()
    {
        return $this->searchBackBtnColor;
    }

    public function setSearchBackBtnColor($searchBackBtnColor)
    {
        $this->searchBackBtnColor = $searchBackBtnColor;
    }

    public function getSearchBackBtnHoverColor()
    {
        return $this->searchBackBtnHoverColor;
    }

    public function setSearchBackBtnHoverColor($searchBackBtnHoverColor)
    {
        $this->searchBackBtnHoverColor = $searchBackBtnHoverColor;
    }

    public function getSearchBackBtnHoverTextColor()
    {
        return $this->searchBackBtnHoverTextColor;
    }

    public function setSearchBackBtnHoverTextColor($searchBackBtnHoverTextColor)
    {
        $this->searchBackBtnHoverTextColor = $searchBackBtnHoverTextColor;
    }

    public function setSearchInfoBackgroundColor($searchInfoBackgroundColor)
    {
        $this->searchInfoBackgroundColor = $searchInfoBackgroundColor;
    }

    public function getSearchInfoTextColor()
    {
        return $this->searchInfoTextColor;
    }

    public function setSearchInfoTextColor($searchInfoTextColor)
    {
        $this->searchInfoTextColor = $searchInfoTextColor;
    }

    public function getSearchInfoHelpEnabled()
    {
        return $this->searchInfoHelpEnabled;
    }

    public function setSearchInfoHelpEnabled($searchInfoHelpEnabled)
    {
        $this->searchInfoHelpEnabled = $searchInfoHelpEnabled;
    }

    public function getDatasetSelectBtnColor()
    {
        return $this->datasetSelectBtnColor;
    }

    public function setDatasetSelectBtnColor($datasetSelectBtnColor)
    {
        $this->datasetSelectBtnColor = $datasetSelectBtnColor;
    }

    public function getDatasetSelectBtnHoverColor()
    {
        return $this->datasetSelectBtnHoverColor;
    }

    public function setDatasetSelectBtnHoverColor($datasetSelectBtnHoverColor)
    {
        $this->datasetSelectBtnHoverColor = $datasetSelectBtnHoverColor;
    }

    public function getDatasetSelectBtnHoverTextColor()
    {
        return $this->datasetSelectBtnHoverTextColor;
    }

    public function setDatasetSelectBtnHoverTextColor($datasetSelectBtnHoverTextColor)
    {
        $this->datasetSelectBtnHoverTextColor = $datasetSelectBtnHoverTextColor;
    }

    public function getDatasetSelectedIconColor()
    {
        return $this->datasetSelectedIconColor;
    }

    public function setDatasetSelectedIconColor($datasetSelectedIconColor)
    {
        $this->datasetSelectedIconColor = $datasetSelectedIconColor;
    }

    public function getSearchInfoBackgroundColor()
    {
        return $this->searchInfoBackgroundColor;
    }

    public function getSearchCriterionBackgroundColor()
    {
        return $this->searchCriterionBackgroundColor;
    }

    public function setSearchCriterionBackgroundColor($searchCriterionBackgroundColor)
    {
        $this->searchCriterionBackgroundColor = $searchCriterionBackgroundColor;
    }

    public function getSearchCriterionTextColor()
    {
        return $this->searchCriterionTextColor;
    }

    public function setSearchCriterionTextColor($searchCriterionTextColor)
    {
        $this->searchCriterionTextColor = $searchCriterionTextColor;
    }

    public function getOutputColumnsSelectedColor()
    {
        return $this->outputColumnsSelectedColor;
    }

    public function setOutputColumnsSelectedColor($outputColumnsSelectedColor)
    {
        $this->outputColumnsSelectedColor = $outputColumnsSelectedColor;
    }

    public function getOutputColumnsSelectAllBtnColor()
    {
        return $this->outputColumnsSelectAllBtnColor;
    }

    public function setOutputColumnsSelectAllBtnColor($outputColumnsSelectAllBtnColor)
    {
        $this->outputColumnsSelectAllBtnColor = $outputColumnsSelectAllBtnColor;
    }

    public function getOutputColumnsSelectAllBtnHoverColor()
    {
        return $this->outputColumnsSelectAllBtnHoverColor;
    }

    public function setOutputColumnsSelectAllBtnHoverColor($outputColumnsSelectAllBtnHoverColor)
    {
        $this->outputColumnsSelectAllBtnHoverColor = $outputColumnsSelectAllBtnHoverColor;
    }

    public function getOutputColumnsSelectAllBtnHoverTextColor()
    {
        return $this->outputColumnsSelectAllBtnHoverTextColor;
    }

    public function setOutputColumnsSelectAllBtnHoverTextColor($outputColumnsSelectAllBtnHoverTextColor)
    {
        $this->outputColumnsSelectAllBtnHoverTextColor = $outputColumnsSelectAllBtnHoverTextColor;
    }

    public function getResultPanelBorderSize()
    {
        return $this->resultPanelBorderSize;
    }

    public function setResultPanelBorderSize($resultPanelBorderSize)
    {
        $this->resultPanelBorderSize = $resultPanelBorderSize;
    }

    public function getResultPanelBorderColor()
    {
        return $this->resultPanelBorderColor;
    }

    public function setResultPanelBorderColor($resultPanelBorderColor)
    {
        $this->resultPanelBorderColor = $resultPanelBorderColor;
    }

    public function getResultPanelTitleColor()
    {
        return $this->resultPanelTitleColor;
    }

    public function setResultPanelTitleColor($resultPanelTitleColor)
    {
        $this->resultPanelTitleColor = $resultPanelTitleColor;
    }

    public function getResultPanelBackgroundColor()
    {
        return $this->resultPanelBackgroundColor;
    }

    public function setResultPanelBackgroundColor($resultPanelBackgroundColor)
    {
        $this->resultPanelBackgroundColor = $resultPanelBackgroundColor;
    }

    public function getResultPanelTextColor()
    {
        return $this->resultPanelTextColor;
    }

    public function setResultPanelTextColor($resultPanelTextColor)
    {
        $this->resultPanelTextColor = $resultPanelTextColor;
    }

    public function getResultDownloadBtnColor()
    {
        return $this->resultDownloadBtnColor;
    }

    public function setResultDownloadBtnColor($resultDownloadBtnColor)
    {
        $this->resultDownloadBtnColor = $resultDownloadBtnColor;
    }

    public function getResultDownloadBtnHoverColor()
    {
        return $this->resultDownloadBtnHoverColor;
    }

    public function setResultDownloadBtnHoverColor($resultDownloadBtnHoverColor)
    {
        $this->resultDownloadBtnHoverColor = $resultDownloadBtnHoverColor;
    }

    public function getResultDownloadBtnTextColor()
    {
        return $this->resultDownloadBtnTextColor;
    }

    public function setResultDownloadBtnTextColor($resultDownloadBtnTextColor)
    {
        $this->resultDownloadBtnTextColor = $resultDownloadBtnTextColor;
    }

    public function getResultDatatableActionsBtnColor()
    {
        return $this->resultDatatableActionsBtnColor;
    }

    public function setResultDatatableActionsBtnColor($resultDatatableActionsBtnColor)
    {
        $this->resultDatatableActionsBtnColor = $resultDatatableActionsBtnColor;
    }

    public function getResultDatatableActionsBtnHoverColor()
    {
        return $this->resultDatatableActionsBtnHoverColor;
    }

    public function setResultDatatableActionsBtnHoverColor($resultDatatableActionsBtnHoverColor)
    {
        $this->resultDatatableActionsBtnHoverColor = $resultDatatableActionsBtnHoverColor;
    }

    public function getResultDatatableActionsBtnTextColor()
    {
        return $this->resultDatatableActionsBtnTextColor;
    }

    public function setResultDatatableActionsBtnTextColor($resultDatatableActionsBtnTextColor)
    {
        $this->resultDatatableActionsBtnTextColor = $resultDatatableActionsBtnTextColor;
    }

    public function getResultDatatableBordered()
    {
        return $this->resultDatatableBordered;
    }

    public function setResultDatatableBordered($resultDatatableBordered)
    {
        $this->resultDatatableBordered = $resultDatatableBordered;
    }

    public function getResultDatatableBorderedRadius()
    {
        return $this->resultDatatableBorderedRadius;
    }

    public function setResultDatatableBorderedRadius($resultDatatableBorderedRadius)
    {
        $this->resultDatatableBorderedRadius = $resultDatatableBorderedRadius;
    }

    public function getResultDatatableBorderColor()
    {
        return $this->resultDatatableBorderColor;
    }

    public function setResultDatatableBorderColor($resultDatatableBorderColor)
    {
        $this->resultDatatableBorderColor = $resultDatatableBorderColor;
    }

    public function getResultDatatableHeaderBackgroundColor()
    {
        return $this->resultDatatableHeaderBackgroundColor;
    }

    public function setResultDatatableHeaderBackgroundColor($resultDatatableHeaderBackgroundColor)
    {
        $this->resultDatatableHeaderBackgroundColor = $resultDatatableHeaderBackgroundColor;
    }

    public function getResultDatatableHeaderTextColor()
    {
        return $this->resultDatatableHeaderTextColor;
    }

    public function setResultDatatableHeaderTextColor($resultDatatableHeaderTextColor)
    {
        $this->resultDatatableHeaderTextColor = $resultDatatableHeaderTextColor;
    }

    public function getResultDatatableRowsBackgroundColor()
    {
        return $this->resultDatatableRowsBackgroundColor;
    }

    public function setResultDatatableRowsBackgroundColor($resultDatatableRowsBackgroundColor)
    {
        $this->resultDatatableRowsBackgroundColor = $resultDatatableRowsBackgroundColor;
    }

    public function getResultDatatableRowsTextColor()
    {
        return $this->resultDatatableRowsTextColor;
    }

    public function setResultDatatableRowsTextColor($resultDatatableRowsTextColor)
    {
        $this->resultDatatableRowsTextColor = $resultDatatableRowsTextColor;
    }

    public function getResultDatatableSortedColor()
    {
        return $this->resultDatatableSortedColor;
    }

    public function setResultDatatableSortedColor($resultDatatableSortedColor)
    {
        $this->resultDatatableSortedColor = $resultDatatableSortedColor;
    }

    public function getResultDatatableSortedActiveColor()
    {
        return $this->resultDatatableSortedActiveColor;
    }

    public function setResultDatatableSortedActiveColor($resultDatatableSortedActiveColor)
    {
        $this->resultDatatableSortedActiveColor = $resultDatatableSortedActiveColor;
    }

    public function getResultDatatableLinkColor()
    {
        return $this->resultDatatableLinkColor;
    }

    public function setResultDatatableLinkColor($resultDatatableLinkColor)
    {
        $this->resultDatatableLinkColor = $resultDatatableLinkColor;
    }

    public function getResultDatatableLinkHoverColor()
    {
        return $this->resultDatatableLinkHoverColor;
    }

    public function setResultDatatableLinkHoverColor($resultDatatableLinkHoverColor)
    {
        $this->resultDatatableLinkHoverColor = $resultDatatableLinkHoverColor;
    }

    public function getResultDatatableRowsSelectedColor()
    {
        return $this->resultDatatableRowsSelectedColor;
    }

    public function setResultDatatableRowsSelectedColor($resultDatatableRowsSelectedColor)
    {
        $this->resultDatatableRowsSelectedColor = $resultDatatableRowsSelectedColor;
    }

    public function getResultDatatablePaginationLinkColor()
    {
        return $this->resultDatatablePaginationLinkColor;
    }

    public function setResultDatatablePaginationLinkColor($resultDatatablePaginationLinkColor)
    {
        $this->resultDatatablePaginationLinkColor = $resultDatatablePaginationLinkColor;
    }

    public function getResultDatatablePaginationActiveBckColor()
    {
        return $this->resultDatatablePaginationActiveBckColor;
    }

    public function setResultDatatablePaginationActiveBckColor($resultDatatablePaginationActiveBckColor)
    {
        $this->resultDatatablePaginationActiveBckColor = $resultDatatablePaginationActiveBckColor;
    }

    public function getResultDatatablePaginationActiveTextColor()
    {
        return $this->resultDatatablePaginationActiveTextColor;
    }

    public function setResultDatatablePaginationActiveTextColor($resultDatatablePaginationActiveTextColor)
    {
        $this->resultDatatablePaginationActiveTextColor = $resultDatatablePaginationActiveTextColor;
    }

    public function getSampEnabled()
    {
        return $this->sampEnabled;
    }

    public function setSampEnabled($sampEnabled)
    {
        $this->sampEnabled = $sampEnabled;
    }

    public function getBackToPortal()
    {
        return $this->backToPortal;
    }

    public function setBackToPortal($backToPortal)
    {
        $this->backToPortal = $backToPortal;
    }

    public function getUserMenuEnabled()
    {
        return $this->userMenuEnabled;
    }

    public function setUserMenuEnabled($userMenuEnabled)
    {
        $this->userMenuEnabled = $userMenuEnabled;
    }

    public function getSearchByCriteriaAllowed()
    {
        return $this->searchByCriteriaAllowed;
    }

    public function setSearchByCriteriaAllowed($searchByCriteriaAllowed)
    {
        $this->searchByCriteriaAllowed = $searchByCriteriaAllowed;
    }

    public function getSearchByCriteriaLabel()
    {
        return $this->searchByCriteriaLabel;
    }

    public function setSearchByCriteriaLabel($searchByCriteriaLabel)
    {
        $this->searchByCriteriaLabel = $searchByCriteriaLabel;
    }

    public function getSearchMultipleAllowed()
    {
        return $this->searchMultipleAllowed;
    }

    public function setSearchMultipleAllowed($searchMultipleAllowed)
    {
        $this->searchMultipleAllowed = $searchMultipleAllowed;
    }

    public function getSearchMultipleLabel()
    {
        return $this->searchMultipleLabel;
    }

    public function setSearchMultipleLabel($searchMultipleLabel)
    {
        $this->searchMultipleLabel = $searchMultipleLabel;
    }

    public function getSearchMultipleAllDatasetsSelected()
    {
        return $this->searchMultipleAllDatasetsSelected;
    }

    public function setSearchMultipleAllDatasetsSelected($searchMultipleAllDatasetsSelected)
    {
        $this->searchMultipleAllDatasetsSelected = $searchMultipleAllDatasetsSelected;
    }

    public function getSearchMultipleProgressBarTitle()
    {
        return $this->searchMultipleProgressBarTitle;
    }

    public function setSearchMultipleProgressBarTitle($searchMultipleProgressBarTitle)
    {
        $this->searchMultipleProgressBarTitle = $searchMultipleProgressBarTitle;
    }

    public function getSearchMultipleProgressBarSubtitle()
    {
        return $this->searchMultipleProgressBarSubtitle;
    }

    public function setSearchMultipleProgressBarSubtitle($searchMultipleProgressBarSubtitle)
    {
        $this->searchMultipleProgressBarSubtitle = $searchMultipleProgressBarSubtitle;
    }

    public function getSearchMultipleProgressBarStepPosition()
    {
        return $this->searchMultipleProgressBarStepPosition;
    }

    public function setSearchMultipleProgressBarStepPosition($searchMultipleProgressBarStepPosition)
    {
        $this->searchMultipleProgressBarStepPosition = $searchMultipleProgressBarStepPosition;
    }

    public function getSearchMultipleProgressBarStepDatasets()
    {
        return $this->searchMultipleProgressBarStepDatasets;
    }

    public function setSearchMultipleProgressBarStepDatasets($searchMultipleProgressBarStepDatasets)
    {
        $this->searchMultipleProgressBarStepDatasets = $searchMultipleProgressBarStepDatasets;
    }

    public function getSearchMultipleProgressBarStepResult()
    {
        return $this->searchMultipleProgressBarStepResult;
    }

    public function setSearchMultipleProgressBarStepResult($searchMultipleProgressBarStepResult)
    {
        $this->searchMultipleProgressBarStepResult = $searchMultipleProgressBarStepResult;
    }

    public function getDocumentationAllowed()
    {
        return $this->documentationAllowed;
    }

    public function setDocumentationAllowed($documentationAllowed)
    {
        $this->documentationAllowed = $documentationAllowed;
    }

    public function getDocumentationLabel()
    {
        return $this->documentationLabel;
    }

    public function setDocumentationLabel($documentationLabel)
    {
        $this->documentationLabel = $documentationLabel;
    }

    public function getDatasetFamilies()
    {
        return $this->datasetFamilies;
    }

    public function getNbDatasets()
    {
        $nbDatasets = 0;
        foreach ($this->datasetFamilies as $family) {
            $nbDatasets += count($family->getDatasets());
        }
        return $nbDatasets;
    }

    public function jsonSerialize(): array
    {
        return [
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'scientific_manager' => $this->getScientificManager(),
            'instrument' => $this->getInstrument(),
            'wavelength_domain' => $this->getWavelengthDomain(),
            'display' => $this->getDisplay(),
            'data_path' => $this->getDataPath(),
            'files_path' => $this->getFilesPath(),
            'public' => $this->getPublic(),
            'portal_logo' => $this->getPortalLogo(),
            'portal_color' => $this->getPortalColor(),
            'design_background_color' => $this->getDesignBackgroundColor(),
            'design_text_color' => $this->getDesignTextColor(),
            'design_font_family' => $this->getDesignFontFamily(),
            'design_link_color' => $this->getDesignLinkColor(),
            'design_link_hover_color' => $this->getDesignLinkHoverColor(),
            'design_logo' => $this->getDesignLogo(),
            'design_logo_href' => $this->getDesignLogoHref(),
            'design_favicon' => $this->getDesignFavicon(),
            'navbar_background_color' => $this->getNavbarBackgroundColor(),
            'navbar_border_bottom_color' => $this->getNavbarBorderBottomColor(),
            'navbar_color_href' => $this->getNavbarColorHref(),
            'navbar_font_family' => $this->getNavbarFontFamily(),
            'navbar_sign_in_btn_color' => $this->getNavbarSignInBtnColor(),
            'navbar_user_btn_color' => $this->getNavbarUserBtnColor(),
            'footer_background_color' => $this->getFooterBackgroundColor(),
            'footer_border_top_color' => $this->getFooterBorderTopColor(),
            'footer_text_color' => $this->getFooterTextColor(),
            'footer_logos' => $this->getFooterLogos(),
            'family_border_color' => $this->getFamilyBorderColor(),
            'family_header_background_color' => $this->getFamilyHeaderBackgroundColor(),
            'family_title_color' => $this->getFamilyTitleColor(),
            'family_title_bold' => $this->getFamilyTitleBold(),
            'family_background_color' => $this->getFamilyBackgroundColor(),
            'family_text_color' => $this->getFamilyTextColor(),
            'progress_bar_title' => $this->getProgressBarTitle(),
            'progress_bar_title_color' => $this->getProgressBarTitleColor(),
            'progress_bar_subtitle' => $this->getProgressBarSubtitle(),
            'progress_bar_subtitle_color' => $this->getProgressBarSubtitleColor(),
            'progress_bar_step_dataset_title' => $this->getProgressBarStepDatasetTitle(),
            'progress_bar_step_criteria_title' => $this->getProgressBarStepCriteriaTitle(),
            'progress_bar_step_output_title' => $this->getProgressBarStepOutputTitle(),
            'progress_bar_step_result_title' => $this->getProgressBarStepResultTitle(),
            'progress_bar_color' => $this->getProgressBarColor(),
            'progress_bar_active_color' => $this->getProgressBarActiveColor(),
            'progress_bar_circle_color' => $this->getProgressBarCircleColor(),
            'progress_bar_circle_icon_color' => $this->getProgressBarCircleIconColor(),
            'progress_bar_circle_icon_active_color' => $this->getProgressBarCircleIconActiveColor(),
            'progress_bar_text_color' => $this->getProgressBarTextColor(),
            'progress_bar_text_bold' => $this->getProgressBarTextBold(),
            'search_next_btn_color' => $this->getSearchNextBtnColor(),
            'search_next_btn_hover_color' => $this->getSearchNextBtnHoverColor(),
            'search_next_btn_hover_text_color' => $this->getSearchNextBtnHoverTextColor(),
            'search_back_btn_color' => $this->getSearchBackBtnColor(),
            'search_back_btn_hover_color' => $this->getSearchBackBtnHoverColor(),
            'search_back_btn_hover_text_color' => $this->getSearchBackBtnHoverTextColor(),
            'search_info_background_color' => $this->getSearchInfoBackgroundColor(),
            'search_info_text_color' => $this->getSearchInfoTextColor(),
            'search_info_help_enabled' => $this->getSearchInfoHelpEnabled(),
            'dataset_select_btn_color' => $this->getDatasetSelectBtnColor(),
            'dataset_select_btn_hover_color' => $this->getDatasetSelectBtnHoverColor(),
            'dataset_select_btn_hover_text_color' => $this->getDatasetSelectBtnHoverTextColor(),
            'dataset_selected_icon_color' => $this->getDatasetSelectedIconColor(),
            'search_criterion_background_color' => $this->getSearchCriterionBackgroundColor(),
            'search_criterion_text_color' => $this->getSearchCriterionTextColor(),
            'output_columns_selected_color' => $this->getOutputColumnsSelectedColor(),
            'output_columns_select_all_btn_color' => $this->getOutputColumnsSelectAllBtnColor(),
            'output_columns_select_all_btn_hover_color' => $this->getOutputColumnsSelectAllBtnHoverColor(),
            'output_columns_select_all_btn_hover_text_color' => $this->getOutputColumnsSelectAllBtnHoverTextColor(),
            'result_panel_border_size' => $this->getResultPanelBorderSize(),
            'result_panel_border_color' => $this->getResultPanelBorderColor(),
            'result_panel_title_color' => $this->getResultPanelTitleColor(),
            'result_panel_background_color' => $this->getResultPanelBackgroundColor(),
            'result_panel_text_color' => $this->getResultPanelTextColor(),
            'result_download_btn_color' => $this->getResultDownloadBtnColor(),
            'result_download_btn_hover_color' => $this->getResultDownloadBtnHoverColor(),
            'result_download_btn_text_color' => $this->getResultDownloadBtnTextColor(),
            'result_datatable_actions_btn_color' => $this->getResultDatatableActionsBtnColor(),
            'result_datatable_actions_btn_hover_color' => $this->getResultDatatableActionsBtnHoverColor(),
            'result_datatable_actions_btn_text_color' => $this->getResultDatatableActionsBtnTextColor(),
            'result_datatable_bordered' => $this->getResultDatatableBordered(),
            'result_datatable_bordered_radius' => $this->getResultDatatableBorderedRadius(),
            'result_datatable_border_color' => $this->getResultDatatableBorderColor(),
            'result_datatable_header_background_color' => $this->getResultDatatableHeaderBackgroundColor(),
            'result_datatable_header_text_color' => $this->getResultDatatableHeaderTextColor(),
            'result_datatable_rows_background_color' => $this->getResultDatatableRowsBackgroundColor(),
            'result_datatable_rows_text_color' => $this->getResultDatatableRowsTextColor(),
            'result_datatable_sorted_color' => $this->getResultDatatableSortedColor(),
            'result_datatable_sorted_active_color' => $this->getResultDatatableSortedActiveColor(),
            'result_datatable_link_color' => $this->getResultDatatableLinkColor(),
            'result_datatable_link_hover_color' => $this->getResultDatatableLinkHoverColor(),
            'result_datatable_rows_selected_color' => $this->getResultDatatableRowsSelectedColor(),
            'result_datatable_pagination_link_color' => $this->getResultDatatablePaginationLinkColor(),
            'result_datatable_pagination_active_bck_color' => $this->getResultDatatablePaginationActiveBckColor(),
            'result_datatable_pagination_active_text_color' => $this->getResultDatatablePaginationActiveTextColor(),
            'samp_enabled' => $this->getSampEnabled(),
            'back_to_portal' => $this->getBackToPortal(),
            'user_menu_enabled' => $this->getUserMenuEnabled(),
            'search_by_criteria_allowed' => $this->getSearchByCriteriaAllowed(),
            'search_by_criteria_label' => $this->getSearchByCriteriaLabel(),
            'search_multiple_allowed' => $this->getSearchMultipleAllowed(),
            'search_multiple_label' => $this->getSearchMultipleLabel(),
            'search_multiple_all_datasets_selected' => $this->getSearchMultipleAllDatasetsSelected(),
            'search_multiple_progress_bar_title' => $this->getSearchMultipleProgressBarTitle(),
            'search_multiple_progress_bar_subtitle' => $this->getSearchMultipleProgressBarSubtitle(),
            'search_multiple_progress_bar_step_position' => $this->getSearchMultipleProgressBarStepPosition(),
            'search_multiple_progress_bar_step_datasets' => $this->getSearchMultipleProgressBarStepDatasets(),
            'search_multiple_progress_bar_step_result' => $this->getSearchMultipleProgressBarStepResult(),
            'documentation_allowed' => $this->getDocumentationAllowed(),
            'documentation_label' => $this->getDocumentationLabel(),
            'nb_dataset_families' => count($this->getDatasetFamilies()),
            'nb_datasets' => $this->getNbDatasets()
        ];
    }
}
