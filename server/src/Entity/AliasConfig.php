<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="alias")
 */
class AliasConfig implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="text", name="table_alias", nullable=false)
     */
    protected $tableAlias;

    /**
     * @var string
     *
     * @Column(type="text", name="column_alias", nullable=false)
     */
    protected $columnAlias;

    /**
     * @var string
     *
     * @Column(type="text", name="column_name", nullable=false)
     */
    protected $columnName;

    /**
     * @var string
     *
     * @Column(type="text", name="column_alias_long", nullable=false)
     */
    protected $columnAliasLong;

    public function getId()
    {
        return $this->id;
    }

    public function getTableAlias()
    {
        return $this->tableAlias;
    }

    public function setTableAlias($tableAlias)
    {
        $this->tableAlias = $tableAlias;
    }

    public function getColumnAlias()
    {
        return $this->columnAlias;
    }

    public function setColumnAlias($columnAlias)
    {
        $this->columnAlias = $columnAlias;
    }

    public function getColumnName()
    {
        return $this->columnName;
    }

    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;
    }

    public function getColumnAliasLong()
    {
        return $this->columnAliasLong;
    }

    public function setColumnAliasLong($columnAliasLong)
    {
        $this->columnAliasLong = $columnAliasLong;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'table_alias' => $this->getTableAlias(),
            'column_alias' => $this->getColumnAlias(),
            'column_name' => $this->getColumnName(),
            'column_alias_long' => $this->getColumnAliasLong()
        ];
    }
}
