<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="image")
 */
class Image implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", name="label", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", name="file_path", nullable=false)
     */
    protected $filePath;

    /**
     * @var integer
     *
     * @Column(type="bigint", name="file_size", nullable=false)
     */
    protected $fileSize;

    /**
     * @var float
     *
     * @Column(type="float", name="ra_min", nullable=false)
     */
    protected $raMin;

    /**
     * @var float
     *
     * @Column(type="float", name="ra_max", nullable=false)
     */
    protected $raMax;

    /**
     * @var float
     *
     * @Column(type="float", name="dec_min", nullable=false)
     */
    protected $decMin;

    /**
     * @var float
     *
     * @Column(type="float", name="dec_max", nullable=false)
     */
    protected $decMax;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $stretch;

    /**
     * @var float
     *
     * @Column(type="float", nullable=false)
     */
    protected $pmin;

    /**
     * @var float
     *
     * @Column(type="float", nullable=false)
     */
    protected $pmax;

    /**
     * @var Dataset
     *
     * @ManyToOne(targetEntity="Dataset")
     * @JoinColumn(name="dataset_name", referencedColumnName="name", nullable=false)
     */
    protected $dataset;

    public function __construct(Dataset $dataset)
    {
        $this->dataset = $dataset;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    public function getFileSize()
    {
        return $this->fileSize;
    }

    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;
    }

    public function getRaMin()
    {
        return $this->raMin;
    }

    public function setRaMin($raMin)
    {
        $this->raMin = $raMin;
    }

    public function getRaMax()
    {
        return $this->raMax;
    }

    public function setRaMax($raMax)
    {
        $this->raMax = $raMax;
    }

    public function getDecMin()
    {
        return $this->decMin;
    }

    public function setDecMin($decMin)
    {
        $this->decMin = $decMin;
    }

    public function getDecMax()
    {
        return $this->decMax;
    }

    public function setDecMax($decMax)
    {
        $this->decMax = $decMax;
    }

    public function getStretch()
    {
        return $this->stretch;
    }

    public function setStretch($stretch)
    {
        $this->stretch = $stretch;
    }

    public function getPmin()
    {
        return $this->pmin;
    }

    public function setPmin($pmin)
    {
        $this->pmin = $pmin;
    }

    public function getPmax()
    {
        return $this->pmax;
    }

    public function setPmax($pmax)
    {
        $this->pmax = $pmax;
    }

    public function getDataset()
    {
        return $this->dataset;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'file_path' => $this->getFilePath(),
            'file_size' => $this->getFileSize(),
            'ra_min' => $this->getRaMin(),
            'ra_max' => $this->getRaMax(),
            'dec_min' => $this->getDecMin(),
            'dec_max' => $this->getDecMax(),
            'stretch' => $this->getStretch(),
            'pmin' => $this->getPmin(),
            'pmax' => $this->getPmax()
        ];
    }
}
