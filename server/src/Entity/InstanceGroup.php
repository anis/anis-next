<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="instance_group")
 */
class InstanceGroup implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $role;

    /**
     * @var Instance[]
     *
     * Many Groups have Many Instances privileges.
     *
     * @ManyToMany(targetEntity="Instance")
     * @JoinTable(
     *     name="instance_groups_datasets",
     *     joinColumns={@JoinColumn(name="instance_group_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@JoinColumn(name="instance_name", referencedColumnName="name", onDelete="CASCADE")}
     * )
     */
    protected $instances;

    public function getId()
    {
        return $this->id;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getInstances()
    {
        return $this->instances;
    }

    public function setInstances($instances)
    {
        $this->instances = $instances;
    }

    public function jsonSerialize(): array
    {
        $instanceNames = array();
        foreach ($this->getInstances() as $instance) {
            $instanceNames[] = $instance->getName();
        }

        return [
            'id' => $this->getId(),
            'role' => $this->getRole(),
            'instances' => $instanceNames
        ];
    }
}
