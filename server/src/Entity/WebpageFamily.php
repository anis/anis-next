<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="webpage_family")
 */
class WebpageFamily implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $icon;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var Instance
     *
     * @ManyToOne(targetEntity="Instance")
     * @JoinColumn(name="instance_name", referencedColumnName="name", nullable=false, onDelete="CASCADE")
     */
    protected $instance;

    public function __construct(Instance $instance)
    {
        $this->instance = $instance;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'icon' => $this->getIcon(),
            'display' => $this->getDisplay()
        ];
    }
}
