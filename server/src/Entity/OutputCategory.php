<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="output_category")
 */
class OutputCategory implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var OutputFamily
     *
     * @ManyToOne(targetEntity="OutputFamily")
     * @JoinColumn(name="output_family", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $outputFamily;

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function setOutputFamily($outputFamily)
    {
        $this->outputFamily = $outputFamily;
    }

    public function getOutputFamily()
    {
        return $this->outputFamily;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'display' => $this->getDisplay(),
            'id_output_family' => $this->getOutputFamily()->getId()
        ];
    }
}
