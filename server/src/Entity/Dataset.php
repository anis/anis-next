<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="dataset")
 */
class Dataset implements \JsonSerializable
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", name="table_ref", nullable=false)
     */
    protected $tableRef;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var string
     *
     * @Column(type="string", name="data_path", nullable=true)
     */
    protected $dataPath;

    /**
     * @var bool
     *
     * @Column(type="boolean", nullable=false)
     */
    protected $public;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="download_json", nullable=false)
     */
    protected $downloadJson;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="download_csv", nullable=false)
     */
    protected $downloadCsv;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="download_ascii", nullable=false)
     */
    protected $downloadAscii;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="download_vo", nullable=false)
     */
    protected $downloadVo;

     /**
     * @var bool
     *
     * @Column(type="boolean", name="download_fits", nullable=false, options={"default" :false})
     */
    protected $downloadFits;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="server_link_enabled", nullable=false)
     */

    protected $serverLinkEnabled;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="datatable_enabled", nullable=false)
     */
    protected $datatableEnabled;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="datatable_selectable_rows", nullable=false)
     */
    protected $datatableSelectableRows;

    /**
     * @var ConeSearchConfig
     *
     * @OneToOne(targetEntity="ConeSearchConfig", cascade={"persist", "remove"})
     * @JoinColumn(name="cone_search_config_id", referencedColumnName="id", nullable=true)
     */
    private $coneSearchConfig;

    /**
     * @var DetailConfig
     *
     * @OneToOne(targetEntity="DetailConfig", cascade={"persist", "remove"})
     * @JoinColumn(name="detail_config_id", referencedColumnName="id", nullable=true)
     */
    private $detailConfig;

    /**
     * @var AliasConfig
     *
     * @OneToOne(targetEntity="AliasConfig", cascade={"persist", "remove"})
     * @JoinColumn(name="alias_config_id", referencedColumnName="id", nullable=true)
     */
    private $aliasConfig;

    /**
     * @var Database
     *
     * @ManyToOne(targetEntity="Database")
     * @JoinColumn(name="id_database", referencedColumnName="id", nullable=false)
     */
    protected $database;

    /**
     * @var DatasetFamily
     *
     * @ManyToOne(targetEntity="DatasetFamily", inversedBy="datasets")
     * @JoinColumn(name="id_dataset_family", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $datasetFamily;

    /**
     * @var Attribute[]
     *
     * @OneToMany(targetEntity="Attribute", mappedBy="dataset")
     */
    protected $attributes;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->attributes = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTableRef()
    {
        return $this->tableRef;
    }

    public function setTableRef($tableRef)
    {
        $this->tableRef = $tableRef;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function getDataPath()
    {
        return $this->dataPath;
    }

    public function setDataPath($dataPath)
    {
        $this->dataPath = $dataPath;
    }

    public function getPublic()
    {
        return $this->public;
    }

    public function setPublic($public)
    {
        $this->public = $public;
    }

    public function getDownloadJson()
    {
        return $this->downloadJson;
    }

    public function setDownloadJson($downloadJson)
    {
        $this->downloadJson = $downloadJson;
    }

    public function getDownloadCsv()
    {
        return $this->downloadCsv;
    }

    public function setDownloadCsv($downloadCsv)
    {
        $this->downloadCsv = $downloadCsv;
    }

    public function getDownloadAscii()
    {
        return $this->downloadAscii;
    }

    public function setDownloadAscii($downloadAscii)
    {
        $this->downloadAscii = $downloadAscii;
    }

    public function getDownloadVo()
    {
        return $this->downloadVo;
    }

    public function setDownloadVo($downloadVo)
    {
        $this->downloadVo = $downloadVo;
    }

    public function getDownloadFits()
    {
        return $this->downloadFits;
    }

    public function setDownloadFits($downloadFits)
    {
        $this->downloadFits = $downloadFits;
    }

    public function getServerLinkEnabled()
    {
        return $this->serverLinkEnabled;
    }

    public function setServerLinkEnabled($serverLinkEnabled)
    {
        $this->serverLinkEnabled = $serverLinkEnabled;
    }

    public function getDatatableEnabled()
    {
        return $this->datatableEnabled;
    }

    public function setDatatableEnabled($datatableEnabled)
    {
        $this->datatableEnabled = $datatableEnabled;
    }

    public function getDatatableSelectableRows()
    {
        return $this->datatableSelectableRows;
    }

    public function setDatatableSelectableRows($datatableSelectableRows)
    {
        $this->datatableSelectableRows = $datatableSelectableRows;
    }

    public function getConeSearchConfig()
    {
        return $this->coneSearchConfig;
    }

    public function setConeSearchConfig($coneSearchConfig)
    {
        $this->coneSearchConfig = $coneSearchConfig;
    }

    public function getDetailConfig()
    {
        return $this->detailConfig;
    }

    public function setDetailConfig($detailConfig)
    {
        $this->detailConfig = $detailConfig;
    }

    public function getAliasConfig()
    {
        return $this->aliasConfig;
    }

    public function setAliasConfig($aliasConfig)
    {
        $this->aliasConfig = $aliasConfig;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getDatasetFamily()
    {
        return $this->datasetFamily;
    }

    public function setDatasetFamily($datasetFamily)
    {
        $this->datasetFamily = $datasetFamily;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getFullDataPath()
    {
        return $this->getDatasetFamily()->getInstance()->getDataPath() . $this->getDataPath();
    }

    public function jsonSerialize(): array
    {
        return [
            'name' => $this->getName(),
            'table_ref' => $this->getTableRef(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'display' => $this->getDisplay(),
            'data_path' => $this->getDataPath(),
            'public' => $this->getPublic(),
            'download_json' => $this->getDownloadJson(),
            'download_csv' => $this->getDownloadCsv(),
            'download_ascii' => $this->getDownloadAscii(),
            'download_vo' => $this->getDownloadVo(),
            'download_fits' => $this->getDownloadFits(),
            'server_link_enabled' => $this->getServerLinkEnabled(),
            'datatable_enabled' => $this->getDatatableEnabled(),
            'datatable_selectable_rows' => $this->getDatatableSelectableRows(),
            'cone_search_config_id' => is_null($this->getConeSearchConfig())
            ? null : $this->getConeSearchConfig()->getId(),
            'detail_config_id' => is_null($this->getDetailConfig())
            ? null : $this->getDetailConfig()->getId(),
            'alias_config_id' => is_null($this->getAliasConfig())
            ? null : $this->getAliasConfig()->getId(),
            'id_database' => $this->getDatabase()->getId(),
            'id_dataset_family' => $this->getDatasetFamily()->getId(),
            'full_data_path' => $this->getFullDataPath(),
        ];
    }
}
