<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="dataset_family")
 */
class DatasetFamily implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var bool
     *
     * @Column(type="boolean", nullable=false)
     */
    protected $opened;

    /**
     * @var Instance
     *
     * @ManyToOne(targetEntity="Instance", inversedBy="datasetFamilies")
     * @JoinColumn(name="instance_name", referencedColumnName="name", nullable=false, onDelete="CASCADE")
     */
    protected $instance;

    /**
     * @var Dataset[]
     *
     * @OneToMany(targetEntity="Dataset", mappedBy="datasetFamily")
     */
    protected $datasets;

    public function __construct(Instance $instance)
    {
        $this->instance = $instance;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function getOpened()
    {
        return $this->opened;
    }

    public function setOpened($opened)
    {
        $this->opened = $opened;
    }

    public function getInstance()
    {
        return $this->instance;
    }

    public function getDatasets()
    {
        return $this->datasets;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'display' => $this->getDisplay(),
            'opened' => $this->getOpened()
        ];
    }
}
