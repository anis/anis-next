<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="dataset_group")
 */
class DatasetGroup implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $role;

    /**
     * @var Instance
     *
     * @ManyToOne(targetEntity="Instance")
     * @JoinColumn(name="instance_name", referencedColumnName="name", nullable=false, onDelete="CASCADE")
     */
    protected $instance;

    /**
     * @var Dataset[]
     *
     * Many Groups have Many Datasets privileges.
     *
     * @ManyToMany(targetEntity="Dataset")
     * @JoinTable(
     *     name="datasets_groups",
     *     joinColumns={@JoinColumn(name="dataset_group_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@JoinColumn(name="dataset_name", referencedColumnName="name", onDelete="CASCADE")}
     * )
     */
    protected $datasets;

    public function __construct(Instance $instance)
    {
        $this->instance = $instance;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getInstance()
    {
        return $this->instance;
    }

    public function getDatasets()
    {
        return $this->datasets;
    }

    public function setDatasets($datasets)
    {
        $this->datasets = $datasets;
    }

    public function jsonSerialize(): array
    {
        $datasetNames = array();
        foreach ($this->getDatasets() as $dataset) {
            $datasetNames[] = $dataset->getName();
        }

        return [
            'id' => $this->getId(),
            'role' => $this->getRole(),
            'instance_name' => $this->getInstance()->getName(),
            'datasets' => $datasetNames
        ];
    }
}
