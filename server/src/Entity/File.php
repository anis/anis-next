<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="file")
 */
class File implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", name="label", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", name="file_path", nullable=false)
     */
    protected $filePath;

    /**
     * @var integer
     *
     * @Column(type="integer", name="file_size", nullable=false)
     */
    protected $fileSize;

    /**
     * @var string
     *
     * @Column(type="string", name="type", nullable=false)
     */
    protected $type;

    /**
     * @var Dataset
     *
     * @ManyToOne(targetEntity="Dataset")
     * @JoinColumn(name="dataset_name", referencedColumnName="name", nullable=false)
     */
    protected $dataset;

    public function __construct(Dataset $dataset)
    {
        $this->dataset = $dataset;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    public function getFileSize()
    {
        return $this->fileSize;
    }

    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'file_path' => $this->getFilePath(),
            'file_size' => $this->getFileSize(),
            'type' => $this->getType()
        ];
    }
}
