<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use App\Entity\Dataset;

/**
 * Represents the Anis Count Query Part
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
class Count extends AbstractQueryPart
{
    /**
     * Add select count clause to the request with the alias nb
     *
     * @param AnisQueryBuilder $anisQueryBuilder Represents the query being built
     * @param Dataset          $dataset      Represents the requested dataset
     * @param string[]         $queryParams  The query params of the url (after ?)
     */
    public function __invoke(AnisQueryBuilder $anisQueryBuilder, Dataset $dataset, array $queryParams): void
    {
        if ($queryParams['a'] === 'count') {
            $anisQueryBuilder->getDoctrineQueryBuilder()->select('COUNT(*) as nb');
            $anisQueryBuilder->setAttributesSelected(array());
        }
    }
}
