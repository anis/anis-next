<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use Doctrine\DBAL\Query\Expression\CompositeExpression;
use App\Entity\Dataset;
use App\Search\Query\Operator\IOperatorFactory;

/**
 * Represents the Anis Where Query Part
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
class Where extends AbstractQueryPart
{
    /**
     * Factory used to create a criterion
     *
     * @var IOperatorFactory
     */
    private $operatorFactory;

    /**
     * Create the class with an operator factory
     *
     * @param IOperatorFactory $operatorFactory
     */
    public function __construct(IOperatorFactory $operatorFactory)
    {
        $this->operatorFactory = $operatorFactory;
    }

    /**
     * Add where clause to the request using operator factory to build criteria
     *
     * @param AnisQueryBuilder $anisQueryBuilder Represents the query being built
     * @param Dataset          $dataset          Represents the requested dataset
     * @param string[]         $queryParams      The query params of the url (after ?)
     */
    public function __invoke(AnisQueryBuilder $anisQueryBuilder, Dataset $dataset, array $queryParams): void
    {
        if (array_key_exists('c', $queryParams)) {
            $queryBuilder = $anisQueryBuilder->getDoctrineQueryBuilder();
            $criteria = explode(';', $queryParams['c']);
            $expressions = array();
            foreach ($criteria as $criterion) {
                $params = $params = explode('::', $criterion);
                $attribute = $this->getAttribute($dataset, (int) $params[0]);
                $column = $dataset->getTableRef() . '.' . $attribute->getName();
                $columnType = $attribute->getType();
                if (array_key_exists(2, $params)) {
                    $values = explode('|', $params[2]);
                } else {
                    $values = array();
                }
                $operator = $this->operatorFactory->create(
                    $params[1],
                    $queryBuilder->expr(),
                    $column,
                    $columnType,
                    $values
                );
                $expressions[] = $operator->getExpression();
            }
            $queryBuilder->andWhere(new CompositeExpression(CompositeExpression::TYPE_AND, $expressions));
        }
    }
}
