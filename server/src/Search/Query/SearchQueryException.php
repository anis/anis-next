<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use App\Search\SearchException;

/**
 * Represents and references the exceptions catch by the package Query
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
class SearchQueryException extends SearchException
{
    /**
     * Occurs when one of the attributes in the parameter a does not exist
     *
     * @param int    $id           ID number not found
     * @param string $datasetLabel Dataset label of selected dataset (URL)
     *
     * @return SearchQueryException
     */
    public static function attributeNotFound(int $id, string $datasetLabel): SearchQueryException
    {
        return new self("Attribute with the id " . $id . " is not found for the dataset " . $datasetLabel);
    }

    /**
     * @return SearchQueryException
     */
    public static function badNumberOfParamsForOrder(): SearchQueryException
    {
        return new self("Order (o) needs 2 params to work");
    }

    /**
     * Parameter o must be of type a (asc) or d (desc)
     *
     * @param string $order Type of order given by user (URL)
     *
     * @return SearchQueryException
     */
    public static function typeOfOrderDoesNotExist(string $order): SearchQueryException
    {
        return new self("The type of order '" . $order . "' does not exist");
    }

    /**
     * @return SearchQueryException
     */
    public static function badNumberOfParamsForLimit(): SearchQueryException
    {
        return new self("Limit (p) needs 2 params to work");
    }

    /**
     * @return SearchQueryException
     */
    public static function badNumberOfParamsForConeSearch(): SearchQueryException
    {
        return new self("Cone search (cs) needs 3 params to work");
    }

    /**
     * @return SearchQueryException
     */
    public static function coneSearchUnavailable(): SearchQueryException
    {
        return new self("The cone search is unavailable for this dataset");
    }
}
