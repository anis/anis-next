<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use App\Entity\Dataset;
use App\Entity\Attribute;

/**
 * Abstract class extends by query parts class
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
abstract class AbstractQueryPart implements IQueryPart
{
    /**
     * Returns the Attribute object of a dataset based on its ID (primary key)
     *
     * @param  Dataset         $dataset      Represents the requested dataset
     * @param  int             $id           Unique identifier for the requested attribute
     *
     * @return Attribute       Returns the attribute found
     * @throws SearchException Attribute with ID not found into the selected dataset
     */
    protected function getAttribute(Dataset $dataset, int $id): Attribute
    {
        $attributes = $dataset->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getId() === $id) {
                return $attribute;
            }
        }
        throw SearchQueryException::attributeNotFound($id, $dataset->getLabel());
    }
}
