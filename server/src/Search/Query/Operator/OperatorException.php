<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use App\Search\SearchException;

/**
 * Represents and references the exceptions thrown by the package operator
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query\Operator
 */
class OperatorException extends SearchException
{
    /**
     * Occurs when the main program has called on an operator type who does not exist in the factory
     *
     * @param string $operator
     *
     * @return OperatorException
     */
    public static function unknownOperator(string $operator): OperatorException
    {
        return new self("The given operator '" . $operator . "' is unknown");
    }

    /**
     * Occurs when the main program has called on an operator with the bad numbers of parameters
     *
     * @param string $operator
     * @param int    $numberOfParametersRequired
     *
     * @return OperatorException
     */
    public static function operatorBadNumberOfParameters(
        string $operator,
        int $numberOfParametersRequired
    ): OperatorException {
        return new self("Operator '" . $operator . "' needs " . $numberOfParametersRequired . ' values to work');
    }

    /**
     * Specific exception for in or not in operators.
     * Occurs when the main program has called in or not in with the bad numbers of parameters
     *
     * @param string $operator
     *
     * @return OperatorException
     */
    public static function inBadNumberOfParameters(string $operator): OperatorException
    {
        return new self("Operator '" . $operator . "' needs at least 1 parameter to work");
    }

    /**
     * Occurs when the column type for a given criterion is different from the type of the value entered by the user
     *
     * @param string $value
     *
     * @return OperatorException
     */
    public static function valueIsNotNumeric(string $value): OperatorException
    {
        return new self("You passed a string value '" . $value . "' or the criterion value required a numeric value");
    }

    public static function operatorJsonNeedColumnTypeJson()
    {
        return new self("Operator js need a json attribute type");
    }
}
