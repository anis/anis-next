<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

/**
 * Operator that represents a not null of a where clause
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query\Operator
 */
class OperatorNotNull extends Operator
{
    /**
     * This method returns the not null expression for this criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        return $this->expr->isNotNull($this->column);
    }
}
