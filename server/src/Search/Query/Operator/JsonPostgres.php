<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Operator that represents a json postgres where clause
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query\Operator
 */
class JsonPostgres extends Operator
{
    /**
     * Path inside the json to the criterion
     *
     * @var string|array
     */
    private $path;

    /**
     * Operator fot this json postgres criterion
     *
     * @var string
     */
    private $operator;

    /**
     * Value of this criterion
     *
     * @var string
     */
    private $value;

    /**
     * Create the class before call getExpression method to execute this operator
     *
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string            $path
     * @param string            $operator
     * @param string            $value
     */
    public function __construct(
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        string $path,
        string $operator,
        string $value
    ) {
        parent::__construct($expr, $column, $columnType);
        $this->path = explode(',', $path);
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * This method returns the json postgres expression for this criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        switch ($this->operator) {
            case 'eq':
                $expr = $this->expr->eq($this->getColumn(), $this->getValue());
                break;
            case 'gt':
                $expr = $this->expr->gt($this->getColumn(), $this->getValue());
                break;
            case 'gte':
                $expr = $this->expr->gte($this->getColumn(), $this->getValue());
                break;
            case 'lt':
                $expr = $this->expr->lt($this->getColumn(), $this->getValue());
                break;
            case 'lte':
                $expr = $this->expr->lte($this->getColumn(), $this->getValue());
                break;
            default:
                $expr = '';
        }
        return $expr;
    }

    private function getColumn(): string
    {
        return $this->column . '->' . implode('->', array_map(array($this, 'getSqlValue'), $this->path));
    }

    private function getValue()
    {
        if (is_numeric($this->value)) {
            $newValue = 1 * $this->value;
        } elseif ($this->value === 'true') {
            $newValue = true;
        } elseif ($this->value === 'false') {
            $newValue = false;
        } else {
            $newValue = $this->value;
        }

        return $this->getSqlValue(json_encode($newValue));
    }
}
