<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Operator that represents a sql not in of a where clause
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query\Operator
 */
class NotIn extends Operator
{
    /**
     * Values of this criterion
     *
     * @var string[]
     */
    private $values;

    /**
     * Create the class before call getExpression method to execute this operator
     *
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string[]          $values
     */
    public function __construct(ExpressionBuilder $expr, string $column, string $columnType, array $values)
    {
        parent::__construct($expr, $column, $columnType);
        foreach ($values as $value) {
            $this->verifyTypeCompatibility($value);
        }
        $this->values = $values;
    }

    /**
     * This method returns the not in expression for this criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        return $this->expr->notIn($this->column, array_map(array($this, 'getSqlValue'), $this->values));
    }
}
