<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Operator that represents a sql not like of a where clause
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query\Operator
 */
class NotLike extends Operator
{
    /**
     * Value of this criterion
     *
     * @var string
     */
    private $value;

    /**
     * Create the class before call getExpression method to execute this operator
     *
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string            $value
     */
    public function __construct(ExpressionBuilder $expr, string $column, string $columnType, string $value)
    {
        parent::__construct($expr, $column, $columnType);
        $this->verifyTypeCompatibility($value);
        $this->value = $value;
    }

    /**
     * This method returns the not like expression for this criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        return $this->expr->notLike(
            'CAST(' . $this->column . ' AS text)',
            $this->expr->literal('%' . $this->value . '%')
        );
    }
}
