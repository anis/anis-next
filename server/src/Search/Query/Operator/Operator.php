<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\DBAL\Types\Types;
use is_numeric;

/**
 * Abstract class that represents an operator.
 * An operator represents a where clause for a query.
 * Example: column1 = value
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query\Operator
 */
abstract class Operator implements IOperator
{
    /**
     * Doctrine provides this object to help construct the where clause
     *
     * @var ExpressionBuilder
     */
    protected $expr;

    /**
     * Identifies the database column on which the where clause will be applied
     *
     * @var string
     */
    protected $column;

    /**
     * Contains the type of the column
     *
     * @var string
     */
    protected $columnType;

    /**
     * Create the class before call getExpression method to execute this operator
     *
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     */
    public function __construct(ExpressionBuilder $expr, string $column, string $columnType)
    {
        $this->expr = $expr;
        $this->column = $column;
        $this->columnType = $columnType;
    }

    /**
     * Allows to verify the compatibility between the value sent in parameter of the operator and the type of the column
     *
     * @param string $value
     *
     * @throws OperatorException if the compatibility is incorrect
     */
    protected function verifyTypeCompatibility(string $value): void
    {
        if ($this->isColumnTypeNumeric() && !is_numeric($value)) {
            throw OperatorException::valueIsNotNumeric($value);
        }
    }

    /**
     * Returns the raw value if this type is numeric otherwise with quotes (string type)
     *
     * @param string $value
     *
     * @return string
     */
    protected function getSqlValue(string $value): string
    {
        if ($this->isColumnTypeNumeric()) {
            return $value;
        } else {
            return $this->expr->literal($value);
        }
    }

    /**
     * Returns true if the column type is numeric else returns false
     *
     * @return bool
     */
    protected function isColumnTypeNumeric(): bool
    {
        switch ($this->columnType) {
            case Types::BIGINT:
            case Types::DECIMAL:
            case Types::INTEGER:
            case Types::SMALLINT:
            case Types::FLOAT:
                return true;
            default:
                return false;
        }
    }
}
