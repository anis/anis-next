<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search;

use Exception;

/**
 * Represents and references the exceptions catch by the class SearchAction
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search
 */
class SearchException extends Exception
{
}
