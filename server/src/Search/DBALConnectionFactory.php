<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search;

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Connection;
use App\Entity\Database;

/**
 * Factory used to create a Doctrine DBAL connection to a business database
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search
 */
class DBALConnectionFactory
{
    /**
     * This method create a connection to a business database.
     * It retrieves connection information from the Database object.
     *
     * Database object stores information about a business database in
     * the metamodel
     *
     * @param Database $database This object contains the database connection parameters
     *
     * @return Connection
     */
    public function create(Database $database): Connection
    {
        $config = new \Doctrine\DBAL\Configuration();
        $connectionParams = array(
            'dbname' => $database->getDbName(),
            'user' => $database->getLogin(),
            'password' => $database->getPassword(),
            'host' => $database->getHost(),
            'port' => $database->getPort(),
            'driver' => $database->getType(),
        );
        $connection = DriverManager::getConnection($connectionParams, $config);

        // Permet de faire fonctionner Doctrine avec les fonctions de pgsphere
        $connection->getDatabasePlatform()->registerDoctrineTypeMapping('spoly', 'string');
        $connection->getDatabasePlatform()->registerDoctrineTypeMapping('spoint', 'string');

        return $connection;
    }
}
