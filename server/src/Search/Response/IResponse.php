<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

use Psr\Http\Message\ResponseInterface;
use App\Search\Query\AnisQueryBuilder;

/**
 * Interface implemented by search response formatting classes
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Response
 */
interface IResponse
{
    public function getResponse(ResponseInterface $response, AnisQueryBuilder $anisQueryBuilder): ResponseInterface;
}
