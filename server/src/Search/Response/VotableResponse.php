<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

use Psr\Http\Message\ResponseInterface;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;

/**
 * Class to build the votable search response
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Response
 */
class VotableResponse implements IResponse
{
    /**
     * @param  ResponseInterface $response PSR-7   This object represents the HTTP response
     * @param  AnisQueryBuilder  $anisQueryBuilder Object used to wrap the Doctrine DBAL Query Builder
     *
     * @return ResponseInterface
     */
    public function getResponse(ResponseInterface $response, AnisQueryBuilder $anisQueryBuilder): ResponseInterface
    {
        $nbRecords = $this->getNbRecords($anisQueryBuilder);
        $stmt = $anisQueryBuilder->getDoctrineQueryBuilder()->executeQuery();
        $dataset = $anisQueryBuilder->getDatasetSelected();
        $attributes = $anisQueryBuilder->getAttributesSelected();
        $payload = $this->getVoHeader($dataset, $attributes, $nbRecords);
        while ($row = $stmt->fetchAssociative()) {
            $payload .= $this->getVoRow($row, $attributes);
        }
        $payload .= $this->getVoFooter();
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'text/xml');
    }

    /**
     * @param  AnisQueryBuilder  $anisQueryBuilder Object used to wrap the Doctrine DBAL Query Builder
     *
     * @return int
     */
    private function getNbRecords(AnisQueryBuilder $anisQueryBuilder): int
    {
        $doctrineQueryBuilder = $anisQueryBuilder->getDoctrineQueryBuilder();
        $sql = 'SELECT count(*) as nb FROM (' . $doctrineQueryBuilder .  ') as sub';
        return $doctrineQueryBuilder->getConnection()->fetchOne($sql);
    }

    /**
     * @param Dataset     $dataset    The query dataset selected
     * @param Attribute[] $attributes The query attributes selected
     * @param int         $nbRecords  The number of records for this query
     *
     * @return string
     */
    private function getVoHeader(Dataset $dataset, array $attributes, int $nbRecords): string
    {
        $str = '<?xml version="1.0" encoding="UTF-8" ?>';
        $str .= '<VOTABLE version="1.2" xmlns="http://www.ivoa.net/xml/VOTable/v1.2" ';
        $str .= 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:stc="http://www.ivoa.net/xml/STC/v1.30">';
        $str .= '<DESCRIPTION>';
        $str .= 'This file has been created by ANIS the ' . date(DATE_RFC822) . ' ; https://anis.lam.fr';
        $str .= '</DESCRIPTION>';
        $str .= '<RESOURCE>';
        $str .= '<TABLE name="' . $dataset->getLabel() . '" nrows="' . $nbRecords . '">';
        foreach ($attributes as $attribute) {
            $str .= '<FIELD name="' . $attribute->getLabel() . '"';
            if ($attribute->getVoUcd() !== null) {
                $str .= ' ucd="' . $attribute->getVoUcd() . '"';
            }
            if ($attribute->getVoDatatype() !== null) {
                $str .= ' datatype="' . $attribute->getVoDatatype() . '"';
            }
            if ($attribute->getVoUnit() !== null) {
                $str .= ' unit="' . $attribute->getVoUnit() . '"';
            }
            if ($attribute->getVoSize() !== null) {
                $str .= ' arraysize="' . $attribute->getVoSize() . '"';
            }
            if ($attribute->getVoUtype() !== null) {
                $str .= ' utype="' . $attribute->getVoUtype() . '"';
            }
            $str .= '>';
            if ($attribute->getVoDescription() !== null) {
                $str .= '<DESCRIPTION>' . htmlspecialchars($attribute->getVoDescription()) . '</DESCRIPTION>';
            }
            $str .= '</FIELD>';
        }
        $str .= '<DATA>';
        return $str . '<TABLEDATA>';
    }

    /**
     * @param array       $row        Array contains a record of the result
     * @param Attribute[] $attributes The query attributes selected
     *
     * @return string
     */
    protected function getVoRow(array $row, array $attributes): string
    {
        $str = '<TR>';
        foreach ($attributes as $attribute) {
            $str .= '<TD>' . $row[$attribute->getLabel()] . '</TD>';
        }
        return $str . '</TR>';
    }

    /**
     * @return string
     */
    private function getVoFooter(): string
    {
        return '</TABLEDATA></DATA></TABLE></RESOURCE></VOTABLE>';
    }
}
