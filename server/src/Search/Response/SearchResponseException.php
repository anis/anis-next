<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

use App\Search\SearchException;

/**
 * Represents and references the exceptions catch by the package Response
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Response
 */
class SearchResponseException extends SearchException
{
    /**
     * @return SearchResponseException
     */
    public static function typeOfFormatDoesNotExist(): SearchResponseException
    {
        return new self("The parameter (f) needs a supported format (json, csv, ascii)");
    }
}
