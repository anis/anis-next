<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Nyholm\Psr7\Response;

/**
 * Middleware to protected a group of routes (Authorization required)
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Middleware
 */
final class RouteGuardMiddleware implements MiddlewareInterface
{
    /**
     * True if authorization is activated false else
     *
     * @var bool
     */
    private $authorizationEnabled;

    /**
     * List the HTTP methods to be protected for the requested route
     *
     * @var array
     */
    private $methods;

    /**
     * Roles to which the user must belong to past the guard
     *
     * @var array
     */
    private $roles;

    /**
     * Create the classe before call process to execute this middleware
     *
     * @param array  $authorizationEnabled True if authorization is activated false else
     * @param bool   $methods              List the HTTP methods to be protected for the requested route
     * @param array  $roles                Roles to which the user must belong to past the guard
     */
    public function __construct(bool $authorizationEnabled, array $methods, array $roles)
    {
        $this->authorizationEnabled = $authorizationEnabled;
        $this->methods = $methods;
        $this->roles = $roles;
    }

    /**
     * Verify authorization
     *
     * @param  ServerRequestInterface   $request  PSR-7  This object represents the HTTP request
     * @param  RequestHandlerInterface  $handler  PSR-15 Request handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request->getMethod() === OPTIONS || !$this->authorizationEnabled) {
            return $handler->handle($request);
        }

        if (!in_array($request->getMethod(), $this->methods)) {
            return $handler->handle($request);
        }

        // Token not found
        $token = $request->getAttribute('token');
        if (!$token) {
            return $this->getResponse('HTTP 401: This url need a valid token', 401);
        }

        // Searches if any of the administrator roles are present in the token roles.
        $admin = false;
        for ($i = 0; $i < count($this->roles); $i++) {
            $admin = in_array($this->roles[$i], $token->realm_access->roles);
            if ($admin) {
                break;
            }
        }
        // If admin is false 403
        if (!$admin) {
            return $this->getResponse('HTTP 403: This url need a higher level of permission', 403);
        }

        return $handler->handle($request);
    }

    /**
     * @param string $message Unauthorized response message
     * @param int    $code    HTTP code returns
     *
     * @return Response
     */
    private function getResponse(string $message, int $code): Response
    {
        $resonse = new Response();
        $resonse->getBody()->write(json_encode(array(
            'message' => $message
        )));
        return $resonse->withStatus($code)->withHeader('Access-Control-Allow-Origin', '*');
    }
}
