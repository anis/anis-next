<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Nyholm\Psr7\Response as NyholmResponse;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Log\LoggerInterface;
use Firebase\JWT\JWT;
use Firebase\JWT\JWK;

/**
 * Middleware to handle Authorization request header (JWT)
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Middleware
 */
final class AuthorizationMiddleware implements MiddlewareInterface
{
    /**
     * The logger interface is the central access point to log information
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Contains settings to handle Json Web Token
     *
     * @var array
     */
    private $settings;

    /**
     * Create the classe before call process to execute this middleware
     *
     * @param LoggerInterface $logger   The logger interface
     * @param array           $settings Settings about token
     */
    public function __construct(LoggerInterface $logger, array $settings)
    {
        $this->logger = $logger;
        $this->settings = $settings;
    }

    /**
     * Try to validating and verifing the signature of the json web token
     * if Authorization header is present
     *
     * @param  ServerRequestInterface   $request  PSR-7 This object represents the HTTP request
     * @param  RequestHandlerInterface  $handler  PSR-15 request handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (
            $request->getMethod() === OPTIONS
            || (!$request->hasHeader('Authorization') && !array_key_exists('token', $request->getQueryParams()))
            || !boolval($this->settings['enabled'])
        ) {
            return $handler->handle($request);
        }

        // Get token string from Authorizarion header
        if ($request->hasHeader('Authorization')) {
            $bearer = $request->getHeader('Authorization')[0];
        } else {
            $bearer = 'Bearer ' . $request->getQueryParams()['token'];
        }

        $data = explode(' ', $bearer);
        if ($data[0] !== 'Bearer') {
            return $this->getUnauthorizedResponse(
                'HTTP 401: Authorization must contain a string with the following format -> Bearer JWT'
            );
        }

        $keys = $this->getJwkKeySet();

        try {
            $token = JWT::decode($data[1], $keys, array('RS256'));
        } catch (\Exception $e) {
            return $this->getUnauthorizedResponse('HTTP 401: ' . $e->getMessage());
        }

        return $handler->handle($request->withAttribute('token', $token));
    }

    private function getJwkKeySet()
    {
        if (!($jwks = apcu_fetch('jwks'))) {
            $this->logger->info('Jwks not found, retrieve jwkKeySet and cache the result');
            $certsUrl = $this->settings['jwks_url'];
            $jwks = file_get_contents($certsUrl);
            apcu_store('jwks', $jwks);
        }

        $keys = JWK::parseKeySet(json_decode($jwks, true));
        return $keys;
    }

    /**
     * @param string $message Unauthorized response message
     *
     * @return Response
     */
    private function getUnauthorizedResponse(string $message): NyholmResponse
    {
        $resonse = new NyholmResponse();
        $resonse->getBody()->write(json_encode(array(
            'message' => $message
        )));
        return $resonse->withStatus(401)->withHeader('Access-Control-Allow-Origin', '*');
    }
}
