<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;

/**
 * Middleware to force content type to application/json
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Middleware
 */
final class ContentTypeJsonMiddleware implements MiddlewareInterface
{
    /**
     * Force return response content type to application/json
     *
     * @param  ServerRequestInterface   $request  PSR-7  This object represents the HTTP request
     * @param  RequestHandlerInterface  $handler  PSR-15 Request handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        if ($response->hasHeader('Content-Type')) {
            return $response;
        } else {
            return $response->withHeader('Content-Type', 'application/json');
        }
    }
}
