<?php

/*
 * This file is part of Anis Auth.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;

/**
 * Middleware to allow resources to be requested from another origin
 *
 * @author Tifenn Guillas <tifenn.guillas@lam.fr>
 * @package App\Middleware
 */
final class CorsMiddleware implements MiddlewareInterface
{
    /**
     * Allow resources to be requested from another origin
     *
     * @param  ServerRequestInterface   $request  PSR-7  This object represents the HTTP request
     * @param  RequestHandlerInterface  $handler  PSR-15 Request handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);

        if ($request->getMethod() === OPTIONS) {
            return $response
                ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
                ->withHeader('Access-Control-Allow-Origin', '*');
        }

        return $response
            ->withHeader('Access-Control-Allow-Origin', '*');
    }
}
