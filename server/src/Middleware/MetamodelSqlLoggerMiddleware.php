<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Middleware to log metamodel sql requests
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Middleware
 */
final class MetamodelSqlLoggerMiddleware implements MiddlewareInterface
{
    /**
     * The logger interface is the central access point to log information
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The EntityManager is the central access point to Doctrine ORM functionality
     *
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Create the classe before call process to execute this middleware
     *
     * @param LoggerInterface         $logger  The logger interface
     * @param EntityManagerInterface  $em      Doctrine Entity Manager Interface
     */
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->em = $em;
    }

    /**
     * Log metamodel doctrine sql requests
     *
     * @param  ServerRequestInterface   $request  PSR-7  This object represents the HTTP request
     * @param  RequestHandlerInterface  $handler  PSR-15 Request handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        $sqlLogger = $this->em->getConfiguration()->getSQLLogger();

        if ($sqlLogger) {
            $nbQueries = count($sqlLogger->queries);
            $this->logger->info('----------- METAMODEL SQL LOGGER -----------');
            $this->logger->info('Number of sql requests : ' . $nbQueries);
            for ($i = 1; $i <= $nbQueries; $i++) {
                $this->logger->info($i . ' : ' . json_encode($sqlLogger->queries[$i]));
            }
            $this->logger->info('----------- METAMODEL SQL LOGGER -----------');
        }

        return $response;
    }
}
