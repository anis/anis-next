<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Exception\HttpNotFoundException;
use Nyholm\Psr7\Factory\Psr17Factory;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class InstanceFileExplorerAction extends AbstractAction
{
    /**
     * Contains ANIS data path value
     *
     * @var string
     */
    private $dataPath;

    /**
     * Contains settings to handle Json Web Token (app/settings.php)
     *
     * @var array
     */
    private $settings;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em        Doctrine Entity Manager Interface
     * @param string                 $dataPath  Contains anis-server data path
     * @param array                  $settings  Settings about token
     */
    public function __construct(EntityManagerInterface $em, string $dataPath, array $settings)
    {
        parent::__construct($em);
        $this->dataPath = $dataPath;
        $this->settings = $settings;
    }

    /**
     * `GET` Returns the list of files if path is a directory or stream file
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct instance with primary key
        $instance = $this->em->find('App\Entity\Instance', $args['name']);

        // If instance is not found 404
        if (is_null($instance)) {
            throw new HttpNotFoundException(
                $request,
                'Instance with name ' . $args['name'] . ' is not found'
            );
        }

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && boolval($this->settings['enabled'])) {
            $this->verifyInstanceAuthorization(
                $request,
                $instance->getName(),
                explode(',', $this->settings['admin_roles'])
            );
        }

        $path = $this->dataPath . $instance->getDataPath() . $instance->getFilesPath();

        if (array_key_exists('fpath', $args)) {
            $path .= $args['fpath'];
        }

        if (!file_exists($path)) {
            throw new HttpNotFoundException(
                $request,
                'The requested path is not found'
            );
        }

        if (is_file($path)) {
            // If the file found so stream it
            $psr17Factory = new Psr17Factory();
            $stream = $psr17Factory->createStreamFromFile($path, 'r');

            return $response->withBody($stream)
                ->withHeader('Content-Type', mime_content_type($path))
                ->withHeader('Content-Length', filesize($path));
        } else {
            $files = array();

            foreach (scandir($path) as $file) {
                $type = filetype($path . DIRECTORY_SEPARATOR . $file);
                if ($type === 'link') {
                    $targetFile = readlink($path . DIRECTORY_SEPARATOR . $file);
                    $size = filesize($targetFile);
                    $type = filetype($targetFile);
                    $mimetype = mime_content_type($targetFile);
                } else {
                    $size = filesize($path . DIRECTORY_SEPARATOR . $file);
                    $mimetype = mime_content_type($path . DIRECTORY_SEPARATOR . $file);
                }

                $files[] = array(
                    'name' => $file,
                    'size' => $size,
                    'type' => $type,
                    'mimetype' => $mimetype
                );
            }

            $response->getBody()->write(json_encode($files));
            return $response;
        }
    }
}
