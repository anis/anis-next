<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class ClientSettingsAction
{
    /**
     * The ANIS settings array
     *
     * @var array
     */
    private $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    /**
     * This action returns the ANIS settings needed to start the web client
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $payload = json_encode(array(
            'servicesUrl' => $this->settings['services_url'],
            'baseHref' => $this->settings['base_href'],
            'authenticationEnabled' => boolval($this->settings['token']['enabled']),
            'ssoAuthUrl' => $this->settings['sso']['auth_url'],
            'ssoRealm' => $this->settings['sso']['realm'],
            'ssoClientId' => $this->settings['sso']['client_id'],
            'adminRoles' => $this->settings['token']['admin_roles'],
            'matomoEnabled' => boolval($this->settings['matomo']['enabled']),
            'matomoSiteId' => intval($this->settings['matomo']['site_id']),
            'matomoTrackerUrl' => $this->settings['matomo']['tracker_url']
        ));
        $response->getBody()->write($payload);
        return $response;
    }
}
