<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Database;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatabaseAction extends AbstractAction
{
    /**
     * `GET` Returns the database found
     * `PUT` Full update the database and returns the new version
     * `DELETE` Delete the database found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct database with primary key
        $database = $this->em->find('App\Entity\Database', $args['id']);

        // If database is not found 404
        if (is_null($database)) {
            throw new HttpNotFoundException(
                $request,
                'Database with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($database);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('label', 'dbname', 'dbtype', 'dbhost', 'dbport', 'dblogin', 'dbpassword') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the database'
                    );
                }
            }

            $this->editDatabase($database, $parsedBody);
            $payload = json_encode($database);
        }

        if ($request->getMethod() === DELETE) {
            $id = $database->getId();
            $this->em->remove($database);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Database with id ' . $id . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update database object with setters
     *
     * @param Database $database   The database to update
     * @param array    $parsedBody Contains the new values ​​of the database sent by the user
     */
    private function editDatabase(Database $database, array $parsedBody): void
    {
        $database->setLabel($parsedBody['label']);
        $database->setDbName($parsedBody['dbname']);
        $database->setType($parsedBody['dbtype']);
        $database->setHost($parsedBody['dbhost']);
        $database->setPort($parsedBody['dbport']);
        $database->setLogin($parsedBody['dblogin']);
        $database->setPassword($parsedBody['dbpassword']);
        $this->em->flush();
    }
}
