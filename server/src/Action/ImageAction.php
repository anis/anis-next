<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Image;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class ImageAction extends AbstractAction
{
    /**
     * `GET` Returns the image found
     * `PUT` Full update the image and returns the new version
     * `DELETE` Delete the image found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct image with primary key
        $image = $this->em->find('App\Entity\Image', $args['id']);

        // If output family is not found 404
        if (is_null($image)) {
            throw new HttpNotFoundException(
                $request,
                'Image with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($image);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            $fields = array(
                'label',
                'file_path',
                'file_size',
                'ra_min',
                'ra_max',
                'dec_min',
                'dec_max',
                'stretch',
                'pmin',
                'pmax'
            );

            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the image'
                    );
                }
            }

            $this->editImage($image, $parsedBody);
            $payload = json_encode($image);
        }

        if ($request->getMethod() === DELETE) {
            $id = $image->getId();
            $this->em->remove($image);
            $this->em->flush();
            $payload = json_encode(array(
                'message' => 'Image with id ' . $id . ' is removed!'
            ));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update image object with setters
     *
     * @param Image      $image      The image to update
     * @param string[]   $parsedBody Contains the new values ​​of the image sent by the user
     */
    private function editImage(Image $image, array $parsedBody): void
    {
        $image->setLabel($parsedBody['label']);
        $image->setFilePath($parsedBody['file_path']);
        $image->setFileSize($parsedBody['file_size']);
        $image->setRaMin($parsedBody['ra_min']);
        $image->setRaMax($parsedBody['ra_max']);
        $image->setDecMin($parsedBody['dec_min']);
        $image->setDecMax($parsedBody['dec_max']);
        $image->setStretch($parsedBody['stretch']);
        $image->setPmin($parsedBody['pmin']);
        $image->setPmax($parsedBody['pmax']);
        $this->em->flush();
    }
}
