<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\OutputFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class OutputFamilyAction extends AbstractAction
{
    /**
     * `GET` Returns the output family found
     * `PUT` Full update the output family and returns the new version
     * `DELETE` Delete the output family found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct output family with primary key
        $outputFamily = $this->em->find('App\Entity\OutputFamily', $args['id']);

        // If output family is not found 404
        if (is_null($outputFamily)) {
            throw new HttpNotFoundException(
                $request,
                'Output family with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($outputFamily);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            $fields = array('label', 'display', 'opened');
            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the output family'
                    );
                }
            }

            $this->editOutputFamily($outputFamily, $parsedBody);
            $payload = json_encode($outputFamily);
        }

        if ($request->getMethod() === DELETE) {
            $id = $outputFamily->getId();
            $this->em->remove($outputFamily);
            $this->em->flush();
            $payload = json_encode(array(
                'message' => 'Output family with id ' . $id . ' is removed!'
            ));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update output family object with setters
     *
     * @param OutputFamily  $family     The output family to update
     * @param string[]      $parsedBody Contains the new values ​​of the output family sent by the user
     */
    private function editOutputFamily(OutputFamily $family, array $parsedBody): void
    {
        $family->setLabel($parsedBody['label']);
        $family->setDisplay($parsedBody['display']);
        $family->setOpened($parsedBody['opened']);
        $this->em->flush();
    }
}
