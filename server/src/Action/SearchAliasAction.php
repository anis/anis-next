<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;
use App\Search\DBALConnectionFactory;
use App\Entity\AliasConfig;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class SearchAliasAction extends AbstractAction
{
    /**
     * @var DBALConnectionFactory
     */
    private $connectionFactory;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em Doctrine       Entity Manager Interface
     * @param DBALConnectionFactory  $connectionFactory Factory used to construct connection to business database
     */
    public function __construct(EntityManagerInterface $em, DBALConnectionFactory $connectionFactory)
    {
        parent::__construct($em);
        $this->connectionFactory = $connectionFactory;
    }

    /**
     * `GET`  Returns a list of all alias found
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search dataset
        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        // Returns HTTP 400 if alias config is not found
        if (is_null($dataset->getAliasConfig())) {
            throw new HttpBadRequestException(
                $request,
                'Alias configuration for the dataaset' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $connection = $this->connectionFactory->create($dataset->getDatabase());
            $aliases = $this->getAliases($connection, $dataset->getAliasConfig(), $args['alias']);
            $payload = json_encode($aliases);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    private function getAliases(Connection $connection, AliasConfig $aliasConfig, string $alias)
    {
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder
            ->select(
                'aliases.' . $aliasConfig->getColumnAlias() . ' as alias',
                'aliases.' . $aliasConfig->getColumnName() . ' as name',
                'aliases.' . $aliasConfig->getColumnAliasLong() . ' as alias_long'
            )
            ->from($aliasConfig->getTableAlias(), 'aliases')
            ->where($aliasConfig->getColumnAlias() . ' LIKE ?')
            ->setParameter(0, '%' . $alias . '%');
        return $queryBuilder->executeQuery()->fetchAllAssociative();
    }
}
