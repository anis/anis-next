<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\WebpageFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class WebpageFamilyAction extends AbstractAction
{
    /**
     * `GET` Returns the webpage family found
     * `PUT` Full update the webpage family and returns the new version
     * `DELETE` Delete the webpage family found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct webpage family with primary key
        $webpageFamily = $this->em->find('App\Entity\WebpageFamily', $args['id']);

        // If webpage family is not found 404
        if (is_null($webpageFamily)) {
            throw new HttpNotFoundException(
                $request,
                'Webpage family with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($webpageFamily);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            $fields = array('label', 'icon', 'display');
            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the webpage family'
                    );
                }
            }

            $this->editWebpageFamily($webpageFamily, $parsedBody);
            $payload = json_encode($webpageFamily);
        }

        if ($request->getMethod() === DELETE) {
            $id = $webpageFamily->getId();
            $this->em->remove($webpageFamily);
            $this->em->flush();
            $payload = json_encode(array(
                'message' => 'Webpage family with id ' . $id . ' is removed!'
            ));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update webpage family object with setters
     *
     * @param WebpageFamily $family     The webpage family to update
     * @param array         $parsedBody Contains the new values ​​of the webpage family sent by the user
     */
    private function editWebpageFamily(WebpageFamily $family, array $parsedBody): void
    {
        $family->setLabel($parsedBody['label']);
        $family->setIcon($parsedBody['icon']);
        $family->setDisplay($parsedBody['display']);
        $this->em->flush();
    }
}
