<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Exception\HttpNotFoundException;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class WebpageListByInstanceAction extends AbstractAction
{
    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em Doctrine Entity Manager Interface
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * `GET`  Returns a list of all webpages for a given instance
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $instance = $this->em->find('App\Entity\Instance', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($instance)) {
            throw new HttpNotFoundException(
                $request,
                'Instance with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $qb = $this->em->createQueryBuilder();
            $qb->select('w')
                ->from('App\Entity\Webpage', 'w')
                ->join('w.webpageFamily', 'f')
                ->where($qb->expr()->eq('IDENTITY(f.instance)', ':instanceName'));

            $qb->setParameter('instanceName', $instance->getName());
            $webpages = $qb->getQuery()->getResult();
            $payload = json_encode($webpages);
        }

        $response->getBody()->write($payload);
        return $response;
    }
}
