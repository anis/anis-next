<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Dataset;
use App\Entity\DatasetFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatasetAction extends AbstractAction
{
    /**
     * `GET` Returns the dataset found
     * `PUT` Full update the dataset and returns the new version
     * `DELETE` Delete the dataset found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // If dataset is not found 404
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($dataset, JSON_UNESCAPED_SLASHES);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            $fields = array(
                'label',
                'description',
                'display',
                'data_path',
                'public',
                'download_json',
                'download_csv',
                'download_ascii',
                'download_vo',
                'download_fits',
                'server_link_enabled',
                'datatable_enabled',
                'datatable_selectable_rows',
                'id_dataset_family'
            );
            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the dataset'
                    );
                }
            }

            // Dataset family is mandatory to edit a dataset
            $idDatasetFamily = $parsedBody['id_dataset_family'];
            $family = $this->em->find('App\Entity\DatasetFamily', $idDatasetFamily);
            if (is_null($family)) {
                throw new HttpBadRequestException(
                    $request,
                    'Dataset family with id ' . $idDatasetFamily . ' is not found'
                );
            }

            $this->editDataset($dataset, $parsedBody, $family);
            $payload = json_encode($dataset, JSON_UNESCAPED_SLASHES);
        }

        if ($request->getMethod() === DELETE) {
            $name = $dataset->getName();
            $this->em->remove($dataset);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Dataset with name ' . $name . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update dataset object with setters
     *
     * @param Dataset       $dataset    The dataset to update
     * @param string[]      $parsedBody Contains the new values ​​of the dataset sent by the user
     * @param DatasetFamily $family     Contains the dataset family doctrine object
     */
    private function editDataset(Dataset $dataset, array $parsedBody, DatasetFamily $family): void
    {
        $dataset->setLabel($parsedBody['label']);
        $dataset->setDescription($parsedBody['description']);
        $dataset->setDatasetFamily($family);
        $dataset->setDisplay($parsedBody['display']);
        $dataset->setDataPath($parsedBody['data_path']);
        $dataset->setPublic($parsedBody['public']);
        $dataset->setDownloadJson($parsedBody['download_json']);
        $dataset->setDownloadCsv($parsedBody['download_csv']);
        $dataset->setDownloadAscii($parsedBody['download_ascii']);
        $dataset->setDownloadVo($parsedBody['download_vo']);
        $dataset->setDownloadFits($parsedBody['download_fits']);
        $dataset->setServerLinkEnabled($parsedBody['server_link_enabled']);
        $dataset->setDatatableEnabled($parsedBody['datatable_enabled']);
        $dataset->setDatatableSelectableRows($parsedBody['datatable_selectable_rows']);

        $this->em->flush();
    }
}
