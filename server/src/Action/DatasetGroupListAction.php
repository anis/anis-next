<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Instance;
use App\Entity\DatasetGroup;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatasetGroupListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all groups listed in the metamodel database
     * `POST` Add a new group
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $instance = $this->em->find('App\Entity\Instance', $args['name']);

        // Returns HTTP 404 if the instance is not found
        if (is_null($instance)) {
            throw new HttpNotFoundException(
                $request,
                'Instance with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            // Retrieve group with id
            $groups = $this->em->getRepository('App\Entity\DatasetGroup')->findBy(
                array('instance' => $instance),
                array('id' => 'ASC')
            );
            $payload = json_encode($groups);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs group information
            foreach (array('role', 'datasets') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new dataset group'
                    );
                }
            }

            $group = $this->postGroup($parsedBody, $instance);
            $payload = json_encode($group);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new group for an instance into the metamodel
     *
     * @param array    $parsedBody Contains the values ​​of the new group sent by the user
     * @param Instance $instance   Instance in which the group will be added
     *
     * @return Group The newly created group
     */
    private function postGroup(array $parsedBody, Instance $instance): DatasetGroup
    {
        $group = new DatasetGroup($instance);
        $group->setRole($parsedBody['role']);
        $group->setDatasets($this->getDatasets($parsedBody['datasets']));

        $this->em->persist($group);
        $this->em->flush();

        return $group;
    }

    /**
     * Retrieves list of datasets by list of datasets names
     *
     * @param string[] $listOfDatasetsNames List of datasets names
     *
     * @return Dataset[] List of datasets found
     */
    private function getDatasets(array $listOfDatasetsNames): array
    {
        if (count($listOfDatasetsNames) < 1) {
            return array();
        }

        $in = implode(',', array_map(function ($d) {
            return "'" . $d . "'";
        }, $listOfDatasetsNames));

        $dql = 'SELECT d FROM App\Entity\Dataset d WHERE d.name IN (' . $in . ')';
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
