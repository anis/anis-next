<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Dataset;
use App\Entity\Image;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class ImageListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all images for a given dataset
     * `POST` Add a new image to a given dataset
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $images = $this->em->getRepository('App\Entity\Image')->findBy(
                array('dataset' => $dataset),
                array('id' => 'ASC')
            );
            $payload = json_encode($images);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            $fields = array(
                'label',
                'file_path',
                'file_size',
                'ra_min',
                'ra_max',
                'dec_min',
                'dec_max',
                'stretch',
                'pmin',
                'pmax'
            );

            // To work this action needs information
            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new image'
                    );
                }
            }

            $image = $this->postImage($parsedBody, $dataset);
            $payload = json_encode($image);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param array    $parsedBody  Contains the values ​​of the new image sent by the user
     * @param Dataset  $dataset     Dataset for adding the image
     *
     * @return Image
     */
    private function postImage(array $parsedBody, Dataset $dataset): Image
    {
        $image = new Image($dataset);
        $image->setLabel($parsedBody['label']);
        $image->setFilePath($parsedBody['file_path']);
        $image->setFileSize($parsedBody['file_size']);
        $image->setRaMin($parsedBody['ra_min']);
        $image->setRaMax($parsedBody['ra_max']);
        $image->setDecMin($parsedBody['dec_min']);
        $image->setDecMax($parsedBody['dec_max']);
        $image->setStretch($parsedBody['stretch']);
        $image->setPmin($parsedBody['pmin']);
        $image->setPmax($parsedBody['pmax']);

        $this->em->persist($image);
        $this->em->flush();

        return $image;
    }
}
