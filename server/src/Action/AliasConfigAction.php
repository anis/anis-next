<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\AliasConfig;
use App\Entity\Dataset;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class AliasConfigAction extends AbstractAction
{
/**
     * `GET` Returns the alias configuration found
     * `PUT` Full update the alias configuration and returns the new version
     * `DELETE` Delete the alias configuration and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        }

        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        // Search the correct alias configuration with primary key
        if ($dataset->getAliasConfig()) {
            $aliasConfig = $this->em->find(
                'App\Entity\AliasConfig',
                $dataset->getAliasConfig()->getId()
            );
        } else {
            $aliasConfig = null;
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($aliasConfig);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();
            $this->checkParsedBody($parsedBody, $request);
            $aliasConfig = $this->postAliasConfig($parsedBody, $dataset);
            $payload = json_encode($aliasConfig);
            $response = $response->withStatus(201);
        }

        if ($request->getMethod() === PUT) {
            // Returns HTTP 404 if the alias configuation is not found
            if (is_null($aliasConfig)) {
                throw new HttpNotFoundException(
                    $request,
                    'Alias config is not found'
                );
            }

            $parsedBody = $request->getParsedBody();
            $this->checkParsedBody($parsedBody, $request);

            $this->editAliasConfig($aliasConfig, $parsedBody);
            $payload = json_encode($aliasConfig);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param array                  $parsedBody  Contains the values ​​of the new alias configuration
     * @param ServerRequestInterface $request     PSR-7 This object represents the HTTP request
     */
    private function checkParsedBody($parsedBody, $request)
    {
        $fields = array(
            'table_alias',
            'column_alias',
            'column_name',
            'column_alias_long'
        );

        // To work this actions needs information
        foreach ($fields as $a) {
            if (!array_key_exists($a, $parsedBody)) {
                throw new HttpBadRequestException(
                    $request,
                    'Param ' . $a . ' needed to add a edit alias configuration'
                );
            }
        }
    }

    /**
     * @param array   $parsedBody Contains the values ​​of the new alias configuration sent by the user
     * @param Dataset $dataset    Dataset for adding the alias configuration
     *
     * @return AliasConfig
     */
    private function postAliasConfig(array $parsedBody, Dataset $dataset): AliasConfig
    {
        $aliasConfig = new AliasConfig();
        $aliasConfig->setTableAlias($parsedBody['table_alias']);
        $aliasConfig->setColumnAlias($parsedBody['column_alias']);
        $aliasConfig->setColumnName($parsedBody['column_name']);
        $aliasConfig->setColumnAliasLong($parsedBody['column_alias_long']);

        $dataset->setAliasConfig($aliasConfig);

        $this->em->persist($dataset);
        $this->em->flush();

        return $aliasConfig;
    }

    /**
     * Update alias configuration object with setters
     *
     * @param AliasConfig  $aliasConfig  The alias configuration to update
     * @param string[]     $parsedBody   Contains the new values ​​of the alias configuration sent by the user
     */
    private function editAliasConfig(AliasConfig $aliasConfig, array $parsedBody): void
    {
        $aliasConfig->setTableAlias($parsedBody['table_alias']);
        $aliasConfig->setColumnAlias($parsedBody['column_alias']);
        $aliasConfig->setColumnName($parsedBody['column_name']);
        $aliasConfig->setColumnAliasLong($parsedBody['column_alias_long']);
        $this->em->flush();
    }
}
