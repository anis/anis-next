<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Entity\OutputCategory;
use App\Entity\OutputFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class OutputCategoryListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all output categories listed in the metamodel database
     * `POST` Add a new output category
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $outputFamily = $this->em->find('App\Entity\OutputFamily', $args['id']);

        // Returns HTTP 404 if the output family is not found
        if (is_null($outputFamily)) {
            throw new HttpNotFoundException(
                $request,
                'Output family with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $outputCategories = $this->em->getRepository('App\Entity\OutputCategory')->findBy(
                array('outputFamily' => $outputFamily),
                array('id' => 'ASC')
            );
            $payload = json_encode($outputCategories);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // Verif mandatories fields
            foreach (array('label', 'display') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new output category'
                    );
                }
            }

            $outputCategory = $this->postOutputCategory($parsedBody, $outputFamily);
            $payload = json_encode($outputCategory);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param array        $parsedBody    Contains the values ​​of the new output category sent by the user
     * @param OutputFamily $outputFamily  Output family for adding the output category
     *
     * @return OutputCategory
     */
    private function postOutputCategory(array $parsedBody, OutputFamily $outputFamily): OutputCategory
    {
        $outputCategory = new OutputCategory();
        $outputCategory->setLabel($parsedBody['label']);
        $outputCategory->setDisplay($parsedBody['display']);
        $outputCategory->setOutputFamily($outputFamily);

        $this->em->persist($outputCategory);
        $this->em->flush();

        return $outputCategory;
    }
}
