<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Connection;
use App\Search\DBALConnectionFactory;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class TableListAction extends AbstractAction
{
    /**
     * @var DBALConnectionFactory
     */
    private $connectionFactory;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em Doctrine       Entity Manager Interface
     * @param DBALConnectionFactory  $connectionFactory Factory used to construct connection to business database
     */
    public function __construct(EntityManagerInterface $em, DBALConnectionFactory $connectionFactory)
    {
        parent::__construct($em);
        $this->connectionFactory = $connectionFactory;
    }

    /**
     * `GET`  Returns a list of all tables and views available in the database
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct database with primary key
        $database = $this->em->find('App\Entity\Database', $args['id']);

        // If database is not found 404
        if (is_null($database)) {
            throw new HttpNotFoundException(
                $request,
                'Database with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $connection = $this->connectionFactory->create($database);
            $sm = $connection->createSchemaManager();
            $tables = array(...$this->getTables($sm), ...$this->getViews($sm, $connection));
            $payload = json_encode($tables);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param AbstractSchemaManager $sm Doctrine schema manager for the selected database
     *
     * @return string[]
     */
    private function getTables(AbstractSchemaManager $sm): array
    {
        $tables = array();
        foreach ($sm->listTables() as $table) {
            $tables[] = $table->getName();
        }
        return $tables;
    }

    /**
     * @param AbstractSchemaManager $sm Doctrine schema manager for the selected database
     *
     * @return string[]
     */
    private function getViews(AbstractSchemaManager $sm, Connection $connection): array
    {
        $views = array();
        if ($sm->getDatabasePlatform()->getName() === 'postgresql') {
            $result = $connection->fetchAllAssociative($this->getPgListViewsSQL());
            foreach ($result as $row) {
                $views[] = $row['viewname'];
            }
        } else {
            foreach ($sm->listViews() as $view) {
                $views[] = $view->getName();
            }
        }
        return $views;
    }

    /**
     * Returns the Postgres SQL query to list the available views
     *
     * @return string
     */
    private function getPgListViewsSQL(): string
    {
        return 'SELECT quote_ident(table_name) AS viewname,
                       table_schema AS schemaname,
                       view_definition AS definition
                FROM   information_schema.views
                WHERE  table_schema NOT IN (\'pg_catalog\',\'information_schema\')';
    }
}
