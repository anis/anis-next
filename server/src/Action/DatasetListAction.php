<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Database;
use App\Entity\DatasetFamily;
use App\Entity\Dataset;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatasetListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all datasets for a given dataset family
     * `POST` Add a new dataset
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $datasetFamily = $this->em->find('App\Entity\DatasetFamily', $args['id']);

        // Returns HTTP 404 if the dataset family is not found
        if (is_null($datasetFamily)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset family with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $datasets = $this->em->getRepository('App\Entity\Dataset')->findBy(
                array('datasetFamily' => $datasetFamily)
            );
            $payload = json_encode($datasets);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            $fields = array(
                'name',
                'table_ref',
                'label',
                'description',
                'display',
                'data_path',
                'public',
                'download_json',
                'download_csv',
                'download_ascii',
                'download_vo',
                'download_fits',
                'server_link_enabled',
                'datatable_enabled',
                'datatable_selectable_rows',
                'id_database'
            );
            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new dataset'
                    );
                }
            }

            // Database is mandatory to add a new dataset
            $idDatabase = $parsedBody['id_database'];
            $database = $this->em->find('App\Entity\Database', $idDatabase);
            if (is_null($database)) {
                throw new HttpBadRequestException(
                    $request,
                    'Database with id ' . $idDatabase . ' is not found'
                );
            }

            $dataset = $this->postDataset($parsedBody, $database, $datasetFamily);
            $payload = json_encode($dataset);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new dataset into the metamodel
     *
     * @param array         $parsedBody    Contains the values ​​of the new dataset sent by the user
     * @param Database      $database      Contains the database doctrine object
     * @param DatasetFamily $datasetFamily Contains the dataset family doctrine object
     *
     * @return Dataset
     */
    private function postDataset(
        array $parsedBody,
        Database $database,
        DatasetFamily $datasetFamily
    ): Dataset {
        $dataset = new Dataset($parsedBody['name']);
        $dataset->setTableRef($parsedBody['table_ref']);
        $dataset->setLabel($parsedBody['label']);
        $dataset->setDescription($parsedBody['description']);
        $dataset->setDisplay($parsedBody['display']);
        $dataset->setDataPath($parsedBody['data_path']);
        $dataset->setPublic($parsedBody['public']);
        $dataset->setDownloadJson($parsedBody['download_json']);
        $dataset->setDownloadCsv($parsedBody['download_csv']);
        $dataset->setDownloadAscii($parsedBody['download_ascii']);
        $dataset->setDownloadVo($parsedBody['download_vo']);
        $dataset->setDownloadFits($parsedBody['download_fits']);
        $dataset->setServerLinkEnabled($parsedBody['server_link_enabled']);
        $dataset->setDatatableEnabled($parsedBody['datatable_enabled']);
        $dataset->setDatatableSelectableRows($parsedBody['datatable_selectable_rows']);
        $dataset->setDatabase($database);
        $dataset->setDatasetFamily($datasetFamily);

        $this->em->persist($dataset);
        $this->em->flush();

        return $dataset;
    }
}
