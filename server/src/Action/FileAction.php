<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\File;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class FileAction extends AbstractAction
{
    /**
     * `GET` Returns the file found
     * `PUT` Full update the file and returns the new version
     * `DELETE` Delete the file found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct file with primary key
        $file = $this->em->find('App\Entity\File', $args['id']);

        // If file is not found 404
        if (is_null($file)) {
            throw new HttpNotFoundException(
                $request,
                'File with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($file);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            $fields = array(
                'label',
                'file_path',
                'file_size',
                'type'
            );

            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the file'
                    );
                }
            }

            $this->editFile($file, $parsedBody);
            $payload = json_encode($file);
        }

        if ($request->getMethod() === DELETE) {
            $id = $file->getId();
            $this->em->remove($file);
            $this->em->flush();
            $payload = json_encode(array(
                'message' => 'File with id ' . $id . ' is removed!'
            ));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update file object with setters
     *
     * @param File     $file       The file to update
     * @param string[] $parsedBody Contains the new values ​​of the file sent by the user
     */
    private function editFile(File $file, array $parsedBody): void
    {
        $file->setLabel($parsedBody['label']);
        $file->setFilePath($parsedBody['file_path']);
        $file->setFileSize($parsedBody['file_size']);
        $file->setType($parsedBody['type']);
        $this->em->flush();
    }
}
