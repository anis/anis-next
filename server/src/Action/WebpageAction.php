<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Webpage;
use App\Entity\WebpageFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class WebpageAction extends AbstractAction
{
    /**
     * `GET` Returns the webpage found
     * `PUT` Full update the webpage and returns the new version
     * `DELETE` Delete the webpage found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct webpage with primary key
        $webpage = $this->em->find('App\Entity\Webpage', $args['id']);

        // If webpage is not found 404
        if (is_null($webpage)) {
            throw new HttpNotFoundException(
                $request,
                'Webpage with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($webpage);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            $fields = array();
            if ($parsedBody['type'] === 'webpage') {
                $fields = array('name', 'label', 'icon', 'display', 'title', 'content', 'style_sheet', 'type');
            } else {
                $fields = array('name', 'label', 'icon', 'display', 'style_sheet', 'type', 'url');
            }

            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the webpage'
                    );
                }
            }

            // Webpage family is mandatory to edit a webpage
            $idWebpageFamily = $parsedBody['id_webpage_family'];
            $family = $this->em->find('App\Entity\WebpageFamily', $idWebpageFamily);
            if (is_null($family)) {
                throw new HttpBadRequestException(
                    $request,
                    'Webpage family with id ' . $idWebpageFamily . ' is not found'
                );
            }

            $this->editWebpage($webpage, $parsedBody, $family);
            $payload = json_encode($webpage);
        }

        if ($request->getMethod() === DELETE) {
            $id = $webpage->getId();
            $this->em->remove($webpage);
            $this->em->flush();
            $payload = json_encode(array(
                'message' => 'Webpage with id ' . $id . ' is removed!'
            ));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update webpage object with setters
     *
     * @param Webpage $webpage    The webpage to update
     * @param array   $parsedBody Contains the new values ​​of the webpage sent by the user
     */
    private function editWebpage(Webpage $webpage, array $parsedBody, WebpageFamily $webpageFamily): void
    {
        $webpage->setName($parsedBody['name']);
        $webpage->setLabel($parsedBody['label']);
        $webpage->setIcon($parsedBody['icon']);
        $webpage->setDisplay($parsedBody['display']);
        if ($parsedBody['type'] === 'webpage') {
            $webpage->setTitle($parsedBody['title']);
            $webpage->setContent($parsedBody['content']);
            $webpage->setStyleSheet($parsedBody['style_sheet']);
            $webpage->setUrl(null);
        } else {
            $webpage->setUrl($parsedBody['url']);
            $webpage->setTitle(null);
            $webpage->setContent(null);
            $webpage->setStyleSheet(null);
        }
        $webpage->setType($parsedBody['type']);

        $webpage->setWebpageFamily($webpageFamily);
        $this->em->flush();
    }
}
