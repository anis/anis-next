<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\DetailConfig;
use App\Entity\Dataset;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DetailConfigAction extends AbstractAction
{
    /**
     * `GET` Returns the detail configuration found
     * `PUT` Full update the detail configuration and returns the new version
     * `DELETE` Delete the detail configuration and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        }

        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        // Search the correct detail configuration with primary key
        if ($dataset->getDetailConfig()) {
            $detailConfig = $this->em->find(
                'App\Entity\DetailConfig',
                $dataset->getDetailConfig()->getId()
            );
        } else {
            $detailConfig = null;
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($detailConfig);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();
            $this->checkParsedBody($parsedBody, $request);
            $detailConfig = $this->postDetailConfig($parsedBody, $dataset);
            $payload = json_encode($detailConfig);
            $response = $response->withStatus(201);
        }

        if ($request->getMethod() === PUT) {
            // Returns HTTP 404 if the detail configuation is not found
            if (is_null($detailConfig)) {
                throw new HttpNotFoundException(
                    $request,
                    'Detail config is not found'
                );
            }

            $parsedBody = $request->getParsedBody();
            $this->checkParsedBody($parsedBody, $request);

            $this->editDetailConfig($detailConfig, $parsedBody);
            $payload = json_encode($detailConfig);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param array                  $parsedBody  Contains the values ​​of the new detail configuration
     * @param ServerRequestInterface $request     PSR-7 This object represents the HTTP request
     */
    private function checkParsedBody($parsedBody, $request)
    {
        $fields = array(
            'content',
            'style_sheet'
        );

        // To work this actions needs information
        foreach ($fields as $a) {
            if (!array_key_exists($a, $parsedBody)) {
                throw new HttpBadRequestException(
                    $request,
                    'Param ' . $a . ' needed to add or edit detail configuration'
                );
            }
        }
    }

    /**
     * @param array   $parsedBody Contains the values ​​of the new detail configuration sent by the user
     * @param Dataset $dataset    Dataset for adding the detail configuration
     *
     * @return DetailConfig
     */
    private function postDetailConfig(array $parsedBody, Dataset $dataset): DetailConfig
    {
        $detailConfig = new DetailConfig();
        $detailConfig->setContent($parsedBody['content']);
        $detailConfig->setStyleSheet($parsedBody['style_sheet']);

        $dataset->setDetailConfig($detailConfig);

        $this->em->persist($dataset);
        $this->em->flush();

        return $detailConfig;
    }

    /**
     * Update detail configuration object with setters
     *
     * @param DetailConfig $detailConfig The detail configuration to update
     * @param string[]     $parsedBody   Contains the new values ​​of the detail sent by the user
     */
    private function editDetailConfig(detailConfig $detailConfig, array $parsedBody): void
    {
        $detailConfig->setContent($parsedBody['content']);
        $detailConfig->setStyleSheet($parsedBody['style_sheet']);
        $this->em->flush();
    }
}
