<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Attribute;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class AttributeAction extends AbstractAction
{
    /**
     * `GET` Returns the attribute found
     * `PUT` Full update the attribute and returns the new version
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        $attribute = $this->em->getRepository('App\Entity\Attribute')->findOneBy(
            array('dataset' => $args['name'], 'id' => $args['id'])
        );

        // If attribute is not found 404
        if (is_null($attribute)) {
            throw new HttpNotFoundException(
                $request,
                'Attribute with dataset name ' . $args['name'] . ' and attribute id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($attribute);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            $fields = array(
                'id',
                'name',
                'label',
                'form_label',
                'description',
                'primary_key',
                'type',
                'search_type',
                'operator',
                'dynamic_operator',
                'min',
                'max',
                'options',
                'placeholder_min',
                'placeholder_max',
                'criteria_display',
                'output_display',
                'selected',
                'renderer',
                'renderer_config',
                'order_by',
                'archive',
                'detail_display',
                'detail_renderer',
                'detail_renderer_config',
                'vo_utype',
                'vo_ucd',
                'vo_unit',
                'vo_description',
                'vo_datatype',
                'vo_size',
                'id_criteria_family',
                'id_output_category',
                'id_detail_output_category'
            );
            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the attribute'
                    );
                }
            }

            $this->editAttribute($attribute, $parsedBody);
            $payload = json_encode($attribute);
        }

        if ($request->getMethod() === DELETE) {
            $id = $attribute->getId();
            $this->em->remove($attribute);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Attribute with id ' . $id . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update attribute object with setters
     *
     * @param Attribute $attribute  The attribute to update
     * @param array     $parsedBody Contains the new values ​​of the attribute sent by the user
     */
    private function editAttribute(Attribute $attribute, array $parsedBody): void
    {
        $attribute->setId($parsedBody['id']);
        $attribute->setName($parsedBody['name']);
        $attribute->setLabel($parsedBody['label']);
        $attribute->setFormLabel($parsedBody['form_label']);
        $attribute->setDescription($parsedBody['description']);
        $attribute->setPrimaryKey($parsedBody['primary_key']);
        $attribute->setType($parsedBody['type']);
        $attribute->setSearchType($parsedBody['search_type']);
        $attribute->setOperator($parsedBody['operator']);
        $attribute->setDynamicOperator($parsedBody['dynamic_operator']);
        $attribute->setMin($parsedBody['min']);
        $attribute->setMax($parsedBody['max']);
        $attribute->setOptions($parsedBody['options']);
        $attribute->setPlaceholderMin($parsedBody['placeholder_min']);
        $attribute->setPlaceholderMax($parsedBody['placeholder_max']);
        $attribute->setCriteriaDisplay($parsedBody['criteria_display']);
        $attribute->setOutputDisplay($parsedBody['output_display']);
        $attribute->setSelected($parsedBody['selected']);
        $attribute->setRenderer($parsedBody['renderer']);
        $attribute->setRendererConfig($parsedBody['renderer_config']);
        $attribute->setOrderBy($parsedBody['order_by']);
        $attribute->setArchive($parsedBody['archive']);
        $attribute->setDetailDisplay($parsedBody['detail_display']);
        $attribute->setDetailRenderer($parsedBody['detail_renderer']);
        $attribute->setDetailRendererConfig($parsedBody['detail_renderer_config']);
        $attribute->setVoUtype($parsedBody['vo_utype']);
        $attribute->setVoUcd($parsedBody['vo_ucd']);
        $attribute->setVoUnit($parsedBody['vo_unit']);
        $attribute->setVoDescription($parsedBody['vo_description']);
        $attribute->setVoDatatype($parsedBody['vo_datatype']);
        $attribute->setVoSize($parsedBody['vo_size']);
        if (is_null($parsedBody['id_criteria_family'])) {
            $criteriaFamily = null;
        } else {
            $criteriaFamily = $this->em->find(
                'App\Entity\CriteriaFamily',
                $parsedBody['id_criteria_family']
            );
        }
        $attribute->setCriteriaFamily($criteriaFamily);
        if (is_null($parsedBody['id_output_category'])) {
            $outputCategory = null;
        } else {
            $outputCategory = $this->em->find(
                'App\Entity\OutputCategory',
                $parsedBody['id_output_category']
            );
        }
        $attribute->setOutputCategory($outputCategory);
        if (is_null($parsedBody['id_detail_output_category'])) {
            $detailOutputCategory = null;
        } else {
            $detailOutputCategory = $this->em->find(
                'App\Entity\OutputCategory',
                $parsedBody['id_detail_output_category']
            );
        }
        $attribute->setDetailOutputCategory($detailOutputCategory);
        $this->em->flush();
    }
}
