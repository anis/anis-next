<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Dataset;
use App\Entity\CriteriaFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class CriteriaFamilyListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all criteria family for a given dataset
     * `POST` Add a new dataset criteria family to a given dataset
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $families = $this->em->getRepository('App\Entity\CriteriaFamily')->findBy(
                array('dataset' => $dataset),
                array('id' => 'ASC')
            );
            $payload = json_encode($families);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs information
            foreach (array('label', 'display', 'opened') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new criteria family'
                    );
                }
            }

            $family = $this->postCriteriaFamily($parsedBody, $dataset);
            $payload = json_encode($family);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new criteria family into the metamodel
     *
     * @param array   $parsedBody Contains the values ​​of the new criteria family sent by the user
     * @param Dataset $dataset    The dataset for adding the family
     *
     * @return CriteriaFamily
     */
    private function postCriteriaFamily(array $parsedBody, Dataset $dataset): CriteriaFamily
    {
        $family = new CriteriaFamily($dataset);
        $family->setLabel($parsedBody['label']);
        $family->setDisplay($parsedBody['display']);
        $family->setOpened($parsedBody['opened']);

        $this->em->persist($family);
        $this->em->flush();

        return $family;
    }
}
