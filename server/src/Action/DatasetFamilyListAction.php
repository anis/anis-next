<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Instance;
use App\Entity\DatasetFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatasetFamilyListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all dataset family for a given instance
     * `POST` Add a new dataset family to a given instance
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $instance = $this->em->find('App\Entity\Instance', $args['name']);

        // Returns HTTP 404 if the instance is not found
        if (is_null($instance)) {
            throw new HttpNotFoundException(
                $request,
                'Instance with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $families = $this->em->getRepository('App\Entity\DatasetFamily')->findBy(array('instance' => $instance));
            $payload = json_encode($families);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs information
            foreach (array('label', 'display', 'opened') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new dataset family'
                    );
                }
            }

            $family = $this->postDatasetFamily($parsedBody, $instance);
            $payload = json_encode($family);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new instance into the metamodel
     *
     * @param array    $parsedBody Contains the values ​​of the new dataset family sent by the user
     * @param Instance $instance   The instance for adding the dataset family
     *
     * @return DatasetFamily
     */
    private function postDatasetFamily(array $parsedBody, Instance $instance): DatasetFamily
    {
        $family = new DatasetFamily($instance);
        $family->setLabel($parsedBody['label']);
        $family->setDisplay($parsedBody['display']);
        $family->setOpened($parsedBody['opened']);

        $this->em->persist($family);
        $this->em->flush();

        return $family;
    }
}
