<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\InstanceGroup;
use App\Entity\Instance;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class InstanceGroupAction extends AbstractAction
{
    /**
     * `GET` Returns the instance group found
     * `PUT` Full update the instance group and returns the new version
     * `DELETE` Delete the instance group found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct instance group with primary key
        $instanceGroup = $this->em->find('App\Entity\InstanceGroup', $args['id']);

        // If group is not found 404
        if (is_null($instanceGroup)) {
            throw new HttpNotFoundException(
                $request,
                'Instance-group with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($instanceGroup);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('role', 'instances') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the instance-group'
                    );
                }
            }

            $this->editInstanceGroup($instanceGroup, $parsedBody);
            $payload = json_encode($instanceGroup);
        }

        if ($request->getMethod() === DELETE) {
            $id = $instanceGroup->getId();
            $this->em->remove($instanceGroup);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Instance-group with id ' . $id . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update instance group object with setters
     *
     * @param InstanceGroup $instanceGroup The instance-group to update
     * @param array         $parsedBody    Contains the new values ​​of the instance-group sent by the user
     */
    private function editInstanceGroup(InstanceGroup $instanceGroup, array $parsedBody): void
    {
        $instanceGroup->setRole($parsedBody['role']);
        $instanceGroup->setInstances($this->getInstances($parsedBody['instances']));
        $this->em->flush();
    }

    /**
     * Retrieves list of instances by list of instances names
     *
     * @param string[] $listOfInstancesNames List of instances names
     *
     * @return Instance[] List of instances found
     */
    private function getInstances(array $listOfInstancesNames): array
    {
        if (count($listOfInstancesNames) < 1) {
            return array();
        }

        $in = implode(',', array_map(function ($d) {
            return "'" . $d . "'";
        }, $listOfInstancesNames));

        $dql = 'SELECT i FROM App\Entity\Instance i WHERE i.name IN (' . $in . ')';
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
