<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class IsArchiveAvailableAction extends AbstractAction
{
    /**
     * Contains ANIS data path value
     *
     * @var string
     */
    private $dataPath;

    /**
     * Contains archive folder path
     *
     * @var string
     */
    private $archiveFolder;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em            Doctrine Entity Manager Interface
     * @param string                 $dataPath      Contains anis-server data path
     * @param string                 $archiveFolder Contains archive folder path
     */
    public function __construct(EntityManagerInterface $em, string $dataPath, string $archiveFolder)
    {
        parent::__construct($em);
        $this->dataPath = $dataPath;
        $this->archiveFolder = $archiveFolder;
    }

    /**
     * `GET` Returns the file found
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $archiveId = $args['id'];

        // Search the file
        $filePath = $this->dataPath . $this->archiveFolder . '/' . $archiveId . '.zip';

        $isAvailable = false;
        if (file_exists($filePath)) {
            $isAvailable = true;
        }

        $payload = json_encode(array('archive_is_available' => $isAvailable));
        $response->getBody()->write($payload);
        return $response;
    }
}
