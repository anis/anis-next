<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Database;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatabaseListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all databases listed in the metamodel database
     * `POST` Add a new database
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        if ($request->getMethod() === GET) {
            $databases = $this->em->getRepository('App\Entity\Database')->findBy(
                array(),
                array('id' => 'ASC')
            );
            $payload = json_encode($databases);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs user information to update
            foreach (array('label', 'dbname', 'dbtype', 'dbhost', 'dbport', 'dblogin', 'dbpassword') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new database'
                    );
                }
            }

            $database = $this->postDatabase($parsedBody);
            $payload = json_encode($database);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new database into the metamodel
     *
     * @param array $parsedBody Contains the values ​​of the new database sent by the user
     */
    private function postDatabase(array $parsedBody): Database
    {
        $database = new Database();
        $database->setLabel($parsedBody['label']);
        $database->setDbName($parsedBody['dbname']);
        $database->setType($parsedBody['dbtype']);
        $database->setHost($parsedBody['dbhost']);
        $database->setPort($parsedBody['dbport']);
        $database->setLogin($parsedBody['dblogin']);
        $database->setPassword($parsedBody['dbpassword']);

        $this->em->persist($database);
        $this->em->flush();

        return $database;
    }
}
