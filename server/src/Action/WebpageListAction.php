<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\WebpageFamily;
use App\Entity\Webpage;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class WebpageListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all webpages for a given webpage family
     * `POST` Add a new webpage
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $webpageFamily = $this->em->find('App\Entity\WebpageFamily', $args['id']);

        // Returns HTTP 404 if the webpage family is not found
        if (is_null($webpageFamily)) {
            throw new HttpNotFoundException(
                $request,
                'Webpage family with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $webpages = $this->em->getRepository('App\Entity\Webpage')->findBy(
                array('webpageFamily' => $webpageFamily)
            );
            $payload = json_encode($webpages);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs information
            $fields = array();
            if ($parsedBody['type'] === 'webpage') {
                $fields = array('name', 'label', 'icon', 'display', 'title', 'content', 'style_sheet', 'type');
            } else {
                $fields = array('name', 'label', 'icon', 'display', 'style_sheet', 'type', 'url');
            }

            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new webpage'
                    );
                }
            }

            $webpage = $this->postWebpage($parsedBody, $webpageFamily);
            $payload = json_encode($webpage);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new webpage into the metamodel
     *
     * @param array         $parsedBody    Contains the values ​​of the new webpage sent by the user
     * @param WebpageFamily $webpageFamily The wbepage family for adding the webpage
     *
     * @return Webpage
     */
    private function postWebpage(array $parsedBody, WebpageFamily $webpageFamily): Webpage
    {
        $webpage = new Webpage();
        $webpage->setName($parsedBody['name']);
        $webpage->setLabel($parsedBody['label']);
        $webpage->setIcon($parsedBody['icon']);
        $webpage->setDisplay($parsedBody['display']);
        if ($parsedBody['type'] === 'webpage') {
            $webpage->setTitle($parsedBody['title']);
            $webpage->setContent($parsedBody['content']);
            $webpage->setStyleSheet($parsedBody['style_sheet']);
            $webpage->setUrl(null);
        } else {
            $webpage->setUrl($parsedBody['url']);
            $webpage->setTitle(null);
            $webpage->setContent(null);
            $webpage->setStyleSheet(null);
        }
        $webpage->setType($parsedBody['type']);
        $webpage->setWebpageFamily($webpageFamily);
        $this->em->persist($webpage);
        $this->em->flush();

        return $webpage;
    }
}
