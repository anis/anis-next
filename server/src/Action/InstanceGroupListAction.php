<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use App\Entity\InstanceGroup;
use App\Entity\Instance;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class InstanceGroupListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all instance groups listed in the metamodel database
     * `POST` Add a new instance group
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        if ($request->getMethod() === GET) {
            $instanceGroups = $this->em->getRepository('App\Entity\InstanceGroup')->findAll();
            $payload = json_encode($instanceGroups);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs instance-group information
            foreach (array('role', 'instances') as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new instance-group'
                    );
                }
            }

            $instanceGroup = $this->postInstanceGroup($parsedBody);
            $payload = json_encode($instanceGroup);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new instance group into the metamodel
     *
     * @param array $parsedBody Contains the values ​​of the new instance-group sent by the user
     *
     * @return InstanceGroup The newly created instance group
     */
    private function postInstanceGroup(array $parsedBody): InstanceGroup
    {
        $instanceGroup = new InstanceGroup();
        $instanceGroup->setRole($parsedBody['role']);
        $instanceGroup->setInstances($this->getInstances($parsedBody['instances']));

        $this->em->persist($instanceGroup);
        $this->em->flush();

        return $instanceGroup;
    }

    /**
     * Retrieves list of instances by list of instances names
     *
     * @param string[] $listOfInstancesNames List of instances names
     *
     * @return Instance[] List of instances found
     */
    private function getInstances(array $listOfInstancesNames): array
    {
        if (count($listOfInstancesNames) < 1) {
            return array();
        }

        $in = implode(',', array_map(function ($d) {
            return "'" . $d . "'";
        }, $listOfInstancesNames));

        $dql = 'SELECT i FROM App\Entity\Instance i WHERE i.name IN (' . $in . ')';
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
