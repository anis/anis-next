<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\ConeSearchConfig;
use App\Entity\Dataset;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class ConeSearchConfigAction extends AbstractAction
{
    /**
     * `GET` Returns the cone-search configuration found
     * `PUT` Full update the cone-search configuration and returns the new version
     * `DELETE` Delete the cone-search configuration and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        }

        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        // Search the correct cone-search configuration with primary key
        if ($dataset->getConeSearchConfig()) {
            $coneSearchConfig = $this->em->find(
                'App\Entity\ConeSearchConfig',
                $dataset->getConeSearchConfig()->getId()
            );
        } else {
            $coneSearchConfig = null;
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($coneSearchConfig);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();
            $this->checkParsedBody($parsedBody, $request);
            $coneSearchConfig = $this->postConeSearchConfig($parsedBody, $dataset);
            $payload = json_encode($coneSearchConfig);
            $response = $response->withStatus(201);
        }

        if ($request->getMethod() === PUT) {
            // Returns HTTP 404 if the cone-search configuation is not found
            if (is_null($coneSearchConfig)) {
                throw new HttpNotFoundException(
                    $request,
                    'Cone search config is not found'
                );
            }

            $parsedBody = $request->getParsedBody();
            $this->checkParsedBody($parsedBody, $request);

            $this->editConeSearchConfig($coneSearchConfig, $parsedBody);
            $payload = json_encode($coneSearchConfig);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param array                  $parsedBody  Contains the values ​​of the new cone-search configuration
     * @param ServerRequestInterface $request     PSR-7 This object represents the HTTP request
     */
    private function checkParsedBody($parsedBody, $request)
    {
        $fields = array(
            'enabled',
            'opened',
            'column_ra',
            'column_dec',
            'resolver_enabled',
            'default_ra',
            'default_dec',
            'default_radius',
            'default_ra_dec_unit',
            'plot_enabled'
        );

        // To work this actions needs information
        foreach ($fields as $a) {
            if (!array_key_exists($a, $parsedBody)) {
                throw new HttpBadRequestException(
                    $request,
                    'Param ' . $a . ' needed to add a edit cone-search configuration'
                );
            }
        }
    }

    /**
     * @param array   $parsedBody Contains the values ​​of the new cone-search configuration sent by the user
     * @param Dataset $dataset    Dataset for adding the cone-search configuration
     *
     * @return ConeSearchConfig
     */
    private function postConeSearchConfig(array $parsedBody, Dataset $dataset): ConeSearchConfig
    {
        $coneSearchConfig = new ConeSearchConfig();
        $coneSearchConfig->setEnabled($parsedBody['enabled']);
        $coneSearchConfig->setOpened($parsedBody['opened']);
        $coneSearchConfig->setColumnRa($parsedBody['column_ra']);
        $coneSearchConfig->setColumnDec($parsedBody['column_dec']);
        $coneSearchConfig->setResolverEnabled($parsedBody['resolver_enabled']);
        $coneSearchConfig->setDefaultRa($parsedBody['default_ra']);
        $coneSearchConfig->setDefaultDec($parsedBody['default_dec']);
        $coneSearchConfig->setDefaultRadius($parsedBody['default_radius']);
        $coneSearchConfig->setDefaultRaDecUnit($parsedBody['default_ra_dec_unit']);
        $coneSearchConfig->setPlotEnabled($parsedBody['plot_enabled']);

        $dataset->setConeSearchConfig($coneSearchConfig);

        $this->em->persist($dataset);
        $this->em->flush();

        return $coneSearchConfig;
    }

    /**
     * Update cone-search configuration object with setters
     *
     * @param ConeSearchConfig $coneSearchConfig The cone-search configuration to update
     * @param string[]         $parsedBody       Contains the new values ​​of the cone-search sent by the user
     */
    private function editConeSearchConfig(coneSearchConfig $coneSearchConfig, array $parsedBody): void
    {
        $coneSearchConfig->setEnabled($parsedBody['enabled']);
        $coneSearchConfig->setOpened($parsedBody['opened']);
        $coneSearchConfig->setColumnRa($parsedBody['column_ra']);
        $coneSearchConfig->setColumnDec($parsedBody['column_dec']);
        $coneSearchConfig->setResolverEnabled($parsedBody['resolver_enabled']);
        $coneSearchConfig->setDefaultRa($parsedBody['default_ra']);
        $coneSearchConfig->setDefaultDec($parsedBody['default_dec']);
        $coneSearchConfig->setDefaultRadius($parsedBody['default_radius']);
        $coneSearchConfig->setDefaultRaDecUnit($parsedBody['default_ra_dec_unit']);
        $coneSearchConfig->setPlotEnabled($parsedBody['plot_enabled']);
        $this->em->flush();
    }
}
