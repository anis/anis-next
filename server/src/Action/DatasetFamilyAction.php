<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\DatasetFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatasetFamilyAction extends AbstractAction
{
    /**
     * `GET` Returns the dataset family found
     * `PUT` Full update the dataset family and returns the new version
     * `DELETE` Delete the dataset family found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct dataset family with primary key
        $datasetFamily = $this->em->find('App\Entity\DatasetFamily', $args['id']);

        // If dataset family is not found 404
        if (is_null($datasetFamily)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset family with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($datasetFamily);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            $fields = array('label', 'display', 'opened');
            foreach ($fields as $a) {
                if (!array_key_exists($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the dataset family'
                    );
                }
            }

            $this->editDatasetFamily($datasetFamily, $parsedBody);
            $payload = json_encode($datasetFamily);
        }

        if ($request->getMethod() === DELETE) {
            $id = $datasetFamily->getId();
            $this->em->remove($datasetFamily);
            $this->em->flush();
            $payload = json_encode(array(
                'message' => 'Dataset family with id ' . $id . ' is removed!'
            ));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update dataset family object with setters
     *
     * @param DatasetFamily $family     The dataset family to update
     * @param array         $parsedBody Contains the new values ​​of the dataset family sent by the user
     */
    private function editDatasetFamily(DatasetFamily $family, array $parsedBody): void
    {
        $family->setLabel($parsedBody['label']);
        $family->setDisplay($parsedBody['display']);
        $family->setOpened($parsedBody['opened']);
        $this->em->flush();
    }
}
