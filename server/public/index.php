<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use DI\Container;
use Slim\Factory\AppFactory;
use App\Handlers\LogErrorHandler;

// Autoloading for libraries
require __DIR__ . '/../vendor/autoload.php';

// Load app constants
require __DIR__ . '/../app/constants.php';

// Create Container using PHP-DI
$container = new Container();

// Setup dependencies
require __DIR__ . '/../app/dependencies.php';

// Set container to create App with on AppFactory
AppFactory::setContainer($container);
$app = AppFactory::create();

// Parse json, form data and xml
$app->addBodyParsingMiddleware();

$ded = $container->get(SETTINGS)['displayErrorDetails'];
if (is_string($ded)) {
    $displayErrorDetails = $ded === 'true';
} else {
    $displayErrorDetails = $ded;
}

$errorHandler = new LogErrorHandler($app->getCallableResolver(), $app->getResponseFactory());
$errorHandler->setLogger($container->get('logger'));

// Add Error Handling Middleware (JSON only)
$errorMiddleware = $app->addErrorMiddleware(
    $displayErrorDetails,
    true,
    true
);
$errorMiddleware->setDefaultErrorHandler($errorHandler);

// Register middlewares
require __DIR__ . '/../app/middlewares.php';

// Register routes
require __DIR__ . '/../app/routes.php';

// Run app
$app->run();
