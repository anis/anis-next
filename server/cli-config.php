<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use DI\Container;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// Autoloading for libraries
require __DIR__ . '/vendor/autoload.php';

// Load app constants
require __DIR__ . '/app/constants.php';

// Create Container using PHP-DI
$container = new Container();

// Setup dependencies
require __DIR__ . '/app/dependencies.php';

return ConsoleRunner::createHelperSet($container->get('em'));