<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

return [
    'displayErrorDetails' => getenv('DISPLAY_ERROR_DETAILS'),
    'database' => [
        'path_proxy' => getenv('DATABASE_PATH_PROXY'),
        'dev_mode' => getenv('DATABASE_DEV_MODE'),
        'connection_options' => [
            'driver' => getenv('DATABASE_CO_DRIVER'),
            'path' => getenv('DATABASE_CO_PATH'),
            'host' => getenv('DATABASE_CO_HOST'),
            'port' => (int) getenv('DATABASE_CO_PORT'),
            'dbname' => getenv('DATABASE_CO_DBNAME'),
            'user' => getenv('DATABASE_CO_USER'),
            'password' => getenv('DATABASE_CO_PASSWORD')
        ],
    ],
    'data_path' => getenv('DATA_PATH'),
    'archive_folder' => getenv('ARCHIVE_FOLDER'),
    'logger' => [
        'name' => getenv('LOGGER_NAME'),
        'path' => getenv('LOGGER_PATH'),
        'level' => getenv('LOGGER_LEVEL')
    ],
    'services_url' => getenv('SERVICES_URL'),
    'base_href' => getenv('BASE_HREF'),
    'sso' => [
        'auth_url' => getenv('SSO_AUTH_URL'),
        'realm' => getenv('SSO_REALM'),
        'client_id' => getenv('SSO_CLIENT_ID')
    ],
    'token' => [
        'enabled' => getenv('TOKEN_ENABLED'),
        'jwks_url' => getenv('TOKEN_JWKS_URL'),
        'admin_roles' => getenv('TOKEN_ADMIN_ROLES')
    ],
    'rmq' => [
        'host' => getenv('RMQ_HOST'),
        'port' => getenv('RMQ_PORT'),
        'user' => getenv('RMQ_USER'),
        'password' => getenv('RMQ_PASSWORD')
    ],
    'matomo' => [
        'enabled' => getenv('MATOMO_ENABLED'),
        'site_id' => getenv('MATOMO_SITE_ID'),
        'tracker_url' => getenv('MATOMO_TRACKER_URL')
    ]
];
