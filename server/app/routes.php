<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use Slim\Routing\RouteCollectorProxy;

$app->get('/', App\Action\RootAction::class);
$app->get('/client-settings', App\Action\ClientSettingsAction::class);

// Metamodel actions (ANIS admin only)
$app->group('', function (RouteCollectorProxy $group) {
    $group->map([OPTIONS, GET, POST], '/database', App\Action\DatabaseListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/database/{id}', App\Action\DatabaseAction::class);
    $group->map([OPTIONS, GET], '/database/{id}/table', App\Action\TableListAction::class);
    $group->map([OPTIONS, GET], '/admin-file-explorer[{fpath:.*}]', App\Action\AdminFileExplorerAction::class);
})->add(new App\Middleware\RouteGuardMiddleware(
    boolval($container->get(SETTINGS)['token']['enabled']), 
    array(GET, POST, PUT, DELETE), 
    explode(',', $container->get(SETTINGS)['token']['admin_roles'])
));

// Metamodel actions
$app->group('', function (RouteCollectorProxy $group) {
    $group->map([OPTIONS, GET, POST], '/instance-group', App\Action\InstanceGroupListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/instance-group/{id}', App\Action\InstanceGroupAction::class);
    $group->map([OPTIONS, GET, POST], '/instance', App\Action\InstanceListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}', App\Action\InstanceAction::class);
    $group->map([OPTIONS, GET], '/instance/{name}/file-explorer[{fpath:.*}]', App\Action\InstanceFileExplorerAction::class);
    $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset-group', App\Action\DatasetGroupListAction::class);
    $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset-family', App\Action\DatasetFamilyListAction::class);
    $group->map([OPTIONS, GET, POST], '/instance/{name}/webpage-family', App\Action\WebpageFamilyListAction::class);
    $group->map([OPTIONS, GET], '/instance/{name}/dataset', App\Action\DatasetListByInstanceAction::class);
    $group->map([OPTIONS, GET], '/instance/{name}/webpage', App\Action\WebpageListByInstanceAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/dataset-group/{id}', App\Action\DatasetGroupAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/dataset-family/{id}', App\Action\DatasetFamilyAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/webpage-family/{id}', App\Action\WebpageFamilyAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset-family/{id}/dataset', App\Action\DatasetListAction::class);
    $group->map([OPTIONS, GET, POST], '/webpage-family/{id}/webpage', App\Action\WebpageListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/dataset/{name}', App\Action\DatasetAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/webpage/{id}', App\Action\WebpageAction::class);
    $group->map([OPTIONS, GET], '/dataset/{name}/file-explorer[{fpath:.*}]', App\Action\DatasetFileExplorerAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/criteria-family', App\Action\CriteriaFamilyListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/criteria-family/{id}', App\Action\CriteriaFamilyAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/output-family', App\Action\OutputFamilyListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/output-family/{id}', App\Action\OutputFamilyAction::class);
    $group->map([OPTIONS, GET], '/dataset/{name}/output-category', App\Action\OutputCategoryListByDatasetAction::class);
    $group->map(
        [OPTIONS, GET, POST],
        '/output-family/{id}/output-category',
        App\Action\OutputCategoryListAction::class
    );
    $group->map([OPTIONS, GET, PUT, DELETE], '/output-category/{id}', App\Action\OutputCategoryAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/attribute', App\Action\AttributeListAction::class);
    $group->map([OPTIONS, GET], '/dataset/{name}/column', App\Action\ColumnListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/dataset/{name}/attribute/{id}', App\Action\AttributeAction::class);
    $group->map(
        [OPTIONS, GET, PUT],
        '/dataset/{name}/attribute/{id}/distinct',
        App\Action\AttributeDistinctAction::class
    );
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/image', App\Action\ImageListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/image/{id}', App\Action\ImageAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/file', App\Action\FileListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/file/{id}', App\Action\FileAction::class);
    $group->map([OPTIONS, GET, POST, PUT], '/dataset/{name}/cone-search-config', App\Action\ConeSearchConfigAction::class);
    $group->map([OPTIONS, GET, POST, PUT], '/dataset/{name}/detail-config', App\Action\DetailConfigAction::class);
    $group->map([OPTIONS, GET, POST, PUT], '/dataset/{name}/alias-config', App\Action\AliasConfigAction::class);
    $group->map([OPTIONS, GET, POST, PUT], '/dataset/{name}/search-alias/{alias}', App\Action\SearchAliasAction::class);
})->add(new App\Middleware\RouteGuardMiddleware(
    boolval($container->get(SETTINGS)['token']['enabled']), 
    array(POST, PUT, DELETE), 
    explode(',', $container->get(SETTINGS)['token']['admin_roles'])
));

// Search actions
$app->get('/search/{dname}', App\Action\SearchAction::class);

// Archive actions
$app->get('/start-task-create-archive/{dname}', App\Action\StartTaskCreateArchiveAction::class);
$app->get('/is-archive-available/{id}', App\Action\IsArchiveAvailableAction::class);
$app->get('/download-archive/{dname}/{id}', App\Action\DownloadArchiveAction::class);
