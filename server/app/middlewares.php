<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

$app->add(new App\Middleware\ContentTypeJsonMiddleware());
$app->add(new Slim\Middleware\ContentLengthMiddleware());
$app->add(new App\Middleware\CorsMiddleware());
$app->add(new App\Middleware\MetamodelSqlLoggerMiddleware($container->get('logger'), $container->get('em')));
$app->add(new App\Middleware\AuthorizationMiddleware($container->get('logger'), $container->get(SETTINGS)['token']));
