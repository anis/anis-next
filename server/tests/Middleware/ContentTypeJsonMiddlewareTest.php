<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Middleware;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Psr\Http\Server\RequestHandlerInterface;

final class ContentTypeJsonMiddlewareTest extends TestCase
{
    public function testContentTypeJson(): void
    {
        $request = new ServerRequest('GET', '/');

        $requestHandler = $this->getRequestHandlerMock();
        $requestHandler->method('handle')
            ->with($this->identicalTo($request))
            ->will($this->returnValue(new Response()));

        $contentTypeJsonMiddleware = new \App\Middleware\ContentTypeJsonMiddleware();
        $response = $contentTypeJsonMiddleware->process($request, $requestHandler);
        $this->assertSame((string) $response->getHeaderLine('Content-Type'), 'application/json');
    }

    public function testContentTypeHtml(): void
    {
        $request = new ServerRequest('GET', '/', array(
            'Content-Type' => 'text/html'
        ));

        $requestHandler = $this->getRequestHandlerMock();
        $requestHandler->method('handle')
            ->with($this->identicalTo($request))
            ->will($this->returnValue((new Response())->withHeader('Content-Type', 'text/html')));

        $contentTypeJsonMiddleware = new \App\Middleware\ContentTypeJsonMiddleware();
        $response = $contentTypeJsonMiddleware->process($request, $requestHandler);
        $this->assertSame((string) $response->getHeaderLine('Content-Type'), 'text/html');
    }

    /**
     * @return RequestHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getRequestHandlerMock()
    {
        return $this->createMock(RequestHandlerInterface::class);
    }
}
