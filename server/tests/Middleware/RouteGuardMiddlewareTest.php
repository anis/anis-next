<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Middleware;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Psr\Http\Server\RequestHandlerInterface;

final class RouteGuardMiddlewareTest extends TestCase
{
    public function testOptionsHttpMethod(): void
    {
        $request = new ServerRequest('OPTIONS', '/');

        $requestHandler = $this->getRequestHandlerMock();
        $requestHandler->method('handle')
            ->with($this->identicalTo($request))
            ->will($this->returnValue(new Response(200, [], 'Hello world')));

        $routeGuardMiddleware = new \App\Middleware\RouteGuardMiddleware(
            false,
            array(),
            array('anis_admin', 'superuser')
        );
        $response = $routeGuardMiddleware->process($request, $requestHandler);
        $this->assertSame((string) $response->getBody(), 'Hello world');
    }

    /**
     * @return RequestHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getRequestHandlerMock()
    {
        return $this->createMock(RequestHandlerInterface::class);
    }
}
