<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Middleware;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Psr\Log\LoggerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Logging\SQLLogger;

final class MetamodelSqlLoggerMiddlewareTest extends TestCase
{
    public function testMetamodelSqlLogger(): void
    {
        $request = new ServerRequest('GET', '/');

        $requestHandler = $this->getRequestHandlerMock();
        $requestHandler->method('handle')
            ->with($this->identicalTo($request))
            ->will($this->returnValue(new Response(200, [], 'Hello world')));

        $metamodelSqlLoggerMiddleware = new \App\Middleware\MetamodelSqlLoggerMiddleware(
            $this->createMock(LoggerInterface::class),
            $this->getEntityManager()
        );

        $response = $metamodelSqlLoggerMiddleware->process($request, $requestHandler);
        $this->assertSame((string) $response->getBody(), 'Hello world');
    }

    private function getEntityManager(): EntityManager
    {
        $sqlLogger = $this->getSQLLoggerMock();
        $sqlLogger->queries = [1 => 'sql1', 2 => 'sql2'];

        $configuration = $this->getConfigurationMock();
        $configuration->method('getSQLLogger')
            ->will($this->returnValue($sqlLogger));

        $em = $this->getEntityManagerMock();
        $em->method('getConfiguration')
            ->will($this->returnValue($configuration));

        return $em;
    }

    /**
     * @return RequestHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getRequestHandlerMock()
    {
        return $this->createMock(RequestHandlerInterface::class);
    }

    /**
     * @return SQLLogger|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getSQLLoggerMock()
    {
        return $this->createMock(SQLLogger::class);
    }

    /**
     * @return Configuration|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getConfigurationMock()
    {
        return $this->createMock(Configuration::class);
    }

    /**
     * @return EntityManager|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getEntityManagerMock()
    {
        return $this->createMock(EntityManager::class);
    }
}
