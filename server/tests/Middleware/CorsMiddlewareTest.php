<?php

/*
 * This file is part of Anis Auth.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Middleware;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Psr\Http\Server\RequestHandlerInterface;

final class CorsMiddlewareTest extends TestCase
{
    public function testCorsHeadersForOptionsMethod(): void
    {
        $request = new ServerRequest('OPTIONS', '/');

        $requestHandler = $this->getRequestHandlerMock();
        $requestHandler->method('handle')
            ->with($this->identicalTo($request))
            ->will($this->returnValue(new Response()));

        $corsMiddleware = new \App\Middleware\CorsMiddleware();
        $response = $corsMiddleware->process($request, $requestHandler);
        $this->assertSame((string) $response->getHeaderLine('Access-Control-Allow-Origin'), '*');
        $this->assertSame(
            'Content-Type, Authorization',
            (string) $response->getHeaderLine('Access-Control-Allow-Headers')
        );
    }

    public function testCorsHeadersForGetMethod(): void
    {
        $request = new ServerRequest('GET', '/');

        $requestHandler = $this->getRequestHandlerMock();
        $requestHandler->method('handle')
            ->with($this->identicalTo($request))
            ->will($this->returnValue(new Response()));

        $corsMiddleware = new \App\Middleware\CorsMiddleware();
        $response = $corsMiddleware->process($request, $requestHandler);
        $this->assertSame((string) $response->getHeaderLine('Access-Control-Allow-Origin'), '*');
    }

    /**
     * @return RequestHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getRequestHandlerMock()
    {
        return $this->createMock(RequestHandlerInterface::class);
    }
}
