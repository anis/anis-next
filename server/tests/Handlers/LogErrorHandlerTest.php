<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Handlers;

use PHPUnit\Framework\TestCase;
use Slim\Interfaces\CallableResolverInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Log\LoggerInterface;
use ReflectionClass;

final class LogErrorHandlerTest extends TestCase
{
    private $logErrorHandler;

    protected function setUp(): void
    {
        $callableResolver = $this->createMock(CallableResolverInterface::class);
        $responseFactory = $this->createMock(ResponseFactoryInterface::class);
        $this->logErrorHandler = new \App\Handlers\LogErrorHandler($callableResolver, $responseFactory);
    }

    protected static function getMethod($name)
    {
        $class = new ReflectionClass('App\Handlers\LogErrorHandler');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    public function testLogError(): void
    {
        $logger = $this->getLoggerInterfaceMock();
        $logger->expects($this->once())
            ->method('error');
        $this->logErrorHandler->setLogger($logger);

        $logError = self::getMethod('logError');
        $logError->invokeArgs($this->logErrorHandler, array('Log test'));
    }

    /**
     * @return LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getLoggerInterfaceMock()
    {
        return $this->createMock(LoggerInterface::class);
    }
}
