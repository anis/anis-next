<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\Database;
use App\Entity\DatasetFamily;
use App\Entity\Instance;

final class DatasetListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatasetListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testDatasetFamilyNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset family with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAllDatasets(): void
    {
        $datasetFamily = $this->getDatasetFamilyMock();
        $this->entityManager->method('find')->willReturn($datasetFamily);

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            array('datasetFamily' => $datasetFamily)
        );
        $this->entityManager->method('getRepository')->with('App\Entity\Dataset')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testAddANewDatasetEmptyNameField(): void
    {
        $datasetFamily = $this->getDatasetFamilyMock();
        $this->entityManager->method('find')->willReturn($datasetFamily);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param name needed to add a new dataset');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewDatasetDatabaseNotFound(): void
    {
        $datasetFamily = $this->getDatasetFamilyMock();
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($datasetFamily, null);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Database with id 1 is not found');
        $fields = $this->getNewDatasetFields();
        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewDataset(): void
    {
        $instance = $this->getInstanceMock();
        $datasetFamily = $this->getDatasetFamilyMock();
        $datasetFamily->method('getInstance')->willReturn($instance);
        $database = $this->createMock(Database::class);
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($datasetFamily, $database);

        $this->entityManager->expects($this->once())->method('persist');

        $fields = $this->getNewDatasetFields();
        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/instance/aspic/dataset', array(
            'Content-Type' => 'application/json'
        ));
    }

    private function getNewDatasetFields(): array
    {
        return array(
            'name' => 'dataset1',
            'table_ref' => 'table1',
            'label' => 'Dataset1 label',
            'description' => 'Dataset1 description',
            'display' => 10,
            'data_path' => '/mnt/dataset1',
            'public' => true,
            'info_survey_enabled' => true,
            'info_survey_label' => "More about this survey",
            'cone_search_enabled' => false,
            'cone_search_opened' => true,
            'cone_search_column_ra' => null,
            'cone_search_column_dec' => null,
            'cone_search_plot_enabled' => false,
            'download_enabled' => true,
            'download_opened' => false,
            'download_json' => true,
            'download_csv' => true,
            'download_ascii' => true,
            'download_vo' => false,
            'download_fits' => false,
            'summary_enabled' => true,
            'summary_opened' => false,
            'server_link_enabled' => false,
            'server_link_opened' => false,
            'datatable_enabled' => true,
            'datatable_opened' => false,
            'datatable_selectable_rows' => false,
            'id_database' => 1,
            'id_dataset_family' => 1
        );
    }

    /**
     * @return DatasetFamily|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatasetFamilyMock()
    {
        return $this->createMock(DatasetFamily::class);
    }

    /**
     * @return Instance|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getInstanceMock()
    {
        return $this->createMock(Instance::class);
    }

    /**
     * @return ObjectRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getObjectRepositoryMock()
    {
        return $this->createMock(ObjectRepository::class);
    }
}
