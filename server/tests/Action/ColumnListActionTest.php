<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\Type;
use App\Entity\Database;
use App\Entity\Dataset;
use App\Search\DBALConnectionFactory;

final class ColumnListActionTest extends TestCase
{
    private $action;
    private $entityManager;
    private $connectionFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->connectionFactory = $this->createMock(DBALConnectionFactory::class);
        $this->action = new \App\Action\ColumnListAction($this->entityManager, $this->connectionFactory);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testDatabaseIsNotFound(): void
    {
        $this->entityManager->method('find')->willReturn(null);
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset with name test is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'test'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetColumnsList(): void
    {
        $database = $this->getDatabaseMock();
        $dataset = $this->getDatasetMock();
        $dataset->method('getDatabase')->willReturn($database);
        $dataset->method('getTableRef')->willReturn('observations');
        $this->entityManager->method('find')->willReturn($dataset);
        $columnId = $this->getColumnMock();
        $columnIdType = $this->getTypeMock();
        $columnIdType->method('getName')->willReturn('integer');
        $columnId->method('getName')->willReturn('id');
        $columnId->method('getType')->willReturn($columnIdType);
        $columnRa = $this->getColumnMock();
        $columnRaType = $this->getTypeMock();
        $columnRaType->method('getName')->willReturn('float');
        $columnRa->method('getName')->willReturn('ra');
        $columnRa->method('getType')->willReturn($columnRaType);
        $columnDec = $this->getColumnMock();
        $columnDecType = $this->getTypeMock();
        $columnDecType->method('getName')->willReturn('float');
        $columnDec->method('getName')->willReturn('dec');
        $columnDec->method('getType')->willReturn($columnDecType);
        $sm = $this->getAbstractSchemaManagerMock();
        $sm->method('listTableColumns')->willReturn(array($columnId, $columnRa, $columnDec));
        $connection = $this->getConnectionMock();
        $connection->method('createSchemaManager')->willReturn($sm);
        $this->connectionFactory->method('create')->willReturn($connection);

        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'observations'));
        $this->assertSame(
            '[{"name":"id","type":"integer"},{"name":"ra","type":"float"},{"name":"dec","type":"float"}]',
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/database/1/table/observation/column', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return Database|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatabaseMock()
    {
        return $this->createMock(Database::class);
    }

    /**
     * @return Dataset|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatasetMock()
    {
        return $this->createMock(Dataset::class);
    }

    /**
     * @return Column|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getColumnMock()
    {
        return $this->createMock(Column::class);
    }

    /**
     * @return Type|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getTypeMock()
    {
        return $this->createMock(Type::class);
    }

    /**
     * @return AbstractSchemaManager|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getAbstractSchemaManagerMock()
    {
        return $this->createMock(AbstractSchemaManager::class);
    }

    /**
     * @return Connection|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getConnectionMock()
    {
        return $this->createMock(Connection::class);
    }
}
