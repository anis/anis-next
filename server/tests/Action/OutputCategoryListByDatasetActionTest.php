<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr;
use App\Entity\Dataset;

final class OutputCategoryListByDatasetActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\OutputCategoryListByDatasetAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testDatasetIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset with name obs_cat is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAllOutputCategoriesForADataset(): void
    {
        $dataset = $this->getDatasetMock();
        $this->entityManager->method('find')->willReturn($dataset);

        $expr = $this->getExprMock();
        $query = $this->getAbstractQueryMock();
        $query->expects($this->once())->method('getResult');

        $queryBuilder = $this->getQueryBuilderMock();
        $queryBuilder->method('select')->willReturn($queryBuilder);
        $queryBuilder->method('from')->willReturn($queryBuilder);
        $queryBuilder->method('join')->willReturn($queryBuilder);
        $queryBuilder->method('where')->willReturn($queryBuilder);
        $queryBuilder->method('expr')->willReturn($expr);
        $queryBuilder->expects($this->once())->method('getQuery')->willReturn($query);

        $this->entityManager->method('createQueryBuilder')->willReturn($queryBuilder);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'obs_cat'));
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/dataset/obs_cat/output-category', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return Dataset|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatasetMock()
    {
        return $this->createMock(Dataset::class);
    }

    /**
     * @return Expr|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getExprMock()
    {
        return $this->createMock(Expr::class);
    }

    /**
     * @return AbstractQuery|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getAbstractQueryMock()
    {
        return $this->createMock(AbstractQuery::class);
    }

    /**
     * @return QueryBuilder|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getQueryBuilderMock()
    {
        return $this->createMock(QueryBuilder::class);
    }
}
