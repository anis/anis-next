<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;

final class DatabaseListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatabaseListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllDatabases(): void
    {
        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            array(),
            array('id' => 'ASC')
        );
        $this->entityManager->method('getRepository')->with('App\Entity\Database')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array());
    }

    public function testAddANewDatabaseEmptyLabelField(): void
    {
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to add a new database');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewDatabase(): void
    {
        $this->entityManager->expects($this->once())->method('persist');

        $fields = array(
            'label' => 'Test1',
            'dbname' => 'test1',
            'dbtype' => 'pgsql',
            'dbhost' => 'db',
            'dbport' => 5432,
            'dblogin' => 'test',
            'dbpassword' => 'test'
        );

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/database', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return ObjectRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getObjectRepositoryMock()
    {
        return $this->createMock(ObjectRepository::class);
    }
}
