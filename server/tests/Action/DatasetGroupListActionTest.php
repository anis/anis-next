<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\Instance;

final class DatasetGroupListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatasetGroupListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllGroupsOfAnInstance(): void
    {
        $instance = $this->getInstanceMock();
        $this->entityManager->method('find')->willReturn($instance);

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            array('instance' => $instance),
            array('id' => 'ASC')
        );
        $this->entityManager->method('getRepository')->with('App\Entity\DatasetGroup')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'default'));
    }

    public function testAddANewGroupEmptyRoleField(): void
    {
        $instance = $this->getInstanceMock();
        $this->entityManager->method('find')->willReturn($instance);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param role needed to add a new dataset group');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'default'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewGroupWithoutDataset(): void
    {
        $instance = $this->getInstanceMock();
        $this->entityManager->method('find')->willReturn($instance);

        $this->entityManager->expects($this->once())->method('persist');

        $fields = array(
            'role' => 'group1',
            'datasets' => []
        );

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('name' => 'default'));
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/group', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return Instance|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getInstanceMock()
    {
        return $this->createMock(Instance::class);
    }

    /**
     * @return ObjectRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getObjectRepositoryMock()
    {
        return $this->createMock(ObjectRepository::class);
    }
}
