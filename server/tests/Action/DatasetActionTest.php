<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\DatasetFamily;
use App\Entity\Dataset;

final class DatasetActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatasetAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testDatasetIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset with name obs_cat is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetADatasetByName(): void
    {
        $dataset = $this->getDatasetMock();
        $dataset->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($dataset);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'obs_cat'));
    }

    public function testEditADatasetEmptyLabelField(): void
    {
        $dataset = $this->getDatasetMock();
        $this->entityManager->method('find')->willReturn($dataset);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the dataset');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditADatasetFamilyNotFound(): void
    {
        $dataset = $this->getDatasetMock();
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($dataset, null);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Dataset family with id 2 is not found');
        $fields = $this->getEditDatasetFields();
        $fields['id_dataset_family'] = 2;
        $request = $this->getRequest('PUT')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditADataset(): void
    {
        $dataset = $this->getDatasetMock();
        $datasetFamily = $this->createMock(DatasetFamily::class);

        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($dataset, $datasetFamily);
        $this->entityManager->expects($this->once())->method('flush');

        $fields = $this->getEditDatasetFields();
        $request = $this->getRequest('PUT')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(200, (int) $response->getStatusCode());
    }

    public function testDeleteADataset(): void
    {
        $dataset = $this->getDatasetMock();
        $dataset->method('getName')->willReturn('obs_cat');
        $this->entityManager->method('find')->willReturn($dataset);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertSame(
            json_encode(array('message' => 'Dataset with name obs_cat is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/dataset/obs_cat', array(
            'Content-Type' => 'application/json'
        ));
    }

    private function getEditDatasetFields(): array
    {
        return array(
            'label' => 'Dataset1 label modified',
            'description' => 'Dataset1 description',
            'display' => 20,
            'data_path' => '/mnt/dataset1',
            'public' => true,
            'info_survey_enabled' => true,
            'info_survey_label' => "More about this survey",
            'cone_search_enabled' => false,
            'cone_search_opened' => true,
            'cone_search_column_ra' => null,
            'cone_search_column_dec' => null,
            'cone_search_plot_enabled' => false,
            'download_enabled' => true,
            'download_opened' => false,
            'download_json' => true,
            'download_csv' => true,
            'download_ascii' => true,
            'download_vo' => false,
            'download_fits' => false,
            'summary_enabled' => true,
            'summary_opened' => false,
            'server_link_enabled' => false,
            'server_link_opened' => false,
            'datatable_enabled' => true,
            'datatable_opened' => false,
            'datatable_selectable_rows' => false,
            'id_dataset_family' => 1
        );
    }

    /**
     * @return Dataset|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatasetMock()
    {
        return $this->createMock(Dataset::class);
    }
}
