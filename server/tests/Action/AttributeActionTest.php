<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\CriteriaFamily;
use App\Entity\OutputCategory;
use App\Entity\Attribute;

final class AttributeActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\AttributeAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testAttributeIsNotFound(): void
    {
        $repository = $this->getObjectRepositoryMock();
        $repository->method('findOneBy')->willReturn(null);
        $this->entityManager->method('getRepository')->willReturn($repository);

        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Attribute with dataset name obs_cat and attribute id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat', 'id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnAttributeById(): void
    {
        $attribute = $this->getAttributeMock();
        $attribute->expects($this->once())->method('jsonSerialize');
        $repository = $this->getObjectRepositoryMock();
        $repository->method('findOneBy')->willReturn($attribute);
        $this->entityManager->method('getRepository')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'obs_cat', 'id' => 1));
    }

    public function testEditAnAttribute(): void
    {
        $attribute = $this->getAttributeMock();
        $attribute->expects($this->once())->method('jsonSerialize');
        $repository = $this->getObjectRepositoryMock();
        $repository->method('findOneBy')->willReturn($attribute);
        $this->entityManager->method('getRepository')->willReturn($repository);

        $criteriaFamily = $this->createMock(CriteriaFamily::class);
        $outputCategory = $this->createMock(OutputCategory::class);

        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($criteriaFamily, $outputCategory);

        $this->entityManager->expects($this->once())->method('flush');

        $fields = $this->getEditAttributeFields();
        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('name' => 'obs_cat', 'id' => 1));
    }

    public function testEditAnAttributeWithCriteriaFamilyAndOutputCategoryNull(): void
    {
        $attribute = $this->getAttributeMock();
        $attribute->expects($this->once())->method('jsonSerialize');
        $repository = $this->getObjectRepositoryMock();
        $repository->method('findOneBy')->willReturn($attribute);
        $this->entityManager->method('getRepository')->willReturn($repository);

        $this->entityManager->expects($this->once())->method('flush');

        $fields = $this->getEditAttributeFields();
        $fields['id_criteria_family'] = null;
        $fields['id_output_category'] = null;
        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('name' => 'obs_cat', 'id' => 1));
    }

    public function testDeleteAnAttribute(): void
    {
        $attribute = $this->getAttributeMock();
        $attribute->method('getId')->willReturn(1);
        $repository = $this->getObjectRepositoryMock();
        $repository->method('findOneBy')->willReturn($attribute);
        $this->entityManager->method('getRepository')->willReturn($repository);
        $this->entityManager->expects($this->once())->method('remove')->with($attribute);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat', 'id' => 1));
        $this->assertSame(
            json_encode(array('message' => 'Attribute with id 1 is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/dataset/obs_cat/attribute/1', array(
            'Content-Type' => 'application/json'
        ));
    }

    private function getEditAttributeFields(): array
    {
        return array(
            'id' => 1,
            'name' => 'id',
            'label' => 'ID',
            'form_label' => 'ID',
            'description' => 'ID description',
            'primary_key' => false,
            'type' => 'integer',
            'search_type' => null,
            'operator' => null,
            'dynamic_operator' => false,
            'min' => null,
            'max' => null,
            'options' => null,
            'placeholder_min' => null,
            'placeholder_max' => null,
            'criteria_display' => 10,
            'output_display' => 20,
            'selected' => false,
            'renderer' => null,
            'renderer_config' => null,
            'order_by' => false,
            'archive' => false,
            'detail' => false,
            'detail_display' => 10,
            'detail_renderer' => null,
            'detail_renderer_config' => null,
            'vo_utype' => null,
            'vo_ucd' => null,
            'vo_unit' => null,
            'vo_description' => null,
            'vo_datatype' => null,
            'vo_size' => 0,
            'id_criteria_family' => 1,
            'id_output_category' => 1,
            'id_detail_output_category' => null
        );
    }

    /**
     * @return Attribute|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getAttributeMock()
    {
        return $this->createMock(Attribute::class);
    }

    /**
     * @return ObjectRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getObjectRepositoryMock()
    {
        return $this->createMock(ObjectRepository::class);
    }
}
