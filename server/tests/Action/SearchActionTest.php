<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Slim\Exception\HttpNotFoundException;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Doctrine\ORM\EntityManager;
use App\Search\DBALConnectionFactory;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Response\IResponseFactory;

final class SearchActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\SearchAction(
            $this->entityManager,
            $this->createMock(DBALConnectionFactory::class),
            $this->createMock(AnisQueryBuilder::class),
            $this->createMock(IResponseFactory::class),
            array()
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testDatasetIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset with name obs_cat is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('dname' => 'obs_cat'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/search/obs_cat?a=1,2,3', array(
            'Content-Type' => 'application/json'
        ));
    }
}
