<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\OutputFamily;
use App\Entity\OutputCategory;

final class OutputCategoryActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\OutputCategoryAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testOutputCategoryIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Output category with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnOutputCategoryById(): void
    {
        $outputCategory = $this->getOutputCategoryMock();
        $outputCategory->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($outputCategory);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testEditAnOutputCategoryEmptyLabelField(): void
    {
        $outputCategory = $this->getOutputCategoryMock();
        $this->entityManager->method('find')->willReturn($outputCategory);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the output category');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditAnOutputCategoryOutputFamilyIsNotFound(): void
    {
        $outputCategory = $this->getOutputCategoryMock();
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($outputCategory, null);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Output family with id 2 is not found');
        $fields = array(
            'label' => 'Default output category',
            'display' => 20,
            'id_output_family' => 2
        );
        $request = $this->getRequest('PUT')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditAnOutputCategory(): void
    {
        $outputCategory = $this->getOutputCategoryMock();
        $outputFamily = $this->getOutputFamilyMock();
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($outputCategory, $outputFamily);
        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'label' => 'Default output category',
            'display' => 20,
            'id_output_family' => 1
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testDeleteAnOutputCategory(): void
    {
        $outputCategory = $this->getOutputCategoryMock();
        $outputCategory->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($outputCategory);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertSame(
            json_encode(array('message' => 'Output category with id 1 is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/output-category/1', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return OutputCategory|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getOutputCategoryMock()
    {
        return $this->createMock(OutputCategory::class);
    }

    /**
     * @return OutputFamily|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getOutputFamilyMock()
    {
        return $this->createMock(OutputFamily::class);
    }
}
