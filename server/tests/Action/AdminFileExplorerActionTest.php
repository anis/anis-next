<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;

final class AdminFileExplorerActionTest extends TestCase
{
    private $action;

    protected function setUp(): void
    {
        $this->action = new \App\Action\AdminFileExplorerAction('/data');
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/file-explorer', array(
            'Content-Type' => 'application/json'
        ));
    }
}
