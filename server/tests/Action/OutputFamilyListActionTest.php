<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\Dataset;

final class OutputFamilyListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\OutputFamilyListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testDatasetIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset with name obs_cat is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAllOutputFamiliesOfADataset(): void
    {
        $dataset = $this->getDatasetMock();
        $this->entityManager->method('find')->willReturn($dataset);

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            array('dataset' => $dataset),
            array('id' => 'ASC')
        );
        $this->entityManager->method('getRepository')->with('App\Entity\OutputFamily')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'obs_cat'));
    }

    public function testAddANewOutputFamilyEmptyLabelField(): void
    {
        $dataset = $this->getDatasetMock();
        $this->entityManager->method('find')->willReturn($dataset);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to add a new output family');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewOutputFamily(): void
    {
        $dataset = $this->getDatasetMock();
        $this->entityManager->method('find')->willReturn($dataset);

        $this->entityManager->expects($this->once())->method('persist');

        $fields = array(
            'label' => 'Default output family',
            'display' => 10,
            'opened' => true
        );

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/dataset/obs_cat/output-family', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return Dataset|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatasetMock()
    {
        return $this->createMock(Dataset::class);
    }

    /**
     * @return ObjectRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getObjectRepositoryMock()
    {
        return $this->createMock(ObjectRepository::class);
    }
}
