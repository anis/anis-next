<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\OutputFamily;

final class OutputCategoryListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\OutputCategoryListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testOutputFamilyIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Output family with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAllOutputCategories(): void
    {
        $outputFamily = $this->getOutputFamilyMock();
        $this->entityManager->method('find')->willReturn($outputFamily);

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            array('outputFamily' => $outputFamily),
            array('id' => 'ASC')
        );
        $this->entityManager->method('getRepository')->with('App\Entity\OutputCategory')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testAddANewOutputCategoryEmptyLabelField(): void
    {
        $outputFamily = $this->getOutputFamilyMock();
        $this->entityManager->method('find')->willReturn($outputFamily);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to add a new output category');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewOutputCategory(): void
    {
        $outputFamily = $this->getOutputFamilyMock();
        $this->entityManager->method('find')->willReturn($outputFamily);

        $this->entityManager->expects($this->once())->method('persist');

        $fields = array(
            'label' => 'Default output category',
            'display' => 10
        );

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/output-family/1/output-category', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return OutputFamily|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getOutputFamilyMock()
    {
        return $this->createMock(OutputFamily::class);
    }

    /**
     * @return ObjectRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getObjectRepositoryMock()
    {
        return $this->createMock(ObjectRepository::class);
    }
}
