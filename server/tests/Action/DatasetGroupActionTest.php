<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\DatasetGroup;

final class DatasetGroupActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatasetGroupAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testGroupIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset group with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAGroupById(): void
    {
        $group = $this->getGroupMock();
        $group->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($group);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testEditAGroupEmptyRoleField(): void
    {
        $group = $this->getGroupMock();
        $this->entityManager->method('find')->willReturn($group);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param role needed to edit the dataset group');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditAGroupWithoutDataset(): void
    {
        $group = $this->getGroupMock();
        $this->entityManager->method('find')->willReturn($group);

        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'role' => 'New_role',
            'datasets' => array()
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testDeleteAGroup(): void
    {
        $group = $this->getGroupMock();
        $group->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($group);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertSame(
            json_encode(array('message' => 'Dataset group with id 1 is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/group/1', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return DatasetGroup|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getGroupMock()
    {
        return $this->createMock(DatasetGroup::class);
    }
}
