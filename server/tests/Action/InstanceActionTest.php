<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\Instance;

final class InstanceActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\InstanceAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testInstanceIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Instance with name aspic is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'aspic'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnInstanceByName(): void
    {
        $instance = $this->getInstanceMock();
        $instance->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($instance);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'aspic'));
    }

    public function testEditAnInstanceEmptyLabelField(): void
    {
        $instance = $this->getInstanceMock();
        $this->entityManager->method('find')->willReturn($instance);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the instance');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'aspic'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditAnInstance(): void
    {
        $instance = $this->getInstanceMock();
        $this->entityManager->method('find')->willReturn($instance);

        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'name' => 'aspic',
            'label' => 'Aspic',
            'description' => 'Test',
            'scientific_manager' => 'M. Durand',
            'instrument' => 'Multiple',
            'wavelength_domain' => 'Visible imaging',
            'display' => 10,
            'data_path' => '/DEFAULT',
            'files_path' => '/INSTANCE_FILES',
            'public' => true,
            'portal_logo' => '',
            'portal_color' => '#7AC29A',
            'design_background_color' => '#FFFFFF',
            'design_text_color' => '#212529',
            'design_font_family' => 'Roboto, sans-serif',
            'design_link_color' => '#007BFF',
            'design_link_hover_color' => '#0056B3',
            'design_logo' => 'logo.png',
            'design_logo_href' => null,
            'design_favicon' => 'favicon.ico',
            'navbar_background_color' => '#F8F9FA',
            'navbar_border_bottom_color' => '#DEE2E6',
            'navbar_color_href' => '#000000',
            'navbar_font_family' => 'auto',
            'navbar_sign_in_btn_color' => '#28A745',
            'navbar_user_btn_color' => '#7AC29A',
            'footer_background_color' => '#F8F9FA',
            'footer_border_top_color' => '#DEE2E6',
            'footer_text_color' => '#000000',
            'footer_logos' => null,
            'family_border_color' => '#DFDFDF',
            'family_header_background_color' => '#F7F7F7',
            'family_title_color' => '#007BFF',
            'family_title_bold' => false,
            'family_background_color' => '#FFFFFF',
            'family_text_color' => '#212529',
            'progress_bar_title' => 'Dataset search',
            'progress_bar_title_color' => '#000000',
            'progress_bar_subtitle' => 'Select a dataset, add criteria, select output columns and display the result.',
            'progress_bar_subtitle_color' => '#6C757D',
            'progress_bar_step_dataset_title' => 'Dataset selection',
            'progress_bar_step_criteria_title' => 'Search criteria',
            'progress_bar_step_output_title' => 'Output columns',
            'progress_bar_step_result_title' => 'Result table',
            'progress_bar_color' => '#E9ECEF',
            'progress_bar_active_color' => '#7AC29A',
            'progress_bar_circle_color' => '#FFFFFF',
            'progress_bar_circle_icon_color' => '#CCCCCC',
            'progress_bar_circle_icon_active_color' => '#FFFFFF',
            'progress_bar_text_color' => '#91B2BF',
            'progress_bar_text_bold' => false,
            'search_next_btn_color' => '#007BFF',
            'search_next_btn_hover_color' => '#007BFF',
            'search_next_btn_hover_text_color' => '#FFFFFF',
            'search_back_btn_color' => '#6C757D',
            'search_back_btn_hover_color' => '#6C757D',
            'search_back_btn_hover_text_color' => '#FFFFFF',
            'search_info_background_color' => '#E9ECEF',
            'search_info_text_color' => '#000000',
            'search_info_help_enabled' => true,
            'dataset_select_btn_color' => '#6C757D',
            'dataset_select_btn_hover_color' => '#6C757D',
            'dataset_select_btn_hover_text_color' => '#FFFFFF',
            'dataset_selected_icon_color' => '#28A745',
            'search_criterion_background_color' => '#7AC29A',
            'search_criterion_text_color' => '#000000',
            'output_columns_selected_color' => '#7AC29A',
            'output_columns_select_all_btn_color' => '#6C757D',
            'output_columns_select_all_btn_hover_color' => '#6C757D',
            'output_columns_select_all_btn_hover_text_color' => '#FFFFFF',
            'result_panel_border_size' => '1px',
            'result_panel_border_color' => '#DEE2E6',
            'result_panel_title_color' => '#000000',
            'result_panel_background_color' => '#FFFFFF',
            'result_panel_text_color' => '#000000',
            'result_download_btn_color' => '#007BFF',
            'result_download_btn_hover_color' => '#0069D9',
            'result_download_btn_text_color' => '#FFFFFF',
            'result_datatable_actions_btn_color' => '#007BFF',
            'result_datatable_actions_btn_hover_color' => '#0069D9',
            'result_datatable_actions_btn_text_color' => '#FFFFFF',
            'result_datatable_bordered' => true,
            'result_datatable_bordered_radius' => false,
            'result_datatable_border_color' => '#DEE2E6',
            'result_datatable_header_background_color' => '#FFFFFF',
            'result_datatable_header_text_color' => '#000000',
            'result_datatable_rows_background_color' => '#FFFFFF',
            'result_datatable_rows_text_color' => '#000000',
            'result_datatable_sorted_color' => '#C5C5C5',
            'result_datatable_sorted_active_color' => '#000000',
            'result_datatable_link_color' => '#007BFF',
            'result_datatable_link_hover_color' => '#0056B3',
            'result_datatable_rows_selected_color' => '#7AC29A',
            'result_datatable_pagination_link_color' => '#7AC29A',
            'result_datatable_pagination_active_bck_color' => '#7AC29A',
            'result_datatable_pagination_active_text_color' => '#FFFFFF',
            'samp_enabled' => true,
            'back_to_portal' => true,
            'user_menu_enabled' => true,
            'search_by_criteria_allowed' => true,
            'search_by_criteria_label' => 'Search',
            'search_multiple_allowed' => false,
            'search_multiple_label' => 'Search multiple',
            'search_multiple_all_datasets_selected' => false,
            'search_multiple_progress_bar_title' => 'Search around a position in multiple datasets',
            'search_multiple_progress_bar_subtitle' => 'Fill RA & DEC position, select datasets and display the result',
            'search_multiple_progress_bar_step_position' => 'Position',
            'search_multiple_progress_bar_step_datasets' => 'Datasets',
            'search_multiple_progress_bar_step_result' => 'Result',
            'documentation_allowed' => false,
            'documentation_label' => ''
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('name' => 'aspic'));
    }

    public function testDeleteAnInstance(): void
    {
        $instance = $this->getInstanceMock();
        $instance->method('getName')->willReturn('aspic');
        $this->entityManager->method('find')->willReturn($instance);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('name' => 'aspic'));
        $this->assertSame(
            json_encode(array('message' => 'Instance with name aspic is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/instance/aspic', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return Instance|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getInstanceMock()
    {
        return $this->createMock(Instance::class);
    }
}
