<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\Dataset;

final class AttributeListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\AttributeListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testDatasetIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset with name obs_cat is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAllAttributesOfADataset(): void
    {
        $dataset = $this->createMock(Dataset::class);
        $this->entityManager->method('find')->willReturn($dataset);

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            array('dataset' => $dataset),
            array('id' => 'ASC')
        );
        $this->entityManager->method('getRepository')->with('App\Entity\Attribute')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'obs_cat'));
    }

    public function testAddNewAttributeEmptyIdField(): void
    {
        $dataset = $this->createMock(Dataset::class);
        $this->entityManager->method('find')->willReturn($dataset);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param id needed to add a new attribute');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertSame(201, $response->getStatusCode());
    }

    public function testAddNewAttribute(): void
    {
        $dataset = $this->createMock(Dataset::class);
        $this->entityManager->method('find')->willReturn($dataset);
        $fields = array(
            'id' => 1,
            'name' => 'id',
            'label' => 'ID',
            'form_label' => 'ID',
            'description' => 'ID description',
            'primary_key' => false,
            'type' => 'integer',
            'search_type' => null,
            'operator' => null,
            'dynamic_operator' => false,
            'min' => null,
            'max' => null,
            'options' => null,
            'placeholder_min' => null,
            'placeholder_max' => null,
            'criteria_display' => 10,
            'output_display' => 20,
            'selected' => false,
            'renderer' => null,
            'renderer_config' => null,
            'order_by' => false,
            'archive' => false,
            'detail' => false,
            'detail_display' => 10,
            'detail_renderer' => null,
            'detail_renderer_config' => null,
            'vo_utype' => null,
            'vo_ucd' => null,
            'vo_unit' => null,
            'vo_description' => null,
            'vo_datatype' => null,
            'vo_size' => 0,
            'id_criteria_family' => null,
            'id_output_category' => null,
            'id_detail_output_category' => null
        );
        $this->entityManager->expects($this->once())->method('persist');

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat'));
        $this->assertSame(201, $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/dataset/obs_cat/attribute', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return ObjectRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getObjectRepositoryMock()
    {
        return $this->createMock(ObjectRepository::class);
    }
}
