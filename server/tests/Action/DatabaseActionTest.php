<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\Database;

final class DatabaseActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatabaseAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testDatabaseIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Database with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetADatabaseById(): void
    {
        $database = $this->getDatabaseMock();
        $database->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($database);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testEditADatabaseEmptyLabelField(): void
    {
        $database = $this->getDatabaseMock();
        $this->entityManager->method('find')->willReturn($database);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the database');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditADatabase(): void
    {
        $database = $this->getDatabaseMock();
        $this->entityManager->method('find')->willReturn($database);
        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'id' => 1,
            'label' => 'Label',
            'dbname' => 'test',
            'dbtype' => 'pgsql',
            'dbhost' => 'db',
            'dbport' => 5432,
            'dblogin' => 'test',
            'dbpassword' => 'test'
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testDeleteADatabase(): void
    {
        $database = $this->getDatabaseMock();
        $database->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($database);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertSame(
            json_encode(array('message' => 'Database with id 1 is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/database/1', array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @return Database|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatabaseMock()
    {
        return $this->createMock(Database::class);
    }
}
