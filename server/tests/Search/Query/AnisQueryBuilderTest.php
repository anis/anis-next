<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;
use App\Search\Query\Select;
use App\Search\Query\From;

final class AnisQueryBuilderTest extends TestCase
{
    /**
     * @var AnisQueryBuilder
     */
    private $anisQueryBuilder;

    protected function setUp(): void
    {
        $this->anisQueryBuilder = new AnisQueryBuilder();
    }

    public function testGetDoctrineQueryBuilder()
    {
        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $this->anisQueryBuilder->setDoctrineQueryBuilder($doctrineQueryBuilder);
        $this->assertSame($doctrineQueryBuilder, $this->anisQueryBuilder->getDoctrineQueryBuilder());
    }

    public function testGetDatasetSelected()
    {
        $datasetSelected = $this->createMock(Dataset::class);
        $this->anisQueryBuilder->setDatasetSelected($datasetSelected);
        $this->assertSame($datasetSelected, $this->anisQueryBuilder->getDatasetSelected());
    }

    public function testGetAttributesSelected()
    {
        $ra = $this->createMock(Attribute::class);
        $dec = $this->createMock(Attribute::class);
        $attributesSelected = array($ra, $dec);
        $this->anisQueryBuilder->setAttributesSelected($attributesSelected);
        $this->assertSame($attributesSelected, $this->anisQueryBuilder->getAttributesSelected());
    }

    public function testBuild()
    {
        $select = $this->createMock(Select::class);
        $select->expects($this->once())->method('__invoke');
        $from = $this->createMock(From::class);
        $from->expects($this->once())->method('__invoke');
        $this->anisQueryBuilder->setDatasetSelected($this->createMock(Dataset::class));
        $this->anisQueryBuilder->addQueryPart($select);
        $this->anisQueryBuilder->addQueryPart($from);
        $this->anisQueryBuilder->build(array());
    }
}
