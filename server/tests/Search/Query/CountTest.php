<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use App\Search\Query\Count;
use App\Search\Query\AnisQueryBuilder;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Entity\Dataset;

final class CountTest extends TestCase
{
    public function testCount(): void
    {
        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->expects($this->once())->method('select');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $datasetSelected = $this->createMock(Dataset::class);
        $queryParams = array('a' => 'count');
        (new Count())($anisQueryBuilder, $datasetSelected, $queryParams);
    }
}
