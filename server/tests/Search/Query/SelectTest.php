<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use App\Search\Query\Select;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Query\SearchQueryException;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;

final class SelectTest extends TestCase
{
    public function testSelect(): void
    {
        $id = $this->createMock(Attribute::class);
        $id->method('getId')->willReturn(1);
        $ra = $this->createMock(Attribute::class);
        $ra->method('getId')->willReturn(2);
        $dec = $this->createMock(Attribute::class);
        $dec->method('getId')->willReturn(3);
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getAttributes')->willReturn(array($id, $ra, $dec));
        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->expects($this->once())->method('select');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = array('a' => '1;2;3');
        (new Select())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testSelectException(): void
    {
        $this->expectException(SearchQueryException::class);
        $id = $this->createMock(Attribute::class);
        $id->method('getId')->willReturn(1);
        $ra = $this->createMock(Attribute::class);
        $ra->method('getId')->willReturn(2);
        $dec = $this->createMock(Attribute::class);
        $dec->method('getId')->willReturn(3);
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getAttributes')->willReturn(array($id, $ra, $dec));
        $datasetSelected->method('getLabel')->willReturn('Test');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $queryParams = array('a' => '1;2;3;4');
        (new Select())($anisQueryBuilder, $datasetSelected, $queryParams);
    }
}
