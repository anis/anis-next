<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Query\Order;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;
use App\Search\Query\SearchQueryException;

final class OrderTest extends TestCase
{
    public function testOrderAsc(): void
    {
        $attribute = $this->createMock(Attribute::class);
        $attribute->method('getId')->willReturn(1);
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getAttributes')->willReturn(array($attribute));
        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->expects($this->once())->method('orderBy');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        (new Order())($anisQueryBuilder, $datasetSelected, array('o' => '1:a'));
    }

    public function testOrderDesc(): void
    {
        $attribute = $this->createMock(Attribute::class);
        $attribute->method('getId')->willReturn(1);
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getAttributes')->willReturn(array($attribute));
        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->expects($this->once())->method('orderBy');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        (new Order())($anisQueryBuilder, $datasetSelected, array('o' => '1:d'));
    }

    public function testOrderException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $datasetSelected = $this->createMock(Dataset::class);
        (new Order())($anisQueryBuilder, $datasetSelected, array('o' => '1:a:d'));
    }

    public function testOrderTypeException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $datasetSelected = $this->createMock(Dataset::class);
        (new Order())($anisQueryBuilder, $datasetSelected, array('o' => '1:b'));
    }
}
