<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query\Operator;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use App\Search\Query\Operator\Equal;

final class EqualTest extends TestCase
{
    private $operator;

    protected function setUp(): void
    {
        $expr = $this->getExpressionBuilderMock();
        $expr->method('literal')
            ->willReturn('\'str\'');
        $expr->method('eq')
            ->willReturn('test = \'str\'');
        $this->operator = new Equal($expr, 'test', 'string', 'str');
    }

    public function testGetExpression(): void
    {
        $this->assertSame('test = \'str\'', $this->operator->getExpression());
    }

    /**
     * @return ExpressionBuilder|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getExpressionBuilderMock()
    {
        return $this->createMock(ExpressionBuilder::class);
    }
}
