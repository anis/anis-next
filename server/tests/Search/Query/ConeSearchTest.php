<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use App\Search\Query\ConeSearch;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Query\SearchQueryException;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;
use App\Entity\ConeSearchConfig;

final class ConeSearchTest extends TestCase
{
    public function testConeSearch(): void
    {
        $id = $this->createMock(Attribute::class);
        $id->method('getId')->willReturn(1);
        $ra = $this->createMock(Attribute::class);
        $ra->method('getId')->willReturn(2);
        $dec = $this->createMock(Attribute::class);
        $dec->method('getId')->willReturn(3);
        $coneSearchConfig = $this->createMock(ConeSearchConfig::class);
        $coneSearchConfig->method('getEnabled')->willReturn(true);
        $coneSearchConfig->method('getColumnRa')->willReturn(2);
        $coneSearchConfig->method('getColumnDec')->willReturn(3);
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getConeSearchConfig')->willReturn($coneSearchConfig);
        $datasetSelected->method('getAttributes')->willReturn(array($id, $ra, $dec));

        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $expr = $this->createMock(ExpressionBuilder::class);
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('where');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = array('cs' => '102.5:0.0:100');
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testConeSearchException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $datasetSelected = $this->createMock(Dataset::class);
        $queryParams = array('cs' => '102.5:0.0:100:10');
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testConeSearchUnavailableException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $coneSearchConfig = $this->createMock(ConeSearchConfig::class);
        $coneSearchConfig->method('getEnabled')->willReturn(false);
        $datasetSelected = $this->createMock(Dataset::class);

        $queryParams = array('cs' => '102.5:0.0:100');
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testConeSearchParamsValues(): void
    {
        $id = $this->createMock(Attribute::class);
        $id->method('getId')->willReturn(1);
        $ra = $this->createMock(Attribute::class);
        $ra->method('getId')->willReturn(2);
        $dec = $this->createMock(Attribute::class);
        $dec->method('getId')->willReturn(3);
        $coneSearchConfig = $this->createMock(ConeSearchConfig::class);
        $coneSearchConfig->method('getEnabled')->willReturn(true);
        $coneSearchConfig->method('getColumnRa')->willReturn(2);
        $coneSearchConfig->method('getColumnDec')->willReturn(3);
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getConeSearchConfig')->willReturn($coneSearchConfig);
        $datasetSelected->method('getAttributes')->willReturn(array($id, $ra, $dec));

        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $expr = $this->createMock(ExpressionBuilder::class);
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('where');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = array('cs' => '102.5:91:0');
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }
}
