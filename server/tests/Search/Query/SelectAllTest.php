<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use App\Search\Query\SelectAll;
use App\Search\Query\AnisQueryBuilder;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;

final class SelectAllTest extends TestCase
{
    public function testCount(): void
    {
        $id = $this->createMock(Attribute::class);
        $id->method('getId')->willReturn(1);
        $ra = $this->createMock(Attribute::class);
        $ra->method('getId')->willReturn(2);
        $dec = $this->createMock(Attribute::class);
        $dec->method('getId')->willReturn(3);
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getAttributes')->willReturn(array($id, $ra, $dec));
        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->expects($this->once())->method('select');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = array('a' => 'all');
        (new SelectAll())($anisQueryBuilder, $datasetSelected, $queryParams);
    }
}
