<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Response;

use PHPUnit\Framework\TestCase;
use App\Search\Response\ResponseFactory;
use App\Search\Response\SearchResponseException;

final class ResponseFactoryTest extends TestCase
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    protected function setUp(): void
    {
        $this->responseFactory = new ResponseFactory();
    }

    public function testUnknownResponseException(): void
    {
        $this->expectException(SearchResponseException::class);
        $this->responseFactory->create('unknown');
    }

    public function testCreateJsonResponse(): void
    {
        $json = $this->responseFactory->create('json');
        $this->assertInstanceOf('App\Search\Response\JsonResponse', $json);
    }

    public function testCreateCsvResponse(): void
    {
        $csv = $this->responseFactory->create('csv');
        $this->assertInstanceOf('App\Search\Response\TextResponse', $csv);
    }

    public function testCreateAsciiResponse(): void
    {
        $ascii = $this->responseFactory->create('ascii');
        $this->assertInstanceOf('App\Search\Response\TextResponse', $ascii);
    }

    public function testCreateVotableResponse(): void
    {
        $votable = $this->responseFactory->create('votable');
        $this->assertInstanceOf('App\Search\Response\VotableResponse', $votable);
    }
}
