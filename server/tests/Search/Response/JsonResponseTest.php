<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Response;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\Response;
use Doctrine\DBAL\Result;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Response\JsonResponse;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Attribute;

final class JsonResponseTest extends TestCase
{
    public function testGetResponse(): void
    {
        $stmt = $this->getResultMock();
        $stmt->method('fetchAssociative')->willReturnOnConsecutiveCalls(array(
            'id' => 1,
            'ra' => 102.5,
            'dec' => 0.1,
            'json' => '{"mode":"GP","name":"MXT-EVT-CAL"}'
        ), false);

        $id = $this->getAttributeMock();
        $id->method('getLabel')->willReturn('id');
        $id->method('getType')->willReturn('integer');
        $ra = $this->getAttributeMock();
        $ra->method('getLabel')->willReturn('ra');
        $ra->method('getType')->willReturn('float');
        $dec = $this->getAttributeMock();
        $dec->method('getLabel')->willReturn('dec');
        $dec->method('getType')->willReturn('float');
        $json = $this->getAttributeMock();
        $json->method('getLabel')->willReturn('json');
        $json->method('getType')->willReturn('json');

        $doctrineQueryBuilder = $this->getDoctrineQueryBuilderMock();
        $doctrineQueryBuilder->method('executeQuery')->willReturn($stmt);
        $anisQueryBuilder = $this->getAnisQueryBuilderMock();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $anisQueryBuilder->method('getAttributesSelected')->willReturn(array($id, $ra, $dec, $json));

        $textResponse = new JsonResponse();
        $r = new Response(200, array(
            'Content-Type' => 'application/json'
        ));
        $response = $textResponse->getResponse(
            $r,
            $anisQueryBuilder
        );
        $this->assertSame('application/json', $response->getHeaders()['Content-Type'][0]);
        $this->assertSame(
            '[{"id":1,"ra":102.5,"dec":0.1,"json":{"mode":"GP","name":"MXT-EVT-CAL"}}]',
            (string) $response->getBody()
        );
    }

    /**
     * @return Result|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getResultMock()
    {
        return $this->createMock(Result::class);
    }

    /**
     * @return Attribute|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getAttributeMock()
    {
        return $this->createMock(Attribute::class);
    }

    /**
     * @return DoctrineQueryBuilder|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDoctrineQueryBuilderMock()
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    /**
     * @return AnisQueryBuilder|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getAnisQueryBuilderMock()
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
