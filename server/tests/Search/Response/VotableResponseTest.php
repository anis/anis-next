<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Response;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\Response;
use Doctrine\DBAL\Result;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Response\VotableResponse;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Attribute;

final class VotableResponseTest extends TestCase
{
    public function testGetResponse(): void
    {
        $stmt = $this->getResultMock();
        $stmt->method('fetch')->willReturnOnConsecutiveCalls(array(
            'id' => 1,
            'ra' => 102.5,
            'dec' => 0.1
        ), false);

        $id = $this->getAttributeMock();
        $id->method('getLabel')->willReturn('id');
        $ra = $this->getAttributeMock();
        $ra->method('getLabel')->willReturn('ra');
        $ra->method('getVoUcd')->willReturn('pos.eq.ra');
        $ra->method('getVoDatatype')->willReturn('double');
        $ra->method('getVoUnit')->willReturn('deg');
        $ra->method('getVoSize')->willReturn('19*');
        $ra->method('getVoUtype')->willReturn('test_utype');
        $ra->method('getVoDescription')->willReturn('Test description');
        $dec = $this->getAttributeMock();
        $dec->method('getLabel')->willReturn('dec');

        $connection = $this->getConnectionMock();
        $connection->method('fetchOne')->willReturn(10);

        $doctrineQueryBuilder = $this->getDoctrineQueryBuilderMock();
        $doctrineQueryBuilder->method('execute')->willReturn($stmt);
        $doctrineQueryBuilder->method('getConnection')->willReturn($connection);
        $anisQueryBuilder = $this->getAnisQueryBuilderMock();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $anisQueryBuilder->method('getAttributesSelected')->willReturn(array($id, $ra, $dec));

        $textResponse = new VotableResponse();
        $response = $textResponse->getResponse(new Response(), $anisQueryBuilder);
        $this->assertSame('text/xml', $response->getHeaders()['Content-Type'][0]);
    }

    /**
     * @return Result|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getResultMock()
    {
        return $this->createMock(Result::class);
    }

    /**
     * @return Attribute|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getAttributeMock()
    {
        return $this->createMock(Attribute::class);
    }

    /**
     * @return Connection|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getConnectionMock()
    {
        return $this->createMock(Connection::class);
    }

    /**
     * @return DoctrineQueryBuilder|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDoctrineQueryBuilderMock()
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    /**
     * @return AnisQueryBuilder|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getAnisQueryBuilderMock()
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
