<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query\Operator;

use PHPUnit\Framework\TestCase;
use App\Search\DBALConnectionFactory;
use App\Entity\Database;

final class DBALConnectionFactoryTest extends TestCase
{
    /**
     * @var DBALConnectionFactory
     */
    private $connectionFactory;

    protected function setUp(): void
    {
        $this->connectionFactory = new DBALConnectionFactory();
    }

    public function testCreate(): void
    {
        $database = $this->getDatabaseMock();
        $database->method('getDbName')
            ->willReturn('test');
        $database->method('getLogin')
            ->willReturn('user');
        $database->method('getPassword')
            ->willReturn('password');
        $database->method('getHost')
            ->willReturn('db');
        $database->method('getPort')
            ->willReturn(5432);
        $database->method('getType')
            ->willReturn('pdo_sqlite');

        $connection = $this->connectionFactory->create($database);
        $this->assertInstanceOf('Doctrine\DBAL\Connection', $connection);
    }

    /**
     * @return Database|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getDatabaseMock()
    {
        return $this->createMock(Database::class);
    }
}
